﻿Imports Telerik.Web.UI

Partial Class Reports_auction_sale_report
    Inherits System.Web.UI.Page

    Protected Sub RadGrid_Auction_List_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid_Auction_List.NeedDataSource
        Dim sel As String = ""
        If CommonCode.is_admin_user Then
            sel = "select dbo.fn_get_auction_image(A.auction_id) As image_path,isnull(A.code,'') as code,A.title,isnull(A.sub_title,'') as sub_title,REPLACE(CONVERT(VARCHAR(9), A.start_date, 6), ' ', '-') AS start_date,REPLACE(CONVERT(VARCHAR(9), A.end_date, 6), ' ', '-') AS end_date,U.*,(B.contact_first_name + ' ' + B.contact_last_name) as bidder,(S.first_name + ' ' + S.last_name) as sale_rep,isnull(C.name,'') as country from [tbl_auctions] A WITH (NOLOCK) inner join (select auction_id,buyer_id,bid_amount as price,0 as quantity,isnull(action,'') as action from tbl_auction_bids WITH (NOLOCK) where action='Awarded' union all " & _
 "select auction_id,buyer_id,price,isnull(quantity,0) as quantity,isnull(action,'') as action from tbl_auction_buy WITH (NOLOCK) where action='Awarded') U on U.auction_id=A.auction_id left join tbl_reg_buyers B WITH (NOLOCK) on U.buyer_id=B.buyer_id left join " & _
 "tbl_reg_sale_rep_buyer_mapping M on U.buyer_id=M.buyer_id inner join tbl_sec_users S on S.user_id=M.user_id left join tbl_master_countries C on B.country_id=C.country_id where (A.display_end_time<getdate())"

            RadGrid_Auction_List.DataSource = SqlHelper.ExecuteDatatable(sel)
        Else            RadGrid_Auction_List.DataSource = Nothing 'SqlHelper.ExecuteDataTable("select A.auction_id,A.code,A.title,isnull(C.name,'') as product_category,isnull(D.name,'') as auction_type,A.start_date,A.end_date, dbo.auction_status(a.auction_id) as Status from [tbl_auctions] A inner join [tbl_reg_seller_user_mapping] B on A.seller_id=B.seller_id left join tbl_master_product_categories C on A.product_catetory_id=C.product_catetory_id left join tbl_auction_types D on A.auction_type_id=D.auction_type_id where B.user_id=" & hid_user_id.Value & "")
            RadGrid_Auction_List.Visible = False
        End If

    End Sub

End Class
