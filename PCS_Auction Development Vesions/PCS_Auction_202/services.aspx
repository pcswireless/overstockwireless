﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AuctionMain.master" AutoEventWireup="false"
    CodeFile="services.aspx.vb" Inherits="services" %>
    <%@ Register Src="~/UserControls/bidder_salesrep.ascx" TagName="SalesRep" TagPrefix="UC1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <div class="innerwraper">
            <UC1:SalesRep runat="server" ID="UC_SalesRep"></UC1:SalesRep>
            <div class="paging">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li>Services</li>
                </ul>
            </div>
            <%--<div class="printbx">
                <ul>
                    <li class="print"><a href="javascript:window.print();">Print</a></li>
                    <li>
                        <asp:Literal ID="lit_pdf" runat="server" /></li>
                </ul>
            </div>--%>
            <div class="clear">
            </div>
            <div class="products">
                <h1 class="title">
                    <asp:Literal ID="tab_sel" runat="server" /></h1>
                <!--seprator -->
                <div class="clear">
                </div>
        <div class="summrybx dtltxtbx_abt">
            <div id="maincontent">
                <h1 class="pgtitle" style="margin-top: 2px;">
                    We play by the rules — your rules.</h1>
                <p>
                    <%--<img src="/pcsww/media/images/page-level/warehouse2.jpg" style="border-bottom: 0px solid; border-left: 0px solid; margin: auto 20px; width: 217px; float: right; height: 247px; border-top: 0px solid; border-right: 0px solid" alt="">--%>You’ve
                    worked hard to build your brand. And as your partner, we will help you protect your
                    brand’s reputation by engaging in responsible redistribution.</p>
                <p>
                    Over the years, we’ve built a strong, expansive network of customers from which
                    we can source and sell products. We also work directly with leading handset manufacturers
                    to procure the latest innovative products that meet market demand. Before every
                    transaction, we take the necessary steps to ensure we do not compete with our customers’
                    business objectives or do anything that could jeopardize their brands, price, or
                    channels.</p>
                <p>
                    In fact, we do just the opposite. We work with you to help you achieve your goals&nbsp;&mdash;
                    from maximizing overstock and end-of-life product to increasing your sales and market
                    share. Our strategic services are customized to address the specific challenges
                    you face, regardless of where you fall in the communications industry.&nbsp;</p>
            </div>
        </div>
    </div>
</asp:Content>
