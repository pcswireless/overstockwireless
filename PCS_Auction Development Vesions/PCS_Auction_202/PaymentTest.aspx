﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PaymentTest.aspx.vb" Inherits="PaymentTest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PCS Auction</title>
    <%--<link href="/Style/stylesheet.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript">

        /***********************************************
        * Different CSS depending on OS (mac/pc)- © Dynamic Drive (www.dynamicdrive.com)
        * This notice must stay intact for use
        * Visit http://www.dynamicdrive.com/ for full source code
        ***********************************************/
        ///////No need to edit beyond here////////////

        var mactest = navigator.userAgent.indexOf("Mac") != -1


        if (mactest) {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet_mac.css"/>');
        }
        else {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet.css"/>');
        }
  </script>
</head>
<body>
    <form id="form1" runat="server">
    <center>
        <table cellpadding="10" cellspacing="0" border="0">
            <tr>
                <td><h1>Confirmation No :</h1>
                </td>
                <td><asp:TextBox runat="server" CssClass="typtxt" ID="txt_conf_no" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_conf_no"
                                                    ValidationGroup="_sales_contact" Display="Static" ErrorMessage="*" CssClass="error"></asp:RequiredFieldValidator>
                </td>
                <td><asp:Button runat="server" ID="btn_go" CssClass="bid" ValidationGroup="_sales_contact" Text="Go" Width="50" />
                </td>
            </tr>
            <tr>
                <td align="center">
                <asp:Label ID="lbl_error" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <asp:Panel ID="pnl_details" runat="server" Visible="false">
        <table width="98%" cellspacing="0" border="0" cellpadding="5">
                <tr>
                    <td align="center" style="border: 1px solid  #E3E3E3;  width: 26%;
                        padding: 10px;">
                        <b>SHIP TO</b>
                    </td>
                    <td align="center" style="border: 1px solid  #E3E3E3;  width: 26%;
                        padding: 10px;">
                        <b>AUCTION TITLE</b>
                    </td>
                    <td align="center" style="border: 1px solid  #E3E3E3; width: 13%;">
                        <b>AUCTION NO</b>
                    </td>
                    <td align="center" style="border: 1px solid  #E3E3E3; width: 9%;">
                        <b>QTY</b>
                    </td>
                    <td align="center" style="border: 1px solid  #E3E3E3; width: 13%;">
                        <b>PRICE</b>
                    </td>
                    <td align="center" style="border: 1px solid  #E3E3E3; width: 13%;">
                        <b>AMOUNT</b>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="border: 1px solid  #E3E3E3; ">
                        <asp:Literal ID="lit_bill_to" runat="server" />
                    </td>
                    <td align="center" style="border: 1px solid  #E3E3E3;">
                        <asp:Literal ID="lit_auc_name" runat="server" />
                    </td>
                    <td style="border: 1px solid  #E3E3E3;" align="center">
                        <asp:Literal ID="lit_auc_code" runat="server" />
                    </td>
                    <td style="border: 1px solid  #E3E3E3; " align="center">
                        <asp:Literal ID="lit_qty" runat="server" />
                    </td>
                    <td style="border: 1px solid  #E3E3E3;" align="center">
                        <asp:Literal ID="lit_price" runat="server" />
                    </td>
                    <td style="border: 1px solid  #E3E3E3;" align="right">
                        <asp:Literal ID="lit_amount" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                    </td>
                    <td style="border: 1px solid  #E3E3E3;">
                        <b>Sub Total</b>
                    </td>
                    <td style="border: 1px solid  #E3E3E3;" align="right">
                    <asp:Literal ID="lit_sub_total" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                    </td>
                    <td style="border: 1px solid  #E3E3E3;">
                        <b>Tax</b>
                    </td>
                    <td style="border: 1px solid  #E3E3E3;" align="right">
                    <asp:Literal ID="lit_tax" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                    </td>
                    <td style="border: 1px solid  #E3E3E3;">
                        <b>Freight</b>
                    </td>
                    <td style="border: 1px solid  #E3E3E3;" align="right">
                    <asp:Literal ID="lit_ship_amount" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                    </td>
                    <td style="border: 1px solid  #E3E3E3;">
                        <b>Total</b>
                    </td>
                    <td style="border: 1px solid  #E3E3E3;">
                    <b><asp:Literal ID="lit_grand_total" runat="server"></asp:Literal></b>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center">
                    <asp:Literal ID="lit_payment_link" runat="server"></asp:Literal>
                    </td>
                </tr>
        </table>

        
        
        </asp:Panel>

    </center>
    </form>
</body>
</html>
