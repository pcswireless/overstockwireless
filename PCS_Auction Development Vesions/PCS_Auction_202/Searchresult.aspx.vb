﻿Imports Telerik.Web.UI

Partial Class Searchresult
    Inherits System.Web.UI.Page
    Public Vew_Auction As Boolean = False
    Public View_User As Boolean = False
    Public View_Bidder_Bulk_Listing As Boolean = False
    Private Sub setPermission()

        If CommonCode.is_super_admin() Then
            RadGrid_Auction.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
            RadGrid_Auction.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
            RadGrid_Auction.MasterTableView.CommandItemSettings.ShowExportToWordButton = True

            RadGrid_Buyer.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
            RadGrid_Buyer.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
            RadGrid_Buyer.MasterTableView.CommandItemSettings.ShowExportToWordButton = True

            RadGrid_User.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
            RadGrid_User.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
            RadGrid_User.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
            Vew_Auction = True
            View_User = True
            View_Bidder_Bulk_Listing = True
        Else

            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 2
                        Vew_Auction = True
                    Case 15
                        View_User = True
                    Case 8
                        View_Bidder_Bulk_Listing = True
                    Case 35
                        RadGrid_Auction.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
                        RadGrid_Auction.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
                        RadGrid_Auction.MasterTableView.CommandItemSettings.ShowExportToWordButton = True

                        RadGrid_Buyer.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
                        RadGrid_Buyer.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
                        RadGrid_Buyer.MasterTableView.CommandItemSettings.ShowExportToWordButton = True

                        RadGrid_User.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
                        RadGrid_User.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
                        RadGrid_User.MasterTableView.CommandItemSettings.ShowExportToWordButton = True

                End Select
            Next
            dt = Nothing
        End If

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            page_heading.Text = "Search Result : " & Request.QueryString.Get("sh")
            hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
            'Response.Write(SqlDataSource1.SelectCommand.ToString)
            Search_DS = SqlHelper.ExecuteDataset("exec sel_search_text '" & Request.QueryString.Get("sh") & "'," & hid_user_id.Value & ",'" & CommonCode.Fetch_Cookie_Shared("username") & "'")
            ' Response.Write("<script>alert('" & Search_DS.Tables.Count & "');</script>")
            Dim _rw As Int16 = 0
            
                For i = 0 To Search_DS.Tables.Count - 1
                    If Search_DS.Tables(i).Rows.Count = 0 Then
                    _rw = _rw + 1
                Else
                    tr_auction.Visible = IIf(tr_auction.Visible = False And Not Search_DS.Tables(i).Columns(0).ColumnName.Contains("auction"), False, True)
                    tr_buyer.Visible = IIf(tr_buyer.Visible = False And Not Search_DS.Tables(i).Columns(0).ColumnName.Contains("buyer"), False, True)
                    tr_user.Visible = IIf(tr_user.Visible = False And Not Search_DS.Tables(i).Columns(0).ColumnName.Contains("user"), False, True)
                End If

                Next
            If _rw = 3 Then
                lbl_msg.Text = "No record found against your search criteria."
                'tr_auction.Visible = False
                'tr_buyer.Visible = False
                'tr_user.Visible = False
            End If
        End If
    End Sub

    Protected Property Search_DS() As DataSet
        Get
            Return DirectCast(ViewState("_ds_search"), DataSet)
        End Get
        Set(ByVal value As DataSet)
            ViewState("_ds_search") = value
        End Set
    End Property

    Protected Sub RadGrid_Auction_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid_Auction.NeedDataSource
        If Vew_Auction = True Then
            RadGrid_Auction.DataSource = Search_DS.Tables(0)
        Else
            Auction_Listing.Visible = False
        End If

    End Sub

    
    Protected Sub RadGrid_Auction_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadGrid_Auction.PreRender
        Dim menu As GridFilterMenu = RadGrid_Auction.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While

        'If (Not Page.IsPostBack) Then
        '    RadGrid_Auction.MasterTableView.FilterExpression = "([Status] like '%Running%') "
        '    Dim column As GridColumn = RadGrid_Auction.MasterTableView.GetColumnSafe("status")
        '    column.CurrentFilterFunction = GridKnownFunction.Contains
        '    column.CurrentFilterValue = "Running"
        '    RadGrid_Auction.MasterTableView.Rebind()
        'End If
    End Sub

    Protected Sub RadGrid_Buyer_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid_Buyer.NeedDataSource
        If View_Bidder_Bulk_Listing = True Then
            RadGrid_Buyer.DataSource = Search_DS.Tables(1)
        Else
            Bidder_Listing.Visible = False
        End If

    End Sub

    Protected Sub RadGrid_Buyer_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid_Buyer.PreRender
        Dim menu As GridFilterMenu = RadGrid_Buyer.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
        'If (Not Page.IsPostBack) Then
        '    RadGrid_Buyer.MasterTableView.FilterExpression = "([status] = 'Approved') "
        '    Dim column As GridColumn = RadGrid_Buyer.MasterTableView.GetColumnSafe("status")
        '    column.CurrentFilterFunction = GridKnownFunction.EqualTo
        '    column.CurrentFilterValue = "Approved"
        '    RadGrid_Buyer.MasterTableView.Rebind()
        'End If
    End Sub

    Protected Sub RadGrid_User_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid_User.NeedDataSource
        If View_User = True Then
            RadGrid_User.DataSource = Search_DS.Tables(2)
        Else
            User_Listing.Visible = False
        End If

    End Sub

    Protected Sub RadGrid_User_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid_User.PreRender
        Dim menu As GridFilterMenu = RadGrid_User.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
        'If (Not Page.IsPostBack) Then
        '    RadGrid_User.MasterTableView.FilterExpression = "([is_active] = True) "
        '    Dim column As GridColumn = RadGrid_User.MasterTableView.GetColumnSafe("is_active")
        '    column.CurrentFilterFunction = GridKnownFunction.EqualTo
        '    column.CurrentFilterValue = "True"
        '    RadGrid_User.MasterTableView.Rebind()
        'End If
    End Sub
End Class
