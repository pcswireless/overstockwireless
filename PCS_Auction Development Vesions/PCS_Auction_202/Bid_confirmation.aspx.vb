﻿
Partial Class Bid_confirmation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Response.Write("<script language='javascript'>top.topFrame.location = '/SiteTopBar.aspx?t=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("t")), "0", Request.QueryString.Get("t")) & "';top.leftFrame.location= '/SiteLeftBar.aspx?t=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("t")), "0", Request.QueryString.Get("t")) & "';</script>")
            If Not Request.QueryString("bid_id") Is Nothing Then
                hid_bid_id.Value = Request.QueryString("bid_id")
            End If
            If Not Request.QueryString("bid_type") Is Nothing Then
                hid_bid_type.Value = Request.QueryString("bid_type")
            End If
            show_message()
        End If
    End Sub
    Private Sub show_message()
        Dim bid_type As String = ""
        bid_type = hid_bid_type.Value.ToUpper()
        If bid_type = "D" Then
            lbl_message.Text = "You have done bidding successfully. <br><br>You can check your bidding status by clicking <b>My Account</b> on the top right corner"
        ElseIf bid_type = "PR" Then
            lbl_message.Text = "You have successfully placed the order as private offer. <br><br>You can check your bidding status by clicking <b>My Account</b> on the top right corner"
        ElseIf bid_type = "PA" Then
            lbl_message.Text = "You have successfully placed the order as partial offer. <br><br>You can check your bidding status by clicking <b>My Account</b> on the top right corner"
        ElseIf bid_type = "BU" Then
            lbl_message.Text = "Your order has been successfully placed. <br><br>You can check your bidding status by clicking <b>My Account</b> on the top right corner"
        ElseIf bid_type = "Q" Then
            lbl_message.Text = "Your offer has been sent successfully. <br><br>You can check your bidding status by clicking <b>My Account</b> on the top right corner"
        ElseIf bid_type = "L" Then
            lit_heading.Text = "SORRY"
            lbl_thank_you.Visible = False
            lbl_message.Text = "Sorry, Bid amount exceeds your bidding limit."
        ElseIf bid_type = "O" Then
            lit_heading.Text = "SORRY"
            lbl_thank_you.Visible = False
            lbl_message.Text = "Sorry, Bid time is over."
        End If
    End Sub
End Class
