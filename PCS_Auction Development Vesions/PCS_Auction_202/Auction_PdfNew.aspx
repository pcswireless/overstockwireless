﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Auction_PdfNew.aspx.vb" Inherits="Auction_PdfNew" EnableEventValidation="false" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    
     <link href="/Style/stylesheet.css" rel="stylesheet" type="text/css" /> 
</head>
<body style="font-size: 12px; font-family: UniSansRegular,Trebuchet MS, Arial, Helvetica, sans-serif;">
    <form id="form1" runat="server">
    
   
    <asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
    <asp:HiddenField ID="hid_auction_type" runat="server" Value="0" />
     <div class="innerwraper wraper1" style="height:32px;margin-top:10px;">
            <div style="float:left; width:900px; text-align:right;">
                <asp:Button runat="server" ID="but_to_pdf" Text="Save as pdf" />
            </div> 
            
            <div style="float:right;">
               <asp:ImageButton ID="img_close" runat="server" AlternateText="Close" />
            </div>    
        </div>
    <!--start-->
        <asp:Panel ID="pnlContent" runat="server">
    <div class="detail" style="padding:10px;">
      
        <div class="innerwraper wraper1" style="border: 2px solid Grey; margin-top: 20px; margin-bottom: 10px; padding-top:10px; width:900px;">
            <div style="background-color:white;padding-left:10px; padding-bottom:8px; padding-top:6px;">
                  <asp:Image ID="imgLogo" runat="server" />            
            </div>
             
            <div>
                <div class="products1">
                    <div class="detailbxwrap">
                        <div style="width:258px; float:left; background:#fff; text-align:center; padding:20px 10px 10px 10px; *padding:20px 0px 10px 0px;">
                            <div id="rg-gallery" class="rg-gallery">
                                <div class="rg-thumbs">
                                    <asp:Image ID="imgImage" runat="server" AlternateText=""/>
                                </div>
                            </div>
                        </div>
                        <!--lftside -->
                        <div style="width:460px; float:right;">
                            
                              <h2><asp:Literal ID="ltr_title" Text="title" runat="server" /></h2> 
                            <span class="grytxt">
                                <asp:Literal ID="ltr_sub_title" Text="sub title" runat="server" /></span><br />
                            <span class="grytxt">Auction # : <strong>
                                <asp:Literal ID="ltr_auction_code" runat="server" /></strong></span>
                            <div class="prodetail">
                                <asp:Panel runat="server" ID="pnl_from_live">
                                   <%-- <iframe id="iframe_price_summary" width="100%" scrolling="no" frameborder="0" runat="server"></iframe>

                                    <div class="clear">
                                    </div>
                                    <iframe id="iframe_auction_rank" scrolling="no" frameborder="0" width="100%" runat="server"
                                        height="55px"></iframe>
                                    <div class="clear">
                                    </div>--%>
                                    <%-- <div class="timeleft">
                                        <div class="tmlft">
                                           <asp:Literal runat="server" ID="ltr_timer_caption" Text=""></asp:Literal>
                                        </div>

                                        <div class="hrtime" style="height: 50px">
                                            <iframe id="iframe_auction_time" scrolling="no" frameborder="0" width="100%" runat="server"
                                                height="50px"></iframe>
                                        </div>
                                        <!--htime -->
                                        <div class="clear">
                                        </div>
                                    </div>--%>
                                    <div class="clear">
                                    </div>
                                    <!--timeklft -->
                                    <asp:HiddenField ID="hid_buy_now_confirm_amount" runat="server" Value="0" />
                                    <div class="clear">
                                    </div>
                                    <!--shpmthd -->
                                    <div class="bidnw">
                                        <div class="cs_Validator" id="div_Validator">
                                            <span id="spn_valid"></span><span id="spn_amt_hlp" style="display: none;">
                                                <asp:Literal ID="ltr_amt_help" runat="server"></asp:Literal>
                                            </span>
                                        </div>
                                       
                                        <div class="clear">
                                        </div>
                                        <p>
                                            <asp:Literal ID="ltr_bid_note" runat="server"></asp:Literal>
                                        </p>
                                    </div>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnl_from_account">
                                    <asp:Literal runat="server" ID="ltr_offer_attachement"></asp:Literal>
                                    <asp:Literal runat="server" ID="ltr_current_status"></asp:Literal>
                                </asp:Panel>
                                <div class="clear">
                                </div>
                                
                            </div>
                            
                        </div>
                       
                        <div class="clear">
                        </div>
                    </div>
                    
                    <div class="clear">
                    </div>

                   
                    <div id="history-popup" style="display: none;">
                        <div class="inputboxes">
                            <asp:Repeater ID="rep_bid_history" runat="server">
                                <ItemTemplate>
                                    <div style="clear: both;">
                                        <div style="float: left; width: 180px; padding: 5px 0px 5px 10px;">
                                            <b>
                                                <%# Eval("bidder")%>
                                            </b>
                                            <br />
                                            <span class="auctionSubTitle">
                                                <%# Eval("email")%></span>
                                        </div>
                                        <div style="float: left; width: 100px; padding: 5px 0px 5px 10px; color: #006FD5;">
                                            <b>
                                                <%# "$" & FormatNumber(Eval("bid_amount"), 2)%></b>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <div style="background-color: #F5F5F5; overflow: hidden; clear: both;">
                                        <div style="float: left; width: 180px; padding: 5px 0px 5px 10px;">
                                            <b>
                                                <%# Eval("bidder")%>
                                            </b>
                                            <br />
                                            <span class="auctionSubTitle">
                                                <%# Eval("email")%></span>
                                        </div>
                                        <div style="float: left; width: 100px; padding: 5px 0px 5px 10px; color: #006FD5;">
                                            <b>
                                                <%# "$" & FormatNumber(Eval("bid_amount"), 2)%></b>
                                        </div>
                                    </div>
                                </AlternatingItemTemplate>
                            </asp:Repeater>
                            <span class="text">
                                <asp:Literal ID="empty_data" runat="server" Text="No bid submitted for this auction."></asp:Literal></span>
                        </div>
                        <div class="clear">
                        </div>
                        <input type="button" name="" id="historyClose" class="fleft" value="Cancel" />
                    </div>
                </div>
               
                <div class="clear">
                </div>
                <br />
                <asp:Literal runat="server" ID="ltr_summary_detail"></asp:Literal>
               <br />
                <div runat="server" id="dv_items_details" class="details dtltxtbx">
                    <h1 style="font-size:16px; color:#3e3e3e; font-weight:normal; text-decoration:none; font-family:'dinregular', arial;">Items</h1>

                    <div style="font-size:14px; color:#3d3a37; font-weight:normal; text-decoration:none; font-family:'dinregular', arial; margin-top:20px; line-height:36px;">
                        <div>
                            <div>
                                <table style="border-collapse: collapse;border-spacing: 0;width:800px" border="1">
                                    <tr>
                                        <th>Manufacturer
                                        </th>
                                        <th>Title
                                        </th>
                                        <th>Part #
                                        </th>
                                        <th>Quantity
                                        </th>
                                        <th>UPC
                                        </th>
                                        <th>SKU
                                        </th>
                                        <th>Estimated MSRP
                                        </th>
                                        <th>Total MSRP
                                        </th>
                                    </tr>
                                    <tr>
                                        <asp:Repeater ID="rptItems" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="background-color: White;">
                                                        <%# Eval("manufacturer")%>
                                                    </td>
                                                    <td style="background-color: White;">
                                                        <%# Eval("title")%>
                                                    </td>
                                                    <td style="background-color: White;">
                                                        <%# Eval("part_no")%>
                                                    </td>
                                                    <td style="background-color: White;">
                                                        <%# Eval("quantity")%>
                                                    </td>
                                                    <td style="background-color: White;">
                                                        <%# Eval("UPC")%>
                                                    </td>
                                                    <td style="background-color: White;">
                                                        <%# Eval("SKU")%>
                                                    </td>
                                                    <td style="background-color: White;">
                                                        <%# CommonCode.GetFormatedMoney(Eval("estimated_msrp"))%>
                                                    </td>
                                                    <td style="background-color: White;">
                                                        <%# CommonCode.GetFormatedMoney(Eval("total_msrp"))%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
      
                </div>
                <br />
                <asp:Literal runat="server" ID="ltr_auction_details"></asp:Literal>
              
            </div>
        </div>
         
    </div>
            </asp:Panel>
    <!--end-->
</form>
</body>
</html>
