﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="Buyers.aspx.vb" Inherits="Bidders_Buyers" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                     args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
                else {
                    args.set_enableAjax(true);
                }
            }

        </script>
    </telerik:RadScriptBlock>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Bidder Listing</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; width: 40%; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    Please select the bidder to edit from below
                </div>
                <div style="float: right; width: 40%; text-align: right; margin-bottom: 3px;" id="div_add_new_bidder"
                    runat="server">
                    <a style="text-decoration: none" href="/Bidders/AddEditBuyer.aspx">
                        <img src="/images/add_new_bidder.gif" style="border: none" alt="Add New Bidder" /></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Vista">
                    <ClientEvents OnRequestStart="Cancel_Ajax" />
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="RadGrid1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="Rad_panel">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
                    left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
                    <img id="Image8" src="/images/img_loading.gif" />
                </telerik:RadAjaxLoadingPanel>
                <telerik:RadAjaxPanel ID="Rad_panel" runat="server">
                    <telerik:RadGrid ID="RadGrid1" GridLines="None" runat="server" PageSize="10" AllowPaging="True"
                        AutoGenerateColumns="False" AllowMultiRowSelection="false" Skin="Vista" AllowFilteringByColumn="True"
                        AllowSorting="true" ShowGroupPanel="True" EnableLinqExpressions="false">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <ExportSettings IgnorePaging="true" FileName="Bidder_Export" Pdf-AllowAdd="true"
                            Pdf-AllowCopy="true" Pdf-AllowPrinting="true" Pdf-PaperSize="A4" Pdf-Creator="a1"
                            OpenInNewWindow="true" ExportOnlyData="true" />
                        <MasterTableView DataKeyNames="buyer_id" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                            AllowFilteringByColumn="True" AllowSorting="true" ItemStyle-Height="40" AlternatingItemStyle-Height="40"
                            CommandItemDisplay="Top">
                            <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false"
                                ShowExportToPdfButton="false" ShowExportToCsvButton="false" ShowAddNewRecordButton="false"
                                ShowRefreshButton="false" />
                            <NoRecordsTemplate>
                                Bidders not available
                            </NoRecordsTemplate>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                            </SortExpressions>
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="Contact Person" DataField="name" UniqueName="name"
                                    HeaderStyle-Width="250" ItemStyle-Width="250" SortExpression="name" FilterControlWidth="120"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                    GroupByExpression="name [Contact Person] Group By name">
                                    <ItemTemplate>
                                        <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');"
                                            onclick="redirectIframe('','/Backend_Leftbar.aspx?t=2&b=<%# Eval("buyer_id")%>','/Bidders/AddEditBuyer.aspx?i=<%# Eval("buyer_id") %>')">
                                            <%# Eval("name")%>
                                        </a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="company_name" FilterControlWidth="125" HeaderText="Company"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                    SortExpression="company_name" UniqueName="company_name">
                                </telerik:GridBoundColumn>
                                
                                <telerik:GridBoundColumn DataField="country_name" FilterControlWidth="125" HeaderText="Country"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                    SortExpression="country_name" UniqueName="country_name">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="sales_rep" FilterControlWidth="125" HeaderText="Sales Rep"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                    SortExpression="sales_rep" UniqueName="sales_rep" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="status" DataField="status" SortExpression="status"
                                    HeaderText="Status" GroupByExpression="status [Status] group by status">
                                    <FilterTemplate>
                                        <telerik:RadComboBox ID="RadComboBoxStatus" DataSourceID="SqlDataSource2" DataTextField="status"
                                            DataValueField="status" Height="200px" AppendDataBoundItems="true" runat="server"
                                            OnClientSelectedIndexChanged="StatusIndexChanged" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("status").CurrentFilterValue %>'>
                                            <Items>
                                                <telerik:RadComboBoxItem Text="All" />
                                            </Items>
                                        </telerik:RadComboBox>
                                        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                            <script type="text/javascript">
                                                function StatusIndexChanged(sender, args) {
                                                    var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                    tableView.filter("status", args.get_item().get_value(), "Contains");

                                                }
                                            </script>
                                        </telerik:RadScriptBlock>
                                    </FilterTemplate>
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false"
                                    ItemStyle-Width="50" HeaderStyle-Width="50" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <a href="javascript:void(0);" onclick="redirectIframe('','/Backend_Leftbar.aspx?t=2&b=<%# Eval("buyer_id")%>','/Bidders/AddEditBuyer.aspx?i=<%# Eval("buyer_id") %>')">
                                            <img src="/images/edit.png" style="border: none;" alt="Edit" /></a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings AllowDragToGroup="true">
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                        <GroupingSettings ShowUnGroupButton="true" />
                    </telerik:RadGrid>
                    <asp:SqlDataSource ID="SqlDataSource2" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                        ProviderName="System.Data.SqlClient" SelectCommand="select status_id,status from [tbl_reg_buyser_statuses]"
                        runat="server"></asp:SqlDataSource>
                </telerik:RadAjaxPanel>
            </td>
        </tr>
    </table>
</asp:Content>
