﻿Imports Telerik.Web.UI

Partial Class Bidders_Buyers
    Inherits System.Web.UI.Page
  
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Private Sub setPermission()
        If CommonCode.is_super_admin() Then
            div_add_new_bidder.Visible = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
        Else
            div_add_new_bidder.Visible = False

            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDataTable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 6
                        div_add_new_bidder.Visible = True
                    Case 35
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
                End Select
            Next
            dt = Nothing


        End If
    End Sub
    Protected Sub RadGrid1_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid1.NeedDataSource
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            'Dim strQuery As String = "select [buyer_id],[company_name] AS company,[contact_title],contact_first_name +' ' + contact_last_name As name,email,is_active from [tbl_reg_buyers] where buyer_id in (select buyer_id from tbl_reg_buyer_seller_mapping where seller_id in (select seller_id from tbl_reg_seller_user_mapping where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & "))"
            Dim strQuery As String = ""
            If CommonCode.is_admin_user() Then
                strQuery = "select A.buyer_id , A.company_name , A.contact_title ,A.contact_first_name +' ' + A.contact_last_name + '<br />' + A.contact_title As name,A.email,ISNULL(A.status_id,0) AS status_id,ISNULL(S.status,'') As status,ISNULL(A.mobile,'') As phone1,isnull((Stuff((Select ' / ' + su.first_name+' '+su.last_name From tbl_reg_sale_rep_buyer_mapping sbm join tbl_sec_users su on su.user_id=sbm.user_id Where  sbm.buyer_id=A.buyer_id FOR XML PATH('')),1,2,'')),'') as sales_rep,isnull(mc.name,'') as country_name  from  tbl_reg_buyers A left join tbl_master_countries mc on A.country_id=mc.country_id inner join tbl_reg_buyser_statuses S on A.status_id=S.status_id"
            Else
                strQuery = "select A.buyer_id , A.company_name, A.contact_title ,A.contact_first_name +' ' + A.contact_last_name + '<br />' + A.contact_title As name,A.email,ISNULL(A.status_id,0) AS status_id,ISNULL(S.status,'') As status,ISNULL(A.mobile,'') As phone1,isnull((Stuff((Select ' / ' + su.first_name+' '+su.last_name From tbl_reg_sale_rep_buyer_mapping sbm join tbl_sec_users su on su.user_id=sbm.user_id Where  sbm.buyer_id=A.buyer_id FOR XML PATH('')),1,2,'')),'') as sales_rep,isnull(mc.name,'') as country_name from  tbl_reg_buyers A  left join tbl_master_countries mc on A.country_id=mc.country_id  inner join tbl_reg_buyser_statuses S on A.status_id=S.status_id INNER JOIN tbl_reg_buyer_users B ON A.buyer_id=B.buyer_id where B.is_admin=1 AND A.buyer_id in (select buyer_id from tbl_reg_buyer_seller_mapping where seller_id in (select seller_id from tbl_reg_seller_user_mapping where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & "))"
            End If

            RadGrid1.DataSource = SqlHelper.ExecuteDataTable(strQuery)
            If (Not Page.IsPostBack) Then
                RadGrid1.MasterTableView.FilterExpression = "([status] = 'Approved') "
                Dim column As GridColumn = RadGrid1.MasterTableView.GetColumnSafe("status")
                column.CurrentFilterFunction = GridKnownFunction.EqualTo
                column.CurrentFilterValue = "Approved"

            End If
        End If
        'RadGrid1.Rebind()

    End Sub

    Protected Sub RadGrid1_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles RadGrid1.PageIndexChanged
        RadGrid1.Rebind()
    End Sub

    Protected Sub RadGrid1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadGrid1.PreRender
        Dim menu As GridFilterMenu = RadGrid1.FilterMenu
        Dim i As Integer = 0
        While i < menu.Items.Count
            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub

    Protected Sub RadGrid1_SortCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridSortCommandEventArgs) Handles RadGrid1.SortCommand
        RadGrid1.Rebind()
    End Sub
End Class
