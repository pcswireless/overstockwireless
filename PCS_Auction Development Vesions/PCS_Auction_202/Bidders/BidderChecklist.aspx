﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="BidderChecklist.aspx.vb" Inherits="Bidders_BidderChecklist" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Skin="Simple" />
    <input type="hidden" id="hid_manufacturer_combo_id" name="hid_manufacturer_combo_id"
        value="0" />
    <asp:TextBox ID="txt_test" runat="server" Visible="false"></asp:TextBox>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" alt="" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Bidder Checklist</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxPanel ID="pnl1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                    <telerik:RadGrid ID="RadGrid_checklist" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                        AllowAutomaticInserts="True" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True"
                        AllowSorting="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                        AllowMultiRowSelection="false" AllowMultiRowEdit="false" Skin="Vista" ShowGroupPanel="false">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <HeaderStyle BackColor="#BEBEBE" />
                        <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="checklist_id"
                            DataSourceID="SqlDataSource1" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                            SkinID="Vista" ItemStyle-Height="40" AlternatingItemStyle-Height="40">
                            <CommandItemStyle BackColor="#E1DDDD" />
                            <NoRecordsTemplate>
                                Checklist not available
                            </NoRecordsTemplate>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                            </SortExpressions>
                            <CommandItemSettings AddNewRecordText="Add New Checklist" />
                            <Columns>
                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                    EditImageUrl="/Images/edit_grid.gif">
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                </telerik:GridEditCommandColumn>
                                <telerik:GridTemplateColumn HeaderText="Title Name" SortExpression="name" UniqueName="name"
                                    DataField="name" EditFormHeaderTextFormat="Title Name">
                                    <ItemTemplate>
                                        <%# Eval("name")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Description" SortExpression="description"
                                    UniqueName="description" DataField="description" EditFormHeaderTextFormat="Description">
                                    <ItemTemplate>
                                        <%# Eval("description")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"
                                    ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                    CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="50" />
                                </telerik:GridButtonColumn>
                            </Columns>
                            <EditFormSettings InsertCaption="New Items" EditFormType="Template">
                                <FormTemplate>
                                    <table cellpadding="0" cellspacing="2" border="0" width="800px">
                                        <tr>
                                            <td style="font-weight: bold; padding-top: 10px;" colspan="4">
                                                Checklist Details
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 135px;">
                                                Name&nbsp;<span class="req_star">*</span>
                                            </td>
                                            <td style="width: 260px;" class="details">
                                                <asp:TextBox ID="txt_grd_name" runat="server" Text='<%#Bind("name") %>' CssClass="inputtype" />
                                                <asp:RequiredFieldValidator ID="Req_txt_grd_name" runat="server" Font-Size="10px"
                                                    ValidationGroup="grd_items" ControlToValidate="txt_grd_name" Display="Dynamic"
                                                    ErrorMessage="<br>Name Required"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 135px;">
                                                Description
                                            </td>
                                            <td style="width: 260px;" class="details">
                                                <asp:TextBox ID="txt_description" runat="server" Text='<%#Bind("description") %>'
                                                    Rows="5" Height="60" TextMode="MultiLine" CssClass="inputtype" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" colspan="2">
                                                <asp:ImageButton ID="but_grd_submit" CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem),"PerformInsert","Update") %>'
                                                    ValidationGroup="grd_items" OnClientClick="return ValidationGroupEnable('grd_items', true);"
                                                    AlternateText='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "Save" , "Update") %>'
                                                    runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif" , "/images/update.gif") %>' />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:ImageButton ID="but_grd_cancel" CausesValidation="false" CommandName="Cancel"
                                                    runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </FormTemplate>
                            </EditFormSettings>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                    </telerik:RadGrid>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                        ProviderName="System.Data.SqlClient" SelectCommand="select [checklist_id],[name],[description] from [tbl_master_bidder_checklist]"
                        DeleteCommand="DELETE FROM [tbl_master_bidder_checklist] WHERE [checklist_id] = @checklist_id"
                        InsertCommand="INSERT INTO tbl_master_bidder_checklist(name,description) VALUES (@name,@description)"
                        UpdateCommand="UPDATE tbl_master_bidder_checklist SET name= @name, description=@description WHERE checklist_id= @checklist_id">
                        <DeleteParameters>
                            <asp:Parameter Name="checklist_id" Type="Int32" />
                        </DeleteParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="checklist_id" Type="Int32" />
                            <asp:Parameter Name="name" Type="String" />
                            <asp:Parameter Name="description" Type="String" />
                        </UpdateParameters>
                        <InsertParameters>
                            <asp:Parameter Name="name" Type="String" />
                            <asp:Parameter Name="description" Type="String" />
                        </InsertParameters>
                    </asp:SqlDataSource>
                </telerik:RadAjaxPanel>
            </td>
        </tr>
    </table>
</asp:Content>
