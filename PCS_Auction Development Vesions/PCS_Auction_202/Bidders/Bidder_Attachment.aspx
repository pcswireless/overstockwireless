﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendPopUP.master" AutoEventWireup="false" CodeFile="Bidder_Attachment.aspx.vb" Inherits="Bidders_Bidder_Attachment" %>
<%@ Register Src="~/Bidders/UserControls/Bidder_Attachments.ascx" TagName="BidderAttach" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table cellpadding="10" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Bidder Attachments
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <uc:BidderAttach ID="UC_BidderAttach" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_msg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        window.focus();
    </script>
</asp:Content>

