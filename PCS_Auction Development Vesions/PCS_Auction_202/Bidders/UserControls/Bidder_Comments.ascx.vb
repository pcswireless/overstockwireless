﻿Imports Telerik.Web.UI
Partial Class Bidders_UserControls_Bidder_Comments
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                hid_buyer_id.Value = Request.QueryString.Get("i")
                hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
            End If
        End If
    End Sub

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub

    Private Sub setPermission()
        If Not CommonCode.is_super_admin() Then

            Dim Edit_Bidder As Boolean = False

            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 7
                        Edit_Bidder = True
                End Select
            Next
            dt = Nothing

            If Edit_Bidder = False Then
                RadGrid_Comments.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None
            End If

        End If
    End Sub
    Protected Sub RadGrid_Comments_ItemInserted(ByVal source As Object, ByVal e As Telerik.Web.UI.GridInsertedEventArgs) Handles RadGrid_Comments.ItemInserted
        Dim Str As String

        If Not e.Exception Is Nothing Then
            e.ExceptionHandled = True
            e.KeepInInsertMode = True
            Str = "Comment cannot be inserted. Reason: " + e.Exception.Message
        Else
            Str = "New Comment is inserted!"
            CommonCode.insert_system_log("New comment inserted", "RadGrid_Comments_ItemInserted", Request.QueryString.Get("i"), "", "Bidder")
        End If
        RadGrid_Comments.Controls.Add(New LiteralControl(String.Format("<span style='color:red'>" & Str & "</span>")))
    End Sub
  
End Class
