﻿Imports Telerik.Web.UI
Partial Class Bidders_UserControls_Bidder_Attachments
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

    End Sub
#Region "Attachments"
    Protected Sub RadGrid_Attachment_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            Dim strQuery As String = ""
            strQuery = "SELECT buyer_attachment_id, ISNULL(A.buyer_id, 0) AS buyer_id, ISNULL(A.title, '') AS title,	ISNULL(A.upload_date, '1/1/1900') AS upload_date, " & _
            "ISNULL(A.upload_by_user_id, 0) AS upload_by_user_id, " & _
            "ISNULL(A.filename, '') AS filename, " & _
            "(ISNULL(U.first_name,'') + ' ' + ISNULL(U.last_name,'')) AS upload_by_user " & _
            "FROM tbl_reg_buyer_attachments A INNER JOIN tbl_sec_users U on A.upload_by_user_id=U.user_id where A.buyer_id=" & Request.QueryString.Get("i") & " order by upload_date desc"
            RadGrid_Attachment.DataSource = SqlHelper.ExecuteDatatable(strQuery)
        End If
    End Sub
    Protected Sub RadGrid_Attachment_DeleteCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)

        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        Dim attachment_id As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("buyer_attachment_id").ToString()
        Try
            SqlHelper.ExecuteNonQuery("DELETE from tbl_reg_buyer_attachments where buyer_attachment_id='" & attachment_id & "'")
            CommonCode.insert_system_log("Bidder financial information related attachment deleted", "RadGrid_Attachment_DeleteCommand", Request.QueryString.Get("i"), "", "Bidder")
            redirect()
        Catch ex As Exception
            RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to delete Attachment. Reason: " + ex.Message))
            e.Canceled = True
        End Try

    End Sub
    Protected Sub RadGrid_Attachment_UpdateCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditableItem of the RadGrid        
        Dim editedItem As GridEditFormItem = TryCast(e.Item, GridEditFormItem)
        'Get the primary key value using the DataKeyValue.        
        Dim attachment_id As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("buyer_attachment_id").ToString()
        'Access the textbox from the edit form template and store the values in string variables.        

        Dim Title As String = (TryCast(editedItem.FindControl("txt_attach_title"), TextBox)).Text
        Dim filename As String = ""
        Dim FileUpload As System.Web.UI.WebControls.FileUpload = TryCast(editedItem.FindControl("fileupload_attach"), System.Web.UI.WebControls.FileUpload)
        If FileUpload.HasFile Then filename = FileUpload.FileName
        If FileUpload.HasFile Then
            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_attachments set title='" & CommonCode.encodeSingleQuote(Title) & "', filename='" & filename & "' where buyer_attachment_id=" & attachment_id)
            UploadFileAttachment(FileUpload, Request.QueryString.Get("i"), attachment_id)
            CommonCode.insert_system_log("Bidder financial information related attachment updated", "RadGrid_Attachment_UpdateCommand", Request.QueryString.Get("i"), "", "Bidder")
            redirect()
        Else
            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_attachments set title='" & Title & "' where buyer_attachment_id=" & attachment_id)
            CommonCode.insert_system_log("Bidder financial information related attachment title updated", "RadGrid_Attachment_UpdateCommand", Request.QueryString.Get("i"), "", "Bidder")

        End If
    End Sub
    Protected Sub RadGrid_Attachment_InsertCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditFormInsertItem of the RadGrid        
        Dim insertedItem As GridEditFormInsertItem = DirectCast(e.Item, GridEditFormInsertItem)
        Dim attachment_id As Integer = 0
        Dim Title As String = (TryCast(insertedItem.FindControl("txt_attach_title"), TextBox)).Text
        Dim filename As String = ""
        Dim FileUpload As System.Web.UI.WebControls.FileUpload = TryCast(insertedItem.FindControl("fileupload_attach"), System.Web.UI.WebControls.FileUpload)

        If FileUpload.HasFile Then
            filename = FileUpload.FileName
        End If

        ' Try
        If filename <> "" Then
            attachment_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_reg_buyer_attachments(buyer_id, title, upload_date, upload_by_user_id, filename) VALUES (" & Request.QueryString.Get("i") & ", '" & CommonCode.encodeSingleQuote(Title) & "', getdate(), " & CommonCode.Fetch_Cookie_Shared("user_id") & ", '" & filename & "' ) select scope_identity()")
            UploadFileAttachment(FileUpload, Request.QueryString.Get("i"), attachment_id)
            CommonCode.insert_system_log("Bidder financial information related attachment uploaded", "RadGrid_Attachment_InsertCommand", Request.QueryString.Get("i"), "", "Bidder")
            redirect()
        End If
        ' Catch ex As Exception
        ' RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to insert attachment. Reason: " + ex.Message))
        ' e.Canceled = True
        'End Try

    End Sub
    Private Sub redirect()
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh_1", "window.parent.opener.rebind_grid();", True)
    End Sub
    Private Sub UploadFileAttachment(ByVal FileUpload As System.Web.UI.WebControls.FileUpload, ByVal buyer_id As Integer, ByVal attachment_id As Integer)
        Try
            Dim filename As String = ""
            If FileUpload.HasFile Then
                filename = FileUpload.FileName

            End If

            Dim pathToCreate As String = "../Upload/Bidders/official_attachments/" & buyer_id
            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
            End If

            pathToCreate = pathToCreate & "/" & attachment_id
            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
            End If
            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
            If infoFile.Exists Then
                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
            End If
            FileUpload.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

        Catch ex As Exception
            RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))

        End Try
    End Sub

    Protected Sub RadGrid_Attachment_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadGrid_Attachment.PreRender
        Dim menu As GridFilterMenu = RadGrid_Attachment.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub
#End Region
End Class
