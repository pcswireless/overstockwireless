﻿Imports Telerik.Web.UI
Imports System.Drawing

Partial Class Bidders_UserControls_BidderAuctions
    Inherits System.Web.UI.UserControl
    Private _auctionMode As String
    'AuctionMode w--won,i-invited,l--loosing
    Public Property AuctionMode() As String
        Get
            Return _auctionMode
        End Get
        Set(ByVal value As String)
            _auctionMode = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
    Protected Sub RadGrid_Auction_List_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid_Auction_List.NeedDataSource
        Dim dt As New DataTable
        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            If AuctionMode = "IU" Then
                lblHeading.Text = "Invited Auctions - Upcoming"
                dt = CommonCode.GetBidderInvitedAuctionsUpcoming(Request.QueryString("i"))
                RadGrid_Auction_List.Columns.FindByUniqueName("rank").Visible = False
                RadGrid_Auction_List.Columns.FindByUniqueName("max_bid_amt").Visible = False
            ElseIf AuctionMode = "IL" Then
                lblHeading.Text = "Invited Auctions - Live"
                dt = CommonCode.GetBidderInvitedAuctionsLive(Request.QueryString("i"))
                RadGrid_Auction_List.Columns.FindByUniqueName("rank").Visible = True
                RadGrid_Auction_List.Columns.FindByUniqueName("max_bid_amt").Visible = False
            ElseIf AuctionMode = "W" Then
                lblHeading.Text = "Auctions Won"
                dt = CommonCode.GetBidderWonAuctions(Request.QueryString("i"))
                RadGrid_Auction_List.Columns.FindByUniqueName("rank").Visible = False
                RadGrid_Auction_List.Columns.FindByUniqueName("max_bid_amt").Visible = True
            ElseIf AuctionMode = "L" Then
                dt = CommonCode.GetBidderLostAuctions(Request.QueryString("i"))
                lblHeading.Text = "Auctions Lost"
                RadGrid_Auction_List.Columns.FindByUniqueName("rank").Visible = False
                RadGrid_Auction_List.Columns.FindByUniqueName("max_bid_amt").Visible = True
                RadGrid_Auction_List.Columns.FindByUniqueName("max_bid_amt").HeaderText = "Bidder Last Bid / Winner Bid"
            End If
        End If
        RadGrid_Auction_List.DataSource = dt
    End Sub
    Protected Sub RadGrid_Auction_List_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadGrid_Auction_List.PreRender
        Dim menu As GridFilterMenu = RadGrid_Auction_List.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While

        'If (Not Page.IsPostBack) Then
        '    RadGrid_Auction_List.MasterTableView.FilterExpression = "([product_catetory] LIKE \'%Product Category1%\') "
        '    Dim column As GridColumn = RadGrid_Auction_List.MasterTableView.GetColumnSafe("product_catetory")
        '    column.CurrentFilterFunction = GridKnownFunction.Contains
        '    column.CurrentFilterValue = "Product Category1"
        '    RadGrid_Auction_List.MasterTableView.Rebind()
        'End If
    End Sub

End Class
