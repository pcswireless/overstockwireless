﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Bidder_Attachments.ascx.vb" Inherits="Bidders_UserControls_Bidder_Attachments" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
 <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            
            function conditionalPostback(e, sender) {
                sender.set_enableAjax(false);

            }
            </script>
     </telerik:RadScriptBlock>
<telerik:radajaxpanel id="RadAjaxPanel2_attachment" runat="server" loadingpanelid="RadAjaxLoadingPanel1"
    cssclass="TabGrid" clientevents-onrequeststart="conditionalPostback">
                                                <telerik:RadProgressManager ID="RadProgressManager1" runat="server" Skin="Vista" />
                                                <telerik:RadProgressArea ID="RadProgressArea1" runat="server" Skin="Vista" />
                                                
                                                <telerik:RadGrid ID="RadGrid_Attachment" runat="server" Skin="Vista" AllowPaging="True"
                                                    AllowSorting="True" AutoGenerateColumns="False" Width="100%" OnNeedDataSource="RadGrid_Attachment_NeedDataSource"
                                                    OnDeleteCommand="RadGrid_Attachment_DeleteCommand" OnInsertCommand="RadGrid_Attachment_InsertCommand"
                                                    OnUpdateCommand="RadGrid_Attachment_UpdateCommand" AllowFilteringByColumn="True">
                                                    <PagerStyle Mode="NextPrevAndNumeric" />
                                                    <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="buyer_attachment_id"
                                                        HorizontalAlign="NotSet" AutoGenerateColumns="False" EditMode="EditForms">
                                                        <CommandItemStyle BackColor="#e1dddd" />
                                                        <NoRecordsTemplate>
                                                            Attachments not available</NoRecordsTemplate>
                                                        <SortExpressions>
                                                            <telerik:GridSortExpression FieldName="title" SortOrder="Ascending" />
                                                        </SortExpressions>
                                                        <CommandItemSettings AddNewRecordText="Add New Attachment" ShowRefreshButton="false" />
                                                        <Columns>
                                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                EditImageUrl="/Images/edit_grid.gif">
                                                                <ItemStyle HorizontalAlign="center" CssClass="MyImageButton" Width="30" />
                                                            </telerik:GridEditCommandColumn>
                                                            <telerik:GridBoundColumn DataField="buyer_attachment_id" HeaderText="attachment_id"
                                                                UniqueName="buyer_attachment_id" ReadOnly="True" Visible="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="title" HeaderText="Title" UniqueName="Title"
                                                                HeaderStyle-Width="180" ItemStyle-Width="150" FilterControlWidth="150">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridTemplateColumn HeaderText="File Name" SortExpression="filename" UniqueName="filename"
                                                                EditFormColumnIndex="1" ItemStyle-Width="150" GroupByExpression="filename [File Name] Group By filename">
                                                                <ItemTemplate>
                                                                    <a href="/Upload/Bidders/official_attachments/<%= Request.QueryString("i") %>/<%#Eval("buyer_attachment_id") %>/<%#Eval("filename") %>"
                                                                        target="_blank">
                                                                        <%#Eval("filename") %></a>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"
                                                                ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                                                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                                            </telerik:GridButtonColumn>
                                                        </Columns>
                                                        <EditFormSettings EditFormType="Template" EditColumn-ItemStyle-Width="100%" EditColumn-HeaderStyle-HorizontalAlign="Left"
                                                            EditColumn-ItemStyle-HorizontalAlign="Left">
                                                            <FormTemplate>
                                                                <table cellpadding="0" cellspacing="2" border="0" style="padding-top: 10px; text-align: left;"
                                                                    width="100%">
                                                                    <tr>
                                                                        <td style="font-weight: bold;" colspan="4">
                                                                            Attachment Detail
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="caption" style="width: 110px;">
                                                                            Title
                                                                        </td>
                                                                        <td class="details" style="width: 270px;">
                                                                            <asp:TextBox ID="txt_attach_title" runat="server" Text='<%# Bind("title") %>' CssClass="inputtype"
                                                                                ValidationGroup="val_attach"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfv_attach_title" runat="server" ErrorMessage="<br>Title Required"
                                                                                CssClass="error" ControlToValidate="txt_attach_title" Display="Dynamic" ValidationGroup="val_attach"></asp:RequiredFieldValidator>
                                                                        </td>
                                                                        <td style="width: 145px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td style="width: 270px;">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="caption" style="width: 110px;">
                                                                            Select File
                                                                        </td>
                                                                        <td class="details" style="width: 270px;">
                                                                            <asp:FileUpload ID="fileupload_attach" runat="server" CssClass="TextBox" />
                                                                            <asp:RequiredFieldValidator ID="rfv_attach_file" runat="server" ErrorMessage="<br>File Required"
                                                                                CssClass="error" ControlToValidate="fileupload_attach" Display="Dynamic" ValidationGroup="val_attach"></asp:RequiredFieldValidator>
                                                                        </td>
                                                                        <td style="width: 145px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td style="width: 270px;">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4" align="right">
                                                                            <asp:ImageButton ID="img_btn_save_attach" runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif", "/images/update.gif") %>'
                                                                                CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "PerformInsert", "Update")%>'
                                                                                ValidationGroup="val_attach" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <asp:ImageButton ID="img_btn_cancel" runat="server" ImageUrl="/images/cancel.gif"
                                                                                CausesValidation="false" CommandName="cancel" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </FormTemplate>
                                                        </EditFormSettings>
                                                    </MasterTableView>
                                                    <ClientSettings>
                                                        <Selecting AllowRowSelect="True"></Selecting>
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </telerik:radajaxpanel>
