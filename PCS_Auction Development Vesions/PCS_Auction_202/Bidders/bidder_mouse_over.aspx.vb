﻿
Partial Class Bidders_bidder_mouse_over
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.QueryString.Get("i") <> Nothing AndAlso Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDatatable("SELECT A.buyer_id,(A.contact_first_name+' '+A.contact_last_name) as name,isnull(max_amt_bid,0) as max_amt_bid,isnull(A.company_name,'') as company,isnull(A.mobile,'') as phone1,isnull(A.phone,'') as phone2,isnull(A.fax,'') as fax, isnull(A.email,'') as email,isnull(A.city,'') as city,isnull(A.address1,'') as address1,isnull(A.address2,'') as address2, isnull(A.contact_title,'') as title,isnull(A.state_text,'') as state,isnull(A.is_phone1_mobile,0) as is_phone1_mobile,isnull(A.zip,'') as zip FROM tbl_reg_buyers A WITH (NOLOCK) where A.buyer_id=" & Request.QueryString.Get("i") & "")
            If dt.Rows.Count > 0 Then
                lbl_bidder.Text = "<b>" & dt.Rows(0)("name") & "</b>" & IIf(dt.Rows(0)("title") <> "", "<br>" & dt.Rows(0)("title"), "") & "<br>" & dt.Rows(0)("company") & "<br>" & dt.Rows(0)("address1") & IIf(dt.Rows(0)("address2") <> "", "<br>" & dt.Rows(0)("address2"), "") & "<br>" & dt.Rows(0)("city") & ", " & dt.Rows(0)("state") & " - " & dt.Rows(0)("zip") & "<br><br>(P) " & dt.Rows(0)("phone1") & IIf(dt.Rows(0)("phone2") <> "", ", " & dt.Rows(0)("phone2"), "") & IIf(dt.Rows(0)("fax") <> "", "<br>(F) " & dt.Rows(0)("fax"), "") & "<br>(E) " & dt.Rows(0)("email") & "<br>"
                lbl_bidder.Text = lbl_bidder.Text & "<font color='#AC7117' size='2'><b>Max Bid: " & IIf(dt.Rows(0)("max_amt_bid") = 0, "Unlimited", FormatCurrency(dt.Rows(0)("max_amt_bid"), 2)) & "</b></font>"
            Else
                lbl_bidder.Text = "No record for this buyer."
            End If
        Else
            lbl_bidder.Text = "No record for this buyer."
        End If
        'dv_load.Visible = False

    End Sub
End Class
