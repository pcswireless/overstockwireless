﻿Imports Telerik.Web.UI
Imports System.Drawing
Partial Class Bidders_BidderApproval
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            SetPermission()
        End If
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        SetPermission()
    End Sub
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        RadGrid2.Rebind()
    End Sub
    Private Sub SetPermission()
        If CommonCode.is_super_admin() Then
            RadGrid2.Columns.FindByUniqueName("status").Visible = True
            RadGrid2.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
            RadGrid2.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
            RadGrid2.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
        Else
            RadGrid2.Columns.FindByUniqueName("status").Visible = False

            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))
            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 7
                        RadGrid2.Columns.FindByUniqueName("status").Visible = True
                    Case 35
                        RadGrid2.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
                        RadGrid2.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
                        RadGrid2.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
                End Select

            Next
            dt = Nothing

        End If

    End Sub

    Private Sub update_buyer_approval_status(ByVal status_id As Integer, ByVal buyer_id As Integer)
        Dim status As String = SqlHelper.ExecuteScalar("select status from tbl_reg_buyser_statuses where status_id=" & status_id)
        Dim objEmail As New Email()
        If status_id = 2 Then
            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set is_active=1 where is_admin=1 and buyer_id=" & buyer_id)
            objEmail.send_buyer_approval_email(buyer_id)
        Else
            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set is_active=0 where is_admin=0 and buyer_id=" & buyer_id)
            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set is_active=0 where is_admin=1 and buyer_id=" & buyer_id)
            If status_id = 5 Then
                objEmail.send_buyer_status_changed_email(buyer_id, status)
            End If
        End If
        objEmail = Nothing
        Dim strQuery As String = "UPDATE tbl_reg_buyers SET status_id=" & status_id & ", approval_status ='" & status & "',approval_status_date =getdate(),approval_status_by_user_id =" & CommonCode.Fetch_Cookie_Shared("user_id") & " WHERE buyer_id =" & buyer_id

        SqlHelper.ExecuteNonQuery(strQuery)

    End Sub

    Protected Sub RadGrid2_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid2.ItemDataBound
        If e.Item.ItemType = GridItemType.AlternatingItem Or e.Item.ItemType = GridItemType.Item Then
            Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)

            CType(e.Item.FindControl("btnAttach"), Button).Attributes.Add("onclick", "javascript:return open_bidder_attachment(" & drv("buyer_id") & ");")
            CType(e.Item.DataItem, DataRowView).Item("status").ToString()
            Dim dropdown1 As New DropDownList
            dropdown1 = CType(e.Item.FindControl("ddl_action"), DropDownList)
            If (TypeOf e.Item Is GridDataItem) Then
                Dim dataItem As GridDataItem = CType(e.Item, GridDataItem)
            End If
        End If
    End Sub


    Protected Sub RadGrid2_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid2.NeedDataSource
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            Dim strQuery As String = ""
            If CommonCode.is_admin_user() Then
                SqlDataSource2.SelectCommand = "select status_id,status from [tbl_reg_buyser_statuses] where status_id in (1,3,4,7)"

                strQuery = "select B.buyer_id,case when ISNULL(B.max_amt_bid,0)=9999999 then 0 else ISNULL(B.max_amt_bid,0) end As max_amt_bid, isnull(B.status_id,0) as status_id,(B.contact_first_name +' ' + B.contact_last_name) as name,B.company_name as company_name,B.mobile as phone1,isnull(S.status,'') as status,B.status_id,ISNULL(B.submit_date,'1/1/1900') As submit_date,ISNULL(MM.user_id,0) As sales_rep_id, " & _
               "isnull((SELECT count(buyer_attachment_id) from tbl_reg_buyer_attachments where buyer_id=B.buyer_id),0) as attachments from tbl_reg_buyers B left join tbl_reg_buyser_statuses S on B.status_id=S.status_id Left JOIN tbl_reg_sale_rep_buyer_mapping MM ON B.buyer_id=MM.buyer_id where B.status_id in (1,3,4,7) "
            Else
                SqlDataSource_Combo.SelectCommand = "select * from tbl_reg_buyser_statuses where status_id<>2  order by sno"
                strQuery = "select B.buyer_id,case when ISNULL(B.max_amt_bid,0)=9999999 then 0 else ISNULL(B.max_amt_bid,0) end As max_amt_bid,isnull(B.status_id,0) as status_id,(B.contact_first_name +' ' + B.contact_last_name) as name,B.company_name as company_name,B.mobile as phone1,isnull(ST.status,'') as status,B.status_id,ISNULL(B.submit_date,'1/1/1900') As submit_date,ISNULL(MM.user_id,0) As sales_rep_id, " & _
               "isnull((SELECT count(buyer_attachment_id) from tbl_reg_buyer_attachments where buyer_id=B.buyer_id),0) as attachments from tbl_reg_buyers B left join tbl_reg_buyser_statuses ST on B.status_id=ST.status_id Left JOIN tbl_reg_sale_rep_buyer_mapping MM ON B.buyer_id=MM.buyer_id  INNER JOIN tbl_reg_buyer_seller_mapping M ON B.buyer_id=M.buyer_id INNER JOIN tbl_reg_sellers S ON M.seller_id=S.seller_id INNER JOIN tbl_reg_seller_user_mapping UM ON S.seller_id=UM.seller_id " & _
               " where B.status_id in (1,3,4) and UM.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
            End If

            Me.RadGrid2.DataSource = SqlHelper.ExecuteDataset(strQuery)
            If CommonCode.is_bidder_approval_agent() Then
                RadGrid2.Columns(5).Visible = False
                RadGrid2.Columns(6).Visible = False
            End If
            If CommonCode.is_admin_user() Then
                If (Not Page.IsPostBack) Then
                    Dim column As GridColumn = RadGrid2.MasterTableView.GetColumnSafe("status")
                    RadGrid2.MasterTableView.FilterExpression = "([Status] like '%%') "
                    column.CurrentFilterFunction = GridKnownFunction.Contains
                    column.CurrentFilterValue = ""

                End If
            End If

        End If
    End Sub

    Protected Sub RadGrid2_ItemCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrid2.ItemCommand
        Dim flag As Boolean = False
        'For i = 0 To RadGrid2.Items.Count - 1
        If e.CommandName = "SA_update" Then
            Dim lbl_error As Label = CType(e.Item.FindControl("lbl_error"), Label)
            lbl_error.Text = ""
            Dim hd_status As HiddenField = CType(e.Item.FindControl("hid_grd_status_id"), HiddenField)
            Dim hd_buyer_id As HiddenField = CType(e.Item.FindControl("hd_buyer_id"), HiddenField)
            Dim hd_attachment_count As HiddenField = CType(e.Item.FindControl("hd_attachment_count"), HiddenField)
            Dim ddl_action As DropDownList = CType(e.Item.FindControl("ddl_action"), DropDownList)
            Dim dd_status As String = ddl_action.SelectedValue
            Dim dd_sales_rep As Integer = SqlHelper.of_FetchKey("default_sales_rep_id")
            Dim txt_comment As String = CType(e.Item.FindControl("txt_comment"), TextBox).Text


            Dim txt_amt As String = ""
            If Not CType(e.Item.FindControl("txt_max_amt"), RadNumericTextBox).Value Is Nothing Then
                txt_amt = CType(e.Item.FindControl("txt_max_amt"), RadNumericTextBox).Value()
            End If
            If txt_amt = "" Then
                txt_amt = 0
            End If
            'Dim txt_amt As Integer = CType(IIf(txt_a = "0", 0, txt_a), Integer)
            Dim strLimit As String = "update tbl_reg_buyers set max_amt_bid=" & txt_amt & " where buyer_id=" & hd_buyer_id.Value & ";update tbl_reg_buyer_users set bidding_limit=" & txt_amt & " where is_admin=1 and buyer_id=" & hd_buyer_id.Value & ""
            Dim strRepMap As String = "IF not exists(select * from tbl_reg_sale_rep_buyer_mapping where buyer_id =" & hd_buyer_id.Value & ") Begin INSERT INTO tbl_reg_sale_rep_buyer_mapping(buyer_id, user_id) VALUES (" & hd_buyer_id.Value & ", " & dd_sales_rep & " ) end else begin Update tbl_reg_sale_rep_buyer_mapping set user_id=" & dd_sales_rep & " where buyer_id=" & hd_buyer_id.Value & " End"
            Dim strComment As String = "insert INTO tbl_reg_buyer_comments(buyer_id,comment,submit_date,submit_by_user_id) Values (" & hd_buyer_id.Value & ",replace(replace('" & txt_comment & "',CHAR(13)+CHAR(10),'<br/>'),CHAR(10),'<br/>'),GETDATE()," & CommonCode.Fetch_Cookie_Shared("user_id") & ")"


            If hd_status.Value.ToString <> dd_status Then

                flag = True
                SqlHelper.ExecuteNonQuery(strLimit)
                SqlHelper.ExecuteNonQuery(strRepMap)
                update_buyer_approval_status(dd_status, hd_buyer_id.Value)
                If txt_comment.Trim() <> "" Then
                    SqlHelper.ExecuteNonQuery(strComment)
                End If

            End If
        End If

        'Next
        If flag = True Then RadGrid2.Rebind()
    End Sub

    Protected Sub RadGrid2_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadGrid2.PreRender
        Dim menu As GridFilterMenu = RadGrid2.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub
End Class
