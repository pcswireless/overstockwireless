﻿Imports Telerik.Web.UI
Imports System.Drawing
Imports System.Data
Imports System.Data.SqlClient
Partial Class Bidders_AddEditBuyer
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString.Get("e") = "1" Then
                lblMessage.Text = "New bidder created successfully."
            End If

            Dim ajx_setng As New AjaxUpdatedControl
            panelSet()
            fill_countries()
            fill_sale_rep()
            fill_how_find_us()
            fill_companies()
            fill_buyer_status()
            fill_titles()
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                'Me.UC_System_log_link.Unique_ID = Request.QueryString("i")
                'Me.UC_System_log_link.Module_Name = "Bidder"

                Dim can_delete As Integer = SqlHelper.ExecuteScalar("select dbo.can_delete_buyer(" & Request.QueryString("i") & ")")
                If can_delete = 1 Then
                    imgbtn_basic_delete.attributes.add("onClick", "return confirm('Are you sure to delete this bidder?')")
                Else
                    imgbtn_basic_delete.attributes.add("onClick", "return confirm('Bidder already has some activity.Are you sure to delete this bidder?');")
                End If

                pnl_bidder_status.Visible = True
                setPermission()
                set_form_mode(False)
                set_button_visibility("View")
                BindBuyerBasicInfo()
                bind_official_detail()
                set_form_mode_official(False)
                set_button_visibility_official("View")

            Else

                pnl_bidder_status.Visible = False
                imgbtn_basic_delete.Visible = False
                lblPageHeading.Text = "New Bidder"
                set_form_mode(True)
                set_button_visibility("Add")

                trOfficialInfo.Visible = False
                trSubLogin.Visible = False
                trInvite.Visible = False
                RadTabStrip1.Tabs(1).Visible = False

            End If
            If CommonCode.is_bidder_approval_agent() Then
                'div_bidder_hide.visible = False
                RadTabStrip1.Tabs(1).Visible = False
                RadTabStrip1.Tabs(2).Visible = False
                RadTabStrip1.Tabs(3).Visible = False
            End If


            If Not CommonCode.is_admin_user Then
                tr_tc_caption.Visible = False
                tr_tc.Visible = False

            Else
                tr_tc_caption.Visible = True
                tr_tc.Visible = True
            End If
        End If


        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            Me.UC_System_log_link.Unique_ID = Request.QueryString("i")
            Me.UC_System_log_link.Module_Name = "Bidder"
        End If

    End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        setPermission()
        If Not IsPostBack Then

            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then

                'RadAjaxManager_Main.AjaxSettings.AddAjaxSetting(img_btnsend.UniqueID, RadAjaxPanel1.UniqueID, Nothing)
                'RadAjaxManager_Main.AjaxSettings.AddAjaxSetting(img_btnsend.UniqueID, RadAjaxPanel11.UniqueID, Nothing)
                'RadAjaxManager_Main.AjaxSettings.AddAjaxSetting(img_btnsend.UniqueID, RadAjaxPanel2.UniqueID, Nothing)
                'RadAjaxManager_Main.AjaxSettings.AddAjaxSetting(img_btnsend.UniqueID, RadAjaxPanel22.UniqueID, Nothing)
                'RadAjaxManager_Main.AjaxSettings.AddAjaxSetting(img_btnsend.UniqueID, Rad_Ajax_panel_status.UniqueID, Nothing)
                'RadAjaxManager_Main.AjaxSettings.AddAjaxSetting(img_btnsend.UniqueID, RadAjaxPanel5.UniqueID, Nothing)
            Else

            End If
        End If
        If rad_grid_invite_auctions.Items.count > 0 Then
            btn_invite_all.visible = True
        Else
            btn_invite_all.visible = False
        End If
    End Sub
    Private Sub setPermission()
        If Not CommonCode.is_super_admin() Then

            Dim Edit_Bidder As Boolean = False
            Dim New_Bidder As Boolean = False
            Dim Show_Bidder As Boolean = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDataTable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 6
                        New_Bidder = True
                    Case 7
                        Edit_Bidder = True
                    Case 8
                        Show_Bidder = True
                End Select
            Next
            dt = Nothing
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                If Show_Bidder = False Then Response.Redirect("/NoPermission.aspx")
                If Edit_Bidder = False Then
                    ddl_status.Enabled = False
                    'Set basic info tab
                    Panel1_Content1button.Visible = False
                    'Set official info tab
                    Panel2_Content1button.Visible = False
                    RadGrid_Attachment.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None
                    RadGrid_Attachment.Columns.FindByUniqueName("EditCommandColumn").Visible = False
                    RadGrid_Attachment.Columns.FindByUniqueName("DeleteColumn").Visible = False
                    'Set comment detail tab
                    'btn_submit_comment.Visible = False
                    'Set sub-login tab
                    rad_grid_sublogins.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None
                    rad_grid_sublogins.Columns.FindByUniqueName("EditCommandColumn").Visible = False
                    rad_grid_sublogins.Columns.FindByUniqueName("DeleteColumn").Visible = False
                    'Set invitation tab
                    btn_invite_all.Visible = False
                    rad_grid_invite_auctions.Columns.FindByUniqueName("CheckBoxTemplateColumn").Visible = False

                End If

            Else
                If New_Bidder = False Then Response.Redirect("/NoPermission.aspx")
            End If

        End If
    End Sub
    Private Sub panelSet()
        If Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
            RadTabStrip1.Tabs(0).Text = "Edit Bidder"
            RadTabStrip1.Tabs(0).CssClass = "auctiontab"
            RadTabStrip1.Tabs(1).CssClass = "auctiontab"
            RadTabStrip1.Tabs(2).CssClass = "auctiontab"
            RadTabStrip1.Tabs(3).CssClass = "auctiontab"
            RadTabStrip1.Tabs(0).SelectedCssClass = "auctiontabSelected"
            RadTabStrip1.Tabs(1).SelectedCssClass = "auctiontabSelected"
            RadTabStrip1.Tabs(2).SelectedCssClass = "auctiontabSelected"
            RadTabStrip1.Tabs(3).SelectedCssClass = "auctiontabSelected"
            Select Case Request.QueryString.Get("t")
                Case 1
                    cpe1.Collapsed = False
                    cpe11.Collapsed = False
                    pnl_img1.ImageUrl = "/Images/up_Arrow.gif"
                    RadTabStrip1.SelectedIndex = 0
                    RadMultiPage1.SelectedIndex = 0
                Case 2
                    cpe2.Collapsed = False
                    cpe21.Collapsed = False
                    pnl_img2.ImageUrl = "/Images/up_Arrow.gif"
                    RadTabStrip1.SelectedIndex = 0
                    RadMultiPage1.SelectedIndex = 0
                Case 3
                    'cpe5.Collapsed = False
                    pnl_img3.ImageUrl = "/Images/up_Arrow.gif"
                    RadTabStrip1.SelectedIndex = 0
                    RadMultiPage1.SelectedIndex = 0
                Case 4
                    cpe3.Collapsed = False
                    'pnl_img5.ImageUrl = "/Images/up_Arrow.gif"
                    RadTabStrip1.SelectedIndex = 0
                    RadMultiPage1.SelectedIndex = 0
                Case 5
                    RadTabStrip1.SelectedIndex = 1
                    RadMultiPage1.SelectedIndex = 1
                Case 6
                    RadTabStrip1.SelectedIndex = 2
                    RadMultiPage1.SelectedIndex = 2
                Case 7
                    RadTabStrip1.SelectedIndex = 3
                    RadMultiPage1.SelectedIndex = 3
                Case Else
                    cpe1.Collapsed = False
                    cpe11.Collapsed = False
                    pnl_img1.ImageUrl = "/Images/up_Arrow.gif"
                    RadTabStrip1.SelectedIndex = 0
                    RadMultiPage1.SelectedIndex = 0
            End Select
        Else
            RadTabStrip1.Tabs(1).Visible = False
            RadTabStrip1.Tabs(2).Visible = False
            RadTabStrip1.Tabs(3).Visible = False
            RadTabStrip1.Tabs(0).Text = "New Bidder"
            cpe1.Collapsed = False
            cpe11.Collapsed = False
            pnl_img1.ImageUrl = "/Images/up_Arrow.gif"
        End If
    End Sub
    Private Sub fill_titles()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select isnull(name,'') as name,title_id from tbl_master_titles order by name"
        ds = SqlHelper.ExecuteDataset(strQuery)
        ddl_title.DataSource = ds
        ddl_title.DataTextField = "name"
        ddl_title.DataValueField = "name"
        ddl_title.DataBind()
        ddl_title.Items.Insert(0, New ListItem("--Select--", ""))
        ds.Dispose()
    End Sub
    Private Sub fill_sale_rep()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "(select '--select--' as name,0 as user_id) union (select isnull(U.first_name + ' ' + U.last_name,'') as name,U.user_id from tbl_sec_users U inner join tbl_sec_user_profile_mapping M on U.user_id=M.user_id where M.profile_id=2 and U.is_active=1) order by name"
        ds = SqlHelper.ExecuteDataset(strQuery)
        ddl_sale_rep.DataSource = ds
        ddl_sale_rep.DataBind()

        ddl_sales.DataSource = ds
        ddl_sales.DataBind()

        ds.Dispose()
    End Sub
    Private Sub fill_buyer_status()
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDataTable("select status_id,status from [tbl_reg_buyser_statuses] order by sno")
        ddl_buyer_status.DataSource = dt
        ddl_buyer_status.DataBind()

        ddl_status.DataSource = dt
        ddl_status.DataBind()


        dt = Nothing
    End Sub
    Sub check_resale_validate(ByVal source As Object, ByVal e As ServerValidateEventArgs)

        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            If lbl_file.text = "" Then
                e.IsValid = True
            Else
                If Not fileupload1.hasfile Then
                    e.IsValid = False
                Else
                    e.IsValid = True
                End If
            End If
        Else
            If Not fileupload1.hasfile Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If
        End If
    End Sub
    Protected Sub ddl_status_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_status.SelectedIndexChanged
        hid_status_click.value = "top"
        If ddl_status.SelectedItem.Value = 2 Then
            'in case of admin approved
            hid_filename.Value = ""
            update_to_admin_approval()

        ElseIf ddl_status.SelectedItem.Value = 7 Then
            'in case of semi approved
            hid_filename.Value = ""
            update_to_semi_approval()
        Else
            update_buyer_approval_status(ddl_status.SelectedItem.Value)
            CommonCode.insert_system_log("Bidder status updated to " & ddl_status.SelectedItem.Text, "ddl_status_SelectedIndexChanged", Request.QueryString.Get("i"), "", "Bidder")
            Response.Redirect("/Bidders/AddEditBuyer.aspx?i=" & Request.QueryString.Get("i"))
        End If
    End Sub

    
    Protected Sub update_to_admin_approval()
        If ddl_status.SelectedItem.Value = 2 Then
            top_dropdown_change_status()
            
        End If
    End Sub

    'Function for Top Dropdown to Change the Status with also Update the similar DropDownList and Label Control
    Protected Sub top_dropdown_change_status()
        update_buyer_approval_status(ddl_status.selectedvalue)
        ddl_buyer_status.selectedvalue = ddl_status.selectedvalue
        lbl_status.Text = ddl_status.selecteditem.Text
        lbl_approval_status.text = ddl_status.selecteditem.Text
    End Sub

    Protected Sub update_to_semi_approval()
        top_dropdown_change_status()
        
    End Sub
    'this is code for confirm message to update in case of semi approval/admin approval
    Protected Sub img_btnsend_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_btnsend.Click

        If chk_term.Checked = False Then
            modalPopUpExtender1.Show()
            lbl_error.Visible = True
            lbl_error.Text = "You must select this box to proceed."
            ViewState("is_check") = 0
        Else
            ViewState("is_check") = 1
            modalPopUpExtender1.Hide()
            If hid_status_click.value = "top" Then
                update_buyer_approval_status(ddl_status.selectedvalue)
                ddl_buyer_status.selectedvalue = ddl_status.selectedvalue
                lbl_status.Text = ddl_status.selecteditem.Text
                lbl_approval_status.text = ddl_status.selecteditem.Text
            Else
                If hid_filename.Value <> "" Then
                    basicUpdate(hid_filename.Value)
                Else
                    basicUpdate("")
                End If
                
            End If
        End If
    End Sub

    Protected Sub img_btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_btnCancel.Click
        ViewState("is_check") = 0
        BindBuyerBasicInfo()
    End Sub

    Private Sub update_buyer_approval_status(ByVal status_id As Integer)

        Dim status As String = SqlHelper.ExecuteScalar("select status from tbl_reg_buyser_statuses where status_id=" & status_id)

        Dim strQuery As String = "UPDATE tbl_reg_buyers SET status_id=" & status_id & ", approval_status ='" & status & "',approval_status_date =getdate(),approval_status_by_user_id =" & CommonCode.Fetch_Cookie_Shared("user_id") & " WHERE buyer_id =" & Request.QueryString.Get("i")
        hid_status_id.value = status_id
        SqlHelper.ExecuteNonQuery(strQuery)
        Dim objEmail As New Email()
        If status_id = 2 Then
            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set is_active=1 where is_admin=1 and buyer_id=" & Request.QueryString.Get("i"))
            objEmail.send_buyer_approval_email(Request.QueryString.Get("i"))
        ElseIf status_id = 7 Then
            'SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set is_active=0 where is_admin=0 and buyer_id=" & Request.QueryString.Get("i"))
            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set is_active=0 where buyer_id=" & Request.QueryString.Get("i"))
            objEmail.Bidderd_Admin_Active(Request.QueryString.Get("i"))
        Else
            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set is_active=0 where buyer_id=" & Request.QueryString.Get("i"))
            ' SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set is_active=0 where is_admin=0 and buyer_id=" & Request.QueryString.Get("i"))
            'SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set is_active=0 where is_admin=1 and buyer_id=" & Request.QueryString.Get("i"))
            If status_id = 5 Then
                objEmail.send_buyer_status_changed_email(Request.QueryString.Get("i"), status)
            End If
        End If
        objEmail = Nothing
    End Sub

#Region "Basic INFO"

    Private Sub BindBuyerBasicInfo()
        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            Dim buyer_id As Integer = Request.QueryString.Get("i")
            Dim dt As DataTable = New DataTable()
            Dim strQuery As String = ""

            strQuery = "SELECT A.buyer_id, " & _
                          "ISNULL(A.company_name, '') AS company_name, " & _
                          "ISNULL(A.contact_title, '') AS contact_title, " & _
                          "ISNULL(A.contact_first_name, '') AS contact_first_name, " & _
                          "ISNULL(A.contact_last_name, '') AS contact_last_name, " & _
                          "ISNULL(A.email, '') AS email, " & _
                          "ISNULL(A.website, '') AS website, " & _
                          "ISNULL(A.phone, '') AS phone, " & _
                            "ISNULL(A.phone_ext, '') AS phone_ext, " & _
                          "ISNULL(A.mobile, '') AS mobile, " & _
                          "ISNULL(A.fax, '') AS fax," & _
                          "ISNULL(A.address1, '') AS address1, " & _
                          "ISNULL(A.address2, '') AS address2, " & _
                          "ISNULL(A.city, '') AS city, " & _
                          "ISNULL(A.state_id, 0) AS state_id," & _
                          "ISNULL(A.status_id, 0) AS status_id," & _
                          "ISNULL(S.status, '') AS status," & _
                          "ISNULL(A.state_text,'') As state_text, " & _
                          "ISNULL(A.zip, '') AS zip, " & _
                          "ISNULL(A.country_id, 0) AS country_id, " & _
                          "ISNULL(C.name, '') AS country, " & _
                          "ISNULL(A.comment, '') AS comment, " & _
                          "ISNULL(W.tc_accepted_date, '') AS tc_accepted_date, " & _
                          "ISNULL(W.tc_accept_ip, '') AS tc_accept_ip, " & _
                          "ISNULL(A.is_phone1_mobile, 0) AS is_phone1_mobile, " & _
                          "ISNULL(A.is_phone2_mobile, 0) AS is_phone2_mobile, " & _
                          "ISNULL(U.first_name + ' ' + U.last_name, '') AS user_name, " & _
                          "ISNULL(U.user_id, 0) AS user_id," & _
                          "ISNULL(A.resale_certificate, '') AS resale_certificate," & _
                          "ISNULL(A.microtelecom_account_no, '') AS microtelecom_account_no," & _
                          "ISNULL(A.default_shipping_carrier, '') AS default_shipping_carrier," & _
                          "ISNULL(A.default_shipping_account, '') AS default_shipping_account," & _
                          "(select count(unsubscription_id) from tbl_unsubscription where email=A.email) as is_unsubscribed_invitation " & _
                          "FROM tbl_reg_buyers A WITH (NOLOCK) inner join tbl_reg_buyser_statuses S on A.status_id=S.status_id inner join tbl_reg_buyer_users W on A.buyer_id=W.buyer_id  Left JOIN tbl_master_countries C ON A.country_id=C.country_id left join tbl_reg_sale_rep_buyer_mapping R on A.buyer_id=R.buyer_id left join tbl_sec_users U on U.user_id=R.user_id  WHERE A.buyer_id =" & buyer_id.ToString() & " and w.is_admin=1"

            'Response.Write(strQuery)
            txt_test.Text = strQuery
            dt = SqlHelper.ExecuteDatatable(strQuery)

            If dt.Rows.Count > 0 Then
                If Not ddl_status.Items.FindByValue(dt.Rows(0)("status_id")) Is Nothing Then
                    ddl_status.ClearSelection()
                    ddl_status.Items.FindByValue(dt.Rows(0)("status_id")).Selected = True
                End If
                If Not CommonCode.is_admin_user Then
                    If Not ddl_status.Items.FindByValue(dt.Rows(0)("status_id")) Is Nothing Then
                        ddl_status.ClearSelection()
                        ddl_status.Items.FindByValue(dt.Rows(0)("status_id")).Selected = True
                        If CommonCode.is_bidder_approval_agent Then
                            If dt.Rows(0)("status_id") <> 2 Then
                                ddl_status.Items.Remove(ddl_status.Items.FindByValue("2"))
                                ddl_buyer_status.Items.Remove(ddl_buyer_status.Items.FindByValue("2"))
                            End If
                        End If


                    End If
                End If
                lblPageHeading.Text = dt.Rows(0)("company_name")
                txt_company.Text = dt.Rows(0)("company_name")
                lbl_company.Text = dt.Rows(0)("company_name")
                If Not ddl_title.Items.FindByText(dt.Rows(0)("contact_title")) Is Nothing Then
                    ddl_title.ClearSelection()
                    ddl_title.Items.FindByText(dt.Rows(0)("contact_title")).Selected = True
                End If
                lbl_title.Text = dt.Rows(0)("contact_title")

                ddl_sale_rep.ClearSelection()
                If Not ddl_sale_rep.Items.FindByValue(dt.Rows(0)("user_id")) Is Nothing Then
                    ddl_sale_rep.Items.FindByValue(dt.Rows(0)("user_id")).Selected = True
                End If

                ddl_sales.clearselection()
                If Not ddl_sales.Items.FindByValue(dt.Rows(0)("user_id")) Is Nothing Then
                    ddl_sales.Items.FindByValue(dt.Rows(0)("user_id")).Selected = True
                End If

                lbl_sale_rep.Text = dt.Rows(0)("user_name")
                lbl_sales.Text = dt.Rows(0)("user_name")

                If dt.Rows(0)("tc_accepted_date") = "1/1/1900" Then
                    lbl_tc_date.Text = ""
                Else
                    lbl_tc_date.Text = dt.Rows(0)("tc_accepted_date")

                End If
                lbl_tc_ip.Text = dt.Rows(0)("tc_accept_ip")
                txt_comment.Text = dt.Rows(0)("comment").ToString.Replace("<br/>", Char.ConvertFromUtf32(13)).Replace("<br/>", Char.ConvertFromUtf32(10))
                lbl_comment.Text = dt.Rows(0)("comment").ToString
                txt_first_name.Text = dt.Rows(0)("contact_first_name")
                lbl_first_name.Text = dt.Rows(0)("contact_first_name")
                txt_last_name.Text = dt.Rows(0)("contact_last_name")
                lbl_last_name.Text = dt.Rows(0)("contact_last_name")
                txt_email.Text = dt.Rows(0)("email")
                'txt_confirm_email.Text = dt.Rows(0)("email")
                lbl_email.Text = "<a href='mailto:" & dt.Rows(0)("email") & "'>" & dt.Rows(0)("email") & "</a>"
                ' lbl_confirm_email.Text = dt.Rows(0)("email")
                'If dt.Rows(0)("is_phone1_mobile").ToString = "1" Then
                rdo_ph_1_mobile.Checked = dt.Rows(0)("is_phone1_mobile")
                'End If
                'If dt.Rows(0)("is_phone2_mobile").ToString = "1" Then
                rdo_ph_2_mobile.Checked = dt.Rows(0)("is_phone2_mobile")
                'End If
                txt_website.Text = dt.Rows(0)("website")
                lbl_website.Text = dt.Rows(0)("website")
                txt_mobile.Text = dt.Rows(0)("mobile")
                lbl_mobile.Text = dt.Rows(0)("mobile")
                txt_phone.Text = dt.Rows(0)("phone")
                lbl_phone.Text = dt.Rows(0)("phone")
                txt_phone_ext.Text = dt.Rows(0)("phone_ext")
                lbl_phone_ext.Text = dt.Rows(0)("phone_ext")
                txt_fax.Text = dt.Rows(0)("fax")
                lbl_fax.Text = dt.Rows(0)("fax")
                txt_address1.Text = dt.Rows(0)("address1") '.Replace("<br/>", Char.ConvertFromUtf32(13)).Replace("<br/>", Char.ConvertFromUtf32(10))
                lbl_address1.Text = dt.Rows(0)("address1")
                txt_address2.Text = dt.Rows(0)("address2") '.Replace("<br/>", Char.ConvertFromUtf32(13)).Replace("<br/>", Char.ConvertFromUtf32(10))
                lbl_address2.Text = dt.Rows(0)("address2")
                txt_city.Text = dt.Rows(0)("city")
                lbl_city.Text = dt.Rows(0)("city")
                txt_ship_carrier.Text = dt.Rows(0)("default_shipping_carrier")
                lbl_ship_carrier.Text = dt.Rows(0)("default_shipping_carrier")
                txt_account_number.Text = dt.Rows(0)("default_shipping_account")
                lbl_account_number.Text = dt.Rows(0)("default_shipping_account")
                lnk_file1.Text = iif(dt.Rows(0)("resale_certificate").ToString.Trim <> "", dt.Rows(0)("resale_certificate").ToString.Trim, "")
                'If lnk_file1.Text.trim() = "" Then
                '    lnk_remove.visible = False
                'Else
                '    lnk_remove.visible = True
                'End If
                lbl_file.Text = iif(dt.Rows(0)("resale_certificate").ToString.Trim <> "", "", "Not Uploaded")
                lnk_file1.CommandArgument = dt.Rows(0)("resale_certificate").ToString.Trim
                lbl_microtelecom_no.Text = dt.Rows(0)("microtelecom_account_no")
                txt_microtelecom_no.text = dt.Rows(0)("microtelecom_account_no")
                chk_invite_auction.Checked = Convert.ToBoolean(dt.Rows(0)("is_unsubscribed_invitation"))
                If Convert.ToBoolean(dt.Rows(0)("is_unsubscribed_invitation")) Then
                    lbl_invite_auction_unsubscribe.Text = "<img src='/Images/true.gif' style='border: none;' alt='' />"
                Else
                    lbl_invite_auction_unsubscribe.Text = "<img src='/Images/false.gif' style='border: none;' alt='' />"
                End If

                hid_unsubscribe_invite.Value = dt.Rows(0)("is_unsubscribed_invitation")
                If Not ddl_buyer_status.Items.FindByValue(dt.Rows(0)("status_id")) Is Nothing Then
                    ddl_buyer_status.ClearSelection()
                    ddl_buyer_status.Items.FindByValue(dt.Rows(0)("status_id")).Selected = True
                End If

                hid_status_id.Value = dt.Rows(0)("status_id")
                lbl_status.Text = dt.Rows(0)("status")

                txt_zip.Text = dt.Rows(0)("zip")
                lbl_zip.Text = dt.Rows(0)("zip")
                If Not ddl_country.Items.FindByValue(dt.Rows(0)("country_id")) Is Nothing Then
                    ddl_country.ClearSelection()
                    ddl_country.Items.FindByValue(dt.Rows(0)("country_id")).Selected = True
                End If
                'fill_states()

                txt_other_state.Text = dt.Rows(0)("state_text")
                lbl_state.Text = dt.Rows(0)("state_text")


                lbl_country.Text = dt.Rows(0)("country")
                dt = Nothing

                dt = get_buyer_admin_detail(buyer_id)
                If dt.Rows.Count > 0 Then
                    txt_user_id.Text = dt.Rows(0)("username")
                    lbl_user_id.Text = dt.Rows(0)("username")
                    lit_change_password.Text = "<a href='javascript:void(0);' onclick=""return open_pass_win('/change_password.aspx','b=0&i=" & SqlHelper.ExecuteScalar("select buyer_user_id from tbl_reg_buyer_users where is_admin=1 and buyer_id=" & buyer_id & "") & "&o=0');"">Click to Change Password</a>"

                    lbl_password.Text = "******"

                End If


                dt = Nothing


                'lst_how_find_us.DataSource = dt
                'lst_how_find_us.DataBind()

                If Not ddl_how_find_us.Items.FindByValue(get_buyer_how_find_us(buyer_id)) Is Nothing Then
                    ddl_how_find_us.ClearSelection()
                    ddl_how_find_us.Items.FindByValue(get_buyer_how_find_us(buyer_id)).Selected = True
                End If


                ltrl_how_find_us.Text = IIf(ddl_how_find_us.SelectedValue <> "", ddl_how_find_us.SelectedItem.Text, "&nbsp;")

                
                Dim str As String = ""
                
                dt = get_buyer_companies_linked(buyer_id)
                'lst_companies_linked.DataSource = dt
                ' lst_companies_linked.DataBind()
                For i As Integer = 0 To dt.Rows.Count - 1
                    For Each lst As ListItem In chklst_companies_linked.Items
                        If dt.Rows(i)("seller_id") = lst.Value Then
                            lst.Selected = True
                        End If
                    Next
                Next
                str = ""
                str = get_buyer_companies_linked_string(buyer_id)
                ltrl_linked_companies.Text = str

                ltrl_buckets.Text = get_buyer_bucket_string(buyer_id)
                ltrl_bucket_link.text = "<a href=""javascript:void(0);"" onclick=""return open_bucket_assign('" & Security.EncryptionDecryption.EncryptValueFormatted(buyer_id) & "')"">Manage invitations</a>"


            End If
            ' dt.Dispose()

        End If
    End Sub
    Private Function get_buyer_companies_linked(ByVal buyer_id As Integer) As DataTable
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable()
        strQuery = "SELECT 	ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.seller_id, 0) AS seller_id,ISNULL(S.company_name,'') as company_name " & _
                    "FROM tbl_reg_buyer_seller_mapping A INNER JOIN tbl_reg_sellers S ON A.seller_id=S.seller_id " & _
                    "WHERE ISNULL(S.is_active,0)=1 and A.buyer_id =" & buyer_id.ToString()
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function
    Private Function get_buyer_companies_linked_string(ByVal buyer_id As Integer) As String
        Dim str As String = ""
        Dim strQuery As String = "Declare @str varchar(2000) select @str=COALESCE(@str + ', ', '') + ISNULL(S.company_name,'') FROM tbl_reg_buyer_seller_mapping A INNER JOIN tbl_reg_sellers S ON A.seller_id=S.seller_id  " & _
            "WHERE ISNULL(S.is_active,0)=1 and A.buyer_id =" & buyer_id.ToString() & " select ISNULL(@str,'')"
        str = SqlHelper.ExecuteScalar(strQuery)
        If str = "" Then
            str = "No company linked."
        End If
        Return str
    End Function
    Private Function get_buyer_buckets(ByVal buyer_id As Integer) As DataTable
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable()
        strQuery = "SELECT 	ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.bucket_id, 0) AS bucket_id,ISNULL(B.bucket_name,'') as bucket " & _
                    "FROM tbl_reg_buyer_bucket_mapping A INNER JOIN tbl_master_buckets B ON A.bucket_id=B.bucket_id " & _
                    "WHERE A.buyer_id =" & buyer_id.ToString()
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function
    'Private Function get_buyer_industry_types(ByVal buyer_id As Integer) As DataTable
    '    Dim strQuery As String = ""
    '    Dim dt As DataTable = New DataTable()
    '    strQuery = "SELECT 	ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.industry_type_id, 0) AS industry_type_id,ISNULL(I.name,'') as industry_type " & _
    '                "FROM tbl_reg_buyer_industry_type_mapping A INNER JOIN tbl_master_industry_types I ON A.industry_type_id=I.industry_type_id " & _
    '                "WHERE A.buyer_id =" & buyer_id.ToString()
    '    dt = SqlHelper.ExecuteDatatable(strQuery)
    '    Return dt
    'End Function
    'Private Function get_buyer_industry_type_string(ByVal buyer_id As Integer) As String
    '    Dim str As String = ""
    '    Dim strQuery As String = "Declare @str varchar(1000) select @str=COALESCE(@str + ', ', '') + ISNULL(I.name,'') FROM tbl_reg_buyer_industry_type_mapping A INNER JOIN tbl_master_industry_types I ON A.industry_type_id=I.industry_type_id " & _
    '        "WHERE A.buyer_id =" & buyer_id.ToString() & " select ISNULL(@str,'')"
    '    str = SqlHelper.ExecuteScalar(strQuery)
    '    If str = "" Then
    '        str = "Industry type not selected."
    '    End If
    '    Return str
    'End Function
    Private Function get_buyer_bucket_string(ByVal buyer_id As Integer) As String
        Dim str As String = ""
        Dim strQuery As String = "select B.bucket_name,V.bucket_id,V.bucket_value_id,V.bucket_value from tbl_master_buckets B inner join tbl_master_bucket_values V on B.bucket_id=V.bucket_id inner join tbl_reg_buyer_bucket_mapping M on V.bucket_value_id=M.bucket_value_id and M.buyer_id=" & buyer_id & " where B.is_active=1 and V.is_active=1 order by B.sno,V.bucket_value_id"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataSet(strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim bucket_id As Integer = 0
            str = "<table cellpadding='10' cellspacing='1' style='background-color: Gray;' width='100%'>"
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If bucket_id <> ds.Tables(0).Rows(i)("bucket_id") Then
                    bucket_id = ds.Tables(0).Rows(i)("bucket_id")
                    str = str & "<tr><td valign='top' style='background-color: White;'><b>" & ds.Tables(0).Rows(i)("bucket_name") & "</b></td><td valign='top' style='background-color: White;'>" & ds.Tables(0).Rows(i)("bucket_value") & "</td></tr>"
                Else
                    str = str & "<tr><td valign='top' style='background-color: White;'>&nbsp;</td><td valign='top' style='background-color: White;'>" & ds.Tables(0).Rows(i)("bucket_value") & "</td></tr>"
                End If

            Next
            str = str & "</table>"
        Else
            str = "Bucket not selected."
        End If
        Return str
    End Function
    'Private Function get_buyer_business_types(ByVal buyer_id As Integer) As DataTable
    '    Dim strQuery As String = ""
    '    Dim dt As DataTable = New DataTable()
    '    strQuery = "SELECT 	ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.business_type_id, 0) AS business_type_id,ISNULL(B.name,'') as business_type " & _
    '                "FROM tbl_reg_buyer_business_type_mapping A INNER JOIN tbl_master_business_types B ON A.business_type_id=B.business_type_id " & _
    '                "WHERE A.buyer_id =" & buyer_id.ToString()
    '    dt = SqlHelper.ExecuteDatatable(strQuery)
    '    Return dt
    'End Function
    'Private Function get_buyer_business_type_string(ByVal buyer_id As Integer) As String

    '    Dim str As String = ""
    '    Dim strQuery As String = "Declare @str varchar(1000) select @str=COALESCE(@str + ', ', '') + ISNULL(B.name,'') FROM tbl_reg_buyer_business_type_mapping A INNER JOIN tbl_master_business_types B ON A.business_type_id=B.business_type_id " & _
    '        "WHERE A.buyer_id =" & buyer_id.ToString() & " select ISNULL(@str,'')"
    '    str = SqlHelper.ExecuteScalar(strQuery)
    '    If str = "" Then
    '        str = "Business type not selected."
    '    End If
    '    Return str
    'End Function
    Private Function get_buyer_how_find_us(ByVal buyer_id As Integer) As String
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable()
        strQuery = "SELECT top 1 ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.how_find_us_id, 0) AS how_find_us_id,ISNULL(F.name,'') As how_find_us FROM tbl_reg_buyer_find_us_assignment A INNER JOIN tbl_master_how_find_us F ON A.how_find_us_id=F.how_find_us_id WHERE buyer_id=" & buyer_id.ToString()
        dt = SqlHelper.ExecuteDatatable(strQuery)
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0)("how_find_us_id")
        Else
            Return ""
        End If

    End Function
    Private Function get_buyer_admin_detail(ByVal buyer_id As Integer) As DataTable
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable()
        strQuery = "SELECT ISNULL(A.username, '') AS username,ISNULL(A.password, '') AS password FROM tbl_reg_buyer_users A WHERE A.buyer_id =" & buyer_id.ToString()
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function
    Private Sub set_button_visibility(ByVal mode As String)
        If mode.ToUpper() = "VIEW" Then
            imgbtn_basic_edit.Visible = True
            imgbtn_basic_save.Visible = False
            imgbtn_basic_update.Visible = False
            lbl_user_id.Visible = True
            lbl_tc_date.Visible = True
            lbl_tc_ip.Visible = True
            txt_password.Visible = False
            rfv_password.Enabled = False
            RegularExpressionValidator5.Enabled = False
            txt_retype_password.Visible = False
            trRetypePassword.Visible = False
            rfv_retype_password.Enabled = False
            comp_retype_password.Enabled = False
            'imgbtn_basic_cancel.Visible = False
            div_basic_cancel.Visible = False
            'lnk_remove.visible = False
        ElseIf mode.ToUpper() = "EDIT" Then
            imgbtn_basic_edit.Visible = False
            lbl_user_id.Visible = True
            lbl_tc_date.Visible = True
            lbl_tc_ip.Visible = True
            imgbtn_basic_save.Visible = False
            imgbtn_basic_update.Visible = True
            txt_password.Visible = False
            rfv_password.Enabled = False
            RegularExpressionValidator5.Enabled = False
            txt_retype_password.Visible = False
            rfv_retype_password.Enabled = False
            comp_retype_password.Enabled = False
            trRetypePassword.Visible = False
            'imgbtn_basic_cancel.Visible = True
            div_basic_cancel.Visible = True
            'If lnk_file1.Text.trim() = "" Then
            '    lnk_remove.visible = False
            'Else
            '    lnk_remove.visible = True
            'End If
            'lnk_remove.visible = True
        Else
            imgbtn_basic_edit.Visible = False
            imgbtn_basic_save.Visible = True
            imgbtn_basic_update.Visible = False
            txt_password.Visible = True
            rfv_password.Enabled = True
            RegularExpressionValidator5.Enabled = True
            txt_retype_password.Visible = True
            rfv_retype_password.Enabled = True
            comp_retype_password.Enabled = True
            trRetypePassword.Visible = True
            'imgbtn_basic_cancel.Visible = False
            div_basic_cancel.Visible = False
            'lnk_remove.visible = False
        End If
        'If CommonCode.is_bidder_approval_agent Then
        '    'RequiredFieldValidator1.ValidationGroup = "a"
        'Else
        '    If Not mode.ToUpper() = "VIEW" Then
        '        RequiredFieldValidator1.ValidationGroup = "val_basic_info"
        '    End If

        'End If

    End Sub
    Private Sub fill_countries()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select country_id,name,ISNULL(code,'') As code from tbl_master_countries"
        ds = SqlHelper.ExecuteDataset(strQuery)
        ddl_country.DataSource = ds
        ddl_country.DataTextField = "name"
        ddl_country.DataValueField = "country_id"
        ddl_country.DataBind()
        ddl_country.Items.Insert(0, New ListItem("--Select--", ""))
        ddl_country.ClearSelection()
        ddl_country.Items.FindByText("USA").Selected = True
        ds.Dispose()
    End Sub
    Private Sub fill_how_find_us()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select how_find_us_id,name,ISNULL(description,'') as description,ISNULL(sno,99)as sno from tbl_master_how_find_us order by sno"
        ds = SqlHelper.ExecuteDataset(strQuery)
        ddl_how_find_us.DataSource = ds
        ddl_how_find_us.DataTextField = "name"
        ddl_how_find_us.DataValueField = "how_find_us_id"
        ddl_how_find_us.DataBind()
        ddl_how_find_us.Items.Insert(0, New ListItem("--Select--", ""))
        ds.Dispose()
    End Sub

    Private Sub fill_companies()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select seller_id,company_name from tbl_reg_sellers where is_active=1 order by company_name"
        ds = SqlHelper.ExecuteDataset(strQuery)
        chklst_companies_linked.DataSource = ds
        chklst_companies_linked.DataTextField = "company_name"
        chklst_companies_linked.DataValueField = "seller_id"
        chklst_companies_linked.DataBind()
        ds.Dispose()

        If String.IsNullOrEmpty(Request.QueryString("i")) Then
            For Each itm As ListItem In chklst_companies_linked.Items
                If itm.Value = 1 Then
                    itm.Selected = True
                End If
            Next
        End If
    End Sub
    Private Sub set_form_mode(ByVal flag As Boolean)
        txt_company.Visible = flag
        ddl_title.Visible = flag
        txt_first_name.Visible = flag
        txt_last_name.Visible = flag
        txt_email.Visible = flag
        'txt_confirm_email.Visible = flag
        txt_website.Visible = flag
        txt_mobile.Visible = flag
        rdo_ph_1_landline.Visible = flag
        rdo_ph_2_landline.Visible = flag
        rdo_ph_1_mobile.Visible = flag
        rdo_ph_2_mobile.Visible = flag
        txt_phone.Visible = flag
        txt_phone_ext.Visible = flag
        txt_fax.Visible = flag
        txt_address1.Visible = flag
        txt_address2.Visible = flag
        txt_city.Visible = flag
        txt_comment.Visible = flag

        'If flag Then
        '    ddl_state.Attributes.CssStyle.Add("display", "")
        'Else
        '    ddl_state.Attributes.CssStyle.Add("display", "none")
        'End If

        txt_other_state.Visible = flag
        txt_zip.Visible = flag
        ddl_country.Visible = flag
        txt_user_id.Visible = flag
        lit_change_password.Visible = flag

        ddl_how_find_us.Visible = flag
        chk_invite_auction.Visible = flag
        ddl_sale_rep.Visible = flag
        'chklst_business_types.Visible = flag
        'chklst_industry_types.Visible = flag
        ltrl_bucket_link.Visible = flag
        ddl_buyer_status.Visible = flag
        chklst_companies_linked.Visible = flag
        fileupload1.Visible = flag
        txt_microtelecom_no.Visible = flag
        txt_ship_carrier.Visible = flag
        txt_account_number.Visible = flag
        'lbl_confirm_email_caption.Visible = flag
        'lbl_confirm_email.Visible = flag
        'lbl_retype_password_caption.Visible = flag

        span_retype_passeord.Visible = flag
        'span_confirm_email.Visible = flag
        span_company_name.Visible = flag

        span_first_name.Visible = flag
        span_email.Visible = flag
        span_mobile.Visible = flag
        ' span_ship_carrier.Visible = flag
        'span_account_number.Visible = flag

        span_address1.Visible = flag
        span_city.Visible = flag
        span_state.Visible = flag
        span_zip.Visible = flag
        span_country.Visible = flag
        span_user_id.Visible = flag
        span_password.Visible = flag
        span_resale.Visible = flag

        lbl_sale_rep.Visible = Not (flag)
        lbl_company.Visible = Not (flag)
        lbl_title.Visible = Not (flag)
        lbl_first_name.Visible = Not (flag)
        lbl_last_name.Visible = Not (flag)
        lbl_email.Visible = Not (flag)
        lbl_retype_password.Visible = Not (flag)
        lbl_comment.Visible = Not (flag)
        lbl_website.Visible = Not (flag)
        lbl_mobile.Visible = Not (flag)
        lbl_phone.Visible = Not (flag)
        lbl_phone_ext.Visible = Not (flag)
        lbl_fax.Visible = Not (flag)
        lbl_address1.Visible = Not (flag)
        lbl_address2.Visible = Not (flag)
        lbl_city.Visible = Not (flag)
        lbl_state.Visible = Not (flag)
        lbl_zip.Visible = Not (flag)
        lbl_country.Visible = Not (flag)
        lbl_user_id.Visible = Not (flag)
        lbl_tc_date.Visible = Not (flag)
        lbl_tc_ip.Visible = Not (flag)
        lbl_password.Visible = Not (flag)
        'lst_how_find_us.Visible = Not (flag)
        ltrl_how_find_us.Visible = Not (flag)
        'lst_business_types.Visible = Not (flag)
        'ltrl_business_types.Visible = Not (flag)
        'lst_industry_types.Visible = Not (flag)
        'ltrl_industry_type.Visible = Not (flag)
        ltrl_buckets.Visible = Not (flag)
        'lst_companies_linked.Visible = Not (flag)
        ltrl_linked_companies.Visible = Not (flag)
        lbl_status.Visible = Not (flag)
        lbl_invite_auction_unsubscribe.Visible = Not (flag)
        'lnk_file1.Visible = Not (flag)
        lbl_microtelecom_no.Visible = Not (flag)
        lbl_ship_carrier.Visible = Not (flag)
        lbl_account_number.Visible = Not (flag)
        If lbl_file.text.trim = "Not Uploaded" Then
            lbl_file.Visible = Not (flag)
        End If
        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            txt_user_id.Visible = False
            tr_unsubscribe_invite.Visible = True

        End If

        If CommonCode.Fetch_Cookie_Shared("profile_code").ToUpper = SqlHelper.of_FetchKey("bidder_approval_agent_profile_code") Then
            ddl_sales.Visible = False
            lbl_sales.Visible = True
            ddl_sale_rep.Visible = False
            lbl_sale_rep.visible = True
            'RequiredFieldValidator1.ValidationGroup = "a"
        End If
    End Sub
    Protected Sub imgbtn_basic_save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_basic_save.Click
        hid_password.Value = txt_password.Text.Trim()
        If Page.IsValid Then
            If Not ddl_sales.Items.FindByValue(ddl_sale_rep.SelectedValue) Is Nothing Then
                ddl_sales.ClearSelection()
                ddl_sales.Items.FindByValue(ddl_sale_rep.SelectedValue).Selected = True
            End If
            txt_max_amount_bid.Text = "9999999"
            save_new()

            'If ddl_buyer_status.selectedValue = 2 Then

            '    modalPopUpExtender1.Show()
            'Else
            '    save_new()
            'End If
        Else
            txt_password.Attributes.Add("value", hid_password.Value)
            txt_retype_password.Attributes.Add("value", hid_password.Value)
        End If
    End Sub
    Private Sub save_new()

        Dim i As Integer = validate_user_id(txt_user_id.Text.Trim, 0)
        If i = 1 Then
            lblMessage.Text = "User exists. Please choose other user id."
            Exit Sub
        End If
        i = Validate_email(txt_email.Text.Trim, 0, 0)
        If i = 1 Then
            lblMessage.Text = "Email is already in our record."
            Exit Sub
        End If
        Dim buyer_id As Integer = 0
        Dim strQuery As String = ""
        Dim strupload = ""
        If fileupload1.HasFile Then
            strupload = IO.Path.GetFileName(fileupload1.PostedFile.FileName)
        End If
        '" & IIf(fileupload1.HasFile,IO.Path.GetFileName(fileupload1.PostedFile.FileName), "") & "
        strQuery = "Insert into tbl_reg_buyers (company_name,contact_title,contact_first_name,contact_last_name,email,website,mobile,phone,fax,address1,address2,city,state_id,zip,country_id,status_id,state_text,comment,is_phone1_mobile,is_phone2_mobile,approval_status,submit_date,submit_by_user_id,max_amt_bid,phone_ext,resale_certificate,microtelecom_account_no,default_shipping_carrier,default_shipping_account) Values ('" & CommonCode.encodeSingleQuote(txt_company.Text.Trim()) & "','" & ddl_title.SelectedValue & "','" & CommonCode.encodeSingleQuote(txt_first_name.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_last_name.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_email.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_website.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_mobile.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_phone.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_fax.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_address1.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_address2.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_city.Text.Trim()) & "',0,'" & CommonCode.encodeSingleQuote(txt_zip.Text.Trim()) & "'," & ddl_country.SelectedItem.Value & "," & ddl_buyer_status.SelectedValue & ",'" & CommonCode.encodeSingleQuote(txt_other_state.Text.Trim) & "','" & CommonCode.encodeSingleQuote(txt_comment.Text.Trim.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "'," & IIf(rdo_ph_1_landline.Checked, 0, 1) & "," & IIf(rdo_ph_2_landline.Checked, 0, 1) & ",'" & ddl_buyer_status.SelectedItem.Text & "',getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ",'" & iif(txt_max_amount_bid.Text.trim = "", 9999999, txt_max_amount_bid.Text.trim) & "','" & txt_phone_ext.text & "','" & strupload & "','" & txt_microtelecom_no.Text & "','" & txt_ship_carrier.Text & "','" & txt_account_number.Text & "') select SCOPE_IDENTITY()"
        ' txt_address1.Text = strQuery
        buyer_id = SqlHelper.ExecuteScalar(strQuery)
        If buyer_id > 0 Then
            If fileupload1.HasFile Then
                If Not System.IO.Directory.Exists(Server.MapPath("/upload/bidders/resale_certificate/" & buyer_id)) Then
                    IO.Directory.CreateDirectory(Server.MapPath("/upload/bidders/resale_certificate/" & buyer_id))
                End If
                fileupload1.PostedFile.SaveAs(Server.MapPath("/upload/bidders/resale_certificate/" & buyer_id & "/" & IO.Path.GetFileName(fileupload1.PostedFile.FileName)))
            End If
            CommonCode.insert_system_log("New bidder created", "imgbtn_basic_save_Click", buyer_id, "", "Bidder")
            strQuery = "insert into tbl_reg_sale_rep_buyer_mapping (user_id,buyer_id) values (" & iif(ddl_sale_rep.selectedvalue > 0, ddl_sale_rep.selectedvalue, SqlHelper.of_FetchKey("default_sales_rep_id")) & "," & buyer_id & "); Insert into tbl_reg_buyer_users (buyer_id,first_name,last_name,title,username,password,email,is_active,submit_date,submit_by_user_id,is_admin,bidding_limit) Values (" & buyer_id.ToString & ",'" & CommonCode.encodeSingleQuote(txt_first_name.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_last_name.Text.Trim()) & "','" & ddl_title.SelectedValue & "','" & CommonCode.encodeSingleQuote(txt_user_id.Text.Trim()) & "','" & Security.EncryptionDecryption.EncryptValue(hid_password.Value) & "','" & CommonCode.encodeSingleQuote(txt_email.Text.Trim()) & "'," & IIf(ddl_buyer_status.SelectedValue = 2, 1, 0) & ",GETDATE()," & CommonCode.Fetch_Cookie_Shared("user_id") & ",1,'" & iif(txt_max_amount_bid.Text.trim = "", 0, txt_max_amount_bid.Text.trim) & "')"
            SqlHelper.ExecuteNonQuery(strQuery)
            Update_buyer_how_find_us(buyer_id)
            'Update_buyer_business_types(buyer_id)
            'Update_buyer_industry_types(buyer_id)
            Update_buyer_companies_linked(buyer_id)
            
            lblMessage.Text = "New bidder created successfully."

            Dim obj_email As New Email
            If ddl_buyer_status.SelectedValue <> 2 Then
                obj_email.Bidderd_Admin_Active(buyer_id)
            Else
                obj_email.send_buyer_approval_email(buyer_id)
            End If
            obj_email = Nothing
            Response.Redirect("/Bidders/AddEditBuyer.aspx?e=1&i=" & buyer_id)
        Else
            txt_password.Attributes.Add("value", hid_password.Value)
            txt_retype_password.Attributes.Add("value", hid_password.Value)
            lblMessage.Text = "Error in creating new buyer. Please contact administrator."
        End If

    End Sub
    Private Function validate_user_id(ByVal username As String, ByVal id As Integer) As Integer
        Dim strQuery As String = ""
        If id > 0 Then
            strQuery = "if exists(select buyer_user_id from tbl_reg_buyer_users where username='" & CommonCode.encodeSingleQuote(username) & "' and buyer_user_id <> " & id & ") or exists(select user_id from tbl_sec_users where username='" & CommonCode.encodeSingleQuote(username) & "') select 1 else select 0"
        Else
            strQuery = "if exists(select buyer_user_id from tbl_reg_buyer_users where username='" & CommonCode.encodeSingleQuote(username) & "') or exists(select user_id from tbl_sec_users where username='" & CommonCode.encodeSingleQuote(username) & "') select 1 else select 0"
        End If

        Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)

        Return i
    End Function
    Private Function Validate_email(ByVal email As String, ByVal buyer_id As Integer, ByVal buyer_user_id As Integer) As Integer
        Dim strQuery As String = ""

        If buyer_id > 0 Then
            strQuery = "if exists(select buyer_user_id from tbl_reg_buyer_users where upper(email)=upper('" & email & "') and buyer_id<>" & buyer_id & ") or exists(select user_id from tbl_sec_users where upper(email)=upper('" & email & "')) select 1 else select 0"
        ElseIf buyer_user_id > 0 Then
            strQuery = "if exists(select buyer_user_id from tbl_reg_buyer_users where upper(email)=upper('" & email & "') and buyer_user_id<>" & buyer_user_id & ") or exists(select user_id from tbl_sec_users where upper(email)=upper('" & email & "')) select 1 else select 0"
        Else
            strQuery = "if exists(select buyer_user_id from tbl_reg_buyer_users where upper(email)=upper('" & email & "')) or exists(select user_id from tbl_sec_users where upper(email)=upper('" & email & "')) select 1 else select 0"
        End If

        Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)
        Return i
    End Function
   
    Private Sub Update_buyer_companies_linked(ByVal buyer_id As Integer)
        For Each lst As ListItem In chklst_companies_linked.Items
            Dim strQuery As String = ""
            Dim i As String = lst.Value
            If lst.Selected Then
                strQuery = "if not exists(select mapping_id from tbl_reg_buyer_seller_mapping where buyer_id=" & buyer_id.ToString() & " and seller_id=" & i & ") insert into tbl_reg_buyer_seller_mapping(buyer_id,seller_id) Values(" & buyer_id.ToString() & "," & i & ")"
            Else
                strQuery = "delete from tbl_reg_buyer_seller_mapping where buyer_id=" & buyer_id.ToString() & " and seller_id=" & i
            End If
            SqlHelper.ExecuteNonQuery(strQuery)
        Next
    End Sub
    Private Sub Update_buyer_how_find_us(ByVal buyer_id As Integer)
        Dim strQuery As String = ""
        If ddl_how_find_us.SelectedValue <> "" Then

            strQuery = "if not exists(select assignment_id from tbl_reg_buyer_find_us_assignment where buyer_id=" & buyer_id.ToString() & " and how_find_us_id=" & ddl_how_find_us.SelectedValue & ") insert into tbl_reg_buyer_find_us_assignment(buyer_id,how_find_us_id) Values(" & buyer_id.ToString() & "," & ddl_how_find_us.SelectedValue & ")"
        Else
            strQuery = "delete from tbl_reg_buyer_find_us_assignment where buyer_id=" & buyer_id.ToString() & " and how_find_us_id=" & ddl_how_find_us.SelectedValue
        End If

        SqlHelper.ExecuteNonQuery(strQuery)

    End Sub
    Protected Sub imgbtn_basic_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_basic_edit.Click
        set_form_mode(True)
        set_button_visibility("Edit")
        rfv_user_id.Visible = False

    End Sub
    Protected Sub imgbtn_basic_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_basic_cancel.Click
        set_form_mode(False)
        set_button_visibility("View")
    End Sub
    Protected Sub btn_refresh_buckets_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_refresh_buckets.Click
        ltrl_buckets.Text = get_buyer_bucket_string(Request.QueryString("i"))
    End Sub
    'Basic Update Click
    Protected Sub imgbtn_basic_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_basic_update.Click
        If Page.IsValid Then
            'first check that user select for semi approved/approved
            hid_status_click.value = "basic"
            Dim file_name As String = ""
            hid_filename.Value = ""
            If ddl_buyer_status.SelectedItem.Value = 2 Or ddl_buyer_status.SelectedItem.Value = 7 Then
                 If fileupload1.HasFile Then
                    file_name = IO.Path.GetFileName(fileupload1.PostedFile.FileName)
                    If Not System.IO.Directory.Exists(Server.MapPath("/upload/bidders/resale_certificate/" & Request.QueryString.Get("i"))) Then
                        IO.Directory.CreateDirectory(Server.MapPath("/upload/bidders/resale_certificate/" & Request.QueryString.Get("i")))
                    End If
                    fileupload1.PostedFile.SaveAs(Server.MapPath("/upload/bidders/resale_certificate/" & Request.QueryString.Get("i") & "/" & IO.Path.GetFileName(fileupload1.PostedFile.FileName)))
                End If

            Else
                If fileupload1.HasFile Then
                    file_name = IO.Path.GetFileName(fileupload1.PostedFile.FileName)
                    If Not System.IO.Directory.Exists(Server.MapPath("/upload/bidders/resale_certificate/" & Request.QueryString.Get("i"))) Then
                        IO.Directory.CreateDirectory(Server.MapPath("/upload/bidders/resale_certificate/" & Request.QueryString.Get("i")))
                    End If
                    fileupload1.PostedFile.SaveAs(Server.MapPath("/upload/bidders/resale_certificate/" & Request.QueryString.Get("i") & "/" & IO.Path.GetFileName(fileupload1.PostedFile.FileName)))
                End If
                ViewState("is_check") = 1
            End If

            'If ViewState("is_check") = 0 Then
            '    If file_name <> "" Then
            '        hid_filename.Value = file_name
            '    End If
            '    modalPopUpExtender1.Show()
            '    Return
            'End If
            basicUpdate(file_name)

                'fire code after check that user is not selecting approval 
            'If ddl_buyer_status.selectedValue = 2 And hid_status_id.Value <> 2 Then
            'If hid_buyer_max_bid_amount.value > 0 Then
            '    txt_max_amount_bid.Text = iif(hid_buyer_max_bid_amount.value = "9999999", "", hid_buyer_max_bid_amount.value)
            'End If

            'If Not ddl_sales.Items.FindByValue(ddl_sale_rep.SelectedValue) Is Nothing Then
            '    ddl_sales.ClearSelection()
            '    ddl_sales.Items.FindByValue(ddl_sale_rep.SelectedValue).Selected = True
            'End If
            'modalPopUpExtender1.Show()
            'Else
            'basicUpdate()
            'End If
        Else
            fileupload1.Focus()
            SetFocus(fileupload1)
        End If
    End Sub
    Protected Sub lnk_file1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_file1.Click
        Dim filename As String = lnk_file1.Text
        Response.ContentType = "application/octet-stream"
        Response.AppendHeader("Content-Disposition", "attachment;filename=" & filename)
        Dim aaa As String = Server.MapPath("~/upload/bidders/resale_certificate/" & Request.QueryString.Get("i") & "/" & filename)
        Response.TransmitFile(Server.MapPath("~/upload/bidders/resale_certificate/" & Request.QueryString.Get("i") & "/" & filename))
        Response.End()
    End Sub
    Protected Sub imgbtn_basic_delete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_basic_delete.Click
        'Response.Write(Request.QueryString.Get("i"))
        SqlHelper.ExecuteNonQuery("exec usp_delete_buyer " & Request.QueryString.Get("i"))
        Response.Redirect("/Bidders/Buyers.aspx")
    End Sub
    Private Sub basicUpdate(ByVal file As String)
        Dim buyer_id As Integer = IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i"))
        Dim i As Integer = 0

        i = Validate_email(txt_email.Text.Trim, buyer_id, 0)
        If i = 1 Then
            lblMessage.Text = "Email is already in our record."
            Exit Sub
        End If

        Dim strQuery As String = ""

        If hid_status_id.Value <> ddl_buyer_status.SelectedValue Then
            update_buyer_approval_status(ddl_buyer_status.SelectedItem.Value)
        End If

        strQuery = "if exists(select mapping_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & Request.QueryString.Get("i") & ") update tbl_reg_sale_rep_buyer_mapping set user_id=" & iif(ddl_sale_rep.selectedvalue > 0, ddl_sale_rep.selectedvalue, SqlHelper.of_FetchKey("default_sales_rep_id")) & " where buyer_id=" & Request.QueryString.Get("i") & " else insert into tbl_reg_sale_rep_buyer_mapping (user_id,buyer_id) values (" & iif(ddl_sale_rep.selectedvalue > 0, ddl_sale_rep.selectedvalue, SqlHelper.of_FetchKey("default_sales_rep_id")) & "," & Request.QueryString.Get("i") & "); UPDATE tbl_reg_buyers SET " & _
                   "company_name ='" & CommonCode.encodeSingleQuote(txt_company.Text.Trim()) & "' " & _
                   ",contact_title ='" & ddl_title.SelectedValue & "' " & _
                   ",contact_first_name ='" & CommonCode.encodeSingleQuote(txt_first_name.Text.Trim()) & "' " & _
                   ",contact_last_name = '" & CommonCode.encodeSingleQuote(txt_last_name.Text.Trim()) & "' " & _
                   ",email ='" & CommonCode.encodeSingleQuote(txt_email.Text.Trim()) & "' " & _
                   ",website = '" & CommonCode.encodeSingleQuote(txt_website.Text.Trim()) & "' " & _
                  ",phone ='" & CommonCode.encodeSingleQuote(txt_phone.Text.Trim()) & "' " & _
                    ",phone_ext ='" & CommonCode.encodeSingleQuote(txt_phone_ext.Text.Trim()) & "' " & _
                   IIf(file.Trim <> "", ",resale_certificate ='" & file.Trim & "'", "") & _
                  ",mobile ='" & CommonCode.encodeSingleQuote(txt_mobile.Text.Trim()) & "' " & _
                  ",microtelecom_account_no ='" & CommonCode.encodeSingleQuote(txt_microtelecom_no.Text.Trim()) & "' " & _
                  ",fax = '" & CommonCode.encodeSingleQuote(txt_fax.Text.Trim()) & "' " & _
                  ",address1 ='" & CommonCode.encodeSingleQuote(txt_address1.Text.Trim()) & "' " & _
                  ",address2 ='" & CommonCode.encodeSingleQuote(txt_address2.Text.Trim()) & "' " & _
                  ",city ='" & CommonCode.encodeSingleQuote(txt_city.Text.Trim()) & "' " & _
                  ",state_id =0" & _
                  ",state_text='" & CommonCode.encodeSingleQuote(txt_other_state.Text.Trim) & "' " & _
                  ",zip ='" & CommonCode.encodeSingleQuote(txt_zip.Text.Trim()) & "' " & _
                  ",country_id =" & ddl_country.SelectedItem.Value & _
                  ",comment ='" & CommonCode.encodeSingleQuote(txt_comment.Text.Trim.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "' " & _
                  ",is_phone1_mobile =" & IIf(rdo_ph_1_landline.Checked, 0, 1) & _
                  ",is_phone2_mobile =" & IIf(rdo_ph_2_landline.Checked, 0, 1) & _
                  ",default_shipping_carrier = '" & CommonCode.encodeSingleQuote(txt_ship_carrier.Text.Trim()) & "' " & _
                  ",default_shipping_account = '" & CommonCode.encodeSingleQuote(txt_account_number.Text.Trim()) & "' " & _
                  " WHERE buyer_id = " & buyer_id & " select 1"
        'txt_company.Text = strQuery
        'Response.Write(strQuery)
        Dim j As Integer = SqlHelper.ExecuteScalar(strQuery)

        If j = 1 Then

            strQuery = "update tbl_reg_buyer_users set first_name='" & CommonCode.encodeSingleQuote(txt_first_name.Text.Trim()) & "',last_name='" & CommonCode.encodeSingleQuote(txt_last_name.Text.Trim()) & "',title='" & ddl_title.SelectedValue & "',email='" & CommonCode.encodeSingleQuote(txt_email.Text.Trim()) & "',submit_by_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " where is_admin=1 and buyer_id=" & buyer_id
            SqlHelper.ExecuteNonQuery(strQuery)
            CommonCode.insert_system_log("Bidder basic information updated", "imgbtn_basic_update_Click", Request.QueryString.Get("i"), "", "Bidder")

            'If file <> "" Then
            '    If Not System.IO.Directory.Exists(Server.MapPath("/upload/bidders/resale_certificate/" & Request.QueryString.Get("i"))) Then
            '        IO.Directory.CreateDirectory(Server.MapPath("/upload/bidders/resale_certificate/" & Request.QueryString.Get("i")))
            '    End If
            '    io.PostedFile.SaveAs(Server.MapPath("/upload/bidders/resale_certificate/" & Request.QueryString.Get("i") & "/" & IO.Path.GetFileName(fileupload1.PostedFile.FileName)))
            'End If

            Update_buyer_how_find_us(buyer_id)
            Update_buyer_companies_linked(buyer_id)
           
            If hid_unsubscribe_invite.Value <> chk_invite_auction.Checked Then
                update_invitation_unsubscription(buyer_id)
            End If
            lblMessage.Text = "Bidder's basic information updated successfully."
            BindBuyerBasicInfo()
            bind_official_detail()
            Dim is_checked As Integer = 0



            set_form_mode(False)
            set_button_visibility("View")
        Else
            lblMessage.Text = "Error in updating. Please contact administrator."

        End If
    End Sub
    Private Sub update_invitation_unsubscription(ByVal buyer_id As Integer)
        Dim strQuery As String = ""
        If chk_invite_auction.Checked Then
            strQuery = "Declare @email varchar(50) set @email=(select email from tbl_reg_buyers WITH (NOLOCK) where buyer_id=" & buyer_id & ") if not exists(select unsubscription_id from tbl_unsubscription where email=@email and type=1)Begin Insert into tbl_unsubscription(email,type)Values(@email,1) select 1 End else Begin select 0 End"
        Else
            strQuery = "Declare @email varchar(50) set @email=(select email from tbl_reg_buyers WITH (NOLOCK) where buyer_id=" & buyer_id & ") Delete from tbl_unsubscription where email=@email and type=1 select 1"
        End If
        Dim status As Integer = SqlHelper.ExecuteScalar(strQuery)
        'txt_address1.Text = strQuery
    End Sub

#End Region

#Region "OFFICIAL INFO"

    Private Sub Bind_buyer_attachments()
        'Dim strQuery As String = ""
        'strQuery = "SELECT buyer_attachment_id, ISNULL(A.buyer_id, 0) AS buyer_id, ISNULL(A.title, '') AS title,	ISNULL(A.upload_date, '1/1/1900') AS upload_date, " & _
        '"ISNULL(A.upload_by_user_id, 0) AS upload_by_user_id, " & _
        '"ISNULL(A.filename, '') AS filename, " & _
        '"(ISNULL(U.first_name,'') + ISNULL(U.last_name,'')) AS upload_by_user " & _
        '"FROM tbl_reg_buyer_attachments A INNER JOIN tbl_sec_users U on A.upload_by_user_id=U.user_id where A.buyer_id=" & Request.QueryString.Get("i")
        'SqlDataSource1.SelectCommand = strQuery
        'rad_grid_attachment.Rebind()
    End Sub
    Private Sub set_form_mode_official(ByVal flag As Boolean)
        txt_max_amount_bid.Visible = flag
        txt_routing_no.Visible = flag
        txt_account_no.Visible = flag
        txt_name_bank.Visible = flag
        txt_branch_name.Visible = flag
        txt_bank_contact_detail.Visible = flag
        txt_bank_address.Visible = flag
        txt_tax_id_ssn_no.Visible = flag
        dd_no_of_employees.Visible = flag
        dd_annual_turnover.Visible = flag
        txt_bank_phone.Visible = flag
        lbl_approval_status.Visible = True
        'span_account_num.Visible = flag
        'span_annual_turn_over.Visible = False
        'span_bank_name.Visible = flag
        'span_baranch_name.Visible = flag
        'span_bid_amt.Visible = flag
        'span_emp_num.Visible = flag
        'span_annual_turn_over.Visible = flag
        'span_routing_num.Visible = flag
        'span_tax_id.Visible = flag

        lbl_max_amount_bid.Visible = Not (flag)
        lbl_routing_no.Visible = Not (flag)
        lbl_account_no.Visible = Not (flag)
        lbl_name_bank.Visible = Not (flag)
        lbl_branch_name.Visible = Not (flag)
        lbl_bank_address.Visible = Not (flag)
        lbl_bank_contact_detail.Visible = Not (flag)
        lbl_tax_id_ssn_no.Visible = Not (flag)
        lbl_no_of_employees.Visible = Not (flag)
        lbl_annual_turnover.Visible = Not (flag)
        lbl_bank_phone.Visible = Not (flag)
       
    End Sub
    Private Sub set_button_visibility_official(ByVal mode As String)
        If mode.ToUpper() = "VIEW" Then
            imgbtn_official_edit.Visible = True
            imgbtn_official_update.Visible = False
            'imgbtn_official_cancel.Visible = False
            div_official_cancel.Visible = False
        ElseIf mode.ToUpper() = "EDIT" Then
            imgbtn_official_edit.Visible = False
            imgbtn_official_update.Visible = True
            ' imgbtn_official_cancel.Visible = True
            div_official_cancel.Visible = True
        Else
            imgbtn_official_edit.Visible = False
            imgbtn_official_update.Visible = False
            'imgbtn_official_cancel.Visible = False
            div_official_cancel.Visible = False
        End If
    End Sub
    Protected Sub imgbtn_official_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_official_edit.Click
        set_form_mode_official(True)
        set_button_visibility_official("Edit")
    End Sub
    Protected Sub imgbtn_official_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_official_update.Click
        If Page.IsValid Then
            Update_official_info()
        End If
        'Response.Redirect("/")
    End Sub
    Protected Sub imgbtn_official_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_official_cancel.Click
        set_form_mode_official(False)
        set_button_visibility_official("View")
    End Sub
    Private Sub Update_official_info()
        Dim strQuery As String = ""
        strQuery = "UPDATE tbl_reg_buyers SET " & _
        "max_amt_bid ='" & CommonCode.encodeSingleQuote(txt_max_amount_bid.Text.Trim()) & "'" & _
        ",no_of_employees ='" & dd_no_of_employees.SelectedValue & "'" & _
        ",annual_turnover ='" & dd_annual_turnover.SelectedValue & "'" & _
        ",bank_routing_no ='" & CommonCode.encodeSingleQuote(txt_routing_no.Text.Trim()) & "'" & _
        ",bank_account_no ='" & CommonCode.encodeSingleQuote(txt_account_no.Text.Trim()) & "'" & _
        ",bank_name ='" & CommonCode.encodeSingleQuote(txt_name_bank.Text.Trim()) & "'" & _
        ",bank_contact ='" & CommonCode.encodeSingleQuote(txt_bank_contact_detail.Text.Trim()) & "'" & _
        ",bank_address ='" & CommonCode.encodeSingleQuote(txt_bank_address.Text.Trim()) & "'" & _
        ",bank_phone ='" & CommonCode.encodeSingleQuote(txt_bank_phone.Text.Trim()) & "'" & _
        ",tax_id_ssn_no ='" & Security.EncryptionDecryption.EncryptValue(txt_tax_id_ssn_no.Text.Trim()) & "'" & _
        ",bank_branch_name ='" & CommonCode.encodeSingleQuote(txt_branch_name.Text.Trim()) & "'" & _
        ",microtelecom_account_no ='" & CommonCode.encodeSingleQuote(txt_microtelecom_no.Text.Trim()) & "'" & _
        " WHERE buyer_id =" & Request.QueryString.Get("i")

        SqlHelper.ExecuteNonQuery(strQuery)
        
        If txt_max_amount_bid.Text.Trim() <> hid_buyer_max_bid_amount.Value Then
            strQuery = "update tbl_reg_buyer_users set bidding_limit= '" & CommonCode.encodeSingleQuote(iif(txt_max_amount_bid.Text.Trim() = "", 0, txt_max_amount_bid.Text.Trim())) & "' where buyer_id= " & Request.QueryString.Get("i") & " and is_admin=1"
            SqlHelper.ExecuteNonQuery(strQuery)
        End If

        CommonCode.insert_system_log("Bidder financial information updated", "Update_official_info", Request.QueryString.Get("i"), "", "Bidder")
        'txt_branch_name.Text = strQuery
        'Exit Sub
        set_form_mode_official(False)
        set_button_visibility_official("View")
        bind_official_detail()
    End Sub
    Private Sub bind_official_detail()
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable
        strQuery = "SELECT " & _
                     "ISNULL(A.max_amt_bid, 0) AS max_amt_bid, " & _
                    "ISNULL(A.no_of_employees, 0) AS no_of_employees," & _
                    "ISNULL(A.annual_turnover, 0) AS annual_turnover, " & _
                    "ISNULL(A.bank_routing_no, '') AS bank_routing_no," & _
                    "ISNULL(A.bank_account_no, '') AS bank_account_no," & _
                    "ISNULL(A.bank_name, '') AS bank_name," & _
                    "ISNULL(A.bank_contact, '') AS bank_contact," & _
                    "ISNULL(A.bank_address, '') AS bank_address," & _
                    "ISNULL(A.bank_phone, '') AS bank_phone," & _
                    "ISNULL(A.approval_status,'') AS approval_status," & _
                    "ISNULL(A.tax_id_ssn_no,'') AS tax_id_ssn_no, " & _
                    "ISNULL(A.bank_branch_name,'') AS bank_branch_name ," & _
                    "ISNULL(A.microtelecom_account_no,'') AS microtelecom_account_no " & _
                    "FROM tbl_reg_buyers A WITH (NOLOCK) WHERE A.buyer_id =" & Request.QueryString.Get("i")
        dt = SqlHelper.ExecuteDatatable(strQuery)
        If dt.Rows.Count > 0 Then

            Dim is_active As Boolean = False

            txt_max_amount_bid.Text = IIf(dt.Rows(0)("max_amt_bid") = 9999999, "", CommonCode.FormatMoney(dt.Rows(0)("max_amt_bid")))
            hid_buyer_max_bid_amount.Value = dt.Rows(0)("max_amt_bid")
            lbl_max_amount_bid.Text = IIf(dt.Rows(0)("max_amt_bid") = 9999999, "", CommonCode.FormatMoney(dt.Rows(0)("max_amt_bid")))
            txt_routing_no.Text = dt.Rows(0)("bank_routing_no")
            lbl_routing_no.Text = dt.Rows(0)("bank_routing_no")
            txt_account_no.Text = dt.Rows(0)("bank_routing_no")
            lbl_account_no.Text = dt.Rows(0)("bank_routing_no")
            txt_name_bank.Text = dt.Rows(0)("bank_name")
            lbl_name_bank.Text = dt.Rows(0)("bank_name")
            txt_branch_name.Text = dt.Rows(0)("bank_branch_name")
            lbl_branch_name.Text = dt.Rows(0)("bank_branch_name")
            txt_bank_contact_detail.Text = dt.Rows(0)("bank_contact")
            lbl_bank_contact_detail.Text = dt.Rows(0)("bank_contact")
            txt_bank_address.Text = dt.Rows(0)("bank_address")
            lbl_bank_address.Text = dt.Rows(0)("bank_address")
            txt_bank_phone.Text = dt.Rows(0)("bank_phone")
            lbl_bank_phone.Text = dt.Rows(0)("bank_phone")
            txt_tax_id_ssn_no.Text = IIF(dt.Rows(0)("tax_id_ssn_no") = "", "", Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("tax_id_ssn_no")))
            lbl_tax_id_ssn_no.Text = IIF(dt.Rows(0)("tax_id_ssn_no") = "", "", Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("tax_id_ssn_no")))
            If Not dd_no_of_employees.Items.FindByValue(dt.Rows(0)("no_of_employees")) Is Nothing Then
                dd_no_of_employees.ClearSelection()
                dd_no_of_employees.Items.FindByValue(dt.Rows(0)("no_of_employees")).Selected = True
            End If

            lbl_no_of_employees.Text = dt.Rows(0)("no_of_employees")
            If Not dd_annual_turnover.Items.FindByValue(dt.Rows(0)("annual_turnover")) Is Nothing Then
                dd_annual_turnover.ClearSelection()
                dd_annual_turnover.Items.FindByValue(dt.Rows(0)("annual_turnover")).Selected = True
            End If

            lbl_annual_turnover.Text = dt.Rows(0)("annual_turnover")
            lbl_approval_status.Text = dt.Rows(0)("approval_status")

            dt.Dispose()

        End If
    End Sub

#Region "Attachments"
    Protected Sub RadGrid_Attachment_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            Dim strQuery As String = ""
            strQuery = "SELECT buyer_attachment_id, ISNULL(A.buyer_id, 0) AS buyer_id, ISNULL(A.title, '') AS title,	ISNULL(A.upload_date, '1/1/1900') AS upload_date, " & _
            "ISNULL(A.upload_by_user_id, 0) AS upload_by_user_id, " & _
            "ISNULL(A.filename, '') AS filename, " & _
            "(ISNULL(U.first_name,'') + ' ' + ISNULL(U.last_name,'')) AS upload_by_user " & _
            "FROM tbl_reg_buyer_attachments A INNER JOIN tbl_sec_users U on A.upload_by_user_id=U.user_id where A.buyer_id=" & Request.QueryString.Get("i") & " order by upload_date desc"
            RadGrid_Attachment.DataSource = SqlHelper.ExecuteDatatable(strQuery)
        End If
    End Sub
    Protected Sub RadGrid_Attachment_DeleteCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)

        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        Dim attachment_id As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("buyer_attachment_id").ToString()
        Try
            SqlHelper.ExecuteNonQuery("DELETE from tbl_reg_buyer_attachments where buyer_attachment_id='" & attachment_id & "'")
            CommonCode.insert_system_log("Bidder financial information related attachment deleted", "RadGrid_Attachment_DeleteCommand", Request.QueryString.Get("i"), "", "Bidder")
        Catch ex As Exception
            RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to delete Attachment. Reason: " + ex.Message))
            e.Canceled = True
        End Try

    End Sub
    Protected Sub RadGrid_Attachment_UpdateCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditableItem of the RadGrid        
        Dim editedItem As GridEditFormItem = TryCast(e.Item, GridEditFormItem)
        'Get the primary key value using the DataKeyValue.        
        Dim attachment_id As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("buyer_attachment_id").ToString()
        'Access the textbox from the edit form template and store the values in string variables.        

        Dim Title As String = (TryCast(editedItem.FindControl("txt_attach_title"), TextBox)).Text
        Dim filename As String = ""
        Dim FileUpload As System.Web.UI.WebControls.FileUpload = TryCast(editedItem.FindControl("fileupload_attach"), System.Web.UI.WebControls.FileUpload)
        If FileUpload.HasFile Then filename = FileUpload.FileName
        If FileUpload.HasFile Then
            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_attachments set title='" & CommonCode.encodeSingleQuote(Title) & "', filename='" & filename & "' where buyer_attachment_id=" & attachment_id)
            UploadFileAttachment(FileUpload, Request.QueryString.Get("i"), attachment_id)
            CommonCode.insert_system_log("Bidder financial information related attachment updated", "RadGrid_Attachment_UpdateCommand", Request.QueryString.Get("i"), "", "Bidder")
        Else
            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_attachments set title='" & Title & "' where buyer_attachment_id=" & attachment_id)
            CommonCode.insert_system_log("Bidder financial information related attachment title updated", "RadGrid_Attachment_UpdateCommand", Request.QueryString.Get("i"), "", "Bidder")

        End If
    End Sub
    Protected Sub RadGrid_Attachment_InsertCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditFormInsertItem of the RadGrid        
        Dim insertedItem As GridEditFormInsertItem = DirectCast(e.Item, GridEditFormInsertItem)
        Dim attachment_id As Integer = 0
        Dim Title As String = (TryCast(insertedItem.FindControl("txt_attach_title"), TextBox)).Text
        Dim filename As String = ""
        Dim FileUpload As System.Web.UI.WebControls.FileUpload = TryCast(insertedItem.FindControl("fileupload_attach"), System.Web.UI.WebControls.FileUpload)

        If FileUpload.HasFile Then
            filename = FileUpload.FileName
        End If

        ' Try
        If filename <> "" Then
            attachment_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_reg_buyer_attachments(buyer_id, title, upload_date, upload_by_user_id, filename) VALUES (" & Request.QueryString.Get("i") & ", '" & CommonCode.encodeSingleQuote(Title) & "', getdate(), " & CommonCode.Fetch_Cookie_Shared("user_id") & ", '" & filename & "' ) select scope_identity()")
            UploadFileAttachment(FileUpload, Request.QueryString.Get("i"), attachment_id)
            CommonCode.insert_system_log("Bidder financial information related attachment uploaded", "RadGrid_Attachment_InsertCommand", Request.QueryString.Get("i"), "", "Bidder")
        End If
        ' Catch ex As Exception
        ' RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to insert attachment. Reason: " + ex.Message))
        ' e.Canceled = True
        'End Try

    End Sub
    Private Sub UploadFileAttachment(ByVal FileUpload As System.Web.UI.WebControls.FileUpload, ByVal buyer_id As Integer, ByVal attachment_id As Integer)
        Try
            Dim filename As String = ""
            If FileUpload.HasFile Then
                filename = FileUpload.FileName

            End If

            Dim pathToCreate As String = "../Upload/Bidders/official_attachments/" & buyer_id
            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
            End If

            pathToCreate = pathToCreate & "/" & attachment_id
            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
            End If
            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
            If infoFile.Exists Then
                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
            End If
            FileUpload.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

        Catch ex As Exception
            RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))

        End Try
    End Sub

    Protected Sub RadGrid_Attachment_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadGrid_Attachment.PreRender
        Dim menu As GridFilterMenu = RadGrid_Attachment.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub
#End Region


#End Region

#Region "Sub-Logins"
    Protected Sub rad_grid_sublogins_InsertCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditFormInsertItem of the RadGrid        
        Dim insertedItem As GridEditFormInsertItem = DirectCast(e.Item, GridEditFormInsertItem)
        TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = ""
        Dim buyer_id As Integer = Request.QueryString.Get("i")
        Dim first_name As String = "" ' (TryCast(insertedItem("first_name").Controls(0), TextBox)).Text
        Dim last_name As String = "" '(TryCast(insertedItem("last_name").Controls(0), TextBox)).Text
        Dim txt_first_name As TextBox = TryCast(insertedItem.FindControl("txt_sub_first_name"), TextBox)
        Dim txt_email As TextBox = TryCast(insertedItem.FindControl("txt_sub_email"), TextBox)
        Dim txt_last_name As TextBox = TryCast(insertedItem.FindControl("txt_sub_last_name"), TextBox)
        first_name = txt_first_name.Text.Trim()
        last_name = txt_last_name.Text.Trim()
        Dim txt_password As TextBox = TryCast(insertedItem.FindControl("txt_sub_password"), TextBox)
        Dim title As String = TryCast(insertedItem.FindControl("dd_sub_title"), DropDownList).SelectedValue '(TryCast(insertedItem("title").Controls(0), TextBox)).Text
        Dim username As String = (TryCast(insertedItem.FindControl("txt_sub_username"), TextBox)).Text.Trim
        Dim password As String = txt_password.Text.Trim() '(TryCast(insertedItem("password").Controls(0), TextBox)).Text
        Dim bidding_limit As String = TryCast(insertedItem.FindControl("txt_sub_bidding_limit"), TextBox).Text.Trim()
        ' Dim bidding_limit As String = (TryCast(insertedItem("bidding_limit").Controls(0), TextBox)).Text
        ' Dim rdolst_is_active As RadioButtonList = TryCast(insertedItem.FindControl("rdo_is_active"), RadioButtonList)
        Dim chk_is_active As CheckBox = TryCast(insertedItem.FindControl("chk_sub_is_active"), CheckBox)
        Dim is_active As Integer = 0
        If chk_is_active.Checked Then
            is_active = 1
        Else
            is_active = 0
        End If
        Try
            Dim strError As String = ""
            If first_name = "" Then
                strError = "First name required."
                ' ElseIf title = "" Then
                ' strError = "Title required."
            ElseIf username = "" Then
                strError = "Username required."
            ElseIf password = "" Then
                strError = "Password required."
            ElseIf password.Length < 6 Then
                strError = "Password must be atleast 6 characters."
            End If
            Dim dblBidding_limit As Double = 0
            If strError <> "" Then
                TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>" & strError & "</font>"
                e.Canceled = True
                Exit Sub
            Else
                If Not Double.TryParse(bidding_limit, dblBidding_limit) Then
                    TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Invalid bidding limit.</font>"
                    e.Canceled = True
                    Exit Sub
                Else
                    If hid_buyer_max_bid_amount.Value < dblBidding_limit AndAlso dblBidding_limit > 0 Then
                        TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>You can not set Bidding Limit more than Bidder allowed limit</font>"
                        e.Canceled = True
                        Exit Try
                    End If

                End If
            End If

            If validate_user_id(username, 0) = 1 Then
                TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Username already exists.</font>"
                e.Canceled = True
            ElseIf Validate_email(txt_email.Text.Trim, 0, 0) = 1 Then
                TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Email already exists.</font>"
                e.Canceled = True
            Else
                Dim buyer_user_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_reg_buyer_users(buyer_id, first_name, last_name,title,username,password,bidding_limit,is_active,submit_date,submit_by_user_id,is_admin,email) VALUES (" & buyer_id & ", '" & CommonCode.encodeSingleQuote(first_name) & "','" & CommonCode.encodeSingleQuote(last_name) & "','" & CommonCode.encodeSingleQuote(title) & "','" & CommonCode.encodeSingleQuote(username) & "','" & Security.EncryptionDecryption.EncryptValue(password) & "'," & iif(bidding_limit = "", 0, bidding_limit) & "," & is_active & ",getdate(), " & CommonCode.Fetch_Cookie_Shared("user_id") & ",0,'" & CommonCode.encodeSingleQuote(txt_email.Text.Trim) & "')   select scope_identity()")
                Dim objEmail As New Email()
                objEmail.send_bidder_sub_login_creation_email(buyer_user_id)
                objEmail = Nothing
                CommonCode.insert_system_log("Bidder sub-login created", "rad_grid_sublogins_InsertCommand", Request.QueryString.Get("i"), "", "Bidder")
            End If
        Catch ex As Exception
            TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Unable to add Sub-Login. Reason: " + ex.Message & "</font>"
            e.Canceled = True
        End Try

    End Sub

    Protected Sub rad_grid_sublogins_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles rad_grid_sublogins.ItemCommand

        If e.CommandName = RadGrid.InitInsertCommandName Then '"Add new" button clicked

            Dim editColumn As GridEditCommandColumn = CType(rad_grid_sublogins.MasterTableView.GetColumn("EditCommandColumn"), GridEditCommandColumn)
            editColumn.Visible = False

            'ElseIf (e.CommandName = RadGrid.EditCommandName And TypeOf e.Item Is GridEditFormItem) Then
            '    e.Canceled = True

        ElseIf (e.CommandName = RadGrid.RebindGridCommandName AndAlso e.Item.OwnerTableView.IsItemInserted) Then
            e.Canceled = True

        Else
            Dim editColumn As GridEditCommandColumn = CType(rad_grid_sublogins.MasterTableView.GetColumn("EditCommandColumn"), GridEditCommandColumn)
            If Not editColumn.Visible Then
                editColumn.Visible = True
            End If

        End If
        If (e.CommandName = RadGrid.EditCommandName And TypeOf e.Item Is GridEditFormItem) Then

            Dim item As GridEditFormItem = CType(e.Item, GridEditFormItem)
            CType(item.FindControl("txt_sub_username"), TextBox).Enabled = False
        End If
    End Sub

    Protected Sub rad_grid_sublogins_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles rad_grid_sublogins.NeedDataSource
        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then

            Dim strQuery As String = ""
            strQuery = "SELECT " & _
            "buyer_user_id," & _
            "ISNULL(A.buyer_id, 0) AS buyer_id," & _
            "ISNULL(A.first_name, '') + ' ' + ISNULL(A.last_name, '') AS name," & _
            "ISNULL(A.first_name, '') AS first_name," & _
            "ISNULL(A.last_name, '') AS last_name," & _
            "title," & _
            "A.tc_accepted_date," & _
            "ISNULL(A.tc_accept_ip, '') AS tc_accept_ip," & _
            "ISNULL(A.username, '') AS username," & _
            "ISNULL(A.password, '') AS password," & _
            "ISNULL(A.email, '') AS email," & _
            "Convert(decimal(12,2),ISNULL(A.bidding_limit, 0)) AS bidding_limit," & _
            "ISNULL(A.is_active, 0) AS is_active" & _
            " FROM tbl_reg_buyer_users A where buyer_id=" & Request.QueryString.Get("i") & " and is_admin=0 order by buyer_user_id DESC"
            rad_grid_sublogins.DataSource = SqlHelper.ExecuteDatatable(strQuery)
        End If
    End Sub

    Protected Sub rad_grid_sublogins_UpdateCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditableItem of the RadGrid   

        Dim editedItem As GridEditFormItem = TryCast(e.Item, GridEditFormItem)
        TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = ""
        'Get the primary key value using the DataKeyValue.        
        Dim buyer_user_id As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("buyer_user_id").ToString()
        Dim first_name As String = ""
        Dim last_name As String = ""
        Dim txt_first_name As TextBox = TryCast(editedItem.FindControl("txt_sub_first_name"), TextBox)
        Dim txt_last_name As TextBox = TryCast(editedItem.FindControl("txt_sub_last_name"), TextBox)
        first_name = txt_first_name.Text.Trim()
        last_name = txt_last_name.Text.Trim()
        Dim txt_email As TextBox = TryCast(editedItem.FindControl("txt_sub_email"), TextBox)
        Dim title As String = TryCast(editedItem.FindControl("dd_sub_title"), DropDownList).SelectedValue

        Dim txt_password As TextBox = TryCast(editedItem.FindControl("txt_sub_password"), TextBox)
        Dim old_active_status As Integer = 0
        Dim password As String = txt_password.Text.Trim()
        Dim bidding_limit As String = TryCast(editedItem.FindControl("txt_sub_bidding_limit"), TextBox).Text.Trim()
        Dim chk_is_active As CheckBox = TryCast(editedItem.FindControl("chk_sub_is_active"), CheckBox)
        Dim hid_old_active_status As HiddenField = TryCast(editedItem.FindControl("hid_old_active_status"), HiddenField)
        old_active_status = hid_old_active_status.Value
        Dim is_active As Integer = 0

        If chk_is_active.Checked Then
            is_active = 1
        Else
            is_active = 0
        End If
        Try
            Dim strError As String = ""
            If first_name = "" Then
                strError = "First name required."
            ElseIf password = "" Then
                strError = "Password required."
            ElseIf password.Length < 6 Then
                strError = "Password must be atleast 6 characters."
            End If
            Dim dblBidding_limit As Double = 0
            If strError <> "" Then
                TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>" & strError & "</font>"
                e.Canceled = True
                Exit Try
            Else
                If Not Double.TryParse(bidding_limit, dblBidding_limit) AndAlso bidding_limit <> "" Then
                    TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Invalid bidding limit.</font>"

                    e.Canceled = True
                    Exit Try
                Else
                    If hid_buyer_max_bid_amount.Value < dblBidding_limit AndAlso dblBidding_limit > 0 Then
                        TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>You can not set Bidding Limit more than Bidder allowed limit.</font>"
                        e.Canceled = True
                        Exit Try
                    End If
                End If
            End If

            If Validate_email(txt_email.Text.Trim, 0, buyer_user_id) = 1 Then
                TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Email already exists.</font>"
                e.Canceled = True
            Else
                Dim old_pwd As String = SqlHelper.ExecuteScalar("select password from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id)
                old_pwd = Security.EncryptionDecryption.DecryptValue(old_pwd)
                SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set first_name='" & CommonCode.encodeSingleQuote(first_name) & "',last_name='" & CommonCode.encodeSingleQuote(last_name) & "',title='" & CommonCode.encodeSingleQuote(title) & "',password='" & Security.EncryptionDecryption.EncryptValue(password) & "',bidding_limit=" & iif(bidding_limit = "", 0, bidding_limit) & ",is_active=" & is_active & ",email='" & CommonCode.encodeSingleQuote(txt_email.Text.Trim) & "' where buyer_user_id=" & buyer_user_id)
                Dim objEmail As New Email
                If password <> old_pwd Then
                    objEmail.Send_ChangePassword_Mail(txt_email.Text.Trim, first_name)

                End If
                If is_active <> old_active_status Then
                    'send email
                    objEmail.send_bidder_sub_login_active_status_changed_email(buyer_user_id)
                End If
                objEmail = Nothing
                CommonCode.insert_system_log("Bidder sub-login updated", "rad_grid_sublogins_UpdateCommand", Request.QueryString.Get("i"), "", "Bidder")
            End If

        Catch ex As Exception
            TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Unable to add Sub-Login. Reason: " + ex.Message & "</font>"
            e.Canceled = True
        End Try
    End Sub
    Protected Sub rad_grid_sublogins_DeleteCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles rad_grid_sublogins.DeleteCommand
        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        Dim buyer_user_id As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("buyer_user_id").ToString()
        Dim buyer_id As String = item("buyer_id").Text
        Try
            SqlHelper.ExecuteNonQuery("DELETE from tbl_reg_buyer_users where buyer_user_id='" & buyer_user_id & "'")
            CommonCode.insert_system_log("Bidder sub-login deleted.", "rad_grid_sublogins_DeleteCommand", Request.QueryString.Get("i"), "", "Bidder")
        Catch ex As Exception
            rad_grid_sublogins.Controls.Add(New LiteralControl("Unable to delete Sub-Login. Reason: " + ex.Message))
            e.Canceled = True
        End Try

    End Sub

    Protected Sub rad_grid_sublogins_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles rad_grid_sublogins.ItemDataBound
        Dim is_active As Boolean = False
        If TypeOf e.Item Is GridEditableItem AndAlso e.Item.IsInEditMode Then
            Dim dataItem As GridEditableItem = CType(e.Item, GridEditableItem)
            Dim txt_password As TextBox = DirectCast(dataItem.FindControl("txt_sub_password"), TextBox)
            Dim hid_password As HiddenField = DirectCast(dataItem.FindControl("hid_sub_password"), HiddenField)
            Dim txt_username As TextBox = DirectCast(dataItem.FindControl("txt_sub_username"), TextBox)
            'Dim lbl_tcsub_acpt_date As TextBox = TryCast(dataItem.FindControl("lbl_tc_subaccept_date"), TextBox)
            'Dim lbl_tc_subaccept_date = TryCast(dataItem.FindControl("txt_sub_email"), TextBox)

            Dim txt_email As TextBox = TryCast(dataItem.FindControl("txt_sub_email"), TextBox)

            txt_password.Attributes.Add("value", hid_password.Value)

        End If
    End Sub
    Protected Function checked(ByVal r As Object) As Boolean
        If r Is DBNull.Value Then
            Return True
        ElseIf r = True Then
            Return True
        Else
            Return False
        End If
    End Function

#End Region

#Region "Invite Auctions"

    Protected Sub rad_grid_invite_auctions_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles rad_grid_invite_auctions.NeedDataSource
        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            Dim bidder_max_bid As Double = SqlHelper.ExecuteScalar("select isnull(max_amt_bid,0) from tbl_reg_buyers WITH (NOLOCK) where buyer_id=" & IIf(String.IsNullOrEmpty(Request.QueryString("i")), 0, Request.QueryString("i")))
            Dim strQuery As String = ""
            strQuery = "SELECT  " & _
                       "A.auction_id, " & _
                      "ISNULL(A.code, '') AS code, " & _
                      "ISNULL(A.title, '') AS title, " & _
                      "dbo.auction_status(A.auction_id) as status " & _
                      "FROM tbl_auctions A WITH (NOLOCK)"
            If Not CommonCode.is_admin_user() Then
                strQuery = strQuery & " inner join [tbl_reg_seller_user_mapping] B on A.seller_id=B.seller_id and B.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
            End If
            strQuery = strQuery & " where isnull(A.discontinue,0)=0 and 1=case when isnull(A.auction_type_id,0)=2 or isnull(A.auction_type_id,0)=3 then case when isnull(A.reserve_price,0)<=" & bidder_max_bid & " then 1 else 0 end when isnull(A.auction_type_id,0)=1 then case when isnull(A.show_price,0)<=" & bidder_max_bid & " then 1 else 0 end else 1 end and A.is_active=1 and ISNULL(A.display_end_time, '1/1/1900') > getdate() and [dbo].[is_buyer_invited](A.auction_id," & Request.QueryString("i") & ") = 0 and A.show_relevant_bidders_only=1"
            rad_grid_invite_auctions.DataSource = SqlHelper.ExecuteDatatable(strQuery)

        End If
    End Sub

    Protected Sub btn_invite_all_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_invite_all.Click
        For Each dataItem As GridDataItem In rad_grid_invite_auctions.MasterTableView.Items
            Dim is_checked As Boolean = TryCast(dataItem.FindControl("CheckBox1"), CheckBox).Checked
            Dim strQuery As String = ""
            Dim auction_id As Integer = CInt(dataItem.GetDataKeyValue("auction_id").ToString())
            If is_checked Then
                insertToAuctionInvitation(Request.QueryString.Get("i"), auction_id)
                'strQuery = "if not exists(select invitation_filter_value_id from tbl_auction_invitation_filter_values where buyer_id=" & Request.QueryString.Get("i") & " and ISNULL(invitation_filter_id,0)=0 and auction_id=" & auction_id & ") Insert into tbl_auction_invitation_filter_values(buyer_id,auction_id,invitation_filter_id,business_type_id,industry_type_id,bucket_id)Values(" & Request.QueryString.Get("i") & "," & auction_id & ",0,0,0,0) "
            Else
                strQuery = "delete from tbl_auction_invitation_filter_values where buyer_id=" & Request.QueryString.Get("i") & " and auction_id=" & auction_id
            End If
            SqlHelper.ExecuteNonQuery(strQuery)

            Dim objAuction As New Auction()
            objAuction.update_auction_bidder_invitation(auction_id)
            objAuction = Nothing

            'lbl_invite_qry.Text = lbl_invite_qry.Text & ";" & strQuery
        Next
        CommonCode.insert_system_log("Bidder invited for auctions", "Update_official_info", Request.QueryString.Get("i"), "", "Bidder")
        rad_grid_invite_auctions.Rebind()

    End Sub

    Private Sub insertToAuctionInvitation(ByVal bidder_id As Integer, ByVal auction_id As Integer)
        Dim invitation_id As Integer = SqlHelper.ExecuteScalar("if exists(select invitation_id from tbl_auction_invitations where auction_id=" & auction_id & ") begin select top 1 invitation_id from tbl_auction_invitations where auction_id=" & auction_id & " end else select 0")
        If invitation_id = 0 Then invitation_id = insertNewInvitation(auction_id)
        Dim qry As String = "if exists(select invitation_filter_id from tbl_auction_invitation_filters where invitation_id=" & invitation_id & " and filter_type='bidder') begin select invitation_filter_id from tbl_auction_invitation_filters where invitation_id=" & invitation_id & " and filter_type='bidder' end else begin INSERT INTO tbl_auction_invitation_filters(invitation_id, filter_type) VALUES (" & invitation_id & ",'bidder') SELECT SCOPE_IDENTITY() end"
        Dim invitation_filter_id As Integer = SqlHelper.ExecuteScalar(qry)
        'lbl_invite_qry.Text = lbl_invite_qry.Text & ";" & qry
        qry = "if not exists(select invitation_filter_value_id from tbl_auction_invitation_filter_values where invitation_filter_id=" & invitation_filter_id & " and bucket_id=0 and buyer_id=" & bidder_id & ") begin INSERT INTO tbl_auction_invitation_filter_values(invitation_filter_id, bucket_id,bucket_value_id, buyer_id, auction_id)" & _
                        "VALUES (" & invitation_filter_id & ",0,0," & bidder_id & ", " & auction_id & ") end"
        SqlHelper.ExecuteNonQuery(qry)
        'lbl_invite_qry.Text = lbl_invite_qry.Text & ";" & qry
    End Sub

    Private Function insertNewInvitation(ByVal auction_id As Integer) As Integer
        Dim qry As String = ""
        Dim invitation_id As Integer = 0
        Dim title As String = ""
        invitation_id = SqlHelper.ExecuteScalar("select isnull(max(invitation_id),0) from tbl_auction_invitations")
        invitation_id = invitation_id + 1
        title = "My Fav " & invitation_id

        qry = "INSERT INTO tbl_auction_invitations(auction_id, name, submit_date, submit_by_user_id) " & _
            "VALUES (" & auction_id & ", '" & title & "', getdate(), " & CommonCode.Fetch_Cookie_Shared("user_id") & ")  select scope_identity()"
        'lbl_invite_qry.Text = lbl_invite_qry.Text & ";" & qry
        invitation_id = SqlHelper.ExecuteScalar(qry)
        Return invitation_id
    End Function

    Protected Sub ToggleRowSelection(ByVal sender As Object, ByVal e As EventArgs)

        TryCast(TryCast(sender, CheckBox).NamingContainer, GridItem).Selected = TryCast(sender, CheckBox).Checked
        Dim checkHeader As Boolean = True
        For Each dataItem As GridDataItem In rad_grid_invite_auctions.MasterTableView.Items
            If Not TryCast(dataItem.FindControl("CheckBox1"), CheckBox).Checked Then
                checkHeader = False
                Exit For
            End If
        Next
        Dim headerItem As GridHeaderItem = TryCast(rad_grid_invite_auctions.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)
        TryCast(headerItem.FindControl("headerChkbox"), CheckBox).Checked = checkHeader
    End Sub

    Protected Sub ToggleSelectedState(ByVal sender As Object, ByVal e As EventArgs)
        Dim headerCheckBox As CheckBox = TryCast(sender, CheckBox)
        For Each dataItem As GridDataItem In rad_grid_invite_auctions.MasterTableView.Items
            TryCast(dataItem.FindControl("CheckBox1"), CheckBox).Checked = headerCheckBox.Checked
            dataItem.Selected = headerCheckBox.Checked
        Next
    End Sub

    Private Sub SelectInvitedAuctions()
        Dim strQuery As String = "select ISNULL(auction_id,0) as auction_id from tbl_auction_invitation_filter_values where buyer_id=" & Request.QueryString.Get("i")
        Dim dt As DataTable = New DataTable()
        dt = SqlHelper.ExecuteDatatable(strQuery)

        If dt.Rows.Count > 0 Then

            For Each dataItem As GridDataItem In rad_grid_invite_auctions.MasterTableView.Items

                Dim CheckBox1 As CheckBox = TryCast(dataItem.FindControl("CheckBox1"), CheckBox)
                Dim auction_id As Integer = 0
                auction_id = dataItem.GetDataKeyValue("auction_id")
                For i As Integer = 0 To dt.Rows.Count - 1
                    If dt.Rows(i)("auction_id") = auction_id Then
                        dataItem.Selected = True
                        Exit For
                    End If
                Next
            Next
        End If

    End Sub

#End Region
    Private Sub top_fileupload()
        Dim filename As String = ""
        Dim FileUpload As System.Web.UI.WebControls.FileUpload = topfileupload_attach
        Dim buyer_id As String = Request.QueryString.Get("i")
        Dim attachment_id As Integer = 0
        Dim Title As String = TextBox1.Text
        If FileUpload.HasFile Then filename = FileUpload.FileName
        If FileUpload.HasFile Then
            'lbl_file_upload_status.text = FileUpload.PostedFile.FileName
            'attachment_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_reg_buyer_attachments(buyer_id, title, upload_date, upload_by_user_id, filename) VALUES (" & buyer_id & ", '" & TextBox1.Text & "', '" & Now & "'," & CommonCode.Fetch_Cookie_Shared("user_id") & ", '" & filename & "' )")
            'If attachment_id > 0 Then
            '    UploadFileAttachment(FileUpload, Request.QueryString.Get("i"), attachment_id)
            '    lbl_file_upload_status.text = "Upload Successful"
            'End If


            If filename <> "" Then
                attachment_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_reg_buyer_attachments(buyer_id, title, upload_date, upload_by_user_id, filename) VALUES (" & Request.QueryString.Get("i") & ", '" & CommonCode.encodeSingleQuote(Title) & "', getdate(), " & CommonCode.Fetch_Cookie_Shared("user_id") & ", '" & filename & "' ) select scope_identity()")
                UploadFileAttachment(FileUpload, Request.QueryString.Get("i"), attachment_id)
                CommonCode.insert_system_log("Bidder financial information related attachment uploaded", "RadGrid_Attachment_InsertCommand", Request.QueryString.Get("i"), "", "Bidder")
            End If



        End If
        'RadGrid_Attachment.Rebind()
        'TextBox1.text = String.Empty
        'modalPopUpExtender1.Show()
    End Sub
    Private Sub top_fileupload1()
        Dim filename As String = ""
        Dim FileUpload As System.Web.UI.WebControls.FileUpload = topfileupload_attach1
        Dim buyer_id As String = Request.QueryString.Get("i")
        Dim attachment_id As Integer = 0
        Dim Title As String = TextBox2.Text
        If FileUpload.HasFile Then filename = FileUpload.FileName
        'If FileUpload.HasFile Then
        '    attachment_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_reg_buyer_attachments(buyer_id, title, upload_date, upload_by_user_id, filename) VALUES (" & buyer_id & ", '" & TextBox2.Text & "', '" & Now & "'," & CommonCode.Fetch_Cookie_Shared("user_id") & ", '" & filename & "' )")
        '    If attachment_id > 0 Then
        '        UploadFileAttachment(FileUpload, Request.QueryString.Get("i"), attachment_id)
        '    End If
        'End If

        If filename <> "" Then
            attachment_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_reg_buyer_attachments(buyer_id, title, upload_date, upload_by_user_id, filename) VALUES (" & Request.QueryString.Get("i") & ", '" & CommonCode.encodeSingleQuote(Title) & "', getdate(), " & CommonCode.Fetch_Cookie_Shared("user_id") & ", '" & filename & "' ) select scope_identity()")
            UploadFileAttachment(FileUpload, Request.QueryString.Get("i"), attachment_id)
            CommonCode.insert_system_log("Bidder financial information related attachment uploaded", "RadGrid_Attachment_InsertCommand", Request.QueryString.Get("i"), "", "Bidder")
        End If




        'RadGrid_Attachment.Rebind()
        'Textbox2.text = String.Empty
        'modalPopUpExtender1.Show()
    End Sub
    

End Class





