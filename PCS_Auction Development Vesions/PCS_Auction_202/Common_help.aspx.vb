﻿
Partial Class Common_help
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Bind_help()
    End Sub
    Private Sub Bind_help()
        If IsNumeric(Request.QueryString("i")) Then
            Dim strQuery As String = ""
            Dim dt As DataTable
            strQuery = "select help_id,name,title,description from tbl_help where help_id=" & Request.QueryString.Get("i")
            dt = SqlHelper.ExecuteDataTable(strQuery)
            If dt.Rows.Count > 0 Then
                lbl_help_title.Text = dt.Rows(0)("title")
                ltrl_help_desc.Text = dt.Rows(0)("description")
            End If
        End If
    End Sub

End Class
