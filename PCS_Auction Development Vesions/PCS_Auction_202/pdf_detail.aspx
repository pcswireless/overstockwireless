﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/SitePopUp.master" CodeFile="pdf_detail.aspx.vb" Inherits="pdf_detail" EnableEventValidation="false" %>
<%@ Register Src="~/UserControls/auction_price_summary.ascx" TagName="price_summary" TagPrefix="UC1" %>
<%@ Register Src="~/UserControls/Bidder_Rank.ascx" TagName="auction_rank" TagPrefix="UC1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="text-align: right; padding-right: 45px;">
        <asp:Button runat="server" ID="but_to_pdf" Text="Save as pdf" />
        &nbsp;
        <asp:ImageButton ID="img_close" runat="server" AlternateText="Close" />
    </div>
    <link href="/Style/elastislide.css" rel="stylesheet" type="text/css" />
    <link href="/style/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/Style/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="/source/jquery.fancybox.css?v=2.1.5"
        media="screen" />
    <script type="text/javascript">

        /***********************************************
        * Different CSS depending on OS (mac/pc)- © Dynamic Drive (www.dynamicdrive.com)
        * This notice must stay intact for use
        * Visit http://www.dynamicdrive.com/ for full source code
        ***********************************************/
        ///////No need to edit beyond here////////////

        var mactest = navigator.userAgent.indexOf("Mac") != -1


        if (mactest) {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet_mac.css"/>');
        }
        else {
            document.write('<link rel="stylesheet" type="text/css" href="/style/newstylesheet.css"/>');
        }
    </script>
     <script type="text/javascript">        E_refresh = window.setTimeout(function () { window.location.href = window.location.href }, 10000);</script>
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>

    <script type="text/javascript" src="/js/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/js/jquery.elastislide.js"></script>
    <script type="text/javascript" src="/js/gallery.js"></script>
    <script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">
        <div class="rg-image-wrapper">
            {{if itemsCount > 1}}
        <div class="rg-image-nav">
        </div>
            {{/if}}
        <div class="rg-image" style="padding-left: 5px\9; padding-right: 5px\9;">
        </div>
            <div class="rg-loading">
            </div>
            <div class="rg-caption-wrapper">
                <div class="rg-caption" style="display: none;">
                    <p>
                    </p>
                </div>
            </div>
        </div>
    </script>


    <asp:Panel ID="pnlContent" runat="server">
        <div class="wraper content">
            <asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
            <asp:HiddenField ID="hid_auction_type" runat="server" Value="0" />

            <div>
                <div class="innerwraper wraper1">


                    <div class="products1">
                        <div style="margin-top: 25px; background: url('../images/auction/page_bg.gif') repeat-y 0px 0px; width: 900px; background-color: #ffffff;">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td style="float: left; width: 300px; padding-left: 10px; padding-top: 20px;" valign="top">
                                        <div>
                                            <asp:Image ID="imgMain" runat="server" />
                                        </div>
                                    </td>
                                    <td class="rightside" style="width: 510px;padding-left:6px;padding-top:10px;" valign="top">
                                        <div style="float:right;">
                                            <strong>
                                                <asp:Literal ID="ltr_title" Text="title" runat="server" /></strong>
                                            <br />
                                            <span class="grytxt">
                                                <asp:Literal ID="ltr_sub_title" Text="sub title" runat="server" /></span><br />
                                            <span class="grytxt">Auction # : <strong>
                                                <asp:Literal ID="ltr_auction_code" runat="server" /></strong></span>
                                            <div class="prodetail">
                                                <asp:Panel runat="server" ID="pnl_from_live">
                                                    <%--<iframe id="iframe_price_summary" width="100%" height="200px" scrolling="no" frameborder="0" runat="server"></iframe>--%>
                                                     <UC1:price_summary runat="server" ID="iframe_price_summary"></UC1:price_summary>
                                                    <div class="clear">
                                                    </div>
                                                   <%-- <iframe id="iframe_auction_rank" scrolling="no" frameborder="0" width="100%" runat="server"
                                                        height="55px"></iframe>--%>
                                                    <UC1:auction_rank runat="server" ID="iframe_auction_rank"></UC1:auction_rank>
                                                    <asp:Literal runat="server" ID="ltr_quote_msg"></asp:Literal>
                                                    <div class="clear">
                                                    </div>
                                                    <div style="padding-top: 15px; padding-left: 20px; font-family: 'dinbold'; font-size: 18px; color: #575757; font-weight: normal;">
                                                        <div class="tmlft">
                                                            <span style="padding-top: 0px;">
                                                                <asp:Literal runat="server" ID="ltr_timer_caption" Text="Time Left"></asp:Literal></span>
                                                        </div>
                                                        <!--tmlft -->
                                                        <div class="hrtime">
                                                            <iframe id="iframe_auction_time" scrolling="no" frameborder="0" width="100%" runat="server"
                                                                height="50px"></iframe>
                                                        </div>
                                                        <!--htime -->
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                    <!--timeklft -->
                                                    <div class="shpmthd">
                                                        <div style="float: left;">

                                                            <%--<asp:TextBox runat="server" CssClass="typtxt" ID="txt_max_bid_amt" Text="Enter Your Bid Amount"
                                                        ToolTip="Enter Your Bid Amount"></asp:TextBox>--%>
                                                            <%-- <span class="timeleft_qty" style="float: left;">
                                                        <asp:Literal runat="server" ID="ltr_buy_qty_or_caption" Text=""></asp:Literal>
                                                    </span>--%>

                                                            <%--<asp:DropDownList CssClass="typtxt" ID="ddl_buy_now_qty" Style="width: 100px;" runat="server"
                                                        AutoPostBack="false" />--%>

                                                            <div class="clear">
                                                            </div>
                                                            <iframe id="iframe_auction_amount" scrolling="no" frameborder="0" width="200px;" runat="server"
                                                                height="35px;"></iframe>
                                                            <asp:Panel runat="server" ID="dv_bidding1" Visible="false">
                                                                <div class="clear">
                                                                </div>
                                                                <asp:Label ForeColor="Red" Font-Size="22" Text="Sold Out" Font-Names="'dinregular',arial" Visible="false" runat="server" ID="ltr_buynow_close"></asp:Label>
                                                            </asp:Panel>
                                                        </div>
                                                        <%--<div style="float: left;" runat="server" id="dv_shipping" visible="false" class="noprint">
                                                <div class="btnship">
                                                    <asp:DropDownList ID="ddl_shipping" runat="server" CssClass="typtxt" Style="width: 238px; margin-top: 15px;">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>--%>
                                                    </div>

                                                    <asp:HiddenField ID="hid_buy_now_confirm_amount" runat="server" Value="0" />
                                                    <div class="clear">
                                                    </div>
                                                    <!--shpmthd -->
                                                    <div class="bidnw">
                                                        <%-- <div class="cs_Validator" id="div_Validator">
                                                <span id="spn_valid"></span><span id="spn_amt_hlp" style="display: none;">
                                                    <asp:Literal ID="ltr_amt_help" runat="server"></asp:Literal>
                                                </span>
                                            </div>
                                            <asp:Panel ID="pnlNoprint" runat="server" CssClass="noprint">

                                                <asp:Button runat="server" ID="but_main_auction" CssClass="bid" Text="Bid Now" Style="visibility: hidden;" />
                                                <asp:Literal runat="server" ID="ltr_offer_button"></asp:Literal>
                                            </asp:Panel>
                                                        --%>
                                                        <div class="clear">
                                                        </div>
                                                        <p>
                                                            <asp:Literal ID="ltr_bid_note" runat="server"></asp:Literal>
                                                        </p>
                                                    </div>
                                                </asp:Panel>
                                                <asp:Panel runat="server" ID="pnl_from_account">
                                                    <asp:Literal runat="server" ID="ltr_offer_attachement"></asp:Literal>
                                                    <asp:Literal runat="server" ID="ltr_current_status"></asp:Literal>
                                                </asp:Panel>
                                                <div class="clear">
                                                </div>
                                                <!--bidnow -->
                                            </div>
                                            <!--prodetail -->
                                        </div>
                                    </td>
                                </tr>
                            </table>

                            <!--lftside -->

                            <!--rightside -->
                            <div class="clear">
                            </div>
                        </div>
                        <!--detailbxwrap -->
                        <div class="clear">
                        </div>
                        <%-- <div class="middlerow">
                            <div class="askbx">
                                <span class="noprint">
                                    <asp:LinkButton runat="server" ID="lnk_my_auction" CssClass="actn" CommandName="ins"
                                        Text="Add To My Live Auctions" Visible="false"></asp:LinkButton>
                                    <asp:Literal ID="lit_ask_question" runat="server" Visible="false" /></span>

                                <div class="clear">
                                </div>
                            </div>
                           
                            
                            <div class="clear">
                            </div>
                        </div>--%>
                    </div>
                    <!--products -->
                    <div class="clear">
                    </div>
                    <asp:Literal runat="server" ID="ltr_summary_detail"></asp:Literal>
                    <!--summrybx -->
                    <div runat="server" id="dv_items_details" class="details dtltxtbx">
                        <h1 class="pgtitle">Items</h1>

                        <div class="txt6">
                            <div class="item_size">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th>Manufacturer
                                            </th>
                                            <th>Title
                                            </th>
                                            <th>Part #
                                            </th>
                                            <th>Quantity
                                            </th>
                                            <th>UPC
                                            </th>
                                            <th>SKU
                                            </th>
                                            <th>Estimated MSRP
                                            </th>
                                            <th>Total MSRP
                                            </th>
                                        </tr>
                                        <tr>
                                            <asp:Repeater ID="rptItems" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="background-color: White;">
                                                            <%# Eval("manufacturer")%>
                                                        </td>
                                                        <td style="background-color: White;">
                                                            <%# Eval("title")%>
                                                        </td>
                                                        <td style="background-color: White;">
                                                            <%# Eval("part_no")%>
                                                        </td>
                                                        <td style="background-color: White;">
                                                            <%# Eval("quantity")%>
                                                        </td>
                                                        <td style="background-color: White;">
                                                            <%# Eval("UPC")%>
                                                        </td>
                                                        <td style="background-color: White;">
                                                            <%# Eval("SKU")%>
                                                        </td>
                                                        <td style="background-color: White;">
                                                            <%# CommonCode.GetFormatedMoney(Eval("estimated_msrp"))%>
                                                        </td>
                                                        <td style="background-color: White;">
                                                            <%# CommonCode.GetFormatedMoney(Eval("total_msrp"))%>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!--text6 -->
                    </div>
                    <!--items -->
                    <asp:Literal runat="server" ID="ltr_auction_details"></asp:Literal>
                    <!--details -->


                </div>
            </div>

        </div>
    </asp:Panel>
    <script>
        window.focus();
    </script>
</asp:Content>
