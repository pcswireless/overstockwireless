﻿
Partial Class login_frontend
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            If Request.QueryString.Get("bi") = Nothing Then
                If CommonCode.Fetch_Cookie_Shared("is_backend") = "1" Then
                    If Request.QueryString.Get("q") <> Nothing Then
                        Response.Redirect("/Backend_Home.aspx?t=1&q=" & Request.QueryString.Get("q"))
                    Else
                        Response.Redirect("/Backend_Home.aspx?t=1")
                    End If
                ElseIf CommonCode.Fetch_Cookie_Shared("is_backend") = "0" Then
                    If Request.QueryString.Get("a") <> Nothing Then
                        Response.Redirect("/Auction_Details.aspx?t=1&a=" & Request.QueryString.Get("a") & "")
                    Else
                        Response.Redirect("/Auction_Listing.aspx?t=1")
                    End If
                End If
            End If

        End If
        
    End Sub
End Class
