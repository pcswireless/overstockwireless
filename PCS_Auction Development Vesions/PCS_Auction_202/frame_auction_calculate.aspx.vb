﻿
Partial Class frame_auction_calculate
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim obj As New Auction
            ViewState("rank_list") = ""
            Dim dt As New DataTable
            dt = obj.fetch_auction_Listing_new(IIf(String.IsNullOrEmpty(Request.QueryString.Get("t")), 1, Request.QueryString.Get("t")), IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), 0, Request.QueryString.Get("c")), IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), 0, Request.QueryString.Get("p")), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("user_id")), CommonCode.Fetch_Cookie_Shared("user_id"), 0), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("buyer_id")), CommonCode.Fetch_Cookie_Shared("buyer_id"), 0), SqlHelper.of_FetchKey("frontend_login_needed"), IIf(String.IsNullOrEmpty(Request.QueryString.Get("a")), 0, Request.QueryString.Get("a"))).Tables(0)
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    setAuction(dr("auction_id"))
                Next
                If ViewState("rank_list") <> "" Then
                    'Response.Write(ViewState("rank_list"))
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "_send", "window.parent.calculate_rank('" & ViewState("rank_list") & "')", True)
                End If

            End If
            obj = Nothing
        End If
    End Sub

    Private Sub setAuction(ByVal auction_id As Int32)
        Me.litRefresh.Visible = True

        'IG 2014 0911
        Dim dtTable As New DataTable()

        Dim SqlParameter() As SqlParameter = New SqlParameter(0) {}

        SqlParameter(0) = New SqlParameter("@auction_id", SqlDbType.Int)
        SqlParameter(0).Direction = ParameterDirection.Input
        SqlParameter(0).Value = auction_id

        

        dtTable = SqlHelper.ExecuteDatatable(SqlHelper.of_getConnectString(), CommandType.StoredProcedure, "get_auction_frame_byid", SqlParameter)


        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)

                Dim bid_amount As Double = 0
                Dim bidder_max_amount As Double = 0

                'panel setting
                If .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then

                   Dim c_bid_amount As Double = 0

                    If .Item("bid_amount").ToString <> String.Empty Then
                        c_bid_amount = .Item("bid_amount")
                    End If


                    If .Item("is_show_rank") Then
                        If String.IsNullOrEmpty(CommonCode.Fetch_Cookie_Shared("buyer_id")) Then
                            ViewState("rank_list") = ViewState("rank_list") & auction_id & "#Current Rank : --~"
                        Else
                            Dim rak As Integer = SqlHelper.ExecuteScalar("select dbo.buyer_bid_rank(" & auction_id & "," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
                            ViewState("rank_list") = ViewState("rank_list") & auction_id & "#Current Rank : " & IIf(rak = 0, "--", rak) & "~"

                        End If
                    End If
                End If


            End With
        End If
        dtTable = Nothing
    End Sub
End Class
