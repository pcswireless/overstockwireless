﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendPopUP.master" AutoEventWireup="false"
    CodeFile="InvitedBidderList.aspx.vb" Inherits="MessageBoard_InvitedBidderList" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="padding-bottom: 10px; padding-top: 8px;">
        <div class="pageheading">
            &nbsp;
            <asp:Label ID="lbl_heading" runat="server"></asp:Label></div>
    </div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div style="padding: 0; margin: 0; padding-bottom: 10px; background-color: rgb(251,251,251);">
                    <table width="96%" style="text-align: left;">
                        <tr>
                            <td style="padding-left: 10px;">
                                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                                    <AjaxSettings>
                                        <telerik:AjaxSetting AjaxControlID="rad_grid_invited_bidders">
                                            <UpdatedControls>
                                                <telerik:AjaxUpdatedControl ControlID="rad_grid_invited_bidders" LoadingPanelID="RadAjaxLoadingPanel1" />
                                            </UpdatedControls>
                                        </telerik:AjaxSetting>
                                    </AjaxSettings>
                                </telerik:RadAjaxManager>
                                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
                                    left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
                                    <img id="Image8" src="/images/img_loading.gif" />
                                </telerik:RadAjaxLoadingPanel>
                                <asp:Panel ID="pnl_invited_bidders" runat="server" Width="100%">
                                    <telerik:RadGrid ID="rad_grid_invited_bidders" GridLines="None" runat="server" PageSize="10"
                                        AllowPaging="True" AutoGenerateColumns="False" Skin="Vista" AllowFilteringByColumn="True"
                                        AllowSorting="true">
                                        <HeaderStyle BackColor="#BEBEBE" />
                                        <ItemStyle Font-Size="11px" />
                                        <PagerStyle Mode="NextPrevAndNumeric" />
                                        <MasterTableView DataKeyNames="buyer_id" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                                            AllowFilteringByColumn="True">
                                            <Columns>
                                                <telerik:GridTemplateColumn HeaderText="Company" SortExpression="company_name" ShowSortIcon="true"
                                                    UniqueName="company_name" DataField="company_name" FilterControlWidth="50">
                                                    <ItemTemplate>
                                                        <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');">
                                                            <%# Eval("company_name")%></a>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn DataField="contact_title" HeaderText="Title" SortExpression="contact_title"
                                                    UniqueName="contact_title" FilterControlWidth="50">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="name" HeaderText="Name" SortExpression="name"
                                                    UniqueName="name" FilterControlWidth="50">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridTemplateColumn DataField="email" SortExpression="email" AllowFiltering="true"
                                                    HeaderText="Email Address" FilterControlWidth="60" GroupByExpression="email [Email Address] Group By email">
                                                    <ItemTemplate>
                                                        <a href="mailto:<%# Eval("email")%>">
                                                            <%# Eval("email")%></a>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn DataField="mobile" HeaderText="Phone" SortExpression="mobile"
                                                    UniqueName="mobile" FilterControlWidth="50">
                                                </telerik:GridBoundColumn>
                                               
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                </asp:Panel>
                                <asp:Panel ID="pnl_invited_sellers" runat="server" Width="100%">
                                    <telerik:RadGrid ID="rad_grid_sellers" GridLines="None" runat="server" PageSize="10"
                                        AllowPaging="True" AutoGenerateColumns="False" Skin="Vista" AllowFilteringByColumn="True"
                                        AllowSorting="true">
                                        <HeaderStyle BackColor="#BEBEBE" />
                                        <ItemStyle Font-Size="11px" />
                                        <PagerStyle Mode="NextPrevAndNumeric" />
                                        <MasterTableView DataKeyNames="user_id" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                                            AllowFilteringByColumn="True">
                                            <Columns>
                                                <telerik:GridTemplateColumn HeaderText="Title" SortExpression="contact_title" ShowSortIcon="true"
                                                    UniqueName="contact_title" DataField="contact_title" FilterControlWidth="50"
                                                    GroupByExpression="contact_title [Title] Group By contact_title">
                                                    <ItemTemplate>
                                                        <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/users/user_mouse_over.aspx?i=<%# Eval("user_id") %>','270','white','true');">
                                                            <%# Eval("contact_title")%></a>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn DataField="name" HeaderText="Name" SortExpression="name"
                                                    UniqueName="name" FilterControlWidth="50">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridTemplateColumn DataField="email" SortExpression="email" AllowFiltering="true"
                                                    HeaderText="Email Address" FilterControlWidth="60" GroupByExpression="email [Email Address] Group By email">
                                                    <ItemTemplate>
                                                        <a href="mailto:<%# Eval("email")%>">
                                                            <%# Eval("email")%></a>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn DataField="mobile" HeaderText="Phone" SortExpression="mobile"
                                                    UniqueName="mobile" FilterControlWidth="50">
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
