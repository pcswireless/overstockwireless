﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="Message-main.aspx.vb" Inherits="MessageBoard_Message_main" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">

    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                     args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
                else {
                    args.set_enableAjax(true);
                }
            }


           

        </script>
    </telerik:RadScriptBlock>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Message Board Listing</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; width: 40%; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    Please select the title to edit from below
                </div>
                <div style="float: right; width: 40%; text-align: right; margin-bottom: 3px;" id="div_add_new" runat="server">
                    <a style="text-decoration: none" href="/MessageBoard/NewMessage.aspx">
                        <img src="/images/add_new_message.gif" style="border: none" alt="Add New Message" /></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Vista">
                    <ClientEvents OnRequestStart="Cancel_Ajax" />
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="RadGrid1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                                <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="RadAjaxPanel1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
                    left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
                    <img id="Image8" src="/images/img_loading.gif" />
                </telerik:RadAjaxLoadingPanel>
                <asp:Label ID="msg" runat="server"></asp:Label>
                <telerik:RadGrid runat="server" ID="RadGrid1" DataSourceID="SqlDataSource1" AllowFilteringByColumn="True"
                    AllowSorting="True" PageSize="10" ShowFooter="False" ShowGroupPanel="True" AllowPaging="True"
                    AutoGenerateColumns="false" Skin="Vista" EnableLinqExpressions="false">
                    <PagerStyle Mode="NextPrevAndNumeric" />
                    <ExportSettings IgnorePaging="true" FileName="Message_Export" OpenInNewWindow="true"
                        ExportOnlyData="true" />
                    <ClientSettings AllowDragToGroup="true">
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="true" />
                    <MasterTableView DataKeyNames="message_board_id" AutoGenerateColumns="false" AllowFilteringByColumn="True"
                        ItemStyle-Height="40" AlternatingItemStyle-Height="40" CommandItemDisplay="Top">
                        <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false" ShowExportToPdfButton="false"
                            ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                        <NoRecordsTemplate>
                            No message to display
                        </NoRecordsTemplate>
                        <SortExpressions>
                            <telerik:GridSortExpression FieldName="title" SortOrder="Ascending" />
                        </SortExpressions>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Title" SortExpression="title" ShowSortIcon="true"
                                UniqueName="title" DataField="title" FilterControlWidth="200" GroupByExpression="title [Title] Group By title">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onclick="redirectIframe('','','/MessageBoard/NewMessage.aspx?i=<%# Eval("message_board_id") %>')">
                                        <%# Eval("title") %></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="display_for" HeaderText="Display For" SortExpression="display_for" ItemStyle-Width="200"
                                UniqueName="display_for" FilterControlWidth="150" AllowFiltering="true" HeaderStyle-Width="160">
                                <FilterTemplate>
                                    <telerik:RadComboBox ID="rad_combo_display_for" runat="server" AppendDataBoundItems="true"
                                      OnClientSelectedIndexChanged="DisplayForIndexChanged" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("display_for").CurrentFilterValue %>'>
                                        <Items>
                                            <telerik:RadComboBoxItem Value="" Text="All" />
                                            <telerik:RadComboBoxItem Value="For Bidders" Text="For Bidders" />
                                            <telerik:RadComboBoxItem Value="For Users" Text="For Users" />
                                            <telerik:RadComboBoxItem Value="For Bidders & Users" Text="For Bidders & Users" />
                                        </Items>
                                    </telerik:RadComboBox>
                                     <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">
                                        <script type="text/javascript">
                                            function DisplayForIndexChanged(sender, args) {
                                                var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                tableView.filter("display_for", args.get_item().get_value(), "EqualTo");

                                            }
                                        </script>
                                    </telerik:RadScriptBlock>
                                </FilterTemplate>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="status" HeaderText="Status" SortExpression="status"
                                ItemStyle-Width="150" HeaderStyle-Width="140" FilterControlWidth="100" AllowFiltering="true">
                                <FilterTemplate>
                                    <telerik:RadComboBox ID="rad_combo_status" runat="server" AppendDataBoundItems="true"
                                      OnClientSelectedIndexChanged="StatusIndexChanged" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("status").CurrentFilterValue %>'>
                                        <Items>
                                            <telerik:RadComboBoxItem Value="" Text="All" />
                                            <telerik:RadComboBoxItem Value="Running" Text="Running" />
                                            <telerik:RadComboBoxItem Value="Upcoming" Text="Upcoming" />
                                            <telerik:RadComboBoxItem Value="Expired" Text="Expired" />
                                        </Items>
                                    </telerik:RadComboBox>
                                     <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                        <script type="text/javascript">
                                            function StatusIndexChanged(sender, args) {
                                                var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                tableView.filter("status", args.get_item().get_value(), "EqualTo");

                                            }
                                        </script>
                                    </telerik:RadScriptBlock>
                                </FilterTemplate>
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="is_active" SortExpression="is_active" DataField="is_active"
                                HeaderText="Is Active" ItemStyle-Width="65" GroupByExpression="is_active [Is Active] Group By is_active">
                                 <FilterTemplate>
                                   
                                        <telerik:RadComboBox ID="RadComboBo_Chk_Active" runat="server" OnClientSelectedIndexChanged="StatusActiveIndexChanged"
                                             Width="120px"  SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("is_active").CurrentFilterValue %>' >
                                           <Items>
                                           <telerik:RadComboBoxItem Text="All" Value="" />
                                            <telerik:RadComboBoxItem Text="Active" Value="True" />
                                            <telerik:RadComboBoxItem Text="Inactive" Value="False" />
                                            </Items>
                                        </telerik:RadComboBox>
                                   
                                    <telerik:RadScriptBlock ID="RadScriptBlock_combo" runat="server">
                                         <script type="text/javascript">
                                             function StatusActiveIndexChanged(sender, args) {
                                                 var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                 if (args.get_item().get_value() == "") {
                                                     tableView.filter("is_active", args.get_item().get_value(), "NoFilter");
                                                 }
                                                 else {
                                                     tableView.filter("is_active", args.get_item().get_value(), "EqualTo");
                                                 }
                                             }
                                         </script>
                                    </telerik:RadScriptBlock>
                                </FilterTemplate>
                                <ItemTemplate>
                                    <img src='<%#IIf(Eval("is_active"),"/Images/true.gif","/Images/false.gif") %>' style="border: none;"
                                        alt="" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false"
                                ItemStyle-Width="50">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onclick="redirectIframe('','','/MessageBoard/NewMessage.aspx?i=<%# Eval("message_board_id") %>')">
                                        <img src="/images/edit.png" style="border: none;" alt="Edit" /></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="SqlDataSource1" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="select [message_board_id],[title],[description],[display_from],[display_to],[is_active],[submit_date],[submit_by_user_id],dbo.message_board_status(ISNULL(display_from,'1/1/1900'),ISNULL(display_to,'1/1/1900')) As status,case dbo.messageboard_display_for(message_board_id) when 1 then 'For Bidders' when 2 then 'For Users' when 3 then 'For Bidders & Users' else 'Not Decided' end AS display_for from [tbl_message_boards]"
                    runat="server"></asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
