﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    EnableEventValidation="false" CodeFile="NewMessage.aspx.vb" Inherits="MessageBoard_NewMessage"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/MessageBoard/UserControls/MessageInvitation.ascx" TagName="MessageInvitation"
    TagPrefix="uc" %>
<%@ Register Src="~/MessageBoard/UserControls/MessageInvitationSellers.ascx" TagName="MessageInvitationSellers"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <script type="text/javascript">

        function open_win_pop(_path, _id, _winname) {
            w1 = window.open(_path + '?i=' + _id, _winname, 'top=' + ((screen.height - 510) / 2) + ', left=' + ((screen.width - 800) / 2) + ', height=580, width=800,scrollbars=yes,toolbars=no,resizable=1;');
            w1.focus();
            return false;

        }

        function invited_bidders(id, mode) {
            window.open('/MessageBoard/InvitedBidderList.aspx?i=' + id + '&mode=' + mode, 'invited_bidder', 'left=' + ((screen.width - 800) / 2) + ',top=' + (0) + ',width=800,height=700,scrollbars=yes,resizable=no,toolbars=no');
            return false;
        }
    </script>
    <telerik:RadScriptBlock ID="RadScriptBlock_1" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax(sender, args) {

                args.set_enableAjax(false);

            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_edit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid2" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_basic_save">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid2" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_basic_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid2" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_basic_cancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid2" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div style="float: left;">
                    <div class="pageheading">
                        <asp:Literal ID="page_heading" runat="server" Text="New Message"></asp:Literal></div>
                </div>
                <div style="float: right; margin: 10px; padding-right: 20px;">
                    <a href="javascript:void(0);" onclick="javascript:open_help('6');">
                        <img src="/Images/help-button.gif" border="none" alt="" />
                    </a>
                </div>
            </td>
        </tr>
        <tr>
            <td align="left">
                <%--AutoPostBack="true" OnTabClick="RadTabStrip1_TabClick"--%>
                <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="" MultiPageID="RadMultiPage1"
                    SelectedIndex="0" Align="Left">
                    <Tabs>
                        <telerik:RadTab CssClass="auctiontabSelected">
                            <TabTemplate>
                                <span>
                                    <telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
                                        <%= IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), "Add Message Board Details", "Edit Message Board")%></telerik:RadScriptBlock>
                                </span>
                            </TabTemplate>
                        </telerik:RadTab>
                        <telerik:RadTab CssClass="auctiontab">
                            <TabTemplate>
                                <span>Assign Bidder</span>
                            </TabTemplate>
                        </telerik:RadTab>
                        <telerik:RadTab CssClass="auctiontab">
                            <TabTemplate>
                                <span>Assign User</span>
                            </TabTemplate>
                        </telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
            </td>
        </tr>
        <tr>
            <td class="PageTabContentEdit">
                <asp:HiddenField ID="hid_message_board_id" runat="server" Value="0" />
                <asp:HiddenField ID="hid_user_id" runat="server" Value="0" />
                <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" CssClass="multiPage">
                    <telerik:RadPageView ID="RadPageView1" runat="server">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td class="tdTabItem">
                                    <a name="1"></a>
                                    <asp:Panel ID="Panel1_Header1" runat="server" CssClass="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/basic_info.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            Message Board Detail
                                        </div>
                                    </asp:Panel>
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                        CssClass="TabGrid">
                                        <div style="padding-left: 5px;">
                                            <asp:Label ID="lbl_msg" runat="server" Text="" ForeColor="Red"></asp:Label>
                                        </div>
                                        <table cellpadding="0" cellspacing="2" border="0" width="838px">
                                            <tr>
                                                <td style="width: 145px;" class="caption">
                                                    Title&nbsp;<span id="span_title" runat="server" class="req_star">*</span>
                                                </td>
                                                <td style="width: 270px;" class="details">
                                                    <asp:TextBox ID="txt_title" runat="server" CssClass="inputtype"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="req_title" runat="server" ControlToValidate="txt_title"
                                                        ValidationGroup="a" Display="Dynamic" ErrorMessage="<br>Title required"></asp:RequiredFieldValidator>
                                                    <asp:Label ID="lbl_title" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 145px;" class="caption">
                                                    Is Active
                                                </td>
                                                <td style="width: 270px;" class="details">
                                                    <asp:CheckBox ID="chk_Isactive" runat="server" Checked="true"></asp:CheckBox>
                                                    <asp:Label ID="lbl_active" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Display from date&nbsp;<span id="span_from_date" runat="server" class="req_star">*</span>
                                                </td>
                                                <td class="details">
                                                    <telerik:RadDatePicker ID="RadDatePicker1" runat="server" ZIndex="30001" Width="210px"
                                                        Skin="Simple" DateInput-Enabled="false" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RadDatePicker1"
                                                        ValidationGroup="a" Display="Dynamic" ErrorMessage="<br>From date required"></asp:RequiredFieldValidator>
                                                    <asp:Label ID="lbl_datefrom" runat="server"></asp:Label>
                                                </td>
                                                <td class="caption">
                                                    Display to date&nbsp;<span id="span_to_date" runat="server" class="req_star">*</span>
                                                </td>
                                                <td class="details">
                                                    <telerik:RadDatePicker ID="RadDatePicker2" runat="server" ZIndex="30001" Width="210px"
                                                        Skin="Simple" DateInput-Enabled="false" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="RadDatePicker2"
                                                        ValidationGroup="a" Display="Dynamic" ErrorMessage="<br>To date required"></asp:RequiredFieldValidator>
                                                    <asp:Label ID="lbl_dateto" runat="server"></asp:Label>
                                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ValidationGroup="a" ControlToCompare="RadDatePicker2"
                                                        ControlToValidate="RadDatePicker1" Display="Dynamic" ErrorMessage="<br>To date must be later than from date."
                                                        Operator="LessThanEqual" Type="Date" ValueToCompare="<%= RadDatePicker2.SelectedDate %>"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Description&nbsp;<span id="span_desc" runat="server" class="req_star">*</span>
                                                </td>
                                                <td colspan="3" class="details">
                                                    <table cellspacing="0" cellpadding="0" runat="server" id="tbl" width="100%">
                                                        <tr>
                                                            <td style="text-align: left;">
                                                                <telerik:RadEditor runat="server" ID="RadEditor1" Height="515" Width="675" Skin="Sitefinity">
                                                                    <ImageManager ViewPaths="~/Editor/Img/UserDir/Marketing,~/Editor/Img/UserDir/PublicRelations"
                                                                        UploadPaths="~/Editor/Img/UserDir/Marketing,~/Editor/Img/UserDir/PublicRelations"
                                                                        DeletePaths="~/Editor/Img/UserDir/Marketing,~/Editor/Img/UserDir/PublicRelations">
                                                                    </ImageManager>
                                                                </telerik:RadEditor>
                                                            </td>
                                                            <td valign="top">
                                                                &nbsp;<asp:RequiredFieldValidator ID="Req_desp" runat="server" ControlToValidate="RadEditor1"
                                                                    ValidationGroup="a" Display="Dynamic" ErrorMessage="<br>&nbsp;Description required"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:Label ID="lbl_description" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </telerik:RadAjaxPanel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                    <asp:Panel ID="Panel1_Content1button" runat="server" CssClass="collapsePanel">
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel11" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                                            <asp:Panel ID="Panel1_Content1button_per" runat="server">
                                                <div class="addButton">
                                                    <asp:ImageButton ID="img_button_edit" ImageUrl="~/Images/edit.gif" runat="server" />
                                                    <asp:ImageButton ID="but_basic_save" ValidationGroup="a" runat="server" AlternateText="Save"
                                                        ImageUrl="/images/save.gif" />
                                                    <asp:ImageButton ID="but_basic_update" ValidationGroup="a" runat="server" AlternateText="Update"
                                                        ImageUrl="/images/update.gif" />
                                                </div>
                                                <div class="cancelButton" id="div_basic_cancel" runat="server">
                                                    <asp:ImageButton ID="but_basic_cancel" runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                                </div>
                                            </asp:Panel>
                                        </telerik:RadAjaxPanel>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView2" runat="server">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td class="tdTabItem">
                                    <div class="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/invetions.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            Assign Bidder
                                        </div>
                                    </div>
                                    <div style="padding: 10px;">
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                            CssClass="TabGrid">
                                            <uc:MessageInvitation ID="UC_MessageInvitation" runat="server" />
                                        </telerik:RadAjaxPanel>
                                    </div>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView3" runat="server">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td class="tdTabItem">
                                    <div class="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/invetions.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            Assign User
                                        </div>
                                    </div>
                                    <div style="padding: 10px;">
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel3" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                            CssClass="TabGrid">
                                            <uc1:MessageInvitationSellers ID="UC_MessageInvitationSellers" runat="server" />
                                        </telerik:RadAjaxPanel>
                                    </div>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
</asp:Content>
