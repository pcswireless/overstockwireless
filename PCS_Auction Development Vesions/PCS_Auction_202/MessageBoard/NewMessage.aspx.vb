﻿Imports Telerik.Web.UI
Imports System.Drawing
Partial Class MessageBoard_NewMessage
    Inherits System.Web.UI.Page
    Dim obj_com As New CommonCode

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Private Sub setPermission()
        If Not CommonCode.is_super_admin() Then
            Dim Vew_Message_Board As Boolean = False
            Dim Edit_Message_Board As Boolean = False
            Dim New_Message_Board As Boolean = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 23
                        Vew_Message_Board = True
                    Case 24
                        New_Message_Board = True
                    Case 25
                        Edit_Message_Board = True
                End Select
            Next
            dt = Nothing
            If Not Request.QueryString("i") = Nothing Then
                If Vew_Message_Board = False Then Response.Redirect("/NoPermission.aspx")
                If Not Edit_Message_Board Then
                    Panel1_Content1button_per.Visible = False
                End If
            Else
                If New_Message_Board = False Then Response.Redirect("/NoPermission.aspx")
            End If


        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lbl_msg.Text = ""
        If Not IsPostBack Then

            If Request.QueryString.Get("e") = "1" Then
                lbl_msg.Text = "New Message created successfully, Please invite some bidder or user to display this message on their corresponding deshboard."
            End If
            panelSet()
            If Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
                ViewState("message_board_id") = Request.QueryString.Get("i")
                img_button_edit.Visible = True
                but_basic_update.Visible = False
                'but_basic_cancel.Visible = False
                div_basic_cancel.Visible = False
                but_basic_save.Visible = False

            Else
                ViewState("message_board_id") = 0
                img_button_edit.Visible = False
                but_basic_update.Visible = False
                'but_basic_cancel.Visible = False
                div_basic_cancel.Visible = False
                but_basic_save.Visible = True

            End If
            If IsNumeric(Request.QueryString("i")) Then
                filldata("view")
            End If

        End If
    End Sub
    Private Sub panelSet()

        If Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
            RadTabStrip1.Tabs(0).Text = "Edit Message Board"
            hid_message_board_id.Value = Request.QueryString.Get("i")

            RadTabStrip1.Tabs(0).CssClass = "auctiontab"
            RadTabStrip1.Tabs(1).CssClass = "auctiontab"
            RadTabStrip1.Tabs(2).CssClass = "auctiontab"

            RadTabStrip1.Tabs(0).SelectedCssClass = "auctiontabSelected"
            RadTabStrip1.Tabs(1).SelectedCssClass = "auctiontabSelected"
            RadTabStrip1.Tabs(2).SelectedCssClass = "auctiontabSelected"
            Select Case Request.QueryString.Get("t")
                Case 1
                    RadTabStrip1.SelectedIndex = 0
                    RadMultiPage1.SelectedIndex = 0
                
            End Select
        Else
            RadTabStrip1.Tabs(1).Visible = False
            RadTabStrip1.Tabs(2).Visible = False
            RadTabStrip1.Tabs(0).Text = "New Message Board"
        End If
    End Sub
    Protected Sub filldata(ByVal mode As String)
        Dim str As String = ""
        Dim ds As New DataSet
        'lbl_msg.Text = ""

        str = "select [message_board_id],[title],[description],[display_from],[display_to],[is_active],[submit_date],[submit_by_user_id] from [tbl_message_boards] where message_board_id=" & ViewState("message_board_id")
        ds.Tables.Clear()
        ds = SqlHelper.ExecuteDataset(str)
        If ds.Tables(0).Rows.Count > 0 Then
            txt_title.Text = CommonCode.decodeSingleQuote(ds.Tables(0).Rows(0)("title").ToString)
            RadEditor1.Content = CommonCode.decodeSingleQuote(ds.Tables(0).Rows(0)("description").ToString)
            RadDatePicker1.SelectedDate = ds.Tables(0).Rows(0)("display_from")
            RadDatePicker2.SelectedDate = ds.Tables(0).Rows(0)("display_to")
            chk_Isactive.Checked = ds.Tables(0).Rows(0)("is_active")

            lbl_title.Text = CommonCode.decodeSingleQuote(ds.Tables(0).Rows(0)("title").ToString)
            page_heading.Text = lbl_title.Text
            lbl_description.Text = CommonCode.decodeSingleQuote(ds.Tables(0).Rows(0)("description").ToString)
            lbl_datefrom.Text = ds.Tables(0).Rows(0)("display_from")
            lbl_dateto.Text = ds.Tables(0).Rows(0)("display_to")
            If ds.Tables(0).Rows(0)("is_active") Then

                lbl_active.Text = "<img src='/Images/true.gif' alt=''>"
                chk_Isactive.Checked = True
            Else
                lbl_active.Text = "<img src='/Images/false.gif' alt=''>"

                chk_Isactive.Checked = False
            End If


        End If
        'If IsNumeric(Request.QueryString("i")) Then
        '    select_buyers()
        'End If
        ds.Dispose()
        ds = Nothing

        If mode = "edit" Then
            set_mode(True)
           
        Else
            set_mode(False)
        End If

    End Sub
  

    Private Sub set_mode(ByVal flag As Boolean)

        txt_title.Visible = flag
        tbl.Visible = flag
        span_desc.Visible = flag
        span_from_date.Visible = flag
        span_to_date.Visible = flag
        span_title.Visible = flag

        RadDatePicker1.Visible = flag
        RadDatePicker2.Visible = flag
        chk_Isactive.Visible = flag
      
        lbl_title.Visible = Not (flag)
        lbl_description.Visible = Not (flag)
        lbl_description.Visible = Not (flag)
        lbl_datefrom.Visible = Not (flag)
        lbl_dateto.Visible = Not (flag)
        lbl_active.Visible = Not (flag)
    End Sub

    Protected Sub but_basic_save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_save.Click
        If Page.IsValid Then
            Dim message_id As Integer = 0
            message_id = SqlHelper.ExecuteScalar("insert into tbl_message_boards (title,description,display_from,display_to,is_active,submit_date,submit_by_user_id) values('" & CommonCode.encodeSingleQuote(txt_title.Text) & "','" & CommonCode.encodeSingleQuote(RadEditor1.Content) & "','" & RadDatePicker1.SelectedDate & "','" & RadDatePicker2.SelectedDate & "','" & IIf(chk_Isactive.Checked, 1, 0) & "',getDate(),'" & CommonCode.Fetch_Cookie_Shared("user_id") & "') select scope_identity()")
            Response.Redirect("/MessageBoard/NewMessage.aspx?e=1&i=" & message_id)
        End If
    End Sub

    Protected Sub but_basic_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_update.Click
        If Page.IsValid Then
            Dim str As String = ""
            str = "update tbl_message_boards set title='" & CommonCode.encodeSingleQuote(txt_title.Text) & "',description='" & CommonCode.encodeSingleQuote(RadEditor1.Content) & "',display_from='" & RadDatePicker1.SelectedDate & "',display_to='" & RadDatePicker2.SelectedDate & "',is_active='" & IIf(chk_Isactive.Checked, 1, 0) & "',submit_date=GETDATE(),submit_by_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " where message_board_id=" & ViewState("message_board_id") & ""
            SqlHelper.ExecuteNonQuery(str)

           
            img_button_edit.Visible = True
            but_basic_update.Visible = False
            ' but_basic_cancel.Visible = False
            div_basic_cancel.Visible = False
            but_basic_save.Visible = False
            lbl_msg.Text = "Message board updated successfully."
            filldata("view")
        End If

    End Sub

    Protected Sub img_button_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_button_edit.Click
        'filldata("edit")

        img_button_edit.Visible = False
        but_basic_update.Visible = True
        'but_basic_cancel.Visible = True
        div_basic_cancel.Visible = True
        but_basic_save.Visible = False

        set_mode(True)
    End Sub
    Protected Sub Clear()

        txt_title.Text = ""
        RadEditor1.Content = ""
        'RadDatePicker1.SelectedDate = ""
        'RadDatePicker2.SelectedDate = ""
        chk_Isactive.Checked = False

    End Sub

    Protected Sub but_basic_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_cancel.Click
        filldata("view")
        img_button_edit.Visible = True
        but_basic_update.Visible = False
        'but_basic_cancel.Visible = False
        div_basic_cancel.Visible = False
        but_basic_save.Visible = False
    End Sub

End Class
