﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PrintConfirmation.aspx.vb"
    Inherits="PrintConfirmation" %>

<%@ Register Src="~/UserControls/order_confirm.ascx" TagName="Confirm" TagPrefix="Order" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Auctions</title><![if IE]>
    <link rel="stylesheet" type="text/css" href="/Style/font_ie.css" />
    <![endif]> <![if !IE]>
    <link rel="stylesheet" type="text/css" href="/Style/font_other.css" />
    <![endif]>
    <link type="text/css" rel="stylesheet" href="/Style/portalmain.css" />
    <script type="text/javascript">

        /***********************************************
        * Different CSS depending on OS (mac/pc)- © Dynamic Drive (www.dynamicdrive.com)
        * This notice must stay intact for use
        * Visit http://www.dynamicdrive.com/ for full source code
        ***********************************************/
        ///////No need to edit beyond here////////////

        var mactest = navigator.userAgent.indexOf("Mac") != -1

        //alert(navigator.userAgent);
        if (mactest) {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet_mac.css"/>');
        }
        else {
            document.write('<link rel="stylesheet" type="text/css" href="/style/newstylesheet.css"/>');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <script type='text/javascript'>        (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://www.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({ c: '16043132-05cd-4602-a161-b7c3e1339364', f: true }); done = true; } }; })();</script>
   <asp:ScriptManager ID="Scriptmanager1" runat="server"></asp:ScriptManager> <Order:Confirm runat="server" ID="UC_Ord_Cnf" />
    </form>
</body>
</html>
