﻿
Partial Class Bidder_Buckets
    Inherits System.Web.UI.Page
    Private rptVal As Repeater
    Private bucket_id As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            'Try
            pnl_request.Visible = True
            pnl_response.Visible = False
            lbl_popmsg_msg.Text = ""
            Dim buyer_id As Integer = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("b"))
            Dim dt As DataTable = SqlHelper.ExecuteDatatable("select contact_first_name,contact_last_name from tbl_reg_buyers WITH (NOLOCK) where buyer_id=" & buyer_id)
            If dt.Rows.Count > 0 Then
                ViewState("buyer_id") = buyer_id
                ltr_title.Text = "Manage bucket for " & dt.Rows(0).Item("contact_first_name") & " " & dt.Rows(0).Item("contact_last_name")
                rpt_bucket.DataSource = SqlHelper.ExecuteDataset("select bucket_id,bucket_name from tbl_master_buckets where is_active=1 and bucket_id in (select bucket_id from tbl_master_bucket_values where is_active=1) order by sno")
                rpt_bucket.DataBind()
            Else
                pnl_request.Visible = False
                pnl_response.Visible = True
                lbl_popmsg_msg.Text = "Invalid Request"
            End If

            'Catch ex As Exception
            '    pnl_request.Visible = False
            '    pnl_response.Visible = True
            '    lbl_popmsg_msg.Text = "Invalid Request"
            'End Try

        End If
    End Sub
    Protected Sub rpt_bucket_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_bucket.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            rptVal = CType(e.Item.FindControl("rpt_bucket_value"), Repeater)
            bucket_id = CType(e.Item.FindControl("lit_bucket_id"), Literal).Text
            rptVal.DataSource = SqlHelper.ExecuteDataset("select V.bucket_id,V.bucket_value_id,V.bucket_value,isnull(M.mapping_id,0) as mapping_id from tbl_master_bucket_values V left join tbl_reg_buyer_bucket_mapping M on V.bucket_value_id=M.bucket_value_id and M.buyer_id=" & ViewState("buyer_id") & " where V.is_active=1 and V.bucket_id=" & bucket_id & " order by V.bucket_value_id")
            rptVal.DataBind()
        End If
    End Sub
    Protected Sub rpt_bucket_value_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            If CType(e.Item.FindControl("ltr_mapping_id"), Literal).Text > 0 Then
                CType(e.Item.FindControl("chk_select"), CheckBox).Checked = True
            Else
                CType(e.Item.FindControl("chk_select"), CheckBox).Checked = False
            End If
        End If
    End Sub
    Protected Sub imgbtn_Update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgbtn_Update.Click
        Dim cnt As Integer
        Dim bucket_id As Integer
        Dim bucket_value_id As Integer
        Dim mapping_id As Integer
        Dim bucket_group(rpt_bucket.Items.Count - 1) As Boolean
        Dim bucket_group_anysel As Boolean = False
       
        For cnt = 0 To rpt_bucket.Items.Count - 1
            bucket_group(cnt) = False
            rptVal = CType(rpt_bucket.Items(cnt).FindControl("rpt_bucket_value"), Repeater)
            For j As Integer = 0 To rptVal.Items.Count - 1
                If CType(rptVal.Items(j).FindControl("chk_select"), CheckBox).Checked = True Then
                    bucket_group(cnt) = True 'any item from this bucket is selected
                    bucket_group_anysel = True 'any item from all buckets is selected
                    Exit For
                End If
            Next
        Next


        For cnt = 0 To rpt_bucket.Items.Count - 1
            rptVal = CType(rpt_bucket.Items(cnt).FindControl("rpt_bucket_value"), Repeater)
            If bucket_group(cnt) Then
                For j As Integer = 0 To rptVal.Items.Count - 1
                    If CType(rptVal.Items(j).FindControl("chk_select"), CheckBox).Checked = True Then

                        mapping_id = CType(rptVal.Items(j).FindControl("ltr_mapping_id"), Literal).Text
                        If mapping_id = 0 Then

                            bucket_id = CType(rptVal.Items(j).FindControl("lit_bucket_id"), Literal).Text
                            bucket_value_id = CType(rptVal.Items(j).FindControl("lit_bucket_value_id"), Literal).Text
                            SqlHelper.ExecuteNonQuery("INSERT INTO tbl_reg_buyer_bucket_mapping(buyer_id, bucket_id, bucket_value_id) VALUES (" & ViewState("buyer_id") & ", " & bucket_id & ", " & bucket_value_id & ")")

                        End If

                    Else

                        mapping_id = CType(rptVal.Items(j).FindControl("ltr_mapping_id"), Literal).Text
                        If mapping_id > 0 Then
                            SqlHelper.ExecuteNonQuery("DELETE FROM tbl_reg_buyer_bucket_mapping WHERE mapping_id=" & mapping_id)
                        End If

                    End If
                Next
            Else

                For j As Integer = 0 To rptVal.Items.Count - 1
                    If bucket_group_anysel Then   'select all in this bucket 
                        mapping_id = CType(rptVal.Items(j).FindControl("ltr_mapping_id"), Literal).Text
                        If mapping_id = 0 Then

                            bucket_id = CType(rptVal.Items(j).FindControl("lit_bucket_id"), Literal).Text
                            bucket_value_id = CType(rptVal.Items(j).FindControl("lit_bucket_value_id"), Literal).Text
                            SqlHelper.ExecuteNonQuery("INSERT INTO tbl_reg_buyer_bucket_mapping(buyer_id, bucket_id, bucket_value_id) VALUES (" & ViewState("buyer_id") & ", " & bucket_id & ", " & bucket_value_id & ")")

                        End If
                    Else 'delete all in this bucket

                        mapping_id = CType(rptVal.Items(j).FindControl("ltr_mapping_id"), Literal).Text
                        If mapping_id > 0 Then
                            SqlHelper.ExecuteNonQuery("DELETE FROM tbl_reg_buyer_bucket_mapping WHERE mapping_id=" & mapping_id)
                        End If


                    End If
                Next

            End If
        Next



        pnl_request.Visible = False
        pnl_response.Visible = True
        lbl_popmsg_msg.Text = "Update Successfully completed."
        If Not String.IsNullOrEmpty(Request.QueryString.Get("pop")) Then
            'Response.Write("<script>window.opener.refresh_buckets();window.close();</script>")
            'temp fix for staging  - IG
            Response.Write("<script>window.opener.refresh_buckets();  window.opener.location.reload();window.close();</script>")
        End If

    End Sub

    Protected Sub lnk_all_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_all.Click

        For cnt As Integer = 0 To rpt_bucket.Items.Count - 1

            rptVal = CType(rpt_bucket.Items(cnt).FindControl("rpt_bucket_value"), Repeater)
            For j As Integer = 0 To rptVal.Items.Count - 1
                CType(rptVal.Items(j).FindControl("chk_select"), CheckBox).Checked = True
            Next
        Next

    End Sub

    Protected Sub lnk_none_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_none.Click

        For cnt As Integer = 0 To rpt_bucket.Items.Count - 1
            rptVal = CType(rpt_bucket.Items(cnt).FindControl("rpt_bucket_value"), Repeater)
            For j As Integer = 0 To rptVal.Items.Count - 1
                CType(rptVal.Items(j).FindControl("chk_select"), CheckBox).Checked = False
            Next
        Next

    End Sub
End Class
