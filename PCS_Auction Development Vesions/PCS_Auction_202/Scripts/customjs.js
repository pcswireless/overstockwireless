﻿function CalculateTotal() {

    var txtAmount = document.getElementById('<%=txt_max_bid_amt.ClientID%>').get_value();
    var txtQuantity = document.getElementById('<%=hid_quantity.ClientID%>').get_value();
    var total

    alert(txtQuantity + " total =" + txtAmount);
    if ((txtAmount) && (parseInt(txtQuantity) > 1)) {

        total = parseInt(txtAmount) * parseInt(txtQuantity);
        document.getElementById('<%=txt_max_bid_amt.ClientID%>').value = total;

    }

}

function CalculatePerUnit() {

    var txtAmount = document.getElementById('<%=txt_max_bid_amt.ClientID%>').get_value();
    var txtQuantity = document.getElementById('<%=hid_quantity.ClientID%>').get_value();
    var perunit
    alert(txtQuantity + " total =" + txtAmount);
    if ((txtAmount) && (parseInt(txtQuantity) > 1)) {

        perunit = parseInt(txtAmount) / parseInt(txtQuantity);
        document.getElementById('<%=txt_max_bid_amt.ClientID%>').value = perunit;
    }

}
