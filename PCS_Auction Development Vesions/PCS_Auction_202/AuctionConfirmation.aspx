﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AuctionConfirmation.aspx.vb"
    MasterPageFile="~/AuctionMain.master" MaintainScrollPositionOnPostback="true"
    Inherits="AuctionConfirmation" %>

<%@ Register Src="~/UserControls/order_confirm.ascx" TagName="Confirm" TagPrefix="Order" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <script type='text/javascript'>        (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://www.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({ c: '16043132-05cd-4602-a161-b7c3e1339364', f: true }); done = true; } }; })();</script>
    
    <Order:Confirm runat="server" ID="UC_Ord_Cnf" />
    <script type="text/javascript">
        window.focus();
    </script>
</asp:Content>
