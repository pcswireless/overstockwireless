﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AuctionMain.master" AutoEventWireup="false"
    CodeFile="Unsubscription.aspx.vb" Inherits="Unsubscription" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <div style="padding-left: 20px; text-align: left;min-height:350px;padding-top:50px;">
        <h1>
            <asp:Literal ID="lit_heading" runat="server" Text="UNSUBSCRIPTION CONFIRM"></asp:Literal></h1>
        <table cellspacing="0" cellpadding="0" width="595px" border="0">
            <tr>
                <td style="padding-top: 15px;" align="left">
                    <asp:Literal ID="lbl_message" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
