﻿
Partial Class Users_bidder_wise_auction
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
            rpt_auction.DataSourceID = "SqlDataSource_Auction"
            rpt_auction.DataBind()
            If rpt_auction.Items.Count > 0 Then
                pnl_noItem.Visible = False
            End If
        Else
            hid_user_id.Value = 0
        End If
    End Sub

End Class
