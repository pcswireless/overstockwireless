﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PaypalRequest.aspx.vb" Inherits="ShoppingCart_PaypalRequest" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="myform1" action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <input type="hidden" name="cmd" value="_cart">
        <input type="hidden" name="upload" value="1">
        <input type="hidden" name="business" value="<%=SqlHelper.of_FetchKey("paypal_business_email") %>">
        
        <asp:Repeater ID="rpt_item" runat="server">
            <ItemTemplate>
                <input type="hidden" name="item_number_<%#Container.ItemIndex+1 %>" value='<%#Container.DataItem("code")%>'>
                <input type="hidden" name="item_name_<%#Container.ItemIndex+1 %>" value='<%#replace(Container.DataItem("title"),"'","`")%>'>
                <input type="hidden" name="amount_<%#Container.ItemIndex+1 %>" value='<%#FormatNumber(Container.DataItem("unit_price"), 2)%>'>
                <input type="hidden" name="quantity_<%#Container.ItemIndex+1 %>" value='<%#Container.DataItem("quantity")%>'>
              
            </ItemTemplate>
        </asp:Repeater>
        <input type="hidden" name="shipping_1" value="<%=FormatNumber(shipping_amount, 2)%>">
        <input type="hidden" name="tax_1" value="<%=FormatNumber(Tax_amount, 2)%>">
        <input type="hidden" name="invoice" value="<%=invoice_no %>">
        <%--<input type="hidden" name="paymentaction" value="authorization">--%>
        <input type="hidden" name="paymentaction" value="sale">
        <input type="hidden" name="currency_code" value="USD">
        <input type="hidden" name="lc" value="US">
        <input type="hidden" name="bn" value="PP-BuyNowBF">
        <input type="hidden" name="no_shipping" value="1">
        <input type="hidden" name="cn" value="Note to Seller">
        <input type="hidden" name="no_note" value="1">
        <input type="hidden" name="rm" value="2">
        <input type="hidden" name="cancel_return" value="<%response.write(SqlHelper.of_FetchKey("ServerHttp") & "/PaypalResponseCheckout.aspx")%>">
        <input type="hidden" name="image_url" value="<%response.write(SqlHelper.of_FetchKey("ServerHttp") & "/images/fend/logo.jpg" )%>">
        <input type="hidden" name="return" value="<%response.write(SqlHelper.of_FetchKey("ServerHttp") & "/PaypalResponseCheckout.aspx")%>">
    
    </form>
    <script type="text/javascript">
        document.getElementById("myform1").submit();
    </script>
    </body>
</html>

