Title      : Network:Intel 825xx Gigabit Platform LAN Network Device Driver
Version    : A00
OEM Name   : Intel
OEM Ver    : 11.8.81.0
Computers  : Latitude - E6320, E6420, E6420 ATG, E6520;
OptiPlex - 990, 790;
Precision - M4600, M6600, T1600
Oses       : Windows 7 32-bit Home Basic, Windows 7 32-bit Home Premium, Windows 7 32-bit Professional, Windows 7 32-bit Starter, Windows 7 32-bit Ultimate, Windows 7 64-bit Home Basic, Windows 7 64-bit Home Premium, Windows 7 64-bit Professional, Windows 7 64-bit Ultimate, Windows Vista 32-bit Business, Windows Vista 32-bit Home Basic, Windows Vista 32-bit Home PremiumWindows Vista 32-bit UltimateWindows Vista 64-bit BusinessWindows Vista 64-bit Home BasicWindows Vista 64-bit Home PremiumWindows Vista 64-bit UltimateWindows XP Home EditionWindows XP Media Center EditionWindows XP ProfessionalWindows XP Tablet PC EditionWindows XP x64 Professional Client
Languages  : Brazilian Portuguese, Chinese-S, Chinese-T, English, French, German, Italian, Japanese, Korean, Spanish
Created    : Mon May 16 14:00:59 CDT 2011
