﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="SalesHome.aspx.vb" Inherits="SalesHome" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <link rel="stylesheet" type="text/css" href="/Style/menu.css" />
    <telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
        <script type="text/javascript">
            function conditionalPostback1(e, sender) {

                sender.set_enableAjax(false);

            }
        </script>
    </telerik:RadScriptBlock>
    <div class="pageheading">
        Dashboard</div>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" ClientEvents-OnRequestStart="conditionalPostback1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
        ClientEvents-OnRequestStart="conditionalPostback1" EnableAJAX="false">
        <table cellspacing="0" cellpadding="0" border="0" width="988">
            <tr>
                <td>
                    <div>
                        <div style="width: 75px; float: left;">
                            <asp:Image runat="server" Height="72" Width="60" ID="img_sale_rep" />
                        </div>
                        <div style="width: 550px; float: left;">
                            <asp:Label runat="server" ID="lbl_sale_rep_name"></asp:Label><br />
                            <asp:Label runat="server" ID="lbl_sale_rep_email"></asp:Label><br />
                            <asp:Label runat="server" Font-Bold="true" ID="lbl_sale_rep_phone"></asp:Label><br />
                            <a style="color: #243E5A; text-decoration: none;" href="javscript:void(0);" onclick="return open_pop_query('','');">
                                <asp:Label runat="server" ForeColor="#363A83" Font-Bold="true" ID="lbl_sale_rep_query"></asp:Label></a>
                        </div>
                        <div style="width: 200px; float: left; text-align: left; padding-top: 0;" class="topR1">
                            <div class="l2" style="">
                                <asp:TextBox runat="server" Text="Search" ID="txt_search" AutoPostBack="true" ValidationGroup="sale_search"
                                    onfocus="return clearBox();" onblur="return lostFocus()" onkeydown="return on_key_down(event)"
                                    CssClass="searchtextbox" />
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hid_user_id" runat="server" Value="0" />
                    <asp:HiddenField ID="hd_auction_id" runat="server" Value="0" />
                    <asp:HiddenField ID="hid_search" runat="server" Value="%%" />
                    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server" />
                    <br />
                    <br />
                    <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                        AllowMultiRowSelection="False" AllowPaging="True" PageSize="20" GridLines="None"
                        ShowGroupPanel="false" OnNeedDataSource="RadGrid1_NeedDataSource">
                        <PagerStyle Mode="NumericPages"></PagerStyle>
                        <MasterTableView DataKeyNames="Auction_id" AllowMultiColumnSorting="false">
                            <NestedViewTemplate>
                                <asp:Panel runat="server" ID="InnerContainer" Visible="false">
                                    <telerik:RadTabStrip runat="server" ID="TabStip1" MultiPageID="Multipage1" SelectedIndex="0">
                                        <Tabs>
                                            <telerik:RadTab runat="server" Text="Bidder" PageViewID="PageView1">
                                            </telerik:RadTab>
                                        </Tabs>
                                    </telerik:RadTabStrip>
                                    <telerik:RadMultiPage runat="server" ID="Multipage1" SelectedIndex="0" RenderSelectedPageOnly="false">
                                        <telerik:RadPageView runat="server" ID="PageView1">
                                            <asp:Label ID="Label1" Font-Bold="true" Font-Italic="true" Text='<%# Eval("auction_id") %>'
                                                Visible="false" runat="server" />
                                            <telerik:RadGrid runat="server" ID="RadGrid_Bidder" ShowFooter="true" AllowSorting="true"
                                                DataSourceID="SqlDataSource3" EnableLinqExpressions="false">
                                                <MasterTableView ShowHeader="true" AutoGenerateColumns="False" AllowPaging="true"
                                                    DataKeyNames="buyer_id" PageSize="5">
                                                    <Columns>
                                                        <telerik:GridBoundColumn SortExpression="name" HeaderText="Name" DataField="name"
                                                            UniqueName="name" ItemStyle-Width="20%">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn SortExpression="email" HeaderText="Email" DataField="email"
                                                            UniqueName="email" ItemStyle-Width="20%">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn SortExpression="phone1" HeaderText="Phone" DataField="phone1"
                                                            UniqueName="phone1" ItemStyle-Width="20%">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Status" ItemStyle-Width="40%">
                                                            <ItemTemplate>
                                                                <div style="padding: 5px; text-align: left; padding-left: 0px;">
                                                                    <iframe id="iframe_rank" src='/buyer_rank_query.aspx?a=<%# Eval("auction_id") %>&b=<%# Eval("buyer_id") %>'
                                                                        scrolling="no" frameborder="0" width="170px" height="50px"></iframe>
                                                                </div>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="SqlDataSource3" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                                ProviderName="System.Data.SqlClient" SelectCommand="select B.buyer_id,isnull(B.company_name,'') as company_name,
(b.contact_first_name + ' ' + b.contact_last_name) as name,
@auction_id as auction_id,isnull(email,'') as email,isnull(mobile,0) as phone1,
(select count(Q.query_id) As query_num from tbl_auction_queries Q 
where Q.auction_id=@auction_id and isnull(Q.admin_marked,0)=0 and 
Q.buyer_id=B.buyer_id and not exists(select query_id from tbl_auction_queries where isnull(parent_query_id,0)=Q.query_id) 
and ISNULL(parent_query_id,0)=0) as query_count from tbl_reg_buyers B 
inner join tbl_reg_sale_rep_buyer_mapping S on B.buyer_id=S.buyer_id 
where (select top 1 buyer_id from tbl_auction_queries where auction_id=@auction_id and buyer_id=B.buyer_id)=B.buyer_id and
S.user_id=@user_id and (select auction_id from tbl_auctions WITH (NOLOCK) where auction_id=@auction_id and is_active=1)=@auction_id and (b.contact_first_name like @bidder_search or b.contact_last_name
 like @bidder_search)"
                                                runat="server">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="Label1" PropertyName="Text" Type="String" Name="auction_id" />
                                                    <asp:ControlParameter ControlID="hid_user_id" PropertyName="Value" Type="String"
                                                        Name="user_id" />
                                                    <asp:ControlParameter ControlID="hid_search" PropertyName="Value" Type="String" Name="bidder_search" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </telerik:RadPageView>
                                    </telerik:RadMultiPage>
                                </asp:Panel>
                            </NestedViewTemplate>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="title" SortOrder="Ascending" />
                            </SortExpressions>
                            <Columns>
                                <telerik:GridBoundColumn DataField="title" HeaderText="Auction Title" UniqueName="Title"
                                    SortExpression="title" ItemStyle-Width="31%">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="Query Pending" ItemStyle-Width="30%">
                                    <ItemTemplate>
                                        <div style="width: 150px; padding-left: 6px; color: #243E5A; font-weight: bold; font-size: 13px;
                                            float: left;">
                                            <a style="color: #243E5A; text-decoration: none;" href="javscript:void(0);" onclick="return open_pop_query('<%# Eval("auction_id") %>','');">
                                                <asp:Literal ID="lit_query_pending" Text='<%# Eval("query_count") & " Pending Query" %>'
                                                    runat="server"></asp:Literal></a>
                                        </div>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Timer" ItemStyle-Width="39%">
                                    <ItemTemplate>
                                        <div style="width: 320px; padding-left: 0px; float: left;">
                                            <iframe id="iframe_timer" style="font-size: 14px;" src='/timerframe.aspx?i=<%# Eval("auction_id") %>'
                                                scrolling="no" frameborder="0" width="200px" height="25px"></iframe>
                                        </div>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings AllowDragToGroup="true" />
                    </telerik:RadGrid>
                    <asp:SqlDataSource ID="SqlDataSource_Auctions" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                        ProviderName="System.Data.SqlClient" SelectCommand="" runat="server"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnl_noItem" runat="server">
                        <div style="border: 1px solid #10AAEA; background: url('/Images/fend/grdnt_template.jpg') repeat-x;
        height: 112px; padding-top: 25px; text-align: center;">
                            <b>Auction Not available in this section</b>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function lostFocus() {
                if (document.getElementById("<%=txt_search.ClientID %>").value == '') {
                    document.getElementById("<%=txt_search.ClientID %>").value = 'Search';
                    return false;
                }
                else
                    return true;
            }


            function clearBox() {
                if (document.getElementById("<%=txt_search.ClientID %>").value == 'Search')
                    document.getElementById("<%=txt_search.ClientID %>").value = '';
                return false;
            }

            function open_pop_query(aid, bid) {
                if (aid != '' & bid != '') {
                    w1 = window.open('/Backend_Dashbord_qry.aspx' + '?q=0&a=' + aid + '&b=' + bid, '_assign18', 'top=' + ((screen.height - 450) / 2) + ', left=' + ((screen.width - 550) / 2) + ', height=400, width=550,scrollbars=yes,toolbars=no,resizable=1;');
                    w1.focus();
                    return false;
                }
                else if (aid != '') {
                    w1 = window.open('/Backend_Dashbord_qry.aspx' + '?q=0&a=' + aid, '_assign18', 'top=' + ((screen.height - 450) / 2) + ', left=' + ((screen.width - 550) / 2) + ', height=400, width=550,scrollbars=yes,toolbars=no,resizable=1;');
                    w1.focus();
                    return false;
                }
                else {
                    w1 = window.open('/Backend_Dashbord_qry.aspx', '_assign18', 'top=' + ((screen.height - 450) / 2) + ', left=' + ((screen.width - 550) / 2) + ', height=400, width=550,scrollbars=yes,toolbars=no,resizable=1;');
                    w1.focus();
                    return false;
                }

            }
        </script>
    </telerik:RadScriptBlock>
</asp:Content>
