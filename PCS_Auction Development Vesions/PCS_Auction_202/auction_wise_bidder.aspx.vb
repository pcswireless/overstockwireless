﻿
Partial Class Users_auction_wise_bidder
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            txt_search.Text = ""
            If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
                Dim ds As New DataSet

                ds = SqlHelper.ExecuteDataset("select (first_name + ' ' + last_name) as name,email,isnull(mobile,0) as phone1,isnull(image_path,'') as image_path,(select count(query_id) from tbl_auction_queries where parent_query_id=0 and sales_rep_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " and query_id not in (select parent_query_id from tbl_auction_queries)) as query from tbl_sec_users where user_id=" & hid_user_id.Value & "")
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0)("image_path") <> "" Then
                        img_sale_rep.ImageUrl = "/Upload/users/" & hid_user_id.Value & "/" & ds.Tables(0).Rows(0)("image_path")
                    Else
                        img_sale_rep.ImageUrl = "/images/buyer_icon.gif"
                    End If
                    lbl_sale_rep_name.Text = "<a href='javascript:void(0);' style='color:#4B708D;text-decoration:none;font-size:14px;font-weight:bold;' onmouseout=""hide_tip_new();"" onmouseover=""tip_new('/users/user_mouse_over.aspx?i=" & hid_user_id.Value & "','270','white','true');"">" & ds.Tables(0).Rows(0)("name") & "</a>"
                    'Response.Output.Write(lbl_sale_rep_name.Text)
                    lbl_sale_rep_email.Text = "<a style='color:#363A83;text-decoration:none;font-size:13px;font-weight:bold;' href='mailto:" & ds.Tables(0).Rows(0)("email") & "'>" & ds.Tables(0).Rows(0)("email") & "</a>"
                    lbl_sale_rep_query.Text = ds.Tables(0).Rows(0)("query") & " Pending Queries"
                    lbl_sale_rep_phone.Text = "Ph: " & ds.Tables(0).Rows(0)("phone1")



                    SqlDataSource_Auctions.SelectCommand = "Select distinct(A.auction_id) as auction_id,'' as srh,isnull(A.title,'') as title,(select count(Q.query_id) As query_num from tbl_auction_queries Q where Q.auction_id=A.auction_id and Q.buyer_id in (select B1.buyer_id from tbl_reg_buyers B1 WITH (NOLOCK) inner join tbl_reg_sale_rep_buyer_mapping S1 on B1.buyer_id=S1.buyer_id and S1.user_id=" & hid_user_id.Value & ") and ISNULL(parent_query_id,0)=0 and not exists(select query_id from tbl_auction_queries where parent_query_id=Q.query_id)) as query_count " & _
                         "from tbl_auctions A WITH (NOLOCK) where isnull(A.discontinue,0)=0 and  A.display_end_time >=getdate() and" & _
 " (1=(case when (A.show_relevant_bidders_only)=1 then (case when (select count(*) from dbo.fn_get_invited_buyers(A.auction_id)" & _
  " where buyer_id in (select S.buyer_id from tbl_reg_sale_rep_buyer_mapping S where S.user_id=" & hid_user_id.Value & "))>0 then 1 else (case when isnull((select count(*) from tbl_auction_invitation_filter_values where auction_id=A.auction_id),0)=0 then 1 else 0 end) end) else 1 end)) and A.is_active=1 and A.auction_type_id in (2,3)"

                End If
                rpt_parent_auction.DataSourceID = "SqlDataSource_Auctions"
                rpt_parent_auction.DataBind()
                If rpt_parent_auction.Items.Count > 0 Then
                    pnl_noItem.Visible = False
                End If



            Else
                hid_user_id.Value = 0
            End If
        End If
    End Sub

    Protected Sub rpt_parent_auction_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_parent_auction.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim dt_list As New DataList
            dt_list = CType(e.Item.FindControl("rpt_bidders"), DataList)
            Dim sel As String = "select B.buyer_id,isnull(B.company_name,'') as company_name,(contact_first_name + ' ' + contact_last_name) as name," & CType(e.Item.DataItem, DataRowView).Item("auction_id") & " as auction_id,isnull(email,'') as email,isnull(mobile,0) as phone1,isnull((select count(Q.query_id) As query_num from tbl_auction_queries Q where Q.auction_id=" & CType(e.Item.DataItem, DataRowView).Item("auction_id") & " and Q.buyer_id=B.buyer_id and ISNULL(parent_query_id,0)=0 and not exists(select query_id from tbl_auction_queries where parent_query_id=Q.query_id)),0) as query_count " & _
    ",isnull((select dbo.buyer_bid_rank(" & CType(e.Item.DataItem, DataRowView).Item("auction_id") & ",B.buyer_id)),0) as rank,isnull((select max(bid_amount) from tbl_auction_bids WITH (NOLOCK) where buyer_id=B.buyer_id and auction_id =" & CType(e.Item.DataItem, DataRowView).Item("auction_id") & "),0) as amount from tbl_reg_buyers B WITH (NOLOCK) inner join tbl_reg_sale_rep_buyer_mapping S on B.buyer_id=S.buyer_id where (1=(select case when A.show_relevant_bidders_only=0 then 1 else " & _
   "(case when (select dbo.is_buyer_invited(A.auction_id,B.buyer_id))=1 then 1 else 0 end) end from tbl_auctions A WITH (NOLOCK)  where A.auction_id=" & CType(e.Item.DataItem, DataRowView).Item("auction_id") & ")) and S.user_id=" & hid_user_id.Value & ""
            dt_list.DataSource = SqlHelper.ExecuteDataTable(sel)
            dt_list.DataBind()


        End If
    End Sub

    Protected Sub fun_search(ByVal srh As String)
        'Dim srh As String = ""
        'Dim ds As New DataSet
        'ds = SqlHelper.ExecuteDataset("select (first_name + ' ' + last_name) as name,email,isnull(mobile,0) as phone1,isnull(image_path,'') as image_path,(select count(query_id) from tbl_auction_queries where parent_query_id=0 and sales_rep_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " and query_id not in (select parent_query_id from tbl_auction_queries)) as query from tbl_sec_users where user_id=" & hid_user_id.Value & "")
        SqlDataSource_Auctions.SelectCommand = "Select distinct(A.auction_id) as auction_id,'" & srh.Replace("'", "") & "' as srh,isnull(A.title,'') as title,(select count(Q.query_id) As query_num from tbl_auction_queries Q where Q.auction_id=A.auction_id and Q.buyer_id in (select B1.buyer_id from tbl_reg_buyers B1 WITH (NOLOCK) inner join tbl_reg_sale_rep_buyer_mapping S1 on B1.buyer_id=S1.buyer_id and S1.user_id=" & hid_user_id.Value & ") and ISNULL(parent_query_id,0)=0 and not exists(select query_id from tbl_auction_queries where parent_query_id=Q.query_id)) as query_count" & _
                        " from tbl_auctions A WITH (NOLOCK) where isnull(A.discontinue,0)=0 and A.display_end_time >=getdate() and A.is_active=1 and A.auction_type_id in (2,3) and" & _
 " (1=(case when (A.show_relevant_bidders_only)=1 then (case when (select count(*) from dbo.fn_get_invited_buyers(A.auction_id ) where buyer_id in (select S.buyer_id from tbl_reg_sale_rep_buyer_mapping S" & _
 " where S.user_id=" & hid_user_id.Value & "))>0 then 1 else (case when isnull((select count(*) from tbl_auction_invitation_filter_values where auction_id=A.auction_id),0)=0 then 1 else 0 end) end) else 1 end)) and (A.title like '%" & srh.Replace("'", "") & "%' or ((select count(*) from tbl_reg_buyers B WITH (NOLOCK) inner join tbl_reg_sale_rep_buyer_mapping S on B.buyer_id=S.buyer_id where dbo.is_buyer_invited(A.auction_id,B.buyer_id)=1 and S.user_id=" & hid_user_id.Value & " and (B.company_name like '%" & srh.Replace("'", "") & "%' or B.contact_first_name like '%" & srh.Replace("'", "") & "%' or B.contact_last_name like '%" & srh.Replace("'", "") & "%'))>0))"
        'Response.Write("<br><br>" & SqlDataSource_Auctions.SelectCommand)
        'Exit Sub
        rpt_parent_auction.DataSource = Nothing
        rpt_parent_auction.DataBind()

        rpt_parent_auction.DataSourceID = "SqlDataSource_Auctions"
        rpt_parent_auction.DataBind()
    End Sub

    Protected Sub txt_search_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_search.TextChanged
        'If Page.IsValid Then
        If txt_search.Text.Trim <> "" AndAlso txt_search.Text.Trim.ToLower <> "search" Then
            fun_search(txt_search.Text.Trim)
        Else
            fun_search(String.Empty)
        End If
        'End If

    End Sub
End Class
