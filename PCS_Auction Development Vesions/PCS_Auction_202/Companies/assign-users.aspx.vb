﻿Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Linq

Partial Class Companies_assign_users
    Inherits System.Web.UI.Page
    Shared dv_list As List(Of Child)
    Shared or_list As List(Of Parent)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            If IsNumeric(Request.QueryString.Get("i")) Then
                hd_seller_id.Value = Request.QueryString.Get("i")
            Else
                hd_seller_id.Value = 0
            End If
        End If
    End Sub


    'Protected Sub UseDragColumnCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    grdParent.MasterTableView.GetColumn("DragDropColumn").Visible = UseDragColumnCheckBox.Checked
    '    grdParent.Rebind()

    '    grdChild.MasterTableView.GetColumn("DragDropColumn").Visible = UseDragColumnCheckBox.Checked
    '    grdChild.Rebind()
    'End Sub


    Protected Property PendingParents_VB() As IList(Of Parent)
        Get
            Return DirectCast(ViewState("_ParentView_State"), IList(Of Parent))
        End Get
        Set(value As IList(Of Parent))
            ViewState("_ParentView_State") = value
        End Set
    End Property



    Protected Property ShippedParents_VB() As IList(Of Child)
        Get
            Return DirectCast(ViewState("_ChildView_State"), IList(Of Child))
        End Get
        Set(value As IList(Of Child))
            ViewState("_ChildView_State") = value
        End Set
    End Property

    'Protected Property ParentStore_DT() As DataTable
    '    Get
    '        Try
    '            'Response.Write("<script>alert('" & PendingParents_VB.Rows.Count.ToString & "')</script>")
    '            Dim obj As Object = PendingParents_VB()

    '            If obj Is Nothing Then
    '                obj = GetParents()
    '                PendingParents_VB = obj
    '            End If
    '            Return DirectCast(obj, DataTable)
    '        Catch ex As Exception
    '            PendingParents_VB = Nothing
    '        End Try
    '        Return New List(Of Parent)
    '    End Get
    '    Set(ByVal value As IList(Of Parent))
    '        PendingParents_VB = value
    '    End Set
    'End Property
    Protected Property ParentStore() As IList(Of Parent)
        Get
            Try
                'Response.Write("<script>alert('" & PendingParents_VB.Rows.Count.ToString & "')</script>")
                'Dim obj As Object = Session("PendingParents_VB") 'PendingParents_VB()
                Dim obj As New Object
                If or_list IsNot Nothing Then
                    obj = or_list ' (From li In or_list Where li.IsVisible = True Select li).ToList() ').FirstOrDefault, IList(Of Parent))
                Else
                    obj = Nothing
                End If

                If obj Is Nothing Then
                    obj = GetParents()
                    or_list = obj
                    'PendingParents_VB = obj
                End If
                Return obj ' DirectCast(obj, IList(Of Parent))
            Catch ex As Exception
                or_list = Nothing
                ' PendingParents_VB = Nothing
            End Try
            Return New List(Of Parent)
        End Get
        Set(ByVal value As IList(Of Parent))
            or_list = value ' PendingParents_VB = value
        End Set
    End Property

    Protected Property ChildStore() As IList(Of Child)
        Get
            Try
                'Dim it=From li In dv_list Where li.
                Dim obj As New Object
                If dv_list IsNot Nothing Then
                    obj = dv_list ' (From li In dv_list Where li.IsVisible = True Select li).ToList()
                Else
                    obj = Nothing
                End If

                If obj Is Nothing Then
                    obj = GetChild()
                    dv_list = obj
                    ' ShippedParents_VB = obj
                End If
                Return obj
            Catch ex As Exception
                dv_list = Nothing
                ' ShippedParents_VB = Nothing
            End Try
            Return New List(Of Child)
        End Get
        Set(ByVal value As IList(Of Child))
            dv_list = value '  ShippedParents_VB = value
        End Set
    End Property

    Protected Sub grdParent_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        grdParent.DataSource = (From li In ParentStore Where li.IsVisible = True Select li).ToList
    End Sub

    Protected Function GetParents() As IList(Of Parent)
        Dim results As IList(Of Parent) = New List(Of Parent)()
        Using connection As SqlConnection = New SqlConnection(SqlHelper.of_getConnectString) ' DbProviderFactories.GetFactory("System.Data.SqlClient").CreateConnection()
            'connection.ConnectionString = ConfigurationManager.ConnectionStrings("telerik").ConnectionSt
            Dim biddery_qry As New StringBuilder
            biddery_qry.Append("Declare @count Int ")
            biddery_qry.Append("Declare @i Int ")
            biddery_qry.Append("Declare @temp varchar (100) ")
            biddery_qry.Append("Declare @name varchar(max)='' ")
            biddery_qry.Append("Declare @user_id Int ")
            biddery_qry.Append("DECLARE @TempRecords TABLE (MyID int IDENTITY(1,1) NOT NULL ,name varchar(100) NOT NULL) ")
            biddery_qry.Append("INSERT INTO @TempRecords SELECT name from tbl_sec_user_profile_mapping A1 inner join tbl_sec_profiles A2 on A1.profile_id=A2.profile_id ")
            biddery_qry.Append("inner join tbl_sec_users A3 on A3.user_id=A1.user_id where A3.user_id not in (select A4.user_id from tbl_reg_seller_user_mapping A4 where A4.seller_id=" & hd_seller_id.Value & ") ")
            biddery_qry.Append(" UNION SELECT name from tbl_sec_user_profile_mapping A8 inner join tbl_sec_profiles A9 on A8.profile_id=A9.profile_id ")
            biddery_qry.Append("inner join tbl_sec_users A10 on A10.user_id=A8.user_id where A10.user_id in (select A11.user_id from tbl_reg_seller_user_mapping A11 where A11.seller_id=" & hd_seller_id.Value & ") ")
            biddery_qry.Append("SET @count = @@rowcount ")
            biddery_qry.Append("SET @i = 1 ")
            biddery_qry.Append("WHILE @i <= @count ")
            biddery_qry.Append("BEGIN ")
            biddery_qry.Append("Select @temp=name from @TempRecords where MyID = @i ")
            biddery_qry.Append("set @name=@name+@temp+', ' ")
            biddery_qry.Append("set @i = @i + 1 End ")
            biddery_qry.Append("if @count<1 ")
            biddery_qry.Append("set @name=',' ")
            biddery_qry.Append("SET @name = LEFT(@name, LEN(@name) - 1) ")
            biddery_qry.Append("SELECT  B.user_id, isnull((B.first_name + B.last_name),'') as name,@name as profile ,B.email,visible=1 ")
            biddery_qry.Append("FROM  tbl_sec_users B where B.user_id not in (select A.user_id from tbl_reg_seller_user_mapping A where A.seller_id=" & hd_seller_id.Value & ") UNION ")
            biddery_qry.Append(" SELECT  B1.user_id, isnull((B1.first_name + B1.last_name),'') as name,@name as profile ,B1.email,visible=0 ")
            biddery_qry.Append("FROM  tbl_sec_users B1 where B1.user_id in (select B2.user_id from tbl_reg_seller_user_mapping B2 where B2.seller_id=" & hd_seller_id.Value & ")")
            'Response.Write("<br>select => " & biddery_qry.ToString)
            Using command As SqlCommand = connection.CreateCommand()
                command.CommandText = biddery_qry.ToString

                connection.Open()
                Try
                    Dim reader As SqlDataReader = command.ExecuteReader()
                    While reader.Read()

                        'Response.End()
                        Dim user_id As Integer = reader("user_id") ' DirectCast(reader.GetValue(reader.GetOrdinal("ParentID")), Integer)
                        Dim name As String = reader("name") 'IIf((Not reader.IsDBNull(reader.GetOrdinal("CustomerID"))), DirectCast(reader.GetValue(reader.GetOrdinal("CustomerID")), String), String.Empty)
                        Dim profile As String = reader("profile") ' IIf((Not reader.IsDBNull(reader.GetOrdinal("RequiredDate"))), DirectCast(reader.GetValue(reader.GetOrdinal("RequiredDate")), Date), Date.MinValue)
                        Dim email As String = reader("email")
                        Dim isvisible As Boolean = reader("Visible")
                        results.Add(New Parent(user_id, name, profile, email, isvisible))

                    End While
                Catch ex As SqlException
                    ' Response.Write(ex.Message)
                    'results.Clear()
                End Try
            End Using
        End Using
        Return results
    End Function


    Protected Function GetChild() As IList(Of Child)
        Dim results As IList(Of Child) = New List(Of Child)()
        ' Using connection As SqlConnection = New SqlConnection(SqlHelper.of_getConnectString) ' DbProviderFactories.GetFactory("System.Data.SqlClient").CreateConnection()
        'connection.ConnectionString = ConfigurationManager.ConnectionStrings("telerik").ConnectionString
        'Dim biddery_qry As New StringBuilder
        'biddery_qry.Append("Declare @count Int ")
        'biddery_qry.Append("Declare @i Int ")
        'biddery_qry.Append("Declare @temp varchar (100) ")
        'biddery_qry.Append("Declare @name varchar(max)='' ")
        'biddery_qry.Append("Declare @user_id Int ")
        'biddery_qry.Append("DECLARE @TempRecords TABLE (MyID int IDENTITY(1,1) NOT NULL ,name varchar(100) NOT NULL) ")
        'biddery_qry.Append("INSERT INTO @TempRecords SELECT name from tbl_sec_user_profile_mapping A1 inner join tbl_sec_profiles A2 on A1.profile_id=A2.profile_id ")
        'biddery_qry.Append("inner join tbl_reg_seller_user_mapping A3 on A3.user_id=A1.user_id where A3.seller_id=" & hd_seller_id.Value & "")
        'biddery_qry.Append("SET @count = @@rowcount ")
        'biddery_qry.Append("SET @i = 1 ")
        'biddery_qry.Append("WHILE @i <= @count ")
        'biddery_qry.Append("BEGIN ")
        'biddery_qry.Append("Select @temp=name from @TempRecords where MyID = @i ")
        'biddery_qry.Append("set @name=@name+@temp+',' ")
        'biddery_qry.Append("set @i = @i + 1 End ")
        'biddery_qry.Append("if @count<1 ")
        'biddery_qry.Append("set @name=',' ")
        'biddery_qry.Append("SET @name = LEFT(@name, LEN(@name) - 1) ")
        'biddery_qry.Append("SELECT  B.user_id, isnull((B.first_name + B.last_name),'') as name,@name as profile ,B.email,visible=1 ")
        'biddery_qry.Append("FROM  tbl_sec_users B inner join tbl_reg_seller_user_mapping A on B.user_id=A.user_id where A.seller_id=" & hd_seller_id.Value & "")
        '  Response.Write("<br><br><br>selected => " & biddery_qry.ToString)
        ' Using command As SqlCommand = connection.CreateCommand()

        ' Command.CommandText = biddery_qry.ToString

        ' connection.Open()
        Try
            For Each reader As Parent In (From li In ParentStore Where li.IsVisible = False Select li).ToList
                ' While reader.Read()
                Dim user_id As Integer = reader.User_Id ' DirectCast(reader.GetValue(reader.GetOrdinal("ParentID")), Integer)
                Dim name As String = reader.Name 'IIf((Not reader.IsDBNull(reader.GetOrdinal("CustomerID"))), DirectCast(reader.GetValue(reader.GetOrdinal("CustomerID")), String), String.Empty)
                Dim profile As String = reader.Profile ' IIf((Not reader.IsDBNull(reader.GetOrdinal("RequiredDate"))), DirectCast(reader.GetValue(reader.GetOrdinal("RequiredDate")), Date), Date.MinValue)
                Dim email As String = reader.Email
                Dim isvisible As Boolean = Not reader.IsVisible
                results.Add(New Child(user_id, name, profile, email, isvisible))

                ' End While
            Next
        Catch ex As SqlException
            results.Clear()

        End Try
        ' End Using
        ' End Using
        Return results
    End Function
    Protected Sub grdChild_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        grdChild.DataSource = (From li In ChildStore Where li.IsVisible = True).ToList
    End Sub

    Protected Sub grdParent_RowDrop(ByVal sender As Object, ByVal e As GridDragDropEventArgs)
        ' Response.Write("<br>from shipped to pending grid")
        If String.IsNullOrEmpty(e.HtmlElement) Then
            If e.DraggedItems(0).OwnerGridID = grdParent.ClientID Then
                ' items are drag from pending to shipped grid
                If (e.DestDataItem Is Nothing AndAlso ChildStore.Count = 0) OrElse (e.DestDataItem IsNot Nothing AndAlso e.DestDataItem.OwnerGridID = grdChild.ClientID) Then
                    Dim ChildView As IList(Of Child)
                    Dim ParentView As IList(Of Parent)

                    ChildView = ChildStore
                    ParentView = ParentStore

                    Dim destinationIndex As Int32 = -1
                    If (e.DestDataItem IsNot Nothing) Then
                        ' Dim child_obj As Child = GetChild(ChildView, DirectCast(e.DestDataItem.GetDataKeyValue("ParentId"), Integer))
                        'destinationIndex = IIf(child_obj IsNot Nothing, ChildView.IndexOf(child_obj), -1)
                    End If

                    For Each draggedItem As GridDataItem In e.DraggedItems
                        Dim child_obj As Child = GetChild(ChildView, DirectCast(draggedItem.GetDataKeyValue("user_id"), Integer))
                        destinationIndex = IIf(child_obj IsNot Nothing, ChildView.IndexOf(child_obj), -1)
                        Dim tmpParent As Parent = GetParent(ParentView, DirectCast(draggedItem.GetDataKeyValue("user_id"), Integer))

                        If tmpParent IsNot Nothing Then
                            If destinationIndex > -1 Then
                                If e.DropPosition = GridItemDropPosition.Below Then
                                    'destinationIndex += 1
                                End If
                                'ChildView.Insert(destinationIndex, New Child(orderId:=tmpParent.ParentID, customerId:=tmpParent.CustomerID, isactive:=1, customerName:=tmpParent.CustName, requiredDate:=tmpParent.RequiredDate))
                                ChildView.Item(destinationIndex).IsVisible = True ', New Child(orderId:=tmpParent.ParentID, customerId:=tmpParent.CustomerID, isactive:=1, customerName:=tmpParent.CustName, requiredDate:=tmpParent.RequiredDate))

                            Else
                                ChildView.Add(New Child(user_id:=tmpParent.User_Id, name:=tmpParent.Name, Profile:=tmpParent.Profile, email:=tmpParent.Email, isvisible:=True))
                            End If
                            ParentView(ParentView.IndexOf(tmpParent)).IsVisible = False
                        End If
                    Next

                    ChildStore = ChildView
                    ParentStore = ParentView
                    grdParent.Rebind()
                    grdChild.Rebind()
                ElseIf e.DestDataItem IsNot Nothing AndAlso e.DestDataItem.OwnerGridID = grdParent.ClientID Then

                    'reorder items in pending grid
                    'Dim ParentView As IList(Of Parent)
                    'ParentView = ParentStore

                    'Dim order As Parent = GetParent(ParentView, DirectCast(e.DestDataItem.GetDataKeyValue("ParentId"), Integer))
                    'Dim destinationIndex As Integer = ParentView.IndexOf(order)

                    'If ((e.DropPosition = GridItemDropPosition.Above) _
                    '        AndAlso (e.DestDataItem.ItemIndex > e.DraggedItems(0).ItemIndex)) Then
                    '    destinationIndex = (destinationIndex - 1)
                    'End If
                    'If ((e.DropPosition = GridItemDropPosition.Below) _
                    '        AndAlso (e.DestDataItem.ItemIndex < e.DraggedItems(0).ItemIndex)) Then
                    '    destinationIndex = (destinationIndex + 1)
                    'End If

                    'Dim ordersToMove As New List(Of Parent)()
                    'For Each draggedItem As GridDataItem In e.DraggedItems
                    '    Dim tmpParent As Parent = GetParent(ParentView, DirectCast(draggedItem.GetDataKeyValue("ParentId"), Integer))
                    '    If tmpParent IsNot Nothing Then
                    '        ordersToMove.Add(tmpParent)
                    '    End If
                    'Next

                    'For Each orderToMove As Parent In ordersToMove
                    '    ParentView.Remove(orderToMove)
                    '    ParentView.Insert(destinationIndex, orderToMove)
                    'Next
                    'ParentStore = ParentView
                    'grdParent.Rebind()

                    'Dim destinationItemIndex As Integer = destinationIndex - (grdParent.PageSize * grdParent.CurrentPageIndex)
                    'e.DestinationTableView.Items(destinationItemIndex).Selected = True
                End If
            End If
        End If
    End Sub

    Private Shared Function GetParent(ByVal ordersToSearchIn As IEnumerable(Of Parent), ByVal user_id As Integer) As Parent
        For Each order As Parent In ordersToSearchIn
            If order.User_Id = user_id Then
                Return order
            End If
        Next
        Return Nothing
    End Function

    Private Shared Function GetChild(ByVal deliversToSearchIn As IEnumerable(Of Child), ByVal user_id As Integer) As Child
        For Each child_obj As Child In deliversToSearchIn
            If child_obj.User_Id = user_id Then
                Return child_obj
            End If
        Next
        Return Nothing
    End Function

    Protected Sub grdChild_RowDrop(ByVal sender As Object, ByVal e As GridDragDropEventArgs)
        '  Response.Write("<br>from pending to shipped grid")
        If String.IsNullOrEmpty(e.HtmlElement) Then
            If e.DraggedItems(0).OwnerGridID = grdChild.ClientID Then

                '  If (e.DestDataItem Is Nothing AndAlso ParentStore.Count = 0) OrElse (e.DestDataItem IsNot Nothing AndAlso e.DestDataItem.OwnerGridID = grdParent.ClientID) Then
                Dim ChildView As IList(Of Child)
                Dim ParentView As IList(Of Parent)

                ChildView = ChildStore
                ParentView = ParentStore

                Dim destinationIndex As Int32 = -1
                Dim destinationIndexneew As Int32 = 0
                If (e.DestDataItem IsNot Nothing) Then
                    'Dim order As Parent = GetParent(ParentView, DirectCast(e.DestDataItem.GetDataKeyValue("user_id"), Integer))
                    'destinationIndex = IIf(order IsNot Nothing, ParentView.IndexOf(order), -1)
                End If

                For Each draggedItem As GridDataItem In e.DraggedItems
                    Dim order As Parent = GetParent(ParentView, DirectCast(draggedItem.GetDataKeyValue("user_id"), Integer))
                    destinationIndex = IIf(order IsNot Nothing, ParentView.IndexOf(order), -1)
                    Dim child_obj As Child = GetChild(ChildView, DirectCast(draggedItem.GetDataKeyValue("user_id"), Integer))

                    If child_obj IsNot Nothing Then
                        If destinationIndex > -1 Then
                            If e.DropPosition = GridItemDropPosition.Below Then
                                ' destinationIndex += 1
                            End If
                            '' ParentView.Insert(destinationIndex, New Parent(orderId:=tmpParent.ParentID, customerId:=tmpParent.CustomerID, isactive:=1, customerName:=tmpParent.CustName, requiredDate:=tmpParent.RequiredDate))
                            'comment by ajay ParentView.Item(destinationIndex).IsVisible = True
                            'coment by ajay ChildView(ChildView.IndexOf(child_obj)).IsVisible = False
                            ParentView.Item(destinationIndex).IsVisible = True ', New Child(orderId:=tmpParent.ParentID, customerId:=tmpParent.CustomerID, isactive:=1, customerName:=tmpParent.CustName, requiredDate:=tmpParent.RequiredDate))
                            ChildView.Remove(child_obj)
                        Else
                            'ParentView.Add(New Parent(user_id:=child_obj.User_Id, name:=child_obj.Name, Profile:=child_obj.Profile, email:=child_obj.Email, isvisible:=True))
                        End If
                        ' ChildView(ChildView.IndexOf(child_obj)).IsVisible = False
                    End If
                Next

                ChildStore = ChildView
                ParentStore = ParentView
                grdParent.Rebind()
                grdChild.Rebind()
            ElseIf e.DestDataItem IsNot Nothing AndAlso e.DestDataItem.OwnerGridID = grdChild.ClientID Then

                'reorder items in pending grid
                'Dim ChildView As IList(Of Child)
                'ChildView = ChildStore

                'Dim child_obj As Child = GetChild(ChildView, DirectCast(e.DestDataItem.GetDataKeyValue("ParentId"), Integer))
                'Dim destinationIndex As Integer = ChildView.IndexOf(child_obj)

                'If ((e.DropPosition = GridItemDropPosition.Above) _
                '        AndAlso (e.DestDataItem.ItemIndex > e.DraggedItems(0).ItemIndex)) Then
                '    destinationIndex = (destinationIndex - 1)
                'End If
                'If ((e.DropPosition = GridItemDropPosition.Below) _
                '        AndAlso (e.DestDataItem.ItemIndex < e.DraggedItems(0).ItemIndex)) Then
                '    destinationIndex = (destinationIndex + 1)
                'End If

                'Dim ordersToMove As New List(Of Child)()
                'For Each draggedItem As GridDataItem In e.DraggedItems
                '    Dim tmpParent As Child = GetChild(ChildView, DirectCast(draggedItem.GetDataKeyValue("ParentId"), Integer))
                '    If tmpParent IsNot Nothing Then
                '        ordersToMove.Add(tmpParent)
                '    End If
                'Next

                'For Each orderToMove As Child In ordersToMove
                '    ChildView.Remove(orderToMove)
                '    ChildView.Insert(destinationIndex, orderToMove)
                'Next
                'ChildStore = ChildView
                'grdChild.Rebind()

                'Dim destinationItemIndex As Integer = destinationIndex - (grdChild.PageSize * grdChild.CurrentPageIndex)
                'e.DestinationTableView.Items(destinationItemIndex).Selected = True
            End If
            'End If
        End If

        'If Not String.IsNullOrEmpty(e.HtmlElement) AndAlso e.HtmlElement = "trashCan" Then
        '    Dim ChildView As IList(Of Child)
        '    ChildView = ChildStore
        '    Dim deleted As Boolean = False

        '    For Each draggedItem As GridDataItem In e.DraggedItems
        '        Dim tmpParent As Child = GetChild(ChildView, DirectCast(draggedItem.GetDataKeyValue("buyer_id"), Integer))

        '        If tmpParent IsNot Nothing Then
        '            deleted = True
        '            ChildView.Remove(tmpParent)
        '        End If
        '    Next
        '    If (deleted) Then
        '        msg.Visible = True
        '    End If
        '    ChildStore = ChildView
        '    grdChild.Rebind()
        'End If
    End Sub

    ' buyer_id, company_name, contact, email, isvisible


    Public Class Parent
        Private _profile As String
        Private _name As String
        Private _user_id As Integer
        Private _email As String
        Private _isvisible As Boolean


        Public Sub New(ByVal user_id As Integer, ByVal name As String, ByVal profile As String, ByVal email As String, ByVal isvisible As Boolean)
            _user_id = user_id
            _profile = profile
            _name = name
            _email = email
            _isvisible = isvisible
        End Sub

        Public Property User_Id() As Integer
            Get
                Return _user_id
            End Get
            Set(value As Integer)
                _user_id = value
            End Set
        End Property

        Public Property Profile() As String
            Get
                Return _profile
            End Get
            Set(value As String)
                _profile = value
            End Set
        End Property

        Public Property Name() As String
            Get
                Return _name
            End Get
            Set(value As String)
                _name = value
            End Set
        End Property

        Public Property Email() As String
            Get
                Return _email
            End Get
            Set(value As String)
                _email = value
            End Set
        End Property

        Public Property IsVisible() As Boolean
            Get
                Return _isvisible
            End Get
            Set(value As Boolean)
                _isvisible = value
            End Set
        End Property
    End Class

    Public Class Child

        Private _profile As String
        Private _name As String
        Private _user_id As Integer
        Private _email As String
        Private _isvisible As Boolean


        Public Sub New(ByVal user_id As Integer, ByVal name As String, ByVal profile As String, ByVal email As String, ByVal isvisible As Boolean)
            _user_id = user_id
            _profile = profile
            _name = name
            _email = email
            _isvisible = isvisible
        End Sub

        Public Property User_Id() As Integer
            Get
                Return _user_id
            End Get
            Set(value As Integer)
                _user_id = value
            End Set
        End Property

        Public Property Profile() As String
            Get
                Return _profile
            End Get
            Set(value As String)
                _profile = value
            End Set
        End Property

        Public Property Name() As String
            Get
                Return _name
            End Get
            Set(value As String)
                _name = value
            End Set
        End Property

        Public Property Email() As String
            Get
                Return _email
            End Get
            Set(value As String)
                _email = value
            End Set
        End Property

        Public Property IsVisible() As Boolean
            Get
                Return _isvisible
            End Get
            Set(value As Boolean)
                _isvisible = value
            End Set
        End Property
    End Class

    Protected Sub but_save_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles but_save.Click
        If hd_seller_id.Value > 0 Then
            Dim dt As New DataTable
            Dim obj As New CommonCode

            Dim ins_qry As String = ""
            Dim del_qry As String = "delete s from tbl_reg_seller_user_mapping s inner join tbl_sec_users b on s.user_id=b.user_id where b.is_active=1 and s.seller_id=" & hd_seller_id.Value & ";"
            dt = SqlHelper.ExecuteDataTable(del_qry)
            For Each dr As GridDataItem In grdChild.Items
                ins_qry = "insert into tbl_reg_seller_user_mapping (seller_id,user_id) values (" & hd_seller_id.Value & "," & dr.GetDataKeyValue("user_id") & ")"
                dt = New DataTable
                dt = SqlHelper.ExecuteDataTable(ins_qry)
            Next
            'Response.Write("<script>window.opener.location.href=window.opener.location.href;window.close();</script>")
            Response.Write("<script>window.close();opener.location.reload();</script>")
        End If
    End Sub
End Class
