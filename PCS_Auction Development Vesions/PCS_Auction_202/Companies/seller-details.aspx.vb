﻿Imports System.IO
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Partial Class Companies_seller_details
    Inherits System.Web.UI.Page

    Private New_Company As Boolean = False
    Private View_Company As Boolean = False
    Private Edit_Company As Boolean = False
    Private Company_Assign_Bidder As Boolean = False
    Private Company_Assign_User As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Request.QueryString.Get("e") = "1" Then
                lbl_error.Text = "New Company created successfully"
            End If
            'Me.Page.ClientScript.GetPostBackEventReference(Me, "")

            'but_basic_save.Attributes.Add("onclick", String.Format("realPostBack('{0}', ''); return false;", but_basic_save.UniqueID))
            'but_basic_update.Attributes.Add("onclick", String.Format("realPostBack('{0}', '',{1}); return false;", but_basic_update.UniqueID, RadAjaxPanel_Basic.ClientID))
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset("select * from tbl_master_countries; select * from tbl_master_industry_types; select * from tbl_master_business_types;select isnull(name,'') as name,title_id from tbl_master_titles order by name")
            dd_basic_country.DataSource = ds.Tables(0)
            dd_basic_country.DataTextField = "name"
            dd_basic_country.DataValueField = "country_id"
            dd_basic_country.DataBind()
            dd_basic_country.Items.Insert(0, New ListItem("Select Country", "0"))

            If Not dd_basic_country.Items.FindByText("USA") Is Nothing Then
                dd_basic_country.ClearSelection()
                dd_basic_country.Items.FindByText("USA").Selected = True
            End If
            fill_states()
            'dd_basic_state.Items.Insert(0, New ListItem("Select State", "0"))

          
            chk_profile_industry.DataSource = ds.Tables(1)
            chk_profile_industry.DataTextField = "name"
            chk_profile_industry.DataValueField = "industry_type_id"
            chk_profile_industry.DataBind()

            chk_profile_business.DataSource = ds.Tables(2)
            chk_profile_business.DataTextField = "name"
            chk_profile_business.DataValueField = "business_type_id"
            chk_profile_business.DataBind()

            ddl_basic_title.DataSource = ds.Tables(3)
            ddl_basic_title.DataTextField = "name"
            ddl_basic_title.DataValueField = "name"
            ddl_basic_title.DataBind()
            ddl_basic_title.Items.Insert(0, New ListItem("Select Title", ""))

            If IsNumeric(Request.QueryString.Get("i")) Then
                'Me.UC_System_log_link.Unique_ID = Request.QueryString("i")
                'Me.UC_System_log_link.Module_Name = "Seller"
                hd_seller_id.Value = Request.QueryString.Get("i")
                Collaspe_Panel_Status(True)
                BasicTabEdit(True)
                ProfileTabEdit(True)

                'Dim a As HtmlAnchor = CType(Page.FindControl("a_bid_assign"), HtmlAnchor)
                a_bid_assign.Attributes.Add("onclick", "return open_buyer_assign('" & hd_seller_id.Value & "')")

                ' a = New HtmlAnchor
                ' a = CType(Page.FindControl("a_user_assign"), HtmlAnchor)
                a_user_assign.Attributes.Add("onclick", "return open_user_assign('" & hd_seller_id.Value & "')")
            Else

                hd_seller_id.Value = 0
                BasicTabEdit(False)
                ProfileTabEdit(False)
                Collaspe_Panel_Status(False)
            End If


        End If
        If IsNumeric(Request.QueryString.Get("i")) Then
            Me.UC_System_log_link.Unique_ID = Request.QueryString("i")
            Me.UC_System_log_link.Module_Name = "Seller"
        End If
        
    End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Private Sub SetPermission()
        If Not CommonCode.is_super_admin() Then
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 9
                        New_Company = True
                    Case 10
                        View_Company = True
                    Case 11
                        Edit_Company = True
                    Case 12
                        Company_Assign_Bidder = True
                    Case 13
                        Company_Assign_User = True

                End Select
            Next
            dt = Nothing
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                If View_Company = False Then Response.Redirect("/NoPermission.aspx")
                If Not Edit_Company Then
                    'Set basic info tab 
                    Panel1_Content1button_per.Visible = False
                    'Set profile tab
                    Panel2_Content2button_per.Visible = False
                    'Set bidders tab
                    a_bid_assign.Visible = False
                    RadGrid_Bidder.Columns.FindByUniqueName("mapping_id").Visible = False
                    'Set users tab
                    a_user_assign.Visible = False
                    RadGrid_User.Columns.FindByUniqueName("DeleteColumn").Visible = False
                    'Set others tab
                    RadGrid_Others.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None
                    RadGrid_Others.Columns.FindByUniqueName("EditCommandColumn").Visible = False
                    'Set auctions tab

                    'Set comments tab
                    Panel7_Content7button.Visible = False

                End If
                If Not Company_Assign_User Then
                    div_assign_bidders.Visible = False
                End If
                If Not Company_Assign_Bidder Then
                    div_assign_users.Visible = False
                End If
            Else
                If New_Company = False Then Response.Redirect("/NoPermission.aspx")
            End If
        Else
            New_Company = True
            View_Company = True
            Edit_Company = True
            Company_Assign_Bidder = True
            Company_Assign_User = True
        End If
        
    End Sub
    Protected Sub Collaspe_Panel_Status(ByVal EnableMode As Boolean)
        cpe2.Enabled = EnableMode
        cpe22.Enabled = EnableMode
        Panel2_Content.Visible = EnableMode
        If EnableMode And Edit_Company = False Then
            Panel2_Content2button.Visible = Not (EnableMode)
        Else
            Panel2_Content2button.Visible = EnableMode
        End If

        cpe3.Enabled = EnableMode
        Panel3_Content.Visible = EnableMode
        cpe4.Enabled = EnableMode
        Panel4_Content.Visible = EnableMode
        cpe5.Enabled = EnableMode
        Panel5_Content.Visible = EnableMode
        cpe6.Enabled = EnableMode
        Panel6_Content.Visible = EnableMode
        cpe7.Enabled = EnableMode
        cpe77.Enabled = EnableMode
        Panel7_Content.Visible = EnableMode
        If EnableMode And Edit_Company = False Then
            Panel7_Content7button.Visible = Not (EnableMode)
            RadGrid_Comment.Columns.FindByUniqueName("DeleteColumn").Visible = False
        Else
            Panel7_Content7button.Visible = EnableMode
        End If
    End Sub

#Region "Bidder_Tab"

    Protected Sub RadGrid_Bidder_DeleteCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrid_Bidder.DeleteCommand
        CommonCode.insert_system_log("Bidder Deleted", "RadGrid_Bidder_DeleteCommand", Request.QueryString.Get("i"), "", "Seller")
    End Sub


    Protected Sub RadGrid_Bidder_ItemCommand(ByVal source As Object, ByVal e As GridCommandEventArgs) Handles RadGrid_Bidder.ItemCommand
        If e.CommandName = RadGrid.ExpandCollapseCommandName And TypeOf e.Item Is GridDataItem Then
            DirectCast(e.Item, GridDataItem).ChildItem.FindControl("InnerContainer").Visible = Not e.Item.Expanded
        End If
    End Sub

    Protected Sub RadGrid_Bidder_ItemCreated(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGrid_Bidder.ItemCreated
        If TypeOf e.Item Is GridNestedViewItem Then
            e.Item.FindControl("InnerContainer").Visible = (DirectCast(e.Item, GridNestedViewItem)).ParentItem.Expanded
        End If
    End Sub
    Protected Sub RadGrid_Bidder_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid_Bidder.PreRender
        RadGrid_Bidder.GroupingSettings.CaseSensitive = False
        'Dim menu As GridFilterMenu = RadGrid_Others.FilterMenu

        'Dim i As Integer = 0
        'While i < menu.Items.Count

        '    If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
        '        i = i + 1
        '    Else
        '        menu.Items.RemoveAt(i)
        '    End If
        'End While
    End Sub
#End Region

#Region "Profile_Tab"
    Private Sub ProfileTabEdit(ByVal readMode As Boolean)
        If hd_seller_id.Value <> 0 Then
            fillProfileTab(hd_seller_id.Value)
        Else
            EmptyProfileData()
        End If
        visibleProfileData(readMode)
    End Sub

    Private Sub fillProfileTab(ByVal seller_id As Integer)
        Dim qry As String = "SELECT A.seller_id, " & _
  "ISNULL(A.bank_routing_no, '') AS bank_routing_no," & _
  "ISNULL(A.bank_account_no, '') AS bank_account_no," & _
  "ISNULL(A.bank_name, '') AS bank_name," & _
  "ISNULL(A.bank_contact, '') AS bank_contact," & _
  "ISNULL(A.bank_address, '') AS bank_address," & _
  "ISNULL(A.bank_phone, '') AS bank_phone" & _
  " FROM tbl_reg_sellers A WHERE" & _
  " A.seller_id = " & seller_id & "; select A.business_type_id,A.name from tbl_master_business_types A inner join tbl_reg_seller_business_type_mapping B on A.business_type_id=B.business_type_id where seller_id=" & seller_id & "; " & _
  "select A.industry_type_id,A.name from tbl_master_industry_types A inner join tbl_reg_seller_industry_type_mapping B on A.industry_type_id = B.industry_type_id where seller_id=" & seller_id & "; " & _
  "Declare @str varchar(max) select @str=COALESCE(@str + ', ', '') + ISNULL(A.name,'') from tbl_master_industry_types A inner join tbl_reg_seller_industry_type_mapping I on A.industry_type_id = I.industry_type_id where seller_id=" & seller_id & " select isnull(@str,'') as name; " & _
  "Declare @str1 varchar(max) select @str1=COALESCE(@str1 + ', ', '') + ISNULL(A.name,'') from tbl_master_business_types A inner join tbl_reg_seller_business_type_mapping B on A.business_type_id=B.business_type_id where seller_id=" & seller_id & " select isnull(@str1,'') as name; "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(qry)
        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)

                txt_profile_account.Text = .Item("bank_account_no")
                txt_profile_bank_address.Text = .Item("bank_address")
                txt_profile_bank_contact.Text = .Item("bank_contact")
                txt_profile_routing.Text = .Item("bank_routing_no")
                txt_profile_bank_name.Text = .Item("bank_name")
                txt_profile_bank_phone.Text = .Item("bank_phone")

                lbl_profile_account.Text = .Item("bank_account_no")
                lbl_profile_bank_address.Text = .Item("bank_address")
                lbl_profile_bank_contact.Text = .Item("bank_contact")
                lbl_profile_routing.Text = .Item("bank_routing_no")
                lbl_profile_bank_name.Text = .Item("bank_name")
                lbl_profile_bank_phone.Text = .Item("bank_phone")
            End With

            'lst_profile_business.DataSource = ds.Tables(1)
            'lst_profile_business.DataBind()
            For Each dr As DataRow In ds.Tables(1).Rows
                chk_profile_business.Items.FindByValue(dr("business_type_id")).Selected = True
            Next
            'lst_profile_industry.DataSource = ds.Tables(2)
            'lst_profile_industry.DataBind()
            For Each dr As DataRow In ds.Tables(2).Rows
                chk_profile_industry.Items.FindByValue(dr("industry_type_id")).Selected = True
            Next
            lbl_profile_industry.Text = ds.Tables(3).Rows(0)(0)
            lbl_profile_business.Text = ds.Tables(4).Rows(0)(0)
        Else
            EmptyProfileData()

        End If
        ds = Nothing

    End Sub

    Private Sub EmptyProfileData()
        txt_profile_account.Text = ""
        txt_profile_bank_address.Text = ""
        txt_profile_bank_contact.Text = ""
        txt_profile_routing.Text = ""
        txt_profile_bank_name.Text = ""
        txt_profile_bank_phone.Text = ""
        For Each lsi As ListItem In chk_profile_industry.Items
            lsi.Selected = False
        Next
        For Each lsb As ListItem In chk_profile_business.Items
            lsb.Selected = False
        Next
        lbl_profile_account.Text = ""
        lbl_profile_bank_address.Text = ""
        lbl_profile_bank_contact.Text = ""
        lbl_profile_routing.Text = ""
        lbl_profile_bank_name.Text = ""
        lbl_profile_bank_phone.Text = ""
        lbl_profile_business.Text = ""
        lbl_profile_industry.Text = ""

    End Sub

    Private Sub visibleProfileData(ByVal readMode As Boolean)

        If readMode Then
            but_profile_edit.Visible = True
            but_profile_update.Visible = False
            div_profile_cancel.Visible = False
        Else
            but_profile_edit.Visible = False

            If hd_seller_id.Value = 0 Then
                but_profile_update.Visible = False
                div_profile_cancel.Visible = False
            Else
                but_profile_update.Visible = True
                div_profile_cancel.Visible = True
            End If
        End If

        txt_profile_account.Visible = Not readMode
        txt_profile_bank_address.Visible = Not readMode
        txt_profile_bank_contact.Visible = Not readMode
        txt_profile_routing.Visible = Not readMode
        txt_profile_bank_name.Visible = Not readMode
        txt_profile_bank_phone.Visible = Not readMode
        chk_profile_business.Visible = Not readMode
        chk_profile_industry.Visible = Not readMode
        span_profile_account.Visible = Not readMode
        span_profile_routing.Visible = Not readMode
        span_profile_bank_name.Visible = Not readMode



        lbl_profile_account.Visible = readMode
        lbl_profile_bank_address.Visible = readMode
        lbl_profile_bank_contact.Visible = readMode
        lbl_profile_routing.Visible = readMode
        lbl_profile_bank_name.Visible = readMode
        lbl_profile_bank_phone.Visible = readMode
        lbl_profile_business.Visible = readMode
        lbl_profile_industry.Visible = readMode

    End Sub

    Protected Sub btn_profile_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_profile_edit.Click
        visibleProfileData(False)
        ' RadTabStrip1.SelectedIndex = 1
    End Sub
    Protected Sub btn_profile_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_profile_cancel.Click
        visibleProfileData(True)
        'RadTabStrip1.SelectedIndex = 1
    End Sub


    Protected Sub btn_profile_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_profile_update.Click
        If hd_seller_id.Value > 0 And Page.IsValid Then
            Dim dt As New DataTable
            Dim obj As New CommonCode

            Dim ins_qry As String = ""
            Dim upd_qry As String = "update tbl_reg_sellers set bank_routing_no='" & txt_profile_routing.Text.Trim.Replace("'", "''") & "', bank_account_no='" & txt_profile_account.Text.Trim.Replace("'", "''") & "'" & _
               ", bank_name='" & txt_profile_bank_name.Text.Trim.Replace("'", "''") & "', bank_contact='" & txt_profile_bank_contact.Text.Trim.Replace("'", "''") & "', bank_address='" & txt_profile_bank_address.Text.Trim.Replace("'", "''") & "'" & _
               ", bank_phone='" & txt_profile_bank_phone.Text.Trim.Replace("'", "''") & "' where seller_id=" & hd_seller_id.Value & "; delete from tbl_reg_seller_business_type_mapping where seller_id=" & hd_seller_id.Value & "; delete from tbl_reg_seller_industry_type_mapping where seller_id=" & hd_seller_id.Value & ";"
            dt = New DataTable

            dt = SqlHelper.ExecuteDataTable(upd_qry)
            For Each lsi As ListItem In chk_profile_industry.Items
                If lsi.Selected Then
                    ins_qry = "insert into tbl_reg_seller_industry_type_mapping (seller_id,industry_type_id) values (" & hd_seller_id.Value & "," & lsi.Value & ")"
                    dt = New DataTable
                    dt = SqlHelper.ExecuteDataTable(ins_qry)
                End If
            Next
            For Each lsb As ListItem In chk_profile_business.Items
                If lsb.Selected Then
                    ins_qry = "insert into tbl_reg_seller_business_type_mapping (seller_id,business_type_id) values (" & hd_seller_id.Value & "," & lsb.Value & ")"
                    dt = New DataTable
                    dt = SqlHelper.ExecuteDataTable(ins_qry)
                End If
            Next
            CommonCode.insert_system_log("Seller profile updated", "btn_profile_update_Click", Request.QueryString.Get("i"), "", "Seller")
            ProfileTabEdit(True)
        End If

    End Sub

#End Region

#Region "User_Tab"

    Protected Sub RadGrid_User_DeleteCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrid_User.DeleteCommand
        CommonCode.insert_system_log("User Deleted", "RadGrid_User_DeleteCommand", Request.QueryString.Get("i"), "", "Seller")
    End Sub


    Protected Sub RadGrid_User_ItemCommand(ByVal source As Object, ByVal e As GridCommandEventArgs) Handles RadGrid_User.ItemCommand
        If e.CommandName = RadGrid.ExpandCollapseCommandName And TypeOf e.Item Is GridDataItem Then
            DirectCast(e.Item, GridDataItem).ChildItem.FindControl("InnerContainer").Visible = Not e.Item.Expanded
        End If
    End Sub

    Protected Sub RadGrid_User_ItemCreated(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGrid_User.ItemCreated
        If TypeOf e.Item Is GridNestedViewItem Then
            e.Item.FindControl("InnerContainer").Visible = (DirectCast(e.Item, GridNestedViewItem)).ParentItem.Expanded
        End If
    End Sub
#End Region

#Region "Basic_Tab"


    Private Sub BasicTabEdit(ByVal readMode As Boolean)
        pnl_edit_mode.Visible = readMode
        If hd_seller_id.Value > 0 Then
            fillBasicTab(hd_seller_id.Value)
        Else
            EmptyBasicData()

        End If
        visibleBasicData(readMode)
    End Sub

    Private Sub fillBasicTab(ByVal seller_id As Integer)
        Dim qry As String = "SELECT A.seller_id, " & _
  "ISNULL(A.company_name, '') AS company_name," & _
  "ISNULL(A.contact_title, '') AS contact_title," & _
  "ISNULL(A.website, '') AS website," & _
  "ISNULL(A.email, '') AS email," & _
  "ISNULL(A.phone, '') AS phone," & _
  "ISNULL(A.fax, '') AS fax," & _
  "ISNULL(A.address1, '') AS address1," & _
  "ISNULL(A.address2, '') AS address2," & _
  "ISNULL(A.city, '') AS city," & _
  "ISNULL(A.state_id, 0) AS state_id," & _
  "ISNULL(A.zip, '') AS zip," & _
  "ISNULL(A.country_id, 0) AS country_id," & _
  "ISNULL(A.vat_no, '') AS vat_no," & _
  "ISNULL(A.tax_id_ssn, '') AS tax_id_ssn," & _
  "ISNULL(A.time_zone, '') AS time_zone," & _
  "ISNULL(A.auto_approve_bidders, 0) AS auto_approve_bidders," & _
  "ISNULL(A.logo1, '') AS logo1," & _
  "ISNULL(A.logo2, '') AS logo2," & _
  "ISNULL(A.logo3, '') AS logo3," & _
  "ISNULL(A.contact_first_name, '') AS contact_first_name," & _
  "ISNULL(A.contact_last_name, '') AS contact_last_name," & _
  "ISNULL(A.is_active, 0) AS is_active," & _
  "ISNULL(A.ship_value, 0) AS ship_value," & _
  "ISNULL(A.ship_operator, 0) AS ship_operator," & _
  "case when (select top 1 auction_id from tbl_auctions WITH (NOLOCK) where seller_id=" & seller_id & ")>0 then 0 else 1 end AS is_active_modify," & _
   "C.name AS country," & _
    "S.name AS state" & _
  " FROM tbl_reg_sellers A inner join tbl_master_countries C on A.country_id=C.country_id Inner Join tbl_master_states S on A.state_id=S.state_id WHERE" & _
  " A.seller_id = " & seller_id
        Dim dt As DataTable = New DataTable()
        dt = SqlHelper.ExecuteDataTable(qry)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                page_heading.Text = CommonCode.decodeSingleQuote(.Item("company_name"))
                txt_basic_company_name.Text = CommonCode.decodeSingleQuote(.Item("company_name"))
                'txt_basic_contact.Text = CommonCode.decodeSingleQuote(.Item("contact_title"))
                txt_basic_email.Text = .Item("email")
                'txt_basic_confirm_email.Text = .Item("email")
                txt_basic_website.Text = .Item("website")
                txt_basic_phone.Text = .Item("phone")
                txt_basic_fax.Text = .Item("fax")
                txt_basic_address_1.Text = CommonCode.decodeSingleQuote(.Item("address1")).Replace("<br/>", Char.ConvertFromUtf32(13)).Replace("<br/>", Char.ConvertFromUtf32(10))
                txt_basic_address_2.Text = CommonCode.decodeSingleQuote(.Item("address2")).Replace("<br/>", Char.ConvertFromUtf32(13)).Replace("<br/>", Char.ConvertFromUtf32(10))
                txt_basic_city.Text = .Item("city")
                txt_basic_first_name.Text = CommonCode.decodeSingleQuote(.Item("contact_first_name"))
                txt_basic_last_name.Text = CommonCode.decodeSingleQuote(.Item("contact_last_name"))
                txt_basic_ship_value.Text = .Item("ship_value")
                hid_seller_active_modify.Value = .Item("is_active_modify")
                If Not ddl_basic_title.Items.FindByValue(.Item("contact_title")) Is Nothing Then
                    ddl_basic_title.ClearSelection()
                    ddl_basic_title.Items.FindByValue(.Item("contact_title")).Selected = True
                End If
                If Not dd_basic_country.Items.FindByValue(.Item("country_id")) Is Nothing Then
                    dd_basic_country.ClearSelection()
                    dd_basic_country.Items.FindByValue(.Item("country_id")).Selected = True
                End If
                If Not dd_basic_state.Items.FindByValue(.Item("state_id")) Is Nothing Then
                    dd_basic_state.ClearSelection()
                    dd_basic_state.Items.FindByValue(.Item("state_id")).Selected = True
                End If
                If Not dd_basic_ship_operator.Items.FindByValue(.Item("ship_operator")) Is Nothing Then
                    dd_basic_ship_operator.ClearSelection()
                    dd_basic_ship_operator.Items.FindByValue(.Item("ship_operator")).Selected = True
                End If

                txt_basic_zip.Text = .Item("zip")
                txt_basic_vat.Text = CommonCode.decodeSingleQuote(.Item("vat_no"))
                txt_basic_ssn.Text = CommonCode.decodeSingleQuote(.Item("tax_id_ssn"))
                txt_basic_timezone.Text = CommonCode.decodeSingleQuote(.Item("time_zone"))
                'chk_basic_approve_bidder.Checked = .Item("auto_approve_bidders")
                chk_basic_active.Checked = .Item("is_active")

                lbl_basic_ship_value.Text = IIf(.Item("ship_value").ToString = "0", "", .Item("ship_value"))
                lbl_basic_ship_operator.Text = IIf(.Item("ship_operator").ToString = "0", "", .Item("ship_operator"))
                lbl_basic_company_name.Text = CommonCode.decodeSingleQuote(.Item("company_name"))
                lbl_basic_title.Text = CommonCode.decodeSingleQuote(.Item("contact_title"))
                lbl_basic_email.Text = .Item("email")
                'lbl_confirm_email_basic.Text = .Item("email")
                lbl_basic_website.Text = .Item("website")
                lbl_basic_phone.Text = .Item("phone")
                lbl_basic_fax.Text = .Item("fax")
                lbl_basic_address_1.Text = CommonCode.decodeSingleQuote(.Item("address1"))
                lbl_basic_address_2.Text = CommonCode.decodeSingleQuote(.Item("address2"))
                lbl_basic_city.Text = .Item("city")
                lbl_basic_state.Text = .Item("state")
                lbl_basic_country.Text = .Item("country")
                lbl_basic_first_name.Text = CommonCode.decodeSingleQuote(.Item("contact_first_name"))
                lbl_basic_last_name.Text = CommonCode.decodeSingleQuote(.Item("contact_last_name"))

                lbl_basic_zip.Text = .Item("zip")
                lbl_basic_vat.Text = CommonCode.decodeSingleQuote(.Item("vat_no"))
                lbl_basic_ssn.Text = CommonCode.decodeSingleQuote(.Item("tax_id_ssn"))
                lbl_basic_timezone.Text = CommonCode.decodeSingleQuote(.Item("time_zone"))

                'If .Item("auto_approve_bidders") Then
                '    lbl_basic_approve_bidder.Text = "<img src='/Images/true.gif' alt=''>"
                'Else
                '    lbl_basic_approve_bidder.Text = "<img src='/Images/false.gif' alt=''>"
                'End If

                If .Item("is_active") Then
                    lbl_basic_active.Text = "<img src='/Images/true.gif' alt=''>"
                Else
                    lbl_basic_active.Text = "<img src='/Images/false.gif' alt=''>"
                End If

                If .Item("logo1") <> "" Then
                    IMG_Image_1.ImageUrl = "/upload/companies/" & seller_id & "/" & .Item("logo1")
                    a_img_image1.HRef = "/upload/companies/" & seller_id & "/" & .Item("logo1")
                    DIV_IMG_Default_Logo1_Img.Attributes.Add("onmouseover", "tip_new('<img width=250 height=200 src=" & IMG_Image_1.ImageUrl.ToString & " border=0 />','250','white');")
                Else
                    IMG_Image_1.ImageUrl = "/images/imagenotavailable.gif"
                End If

                If .Item("logo2") <> "" Then
                    IMG_Image_2.ImageUrl = "/upload/companies/" & seller_id & "/" & .Item("logo2")
                    a_IMG_Image_2.HRef = "/upload/companies/" & seller_id & "/" & .Item("logo2")
                    DIV_IMG_Default_Logo2_Img.Attributes.Add("onmouseover", "tip_new('<img width=250 height=200 src=" & IMG_Image_2.ImageUrl.ToString & " border=0 />','250','white');")
                Else
                    IMG_Image_2.ImageUrl = "/images/imagenotavailable.gif"
                End If

                If .Item("logo3") <> "" Then
                    IMG_Image_3.ImageUrl = "/upload/companies/" & seller_id & "/" & .Item("logo3")
                    a_IMG_Image_3.HRef = "/upload/companies/" & seller_id & "/" & .Item("logo3")
                    DIV_IMG_Default_Logo3_Img.Attributes.Add("onmouseover", "tip_new('<img width=250 height=200 src=" & IMG_Image_3.ImageUrl.ToString & " border=0 />','250','white');")
                Else
                    IMG_Image_3.ImageUrl = "/images/imagenotavailable.gif"
                End If

            End With
        Else
            EmptyBasicData()
            ' RadWindowManager1.RadAlert("No records to display.", 280, 100, "Data Alert", "")
        End If
        dt = Nothing

    End Sub

    Private Sub EmptyBasicData()
        txt_basic_company_name.Text = ""
        ddl_basic_title.ClearSelection()
        dd_basic_ship_operator.ClearSelection()
        txt_basic_email.Text = ""
        txt_basic_website.Text = ""
        txt_basic_phone.Text = ""
        txt_basic_fax.Text = ""
        txt_basic_address_1.Text = ""
        txt_basic_address_2.Text = ""
        txt_basic_city.Text = ""
        'dd_basic_state.SelectedValue = "0"
        'dd_basic_country.SelectedValue = "0"
        txt_basic_zip.Text = ""
        txt_basic_vat.Text = ""
        txt_basic_ssn.Text = ""
        txt_basic_timezone.Text = ""
        txt_basic_first_name.Text = ""
        txt_basic_last_name.Text = ""
        txt_basic_ship_value.Text = ""
        'chk_basic_approve_bidder.Checked = False
        chk_basic_active.Checked = True

        lbl_basic_company_name.Text = ""
        lbl_basic_title.Text = ""
        lbl_basic_email.Text = ""
        lbl_basic_website.Text = ""
        lbl_basic_phone.Text = ""
        lbl_basic_fax.Text = ""
        lbl_basic_address_1.Text = ""
        lbl_basic_address_2.Text = ""
        lbl_basic_city.Text = ""
        lbl_basic_state.Text = ""
        lbl_basic_country.Text = ""
        lbl_basic_zip.Text = ""
        lbl_basic_vat.Text = ""
        lbl_basic_ssn.Text = ""
        lbl_basic_timezone.Text = ""
        'lbl_basic_approve_bidder.Text = ""
        lbl_basic_active.Text = ""
        lbl_error.Text = ""
        lbl_basic_first_name.Text = ""
        lbl_basic_last_name.Text = ""

    End Sub

    Private Sub visibleBasicData(ByVal readMode As Boolean)

        If readMode Then
            but_basic_edit.Visible = True
            but_basic_save.Visible = False
            but_basic_update.Visible = False
            div_basic_cancel.Visible = False
        Else
            but_basic_edit.Visible = False

            If hd_seller_id.Value = 0 Then
                but_basic_save.Visible = True
                but_basic_update.Visible = False
                div_basic_cancel.Visible = False
            Else
                but_basic_save.Visible = False
                but_basic_update.Visible = True
                ' but_basic_cancel.Visible = True
                div_basic_cancel.Visible = True
            End If
        End If

        txt_basic_company_name.Visible = Not readMode
        ddl_basic_title.Visible = Not readMode
        dd_basic_ship_operator.Visible = Not readMode
        txt_basic_email.Visible = Not readMode
        'txt_basic_confirm_email.Visible = Not readMode
        txt_basic_ssn.Visible = Not readMode
        txt_basic_website.Visible = Not readMode
        txt_basic_vat.Visible = Not readMode
        txt_basic_timezone.Visible = Not readMode
        txt_basic_phone.Visible = Not readMode
        txt_basic_fax.Visible = Not readMode
        txt_basic_address_1.Visible = Not readMode
        txt_basic_address_2.Visible = Not readMode
        txt_basic_city.Visible = Not readMode
        dd_basic_state.Visible = Not readMode
        dd_basic_country.Visible = Not readMode
        txt_basic_zip.Visible = Not readMode
        'chk_basic_approve_bidder.Visible = Not readMode
        chk_basic_active.Visible = Not readMode
        fl_img_basic_upload_1.Visible = Not readMode
        span_basic_company_name.Visible = Not readMode
        span_basic_email.Visible = Not readMode
        span_basic_phone.Visible = Not readMode
        span_basic_state.Visible = Not readMode
        span_basic_country.Visible = Not readMode
        span_basic_first_name.Visible = Not readMode
        span_basic_zip.Visible = Not readMode
        span_basic_address_1.Visible = Not readMode
        span_basic_city.Visible = Not readMode
        txt_basic_zip.Visible = Not readMode
        'lbl_error.Visible = Not readMode
        txt_basic_first_name.Visible = Not readMode
        txt_basic_last_name.Visible = Not readMode
        txt_basic_ship_value.Visible = Not readMode
        If IMG_Image_1.ImageUrl <> "/images/imagenotavailable.gif" Then
            CHK_Image1.Visible = Not readMode
        Else
            CHK_Image1.Visible = False
        End If

        fl_img_basic_upload_2.Visible = Not readMode
        If IMG_Image_2.ImageUrl <> "/images/imagenotavailable.gif" Then
            CHK_Image2.Visible = Not readMode
        Else
            CHK_Image2.Visible = False
        End If
        fl_img_basic_upload_3.Visible = Not readMode
        If IMG_Image_3.ImageUrl <> "/images/imagenotavailable.gif" Then
            CHK_Image3.Visible = Not readMode
        Else
            CHK_Image3.Visible = False
        End If

        lbl_basic_company_name.Visible = readMode
        lbl_basic_title.Visible = readMode
        lbl_basic_ship_operator.Visible = readMode
        lbl_basic_ship_value.Visible = readMode
        lbl_basic_email.Visible = readMode
        'lbl_confirm_email_basic.Visible = readMode
        lbl_basic_website.Visible = readMode
        lbl_basic_phone.Visible = readMode
        lbl_basic_fax.Visible = readMode
        lbl_basic_address_1.Visible = readMode
        lbl_basic_address_2.Visible = readMode
        lbl_basic_city.Visible = readMode
        lbl_basic_state.Visible = readMode
        lbl_basic_country.Visible = readMode
        lbl_basic_zip.Visible = readMode
        lbl_basic_vat.Visible = readMode
        lbl_basic_ssn.Visible = readMode
        lbl_basic_timezone.Visible = readMode
        'lbl_basic_approve_bidder.Visible = readMode
        lbl_basic_active.Visible = readMode
        lbl_basic_first_name.Visible = readMode
        lbl_basic_last_name.Visible = readMode
        'If dd_basic_country.Visible And lbl_basic_country.Visible = False Then
        '    dd_basic_country.SelectedValue = "3"
        'End If
        'If dd_basic_state.Visible And lbl_basic_state.Visible = False Then
        '    ' dd_basic_state.Items.FindByText("1").Selected = True
        'End If

    End Sub

    Protected Sub btn_basic_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_edit.Click
        visibleBasicData(False)
    End Sub
    Protected Sub btn_basic_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_cancel.Click
        visibleBasicData(True)
    End Sub
    Public Function check_company() As Boolean
        Dim str As String = ""
        Dim is_exists As Boolean = False
        Dim i As Integer = 0
        If hd_seller_id.Value > 0 Then
            str = "if exists(select seller_id from tbl_reg_sellers where company_name='" & CommonCode.encodeSingleQuote(txt_basic_company_name.Text.Trim()) & "' and seller_id<>" & hd_seller_id.Value & ") select 1 else select 0"
            ' lbl_error.Text = str
            ' Response.End()
        Else
            str = "if exists(select seller_id from tbl_reg_sellers where company_name='" & CommonCode.encodeSingleQuote(txt_basic_company_name.Text.Trim()) & "') select 1 else select 0"
            ' lbl_error.Text = str
            'Response.End()
        End If
        i = SqlHelper.ExecuteScalar(str)
        If i = 1 Then
            is_exists = True
        Else
            is_exists = False
        End If
        Return is_exists
    End Function
    Protected Sub btn_basic_save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_save.Click
        If Page.IsValid Then
            If check_company() Then
                lbl_error.Text = "Company already exists."
                Exit Sub
            End If

            Dim obj As New CommonCode

            Dim insParameter As String = "company_name"
            Dim insValue As String = "'" & CommonCode.encodeSingleQuote(txt_basic_company_name.Text.Trim()) & "'"

            If ddl_basic_title.SelectedValue <> "" Then
                insParameter = insParameter & ",contact_title"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(ddl_basic_title.SelectedValue) & "'"
            End If
            If txt_basic_email.Text.Trim() <> "" Then
                insParameter = insParameter & ",email"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_email.Text.Trim()) & "'"
            End If
            If txt_basic_website.Text.Trim() <> "" Then
                insParameter = insParameter & ",website"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_website.Text.Trim()) & "'"
            End If
            If txt_basic_phone.Text.Trim() <> "" Then
                insParameter = insParameter & ",phone"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_phone.Text.Trim()) & "'"
            End If
            If txt_basic_fax.Text.Trim() <> "" Then
                insParameter = insParameter & ",fax"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_fax.Text.Trim()) & "'"
            End If
            If txt_basic_address_1.Text.Trim() <> "" Then
                insParameter = insParameter & ",address1"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_address_1.Text.Trim()).Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'"
            End If
            If txt_basic_address_2.Text.Trim() <> "" Then
                insParameter = insParameter & ",address2"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_address_2.Text.Trim()).Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'"
            End If
            If txt_basic_city.Text.Trim() <> "" Then
                insParameter = insParameter & ",city"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_city.Text.Trim()) & "'"
            End If
            If dd_basic_state.SelectedValue <> "" Then
                insParameter = insParameter & ",state_id"
                insValue = insValue & ",'" & dd_basic_state.SelectedValue & "'"
            End If
            If dd_basic_country.SelectedValue <> "" Then
                insParameter = insParameter & ",country_id"
                insValue = insValue & ",'" & dd_basic_country.SelectedValue & "'"
            End If
            If dd_basic_ship_operator.SelectedValue <> "0" Then
                insParameter = insParameter & ",ship_operator"
                insValue = insValue & ",'" & dd_basic_ship_operator.SelectedValue & "'"
            End If
            If txt_basic_ship_value.Text.Trim() <> "" Then
                insParameter = insParameter & ",ship_value"
                insValue = insValue & ",'" & txt_basic_ship_value.Text.Trim() & "'"
            End If
            If txt_basic_zip.Text.Trim() <> "" Then
                insParameter = insParameter & ",zip"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_zip.Text.Trim()) & "'"
            End If
            If txt_basic_vat.Text.Trim() <> "" Then
                insParameter = insParameter & ",vat_no"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_vat.Text.Trim()) & "'"
            End If
            If txt_basic_ssn.Text.Trim() <> "" Then
                insParameter = insParameter & ",tax_id_ssn"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_ssn.Text.Trim()) & "'"
            End If
            If txt_basic_timezone.Text.Trim() <> "" Then
                insParameter = insParameter & ",time_zone"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_timezone.Text.Trim()) & "'"
            End If
            If txt_basic_first_name.Text.Trim() <> "" Then
                insParameter = insParameter & ",contact_first_name"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_first_name.Text.Trim()) & "'"
            End If
            If txt_basic_last_name.Text.Trim() <> "" Then
                insParameter = insParameter & ",contact_last_name"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_last_name.Text.Trim()) & "'"
            End If

            insParameter = insParameter & ",auto_approve_bidders"
            'insValue = insValue & ",'" & IIf(chk_basic_approve_bidder.Checked, 1, 0) & "'"
            insValue = insValue & ",0"

            insParameter = insParameter & ",is_active"
            insValue = insValue & ",'" & IIf(chk_basic_active.Checked, 1, 0) & "'"

            insParameter = insParameter & ",submit_date"
            insValue = insValue & ",getdate()"

            insParameter = insParameter & ",submit_by_user_id"
            insValue = insValue & "," & obj.Fetch_Cookie("user_id") & ""

            Dim qry As String = "insert into tbl_reg_sellers(" & insParameter & ") values(" & insValue & ")  select scope_identity()"
            Dim comp_id As Int32 = SqlHelper.ExecuteScalar(qry)
            UploadFileImages(comp_id)

            If comp_id > 0 Then
                lbl_error.Text = "New Company Created."
                CommonCode.insert_system_log("New company created.", "btn_basic_save_Click", comp_id, "", "Seller")
                hd_seller_id.Value = comp_id
                BasicTabEdit(True)
                Collaspe_Panel_Status(True)
                Response.Redirect("/companies/seller-details.aspx?e=1&i=" & comp_id)
            End If
        End If

    End Sub

    Private Sub UploadFileImages(ByVal seller_id As Integer)
        Try
            Dim filename As String = ""
            Dim pathToCreate As String = "~/Upload/Companies/" & seller_id.ToString()
            Dim sqlqry As String = ""

            'Image 1 start
            If CHK_Image1.Checked Then
                filename = SqlHelper.ExecuteScalar("select logo1 from tbl_reg_sellers where seller_id=" & seller_id)
                SqlHelper.ExecuteNonQuery("update tbl_reg_sellers set logo1='' where seller_id=" & seller_id)
                Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
                If infoFile.Exists Then
                    System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
                End If
            End If
            CHK_Image1.Checked = False
            filename = ""

            If fl_img_basic_upload_1.HasFile Then
                filename = fl_img_basic_upload_1.FileName
                If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
                    System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
                End If

                SqlHelper.ExecuteNonQuery("update tbl_reg_sellers set logo1='" & filename & "' where seller_id=" & seller_id)

                Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
                If infoFile.Exists Then
                    System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
                End If
                fl_img_basic_upload_1.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)
                ' Label1.Text = "img-1 "
            End If

            'Image 1 end

            'Image 2 start
            If CHK_Image2.Checked Then
                filename = SqlHelper.ExecuteScalar("select logo2 from tbl_reg_sellers where seller_id=" & seller_id)
                SqlHelper.ExecuteNonQuery("update tbl_reg_sellers set logo2='' where seller_id=" & seller_id)
                Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
                If infoFile.Exists Then
                    System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
                End If
            End If
            CHK_Image2.Checked = False
            filename = ""
            'Label1.Text = Label1.Text & "img-2 "
            If fl_img_basic_upload_2.HasFile Then

                filename = fl_img_basic_upload_2.FileName
                If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
                    System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
                End If

                SqlHelper.ExecuteNonQuery("update tbl_reg_sellers set logo2='" & filename & "' where seller_id=" & seller_id)

                Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
                If infoFile.Exists Then
                    System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
                End If

                fl_img_basic_upload_2.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

            End If

            'Image 2 end

            'Image 3 start
            If CHK_Image3.Checked Then
                filename = SqlHelper.ExecuteScalar("select logo3 from tbl_reg_sellers where seller_id=" & seller_id)
                SqlHelper.ExecuteNonQuery("update tbl_reg_sellers set logo3='' where seller_id=" & seller_id)
                Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
                If infoFile.Exists Then
                    System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
                End If
            End If
            CHK_Image3.Checked = False
            filename = ""

            If fl_img_basic_upload_3.HasFile Then
                filename = fl_img_basic_upload_3.FileName
                If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
                    System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
                End If

                SqlHelper.ExecuteNonQuery("update tbl_reg_sellers set logo3='" & filename & "' where seller_id=" & seller_id)

                Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
                If infoFile.Exists Then
                    System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
                End If
                fl_img_basic_upload_3.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)
                ' Label1.Text = Label1.Text & "img-3 "
            End If

            'Image 3 end

        Catch ex As Exception
            'RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))
        End Try
    End Sub

    Protected Sub btn_basic_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_update.Click

        ' Dim radalertscript As String = "<script language='javascript'>function f(){callConfirm(); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>"
        ' Page.ClientScript.RegisterStartupScript(Me.[GetType](), "radalert", radalertscript)


        lbl_error.Text = ""
        If hd_seller_id.Value > 0 And Page.IsValid Then
            If check_company() Then
                lbl_error.Text = "Company already exists."
                Exit Sub
            ElseIf hid_seller_active_modify.Value = "0" Then
                If chk_basic_active.Checked = False Then
                    lbl_error.Text = "Company can not be deactivate because its assign to one or more auction."
                    Exit Sub
                End If

            End If
            Dim is_update_required As Boolean = False
            Dim updpara As String = ""

            If lbl_basic_company_name.Text.Trim() <> txt_basic_company_name.Text Then
                updpara = updpara & "company_name='" & CommonCode.encodeSingleQuote(txt_basic_company_name.Text.Trim()) & "'"
                is_update_required = True
            End If
            If lbl_basic_title.Text.Trim() <> ddl_basic_title.SelectedValue Then
                If is_update_required Then
                    updpara = updpara & ",contact_title='" & CommonCode.encodeSingleQuote(ddl_basic_title.SelectedValue) & "'"
                Else
                    updpara = updpara & "contact_title='" & CommonCode.encodeSingleQuote(ddl_basic_title.SelectedValue) & "'"
                End If

                is_update_required = True
            End If
            If lbl_basic_email.Text.Trim() <> txt_basic_email.Text Then
                If is_update_required Then
                    updpara = updpara & ",email='" & CommonCode.encodeSingleQuote(txt_basic_email.Text.Trim()) & "'"
                Else
                    updpara = updpara & "email='" & txt_basic_email.Text.Trim() & "'"
                End If

                is_update_required = True
            End If
            If lbl_basic_website.Text.Trim() <> txt_basic_website.Text Then
                If is_update_required Then
                    updpara = updpara & ",website='" & CommonCode.encodeSingleQuote(txt_basic_website.Text.Trim()) & "'"
                Else
                    updpara = updpara & "website='" & CommonCode.encodeSingleQuote(txt_basic_website.Text.Trim()) & "'"
                End If

                is_update_required = True
            End If
            If lbl_basic_fax.Text.Trim() <> txt_basic_fax.Text Then
                If is_update_required Then
                    updpara = updpara & ",fax='" & CommonCode.encodeSingleQuote(txt_basic_fax.Text.Trim()) & "'"
                Else
                    updpara = updpara & "fax='" & txt_basic_fax.Text.Trim() & "'"
                End If

                is_update_required = True
            End If

            If lbl_basic_phone.Text.Trim() <> txt_basic_phone.Text Then
                If is_update_required Then
                    updpara = updpara & ",phone='" & CommonCode.encodeSingleQuote(txt_basic_phone.Text.Trim()) & "'"
                Else
                    updpara = updpara & "phone='" & CommonCode.encodeSingleQuote(txt_basic_phone.Text.Trim()) & "'"
                End If

                is_update_required = True
            End If

            If lbl_basic_first_name.Text.Trim() <> txt_basic_first_name.Text Then
                If is_update_required Then
                    updpara = updpara & ",contact_first_name='" & CommonCode.encodeSingleQuote(txt_basic_first_name.Text.Trim()) & "'"
                Else
                    updpara = updpara & "contact_first_name='" & CommonCode.encodeSingleQuote(txt_basic_first_name.Text.Trim()) & "'"
                End If

                is_update_required = True
            End If

            If lbl_basic_last_name.Text.Trim() <> txt_basic_last_name.Text Then
                If is_update_required Then
                    updpara = updpara & ",contact_last_name='" & CommonCode.encodeSingleQuote(txt_basic_last_name.Text.Trim()) & "'"
                Else
                    updpara = updpara & "contact_last_name='" & CommonCode.encodeSingleQuote(txt_basic_last_name.Text.Trim()) & "'"
                End If

                is_update_required = True
            End If

            If lbl_basic_address_1.Text.Trim() <> txt_basic_address_1.Text.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") Then
                If is_update_required Then
                    updpara = updpara & ",address1='" & CommonCode.encodeSingleQuote(txt_basic_address_1.Text.Trim()).Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'"
                Else
                    updpara = updpara & "address1='" & CommonCode.encodeSingleQuote(txt_basic_address_1.Text.Trim()).Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'"
                End If

                is_update_required = True
            End If

            If lbl_basic_address_2.Text.Trim() <> txt_basic_address_2.Text.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") Then
                If is_update_required Then
                    updpara = updpara & ",address2='" & CommonCode.encodeSingleQuote(txt_basic_address_2.Text.Trim()).Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'"
                Else
                    updpara = updpara & "address2='" & CommonCode.encodeSingleQuote(txt_basic_address_2.Text.Trim()).Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'"
                End If

                is_update_required = True
            End If

            If lbl_basic_city.Text.Trim() <> txt_basic_city.Text Then
                If is_update_required Then
                    updpara = updpara & ",city='" & CommonCode.encodeSingleQuote(txt_basic_city.Text.Trim()) & "'"
                Else
                    updpara = updpara & "city='" & CommonCode.encodeSingleQuote(txt_basic_city.Text.Trim()) & "'"
                End If

                is_update_required = True
            End If

            If lbl_basic_state.Text.Trim() <> dd_basic_state.SelectedItem.Text Then
                If is_update_required Then
                    updpara = updpara & ",state_id='" & dd_basic_state.SelectedValue & "'"
                Else
                    updpara = updpara & "state_id='" & dd_basic_state.SelectedValue & "'"
                End If

                is_update_required = True
            End If
            If lbl_basic_ship_operator.Text.Trim() <> dd_basic_ship_operator.SelectedValue Then
                If is_update_required Then
                    updpara = updpara & ",ship_operator='" & dd_basic_ship_operator.SelectedValue & "'"
                Else
                    updpara = updpara & "ship_operator='" & dd_basic_ship_operator.SelectedValue & "'"
                End If

                is_update_required = True
            End If
            If lbl_basic_ship_value.Text.Trim() <> txt_basic_ship_value.Text Then
                If is_update_required Then
                    updpara = updpara & ",ship_value='" & txt_basic_ship_value.Text & "'"
                Else
                    updpara = updpara & "ship_value='" & txt_basic_ship_value.Text & "'"
                End If

                is_update_required = True
            End If
            
            If lbl_basic_country.Text.Trim() <> dd_basic_country.SelectedItem.Text Then
                If is_update_required Then
                    updpara = updpara & ",country_id='" & dd_basic_country.SelectedValue & "'"
                Else
                    updpara = updpara & "country_id='" & dd_basic_country.SelectedValue & "'"
                End If

                is_update_required = True
            End If
            If lbl_basic_zip.Text.Trim() <> txt_basic_zip.Text Then
                If is_update_required Then
                    updpara = updpara & ",zip='" & CommonCode.encodeSingleQuote(txt_basic_zip.Text) & "'"
                Else
                    updpara = updpara & "zip='" & CommonCode.encodeSingleQuote(txt_basic_zip.Text) & "'"
                End If

                is_update_required = True
            End If

            If lbl_basic_vat.Text.Trim() <> txt_basic_vat.Text Then
                If is_update_required Then
                    updpara = updpara & ",vat_no='" & CommonCode.encodeSingleQuote(txt_basic_vat.Text.Trim()) & "'"
                Else
                    updpara = updpara & "vat_no='" & CommonCode.encodeSingleQuote(txt_basic_vat.Text.Trim()) & "'"
                End If

                is_update_required = True
            End If

            If lbl_basic_ssn.Text.Trim() <> txt_basic_ssn.Text Then
                If is_update_required Then
                    updpara = updpara & ",tax_id_ssn='" & CommonCode.encodeSingleQuote(txt_basic_ssn.Text.Trim()) & "'"
                Else
                    updpara = updpara & "tax_id_ssn='" & CommonCode.encodeSingleQuote(txt_basic_ssn.Text.Trim()) & "'"
                End If

                is_update_required = True
            End If

            If lbl_basic_timezone.Text.Trim() <> txt_basic_timezone.Text Then
                If is_update_required Then
                    updpara = updpara & ",time_zone='" & CommonCode.encodeSingleQuote(txt_basic_timezone.Text.Trim) & "'"
                Else
                    updpara = updpara & "time_zone='" & CommonCode.encodeSingleQuote(txt_basic_timezone.Text.Trim) & "'"
                End If
                is_update_required = True
            End If

            If is_update_required Then
                'updpara = updpara & ",auto_approve_bidders='" & IIf(chk_basic_approve_bidder.Checked, 1, 0) & "'"
                updpara = updpara & ",auto_approve_bidders=auto_approve_bidders"

            Else
                'updpara = updpara & "auto_approve_bidders='" & IIf(chk_basic_approve_bidder.Checked, 1, 0) & "'"
                updpara = updpara & "auto_approve_bidders=auto_approve_bidders"
                is_update_required = True
            End If

            If is_update_required Then
                updpara = updpara & ",is_active='" & IIf(chk_basic_active.Checked, 1, 0) & "'"
            Else
                updpara = updpara & "is_active='" & IIf(chk_basic_active.Checked, 1, 0) & "'"
                is_update_required = True
            End If

            'If chk_basic_approve_bidder.Checked Then
            '    RadAjaxManager1.Alert("You are about to auto approve this member")
            'End If        



            If is_update_required Then
                SqlHelper.ExecuteNonQuery("update tbl_reg_sellers set " & updpara & " where seller_id=" & hd_seller_id.Value)
                UploadFileImages(hd_seller_id.Value)
                lbl_error.Text = "Company Updated."
                CommonCode.insert_system_log("Seller basic information updated", "btn_basic_update_Click", Request.QueryString.Get("i"), "", "Seller")
            End If
            BasicTabEdit(True)

        End If

    End Sub


    Private Sub fill_states()
        If dd_basic_country.SelectedItem.Value <> "" Then
            Dim dt = New DataTable
            dt = SqlHelper.ExecuteDataTable("select * from tbl_master_states where country_id=" & dd_basic_country.SelectedValue & " order by name")
            dd_basic_state.DataSource = dt
            dd_basic_state.DataTextField = "name"
            dd_basic_state.DataValueField = "state_id"
            dd_basic_state.DataBind()
            dd_basic_state.Items.Insert(0, New ListItem("Select State", "0"))
        End If
    End Sub
#End Region

#Region "Others_Tab"
    Protected Sub RadGrid_Others_NeedDatasource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)

        RadGrid_Others.DataSource = SqlHelper.ExecuteDataTable("select a.seller_attachment_id,a.seller_id,a.title,convert(varchar,a.upload_date,101) as upload_date,a.upload_by_user_id,s.first_name+' '+s.last_name as uploaded_by,filename from tbl_reg_seller_attachments a inner join tbl_sec_users s on a.upload_by_user_id=s.user_id and a.seller_id=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
    End Sub
    Protected Sub RadGrid_Others_DeleteCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)

        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        Dim attachment_id As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("seller_attachment_id").ToString()
        Try
            SqlHelper.ExecuteNonQuery("DELETE from tbl_reg_seller_attachments where seller_attachment_id='" & attachment_id & "'")
            CommonCode.insert_system_log("Seller attachment deleted", "RadGrid_Others_DeleteCommand", Request.QueryString.Get("i"), "", "Seller")
        Catch ex As Exception
            RadGrid_Others.Controls.Add(New LiteralControl("Unable to delete Attachment. Reason: " + ex.Message))
            e.Canceled = True
        End Try

    End Sub
    Protected Sub RadGrid_Others_UpdateCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        If Page.IsValid Then

            Dim editedItem As GridEditFormItem = DirectCast(e.Item, GridEditFormItem)
            Dim attachment_id As String = editedItem.GetDataKeyValue("seller_attachment_id").ToString '.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("seller_attachment_id").ToString()
            Dim Title As String = (TryCast(editedItem.FindControl("Title"), TextBox)).Text
            Dim filename As String = ""
            Dim fl_upload As System.Web.UI.WebControls.FileUpload = CType(editedItem.FindControl("fl_others_upload_1"), System.Web.UI.WebControls.FileUpload)

            If fl_upload.HasFile Then
                filename = fl_upload.FileName
            End If
            If filename <> "" Then
                SqlHelper.ExecuteNonQuery("update tbl_reg_seller_attachments set title='" & CommonCode.encodeSingleQuote(Title) & "', filename='" & filename & "' where seller_attachment_id=" & attachment_id)
                UploadFileAttachment(fl_upload, hd_seller_id.Value, attachment_id)
                CommonCode.insert_system_log("Seller attachment updated", "RadGrid_Others_UpdateCommand", Request.QueryString.Get("i"), "", "Seller")
            Else
                SqlHelper.ExecuteNonQuery("update tbl_reg_seller_attachments set title='" & CommonCode.encodeSingleQuote(Title) & "' where seller_attachment_id=" & attachment_id)
                CommonCode.insert_system_log("Seller attachment updated", "RadGrid_Others_UpdateCommand", Request.QueryString.Get("i"), "", "Seller")
            End If

        End If
    End Sub
    Protected Sub RadGrid_Others_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid_Others.PreRender
        Dim menu As GridFilterMenu = RadGrid_Others.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub
    Protected Sub RadGrid_Others_InsertCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditFormInsertItem of the RadGrid    
        ' If Page.IsValid Then

        Dim insertedItem As GridEditFormInsertItem = DirectCast(e.Item, GridEditFormInsertItem)
        Dim attachment_id As Integer = 0
        Dim Title As String = (TryCast(insertedItem.FindControl("Title"), TextBox)).Text
        Dim filename As String = ""
        Dim FileUpload1 As System.Web.UI.WebControls.FileUpload = TryCast(insertedItem.FindControl("fl_others_upload_1"), System.Web.UI.WebControls.FileUpload)

        If FileUpload1.HasFile Then
            filename = FileUpload1.FileName
        End If

        Try
            If FileUpload1.HasFile Then
                attachment_id = SqlHelper.ExecuteScalar("Insert into tbl_reg_seller_attachments(seller_id,title,upload_date,upload_by_user_id,filename)Values(" & hd_seller_id.Value & ",'" & Title & "',getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ",'" & filename & "')  select scope_identity()")
                UploadFileAttachment(FileUpload1, hd_seller_id.Value, attachment_id)
                CommonCode.insert_system_log("Seller attachment uploaded", "RadGrid_Others_InsertCommand", Request.QueryString.Get("i"), "", "Seller")
            End If
        Catch ex As Exception
            RadGrid_Others.Controls.Add(New LiteralControl("Unable to insert attachment. Reason: " + ex.Message))
            e.Canceled = True
        End Try

        ' End If
    End Sub
    Private Sub UploadFileAttachment(ByVal fl_upload As System.Web.UI.WebControls.FileUpload, ByVal seller_id As Integer, ByVal attachment_id As Integer)

        Dim filename As String
        filename = fl_upload.FileName
        Dim pathToCreate As String = "../Upload/Companies/attachments/" + seller_id.ToString
        If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
            System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
        End If

        Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
        If infoFile.Exists Then
            System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
        End If
        fl_upload.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

        'Catch ex As Exception
        'RadGrid_Others.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))
        'End Try
    End Sub
    Protected Sub bindradgrid_other()
        Dim dt As New DataTable
        dt = New DataTable
        dt = SqlHelper.ExecuteDataTable("select a.seller_attachment_id,a.seller_id,a.title,convert(varchar,a.upload_date,101) as upload_date,a.upload_by_user_id,s.first_name+' '+s.last_name as uploaded_by,filename from tbl_reg_seller_attachments a inner join tbl_sec_users s on a.upload_by_user_id=s.user_id")
        RadGrid_Others.DataSource = dt
        RadGrid_Others.DataBind()
    End Sub
#End Region

#Region "Auction_Tab"
    Protected Sub RadGrid_Auction_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        Dim itemCount As Integer = TryCast(sender, RadGrid).MasterTableView.GetItems(GridItemType.Item).Length + TryCast(sender, RadGrid).MasterTableView.GetItems(GridItemType.AlternatingItem).Length
        For Each item As GridItem In TryCast(sender, RadGrid).Items
            If TypeOf item Is GridDataItem AndAlso item.ItemIndex < itemCount - 1 Then
                TryCast(TryCast(item, GridDataItem)("code"), TableCell).Controls.Add(New LiteralControl("<table style='display:none;'><tr><td>"))
            End If
        Next
    End Sub

#End Region


#Region "Comment_Tab"
    Protected Sub but_comment_submit_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles but_comment_submit.Click

        If hd_seller_id.Value > 0 And Page.IsValid Then
            Dim ins_qry As String = "Insert into tbl_reg_seller_comments (seller_id,comment,submit_by_user_id,submit_date) values (" & hd_seller_id.Value & ",'" & CommonCode.encodeSingleQuote(txt_comment.Text.Trim.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "','" & CommonCode.Fetch_Cookie_Shared("user_id") & "',getdate())"
            'Label1.Text = upd_qry
            SqlHelper.ExecuteNonQuery(ins_qry)
            CommonCode.insert_system_log("Seller comment inserted", "but_comment_submit_Click", Request.QueryString.Get("i"), "", "Seller")
            txt_comment.Text = ""
            RadGrid_Comment.Rebind()
        End If
    End Sub
    Protected Sub RadGrid_Comment_ItemDeleted(ByVal source As Object, ByVal e As Telerik.Web.UI.GridDeletedEventArgs) Handles RadGrid_Comment.ItemDeleted
        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        Dim id As String = item.GetDataKeyValue("seller_comment_id").ToString()
        Dim Str As String
        If Not e.Exception Is Nothing Then
            e.ExceptionHandled = True
            Str = "Item cannot be deleted. Reason: " + e.Exception.Message
        Else
            Str = "Item with ID " + id + " is deleted!"
            CommonCode.insert_system_log("Seller comment deleted", "RadGrid_Comment_ItemDeleted", Request.QueryString.Get("i"), "", "Seller")
        End If
        RadGrid_Comment.Controls.Add(New LiteralControl(String.Format("<span style='color:red'>" & Str & "</span>")))
    End Sub

    Protected Sub RadGrid_Comment_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid_Comment.PreRender
        Dim menu As GridFilterMenu = RadGrid_Comment.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub

#End Region

    'Protected Sub RadAjaxManager1_AjaxRequest(sender As Object, e As Telerik.Web.UI.AjaxRequestEventArgs) Handles RadAjaxManager1.AjaxRequest
    '    If div_profile_bank_name.Visible = True AndAlso but_profile_update.Visible = True Then
    '        RadAjaxManager1.AjaxSettings.AddAjaxSetting(but_profile_update, Panel2_Content)
    '    End If
    'End Sub

    'Protected Sub RadGrid_Others_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid_Others.PreRender
    '    If RadGrid_Others.MasterTableView.IsItemInserted Then
    '        'For Each item As GridItemCollection In RadGrid_Others.Items
    '        '    item.Visible = False
    '        'Next
    '        For Each i As GridItem In RadGrid_Others.EditItems
    '            i.RemoveChildEditItems() ' = False
    '        Next
    '    End If
    'End Sub

    Protected Sub but_bind_user_bidder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_bind_user_bidder.Click
        If hid_popup_status.Value = "user" Then
            ' Response.Write("<script>alert('" & hid_popup_status.Value & "')</script>")
            cpe4.Collapsed = False
            RadGrid_User.Rebind()
        ElseIf hid_popup_status.Value = "bidder" Then
            'Response.Write("<script>alert('" & hid_popup_status.Value & "')</script>")
            cpe3.Collapsed = False
            RadGrid_Bidder.Rebind()
        End If
    End Sub

   
    
End Class
