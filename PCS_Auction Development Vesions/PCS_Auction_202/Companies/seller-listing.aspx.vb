﻿Imports Telerik.Web.UI

Partial Class Companies_seller_listing
    Inherits System.Web.UI.Page

    'Protected Sub RadGrid_Seller_ColumnCreated(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrid_Seller.ItemCommand
    '    If e.CommandName = "detail" Then
    '        Response.Redirect("/companies/seller-details.aspx?i=" & e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("seller_id"))
    '    End If
    'End Sub
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        SetPermission()
    End Sub
    Private Sub SetPermission()
        If CommonCode.is_super_admin() Then
            div_new_auction.Visible = True
            RadGrid_Seller.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
            RadGrid_Seller.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
            RadGrid_Seller.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
        Else
            div_new_auction.Visible = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 7
                        div_new_auction.Visible = True
                    Case 35
                        RadGrid_Seller.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
                        RadGrid_Seller.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
                        RadGrid_Seller.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
                End Select
            Next
            dt = Nothing

        End If

    End Sub
    Protected Sub RadGrid_Seller_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid_Seller.NeedDataSource
        Dim strQuery As String = ""
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            If CommonCode.is_admin_user Then
                strQuery = "select A.seller_id,A.company_name ,(A.contact_first_name+' '+A.contact_last_name) as contact_name,A.email,isnull(A.contact_title,'') as contact_title,isnull(A.phone,0) as phone,A.is_active from tbl_reg_sellers A "

            Else
                strQuery = "select A.seller_id,A.company_name ,(A.contact_first_name+' '+A.contact_last_name) as contact_name,A.email,isnull(A.contact_title,'') as contact_title,isnull(A.phone,0) as phone,A.is_active from tbl_reg_sellers A " & _
                          "inner JOIN tbl_reg_seller_user_mapping M ON M.seller_id=A.seller_id " & _
                        "where M.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ""
            End If
            RadGrid_Seller.DataSource = SqlHelper.ExecuteDataTable(strQuery)
            If (Not Page.IsPostBack) Then
                RadGrid_Seller.MasterTableView.FilterExpression = "([is_active] = 1) "
                Dim column As GridColumn = RadGrid_Seller.MasterTableView.GetColumnSafe("is_active")
                column.CurrentFilterFunction = GridKnownFunction.EqualTo
                column.CurrentFilterValue = "1"

            End If
        End If

    End Sub

   
    Protected Sub RadGrid_Seller_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid_Seller.PreRender
        Dim menu As GridFilterMenu = RadGrid_Seller.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub
End Class
