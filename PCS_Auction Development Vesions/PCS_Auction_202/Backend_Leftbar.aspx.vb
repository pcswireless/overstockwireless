﻿
Partial Class Backend_Leftbar
    Inherits System.Web.UI.Page
    Public New_Auction As Boolean = False
    Public Vew_Auction As Boolean = False
    Public Edit_Auction As Boolean = False
    Public Auction_Email_Schedule As Boolean = False
    Public View_Auction_Bulk_Listing As Boolean = False
    Public View_Report As Boolean = False
    Public New_Bidder As Boolean = False
    Public Edit_Bidder As Boolean = False
    Public View_Bidder_Bulk_Listing As Boolean = False
    Public New_Company As Boolean = False
    Public View_Company As Boolean = False
    Public Edit_Company As Boolean = False
    Public Company_Assign_Bidder As Boolean = False
    Public Company_Assign_User As Boolean = False
    Public New_User As Boolean = False
    Public View_User As Boolean = False
    Public Edit_User As Boolean = False
    Public New_Profile As Boolean = False
    Public View_Profile As Boolean = False
    Public Edit_Profile As Boolean = False
    Public Profile_User_Assigned As Boolean = False
    Public Manage_Profiles_Permission As Boolean = False
    Public View_Message_Board As Boolean = False
    Public New_Message_Board As Boolean = False
    Public Edit_Message_Board As Boolean = False
    Public View_Help_List As Boolean = False
    Public Edit_Help_List As Boolean = False
    Public View_Master As Boolean = False
    Public View_Offer As Boolean = False
    Public Edit_Bidder_Approval As Boolean = False
    Public Auction_Queries As Boolean = False
    Public View_Auction_Wise_Bidder As Boolean = False
    Public View_Bidder_Wise_Auction As Boolean = False
    Public View_print_button As Boolean = False
    Public View_rss_button As Boolean = False
    Public View_report_button As Boolean = False
    'Public View_Bidder_query As Boolean = False

    Private Sub setPermission()

        If CommonCode.is_super_admin() Then
            New_Auction = True
            View_Report = True
            Vew_Auction = True
            Edit_Auction = True
            Auction_Email_Schedule = True
            View_Auction_Bulk_Listing = False 'permanently invisible from leftbar
            New_Bidder = True
            Edit_Bidder = True
            View_Bidder_Bulk_Listing = True
            New_Company = True
            View_Company = True
            Edit_Company = True
            Company_Assign_Bidder = True
            Company_Assign_User = True
            New_User = True
            View_User = True
            Edit_User = True
            New_Profile = True
            View_Profile = True
            Edit_Profile = True
            Profile_User_Assigned = True
            Manage_Profiles_Permission = True
            View_Message_Board = True
            New_Message_Board = True
            Edit_Message_Board = True
            View_Help_List = True
            Edit_Help_List = True
            View_Master = True
            View_Offer = True
            Auction_Queries = False 'query not post from frontend
            View_print_button = True
            View_rss_button = True
            View_report_button = True
            'View_Bidder_query = True
        Else

            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 1
                        New_Auction = True
                    Case 2
                        Vew_Auction = True
                    Case 3
                        Edit_Auction = True
                    Case 4
                        Auction_Email_Schedule = True
                    Case 5
                        View_Auction_Bulk_Listing = False 'permanently invisible from leftbar
                    Case 6
                        New_Bidder = True
                    Case 7
                        Edit_Bidder = True
                    Case 8
                        View_Bidder_Bulk_Listing = True
                    Case 9
                        New_Company = True
                    Case 10
                        View_Company = True
                    Case 11
                        Edit_Company = True
                    Case 12
                        Company_Assign_Bidder = True
                    Case 13
                        Company_Assign_User = True
                    Case 14
                        New_User = True
                    Case 15
                        View_User = True
                    Case 16
                        Edit_User = True
                    Case 18
                        New_Profile = True
                    Case 19
                        View_Profile = True
                    Case 20
                        Edit_Profile = True
                    Case 21
                        Profile_User_Assigned = True
                    Case 22
                        Manage_Profiles_Permission = True
                    Case 23
                        View_Message_Board = True
                    Case 24
                        New_Message_Board = True
                    Case 25
                        Edit_Message_Board = True
                    Case 26
                        View_Help_List = True
                    Case 27
                        Edit_Help_List = True
                    Case 28
                        View_Master = True
                    Case 31
                        View_Offer = True
                    Case 32
                        Auction_Queries = False 'query not post from frontend
                    Case 33
                        View_Auction_Wise_Bidder = True
                    Case 34
                        View_Bidder_Wise_Auction = True
                    Case 36
                        View_print_button = True
                    Case 39
                        View_rss_button = True
                    Case 40
                        View_report_button = True
                End Select
            Next
            dt = Nothing
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If CommonCode.Fetch_Cookie_Shared("user_id") = "" Then
            Response.Write("<script language='javascript'>top.location = '/login.aspx';</script>")
            ' Response.Redirect("/Login.aspx")
        End If
        If CommonCode.Fetch_Cookie_Shared("is_backend") = "0" Then
            Response.Write("<script language='javascript'>top.location = '/default.aspx';</script>")
            'Response.Redirect("/")
        End If

        'Set permission
        setPermission()


        If Not Page.IsPostBack Then

            Dim strQuery As String = ""
            If CommonCode.is_admin_user() Then
                strQuery = "select count(B.buyer_id) from tbl_reg_buyers B WITH (NOLOCK) where B.status_id IN (1,3,4,7)"
                'strQuery = "select count(B.buyer_id) from tbl_reg_buyers B WITH (NOLOCK) left join tbl_reg_buyser_statuses S on B.status_id=S.status_id Left JOIN tbl_reg_sale_rep_buyer_mapping MM ON B.buyer_id=MM.buyer_id where B.status_id in (1,3,4,7) "
            Else
                ' strQuery = "select count(distinct B.buyer_id) from tbl_reg_buyers B INNER JOIN tbl_reg_buyer_seller_mapping M ON B.buyer_id=M.buyer_id INNER JOIN tbl_reg_sellers S ON M.seller_id=S.seller_id INNER JOIN tbl_reg_seller_user_mapping UM ON S.seller_id=UM.seller_id " & _
                '" where B.status_id IN (1,3,4) and UM.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
                strQuery = "select count(B.buyer_id) from tbl_reg_buyers B WITH (NOLOCK) left join tbl_reg_buyser_statuses ST on B.status_id=ST.status_id Left JOIN tbl_reg_sale_rep_buyer_mapping MM ON B.buyer_id=MM.buyer_id  INNER JOIN tbl_reg_buyer_seller_mapping M ON B.buyer_id=M.buyer_id INNER JOIN tbl_reg_sellers S ON M.seller_id=S.seller_id INNER JOIN tbl_reg_seller_user_mapping UM ON S.seller_id=UM.seller_id " & _
                           " where B.status_id in (1,3,4) and UM.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
            End If
            Dim no_of_aproval As Integer = SqlHelper.ExecuteScalar(strQuery)
            lit_bidder_approval.Text = "Pending Approvals (" & no_of_aproval.ToString() & ")"
            lit_bidder_approval1.Text = "Pending Approvals (" & no_of_aproval.ToString() & ")"

            'If CommonCode.is_admin_user() Then
            '    lit_no_of_auction_queries.Text = SqlHelper.ExecuteScalar("select count(query_id) from tbl_auction_queries A inner join tbl_auctions Q WITH (NOLOCK) on A.auction_id=Q.auction_id where isnull(Q.discontinue,0)=0 and Q.is_active=1 and isnull(parent_query_id,0)=0 and isnull(admin_marked,0)=0 and isnull(A.auction_id,0)>0 and query_id not in (select isnull(parent_query_id,0) from tbl_auction_queries)")
            '    lit_no_of_bidder_queries.Text = SqlHelper.ExecuteScalar("select count(query_id) from tbl_auction_queries where isnull(parent_query_id,0)=0 and isnull(admin_marked,0)=0 and isnull(auction_id,0)=0 and query_id not in (select isnull(parent_query_id,0) from tbl_auction_queries)")
            'Else
            '    lit_no_of_auction_queries.Text = SqlHelper.ExecuteScalar("select count(query_id) from tbl_auction_queries A inner join tbl_auctions Q WITH (NOLOCK) on A.auction_id=Q.auction_id where isnull(Q.discontinue,0)=0 and Q.is_active=1 and isnull(parent_query_id,0)=0 and isnull(admin_marked,0)=0 and buyer_id in (select distinct buyer_id from tbl_reg_sale_rep_buyer_mapping where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ") and isnull(A.auction_id,0)>0 and query_id not in (select isnull(parent_query_id,0) from tbl_auction_queries)")
            '    lit_no_of_bidder_queries.Text = SqlHelper.ExecuteScalar("select count(query_id) from tbl_auction_queries where isnull(parent_query_id,0)=0 and isnull(admin_marked,0)=0 and buyer_id in (select distinct buyer_id from tbl_reg_sale_rep_buyer_mapping where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ") and isnull(auction_id,0)=0 and query_id not in (select isnull(parent_query_id,0) from tbl_auction_queries)")
            'End If

            pnl_auction_submenu.Visible = False
            pnl_bidder_submenu.Visible = False

            Select Case Request.QueryString.Get("t")
                Case 0
                    tab0.Visible = True
                Case 1
                    tab1.Visible = True
                    tabsetting()
                Case 2
                    tab2.Visible = True
                    If Not String.IsNullOrEmpty(Request.QueryString.Get("b")) And View_Bidder_Bulk_Listing Then
                        pnl_bidder_submenu.Visible = True
                    Else
                        pnl_bidder_submenu.Visible = False
                    End If
                Case 3
                    tab3.Visible = True
                    If Not String.IsNullOrEmpty(Request.QueryString.Get("a")) And Vew_Auction Then
                        pnl_auction_submenu.Visible = True
                    Else
                        pnl_auction_submenu.Visible = False
                    End If
                Case Else
                    tab0.Visible = False
                    tab1.Visible = False
                    tab2.Visible = False
                    tab3.Visible = False
            End Select
           
        End If
    End Sub
    Private Sub tabsetting()
        tab1.Visible = True

        Dim str As String = ""
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" And CommonCode.is_admin_user() = False Then
            str = " seller_id in (select seller_id from tbl_reg_seller_user_mapping where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ") and "
        End If
       
        ' lit_offers.Text = SqlHelper.ExecuteScalar("select COUNT(auction_id) from tbl_auctions where " & str & " is_active=1 and dbo.get_auction_status(auction_id) in (1,2)")

    End Sub
End Class
