﻿Imports System.IO

Partial Class auction_detail_print
    Inherits System.Web.UI.Page
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Redirect("/")
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString.Get("a")) Then

                ViewState("auction_id") = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("a"))
                hid_auction_id.Value = ViewState("auction_id")
                fill_page(ViewState("auction_id"))
            Else
                ViewState("auction_id") = 0
            End If
        End If
    End Sub

    Private Sub setAuction(ByVal auction_id As Integer, ByVal mode As String, ByVal quation_id As Integer, ByVal buy_id As Integer)

        Dim str As String = "SELECT A.auction_id,"
        str = str & "ISNULL(A.code, '') AS code,"
        str = str & "ISNULL(A.title, '') AS title,"
        str = str & "ISNULL(A.sub_title, '') AS sub_title,"
        str = str & "ISNULL(A.product_catetory_id, 0) AS product_catetory_id,"
        str = str & "ISNULL(A.stock_condition_id, 0) AS stock_condition_id,"
        str = str & "ISNULL(A.auction_category_id, 0) AS auction_category_id,"
        str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"
        str = str & "ISNULL(A.is_private_offer, 0) AS is_private_offer,"
        str = str & "ISNULL(A.is_partial_offer, 0) AS is_partial_offer,"
        str = str & "ISNULL(A.auto_acceptance_price_private, 0) AS auto_acceptance_price_private,"
        str = str & "ISNULL(A.auto_acceptance_price_partial, 0) AS auto_acceptance_price_partial,"
        str = str & "ISNULL(A.auto_acceptance_quantity, 0) AS auto_acceptance_quantity,"
        str = str & "ISNULL(A.no_of_clicks, 0) AS no_of_clicks,"
        str = str & "ISNULL(A.start_price, 0) AS start_price,"
        str = str & "ISNULL(A.reserve_price, 0) AS reserve_price,"
        str = str & "ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value,"
        str = str & "ISNULL(A.start_date, '1/1/1900') AS start_date,"
        str = str & "ISNULL(A.display_end_time, '1/1/1900') AS display_end_time,"
        str = str & "ISNULL(A.is_show_reserve_price, 0) AS is_show_reserve_price,"
        str = str & "ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing,"
        str = str & "ISNULL(A.is_show_no_of_bids, 0) AS is_show_no_of_bids,"
        str = str & "A.use_pcs_shipping, A.use_your_shipping,"
        str = str & "ISNULL(A.increament_amount, 0) AS increament_amount,"
        str = str & "ISNULL(A.buy_now_price, 0) AS buy_now_price,"
        str = str & "ISNULL(A.is_buy_it_now, 0) AS is_buy_it_now,"
        str = str & "ISNULL(A.request_for_quote_message, '') AS request_for_quote_message,"
        str = str & "ISNULL(A.show_price, 0) AS show_price,"
        str = str & "ISNULL((select last_shipping_option from tbl_reg_buyers WITH (NOLOCK) where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & "), 0) AS last_shipping_option,"
        str = str & "ISNULL(A.total_qty, 0) AS total_qty,"
        str = str & "ISNULL(A.qty_per_bidder, 0) AS qty_per_bidder,"
        str = str & "dbo.get_auction_status(A.auction_id) as auction_status,"
        str = str & "ISNULL(ct.name,'') as product_category,"
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            str = str & "case when exists(select favourite_id from tbl_auction_favourites where auction_id=A.auction_id and buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ") then 1 else 0 end as in_fav,"
        Else
            str = str & "0 as in_fav,"
        End If
        str = str & "ISNULL(IMG.stock_image_id, 0) AS stock_image_id,ISNULL(IMG.filename, '') AS filename"
        str = str & " FROM "
        str = str & "tbl_auctions A  WITH (NOLOCK) left join tbl_master_product_categories CT  WITH (NOLOCK) on A.product_catetory_id=CT.product_catetory_id left join tbl_master_stock_locations L  WITH (NOLOCK) on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B  WITH (NOLOCK) on A.stock_condition_id=B.stock_condition_id "
        str = str & " left join tbl_auction_stock_images IMG  WITH (NOLOCK) on A.auction_id=IMG.auction_id and IMG.stock_image_id=(SELECT top 1 stock_image_id from tbl_auction_stock_images  WITH (NOLOCK) where auction_id=a.auction_id order by position) "
        str = str & " WHERE A.auction_id =" & auction_id
        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDatatable(str)

        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)
                'Response.Write(.Item("last_shipping_option"))
                Dim title_url As String = .Item("title").ToString.Replace(" ", "-").Replace("/", "").Replace("'", "").Replace("&", "").Replace("#", "")
                ltr_auction_code.Text = .Item("code")
                ltr_title.Text = .Item("title") '"<a href='/auction_Pdf.aspx?i=" & auction_id & "&t=" & .Item("auction_type_id") & "' style='font-size:" & IIf(.Item("title").ToString.Length < 70, "20px", IIf(.Item("title").ToString.Length < 150, "18px", IIf(.Item("title").ToString.Length < 200, "16px", "14px"))) & "; text-decoration: none; color:#575757;'>" & .Item("title") & "</a>"
                ltr_sub_title.Text = .Item("sub_title")

                'ltr_short_desc.Text = .Item("short_description")
                'lit_pdf.Text = "<a href='/auction/" & auction_id & "/" & title_url & ".pdf' target='_blank'>Convert To Pdf</a>"
                ' lit_pdf.Text = "<a href='/Auction_PdfNew.aspx?a=" & auction_id & "&t=" & Request.QueryString.Get("t") & "' target='_blank'>Convert To Pdf</a>"
                ' lit_type.Text = .Item("product_category")
                ' CType(e.Item.FindControl("img_ask_question"), ImageButton).Attributes.Add("onclick", "return openAskQuestion(" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & ");")
                If Convert.ToBoolean(.Item("in_fav")) Then
                    lnk_my_auction.CommandName = "del"
                    lnk_my_auction.Text = "Remove From My Live Auctions"
                Else
                    lnk_my_auction.CommandName = "ins"
                    lnk_my_auction.Text = "Add To My Live Auctions"
                End If

                If .Item("filename").ToString <> "" Then
                    'ltr_auc_img.Text = "<li><a href='#'><img src='/upload/auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString.Replace(".", "_95.") & "' data-large='/upload/auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString.Replace(".", "_375.") & "' alt='image01' data-description='' /></a></li>"
                    'CType(e.Item.FindControl("img_image"), Image).ImageUrl = "/upload/auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString.Replace(".", "_thumb1.")
                    str = "SELECT A.auction_id,convert(varchar,A.auction_id)+'/'+convert(varchar,A.stock_image_id)+'/'+A.filename as path, b.title "
                    str = str & " from tbl_auction_stock_images A  WITH (NOLOCK) inner join tbl_auctions b WITH (NOLOCK) on a.auction_id=b.auction_id where A.auction_id=" & auction_id & "order by isnull(position,999)"
                    Dim dtImage As DataTable
                    dtImage = SqlHelper.ExecuteDatatable(str)
                    If dtImage.Rows.Count > 0 Then
                        imgMain.imageURL = SqlHelper.of_FetchKey("ServerHttp") & "/upload/auctions/stock_images/" & dtImage.Rows(0)("path").ToString.Replace(".", "_230.")
                    End If
                    'Response.Write(str)
                    ' rpt_auction_stock_image.DataSource = SqlHelper.ExecuteDatatable(str)
                    'rpt_auction_stock_image.DataBind()
                    'CType(e.Item.FindControl("a_img_product"), HtmlAnchor).HRef = "/upload/auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename")
                    'CType(e.Item.FindControl("a_img_product"), HtmlAnchor).Title = .Item("title")
                    'If is_grid_view Then
                    '    CType(e.Item.FindControl("a_img_product"), HtmlAnchor).Attributes.Add("rel", "lightbox[PerfumeSize" & auction_id & "1]")

                    'Else
                    '    CType(e.Item.FindControl("a_img_product"), HtmlAnchor).Attributes.Add("rel", "lightbox[PerfumeSize" & auction_id & "0]")

                    'End If
                    'rel = auction_id
                Else
                    ' ltr_auc_img.Text = "<li><a href='#'><img src='/images/imagenotavailable.gif' data-large='/images/imagenotavailable.gif' alt='image01' data-description='' /></a></li>"
                    ' CType(e.Item.FindControl("img_image"), Image).ImageUrl = "/images/imagenotavailable.gif"
                End If

                Dim bid_amount As Double = 0
                Dim bidder_max_amount As Double = 0
                hid_auction_type.Value = .Item("auction_type_id")

                lit_ask_question.Text = "" ' "<a href=""javascript:void(0);"" class='ask' onclick=""return openAskQuestion('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "');"">Ask a Question</a>"
                ' lit_ask_question.Text = "<span class='.askbx_print'>Ask a Question</span>"


                'panel setting
                If .Item("auction_type_id") = 1 Then
                    'CType(e.Item.FindControl("pnl_proxy"), Panel).Visible = False
                    'CType(e.Item.FindControl("pnl_quote"), Panel).Visible = False
                    iframe_auction_amount.Visible = True
                    iframe_auction_amount.Attributes.Add("src", "/frame_auction_amount.aspx?is_frontend=1&i=" & auction_id)
                    iframe_auction_rank.Visible = False
                    iframe_price_summary.Visible = False
                    txt_max_bid_amt.Visible = False
                    ltr_buy_qty_or_caption.Text = "Qty "
                    ddl_buy_now_qty.Visible = True
                    'CType(e.Item.FindControl("lit_buy_now_price"), Literal).Text = "$" & FormatNumber(.Item("show_price"), 2)
                    but_main_auction.Text = "Buy Now"
                    but_main_auction.CommandName = "buy"
                    'but_main_auction.OnClientClick = "return openOffer();"
                    ltr_bid_note.Text = "Your offer to buy it now is a non-revocable offer to enter into a binding contract with PCS. If, at the sole discretion of PCS, your offer to buy it now is accepted, you have entered into a legally binding contract with PCS and are contractually obligated to purchase the item(s)."
                ElseIf .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then

                    iframe_auction_amount.Visible = True
                    iframe_auction_amount.Attributes.Add("src", "/frame_auction_amount.aspx?is_frontend=1&i=" & auction_id)
                    load_bid_history(auction_id)
                    'discuss CType(e.Item.FindControl("ltr_proxy_verbiage"), Literal).Text = IIf(.Item("auction_type_id") = 3, "<div style='text-align: left;font-weight:bold; color:gray'>Your bid is a non-revocable offer to enter into a binding contract with PCS, and if the bid is the winning bid and is accepted by PCS, at PCS' sole discretion, you have entered into a legally binding contract with PCS and are obligated to purchase the item(s).</div>", "")
                    'ltr_bidder_history.Text = "<a id=""bidhistory"" class='hstry' href=""#history-popup"">View Bid History</a>"
                    iframe_price_summary.Visible = True
                    iframe_price_summary.Attributes.Add("src", "/frame_price_summary.aspx?j=1&i=" & auction_id)
                    iframe_auction_rank.Visible = True
                    iframe_auction_rank.Attributes.Add("src", "/frame_rank.aspx?is_frontend=1&i=" & auction_id)
                    txt_max_bid_amt.Visible = True
                    ddl_buy_now_qty.Visible = False
                    but_main_auction.Text = "Bid Now"
                    but_main_auction.CommandName = "bid"

                    'If .Item("auction_status") = 1 Or .Item("auction_status") = 3 Then
                    '    ltr_bidder_history.Visible = True
                    '    ltr_bidder_history.Text = "<a href=""javascript:void(0);"" class='hstry' onclick=""return open_pop_win('/Bid_History.aspx','" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&pop=1');"">View Bid History</a>"
                    'End If

                    'If .Item("auction_status") = 1 Then
                    '    ltr_buy_qty_or_caption.Text = "<a href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>The bidder will be able to set the maximum amount he is willing to pay and the system will bid on the bidder’s behalf. Other bidders will not know his maximum amount. The system will place bids on his behalf using the automatic bid increment amount in order to surpass the current high bid. The system will bid only as much as is necessary to make sure he remains the high bidder or to meet the reserve price, up to his maximum amount. If another bidder places the same maximum bid or higher, the system will notify the bidder so he can place another bid.</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"
                    'Else
                    '    ltr_buy_qty_or_caption.Text = ""
                    'End If

                    Dim dtHBid As New DataTable()
                    dtHBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & auction_id & " order by bid_amount desc")

                    Dim c_bid_amount As Double = 0
                    If dtHBid.Rows.Count > 0 Then
                        c_bid_amount = dtHBid.Rows(0).Item("bid_amount")
                    End If

                    Dim bidder_price As Double = 0
                    Dim bidder_max_price As Double = 0

                    Dim used_shipping_value As String = ""
                    Dim used_shipping_amount As Double = 0
                    Dim used_bid_amount As Double = 0

                    Dim dtBid As New DataTable()
                    'Response.Write("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount,isnull(shipping_value,'') as shipping_value,isnull(shipping_amount,0) as shipping_amount from tbl_auction_bids where auction_id=" & auction_id & " and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by bid_amount desc")
                    dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount,isnull(shipping_value,'') as shipping_value,isnull(shipping_amount,0) as shipping_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & auction_id & " and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by bid_amount desc")


                    If dtBid.Rows.Count > 0 Then

                        'Dim pnl_fedex As Panel = CType(e.Item.FindControl("pnl_fedex"), Panel)
                        If .Item("auction_type_id") = 3 Then
                            bidder_max_amount = dtBid.Rows(0).Item("max_bid_amount")
                        Else
                            bidder_max_amount = dtBid.Rows(0).Item("bid_amount")
                        End If

                        used_shipping_value = dtBid.Rows(0).Item("shipping_value")
                        used_shipping_amount = dtBid.Rows(0).Item("shipping_amount")
                        used_bid_amount = dtBid.Rows(0).Item("bid_amount")

                    End If
                    dtBid = Nothing
                    'ltr_amt_help.Text = "<a href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>Why wasn’t my amount accepted?<br />Your maximum bid may not be accepted if the amount is:<br />1. Less than what the auction must start at (" & FormatCurrency(.Item("start_price"), 2) & ")<br />2. Less than the current winning bid (" & FormatCurrency(c_bid_amount, 2) & ").<br />3. An invalid increment amount (" & FormatCurrency(.Item("increament_amount"), 2) & ").<br />4. The same maximum bid of another bidder.</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"
                    'img_amt_help.Attributes.Add("onmouseout", "hide_tip_new();")
                    'img_amt_help.Attributes.Add("onmouseover", "tip_new('Why wasn’t my amount accepted?<br />Your maximum bid may not be accepted if the amount is:<br />1. Less than what the auction must start at (" & FormatCurrency(.Item("start_price"), 2) & ")<br />2. Less than the current winning bid (" & FormatCurrency(c_bid_amount, 2) & ").<br />3. An invalid increment amount (" & FormatCurrency(.Item("increament_amount"), 2) & ").<br />4. The same maximum bid of another bidder.;")

                    If dtHBid.Rows.Count > 0 Then
                        'running bid
                        bid_amount = dtHBid.Rows(0).Item("bid_amount")
                        If .Item("auction_type_id") = 2 Then
                            ' enter_amount = dtHBid.Rows(0).Item("bid_amount") + .Item("increament_amount")
                        Else
                            If bidder_max_amount = 0 Then 'bidder bid first time
                                bidder_max_amount = dtHBid.Rows(0).Item("bid_amount") + .Item("increament_amount")
                            Else
                                If bidder_max_amount <= dtHBid.Rows(0).Item("bid_amount") Then '2nd position
                                    bidder_max_amount = dtHBid.Rows(0).Item("bid_amount") + .Item("increament_amount")
                                End If
                            End If
                        End If
                    Else
                        'first bid for this auction
                        bidder_max_amount = .Item("start_price")
                        'enter_amount = .Item("start_price")
                    End If

                    dtHBid = Nothing


                    'Shipping Setting start
                    If .Item("auction_status") = 1 Then
                        'if currently running or upcoming accept prebid

                        dv_shipping.Visible = True
                        ddl_shipping.Items.Clear()
                        Dim litm As ListItem
                        Dim ship_select As Boolean = False
                        If .Item("use_pcs_shipping") = True Then


                            If .Item("use_your_shipping") = True Then
                                litm = New ListItem
                                litm.Value = "1"
                                litm.Text = "Use my own shipping carrier to ship this item to me"
                                'litm.Attributes.Add("onclick", "return changeShippingOption('1','0');")
                                ddl_shipping.Items.Add(litm)
                            End If

                            'Dim rbt As RadioButtonList = CType(e.Item.FindControl("rbt_fedex_options"), RadioButtonList)
                            Dim dtShip As New DataTable
                            dtShip = SqlHelper.ExecuteDatatable("SELECT	ISNULL(A.shipping_options, '') AS shipping_options,ISNULL(A.shipping_amount, 0) AS shipping_amount FROM tbl_auction_fedex_rates A WITH (NOLOCK) WHERE AUCTION_ID=" & auction_id & " AND BUYER_ID=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and is_error=0 ORDER BY shipping_amount")

                            If dtShip.Rows.Count > 0 Then

                                For i As Integer = 0 To dtShip.Rows.Count - 1
                                    litm = New ListItem
                                    litm.Value = FormatNumber(dtShip.Rows(i)("shipping_amount"), 2)
                                    litm.Text = dtShip.Rows(i)("shipping_options").ToString().Replace("_", " ") & " " & FormatCurrency(dtShip.Rows(i)("shipping_amount"), 2)
                                    'litm.Attributes.Add("onclick", "return changeShippingOption('" & dtShip.Rows(i)("shipping_options").ToString() & "','" & FormatCurrency(dtShip.Rows(i)("shipping_amount"), 2) & "');")
                                    If used_shipping_value.IndexOf("shipping option:") > 0 Then
                                        If used_shipping_amount = dtShip.Rows(i)("shipping_amount") Then
                                            litm.Selected = True
                                            ship_select = True
                                        Else
                                            litm.Selected = False
                                        End If
                                    End If
                                    ddl_shipping.Items.Add(litm)
                                Next
                            Else
                                'e.Item.FindControl("pnl_fedex").Visible = False
                                litm = New ListItem
                                litm.Value = "2"
                                litm.Text = "Shipping rates will be calculated later"
                                'litm.Attributes.Add("onclick", "return changeShippingOption('2','0');")
                                ddl_shipping.Items.Add(litm)
                            End If

                        Else
                            'only my shipping available
                            litm = New ListItem
                            litm.Value = "1"
                            litm.Text = "Use my own shipping carrier to ship this item to me"
                            'litm.Attributes.Add("onclick", "return changeShippingOption('1','0');")
                            ddl_shipping.Items.Add(litm)
                        End If
                        litm = New ListItem
                        litm.Value = "0"
                        litm.Text = "--Shipping Options--"
                        'litm.Attributes.Add("onclick", "return changeShippingOption('0','0');")
                        ddl_shipping.Items.Insert(0, litm)


                        Dim asd As String
                        For kount As Integer = 0 To ddl_shipping.Items.Count - 1
                            asd = "bimal" & ddl_shipping.Items(kount).Text
                            If asd.IndexOf(.Item("last_shipping_option")) > 0 Then
                                ddl_shipping.ClearSelection()
                                ddl_shipping.Items(kount).Selected = True
                            End If
                        Next
                    Else
                        'if bid over
                        dv_shipping.Visible = False

                    End If

                    'CType(e.Item.FindControl("your_current_bid"), Label).Text = FormatCurrency(used_bid_amount, 2)
                    'hid_total_bid_amt.Value = FormatNumber(used_shipping_amount, 2)
                    'CType(e.Item.FindControl("hid_your_total_bid"), Label).Text = "Total:&nbsp;&nbsp;&nbsp;&nbsp;" & FormatCurrency(used_bid_amount + used_shipping_amount, 2)


                    'Shipping Setting End


                Else
                    iframe_auction_amount.Visible = False
                    ltr_quote_msg.Text = "<p style='padding: 0 20px 15px;'>" & .Item("request_for_quote_message") & "</p>"
                    iframe_auction_rank.Visible = False
                    iframe_price_summary.Visible = False
                    txt_max_bid_amt.Visible = False
                    ltr_buy_qty_or_caption.Text = ""
                    ddl_buy_now_qty.Visible = False
                    'CType(e.Item.FindControl("lit_buy_now_price"), Literal).Text = "$" & FormatNumber(.Item("show_price"), 2)
                    but_main_auction.Text = "Send Offer"
                    but_main_auction.CommandName = "offer"
                    ltr_bid_note.Text = "Your offer is a non-revocable offer to enter into a binding contract with PCS. If, at the sole discretion of PCS, your offer is accepted, you have entered into a legally binding contract with PCS and are contractually obligated to purchase the item(s)."
                    'CType(e.Item.FindControl("pnl_quote"), Panel).Visible = True
                    'ltr_short_desc.Text = ltr_short_desc.Text & IIf(.Item("request_for_quote_message").ToString.Trim <> "", "<br><br>" & .Item("request_for_quote_message"), "")
                    'CType(e.Item.FindControl("pnl_now"), Panel).Visible = False
                    'CType(e.Item.FindControl("pnl_proxy"), Panel).Visible = False

                End If


                'bid button setting
                If .Item("auction_status") = 1 Then
                    Dim buy_it_now_bid As Integer = SqlHelper.ExecuteScalar("select count(*) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and status='Accept' and auction_id=" & auction_id)
                    'CType(e.Item.FindControl("img_bid_now"), ImageButton).Visible = True
                    'ltr_current_status.Text = "<div style='padding: 0 25px 5px;font-size:29px;font-family:dinregular,arial;color:#ED660D;'>Running</div><p><span class='txtx' style='padding-left:25px;'>Closing on : </span>" & Convert.ToDateTime(.Item("display_end_time")).ToShortDateString() & "</p>"
                    ltr_current_status.Text = "<div class=""txt2"" style='padding-left:25px; padding-top:10px;'><iframe id=""iframe_auction_time"" scrolling=""no"" frameborder=""0"" width=""100%"" height=""50px"" src=""/frame_auction_time.aspx?p=d&is_frontend=1&i=" & auction_id & "&m=" & Request("m") & """></iframe></div><p><span class='txtx' style='padding-left:25px;'>Closing on : </span>" & Convert.ToDateTime(.Item("display_end_time")).ToShortDateString() & "</p>"

                    'chk_accept_bid_now.Visible = True
                    If .Item("auction_type_id") = 1 Then
                        'CType(e.Item.FindControl("img_bid_now"), ImageButton).AlternateText = "Buy Now"
                        'CType(e.Item.FindControl("tr_box_caption"), HtmlTableRow).Visible = False
                        'CType(e.Item.FindControl("tr_bid_now"), HtmlTableRow).Visible = False
                        'CType(e.Item.FindControl("img_bid_now"), ImageButton).ImageUrl = "/images/fend/7.png"

                        Dim rem_qty As Integer = 0
                        If .Item("qty_per_bidder") > 0 Then
                            Dim already_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and auction_id=" & auction_id & "")
                            rem_qty = .Item("qty_per_bidder") - already_buy
                        End If
                        Dim total_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and auction_id=" & auction_id & "")
                        If .Item("total_qty") - total_buy < rem_qty Then
                            rem_qty = .Item("total_qty") - total_buy
                        End If

                        'Response.Write(rem_qty)
                        If rem_qty > 0 Then
                            For i = 1 To rem_qty
                                ddl_buy_now_qty.Items.Insert(i - 1, New ListItem(i, i))

                            Next

                            'CType(e.Item.FindControl("pnl_buy_now_confirm"), Panel).Visible = True
                            'CType(e.Item.FindControl("lit_buy_now_amount_confirm"), Literal).Text = FormatCurrency(.Item("show_price"), 2)
                            hid_buy_now_confirm_amount.Value = .Item("show_price")
                        Else
                            ' CType(e.Item.FindControl("pnl_buy_now_confirm"), Panel).Visible = False

                            but_main_auction.Visible = False
                            ddl_buy_now_qty.Visible = False
                            ltr_buy_qty_or_caption.Text = ""
                            ltr_buynow_close.Visible = True
                        End If

                        but_main_auction.OnClientClick = "return redirectConfirm('" & ddl_buy_now_qty.ClientID & "'," & .Item("show_price") & "," & auction_id & ",'buy now')"
                        If Convert.ToBoolean(.Item("is_private_offer")) Then
                            'iframe_auction_window.Attributes.Add("src", "/frame_auction_bid.aspx?a=" & auction_id & "&o=2&t=" & .Item("auction_type_id") & "&p=" & .Item("show_price"))
                            'ltr_buy_now_pop_heading.Text = "<span class='blue'>Buy Now:</span> " & .Item("title") & ""
                            'ltr_offer_button.Text = "<a id='buynow' href='" & "/frame_auction_bid.aspx?a=" & auction_id & "&o=2&t=" & .Item("auction_type_id") & "&p=" & .Item("show_price") & "' class='fancybox fancybox.iframe bynw'>Buy It Now</a>" '<a href=""javascript:void(0)"" onclick=""return openOffer('" & auction_id & "','2','" & .Item("auction_type_id") & "','" & .Item("show_price") & "');""><img src='/Images/fend/4.png' alt='Private Offer' border='0'></a>"
                            'ltr_offer_button.Text = "<a class='bynw'href=""javascript:void(0)"" onclick=""return openOffer('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','eGfVCrPSj3gueIx0aTb0gg==','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','" & .Item("show_price") & "');"">Buy It Now</a>"

                        End If
                        If Convert.ToBoolean(.Item("is_partial_offer")) Then
                            'iframe_auction_window.Attributes.Add("src", "/frame_auction_bid.aspx?a=" & auction_id & "&o=3&t=" & .Item("auction_type_id") & "&p=" & .Item("show_price"))
                            'ltr_buy_now_pop_heading.Text = "<span class='blue'>Buy Now:</span> " & .Item("title") & ""
                            'ltr_offer_button.Text = "<a id='buynow'  href='" & "/frame_auction_bid.aspx?a=" & auction_id & "&o=3&t=" & .Item("auction_type_id") & "&p=" & .Item("show_price") & "' class='fancybox fancybox.iframe bynw'>Buy It Now</a>"
                            ' ltr_offer_button.Text = "<a  class='bynw'  href=""javascript:void(0)"" onclick=""return openOffer('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','W8xozjR//_nRE6AwnpleGA==','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','" & .Item("show_price") & "');"">Buy It Now</a>"

                        End If

                    ElseIf .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then
                        'Dim img_amt_help As Image = CType(e.Item.FindControl("img_amt_help"), Image)
                        'Dim refrid As String = CType(e.Item.FindControl("btn_refresh"), Button).ClientID
                        'Dim txtid As String = CType(e.Item.FindControl("txt_bid_now"), TextBox).ClientID
                        'Dim _qid As String = CType(e.Item.FindControl("rbt_fedex_options"), RadioButtonList).ClientID
                        Dim cmpVal As String = bidder_max_amount
                        iframe_auction_window.Visible = True
                        'Dim divid As String = CType(e.Item.FindControl("div_Validator"), System.Web.UI.HtmlControls.HtmlGenericControl).ClientID
                        'CType(e.Item.FindControl("img_bid_now"), ImageButton).ImageUrl = "/images/fend/6.png"
                        'CType(e.Item.FindControl("txt_bid_now"), TextBox).Visible = True
                        iframe_auction_window.Attributes.Add("src", "")
                        'but_main_auction.OnClientClick = "return validBid('" & auction_id & "','" & txt_max_bid_amt.ClientID & "','" & cmpVal & "','" & div_Validator.ClientID & "','0','" & .Item("auction_type_id") & "','" & hid_shipping_option.ClientID & "','" & hid_shipping_value.ClientID & "','img_amt_help','" & iframe_auction_window.ClientID & "','" & btn_history.ClientID & "');"


                        ViewState("auction_type_id") = .Item("auction_type_id")
                        ViewState("cmpVal") = cmpVal

                        but_main_auction.Attributes.Add("onclick", "return openAuctionBid('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','" & cmpVal & "','" & txt_max_bid_amt.ClientID & "','" & ddl_shipping.ClientID & "')")
                        ltr_bid_note.Text = IIf(.Item("auction_type_id") = 3, "Your bid is a non-revocable offer to enter into a binding contract with PCS. If, at the sole discretion of PCS, your bid is accepted and selected as the winning bid, you have entered into a legally binding contract with PCS and are contractually obligated to purchase the item(s).", "")
                        ' iframe_auction_window.Attributes.Add("src", "/frame_auction_bid.aspx?a=" & auction_id & "&o=0&t=" & .Item("auction_type_id") & "&p=" & .Item("show_price") & "&r=" & btn_refresh_bid.ClientID)
                        If bid_amount < .Item("thresh_hold_value") Or .Item("thresh_hold_value") = 0 Then ' CHANGE BY SANDEEP (NEED TO DISCUSS)

                            'And buy_it_now_bid < .Item("no_of_clicks") And bid_amount < .Item("buy_now_price")
                            If Convert.ToBoolean(.Item("is_buy_it_now")) Then

                                Dim rem_qty As Integer = 0
                                If .Item("qty_per_bidder") > 0 Then
                                    Dim already_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and auction_id=" & auction_id & "")
                                    rem_qty = .Item("qty_per_bidder") - already_buy
                                End If

                                Dim total_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and auction_id=" & auction_id & "")
                                If .Item("total_qty") - total_buy < rem_qty Then
                                    rem_qty = .Item("total_qty") - total_buy
                                End If

                                If rem_qty > 0 Then
                                    'ltr_buy_now_pop_heading.Text = "<span class='blue'>Buy Now:</span> <span style='font-size:" & IIf(.Item("title").ToString.Length < 70, "22px", IIf(.Item("title").ToString.Length < 150, "18px", IIf(.Item("title").ToString.Length < 200, "16px", "14px"))) & ";'>" & .Item("title") & "</span>"
                                    'ltr_offer_button.Text = "<a id='buynow' href='#buynow-popup' class='bynw'>Buy It Now</a>" '
                                    'For i = 1 To rem_qty
                                    'ddl_buy_pop_qty.Items.Insert(i - 1, New ListItem(i, i))
                                    'Next
                                    'hid_buy_now_pop_amount.Value = .Item("buy_now_price")
                                    'ltr_buy_pop_amt.Text = FormatNumber(.Item("buy_now_price"), 2)
                                    'btn_buy_now.CommandName = "buy"
                                    'btn_buy_now.OnClientClick = "return redirectBuyNow('" & ddl_buy_pop_qty.ClientID & "'," & .Item("buy_now_price") & "," & auction_id & ",'buy now')"
                                    ' ltr_offer_button.Text = "<a class='bynw' href=""javascript:void(0)"" onclick=""return openOffer('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','I82X5exjK422E8H_qZdAJg==','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','" & .Item("buy_now_price") & "');"">Buy It Now</a>"
                                Else
                                    ' ltr_offer_button.Text = ""
                                    'CType(e.Item.FindControl("pnl_buy_now"), Panel).Visible = False
                                End If

                                'vish
                                'CType(e.Item.FindControl("lit_offer"), Literal).Text = "<a href=""javascript:void(0)"" onclick=""return openOffer('" & auction_id & "','1','" & .Item("auction_type_id") & "','" & .Item("buy_now_price") & "');""><img src='/Images/fend/4.png' alt='Buy it Now' border='0'></a>"
                            End If

                            If Convert.ToBoolean(.Item("is_private_offer")) Then
                                'ltr_buy_now_pop_heading.Text = "<span class='blue'>Buy Now:</span> " & .Item("title") & ""
                                'ltr_offer_button.Text = "<a class='bynw' href=""javascript:void(0)"" onclick=""return openOffer('" & auction_id & "','2','" & .Item("auction_type_id") & "','" & .Item("show_price") & "','" & iframe_auction_window.ClientID & "');"">Buy It Now</a>" 
                                'ltr_offer_button.Text = "<a class='bynw fancybox fancybox.iframe' href='" & "/frame_auction_bid.aspx?a=" & auction_id & "&o=2&t=" & .Item("auction_type_id") & "&p=" & .Item("show_price") & "'>Buy It Now</a>"
                                ' ltr_offer_button.Text = "<a class='bynw' href=""javascript:void(0)"" onclick=""return openOffer('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','eGfVCrPSj3gueIx0aTb0gg==','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','" & .Item("show_price") & "');"">Buy It Now</a>"

                            End If
                            If Convert.ToBoolean(.Item("is_partial_offer")) Then
                                'ltr_buy_now_pop_heading.Text = "<span class='blue'>Buy Now:</span> " & .Item("title") & ""
                                'ltr_offer_button.Text = "<a class='bynw' href=""javascript:void(0)"" onclick=""return openOffer('" & auction_id & "','3','" & .Item("auction_type_id") & "','" & .Item("show_price") & "','" & iframe_auction_window.ClientID & "');"">Buy It Now</a>" 
                                'ltr_offer_button.Text = "<a class='bynw fancybox fancybox.iframe' href='" & "/frame_auction_bid.aspx?a=" & auction_id & "&o=3&t=" & .Item("auction_type_id") & "&p=" & .Item("show_price") & "'>Buy It Now</a>"
                                ' ltr_offer_button.Text = "<a class='bynw' href=""javascript:void(0)"" onclick=""return openOffer('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','W8xozjR//_nRE6AwnpleGA==','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','" & .Item("show_price") & "');"">Buy It Now</a>"

                            End If

                        End If

                    Else
                        'but_main_auction.OnClientClick = "return openOffer('" & auction_id & "','0','" & .Item("auction_type_id") & "','0','" & iframe_auction_window.ClientID & "');"
                        but_main_auction.Attributes.Add("onclick", "return openOffer('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','ti0i8u6Oh_l2RKo9llO2UQ==','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','0')")
                        btn_buy_now.Text = "Send Offer"
                        btn_buy_now.CommandName = "offer"
                        ltr_buy_now_pop_heading.Text = "<span class='blue'>Offer:</span> " & .Item("title") & ""

                    End If

                    dv_bidding.Visible = True
                    ' dv_bidding1.Visible = True
                ElseIf .Item("auction_status") = 2 Then
                    but_main_auction.Visible = False
                    ltr_timer_caption.Text = "Starts In"
                    'ltr_current_status.Text = "<div style='padding: 0 25px 5px;font-size:29px;font-family:dinregular,arial;color:#0084B2;'>UpComing</div><p><span class='txtx' style='padding-left:25px;'>Start on : </span>" & Convert.ToDateTime(.Item("start_date")).ToShortDateString() & "</p>"
                    ltr_current_status.Text = "<div class=""txt2"" style='padding-left:25px; padding-top:10px;'><iframe id=""iframe_auction_time"" scrolling=""no"" frameborder=""0"" width=""100%"" height=""50px"" src=""/frame_auction_time.aspx?p=d&is_frontend=1&i=" & auction_id & "&m=" & Request("m") & """></iframe></div><p><span class='txtx' style='padding-left:25px;'>Start on : </span>" & Convert.ToDateTime(.Item("start_date")).ToShortDateString() & "</p>"
                ElseIf .Item("auction_status") = 3 Then
                    ltr_timer_caption.Text = ""
                    but_main_auction.Visible = False
                    'iframe_auction_rank.Visible = False
                    'ltr_current_status.Text = "<div style='padding: 0 25px 5px;font-size:29px;font-family:dinregular,arial;color:red;'>Closed</div><p><span class='txtx' style='padding-left:25px;'>Closed on : </span>" & Convert.ToDateTime(.Item("display_end_time")).ToShortDateString() & "</p>"
                    ltr_current_status.Text = "<div class=""txt2"" style='padding-left:25px; padding-top:10px;'><iframe id=""iframe_auction_time"" scrolling=""no"" frameborder=""0"" width=""100%"" height=""50px"" src=""/frame_auction_time.aspx?p=d&is_frontend=1&i=" & auction_id & "&m=" & Request("m") & """></iframe></div><p><span class='txtx' style='padding-left:25px;'>Closed on : </span>" & Convert.ToDateTime(.Item("display_end_time")).ToShortDateString() & "</p>"
                    'iframe_auction_time.Visible = False
                End If

                iframe_auction_time.Attributes.Add("src", "/frame_auction_time.aspx?p=d&is_frontend=1&i=" & auction_id)
            End With
            'Dim dt As New DataTable()
            'dt = SqlHelper.ExecuteDatatable("select product_item_attachment_id, filename from tbl_auction_product_item_attachments where auction_id=" & auction_id & " order by product_item_attachment_id desc")
            'If dt.Rows.Count > 0 Then
            '    If dt.Rows(0).Item("filename") <> "" Then
            '        CType(e.Item.FindControl("lit_item_attachment"), Literal).Text = "<a href='/upload/Auctions/product_items/" & auction_id & "/" & dt.Rows(0).Item("product_item_attachment_id") & "/" & dt.Rows(0).Item("filename") & "' target='_blank'>" & dt.Rows(0).Item("filename") & "</a>"
            '    End If
            'End If
            'dt.Dispose()

        Else
            but_main_auction.Text = "No Auction"
            but_main_auction.OnClientClick = "return false;"
            Response.Redirect("/Auction_Listing.aspx?t=1")
        End If
        dtTable = Nothing
    End Sub

    Private Sub setAuctiondetails(ByVal auction_id As Integer)

        Dim lit_Summary As String = ""
        Dim lit_Details As String = ""
        Dim lit_terms_condition As String = ""
        Dim str As String = ""
        Dim dt As New DataTable()
        Dim i As Integer

        'Dim rptItems As Repeater = CType(e.Item.FindControl("rptItems"), Repeater)
        '----------item bind start
        Dim lit_Items As String = ""


        dt = SqlHelper.ExecuteDatatable("select [product_item_id], isnull(m.[description],'') AS manufacturer, isnull([part_no],'')  AS part_no, isnull(p.[description],'') AS description, isnull(quantity,0) as quantity,ISNULL(p.name,'') AS title,ISNULL(p.upc,'') As UPC,ISNULL(p.sku,'') As SKU,ISNULL(estimated_msrp,0) AS estimated_msrp,isnull(total_msrp,0) As total_msrp from [tbl_auction_product_items] p WITH (NOLOCK) left join tbl_master_manufacturers m WITH (NOLOCK) on p.manufacturer_id=m.manufacturer_id where auction_id= " & auction_id)

        If dt.Rows.Count > 0 Then
            rptItems.DataSource = dt
            rptItems.DataBind()
        Else
            dv_items_details.Visible = False
        End If

        'For i = 0 To dt.Rows.Count - 1
        '    With dt.Rows(i)
        '        lit_Items = lit_Items & "<tr><td><div class='grBackDescription'><b>" & .Item("manufacturer") & "</b>&nbsp;<span class='auctionPartNo'>Part No. " & .Item("part_no") & "</span> &nbsp;<span class='auctionPartNo'>Quantity " & .Item("quantity") & "</span>" & .Item("description") & "</div></td></tr>"
        '    End With
        'Next
        dt.Dispose()
        ''dt = SqlHelper.ExecuteDatatable("select product_item_attachment_id, filename from tbl_auction_product_item_attachments where auction_id=" & auction_id & " order by product_item_attachment_id desc")
        ''If dt.Rows.Count > 0 Then
        ''    If dt.Rows(0).Item("filename") <> "" Then
        ''        lit_Items = lit_Items & "<tr><td><a href='/upload/Auctions/product_items/" & auction_id & "/" & dt.Rows(0).Item("product_item_attachment_id") & "/" & dt.Rows(0).Item("filename") & "' target='_blank'>" & dt.Rows(0).Item("filename") & "</a></td></tr>"
        ''    End If
        ''End If
        ''dt.Dispose()
        'If lit_Items <> "" Then
        '    lit_Items = "<table width='100%'>" & lit_Items & "</table>"
        '    ' CType(e.Item.FindControl("lit_Items"), Literal).Text = lit_Items
        '    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(1).Visible = True
        'Else
        '    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(1).Visible = False
        'End If
        'If rptItems.Items.Count > 0 Then
        '    'lit_Items = "<table width='100%'>" & lit_Items & "</table>"
        '    ' CType(e.Item.FindControl("lit_Items"), Literal).Text = lit_Items
        '    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(1).Visible = True
        'Else
        '    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(1).Visible = False
        'End If
        '----------item bind end


        Dim strQry As String = "SELECT A.auction_id,"
        strQry = strQry & "ISNULL(A.tax_details, '') AS tax_details,"
        ' strQry = strQry & "ISNULL(L.name, '') AS stock_location,"
        strQry = strQry & "ISNULL(A.description, '') AS description,"
        strQry = strQry & "ISNULL(A.special_conditions, '') AS special_conditions,"
        strQry = strQry & "ISNULL(A.payment_terms, '') AS payment_terms,"
        strQry = strQry & "ISNULL(A.product_details, '') AS product_details,"
        strQry = strQry & "ISNULL(A.shipping_terms, '') AS shipping_terms,"
        strQry = strQry & "ISNULL(A.other_terms, '') AS other_terms,"
        strQry = strQry & "ISNULL(A.after_launch_message, '') AS after_launch_message,"
        strQry = strQry & "ISNULL(A.start_date, '1/1/1900') AS start_date,"
        strQry = strQry & "ISNULL(A.display_end_time, '1/1/1900') AS display_end_time,"
        strQry = strQry & "ISNULL(A.is_display_start_date, 0) AS is_display_start_date,"
        strQry = strQry & "ISNULL(A.is_display_end_date, 0) AS is_display_end_date,"
        strQry = strQry & "ISNULL(B.name, '') AS stock_condition,"
        strQry = strQry & "ISNULL(L.name, '') AS stock_location,"
        strQry = strQry & "ISNULL(L.CITY, '') AS city,"
        strQry = strQry & "ISNULL(L.state, '') AS state,"
        strQry = strQry & "ISNULL(C.name, '') AS packaging,"
        strQry = strQry & "ISNULL(D.company_name, '') AS seller,"
        strQry = strQry & "ISNULL(A.[after_launch_filename],'') as [after_launch_filename],"
        'strQry = strQry & "ISNULL(A.request_for_quote_message, '') AS request_for_quote_message,"
        strQry = strQry & "ISNULL(A.short_description, '') AS short_description"
        strQry = strQry & " FROM "
        strQry = strQry & "tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations L WITH (NOLOCK) on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B WITH (NOLOCK) on A.stock_condition_id=B.stock_condition_id "
        strQry = strQry & "Left Join tbl_master_packaging C WITH (NOLOCK) on A.packaging_id=C.packaging_id left join tbl_reg_sellers D WITH (NOLOCK) on A.seller_id=D.seller_id"
        strQry = strQry & " WHERE "
        strQry = strQry & "A.auction_id =" & auction_id

        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDatatable(strQry)
        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)

                '--------- Summary Bind start
                If Convert.ToBoolean(.Item("is_display_start_date")) Or Convert.ToBoolean(.Item("is_display_end_date")) Then

                    lit_Summary = ""

                    If Convert.ToBoolean(.Item("is_display_start_date")) Then
                        Dim dateTimeInfo As DateTime = .Item("start_date")
                        lit_Summary = lit_Summary & "<span class='txtx'>Bid Start Date: <span class='txt5'>" & dateTimeInfo.ToString("dddd, MMMM dd yyyy hh:mm tt") & "</span></span>"
                    End If

                    If Convert.ToBoolean(.Item("is_display_end_date")) Then
                        If Convert.ToBoolean(.Item("is_display_start_date")) Then
                            lit_Summary = lit_Summary & "<span class='txtx'><br />"
                        Else
                            lit_Summary = lit_Summary & "<span class='txtx'>"
                        End If

                        Dim dateTimeInfo As DateTime = .Item("display_end_time")
                        lit_Summary = lit_Summary & "Bid End Date : <span class='txt5'>" & dateTimeInfo.ToString("dddd, MMMM dd yyyy hh:mm tt") & "</span></span>"
                    End If

                    'lit_Summary = lit_Summary & "</div>"
                End If


                If .Item("after_launch_message") <> "" Then
                    str = .Item("after_launch_message")
                End If

                If .Item("after_launch_filename").ToString.Trim <> "" Then
                    If str <> "" Then str = str & "<br>"
                    ' str = str & "<a href='/upload/Auctions/after_launch/" & auction_id & "/" & .Item("after_launch_filename").ToString & "' target='_blank'>" & .Item("after_launch_filename").ToString & "</a>"
                End If
                If str <> "" Then
                    lit_Summary = lit_Summary & "<p>" & str & "</p>"
                End If


                Dim tbl_build As New StringBuilder
                tbl_build.Append(IIf(.Item("stock_condition").ToString.Trim <> "", "Stock Condition : " & .Item("stock_condition").ToString & "<br />", ""))
                tbl_build.Append(IIf(.Item("stock_location").ToString.Trim <> "", "Stock Location : " & .Item("city").ToString & ", " & .Item("state").ToString & "<br />", ""))
                tbl_build.Append(IIf(.Item("packaging").ToString.Trim <> "", "Packaging : " & .Item("packaging").ToString & "<br />", ""))
                tbl_build.Append(IIf(.Item("seller").ToString.Trim <> "", "Company : " & .Item("seller").ToString & "<br />", ""))
                If tbl_build.ToString.Trim <> "" Then
                    lit_Summary = lit_Summary & "<div class='txt6'>" & tbl_build.ToString & "</div>"
                End If
                'lit_Summary = lit_Summary & tbl_build.ToString

                dt = SqlHelper.ExecuteDatatable("SELECT attachment_id,auction_id,isnull(title,'') as title,isnull([filename],'') as [filename] FROM tbl_auction_attachments WITH (NOLOCK) WHERE for_bidder=1 and auction_id = " & auction_id & " order by upload_date desc")
                If dt.Rows.Count > 0 Then
                    lit_Summary = lit_Summary & "<p><span class='txtx'>Attachments</span><br>"
                    ' lit_Summary = lit_Summary & "<div style='padding: 0px 10px 10px 10px;'>"
                    For i = 0 To dt.Rows.Count - 1
                        With dt.Rows(i)
                            If dt.Rows(i)("filename").ToString.Trim <> "" Then
                                'lit_Summary = lit_Summary & "<a href='/Upload/Auctions/auction_attachments/" & auction_id & "/" & .Item("attachment_id") & "/" & .Item("filename") & "' target='_blank'>" & IIf(.Item("title").ToString.Trim <> "", .Item("title"), .Item("filename")) & "</a>"
                                lit_Summary = lit_Summary & IIf(.Item("title").ToString.Trim <> "", .Item("title"), .Item("filename"))
                            End If
                        End With
                    Next
                    lit_Summary = lit_Summary & "</p>"
                End If
                dt.Dispose()
                'If lit_Summary <> "" Then
                '    CType(e.Item.FindControl("lit_Summary"), Literal).Text = lit_Summary
                '    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(0).Visible = True
                'Else
                '    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(0).Visible = False
                'End If
                If lit_Summary.Trim <> "" Then
                    ltr_summary_detail.Text = "<div class='summrybx dtltxtbx'><h1 class='pgtitle'>Summary</h1>" & lit_Summary & "</div>"
                End If


                '--------- Summary Bind end
                '----------Details Tab Start
                If .Item("product_details") <> "" Then
                    'lit_Details = "<p><span class='txtx'>Product Detail</span><br><br>"
                    lit_Details = .Item("product_details")
                End If
                dt = SqlHelper.ExecuteDatatable("select product_attachment_id ,isnull(filename,'') as filename from tbl_auction_product_attachments WITH (NOLOCK) where auction_id=" & auction_id & " order by product_attachment_id desc")
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("filename").ToString.Trim <> "" Then
                        If lit_Details <> "" Then lit_Details = lit_Details & "<br>"
                        lit_Details = lit_Details & dt.Rows(0).Item("filename") '"<a target='_blank' href='/Upload/Auctions/product_attachments/" & auction_id & "/" & dt.Rows(0).Item("product_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a>"
                    End If
                End If
                If lit_Details.Trim <> "" Then
                    lit_Details = "<p><span class='txtx'>Product Detail</span><br>" & lit_Details & "</p>"
                End If
                dt.Dispose()

                If .Item("description") <> "" Then
                    lit_Details = lit_Details & "<p><span class='txtx'>Description</span><br>" & .Item("description") & "</p>"
                    'lit_Details = lit_Details & "<div style='padding: 0px 10px 10px 10px;'>"
                End If

                If .Item("short_description") <> "" Then
                    lit_Details = lit_Details & "<p><span class='txtx'>Short Description</span><br>" & .Item("short_description") & "</p>"
                    'lit_Details = lit_Details & "<div style='padding: 0px 10px 10px 10px;'>"
                End If
                If lit_Details <> "" Then
                    ltr_auction_details.Text = "<div class='terms dtltxtbx'><h1 class='pgtitle'>Details</h1>" & lit_Details & "</div>"
                End If

                '----------Details Tab end

                '----------Terms & Condition Start
                If .Item("payment_terms") <> "" Then
                    ' lit_terms_condition = "<div class='detailsSubHeading' style='font-weight:bold; padding:10px 10px 0px 10px;'>Payment Terms</div>"
                    lit_terms_condition = .Item("payment_terms")
                End If
                dt = SqlHelper.ExecuteDatatable("select payment_term_attachment_id ,isnull(filename,'') as filename from tbl_auction_payment_term_attachments WITH (NOLOCK) where auction_id=" & auction_id & " order by payment_term_attachment_id desc")
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("filename").ToString.Trim <> "" Then
                        If lit_terms_condition <> "" Then lit_terms_condition = lit_terms_condition & "<br>"
                        lit_terms_condition = lit_terms_condition & dt.Rows(0).Item("filename") '"<a target='_blank' href='/Upload/Auctions/payment_term_attachments/" & auction_id & "/" & dt.Rows(0).Item("payment_term_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a>"

                    End If
                End If
                dt.Dispose()
                If lit_terms_condition <> "" Then
                    lit_terms_condition = "<p><span class='txtx'>Payment Terms</span><br>" & lit_terms_condition & "</p>"
                End If

                str = ""
                If .Item("shipping_terms") <> "" Then
                    'str = "<div class='detailsSubHeading' style='font-weight:bold; padding:10px 10px 0px 10px;'>Shipping Terms</div>"
                    str = .Item("shipping_terms")

                End If
                dt = SqlHelper.ExecuteDatatable("select top 1 shipping_term_attachment_id,isnull(filename,'') as filename from tbl_auction_shipping_term_attachments WITH (NOLOCK) where auction_id=" & auction_id & " order by shipping_term_attachment_id desc")
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("filename").ToString.Trim <> "" Then
                        If str <> "" Then str = str & "<br>"
                        str = str & dt.Rows(0).Item("filename") '"<a target='_blank' href='/Upload/Auctions/shipping_term_attachments/" & auction_id & "/" & dt.Rows(0).Item("shipping_term_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a>"

                    End If

                End If
                If str <> "" Then
                    str = "<p><span class='txtx'>Shipping Terms</span><br>" & str & "</p>"
                End If
                dt.Dispose()

                'If str <> "" Then
                'str = "<div style='padding: 10px;'>" & str & "</div>"
                lit_terms_condition = lit_terms_condition & str
                ' End If
                str = ""


                If .Item("special_conditions") <> "" Then
                    'str = "<div class='detailsSubHeading' style='font-weight:bold; padding:10px 10px 0px 10px;'>Special Conditions</div>"
                    str = .Item("special_conditions")
                    'str = "<div class='detailsSubHeading' style='padding-bottom:10px;'><b>Special Conditions</b></div>" & .Item("special_conditions")
                End If
                dt = SqlHelper.ExecuteDatatable("select top 1 special_condition_id,isnull(filename,'') as filename from tbl_auction_special_conditions WITH (NOLOCK) where auction_id=" & auction_id & " order by special_condition_id desc")
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("filename").ToString.Trim <> "" Then
                        If str <> "" Then str = str & "<br>"
                        str = str & dt.Rows(0).Item("filename") '"<a target='_blank' href='/Upload/Auctions/special_conditions/" & auction_id & "/" & dt.Rows(0).Item("special_condition_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a>"

                    End If
                End If
                dt.Dispose()

                If str <> "" Then
                    str = "<p><span class='txtx'>Special Conditions</span><br>" & str & "</p>"
                End If
                lit_terms_condition = lit_terms_condition & str

                str = ""
                If .Item("other_terms") <> "" Then
                    'str = "<div class='detailsSubHeading' style='font-weight:bold; padding:10px 10px 0px 10px;'>Other Terms</div>"
                    str = .Item("other_terms")
                    'str = "<div class='detailsSubHeading' style='padding-bottom:10px;'><b>Other Terms</b></div>" & .Item("other_terms")
                End If
                dt = SqlHelper.ExecuteDatatable("select top 1 term_cond_attachment_id,isnull(filename,'') as filename from tbl_auction_terms_cond_attachments WITH (NOLOCK) where auction_id=" & auction_id & " order by term_cond_attachment_id desc")
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("filename").ToString.Trim <> "" Then
                        If str <> "" Then str = str & "<br>"
                        str = str & dt.Rows(0).Item("filename") '"<a target='_blank' href='/Upload/Auctions/terms_cond_attachments/" & auction_id & "/" & dt.Rows(0).Item("term_cond_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a>"

                    End If
                End If
                dt.Dispose()

                If str <> "" Then
                    str = "<p><span class='txtx'>Other Terms</span><br>" & str & "</p>"
                End If

                lit_terms_condition = lit_terms_condition & str

                str = ""
                If .Item("tax_details") <> "" Then
                    'str = "<div class='detailsSubHeading' style='font-weight:bold; padding:10px 10px 0px 10px;'>Tax Details</div>"
                    str = str & .Item("tax_details")
                    ' str = "<div class='detailsSubHeading' style='padding-bottom:10px;'><b>Tax Details</b></div>" & .Item("tax_details")
                End If
                dt = SqlHelper.ExecuteDatatable("select top 1 tax_attachment_id,isnull(filename,'') as filename from tbl_auction_tax_attachments WITH (NOLOCK) where auction_id=" & auction_id & " order by tax_attachment_id desc")
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("filename").ToString.Trim <> "" Then
                        If str <> "" Then str = str & "<br>"
                        str = str & dt.Rows(0).Item("filename") '"<a target='_blank' href='/Upload/Auctions/tax_attachments/" & auction_id & "/" & dt.Rows(0).Item("tax_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a>"

                    End If
                End If
                dt.Dispose()

                If str <> "" Then
                    str = "<p><span class='txtx'>Tax Details</span><br>" & str & "</p>"
                End If
                lit_terms_condition = lit_terms_condition & str

                'If lit_terms_condition <> "" Then
                '    ltr_term_condition.Text = "<div class='terms dtltxtbx'><h1 class='pgtitle'>Terms and Conditions</h1>" & lit_terms_condition & "</div>"
                'End If
                '----------Terms & Condition Start
                load_auction_bidder_queries(auction_id)

            End With
        Else
            'e.Item.Visible = False
        End If
        dtTable = Nothing

    End Sub
    Protected Sub fill_page(ByVal auction_id As Int32)
        Dim str As String = ""
        If Not String.IsNullOrEmpty(Request.QueryString("m")) And Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            
            If Request.QueryString("m") = "pr" Or Request.QueryString("m") = "pa" Or Request.QueryString("m") = "bu" Then

                Dim strBuy As String = "select bid_date,price,isnull(grand_total_amount,0) as grand_total_amount,isnull(quantity,0) as quantity,case ISNULL(status,'') when 'Accept' then (case ISNULL(action,'') when 'Awarded' then 'Accepted' when 'Denied' then 'Rejected' else 'Pending Decision' end) when 'Pending' then 'Pending Decision' when 'Reject' then 'Rejected' end AS status from tbl_auction_buy WITH (NOLOCK) where buy_id=" & Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString("i"))
                Dim dtBuy As DataTable = New DataTable()
                dtBuy = SqlHelper.ExecuteDatatable(strBuy)
                If dtBuy.Rows.Count > 0 Then
                    str = "<span class='txtx'>Bid Date : </span>" & Convert.ToDateTime(dtBuy.Rows(0)("bid_date")).ToShortDateString() & ""
                    str = str & "<br><span class='txtx'>Price : </span>" & "$" & FormatNumber(dtBuy.Rows(0)("grand_total_amount"), 2) & ""
                    If Request.QueryString("m") = "pa" Then
                        str = str & "<br><span class='txtx'>Qty : </span>" & dtBuy.Rows(0)("quantity") & ""
                    ElseIf Request.QueryString("m") = "bu" And dtBuy.Rows(0)("quantity") > 0 Then
                        str = str & "<br><span class='txtx'>Qty : </span>" & dtBuy.Rows(0)("quantity") & ""
                    End If
                    str = str & "<br><span class='txtx'>Status : <span class='txt5' style='color:#ED660D;'>" & dtBuy.Rows(0)("status") & "</span></span>"
                    Dim print_order_confirmation As String = ""
                    print_order_confirmation = GetGlobalResourceObject("AuctionList", "lit_print_order_confirmation")
                    'lbl_print_confirmation.Visible = True
                    'str = str & "<br><br><a class='bid' style='text-transform:none;font-size:22px;padding:8px 0 8px;width:257px;' href=""/AuctionConfirmation.aspx?b=" & Request.QueryString("i") & "&t=b"">" & print_order_confirmation & "</a>"
                End If
                dtBuy = Nothing
            ElseIf Request.QueryString("m") = "q" Then
                Dim strQuote As String = "select bid_date,details,ISNULL(filename,'') As filename, ISNULL(action,'') As status from tbl_auction_quotations WITH (NOLOCK) where quotation_id=" & Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString("i"))
                Dim dtQuote As DataTable = New DataTable()
                dtQuote = SqlHelper.ExecuteDatatable(strQuote)
                If dtQuote.Rows.Count > 0 Then
                    str = "<span class='txtx'>Bid Date : </span>" & Convert.ToDateTime(dtQuote.Rows(0)("bid_date")).ToShortDateString() & ""
                    str = str & "<p>" & dtQuote.Rows(0)("details") & "</p>"

                    If dtQuote.Rows(0)("filename") <> "" Then
                        str = str & "<br>" & dtQuote.Rows(0)("filename") ' "<br><a href='/Upload/Quotation/" & auction_id & "/" & dtQuote.Rows(0)("filename") & "' target='_blank'>" & dtQuote.Rows(0)("filename") & "</a>"
                    End If
                    If dtQuote.Rows(0)("status") <> "" Then
                        str = str & "<br><span class='txtx'>Status : <span class='txt5' style='color:#ED660D;'>" & IIf(dtQuote.Rows(0)("status") = "" Or dtQuote.Rows(0)("status").ToString().ToUpper() = "PENDING", "Pending Decision", dtQuote.Rows(0)("status")) & "</span></span>"
                    End If
                End If
                dtQuote = Nothing
            ElseIf Request.QueryString("m") = "w" Then
                str = ""
                Dim dtbid As DataTable = SqlHelper.ExecuteDatatable("select bid_date,max_bid_amount,bid_amount from tbl_auction_bids WITH (NOLOCK) where bid_id=" & Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString("i")))
                If dtbid.Rows.Count > 0 Then
                    With dtbid.Rows(0)
                        str = str & "<span style='color:green; font-family:'dinregular', arial; font-size:15px;'>Thank you for participating!</span><br><br>"
                        str = str & "<span class='txtx'>Bid Date : </span>" & Convert.ToDateTime(.Item("bid_date")).ToShortDateString() & ""
                        str = str & "<br><span class='txtx'>My Auto Bid : </span>" & FormatCurrency(.Item("max_bid_amount"), 2) & ""
                        str = str & "<br><span class='txtx'>Winning Bid : </span>" & FormatCurrency(.Item("bid_amount"), 2) & ""
                    End With
                End If
                dtbid.Dispose()

                Dim print_order_receipt As String = ""
                print_order_receipt = GetGlobalResourceObject("AuctionList", "lit_print_order_receipt")
                'lbl_print_confirmation.Visible = True
                ' str = str & "<br><br><a class='bid' style='text-transform:none;font-size:22px;padding:8px 0 8px;width:257px;' href=""/AuctionConfirmation.aspx?b=" & Request.QueryString("i") & "&t=a"">" & print_order_receipt & "</a>"
            End If
            If str <> "" Then
                'heading remove from h1 tag -- Your " & IIf(Request.QueryString("m") = "pr", "Private Offer", IIf(Request.QueryString("m") = "pa", "Partial Offer", IIf(Request.QueryString("m") = "q", "Offer", IIf(Request.QueryString("m") = "bu", "Buy Now", "Win Bid")))) & " Details
                ltr_offer_attachement.Text = "<div class='summrybx dtltxtbx' style='border:none;margin-bottom:0px;'><h1 class='pgtitle' style='padding-top:5px;min-height:0;font-family:dinregular,arial;font-size:25px;'>&nbsp;</h1><div class='txt6' style='margin:0;line-height:20px;'><p>" & str & "</p></div></div>"
            End If
            pnl_from_live.Visible = False
        Else
            pnl_from_account.Visible = False
        End If
        setAuction(auction_id, IIf(String.IsNullOrEmpty(Request.QueryString.Get("m")), "", Request.QueryString.Get("m")), IIf(String.IsNullOrEmpty(Request.QueryString.Get("q")), Request.QueryString.Get("q"), 0), IIf(String.IsNullOrEmpty(Request.QueryString.Get("b")), Request.QueryString.Get("b"), 0))
        setAuctiondetails(auction_id)
    End Sub
    Protected Sub load_auction_bidder_queries(ByVal auction_id As Integer)
        Dim dt As New DataTable
        dt = SqlHelper.ExecuteDatatable("select A.query_id,A.title,A.submit_date,A.buyer_id,B.company_name,isnull(case when B.state_id<>0 then (select name from tbl_master_states where state_id=B.state_id) else B.state_text end,'') as state from tbl_auction_queries A WITH (NOLOCK) inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id inner join tbl_auctions A1 WITH (NOLOCK) on A.auction_id=A1.auction_id where A.parent_query_id=0 and A.is_active=1 and A.auction_id=" & auction_id & " and A.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by submit_date desc")
        'Response.Write("select A.query_id,A.title,A.submit_date,A.buyer_id,B.company_name,isnull(case when B.state_id<>0 then (select name from tbl_master_states where state_id=B.state_id) else B.state_text end,'') as state from tbl_auction_queries A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id inner join tbl_auctions A1 on A.auction_id=A1.auction_id where A.parent_query_id=0 and A.is_active=1 and A.auction_id=" & auction_id & " and A.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by submit_date desc")
        rep_after_queries.DataSource = dt
        rep_after_queries.DataBind()
        If dt.Rows.Count = 0 Then
            lit_your_query.Visible = False
        End If
        dt.Dispose()
    End Sub
    Protected Sub rep_after_queries_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        If e.CommandName = "deleteQry" Then
            SqlHelper.ExecuteNonQuery("update tbl_auction_queries set is_active=0 where query_id=" & e.CommandArgument)
            load_auction_bidder_queries(hid_auction_id.Value)
            'e.Item.Visible = False
        End If
    End Sub
    Protected Sub rep_after_queries_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            CType(e.Item.FindControl("btnDelete"), ImageButton).Attributes.Add("onclick", "return confirm('Are you sure to delete this question?');")
            Dim rep_inner As Repeater = DirectCast(e.Item.FindControl("rep_inner_after_queries"), Repeater)
            rep_inner.DataSource = SqlHelper.ExecuteDatatable("select A.message, A.submit_date, ISNULL(C.first_name,'') AS first_name,ISNULL(C.last_name,'') AS last_name,ISNULL(D.first_name,'') AS buyer_first_name,ISNULL(D.last_name,'') AS buyer_last_name,isnull(C.title,'') as title,isnull(D.title,'') as buyer_title,isnull(C.image_path,'') as image_path,C.user_id from tbl_auction_queries A WITH (NOLOCK) inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id left join tbl_sec_users C WITH (NOLOCK) on A.user_id=C.user_id LEFT JOIN tbl_reg_buyer_users D WITH (NOLOCK) ON A.buyer_user_id=D.buyer_user_id where A.parent_query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & " order by submit_date DESC")
            rep_inner.DataBind()
        End If
    End Sub
    'Protected Sub auction_process_logic(ByVal auction_id As Integer, ByVal enter_amount As Double, ByVal max_amount As Double, ByVal buy_option As Integer, ByVal auction_type_id As Integer, ByVal qty As Integer)
    '    Dim str As String = validOrder(True, buy_option, auction_type_id, enter_amount, qty)
    '    If IsNumeric(buy_option) AndAlso (str = "" And buy_option <> 0) Then
    '        'if offer clicked
    '        Dim dtBid As New DataTable
    '        dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(B.bid_amount, 0) AS bid_amount,ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value from tbl_auction_bids B inner join tbl_auctions A on A.auction_id=B.auction_id  where A.auction_id=" & auction_id & " order by bid_amount desc")
    '        If dtBid.Rows.Count > 0 Then
    '            With dtBid.Rows(0)
    '                If Not (.Item("bid_amount") < .Item("thresh_hold_value")) Then
    '                    str = "Sorry, This offer is currently not available. Please bid to win this product."
    '                End If
    '            End With
    '        End If
    '    End If
    '    If str <> "" Then
    '        'ImageButtonCancelMsg.Attributes.Add("onclick", "return closeNow('0', '')")
    '        ConfirmOrder(str)
    '    Else
    '        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
    '            ' CommonCode.Fetch_Cookie_Shared("user_id") = CommonCode.Fetch_Cookie_Shared("user_id")
    '            '  hid_buyer_id.Value = CommonCode.Fetch_Cookie_Shared("buyer_id")
    '        End If
    '        Dim title As String = SqlHelper.ExecuteScalar("select title from tbl_auctions where auction_id=" & auction_id)
    '        'SetBidNowConfirm(title)
    '        'ImageButtonCancelQuote.Attributes.Add("onclick", "return closeNow('0', '')")
    '        'ImageButtonCancelOffer.Attributes.Add("onclick", "return closeNow('0', '')")
    '        If buy_option = "0" Then
    '            ' ImageButtonCancelMsg.Attributes.Add("onclick", "return closeNow('" & auction_type_id & "', '" & Request.QueryString.Get("r") & "')")
    '        Else
    '            '  ImageButtonCancelMsg.Attributes.Add("onclick", "return closeNow('0', '')")
    '        End If
    '    End If
    'End Sub
    'Private Sub BidNow(ByVal user_id As Integer, ByVal buyer_id As Integer, ByVal buy_option As Integer, ByVal auction_id As Integer, ByVal auction_type_id As Integer, ByVal enter_amount As Double, ByVal qty As Integer)
    '    Dim validate As String = validOrder(False, buy_option, auction_type_id, enter_amount, qty)
    '    If validate = "" Then

    '        Dim str As String = ""
    '        Dim obj As New Bid()
    '        Dim objEmail As New Email()
    '        Dim strShipping As String = ""
    '        Dim strShippingAmount As Double = 0
    '        If dv_shipping.Visible Then
    '            If ddl_shipping.SelectedValue = 1 Then
    '                strShipping = "My shipping carrier " 'is: " & txt_shipping_type.Text.Trim() & " And My Account # is: " & txt_shipping_account_number.Text.Trim()
    '            Else
    '                Dim dtTable As DataTable = SqlHelper.ExecuteDatatable("select ISNULL(A.shipping_amount, 0) AS shipping_amount,ISNULL(A.shipping_options, '') AS shipping_options from tbl_auction_fedex_rates A where  A.fedex_rate_id=" & ddl_shipping.SelectedValue)
    '                If dtTable.Rows.Count > 0 Then
    '                    strShipping = dtTable.Rows(0).Item("shipping_options")
    '                    strShippingAmount = dtTable.Rows(0).Item("shipping_amount")
    '                End If

    '                strShipping = "My shipping option: " & ddl_shipping.SelectedItem.Text
    '            End If
    '        End If

    '        Select Case buy_option

    '            Case 0 'main button click
    '                Select Case auction_type_id
    '                    Case 1
    '                        Response.Write("<script>window.opener.redirectConfirm(1," & enter_amount & "," & hid_auction_id.Value & ",'buy now');window.close();</script>")
    '                        'Response.Redirect("/AuctionConfirmation.aspx?t=b&a=" & hid_auction_id.Value & "&q=1&p=" & enter_amount & "")
    '                        'obj.save_buy_now_bid(hid_auction_id.Value, enter_amount, 0, "buy now", user_id, buyer_id, strShipping)
    '                        ' objEmail.Send_Auction_Order_Mail(hid_auction_id.Value, buy_option, user_id)
    '                        str = "We will review your request to Buy it Now and get back to you within 2 business days.<br><br>Thank you for participating!"
    '                    Case 2
    '                        'If rdo_option.SelectedValue = 1 Then
    '                        '    strShipping = "My shipping carrier is: " & txt_shipping_type.Text.Trim() & " And My Account # is: " & txt_shipping_account_number.Text.Trim()
    '                        'Else
    '                        '    strShipping = "My shipping option: " & rdo_fedex.SelectedItem.Text
    '                        'End If

    '                        obj.insert_tbl_auction_bids(hid_auction_id.Value, user_id, enter_amount, strShipping, strShippingAmount)

    '                        Dim rak As Integer = SqlHelper.ExecuteScalar("select dbo.buyer_bid_rank(" & hid_auction_id.Value & "," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
    '                        If rak = 1 Then
    '                            str = "Congratulations! You are now the highest bidder."
    '                        Else
    '                            Dim dtHBid As New DataTable()
    '                            dtHBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids where auction_id=" & hid_auction_id.Value & " order by bid_amount desc")
    '                            Dim c_bid_amount As Double = 0
    '                            If dtHBid.Rows.Count > 0 Then
    '                                c_bid_amount = dtHBid.Rows(0).Item("bid_amount")
    '                            End If
    '                            dtHBid.Dispose()
    '                            str = "You have been outbid. The current high bid is " & FormatCurrency(c_bid_amount, 2) & ". Please submit a new bid."

    '                        End If

    '                    Case 3
    '                        'If rdo_option.SelectedValue = 1 Then
    '                        '    strShipping = "My shipping carrier is: " & txt_shipping_type.Text.Trim() & " And My Account # is: " & txt_shipping_account_number.Text.Trim()
    '                        'Else
    '                        '    strShipping = "My shipping option: " & rdo_fedex.SelectedItem.Text
    '                        'End If
    '                        obj.insert_tbl_auction_bids(hid_auction_id.Value, user_id, enter_amount, strShipping, strShippingAmount)
    '                        Dim rak As Integer = SqlHelper.ExecuteScalar("select dbo.buyer_bid_rank(" & hid_auction_id.Value & "," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
    '                        If rak = 1 Then
    '                            str = "Congratulations! You are now the highest bidder."
    '                        Else
    '                            Dim dtHBid As New DataTable()
    '                            dtHBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids where auction_id=" & hid_auction_id.Value & " order by bid_amount desc")
    '                            Dim c_bid_amount As Double = 0
    '                            If dtHBid.Rows.Count > 0 Then
    '                                c_bid_amount = dtHBid.Rows(0).Item("bid_amount")
    '                            End If
    '                            dtHBid.Dispose()
    '                            str = "You have been outbid. The current high bid is " & FormatCurrency(c_bid_amount, 2) & ". Please submit a new bid."

    '                        End If
    '                    Case Else
    '                End Select
    '            Case 1
    '                Response.Write("<script>window.opener.redirectConfirm(1," & enter_amount & "," & auction_id & ",'buy now');window.close();</script>")
    '                'Response.Redirect("/AuctionConfirmation.aspx?t=b&a=" & hid_auction_id.Value & "&q=1&p=" & enter_amount & "")
    '                'obj.save_buy_now_bid(hid_auction_id.Value, enter_amount, 0, "buy now", user_id, buyer_id, strShipping)
    '                ' objEmail.Send_Auction_Order_Mail(hid_auction_id.Value, buy_option, user_id)
    '                str = "Thank you for submitting your order."
    '            Case 2
    '                Response.Write("<script>window.opener.redirectConfirm(1," & enter_amount & "," & auction_id & ",'private');window.close();</script>")
    '                'Response.Redirect("/AuctionConfirmation.aspx?t=b&a=" & hid_auction_id.Value & "&q=1&p=" & txt_amount.Text.Trim & "")
    '                'obj.save_buy_now_bid(hid_auction_id.Value, txt_amount.Text, 0, "private", user_id, buyer_id, strShipping)
    '                ' objEmail.Send_Auction_Order_Mail(hid_auction_id.Value, buy_option, user_id)
    '                str = "Thank you for submitting private order."
    '            Case 3
    '                Response.Write("<script>window.opener.redirectConfirm(" & qty & "," & enter_amount & "," & auction_id & ",'partial');window.close();</script>")
    '                'Response.Redirect("/AuctionConfirmation.aspx?t=b&a=" & hid_auction_id.Value & "&q=" & txt_qty.Text.Trim & "&m=" & txt_amount.Text.Trim & "")
    '                'obj.save_buy_now_bid(hid_auction_id.Value, txt_amount.Text, txt_qty.Text, "partial", user_id, buyer_id, strShipping)
    '                ' objEmail.Send_Auction_Order_Mail(hid_auction_id.Value, buy_option, user_id)
    '                str = "Thank you for submitting partial order."
    '            Case Else

    '        End Select
    '        objEmail = Nothing
    '        obj = Nothing


    '        ConfirmOrder(str)
    '    Else
    '        ConfirmOrder(validate)
    '    End If

    'End Sub
    'Private Function validOrder(ByVal bid_over_check_only As Boolean, ByRef buy_option As Integer, ByRef auction_type_id As Integer, ByRef enter_amount As Double, ByRef qty As Integer) As String
    '    Dim flg As String = ""

    '    Dim auc_stat As Integer = SqlHelper.ExecuteScalar("select dbo.get_auction_status(" & hid_auction_id.Value & ")")
    '    Select Case auc_stat
    '        Case 2
    '            Dim is_accept_pre_bid As Boolean = SqlHelper.ExecuteScalar("SELECT ISNULL(is_accept_pre_bid, 0) from tbl_auctions WHERE auction_id =" & hid_auction_id.Value)
    '            If Not is_accept_pre_bid Then
    '                flg = "Sorry, but we’ve sold out of this product. Please check back regularly for the opportunity to participate in future auctions."
    '            End If
    '        Case 3
    '            flg = "Sorry, but we’ve sold out of this product. Please check back regularly for the opportunity to participate in future auctions."
    '    End Select

    '    If flg = "" And auction_type_id = 3 And buy_option = 0 Then

    '        Dim dttt As DataTable = SqlHelper.ExecuteDatatable("select bid_id from tbl_auction_bids where auction_id=" & hid_auction_id.Value & " and max_bid_amount='" & enter_amount & "'")
    '        If dttt.Rows.Count > 0 Then
    '            flg = "Sorry, another bidder already set this amount, try with another amount."
    '        Else

    '            Dim dtt1 As DataTable = SqlHelper.ExecuteDatatable("select top 1 A.bid_amount,B.increament_amount from tbl_auction_bids A inner join tbl_auctions B on A.auction_id=B.auction_id where A.auction_id=" & hid_auction_id.Value & " order by A.bid_amount desc")
    '            If dtt1.Rows.Count > 0 Then
    '                If enter_amount < dtt1.Rows(0).Item("bid_amount") + dtt1.Rows(0).Item("increament_amount") Then
    '                    flg = "Sorry, Amount not Accepted, you have to bid " & FormatCurrency(dtt1.Rows(0).Item("bid_amount") + dtt1.Rows(0).Item("increament_amount"), 2) & " or more."
    '                End If

    '            End If
    '            dtt1.Dispose()

    '        End If
    '        dttt.Dispose()
    '    End If


    '    If flg = "" And bid_over_check_only = False Then

    '        Dim bid_limit As Double = SqlHelper.ExecuteScalar("select case when exists(select buyer_user_id from tbl_reg_buyer_users where buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " ) then (select isnull(bidding_limit,0) from tbl_reg_buyer_users where buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ") else 9999999 end")

    '        If buy_option = 0 Then 'if main button clicked

    '            Select Case auction_type_id
    '                Case 1
    '                    If bid_limit < enter_amount Then
    '                        flg = "Sorry, Bid amount exceeds your bidding limit."
    '                    End If
    '                Case 2
    '                    If bid_limit < enter_amount Then
    '                        flg = "Sorry, Bid amount exceeds your bidding limit."
    '                    End If
    '                Case 3
    '                    If IsNumeric(enter_amount) Then
    '                        If CDbl(enter_amount) < CDbl(enter_amount) Then
    '                            flg = "Sorry, Invalid Amount."
    '                        Else
    '                            If bid_limit < enter_amount Then
    '                                flg = "Sorry, Bid amount exceeds your bidding limit."
    '                            End If

    '                        End If
    '                    Else
    '                        flg = "Sorry, Bid amount is invalid."
    '                    End If

    '            End Select
    '        Else
    '            'if offer clicked
    '            Dim dtBid As New DataTable
    '            dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(B.bid_amount, 0) AS bid_amount,ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value from tbl_auction_bids B inner join tbl_auctions A on A.auction_id=B.auction_id  where A.auction_id=" & hid_auction_id.Value & " order by bid_amount desc")
    '            If dtBid.Rows.Count > 0 Then
    '                With dtBid.Rows(0)
    '                    If Not (.Item("bid_amount") < .Item("thresh_hold_value")) Then
    '                        flg = "Sorry, This offer is currently not available. Please bid to win this product."
    '                    End If
    '                End With
    '            End If
    '            If flg = "" Then
    '                Select Case buy_option
    '                    Case 1
    '                        If bid_limit < enter_amount Then
    '                            flg = "Sorry, Bid amount exceeds your bidding limit."
    '                        End If
    '                    Case 2
    '                        If bid_limit < enter_amount Then
    '                            flg = "Sorry, Bid amount exceeds your bidding limit."
    '                        End If
    '                        'Else
    '                        '    flg = "Sorry, Bid amount is invalid."
    '                        'End If

    '                    Case 3
    '                        'If IsNumeric(txt_amount.Text) Then
    '                        If bid_limit < enter_amount Then
    '                            flg = "Sorry, Bid amount exceeds your bidding limit."
    '                        End If
    '                        'Else
    '                        'flg = "Sorry, Bid amount is invalid."
    '                        'End If

    '                        If IsNumeric(txt_buy_pop_qty.Text.Trim) Then
    '                            If txt_buy_pop_qty.Text.Trim = 0 Then
    '                                flg = "Sorry, quantity is invalid."
    '                            End If
    '                        Else
    '                            flg = "Sorry, quantity is invalid."
    '                        End If

    '                    Case Else

    '                End Select
    '            End If

    '        End If
    '    End If


    '    Return flg
    'End Function
    Protected Sub load_bid_history(ByVal auction_id As Integer)

        If CommonCode.Fetch_Cookie_Shared("buyer_id") <> "" Then
            rep_bid_history.DataSource = SqlHelper.ExecuteDatatable("SELECT A.bid_amount, A.bid_type, (A1.first_name + ' ' +A1.last_name) as bidder,convert(varchar,bid_date, 120) as bid_date,A1.email FROM tbl_auction_bids A WITH (NOLOCK) inner join tbl_reg_buyer_users A1 on A.buyer_user_id=A1.buyer_user_id where A.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and A.auction_id=" & auction_id & " order by A.bid_id desc")
            rep_bid_history.DataBind()
            If rep_bid_history.Items.Count > 0 Then
                empty_data.Visible = False
                rep_bid_history.Visible = True
            Else
                rep_bid_history.Visible = False
                empty_data.Visible = True
            End If
        End If
    End Sub
    'Private Sub ConfirmOrder(ByVal message As String)

    '    chk_accept_term.Checked = False
    '    'pnl_offer.Visible = False
    '    'pnl_send_quote.Visible = False
    '    'pnl_confirm.Visible = True
    '    lbl_popmsg_msg.Text = message

    'End Sub

    Protected Sub but_main_auction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_main_auction.Click
        If Page.IsValid Then
            If IsNumeric(txt_max_bid_amt.Text) And (ViewState("auction_type_id") = 2 Or ViewState("auction_type_id") = 3) Then

                Dim aucid As String = ViewState("auction_id")
                Dim bidamt As Double = txt_max_bid_amt.Text
                If bidamt > ViewState("cmpVal") Then

                    Dim refr_id As String = ""
                    Dim shipOption As String = ddl_shipping.SelectedValue
                    Dim shipValue As String = 0
                    ClientScript.RegisterStartupScript(Me.GetType, "Javascript", "openBid('" & Security.EncryptionDecryption.EncryptValueFormatted(aucid) & "', '" & bidamt & "', '" & Security.EncryptionDecryption.EncryptValueFormatted(ViewState("auction_type_id")) & "', '" & refr_id & "', '" & shipOption & "', '" & shipValue & "');", True)

                End If
            End If

        End If

        'Response.Write("<script>openOffer();</script>")
    End Sub

    Protected Sub btn_history_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_history.Click
        load_bid_history(hid_auction_id.Value)
    End Sub
    Protected Sub btn_query_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_query.Click
        load_auction_bidder_queries(hid_auction_id.Value)
    End Sub
    Protected Sub img_btn_create_thread_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles img_btn_create_thread.Click
        Dim postStr As String = txt_thread_title.Text.Trim().Replace(Char.ConvertFromUtf32(10), "<br/>").Replace("'", "''")
        Dim str As String = "INSERT into tbl_auction_queries(sales_rep_id,auction_id,title,buyer_id,buyer_user_id,parent_query_id,submit_date) " & _
                " Values ((select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")," & hid_auction_id.Value & ",'" & postStr & "'," & CommonCode.Fetch_Cookie_Shared("buyer_id") & "," & CommonCode.Fetch_Cookie_Shared("user_id") & ",0,getdate()); select SCOPE_IDENTITY();"
        Dim query_id As Integer = 0
        query_id = SqlHelper.ExecuteScalar(str)

        Dim sales_rep_id As Integer = 0
        Dim dt As New DataTable
        'dt = SqlHelper.ExecuteDatatable("SELECT A.user_id FROM tbl_sec_users A where A.user_id in (select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
        dt = SqlHelper.ExecuteDatatable("select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id"))
        If dt.Rows.Count > 0 Then
            sales_rep_id = dt.Rows(0)("user_id")
            Dim mailid As String
            Dim Comm As New Email()
            mailid = Comm.set_rep_contact_request_to_salesrep(sales_rep_id, "", txt_thread_title.Text.Trim(), query_id:=query_id)
            mailid = Comm.set_rep_contact_request_to_user(CommonCode.Fetch_Cookie_Shared("user_id"), "", txt_thread_title.Text.Trim())
            Comm = Nothing
        End If
        txt_thread_title.Text = ""
        load_auction_bidder_queries(hid_auction_id.Value)
        lit_ask_confirmation.Text = "Thanks for your question. Someone will get back to you within 24 hours."

    End Sub

    'Protected Sub btn_ask_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_ask.Click
    '    Dim postStr As String = txt_ask.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>").Replace("'", "`")
    '    Dim str As String = "INSERT into tbl_auction_queries(sales_rep_id,auction_id,title,buyer_id,buyer_user_id,parent_query_id,submit_date) " & _
    '            " Values ((select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")," & hid_auction_id.Value & ",'" & postStr & "'," & CommonCode.Fetch_Cookie_Shared("buyer_id") & "," & CommonCode.Fetch_Cookie_Shared("user_id") & ",0,getdate()); select SCOPE_IDENTITY();"
    '    Dim query_id As Integer = 0
    '    query_id = SqlHelper.ExecuteScalar(str)

    '    Dim sales_rep_id As Integer = 0
    '    Dim dt As New DataTable
    '    'dt = SqlHelper.ExecuteDatatable("SELECT A.user_id FROM tbl_sec_users A where A.user_id in (select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
    '    dt = SqlHelper.ExecuteDatatable("select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id"))
    '    If dt.Rows.Count > 0 Then
    '        sales_rep_id = dt.Rows(0)("user_id")
    '        Dim mailid As String
    '        Dim Comm As New Email()
    '        mailid = Comm.set_rep_contact_request_to_salesrep(sales_rep_id, txt_ask.Text.Trim(), "", query_id:=query_id)
    '        mailid = Comm.set_rep_contact_request_to_user(CommonCode.Fetch_Cookie_Shared("user_id"), "", txt_ask.Text.Trim())
    '        Comm = Nothing
    '    End If
    '    If query_id > 0 Then
    '        txt_ask.Text = ""
    '        lit_ask_confirmation.Text = "Thanks for your question. Someone will get back to you within 24 hours."
    '        load_auction_bidder_queries(hid_auction_id.Value)
    '    End If
    'End Sub

    Private Sub AddToMyAuction(ByVal auction_id As Integer, ByVal action As String)
        Dim no_of_fav_auc As Integer = SqlHelper.ExecuteScalar("select count(favourite_id) from tbl_auction_favourites where buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id"))
        If action = "ins" Then
            SqlHelper.ExecuteNonQuery("insert into tbl_auction_favourites(auction_id,buyer_user_id) values ('" & auction_id & "','" & CommonCode.Fetch_Cookie_Shared("user_id") & "')")
            lnk_my_auction.Text = "Remove From My Live Auctions"
            lnk_my_auction.CommandName = "del"
            'If no_of_fav_auc = 0 Then
            '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh", "redirectIframe('/SiteTopBar.aspx?t=1','/SiteLeftBar.aspx?t=1','');", True)
            'Else
            '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh", "redirectIframe('','/SiteLeftBar.aspx?t=1','');", True)
            'End If
        Else
            SqlHelper.ExecuteNonQuery("delete tbl_auction_favourites where auction_id='" & auction_id & "' and buyer_user_id='" & CommonCode.Fetch_Cookie_Shared("user_id") & "'")
            lnk_my_auction.Text = "Add To My Live Auctions"
            lnk_my_auction.CommandName = "ins"
            'If no_of_fav_auc = 1 Then
            '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh", "redirectIframe('/SiteTopBar.aspx?t=1','/SiteLeftBar.aspx?t=1','/AuctionList.aspx?t=1');", True)
            'Else
            '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh", "redirectIframe('','/SiteLeftBar.aspx?t=4','');", True)
            'End If
        End If
    End Sub

    Protected Sub lnk_my_auction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_my_auction.Click
        AddToMyAuction(hid_auction_id.Value, lnk_my_auction.CommandName)
    End Sub
   
End Class
