﻿Imports System.Data
Partial Class ShoppingCart_PaypalRequest
    Inherits System.Web.UI.Page
    Public invoice_no As String = ""
    Public shipping_amount As Double = 0
    Public Tax_amount As Double = 0

    Protected Sub ShoppingCart_PaypalRequest_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            bindGrid()
        End If
    End Sub

    Private Sub bindGrid()
       
        Try
            Dim bid_id As Integer = 0
            Dim buy_id As Integer = 0

            If Request.QueryString.Get("bd") <> "" Then
                bid_id = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("bd"))
            End If
            If Request.QueryString.Get("by") <> "" Then
                buy_id = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("by"))
            End If

            Dim dtItem As DataTable
            If buy_id > 0 Then
                dtItem = SqlHelper.ExecuteDatatable("select A.auction_id,isnull(A.code,'') as code,isnull(A.title,'') as title,B.buy_id as id,B.buyer_id,isnull(B.price,0) as unit_price,isnull(B.quantity,0) as quantity,isnull(B.confirmation_no,'') as confirmation_no,isnull(B.grand_total_amount,0) as total_amount,isnull(B.tax,0) as tax_amount,isnull(B.shipping_amount,0) as shipping_amount from tbl_auctions A WITH (NOLOCK) inner join tbl_auction_buy B WITH (NOLOCK) on A.auction_id=B.auction_id where buy_id=" & buy_id)
            Else
                dtItem = SqlHelper.ExecuteDatatable("select A.auction_id,isnull(A.code,'') as code,isnull(A.title,'') as title,B.bid_id as id,B.buyer_id,isnull(B.bid_amount,0) as unit_price,1 as quantity,'' as confirmation_no,isnull(B.bid_amount,0) as total_amount,0 as tax_amount,isnull(B.shipping_amount,0) as shipping_amount from tbl_auctions A WITH (NOLOCK) inner join tbl_auction_bids B WITH (NOLOCK) on A.auction_id=B.auction_id where bid_id=" & bid_id)
            End If

            If dtItem.Rows.GetHashCode > 0 Then
                With dtItem.Rows(0)
                    Dim confirmation_no As String = .Item("confirmation_no")
                    If confirmation_no = "" Then
                        confirmation_no = .Item("code")
                    End If

                    invoice_no = confirmation_no
                    shipping_amount = .Item("shipping_amount")
                    Tax_amount = .Item("tax_amount")


                    Dim auto_id As Integer = SqlHelper.ExecuteScalar("INSERT INTO tbl_paypal_transaction(auction_id, bid_id, buy_id, invoice_no, item_number, item_name, unit_price, qty, shipping_amount, tax_amount, total_amount, submit_date) values(" & .Item("auction_id") & ", " & bid_id & ", " & buy_id & ", '" & invoice_no & "', '" & .Item("code") & "',  '" & .Item("title").ToString().Replace("'", "`") & "', '" & .Item("unit_price") & "', " & .Item("quantity") & ", 0, 0, '" & .Item("total_amount") & "', getdate()); select SCOPE_IDENTITY()")
                    invoice_no = confirmation_no & "_" & auto_id

                    SqlHelper.ExecuteNonQuery("update tbl_paypal_transaction set invoice_no='" & invoice_no & "' where transaction_id=" & auto_id)

                End With

                rpt_item.DataSource = dtItem
                rpt_item.DataBind()

            Else
                Response.Write("Invalid/Bad Request")
                Response.End()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
            Response.End()
        End Try



    End Sub

End Class
