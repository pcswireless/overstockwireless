﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Invoice.aspx.vb" Inherits="Invoice" %>

<%@ Register Src="~/UserControls/order_confirm.ascx" TagName="Confirm" TagPrefix="Order" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">

        /***********************************************
        * Different CSS depending on OS (mac/pc)- © Dynamic Drive (www.dynamicdrive.com)
        * This notice must stay intact for use
        * Visit http://www.dynamicdrive.com/ for full source code
        ***********************************************/
        ///////No need to edit beyond here////////////

        var mactest = navigator.userAgent.indexOf("Mac") != -1


        if (mactest) {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet_mac.css"/>');
        }
        else {
            document.write('<link rel="stylesheet" type="text/css" href="/style/newstylesheet.css"/>');
        }
  </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sc_1">
    </asp:ScriptManager>
        <div class="wraper content">
        <table cellspacing="0" cellpadding="0" width="100%" align="center">
            <tr>
                <td>
                    <div>
                    <order:confirm runat="server" id="UC_Ord_Cnf" />
                    </div>
                </td>

            </tr>
          
        </table>
            </div>
    </form>
</body>
</html>
