﻿
Partial Class frame_price_summary
    Inherits System.Web.UI.Page
    Dim meta As New HtmlMeta
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Not CommonCode.Fetch_Cookie_Shared("buyer_id") Is Nothing And CommonCode.Fetch_Cookie_Shared("buyer_id") <> "" Then
        pnl_price.Visible = True
        pnl_session.Visible = False
        If Not Page.IsPostBack Then
            Dim str As String = "SELECT A.auction_id,"
            str = str & "ISNULL(A.product_catetory_id, 0) AS product_catetory_id,"
            str = str & "ISNULL(A.auction_category_id, 0) AS auction_category_id,"
            str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"
            str = str & "ISNULL(A.start_price, 0) AS start_price,"
            str = str & "ISNULL(A.reserve_price, 0) AS reserve_price,"
            str = str & "dbo.get_auction_status(A.auction_id) as auction_status,"
            str = str & "ISNULL(A.is_show_reserve_price, 0) AS is_show_reserve_price,"
            str = str & "ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing,"
            str = str & "ISNULL(A.is_show_no_of_bids, 0) AS is_show_no_of_bids,"
            str = str & "ISNULL(A.is_show_increment_price, 0) AS is_show_increment_price,"
            str = str & "ISNULL(A.increament_amount, 0) AS increament_amount"
            str = str & " FROM "
            str = str & "tbl_auctions A WITH (NOLOCK) WHERE A.auction_id =" & Request.QueryString.Get("i")

            Dim dtTable As New DataTable()
            dtTable = SqlHelper.ExecuteDatatable(str)

            If dtTable.Rows.Count > 0 Then
                With dtTable.Rows(0)
                    If .Item("auction_status") <> 3 And CommonCode.Fetch_Cookie_Shared("buyer_id") <> "" Then
                        Me.litRefresh.Visible = True
                        'meta = New HtmlMeta
                        'meta.Attributes.Add("http-equiv", "Refresh")
                        'meta.Attributes.Add("content", "5")
                        'Me.Page.Header.Controls.Add(meta)
                        'meta = Nothing
                    Else
                        Me.litRefresh.Visible = False
                    End If

                    If .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then
                        Dim top_bidder_id As Integer = 0
                        Dim dtHBid As New DataTable()
                        dtHBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount,buyer_id from tbl_auction_bids WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i") & " order by bid_amount desc")

                        Dim c_bid_amount As Double = 0
                        If dtHBid.Rows.Count > 0 Then
                            c_bid_amount = dtHBid.Rows(0).Item("bid_amount")
                            top_bidder_id = dtHBid.Rows(0).Item("buyer_id")
                        End If

                        Dim max_bid_amount As Double = 0
                        Dim bid_amount As Double = 0
                        If CommonCode.Fetch_Cookie_Shared("buyer_id") <> "" Then
                            Dim dtBid As New DataTable()
                            dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i") & " and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by bid_amount desc")

                            If dtBid.Rows.Count > 0 Then
                                With dtBid.Rows(0)
                                    max_bid_amount = .Item("max_bid_amount")
                                    bid_amount = .Item("bid_amount")
                                End With
                            End If
                            dtBid.Dispose()
                        End If


                        Dim div_count As Integer = 1
                        Dim count As Integer = 0
                        Dim strTable As String = "" & "<div class=""prodetail"">"

                        If bid_amount > 0 Then
                            If .Item("auction_type_id") = 3 Then
                                If CommonCode.Fetch_Cookie_Shared("buyer_id") <> "" Then
                                    strTable = strTable & "<div class='flttxt flt1'><div style='width:140px; float:left;'>My Current Bid</div><span id='' " & IIf(top_bidder_id = CommonCode.Fetch_Cookie_Shared("buyer_id"), "style='color:green'", "") & ">: $" & FormatNumber(bid_amount, 2) & "</span></div><div class='flttxt flt2'><div style='width:140px; float:left;'>Auto maximum bid</div><span id=''>: $" & FormatNumber(max_bid_amount, 2) & "</span></div>"
                                Else
                                    strTable = strTable & "<div class='flttxt flt1'><div style='width:140px; float:left;'>My Current Bid</div><span id=''>: $" & FormatNumber(bid_amount, 2) & "</span></div><div class='flttxt flt2'><div style='width:140px; float:left;'>Auto maximum bid</div><span id=''>: $" & FormatNumber(max_bid_amount, 2) & "</span></div>"
                                End If

                                div_count = div_count + 2
                            Else
                                If CommonCode.Fetch_Cookie_Shared("buyer_id") <> "" Then
                                    strTable = strTable & "<div class='flttxt flt1'><div style='width:140px; float:left;'>My Current Bid</div><span id='' " & IIf(top_bidder_id = CommonCode.Fetch_Cookie_Shared("buyer_id"), "style='color:green'", "") & ">: $" & FormatNumber(bid_amount, 2) & "</span></div>"
                                Else
                                    strTable = strTable & "<div class='flttxt flt1'><div style='width:140px; float:left;'>My Current Bid</div><span id=''>: $" & FormatNumber(bid_amount, 2) & "</span></div>"
                                End If

                            End If
                        End If

                        If .Item("auction_status") <> 3 Then

                            If Convert.ToBoolean(.Item("is_show_actual_pricing")) Then
                                If c_bid_amount > .Item("start_price") Then
                                    If CommonCode.Fetch_Cookie_Shared("buyer_id") <> "" Then
                                        strTable = strTable & "<div class='flttxt flt" & div_count & "'><div style='width:140px; float:left;'>Highest Bid</div><span id='' " & IIf(top_bidder_id = CommonCode.Fetch_Cookie_Shared("buyer_id"), "style='color:green'", "style='color:red'") & ">: $" & FormatNumber(c_bid_amount, 2) & "</span></div>"
                                    Else
                                        strTable = strTable & "<div class='flttxt flt" & div_count & "'><div style='width:140px; float:left;'>Highest Bid</div><span id=''>: $" & FormatNumber(c_bid_amount, 2) & "</span></div>"
                                    End If

                                    count = count + 1
                                Else
                                    strTable = strTable & "<div class='flttxt flt" & div_count & "'><div style='width:140px; float:left;'>Start Price</div><span id=''>: $" & FormatNumber(.Item("start_price"), 2) & "</span></div>"
                                    count = count + 1
                                End If
                                div_count = div_count + 1
                            End If

                            If .Item("is_show_increment_price") Then
                                strTable = strTable & "<div class='flttxt flt" & div_count & "'><div style='width:140px; float:left;'>Increments</div><span id=''>: $" & FormatNumber(.Item("increament_amount"), 2) & "</span></div>"
                                count = count + 1
                                div_count = div_count + 1
                            End If

                            If Convert.ToBoolean(.Item("is_show_reserve_price")) And c_bid_amount < .Item("reserve_price") Then
                                If count = 2 Or Request.QueryString.Get("j") = "g" Then
                                    strTable = strTable & ""
                                    count = 0
                                End If

                                Dim auction_reserve_price_help As String = SqlHelper.ExecuteScalar("select isnull(description,'') from tbl_caption_help with (nolock) where caption='auction_reserve_price'")
                                strTable = strTable & "<div class='flttxt flt" & div_count & "'><div style='width:140px; float:left;'>Reserve Price</div><span id=''>: $" & FormatNumber(.Item("reserve_price"), 2) & "</span>" & IIf(auction_reserve_price_help = "", "", "<a style='margin-left:10px;margin-top:20px;' href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>" & auction_reserve_price_help.Replace("'", "’") & "</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>") & "</div>"
                                count = count + 1
                                div_count = div_count + 1

                            End If


                            If Convert.ToBoolean(.Item("is_show_no_of_bids")) Then
                                Dim bid_total As Integer = SqlHelper.ExecuteScalar("select count(distinct buyer_id) from tbl_auction_bids WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i"))

                                If count = 2 Or Request.QueryString.Get("j") = "g" Then
                                    strTable = strTable & ""
                                    count = 0
                                End If

                                strTable = strTable & "<div class='flttxt flt" & div_count & "'><div style='width:140px; float:left;'>No. of bidders</div><span id=''>: " & bid_total & "</span></div>"

                                count = count + 1
                                div_count = div_count + 1
                            End If

                        Else
                            If bid_amount > 0 Then
                                strTable = strTable & "<table border='0' width=100%>"
                                If c_bid_amount < .Item("reserve_price") Then
                                    strTable = strTable & "<tr><td class='flttxtr'>The Reserve Price has not been met for this auction.<br>Thank you for participating!</td></tr>"
                                Else
                                    strTable = strTable & "<tr><td class='flttxtg'>Thank you for participating!</td></tr>"
                                End If
                                strTable = strTable & "<table/>"
                            End If

                        End If


                        strTable = strTable & "</div>"
                        lit_price_setting.Text = strTable
                    End If
                End With
            End If

        End If
        'Else
        'pnl_price.Visible = False
        'pnl_session.Visible = True
        'End If
    End Sub
End Class
