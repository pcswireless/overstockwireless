﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AuctionDetails.aspx.vb"
    Inherits="AuctionDetails" %>

<%--<%@ Register Src="~/UserControls/AuctionListing.ascx" TagName="AuctionListing" TagPrefix="UC1" %>--%>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Auction Details</title>

    <link type="text/css" rel="Stylesheet" href="/Style/AuctionBody.css" />
    <script type="text/javascript" src="/lightbox/prototype.js"></script>
    <script type="text/javascript" src="/lightbox/scriptaculous.js?load=effects"></script>
    <script type="text/javascript" src="/lightbox/lightbox.js"></script>
    <link rel="stylesheet" href="/lightbox/lightbox.css" type="text/css" media="screen" />
    <style type="text/css">
        .modal
        {
            display: none;
            position: absolute;
            top: 0px;
            left: 0px;
            background-color: black;
            z-index: 100;
            opacity: 0.8;
            filter: alpha(opacity=60);
            -moz-opacity: 0.8;
            min-height: 100%;
        }
        #divImage
        {
            display: none;
            z-index: 1000;
            position: fixed;
            top: 0;
            left: 0;
            background-color: White;
            height: 300px;
            width: 410px;
            padding: 3px;
            border: solid 1px black;
        }
    </style>
    <script type="text/javascript" language="JavaScript">

        function redirectIframe(path1, path2, path3) {
            if (path1 != '')
                top.topFrame.location = path1;
            if (path2 != '')
                top.leftFrame.location = path2;
            if (path3 != '')
                top.mainFrame.location = path3;
        }

        function open_pop_win(_path, _id) {
            w = window.open(_path + '?i=' + _id, '_history', 'top=' + ((screen.height - 400) / 2) + ', left=' + ((screen.width - 324) / 2) + ', height=400, width=324,scrollbars=yes,toolbars=no,resizable=1;');
            w.focus();
            return false;
        }

        function Cancel_Ajax(e, sender) {
            args.set_enableAjax(false);

        }
        hu = window.location.search.substring(1);
        if (hu.indexOf('&a=') > 0) {
            redirectIframe(top.topFrame.location, '', '')
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="ScriptManager" runat="server" />
    <div style="padding: 30px 5px 10px 5px;">
        <%--<UC1:AuctionListing runat="server" ID="UC_AuctionListing"/>--%>
    </div>
    </form>
</body>
</html>
