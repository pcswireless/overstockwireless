﻿
Partial Class MyQueries
    Inherits BasePage

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not String.IsNullOrEmpty(CommonCode.Fetch_Cookie_Shared("user_id")) AndAlso CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            hd_buyer_id.Value = CommonCode.Fetch_Cookie_Shared("buyer_id")
            hd_buyer_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
            hd_is_buyer.Value = CommonCode.Fetch_Cookie_Shared("is_buyer")

            ViewState("qry_filter") = "all"
            load_auction_bidder_queries()
        End If
    End Sub
    Protected Sub lnk_all_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_all.Click
        ViewState("qry_filter") = "all"
        load_auction_bidder_queries()
    End Sub
    Protected Sub lnk_pending_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_pending.Click
        ViewState("qry_filter") = "pending"
        load_auction_bidder_queries()
    End Sub
    Protected Sub lnk_ans_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_ans.Click
        ViewState("qry_filter") = "answered"
        load_auction_bidder_queries()
    End Sub
    Protected Sub load_auction_bidder_queries()
        lbl_no_query.Text = ""
        Dim dt As New DataTable
        Dim str As String = "select A.query_id,A.title,ISNULL(A.message,'') as message, A.submit_date,A.buyer_id,B.company_name,ISNULL(A1.title,'') AS auction_title,ISNULL(S.first_name,'') + ' ' + ISNULL(S.last_name,'') As ask_to,ISNULL(A.sales_rep_id,0) AS sales_rep_id," & _
                            "isnull(case when B.state_id<>0 then (select name from tbl_master_states where state_id=B.state_id) else B.state_text end,'') as state " & _
                            "from tbl_auction_queries A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id " & _
                            "left join tbl_auctions A1 WITH (NOLOCK) on A.auction_id=A1.auction_id Left JOIN tbl_sec_users S ON A.sales_rep_id=S.user_id " & _
                            "where A.parent_query_id=0 and A.buyer_user_id=" & hd_buyer_user_id.Value '& " order by submit_date desc"

        lnk_all.CssClass = "filterBy"
        lnk_pending.CssClass = "filterBy"
        lnk_ans.CssClass = "filterBy"

        If ViewState("qry_filter") = "all" Then
            lnk_all.CssClass = "filterByBold"
        End If
        If ViewState("qry_filter") = "answered" Then
            str = str & " and A.query_id in (select parent_query_id from tbl_auction_queries)"
            lnk_ans.CssClass = "filterByBold"
        End If
        If ViewState("qry_filter") = "pending" Then
            str = str & " and A.query_id not in (select parent_query_id from tbl_auction_queries)"
            lnk_pending.CssClass = "filterByBold"
        End If
        str = str & " order by submit_date desc"
        dt = SqlHelper.ExecuteDataTable(str)
        rep_after_queries.DataSource = dt
        rep_after_queries.DataBind()
        'lbl_test.Text = str
        If dt.Rows.Count = 0 Then
            lbl_no_query.Text = "There is no query"
        End If
    End Sub
    Protected Sub rep_after_queries_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rep_after_queries.ItemDataBound
        Dim rep_inner As Repeater = DirectCast(e.Item.FindControl("rep_inner_after_queries"), Repeater)
        rep_inner.DataSource = SqlHelper.ExecuteDatatable("select A.message, A.submit_date, ISNULL(C.first_name,'') AS first_name,ISNULL(C.last_name,'') AS last_name,ISNULL(D.first_name,'') AS buyer_first_name,ISNULL(D.last_name,'') AS buyer_last_name,isnull(C.title,'') as title,isnull(D.title,'') as buyer_title,isnull(C.image_path,'') as image_path,C.user_id from tbl_auction_queries A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id left join tbl_sec_users C on A.user_id=C.user_id LEFT JOIN tbl_reg_buyer_users D ON A.buyer_user_id=D.buyer_user_id where A.parent_query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & " order by submit_date DESC")
        rep_inner.DataBind()
    End Sub
End Class
