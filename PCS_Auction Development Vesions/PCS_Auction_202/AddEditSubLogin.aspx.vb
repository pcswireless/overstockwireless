﻿
Partial Class AddEditSubLogin
    Inherits System.Web.UI.Page
   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
        If Not IsPostBack Then
            If IsNumeric(Request.QueryString.Get("u")) And IsNumeric(Request.QueryString.Get("s")) Then
                txt_sub_username.Enabled = False
                imgbtn_update.Text = "Update"
                binddata()
            ElseIf IsNumeric(Request.QueryString.Get("u")) Then
                imgbtn_update.Text = "Save"
            End If
        End If
    End Sub
    Protected Sub binddata()
        Dim str = "select * from tbl_reg_buyer_users where buyer_user_id=" & Request.QueryString.Get("s")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str)
        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)
                txt_sub_first_name.Text = .Item("first_name")
                txt_sub_last_name.Text = .Item("last_name")
                txt_sub_email.Text = .Item("email")
                txt_sub_username.Text = .Item("username")
                txt_sub_password.Attributes.Add("value", Security.EncryptionDecryption.DecryptValue(.Item("password")))
                txt_sub_bidding_limit.Text = IIf(.Item("bidding_limit") = "0", "", .Item("bidding_limit"))
                'dd_sub_title.SelectedValue = "Administrator"
                If Not dd_sub_title.Items.FindByValue(.Item("title")) Is Nothing Then
                    'dd_sub_title.SelectedValue = "Administrator"
                    dd_sub_title.Items.FindByValue(.Item("title")).Selected = True
                End If
                chk_sub_is_active.Checked = IIf(.Item("is_active") = True, True, False)
            End With
        Else
            Response.End()
        End If
    End Sub
    Protected Sub imgbtn_update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgbtn_update.Click
        If Page.IsValid Then
            If IsNumeric(Request.QueryString.Get("u")) And IsNumeric(Request.QueryString.Get("s")) Then
                Dim buyer_id As Integer = Request.QueryString.Get("u")
                Dim first_name As String = txt_sub_first_name.Text.Trim
                Dim last_name As String = txt_sub_last_name.Text.Trim
                Dim is_active As Integer = 0
                If chk_sub_is_active.Checked Then
                    is_active = 1
                Else
                    is_active = 0
                End If
                Try
                    Dim strError As String = ""
                    If first_name = "" Then
                        strError = "First name required."
                    ElseIf txt_sub_username.Text = "" Then
                        strError = "Username required."
                    ElseIf txt_sub_password.Text = "" Then
                        strError = "Password required."
                    ElseIf txt_sub_password.Text.Length < 6 Then
                        strError = "Password must be atleast 6 characters."
                    End If
                    Dim dblBidding_limit As Double = 0
                    If strError <> "" Then
                        lit_message.Text = "<font color='red'>" & strError & "</font>"
                        Exit Sub
                    Else
                        If txt_sub_bidding_limit.Text <> "" Then

                        End If
                        If Not Double.TryParse(txt_sub_bidding_limit.Text, dblBidding_limit) Then
                            lit_message.Text = "<font color='red'>Invalid bidding limit.</font>"
                            Exit Sub
                        Else
                            Dim str = "select  isnull(max_amt_bid,0) from tbl_reg_buyers WITH (NOLOCK) where buyer_id=" & Request.QueryString.Get("u")
                            Dim max_bid_limit As Double = SqlHelper.ExecuteScalar(str)
                            If max_bid_limit < dblBidding_limit AndAlso max_bid_limit > 0 Then
                                lit_message.Text = "<font color='red'>You can not set Bidding Limit more than your allowed limit</font>"
                                Exit Sub
                            End If
                        End If
                    End If

                    If validate_user_id_update(txt_sub_username.Text.Trim, Request.QueryString.Get("s")) = 1 Then
                        lit_message.Text = "<font color='red'>Username already exists.</font>"
                        Exit Sub
                    ElseIf Validate_email_update(txt_sub_email.Text.Trim, Request.QueryString.Get("s")) = 1 Then
                        lit_message.Text = "<font color='red'>Email already exists.</font>"
                        Exit Sub
                    Else
                        SqlHelper.ExecuteScalar("update tbl_reg_buyer_users set first_name='" & txt_sub_first_name.Text.Trim() & "',last_name='" & last_name & "',title='" & dd_sub_title.SelectedValue & "',username='" & txt_sub_username.Text.Trim & "',password='" & Security.EncryptionDecryption.EncryptValue(txt_sub_password.Text.Trim) & "',bidding_limit=" & IIf(txt_sub_bidding_limit.Text = "", 0, txt_sub_bidding_limit.Text) & ",is_active=" & is_active & ",email='" & txt_sub_email.Text.Trim() & "' where buyer_user_id=" & Request.QueryString.Get("s"))
                        Response.Write("<script>window.opener.refresh_history();window.close();</script>")
                    End If
                Catch ex As Exception
                    lit_message.Text = "<font color='red'>Unable to Update Sub-Login. Reason: " + ex.Message & "</font>"
                End Try
            ElseIf IsNumeric(Request.QueryString.Get("u")) Then
                Dim TitleSubBidder As String = dd_sub_title.SelectedItem.Value

                Dim buyer_id As Integer = Request.QueryString.Get("u")
                Dim first_name As String = txt_sub_first_name.Text.Trim
                Dim last_name As String = txt_sub_last_name.Text.Trim
                Dim is_active As Integer = 0
                If chk_sub_is_active.Checked Then
                    is_active = 1
                Else
                    is_active = 0
                End If
                Try
                    Dim strError As String = ""
                    If first_name = "" Then
                        strError = "First name required."
                    ElseIf txt_sub_username.Text = "" Then
                        strError = "Username required."
                    ElseIf txt_sub_password.Text = "" Then
                        strError = "Password required."
                    ElseIf txt_sub_password.Text.Length < 6 Then
                        strError = "Password must be atleast 6 characters."
                    End If
                    Dim dblBidding_limit As Double = 0
                    If strError <> "" Then
                        lit_message.Text = "<font color='red'>" & strError & "</font>"
                        Exit Sub
                    Else
                        If txt_sub_bidding_limit.Text <> "" Then
                            If Not Double.TryParse(txt_sub_bidding_limit.Text, dblBidding_limit) Then
                                lit_message.Text = "<font color='red'>Invalid bidding limit.</font>"
                                Exit Sub
                            Else
                                Dim str = "select  isnull(max_amt_bid,0) from tbl_reg_buyers WITH (NOLOCK) where buyer_id=" & Request.QueryString.Get("u")
                                Dim max_bid_limit As Double = SqlHelper.ExecuteScalar(str)
                                If max_bid_limit < dblBidding_limit AndAlso max_bid_limit > 0 Then
                                    lit_message.Text = "<font color='red'>You can not set Bidding Limit more than your allowed limit</font>"
                                    Exit Sub
                                End If
                            End If
                        End If

                    End If

                    If validate_user_id(txt_sub_username.Text.Trim) = 1 Then
                        lit_message.Text = "<font color='red'>Username already exists.</font>"
                        Exit Sub
                    ElseIf Validate_email(txt_sub_email.Text.Trim) = 1 Then
                        lit_message.Text = "<font color='red'>Email already exists.</font>"
                        Exit Sub
                    Else
                        Dim buyer_user_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_reg_buyer_users(buyer_id, first_name, last_name,title,username,password,bidding_limit,is_active,submit_date,submit_by_user_id,is_admin,email) VALUES (" & buyer_id & ", '" & first_name & "','" & last_name & "','" & TitleSubBidder & "','" & txt_sub_username.Text.Trim() & "','" & Security.EncryptionDecryption.EncryptValue(txt_sub_password.Text.Trim) & "'," & IIf(txt_sub_bidding_limit.Text.Trim = "", 0, txt_sub_bidding_limit.Text.Trim) & "," & is_active & ",getdate(), " & CommonCode.Fetch_Cookie_Shared("user_id") & ",0,'" & txt_sub_email.Text.Trim & "')   select scope_identity()")
                        Dim objEmail As New Email()
                        objEmail.send_bidder_sub_login_creation_email(buyer_user_id)
                        objEmail = Nothing
                        Response.Write("<script>window.opener.refresh_history();window.close();</script>")
                    End If
                Catch ex As Exception
                    lit_message.Text = "<font color='red'>Unable to add Sub-Login. Reason: " + ex.Message & "</font>"
                End Try
            End If
        End If
    End Sub
    Private Function validate_user_id(ByVal username As String) As Integer
        Dim str = "select count(buyer_id) from tbl_reg_buyer_users where username='" & username & "'"
        Return SqlHelper.ExecuteScalar(str)
    End Function
    Private Function Validate_email(ByVal txt_sub_email As String) As Integer
        Dim str = "select count(buyer_id) from tbl_reg_buyer_users where email='" & txt_sub_email & "'"
        Return SqlHelper.ExecuteScalar(str)
    End Function

    Private Function validate_user_id_update(ByVal username As String, ByVal buyer_sub_user_id As Integer) As Integer
        Dim str = "select count(buyer_id) from tbl_reg_buyer_users where username='" & username & "' and buyer_user_id<>" & buyer_sub_user_id
        Return SqlHelper.ExecuteScalar(str)
    End Function
    Private Function Validate_email_update(ByVal txt_sub_email As String, ByVal buyer_sub_user_id As Integer) As Integer
        Dim str = "select count(buyer_id) from tbl_reg_buyer_users where email='" & txt_sub_email & "' and buyer_user_id<>" & buyer_sub_user_id
        Return SqlHelper.ExecuteScalar(str)
    End Function
End Class
