﻿
Partial Class SiteHome
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            If CommonCode.Fetch_Cookie_Shared("user_id") = "" And SqlHelper.of_FetchKey("frontend_login_needed") = "1" Then
                Response.Write("<script language='javascript'>top.location = '/login.aspx';</script>")
            End If
        End If
    End Sub
   
End Class
