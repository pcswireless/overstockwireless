﻿<%@ Page Language="VB" AutoEventWireup="false" EnableViewState="false" CodeFile="frame_price_summary.aspx.vb"
    Inherits="frame_price_summary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript">

        /***********************************************
        * Different CSS depending on OS (mac/pc)- © Dynamic Drive (www.dynamicdrive.com)
        * This notice must stay intact for use
        * Visit http://www.dynamicdrive.com/ for full source code
        ***********************************************/
        ///////No need to edit beyond here////////////

        var mactest = navigator.userAgent.indexOf("Mac") != -1


        if (mactest) {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet_mac.css"/>');
        }
        else {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet.css"/>');
        }
  </script>
  <style>
   #dhtmltooltip_new {
            position: absolute;
            left: -300px;
            width: 150px;
            border: 1px solid black;
            padding: 0px !important;
            background-color: lightyellow;
            visibility: hidden;
            z-index: 999; /*Remove below line to remove shadow. Below line should always appear last within this CSS*/
            filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,direction=45,strength=2); /* End ToolTip CSS */

        }
  </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="dhtmltooltip_new" style="overflow: visible;">
    </div>
    <asp:Panel runat="server" ID="pnl_session">
        <center>
            <br />
            <span style="font-family: 'dinregular',arial; font-size: 20px; color: #EC6811;">Session Expired! Please Re-Login.</span>
            </center>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnl_price">
        <asp:Literal ID="litRefresh" runat="server">
<%--    <script type="text/javascript">        E_refresh = window.setTimeout(function () { window.location.href = window.location.href }, 5000);</script>--%>
        </asp:Literal>
        <div style="width: 100%; overflow: hidden;">
            <asp:Literal ID="lit_price_setting" runat="server" />
        </div>
    </asp:Panel>
    
    </form>
    <script type="text/javascript" src="/pcs_js/divarrow_tooltip.js"></script>
</body>
</html>
