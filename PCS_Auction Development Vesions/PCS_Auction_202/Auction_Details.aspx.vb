﻿
Partial Class Auction_Details
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
         If Not Page.IsPostBack Then
            ViewState("buyer_id") = 0
            ViewState("user_id") = 0
            Dim buyer_id As String = CommonCode.Fetch_Cookie_Shared("buyer_id")
            Dim user_id As String = CommonCode.Fetch_Cookie_Shared("user_id")
            If Not String.IsNullOrEmpty(buyer_id) Then
                If buyer_id <> "" Then
                    If IsNumeric(buyer_id) Then
                        If buyer_id > 0 Then

                            If Not String.IsNullOrEmpty(user_id) Then
                                If user_id <> "" Then
                                    If IsNumeric(user_id) Then
                                        If user_id > 0 Then
                                            ViewState("buyer_id") = buyer_id
                                            ViewState("user_id") = user_id
                                        End If
                                    End If
                                End If
                            End If

                        End If
                    End If
                End If
            End If

            If Not String.IsNullOrEmpty(Request.QueryString.Get("source")) Then
                ViewState("buyer_id") = 0
                ViewState("user_id") = 0
                Me.pnlDetails.Enabled = False

            End If

            If Not Request.QueryString("i") Is Nothing Then

                Dim bid_id As Integer = 0
                Try
                    bid_id = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("i"))
                Catch ex As Exception
                    bid_id = Request.QueryString.Get("i")
                End Try
                hid_bid_id.Value = bid_id

            End If

            If Not String.IsNullOrEmpty(Request.QueryString.Get("a")) Then

                ViewState("auction_id") = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("a"))
                hid_auction_id.Value = ViewState("auction_id")



                fill_page(ViewState("auction_id"))

            Else

                ViewState("auction_id") = 0

            End If

            If IsNumeric(Request.QueryString.Get("t")) AndAlso Request.QueryString.Get("t") > 0 Then

                If Request.QueryString.Get("t") = 1 Then
                    ltr_bradcom.Text = "<a href='/Auction_Listing.aspx?t=1'>Live Auctions</a>"
                ElseIf Request.QueryString.Get("t") = 2 Then
                    ltr_bradcom.Text = "<a href='/Auction_Listing.aspx?t=2'>Auction History</a>"
                ElseIf Request.QueryString.Get("t") = 3 Then
                    ltr_bradcom.Text = "<a href='/Auction_Listing.aspx?t=3'>My Live Auctions</a>"
                ElseIf Request.QueryString.Get("t") = 4 Then
                    ltr_bradcom.Text = "<a href='/My_Account.aspx'>My Account</a>"
                End If
                ' ltr_bradcom.Text = ""
            End If
            If ViewState("auction_type_id") = 3 Then
                lotMsg.Visible = True
            Else
              lotMsg.Visible = False
            End If


            ' rb_total_bid_amt.Attributes.Add("OnClick", "Javascript:CalculateTotal();")
            ' rb_partial_bid_amt.Attributes.Add("OnClick", "Javascript:CalculatePerUnit();")

            rb_total_bid_amt.Attributes.Add("OnChange", "Javascript:CalculateTotal();")
            rb_partial_bid_amt.Attributes.Add("OnChange", "Javascript:CalculatePerUnit();")
            txt_max_bid_amt.Attributes.Add("OnChange", "Javascript:recalculatePerUnitTotal();")

        End If
    End Sub
    
    Private Sub setAuction(ByVal auction_id As Integer, ByVal mode As String, ByVal quation_id As Integer, ByVal buy_id As Integer)

        Dim str As String = "SELECT A.auction_id,"
        str = str & "ISNULL(A.code, '') AS code,"
        str = str & "ISNULL(A.title, '') AS title,"
        str = str & "ISNULL(A.sub_title, '') AS sub_title,"
        str = str & "ISNULL(A.product_catetory_id, 0) AS product_catetory_id,"
        str = str & "ISNULL(A.stock_condition_id, 0) AS stock_condition_id,"
        str = str & "ISNULL(A.auction_category_id, 0) AS auction_category_id,"
        str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"
        str = str & "ISNULL(A.is_private_offer, 0) AS is_private_offer,"
        str = str & "ISNULL(A.is_partial_offer, 0) AS is_partial_offer,"
        str = str & "ISNULL(A.auto_acceptance_price_private, 0) AS auto_acceptance_price_private,"
        str = str & "ISNULL(A.auto_acceptance_price_partial, 0) AS auto_acceptance_price_partial,"
        str = str & "ISNULL(A.auto_acceptance_quantity, 0) AS auto_acceptance_quantity,"
        str = str & "ISNULL(A.no_of_clicks, 0) AS no_of_clicks,"
        str = str & "ISNULL(A.start_price, 0) AS start_price,"
        str = str & "ISNULL(A.reserve_price, 0) AS reserve_price,"
        str = str & "ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value,"
        str = str & "ISNULL(A.start_date, '1/1/1900') AS start_date,"
        str = str & "ISNULL(A.display_end_time, '1/1/1900') AS display_end_time,"
        str = str & "ISNULL(A.is_show_reserve_price, 0) AS is_show_reserve_price,"
        str = str & "ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing,"
        str = str & "ISNULL(A.is_show_no_of_bids, 0) AS is_show_no_of_bids,"
        str = str & "ISNULL(A.is_show_rank, 0) AS is_show_rank,"
        str = str & "A.use_pcs_shipping, A.use_your_shipping,"
        str = str & "ISNULL(A.increament_amount, 0) AS increament_amount,"
        str = str & "ISNULL(A.buy_now_price, 0) AS buy_now_price,"
        str = str & "ISNULL(A.is_buy_it_now, 0) AS is_buy_it_now,"
        str = str & "ISNULL(A.request_for_quote_message, '') AS request_for_quote_message,"
        str = str & "ISNULL(A.show_price, 0) AS show_price,"
        If ViewState("buyer_id") > 0 Then
            str = str & "ISNULL((select last_shipping_option from tbl_reg_buyers WITH (NOLOCK) where buyer_id=" & ViewState("buyer_id") & "), 0) AS last_shipping_option,"
        Else
            str = str & "0 AS last_shipping_option,"
        End If

        str = str & "ISNULL(A.total_qty, 0) AS total_qty,"
        str = str & "ISNULL(A.qty_per_bidder, 0) AS qty_per_bidder,"
        str = str & "dbo.get_auction_status(A.auction_id) as auction_status,"
        str = str & "ISNULL(ct.name,'') as product_category,"
        If ViewState("user_id") > 0 Then
            str = str & "case when exists(select favourite_id from tbl_auction_favourites where auction_id=A.auction_id and buyer_user_id=" & ViewState("user_id") & ") then 1 else 0 end as in_fav,"
        Else
            str = str & "0 as in_fav,"
        End If
        str = str & "ISNULL(IMG.stock_image_id, 0) AS stock_image_id,ISNULL(IMG.filename, '') AS filename,"
        str = str & "ISNULL(A.live_bid_allow, 0) AS live_bid_allow,"
        str = str & "ISNULL(A.proxy_bid_allow, 0) AS proxy_bid_allow,"
        str = str & "ISNULL(A.shipping_amount, 0) AS shipping_amount"
        str = str & " FROM "
        str = str & "tbl_auctions A  WITH (NOLOCK) left join tbl_master_product_categories CT  WITH (NOLOCK) on A.product_catetory_id=CT.product_catetory_id left join tbl_master_stock_locations L  WITH (NOLOCK) on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B  WITH (NOLOCK) on A.stock_condition_id=B.stock_condition_id "
        str = str & " left join tbl_auction_stock_images IMG  WITH (NOLOCK) on A.auction_id=IMG.auction_id and IMG.stock_image_id=(SELECT top 1 stock_image_id from tbl_auction_stock_images  WITH (NOLOCK) where auction_id=a.auction_id order by position) "
        str = str & " WHERE A.auction_id =" & auction_id

       
        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDatatable(str)

        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)
                'Response.Write(.Item("last_shipping_option"))
                Dim title_url As String = .Item("title").ToString.Replace(" ", "-").Replace("/", "").Replace("'", "").Replace("&", "").Replace("#", "")
                ltr_auction_code.Text = .Item("code")
                ltr_title.Text = .Item("title") '"<a href='/auction_Pdf.aspx?i=" & auction_id & "&t=" & .Item("auction_type_id") & "' style='font-size:" & IIf(.Item("title").ToString.Length < 70, "20px", IIf(.Item("title").ToString.Length < 150, "18px", IIf(.Item("title").ToString.Length < 200, "16px", "14px"))) & "; text-decoration: none; color:#575757;'>" & .Item("title") & "</a>"
                ltr_sub_title.Text = .Item("sub_title")
                'ltr_short_desc.Text = .Item("short_description")
                'lit_pdf.Text = "<a href='/auction/" & auction_id & "/" & title_url & ".pdf' target='_blank'>Convert To Pdf</a>"

                'lit_pdf.Text = "<a href='/Auction_PdfNew.aspx?a=" & auction_id & "&t=" & Request.QueryString.Get("t") & "' target='_blank'>Convert To Pdf</a>"
                lit_type.Text = .Item("product_category")
                ' CType(e.Item.FindControl("img_ask_question"), ImageButton).Attributes.Add("onclick", "return openAskQuestion(" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & ");")
                If ViewState("buyer_id") > 0 Then
                    lnk_my_auction.Visible = True
                    If Convert.ToBoolean(.Item("in_fav")) Then
                        lnk_my_auction.CommandName = "del"
                        lnk_my_auction.Text = "Remove From My Live Auctions"
                    Else
                        lnk_my_auction.CommandName = "ins"
                        lnk_my_auction.Text = "Add To My Live Auctions"
                    End If

                Else
                    lnk_my_auction.Visible = False
                    lnk_my_auction.Attributes.Add("onclick", "return Modal_Dialog();")
                End If
                
                If .Item("filename").ToString <> "" Then
                    'ltr_auc_img.Text = "<li><a href='#'><img src='/upload/auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString.Replace(".", "_95.") & "' data-large='/upload/auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString.Replace(".", "_375.") & "' alt='image01' data-description='' /></a></li>"
                    'CType(e.Item.FindControl("img_image"), Image).ImageUrl = "/upload/auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString.Replace(".", "_thumb1.")
                    str = "SELECT A.auction_id,convert(varchar,A.auction_id)+'/'+convert(varchar,A.stock_image_id)+'/'+A.filename as path, b.title "
                    str = str & " from tbl_auction_stock_images A  WITH (NOLOCK) inner join tbl_auctions b WITH (NOLOCK) on a.auction_id=b.auction_id where A.auction_id=" & auction_id & "order by isnull(position,999)"
                    'Response.Write(str)
                    rpt_auction_stock_image.DataSource = SqlHelper.ExecuteDatatable(str)
                    rpt_auction_stock_image.DataBind()

                Else
                    ' ltr_auc_img.Text = "<li><a href='#'><img src='/images/imagenotavailable.gif' data-large='/images/imagenotavailable.gif' alt='image01' data-description='' /></a></li>"
                    ' CType(e.Item.FindControl("img_image"), Image).ImageUrl = "/images/imagenotavailable.gif"
                End If

                Dim bid_amount As Double = 0
                Dim bidder_max_amount As Double = .Item("start_price")
                hid_auction_type.Value = .Item("auction_type_id")

                'lit_ask_question.Text = "<a href=""javascript:void(0);"" class='ask' onclick=""return openAskQuestion('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "');"">Ask a Question</a>"
                lit_ask_question.Text = "<a href='mailto:" & SqlHelper.of_FetchKey("site_email_to") & "?Subject=AUCTION: " & .Item("title") & "' class='ask' target='_top'>Ask a Question</a>"

                'panel setting
                If .Item("auction_type_id") = 1 Then
                    iframe_auction_amount.Visible = True
                    iframe_auction_amount.Attributes.Add("src", "/frame_auction_amount.aspx?is_frontend=1&i=" & auction_id)
                    iframe_auction_rank.Visible = False
                    iframe_price_summary.Visible = False
                    txt_max_bid_amt.Visible = False
                    ltr_buy_qty_or_caption.Text = "Qty "
                    ddl_buy_now_qty.Visible = True
                    but_main_auction.Text = "Buy it Now"
                    but_main_auction.CommandName = "buy"

                    ltr_bid_note.Text = "Your offer to buy it now is a non-revocable offer to enter into a binding contract with PCS. If, at the sole discretion of PCS, your offer to buy it now is accepted, you have entered into a legally binding contract with PCS and are contractually obligated to purchase the item(s)."
                ElseIf .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then
                    ltr_buy_qty_or_caption.Text = ""
                    iframe_auction_amount.Visible = True
                    iframe_auction_amount.Attributes.Add("src", "/frame_auction_amount.aspx?is_frontend=1&i=" & auction_id)
                    load_bid_history(auction_id)
                    iframe_price_summary.Visible = True
                    iframe_price_summary.Attributes.Add("src", "/frame_price_summary.aspx?j=1&i=" & auction_id)
                    If .Item("is_show_rank") Then
                        iframe_auction_rank.Visible = True
                        iframe_auction_rank.Attributes.Add("src", "/frame_rank.aspx?is_frontend=1&i=" & auction_id)
                        If .Item("auction_status") = 1 Then
                            Dim rank_help As String = SqlHelper.ExecuteScalar("select isnull(description,'') from tbl_caption_help with (nolock) where caption='auction_rank'")
                            If rank_help <> "" Then
                                lit_rank_help.Text = "<a href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>" & rank_help.Replace("'", "’") & "</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"
                            End If
                        End If
                    Else
                        iframe_auction_rank.Visible = False
                    End If
                    
                    txt_max_bid_amt.Visible = True
                    ddl_buy_now_qty.Visible = False
                    but_main_auction.Text = "Proxy Bid"
                    but_main_auction.CommandName = "bid"
                    If ViewState("buyer_id") > 0 Then
                        If .Item("auction_status") = 1 Or .Item("auction_status") = 3 Then
                            ltr_bidder_history.Text = "<a href=""javascript:void(0);"" class='hstry' onclick=""return open_pop_win('/Bid_History.aspx','" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&pop=1');"">View Bid History</a>"
                        End If
                    Else
                        ltr_bidder_history.Text = "" ' "<a href=""javascript:void(0);"" class='hstry' onclick=""return Modal_Dialog();"">View Bid History</a>"
                    End If


                    Dim dtHBid As New DataTable()
                    dtHBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & auction_id & " order by bid_amount desc")

                    Dim bidder_price As Double = 0
                    Dim bidder_max_price As Double = 0

                    Dim used_shipping_value As String = ""
                    Dim used_shipping_amount As Double = 0
                    Dim used_bid_amount As Double = 0
                    If ViewState("buyer_id") > 0 Then
                        Dim dtBid As New DataTable()
                        dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount,isnull(shipping_value,'') as shipping_value,isnull(shipping_amount,0) as shipping_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & auction_id & " and buyer_id=" & ViewState("buyer_id") & " order by bid_amount desc")
                        If dtBid.Rows.Count > 0 Then

                            'Dim pnl_fedex As Panel = CType(e.Item.FindControl("pnl_fedex"), Panel)
                            If .Item("auction_type_id") = 3 Then
                                bidder_max_amount = dtBid.Rows(0).Item("max_bid_amount") + .Item("increament_amount")
                            Else
                                bidder_max_amount = dtBid.Rows(0).Item("bid_amount") + .Item("increament_amount")
                            End If

                            used_shipping_value = dtBid.Rows(0).Item("shipping_value")
                            used_shipping_amount = dtBid.Rows(0).Item("shipping_amount")
                            used_bid_amount = dtBid.Rows(0).Item("bid_amount")
                        End If
                        dtBid = Nothing
                    End If


                    ltr_amt_help.Text = "<a href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>Why wasn’t my amount accepted?<br />Your maximum bid may not be accepted if the amount is:<br />1. Less than what the auction must start at (" & FormatCurrency(.Item("start_price"), 2) & ")<br />2. An invalid increment amount (" & FormatCurrency(.Item("increament_amount"), 2) & ").</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"

                    If dtHBid.Rows.Count > 0 Then
                        'running bid
                        bid_amount = dtHBid.Rows(0).Item("bid_amount")

                    End If

                    dtHBid = Nothing


                    'Shipping Setting start
                    If .Item("auction_status") = 1 Then
                        'if currently running or upcoming accept prebid

                        dv_shipping.Visible = True


                        If .Item("shipping_amount") > 0 Then

                            'litm = New ListItem
                            'litm.Value = FormatNumber(.Item("shipping_amount"), 2)
                            'litm.Text = "The shipping cost for this lot will be $" & FormatNumber(.Item("shipping_amount"), 2)
                            'ddl_shipping.Items.Add(litm)
                            lit_shipping.Visible = True
                            lit_shipping.Text = "<span><br>The shipping cost for this lot will be $" & FormatNumber(.Item("shipping_amount"), 2) & "</span>"
                            ddl_shipping.Visible = False
                        Else
                            ddl_shipping.Visible = True
                            ddl_shipping.Items.Clear()
                            Dim litm As ListItem
                            Dim ship_select As Boolean = False
                            If .Item("use_pcs_shipping") = True Then
                                If .Item("use_your_shipping") = True Then
                                    litm = New ListItem
                                    litm.Value = "1"
                                    litm.Text = "Use my own shipping carrier to ship this item to me"
                                    ddl_shipping.Items.Add(litm)
                                End If

                                If ViewState("buyer_id") > 0 Then
                                    Dim dtShip As New DataTable
                                    dtShip = SqlHelper.ExecuteDatatable("SELECT	ISNULL(A.shipping_options, '') AS shipping_options,ISNULL(A.shipping_amount, 0) AS shipping_amount FROM tbl_auction_fedex_rates A WITH (NOLOCK) WHERE AUCTION_ID=" & auction_id & " AND BUYER_ID=" & ViewState("buyer_id") & " and is_error=0 ORDER BY shipping_amount")

                                    If dtShip.Rows.Count > 0 Then

                                        For i As Integer = 0 To dtShip.Rows.Count - 1
                                            litm = New ListItem
                                            litm.Value = FormatNumber(dtShip.Rows(i)("shipping_amount"), 2)
                                            litm.Text = dtShip.Rows(i)("shipping_options").ToString().Replace("_", " ") & " " & FormatCurrency(dtShip.Rows(i)("shipping_amount"), 2)
                                            If used_shipping_value.IndexOf("shipping option:") > 0 Then
                                                If used_shipping_amount = dtShip.Rows(i)("shipping_amount") Then
                                                    litm.Selected = True
                                                    ship_select = True
                                                Else
                                                    litm.Selected = False
                                                End If
                                            End If
                                            ddl_shipping.Items.Add(litm)
                                        Next
                                    Else
                                        litm = New ListItem
                                        litm.Value = "2"
                                        litm.Text = "Shipping rates will be calculated later"
                                        ddl_shipping.Items.Add(litm)
                                    End If
                                Else
                                    litm = New ListItem
                                    litm.Value = "2"
                                    litm.Text = "Shipping rates will be calculated after login"
                                    ddl_shipping.Items.Add(litm)
                                End If


                            Else
                                'only my shipping available
                                litm = New ListItem
                                litm.Value = "1"
                                litm.Text = "Use my own shipping carrier to ship this item to me"
                                ddl_shipping.Items.Add(litm)
                            End If
                            litm = New ListItem
                            litm.Value = "0"
                            litm.Text = "--Shipping Options--"
                            ddl_shipping.Items.Insert(0, litm)


                            Dim asd As String
                            For kount As Integer = 0 To ddl_shipping.Items.Count - 1
                                asd = "bimal" & ddl_shipping.Items(kount).Text
                                If asd.IndexOf(.Item("last_shipping_option")) > 0 Then
                                    ddl_shipping.ClearSelection()
                                    ddl_shipping.Items(kount).Selected = True
                                End If
                            Next
                        End If


                    Else
                        'if bid over
                        dv_shipping.Visible = False

                    End If

                    'Shipping Setting End


                Else
                    iframe_auction_amount.Visible = False
                    ltr_quote_msg.Text = "<p style='padding: 0 20px 15px; word-break:break-word;'>" & .Item("request_for_quote_message") & "</p>"
                    iframe_auction_rank.Visible = False
                    iframe_price_summary.Visible = False
                    txt_max_bid_amt.Visible = False
                    ltr_buy_qty_or_caption.Text = ""
                    ddl_buy_now_qty.Visible = False
                    but_main_auction.Text = "Send Offer"
                    but_main_auction.CommandName = "offer"
                    ltr_bid_note.Text = "Your offer is a non-revocable offer to enter into a binding contract with PCS. If, at the sole discretion of PCS, your offer is accepted, you have entered into a legally binding contract with PCS and are contractually obligated to purchase the item(s)."

                End If


                'bid button setting
                If .Item("auction_status") = 1 Then
                    Dim buy_it_now_bid As Integer = SqlHelper.ExecuteScalar("select count(*) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and status='Accept' and auction_id=" & auction_id)
                    ltr_current_status.Text = "<div class=""txt2"" style='padding-left:25px; padding-top:10px;'><iframe id=""iframe_auction_time"" scrolling=""no"" frameborder=""0"" width=""100%"" height=""50px"" src=""/frame_auction_time.aspx?p=d&is_frontend=1&i=" & auction_id & "&m=" & Request("m") & """></iframe></div><p><span class='txtx' style='padding-left:25px;'>Closing on : </span>" & Convert.ToDateTime(.Item("display_end_time")).ToShortDateString() & "</p>"

                    Dim bid_limit As Double = -1
                    If ViewState("user_id") > 0 Then
                        bid_limit = SqlHelper.ExecuteScalar("select case when exists(select buyer_user_id from tbl_reg_buyer_users where buyer_user_id=" & ViewState("user_id") & " ) then (select isnull(bidding_limit,0) from tbl_reg_buyer_users where buyer_user_id=" & ViewState("user_id") & ") else 9999999 end")
                    End If

                    Dim dtHelp As DataTable = SqlHelper.ExecuteDatatable("SELECT ISNULL(caption, '') AS caption,ISNULL(description, '') AS description FROM	tbl_caption_help with (nolock)")
                   
                    'chk_accept_bid_now.Visible = True
                    If .Item("auction_type_id") = 1 Then

                        If .Item("show_price") > bid_limit And bid_limit > 0 Then
                            lit_increase_bid_limit.Text = "<div class='bidlimit'><a href='javascript:void(0);' onclick='return open_increase_limit();'>Your current bidding limit is $" & FormatNumber(bid_limit, 0) & "</a></div>"
                            lit_increase_bid_limit1.Text = "<div class='bidding_limit_D'><a href='javascript:void(0);' onclick='return open_increase_limit();'><img src='/Images/bidding_limit.png'></a></div>"
                        End If

                        Dim rem_qty As Integer = .Item("qty_per_bidder")
                        If .Item("qty_per_bidder") > 0 And ViewState("buyer_id") > 0 Then
                            Dim already_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and buyer_id=" & ViewState("buyer_id") & " and auction_id=" & auction_id & "")
                            rem_qty = rem_qty - already_buy
                        End If
                        Dim total_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and auction_id=" & auction_id & "")
                        If .Item("total_qty") - total_buy < rem_qty Then
                            rem_qty = .Item("total_qty") - total_buy
                        End If

                        'Response.Write(rem_qty)
                        If rem_qty > 0 Then
                            For i = 1 To rem_qty
                                ddl_buy_now_qty.Items.Insert(i - 1, New ListItem(i, i))
                            Next

                            hid_buy_now_confirm_amount.Value = .Item("show_price")
                        Else
                            
                            but_main_auction.Visible = False
                            ddl_buy_now_qty.Visible = False
                            ltr_buy_qty_or_caption.Text = ""
                            ltr_buynow_close.Visible = True
                        End If

                       
                        Dim dv1 As DataView = dtHelp.Copy.DefaultView
                        dv1.RowFilter = "caption='buy_it_now_auction'"
                        'The bidder will be able to set the maximum amount he is willing to pay and the system will bid on the bidder’s behalf. Other bidders will not know his maximum amount. The system will place bids on his behalf using the automatic bid increment amount in order to surpass the current high bid. The system will bid only as much as is necessary to make sure he remains the high bidder or to meet the reserve price, up to his maximum amount. If another bidder places the same maximum bid or higher, the system will notify the bidder so he can place another bid.
                        If dv1.Item(0).Item("description").ToString() <> "" Then
                            lit_main_auction_help.Text = "<a style='float:left;margin-right:10px;margin-top:10px;' href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>" & dv1.Item(0).Item("description").ToString().Replace("'", "’") & "</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"
                        End If
                        dv1.Dispose()

                        If ViewState("buyer_id") > 0 Then
                            but_main_auction.OnClientClick = "return redirectConfirm('" & ddl_buy_now_qty.ClientID & "'," & .Item("show_price") & "," & auction_id & ",'buy now')"
                        Else
                            but_main_auction.OnClientClick = "return Modal_Dialog();"
                        End If

                        If Convert.ToBoolean(.Item("is_private_offer")) Then
                            If ViewState("buyer_id") > 0 Then
                                ltr_offer_button.Text = "<a class='bynw' href=""javascript:void(0)"" onclick=""return openOffer('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','eGfVCrPSj3gueIx0aTb0gg==','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','" & .Item("show_price") & "');"">Best Offer</a>"
                            Else
                                ltr_offer_button.Text = "<a class='bynw' href=""javascript:void(0)"" onclick=""return Modal_Dialog();"">Best Offer</a>"
                            End If
                            Dim dv2 As DataView = dtHelp.Copy.DefaultView
                            dv2.RowFilter = "caption='buy_it_now_private_offer'"
                            If dv2.Item(0).Item("description").ToString() <> "" Then
                                ltr_offer_button.Text = ltr_offer_button.Text & "<a style='float:left;margin-left:10px;margin-top:10px;' href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>" & dv2.Item(0).Item("description").ToString().Replace("'", "’") & "</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"
                            End If
                            dv2.Dispose()

                        End If


                        If Convert.ToBoolean(.Item("is_partial_offer")) Then
                            If ViewState("buyer_id") > 0 Then
                                ltr_offer_button.Text = "<a  class='bynw'  href=""javascript:void(0)"" onclick=""return openOffer('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','W8xozjR//_nRE6AwnpleGA==','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','" & .Item("show_price") & "');"">Best Offer</a>"
                            Else
                                ltr_offer_button.Text = "<a class='bynw' href=""javascript:void(0)"" onclick=""return Modal_Dialog();"">Best Offer</a>"
                            End If
                            Dim dv3 As DataView = dtHelp.Copy.DefaultView
                            dv3.RowFilter = "caption='buy_it_now_partial_offer'"
                            If dv3.Item(0).Item("description").ToString() <> "" Then
                                ltr_offer_button.Text = ltr_offer_button.Text & "<a style='float:left;margin-left:10px;margin-top:10px;' href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>" & dv3.Item(0).Item("description").ToString().Replace("'", "’") & "</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"
                            End If
                            dv3.Dispose()
                        End If


                    ElseIf .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then

                        hid_accept_bid_amt.Value = bidder_max_amount
                        iframe_auction_window.Visible = True
                        iframe_auction_window.Attributes.Add("src", "")
                        
                        ViewState("auction_type_id") = .Item("auction_type_id")
                        ViewState("cmpVal") = bidder_max_amount

                        If .Item("reserve_price") > bid_limit And bid_limit > 0 Then
                            lit_increase_bid_limit.Text = "<div class='bidlimit'><a href='javascript:void(0);' onclick='return open_increase_limit();'>Your current bidding limit is $" & FormatNumber(bid_limit, 0) & "</a></div>"
                            lit_increase_bid_limit1.Text = "<div class='bidding_limit_D'><a href='javascript:void(0);' onclick='return open_increase_limit();'><img src='/Images/bidding_limit.png'></a></div>"
                        End If

                        If .Item("proxy_bid_allow") Then
                            but_main_auction.Visible = True
                            If ViewState("buyer_id") > 0 Then
                                but_main_auction.Attributes.Add("onclick", "return openAuctionBid('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','" & txt_max_bid_amt.ClientID & "','" & ddl_shipping.ClientID & "',1," & IIf(.Item("shipping_amount") > 0, 1, 0) & ",'" & FormatNumber(.Item("shipping_amount"), 2) & "')")
                            Else
                                but_main_auction.OnClientClick = "return Modal_Dialog();"
                            End If
                            Dim dv1 As DataView = dtHelp.Copy.DefaultView
                            dv1.RowFilter = "caption='auction_proxy_bid'"
                            If dv1.Item(0).Item("description").ToString() <> "" Then
                                lit_main_auction_help.Text = "<a style='float:left;margin-right:10px;margin-top:10px;' href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>" & dv1.Item(0).Item("description").ToString().Replace("'", "’") & "</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"
                            End If
                            dv1.Dispose()
                        Else
                            but_main_auction.Visible = False
                        End If
                       
                        If .Item("live_bid_allow") Then
                            btn_livebid_bidnow.Visible = True
                            If ViewState("buyer_id") > 0 Then
                                btn_livebid_bidnow.Attributes.Add("onclick", "return openAuctionBid('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','" & txt_max_bid_amt.ClientID & "','" & ddl_shipping.ClientID & "',0," & IIf(.Item("shipping_amount") > 0, 1, 0) & ",'" & FormatNumber(.Item("shipping_amount"), 2) & "')")
                            Else
                                btn_livebid_bidnow.OnClientClick = "return Modal_Dialog();"
                            End If
                            Dim dv1 As DataView = dtHelp.Copy.DefaultView
                            dv1.RowFilter = "caption='auction_bid_now'"
                            
                            If dv1.Item(0).Item("description").ToString() <> "" Then
                                lit_livebid_help.Text = "<a style='float:left;margin-right:10px;margin-top:10px;' href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>" & dv1.Item(0).Item("description").ToString().Replace("'", "’") & "</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"
                            End If
                            dv1.Dispose()
                        Else
                            btn_livebid_bidnow.Visible = False
                        End If
                       
                        ltr_bid_note.Text = IIf(.Item("auction_type_id") = 3, "Your bid is a non-revocable offer to enter into a binding contract with PCS. If, at the sole discretion of PCS, your bid is accepted and selected as the winning bid, you have entered into a legally binding contract with PCS and are contractually obligated to purchase the item(s).", "")
                        If bid_amount < .Item("thresh_hold_value") Or .Item("thresh_hold_value") = 0 Then ' CHANGE BY SANDEEP (NEED TO DISCUSS)

                            If Convert.ToBoolean(.Item("is_buy_it_now")) Then

                                Dim rem_qty As Integer = .Item("qty_per_bidder")
                                If .Item("qty_per_bidder") > 0 And ViewState("buyer_id") > 0 Then
                                    Dim already_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and buyer_id=" & ViewState("buyer_id") & " and auction_id=" & auction_id & "")
                                    rem_qty = .Item("qty_per_bidder") - already_buy
                                End If

                                Dim total_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and auction_id=" & auction_id & "")
                                If .Item("total_qty") - total_buy < rem_qty Then
                                    rem_qty = .Item("total_qty") - total_buy
                                End If

                                If rem_qty > 0 Then

                                    If ViewState("buyer_id") > 0 Then
                                        ltr_offer_button.Text = "<a class='bynw' href=""javascript:void(0)"" onclick=""return openOffer('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','I82X5exjK422E8H_qZdAJg==','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','" & .Item("buy_now_price") & "');"">Buy it Now</a>"
                                    Else
                                        ltr_offer_button.Text = "<a class='bynw' href=""javascript:void(0)"" onclick=""return Modal_Dialog();"">Buy it Now</a>"
                                    End If

                                    Dim dv1 As DataView = dtHelp.Copy.DefaultView
                                    dv1.RowFilter = "caption='auction_buy_it_now'"
                                    If dv1.Item(0).Item("description").ToString() <> "" Then
                                        ltr_offer_button.Text = ltr_offer_button.Text & "<a style='float:left;margin-top:10px;margin-left:10px;' href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>" & dv1.Item(0).Item("description").ToString().Replace("'", "’") & "</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"
                                    End If
                                    dv1.Dispose()

                                Else
                                    ltr_offer_button.Text = ""
                                End If

                            End If

                            If Convert.ToBoolean(.Item("is_private_offer")) Then
                                If ViewState("buyer_id") > 0 Then
                                    ltr_offer_button.Text = "<a class='bynw' href=""javascript:void(0)"" onclick=""return openOffer('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','eGfVCrPSj3gueIx0aTb0gg==','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','" & .Item("show_price") & "');"">Buy it Now</a>"
                                Else
                                    ltr_offer_button.Text = "<a class='bynw' href=""javascript:void(0)"" onclick=""return Modal_Dialog();"">Buy it Now</a>"
                                End If

                                Dim dv1 As DataView = dtHelp.Copy.DefaultView
                                dv1.RowFilter = "caption='auction_private_offer'"
                                If dv1.Item(0).Item("description").ToString() <> "" Then
                                    ltr_offer_button.Text = ltr_offer_button.Text & "<a style='float:left;margin-left:10px;margin-top:10px;' href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>" & dv1.Item(0).Item("description").ToString().Replace("'", "’") & "</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"

                                End If
                                dv1.Dispose()

                            End If
                            If Convert.ToBoolean(.Item("is_partial_offer")) Then
                                If ViewState("buyer_id") > 0 Then
                                    ltr_offer_button.Text = "<a class='bynw' href=""javascript:void(0)"" onclick=""return openOffer('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','W8xozjR//_nRE6AwnpleGA==','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','" & .Item("show_price") & "');"">Buy it Now</a>"
                                Else
                                    ltr_offer_button.Text = "<a class='bynw' href=""javascript:void(0)"" onclick=""return Modal_Dialog();"">Buy it Now</a>"
                                End If
                                Dim dv1 As DataView = dtHelp.Copy.DefaultView
                                dv1.RowFilter = "caption='auction_partial_offer'"
                                If dv1.Item(0).Item("description").ToString() <> "" Then
                                    ltr_offer_button.Text = ltr_offer_button.Text & "<a style='float:left;margin-left:10px;margin-top:10px;' href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>" & dv1.Item(0).Item("description").ToString().Replace("'", "’") & "</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"

                                End If
                                dv1.Dispose()
                            End If

                        End If

                    Else
                        If ViewState("buyer_id") > 0 Then
                            but_main_auction.Attributes.Add("onclick", "return openOffer('" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "','ti0i8u6Oh_l2RKo9llO2UQ==','" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_type_id")) & "','0')")
                        Else
                            but_main_auction.OnClientClick = "return Modal_Dialog();"
                        End If

                        btn_buy_now.Text = "Send Offer"
                        btn_buy_now.CommandName = "offer"
                        ltr_buy_now_pop_heading.Text = "<span class='blue'>Offer:</span> " & .Item("title") & ""
                        Dim dv1 As DataView = dtHelp.Copy.DefaultView
                        dv1.RowFilter = "caption='buy_it_now_auction'"
                        If dv1.Item(0).Item("description").ToString() <> "" Then
                            lit_main_auction_help.Text = "<a style='float:left;margin-right:20px;margin-top:10px;' href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>" & dv1.Item(0).Item("description").ToString().Replace("'", "’") & "</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"
                        End If
                        dv1.Dispose()
                    End If
                    dtHelp.Dispose()
                    dv_bidding.Visible = True
                    dv_bidding1.Visible = True
                ElseIf .Item("auction_status") = 2 Then
                    but_main_auction.Visible = False
                    ltr_timer_caption.Text = "Starts In"
                    ltr_current_status.Text = "<div class=""txt2"" style='padding-left:25px; padding-top:10px;'><iframe id=""iframe_auction_time"" scrolling=""no"" frameborder=""0"" width=""100%"" height=""50px"" src=""/frame_auction_time.aspx?p=d&is_frontend=1&i=" & auction_id & "&m=" & Request("m") & """></iframe></div><p><span class='txtx' style='padding-left:25px;'>Start on : </span>" & Convert.ToDateTime(.Item("start_date")).ToShortDateString() & "</p>"

                ElseIf .Item("auction_status") = 3 Then
                    ltr_timer_caption.Text = ""
                    but_main_auction.Visible = False
                    ltr_current_status.Text = "<div class=""txt2"" style='padding-left:25px; padding-top:10px;'><iframe id=""iframe_auction_time"" scrolling=""no"" frameborder=""0"" width=""100%"" height=""50px"" src=""/frame_auction_time.aspx?p=d&is_frontend=1&i=" & auction_id & "&m=" & Request("m") & """></iframe></div><p><span class='txtx' style='padding-left:25px;'>Closed on : </span>" & Convert.ToDateTime(.Item("display_end_time")).ToShortDateString() & "</p>"

                End If

                iframe_auction_time.Attributes.Add("src", "/frame_auction_time.aspx?p=d&is_frontend=1&i=" & auction_id)



            End With
         
        Else

            but_main_auction.Text = "No Auction"
            but_main_auction.OnClientClick = "return false;"
            Response.Redirect("/Auction_Listing.aspx?t=1")
        End If

        dtTable = Nothing
    End Sub

    Private Sub setAuctiondetails(ByVal auction_id As Integer)

        Dim lit_Summary As String = ""
        Dim lit_Details As String = ""
        Dim lit_terms_condition As String = ""
        Dim str As String = ""
        Dim dt As New DataTable()
        Dim i As Integer

        'Dim rptItems As Repeater = CType(e.Item.FindControl("rptItems"), Repeater)
        '----------item bind start
        Dim lit_Items As String = ""


        dt = SqlHelper.ExecuteDatatable("select [product_item_id], isnull(m.[description],'') AS manufacturer, isnull([part_no],'')  AS part_no, isnull(p.[description],'') AS description, isnull(quantity,0) as quantity,ISNULL(p.name,'') AS title,ISNULL(p.upc,'') As UPC,ISNULL(p.sku,'') As SKU,ISNULL(estimated_msrp,0) AS estimated_msrp,isnull(total_msrp,0) As total_msrp from [tbl_auction_product_items] p WITH (NOLOCK) left join tbl_master_manufacturers m WITH (NOLOCK) on p.manufacturer_id=m.manufacturer_id where auction_id= " & auction_id)

        If dt.Rows.Count > 0 Then
            rptItems.DataSource = dt
            rptItems.DataBind()
            hid_quantity.Value = dt.Rows(0).Item("quantity").ToString()
        Else
            dv_items_details.Visible = False
        End If

        dt.Dispose()
  

        Dim strQry As String = "SELECT A.auction_id,"
        strQry = strQry & "dbo.get_auction_status(A.auction_id) as auction_status,"
        strQry = strQry & "ISNULL(A.tax_details, '') AS tax_details,"
        strQry = strQry & "ISNULL(A.description, '') AS description,"
        strQry = strQry & "ISNULL(A.special_conditions, '') AS special_conditions,"
        strQry = strQry & "ISNULL(A.payment_terms, '') AS payment_terms,"
        strQry = strQry & "ISNULL(A.product_details, '') AS product_details,"
        strQry = strQry & "ISNULL(A.shipping_terms, '') AS shipping_terms,"
        strQry = strQry & "ISNULL(A.other_terms, '') AS other_terms,"
        strQry = strQry & "ISNULL(A.after_launch_message, '') AS after_launch_message,"
        strQry = strQry & "ISNULL(A.start_date, '1/1/1900') AS start_date,"
        strQry = strQry & "ISNULL(A.display_end_time, '1/1/1900') AS display_end_time,"
        strQry = strQry & "ISNULL(A.is_display_start_date, 0) AS is_display_start_date,"
        strQry = strQry & "ISNULL(A.is_display_end_date, 0) AS is_display_end_date,"
        strQry = strQry & "ISNULL(B.name, '') AS stock_condition,"
        strQry = strQry & "ISNULL(B.help_text, '') AS condition_help,"
        strQry = strQry & "ISNULL(L.name, '') AS stock_location,"
        strQry = strQry & "ISNULL(L.CITY, '') AS city,"
        strQry = strQry & "ISNULL(L.state, '') AS state,"
        strQry = strQry & "ISNULL(C.name, '') AS packaging,"
        strQry = strQry & "ISNULL(C.help_text, '') AS packaging_help,"
        strQry = strQry & "ISNULL(D.company_name, '') AS seller,"
        strQry = strQry & "ISNULL(A.[after_launch_filename],'') as [after_launch_filename],"
        strQry = strQry & "ISNULL(A.short_description, '') AS short_description"

        strQry = strQry & " FROM "
        strQry = strQry & "tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations L WITH (NOLOCK) on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B WITH (NOLOCK) on A.stock_condition_id=B.stock_condition_id "
        strQry = strQry & "Left Join tbl_master_packaging C WITH (NOLOCK) on A.packaging_id=C.packaging_id left join tbl_reg_sellers D WITH (NOLOCK) on A.seller_id=D.seller_id"
        strQry = strQry & " WHERE "
        strQry = strQry & "A.auction_id =" & auction_id

        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDatatable(strQry)
        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)

                '--------- Summary Bind start
                'If Convert.ToBoolean(.Item("is_display_start_date")) Or Convert.ToBoolean(.Item("is_display_end_date")) Then

                '    lit_Summary = ""

                '    If Convert.ToBoolean(.Item("is_display_start_date")) Then
                '        Dim dateTimeInfo As DateTime = .Item("start_date")
                '        lit_Summary = lit_Summary & "<span class='txtx'>Bid Start Date: <span class='txt5'>" & dateTimeInfo.ToString("dddd, MMMM dd yyyy hh:mm tt") & "</span></span>"
                '    End If

                '    If Convert.ToBoolean(.Item("is_display_end_date")) Then
                '        If Convert.ToBoolean(.Item("is_display_start_date")) Then
                '            lit_Summary = lit_Summary & "<span class='txtx'><br />"
                '        Else
                '            lit_Summary = lit_Summary & "<span class='txtx'>"
                '        End If

                '        Dim dateTimeInfo As DateTime = .Item("display_end_time")
                '        lit_Summary = lit_Summary & "Bid End Date : <span class='txt5'>" & dateTimeInfo.ToString("dddd, MMMM dd yyyy hh:mm tt") & "</span></span>"
                '    End If

                '    'lit_Summary = lit_Summary & "</div>"
                'End If


                If .Item("after_launch_message") <> "" Then
                    str = .Item("after_launch_message")
                End If

                If .Item("after_launch_filename").ToString.Trim <> "" Then
                    If str <> "" Then str = str & "<br>"
                    str = str & "<a href='/upload/Auctions/after_launch/" & auction_id & "/" & .Item("after_launch_filename").ToString & "' target='_blank'>" & .Item("after_launch_filename").ToString & "</a>"

                End If
                If str <> "" Then
                    lit_Summary = str & "<br>"
                End If
                Dim prod_info As String = ""

                If .Item("stock_condition").ToString.Trim <> "" Then
                    prod_info = "Stock Condition : " & .Item("stock_condition").ToString
                    If .Item("condition_help").ToString.Trim <> "" And .Item("auction_status") = 1 Then
                        prod_info = prod_info & "<a style='margin-left:10px;margin-top:20px;' href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>" & .Item("condition_help").ToString.Replace("'", "’") & "</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"
                    End If
                End If

                If .Item("packaging").ToString.Trim <> "" Then
                    If prod_info <> "" Then prod_info = prod_info & "<br>"
                    prod_info = prod_info & "Packaging : " & .Item("packaging").ToString
                    If .Item("packaging_help").ToString.Trim <> "" And .Item("auction_status") = 1 Then
                        prod_info = prod_info & "<a style='margin-left:10px;margin-top:20px;' href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('<p>" & .Item("packaging_help").ToString.Replace("'", "’") & "</p>','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"
                    End If
                End If

                If prod_info <> "" Then
                    Lit_prod_info.Text = "<div class='txt6' style='text-align:left;padding-left:20px;'>" & prod_info & "</div>"

                End If

                Dim tbl_build As New StringBuilder
                'tbl_build.Append(IIf(.Item("stock_condition").ToString.Trim <> "", "Stock Condition : " & .Item("stock_condition").ToString & "<br />", ""))
                tbl_build.Append(IIf(.Item("stock_location").ToString.Trim <> "", "Stock Location : " & .Item("city").ToString & ", " & .Item("state").ToString & "<br />", ""))
                'tbl_build.Append(IIf(.Item("packaging").ToString.Trim <> "", "Packaging : " & .Item("packaging").ToString & "<br />", ""))
                tbl_build.Append(IIf(.Item("seller").ToString.Trim <> "", "Company : " & .Item("seller").ToString & "<br />", ""))
                If tbl_build.ToString.Trim <> "" Then
                    lit_Summary = lit_Summary & "<br>" & tbl_build.ToString
                End If
                'lit_Summary = lit_Summary & tbl_build.ToString

                dt = SqlHelper.ExecuteDatatable("SELECT attachment_id,auction_id,isnull(title,'') as title,isnull([filename],'') as [filename] FROM tbl_auction_attachments WITH (NOLOCK) WHERE for_bidder=1 and auction_id = " & auction_id & " order by upload_date desc")
                If dt.Rows.Count > 0 Then
                    lit_Summary = lit_Summary & "<p><span class='txtx'>Attachments</span><br>"
                    ' lit_Summary = lit_Summary & "<div style='padding: 0px 10px 10px 10px;'>"
                    For i = 0 To dt.Rows.Count - 1
                        With dt.Rows(i)
                            If dt.Rows(i)("filename").ToString.Trim <> "" Then
                                If i > 0 Then
                                    lit_Summary = lit_Summary & ", <a href='/Upload/Auctions/auction_attachments/" & auction_id & "/" & .Item("attachment_id") & "/" & .Item("filename") & "' target='_blank'>" & IIf(.Item("title").ToString.Trim <> "", .Item("title"), .Item("filename")) & "</a>"
                                Else
                                    lit_Summary = lit_Summary & "<a href='/Upload/Auctions/auction_attachments/" & auction_id & "/" & .Item("attachment_id") & "/" & .Item("filename") & "' target='_blank'>" & IIf(.Item("title").ToString.Trim <> "", .Item("title"), .Item("filename")) & "</a>"
                                End If

                            End If
                        End With
                    Next
                    lit_Summary = lit_Summary & "</p>"
                End If
                dt.Dispose()
                'If lit_Summary <> "" Then
                '    CType(e.Item.FindControl("lit_Summary"), Literal).Text = lit_Summary
                '    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(0).Visible = True
                'Else
                '    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(0).Visible = False
                'End If
                If lit_Summary.Trim <> "" Then
                    ltr_summary_detail.Text = "<div class='terms dtltxtbx'><br><h1 class='pgtitle'>Summary</h1><p>" & lit_Summary & "</p></div>"

                End If

                '--------- Summary Bind end
                '----------Details Tab Start

                If .Item("short_description") <> "" Then
                    lit_Details = "<p><span class='txtx'>Condition</span><br>" & .Item("short_description") & "</p>"
                End If


                dt = SqlHelper.ExecuteDatatable("select product_attachment_id ,isnull(filename,'') as filename from tbl_auction_product_attachments WITH (NOLOCK) where auction_id=" & auction_id & " order by product_attachment_id desc")
                If dt.Rows.Count > 0 Or .Item("product_details") <> "" Then
                    lit_Details = lit_Details & "<p><span class='txtx'>Product Detail</span><br>"
                    If .Item("product_details") <> "" Then
                        lit_Details = lit_Details & .Item("product_details")
                    End If
                    If dt.Rows.Count > 0 Then
                        If dt.Rows(0)("filename").ToString.Trim <> "" Then
                            If lit_Details <> "" Then lit_Details = lit_Details & "<br>"
                            lit_Details = lit_Details & "<a target='_blank' href='/Upload/Auctions/product_attachments/" & auction_id & "/" & dt.Rows(0).Item("product_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a>"
                        End If
                    End If
                    lit_Details = lit_Details & "</p>"
                End If

                dt.Dispose()

                If .Item("description") <> "" Then
                    lit_Details = lit_Details & "<p><span class='txtx'>Description</span><br>" & .Item("description") & "</p>"
                End If


                If lit_Details <> "" Then
                    ltr_auction_details.Text = "<div class='terms dtltxtbx'><br><h1 class='pgtitle'>Details</h1>" & lit_Details & "</div>"
                End If

                '----------Details Tab end

                '----------Terms & Condition Start
                If .Item("payment_terms") <> "" Then
                    ' lit_terms_condition = "<div class='detailsSubHeading' style='font-weight:bold; padding:10px 10px 0px 10px;'>Payment Terms</div>"
                    lit_terms_condition = .Item("payment_terms")
                End If
                dt = SqlHelper.ExecuteDatatable("select payment_term_attachment_id ,isnull(filename,'') as filename from tbl_auction_payment_term_attachments WITH (NOLOCK) where auction_id=" & auction_id & " order by payment_term_attachment_id desc")
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("filename").ToString.Trim <> "" Then
                        If lit_terms_condition <> "" Then lit_terms_condition = lit_terms_condition & "<br>"
                        lit_terms_condition = lit_terms_condition & "<a target='_blank' href='/Upload/Auctions/payment_term_attachments/" & auction_id & "/" & dt.Rows(0).Item("payment_term_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a>"

                    End If
                End If
                dt.Dispose()
                If lit_terms_condition <> "" Then
                    lit_terms_condition = "<p><span class='txtx'>Payment Terms</span><br>" & lit_terms_condition & "</p>"
                End If

                str = ""
                If .Item("shipping_terms") <> "" Then
                    str = .Item("shipping_terms")
                End If
                dt = SqlHelper.ExecuteDatatable("select top 1 shipping_term_attachment_id,isnull(filename,'') as filename from tbl_auction_shipping_term_attachments WITH (NOLOCK) where auction_id=" & auction_id & " order by shipping_term_attachment_id desc")
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("filename").ToString.Trim <> "" Then
                        If str <> "" Then str = str & "<br>"
                        str = str & "<a target='_blank' href='/Upload/Auctions/shipping_term_attachments/" & auction_id & "/" & dt.Rows(0).Item("shipping_term_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a>"

                    End If

                End If
                If str <> "" Then
                    str = "<p><span class='txtx'>Shipping Terms</span><br>" & str & "</p>"
                End If
                dt.Dispose()

                'If str <> "" Then
                'str = "<div style='padding: 10px;'>" & str & "</div>"
                lit_terms_condition = lit_terms_condition & str
                ' End If
                str = ""


                If .Item("special_conditions") <> "" Then
                    'str = "<div class='detailsSubHeading' style='font-weight:bold; padding:10px 10px 0px 10px;'>Special Conditions</div>"
                    str = .Item("special_conditions")
                    'str = "<div class='detailsSubHeading' style='padding-bottom:10px;'><b>Special Conditions</b></div>" & .Item("special_conditions")
                End If
                dt = SqlHelper.ExecuteDatatable("select top 1 special_condition_id,isnull(filename,'') as filename from tbl_auction_special_conditions WITH (NOLOCK) where auction_id=" & auction_id & " order by special_condition_id desc")
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("filename").ToString.Trim <> "" Then
                        If str <> "" Then str = str & "<br>"
                        str = str & "<a target='_blank' href='/Upload/Auctions/special_conditions/" & auction_id & "/" & dt.Rows(0).Item("special_condition_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a>"

                    End If
                End If
                dt.Dispose()

                If str <> "" Then
                    str = "<p><span class='txtx'>Special Conditions</span><br>" & str & "</p>"
                End If
                lit_terms_condition = lit_terms_condition & str

                str = ""
                If .Item("other_terms") <> "" Then
                    'str = "<div class='detailsSubHeading' style='font-weight:bold; padding:10px 10px 0px 10px;'>Other Terms</div>"
                    str = .Item("other_terms")
                    'str = "<div class='detailsSubHeading' style='padding-bottom:10px;'><b>Other Terms</b></div>" & .Item("other_terms")
                End If
                dt = SqlHelper.ExecuteDatatable("select top 1 term_cond_attachment_id,isnull(filename,'') as filename from tbl_auction_terms_cond_attachments WITH (NOLOCK) where auction_id=" & auction_id & " order by term_cond_attachment_id desc")
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("filename").ToString.Trim <> "" Then
                        If str <> "" Then str = str & "<br>"
                        str = str & "<a target='_blank' href='/Upload/Auctions/terms_cond_attachments/" & auction_id & "/" & dt.Rows(0).Item("term_cond_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a>"

                    End If
                End If
                dt.Dispose()

                If str <> "" Then
                    str = "<p><span class='txtx'>Other Terms</span><br>" & str & "</p>"
                End If

                lit_terms_condition = lit_terms_condition & str

                str = ""
                If .Item("tax_details") <> "" Then
                    'str = "<div class='detailsSubHeading' style='font-weight:bold; padding:10px 10px 0px 10px;'>Tax Details</div>"
                    str = str & .Item("tax_details")
                    ' str = "<div class='detailsSubHeading' style='padding-bottom:10px;'><b>Tax Details</b></div>" & .Item("tax_details")
                End If
                dt = SqlHelper.ExecuteDatatable("select top 1 tax_attachment_id,isnull(filename,'') as filename from tbl_auction_tax_attachments WITH (NOLOCK) where auction_id=" & auction_id & " order by tax_attachment_id desc")
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("filename").ToString.Trim <> "" Then
                        If str <> "" Then str = str & "<br>"
                        str = str & "<a target='_blank' href='/Upload/Auctions/tax_attachments/" & auction_id & "/" & dt.Rows(0).Item("tax_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a>"

                    End If
                End If
                dt.Dispose()

                If str <> "" Then
                    str = "<p><span class='txtx'>Tax Details</span><br>" & str & "</p>"
                End If
                lit_terms_condition = lit_terms_condition & str

                If lit_terms_condition <> "" Then
                    ltr_term_condition.Text = "<div class='terms dtltxtbx'><br><h1 class='pgtitle'>Terms and Conditions</h1>" & lit_terms_condition & "</div>"
                End If
                '----------Terms & Condition Start


            End With
        Else
            'e.Item.Visible = False
        End If
        dtTable = Nothing

    End Sub

    Protected Sub fill_page(ByVal auction_id As Int32)
        Dim str As String = ""
        If Not String.IsNullOrEmpty(Request.QueryString("m")) And Not String.IsNullOrEmpty(hid_bid_id.value) Then
            str = ""

            If Request.QueryString("m") = "pr" Or Request.QueryString("m") = "pa" Or Request.QueryString("m") = "bu" Then

                Dim strBuy As String = "select bid_date,price,isnull(grand_total_amount,0) as grand_total_amount,isnull(quantity,0) as quantity,case ISNULL(status,'') when 'Accept' then (case ISNULL(action,'') when 'Awarded' then 'Accepted' when 'Denied' then 'Rejected' else 'Pending Decision' end) when 'Pending' then 'Pending Decision' when 'Reject' then 'Rejected' end AS status from tbl_auction_buy WITH (NOLOCK) where buy_id=" & hid_bid_id.value
                Dim dtBuy As DataTable = New DataTable()
                dtBuy = SqlHelper.ExecuteDatatable(strBuy)
                If dtBuy.Rows.Count > 0 Then
                    str = "<span class='txtx'>Bid Date : </span>" & Convert.ToDateTime(dtBuy.Rows(0)("bid_date")).ToShortDateString() & ""
                    str = str & "<br><span class='txtx'>Price : </span>" & "$" & FormatNumber(dtBuy.Rows(0)("grand_total_amount"), 2) & ""
                    If Request.QueryString("m") = "pa" Then
                        str = str & "<br><span class='txtx'>Qty : </span>" & dtBuy.Rows(0)("quantity") & ""
                    ElseIf Request.QueryString("m") = "bu" And dtBuy.Rows(0)("quantity") > 0 Then
                        str = str & "<br><span class='txtx'>Qty : </span>" & dtBuy.Rows(0)("quantity") & ""
                    End If
                    str = str & "<br><span class='txtx'>Status : <span class='txt5' style='color:#ED660D;'>" & dtBuy.Rows(0)("status") & "</span></span>"
                    Dim print_order_confirmation As String = ""
                    print_order_confirmation = GetGlobalResourceObject("AuctionList", "lit_print_order_confirmation")
                    'lbl_print_confirmation.Visible = True
                    str = str & "<br><br><a class='bid' style='text-transform:none;font-size:22px;padding:8px 0 8px;width:257px;' href=""/AuctionConfirmation.aspx?b=" & Security.EncryptionDecryption.EncryptValueFormatted(hid_bid_id.value) & "&t=b"">" & print_order_confirmation & "</a>"
                End If
                dtBuy = Nothing
            ElseIf Request.QueryString("m") = "q" Then

                Dim strQuote As String = "select bid_date,details,ISNULL(filename,'') As filename, ISNULL(action,'') As status from tbl_auction_quotations WITH (NOLOCK) where quotation_id=" & hid_bid_id.value
                Dim dtQuote As DataTable = New DataTable()
                dtQuote = SqlHelper.ExecuteDatatable(strQuote)
                If dtQuote.Rows.Count > 0 Then
                    str = "<span class='txtx'>Bid Date : </span>" & Convert.ToDateTime(dtQuote.Rows(0)("bid_date")).ToShortDateString() & ""
                    str = str & "<p>" & dtQuote.Rows(0)("details") & "</p>"

                    If dtQuote.Rows(0)("filename") <> "" Then
                        str = str & "<br><a href='/Upload/Quotation/" & auction_id & "/" & dtQuote.Rows(0)("filename") & "' target='_blank'>" & dtQuote.Rows(0)("filename") & "</a>"
                    End If
                    If dtQuote.Rows(0)("status") <> "" Then
                        str = str & "<br><span class='txtx'>Status : <span class='txt5' style='color:#ED660D;'>" & IIf(dtQuote.Rows(0)("status") = "" Or dtQuote.Rows(0)("status").ToString().ToUpper() = "PENDING", "Pending Decision", dtQuote.Rows(0)("status")) & "</span></span>"
                    End If
                End If
                dtQuote = Nothing
            ElseIf Request.QueryString("m") = "w" Then
                str = ""
                Dim dtbid As DataTable = SqlHelper.ExecuteDatatable("select bid_date,max_bid_amount,bid_amount from tbl_auction_bids WITH (NOLOCK) where bid_id=" & hid_bid_id.value)
                If dtbid.Rows.Count > 0 Then
                    With dtbid.Rows(0)
                        str = str & "<span style='color:green; font-family:'dinregular', arial; font-size:15px;'>Thank you for participating!</span><br><br>"
                        str = str & "<span class='txtx'>Bid Date : </span>" & Convert.ToDateTime(.Item("bid_date")).ToShortDateString() & ""
                        str = str & "<br><span class='txtx'>My Auto Bid : </span>" & FormatCurrency(.Item("max_bid_amount"), 2) & ""
                        str = str & "<br><span class='txtx'>Winning Bid : </span>" & FormatCurrency(.Item("bid_amount"), 2) & ""
                    End With
                End If
                dtbid.Dispose()

                Dim print_order_receipt As String = "Print Order Receipt"
                str = str & "<br><br><a class='bid' style='text-transform:none;font-size:22px;padding:8px 0 8px;width:257px;' href=""/AuctionConfirmation.aspx?b=" & Security.EncryptionDecryption.EncryptValueFormatted(hid_bid_id.value) & "&t=a"">" & print_order_receipt & "</a>"
            End If
            If str <> "" Then
                ltr_offer_attachement.Text = "<div class='summrybx dtltxtbx' style='border:none;margin-bottom:0px;'><h1 class='pgtitle' style='padding-top:5px;min-height:0;font-family:dinregular,arial;font-size:25px;'>&nbsp;</h1><div class='txt6' style='margin:0;line-height:20px;'><p>" & str & "</p></div></div>"
            End If
            pnl_from_live.Visible = False
        Else
            pnl_from_account.Visible = False
        End If

        
        setAuction(auction_id, IIf(String.IsNullOrEmpty(Request.QueryString.Get("m")), "", Request.QueryString.Get("mode")), IIf(String.IsNullOrEmpty(Request.QueryString.Get("q")), Request.QueryString.Get("q"), 0), IIf(String.IsNullOrEmpty(Request.QueryString.Get("b")), Request.QueryString.Get("b"), 0))
        setAuctiondetails(auction_id)
    End Sub
    
    Protected Sub load_bid_history(ByVal auction_id As Integer)

        If ViewState("buyer_id") > 0 Then
            rep_bid_history.DataSource = SqlHelper.ExecuteDatatable("SELECT A.bid_amount, A.bid_type, (A1.first_name + ' ' +A1.last_name) as bidder,convert(varchar,bid_date, 120) as bid_date,A1.email FROM tbl_auction_bids A WITH (NOLOCK) inner join tbl_reg_buyer_users A1 on A.buyer_user_id=A1.buyer_user_id where A.buyer_id=" & ViewState("buyer_id") & " and A.auction_id=" & auction_id & " order by A.bid_id desc")
            rep_bid_history.DataBind()
            If rep_bid_history.Items.Count > 0 Then
                empty_data.Visible = False
                rep_bid_history.Visible = True
            Else
                rep_bid_history.Visible = False
                empty_data.Visible = True
            End If
        End If
    End Sub
    
    Protected Sub but_main_auction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_main_auction.Click
        If Page.IsValid Then
            If IsNumeric(txt_max_bid_amt.Text) And (ViewState("auction_type_id") = 2 Or ViewState("auction_type_id") = 3) Then

                Dim aucid As String = ViewState("auction_id")
                Dim bidamt As Double = txt_max_bid_amt.Text
                If bidamt > ViewState("cmpVal") Then

                    Dim refr_id As String = ""
                    Dim shipOption As String = ddl_shipping.SelectedValue
                    Dim shipValue As String = 0
                    ClientScript.RegisterStartupScript(Me.GetType, "Javascript", "openBid('" & Security.EncryptionDecryption.EncryptValueFormatted(aucid) & "', '" & bidamt & "', '" & Security.EncryptionDecryption.EncryptValueFormatted(ViewState("auction_type_id")) & "', '" & refr_id & "', '" & shipOption & "', '" & shipValue & "');", True)

                End If
            End If

        End If

    End Sub

    Protected Sub btn_history_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_history.Click
        load_bid_history(hid_auction_id.Value)
    End Sub
    
    Private Sub AddToMyAuction(ByVal auction_id As Integer, ByVal action As String)
        Dim no_of_fav_auc As Integer = SqlHelper.ExecuteScalar("select count(favourite_id) from tbl_auction_favourites where buyer_user_id=" & ViewState("user_id"))
        If action = "ins" Then
            SqlHelper.ExecuteNonQuery("insert into tbl_auction_favourites(auction_id,buyer_user_id) values ('" & auction_id & "','" & ViewState("user_id") & "')")
            lnk_my_auction.Text = "Remove From My Live Auctions"
            lnk_my_auction.CommandName = "del"
            
        Else
            SqlHelper.ExecuteNonQuery("delete tbl_auction_favourites where auction_id='" & auction_id & "' and buyer_user_id='" & ViewState("user_id") & "'")
            lnk_my_auction.Text = "Add To My Live Auctions"
            lnk_my_auction.CommandName = "ins"
            
        End If
    End Sub

    Protected Sub lnk_my_auction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_my_auction.Click
        AddToMyAuction(hid_auction_id.Value, lnk_my_auction.CommandName)
    End Sub

End Class
