﻿Imports Telerik.Web.UI
Partial Class AuctionList
    Inherits System.Web.UI.Page
    Public rel As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            If CommonCode.Fetch_Cookie_Shared("is_backend") = "1" Then
                Response.Write("<script language='javascript'>top.location.href = '/Backend_Home.aspx?t=1';</script>")
            End If
        End If

        If Not Page.IsPostBack Then
            bindGrid(False)
            'If Not String.IsNullOrEmpty(Request.QueryString.Get("view")) Then
            '    If Request.QueryString.Get("view") = "list" Then
            '        bindGrid(False)
            '        lnk_view.Text = "<a href='/AuctionList.aspx?t=" & Request.QueryString.Get("t") & IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), "", "&c=" & Request.QueryString.Get("c")) & IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), "", "&p=" & Request.QueryString.Get("p")) & "&view=grid'>Switch to Grid View</a>"
            '    Else
            '        lnk_view.Text = "<a href='/AuctionList.aspx?t=" & Request.QueryString.Get("t") & IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), "", "&c=" & Request.QueryString.Get("c")) & IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), "", "&p=" & Request.QueryString.Get("p")) & "&view=list'>Switch to List View</a>"
            '        bindGrid(True)
            '    End If
            'Else
            '    lnk_view.Text = "<a href='/AuctionList.aspx?t=" & Request.QueryString.Get("t") & IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), "", "&c=" & Request.QueryString.Get("c")) & IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), "", "&p=" & Request.QueryString.Get("p")) & "&view=grid'>Switch to Grid View</a>"
            '    bindGrid(False)
            'End If
            'lnk_view1.Text = lnk_view.Text
            showMessage()
        End If

    End Sub
    Private Sub showMessage()
        rep_message.DataSource = SqlHelper.ExecuteDatatable("select [message_board_id],[title],[description] from [tbl_message_boards]  M " & _
        "where dbo.message_board_status(ISNULL(display_from,'1/1/1900'),ISNULL(display_to,'1/1/1900'))='Running' and is_active=1 " & _
        "and " & CommonCode.Fetch_Cookie_Shared("buyer_id") & " in (select buyer_id from fn_get_message_invited_buyers((select top 1 invitation_id from tbl_message_invitations where message_board_id=M.message_board_id)))")
        rep_message.DataBind()

        If rep_message.Items.Count > 0 Then
            div_topmsg.Visible = True
            rep_message.Visible = True
        Else
            div_topmsg.Visible = False
            rep_message.Visible = False
        End If

    End Sub
    Private Sub bindGrid(ByVal is_grid_view As Boolean)

        Dim obj As New Auction

        Dim dv As DataView = obj.fetch_auction_Listing(Request.QueryString.Get("t"), IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), 0, Request.QueryString.Get("c")), IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), 0, Request.QueryString.Get("p")), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("user_id")), CommonCode.Fetch_Cookie_Shared("user_id"), 0), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("buyer_id")), CommonCode.Fetch_Cookie_Shared("buyer_id"), 0), SqlHelper.of_FetchKey("frontend_login_needed"), IIf(String.IsNullOrEmpty(Request.QueryString.Get("a")), 0, Request.QueryString.Get("a"))).Tables(0).DefaultView
        If dv.Count > 0 Then

            'If is_grid_view Then

            '    rpt_auctions_gridview.Visible = True
            '    rpt_auctions.Visible = False
            '    rpt_auctions_gridview.DataSource = dv
            '    rpt_auctions_gridview.DataBind()

            'Else
            '    rpt_auctions.Visible = True
            '    rpt_auctions_gridview.Visible = False
            '    rpt_auctions.DataSource = dv
            '    rpt_auctions.DataBind()
            'End If
            rpt_auctions.DataSource = dv
            rpt_auctions.DataBind()
            pnl_noItem.Visible = False
            pn_auction_content.Visible = True
        Else

            pn_auction_content.Visible = False
            pnl_noItem.Visible = True
            Select Case Request.QueryString.Get("t")
                Case 1
                    lit_no_item.Text = "Live auctions currently not available"
                Case 2
                    lit_no_item.Text = "Hidden auctions currently not available"
                Case 3
                    lit_no_item.Text = "Upcoming auctions currently not available"
                Case 4
                    lit_no_item.Text = "Favorite auctions currently not available"
                Case 5
                    lit_no_item.Text = "Bidding auctions currently not available"
                Case 6
                    lit_no_item.Text = "Auction history currently not available"
                Case Else
                    lit_no_item.Text = "Auction Not available in this section"
            End Select


        End If

        dv.Dispose()
        obj = Nothing

    End Sub
    Protected Sub rpt_auctions_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_auctions.ItemCommand

        If e.CommandName = "add_favourite" Then
            AddtoFab(e.CommandArgument)
            If Request.QueryString.Get("t") <> "1" Then
                e.Item.Visible = False
            Else
                e.Item.FindControl("img_add_favourite").Visible = False
            End If

        End If
        If e.CommandName = "hide_now" Then
            AddtoHidden(e.CommandArgument)
            e.Item.Visible = False
        End If
        If e.CommandName = "refresh" Then
            rebindAuction(e, e.CommandArgument)
        End If
        If e.CommandName = "create_thread" Then
            Dim postStr As String = CType(e.Item.FindControl("txt_thread_title"), TextBox).Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>").Replace("'", "`")
            Dim str As String = "INSERT into tbl_auction_queries(sales_rep_id,auction_id,title,buyer_id,buyer_user_id,parent_query_id,submit_date) " & _
                    " Values ((select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")," & e.CommandArgument & ",'" & postStr & "'," & CommonCode.Fetch_Cookie_Shared("buyer_id") & "," & CommonCode.Fetch_Cookie_Shared("user_id") & ",0,getdate()); select SCOPE_IDENTITY();"
            Dim query_id As Integer = 0
            query_id = SqlHelper.ExecuteScalar(str)

            CType(e.Item.FindControl("txt_thread_title"), TextBox).Text = ""
            'CType(e.Item.FindControl("lit_msg_confirmation"), Literal).Text = "Thank you for submitting your question."

            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDatatable("select A.query_id,A.title,A.submit_date,A.buyer_id,B.company_name,isnull(case when B.state_id<>0 then (select name from tbl_master_states where state_id=B.state_id) else B.state_text end,'') as state from tbl_auction_queries A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id inner join tbl_auctions A1 WITH (NOLOCK) on A.auction_id=A1.auction_id where isnull(A1.discontinue,0)=0 and isnull(A.parent_query_id,0)=0 and A.is_active=1 and A.auction_id=" & e.CommandArgument & " and A.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by submit_date desc")
            CType(e.Item.FindControl("rep_after_queries"), Repeater).DataSource = dt
            CType(e.Item.FindControl("rep_after_queries"), Repeater).DataBind()
            If dt.Rows.Count = 0 Then
                CType(e.Item.FindControl("lit_your_query"), Literal).Visible = False
            Else
                CType(e.Item.FindControl("lit_your_query"), Literal).Visible = False
            End If

            Dim sales_rep_id As Integer = 0
            dt = SqlHelper.ExecuteDatatable("SELECT A.user_id FROM tbl_sec_users A where A.user_id in (select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
            If dt.Rows.Count > 0 Then
                sales_rep_id = dt.Rows(0)("user_id")
                Dim mailid As String
                Dim Comm As New Email()
                mailid = Comm.set_rep_contact_request_to_salesrep(sales_rep_id, postStr, "", query_id:=query_id)
                mailid = Comm.set_rep_contact_request_to_user(CommonCode.Fetch_Cookie_Shared("user_id"), postStr, "")
                Comm = Nothing
            End If
            dt.Dispose()
            If query_id > 0 Then
                CType(e.Item.FindControl("rp_message"), Label).Text = "Thanks for your question. Someone will get back to you within 24 hours."
                'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "qus_pop", "w1 =window.open('/question_confirm_pop.aspx', '_QuestionPop', 'left=' + ((screen.width - 300) / 2) + ',top=' + ((screen.height - 300) / 2) + ',width=270,height=150,scrollbars=no,toolbars=no,resizable=no');w1.focus();", True)

            End If

            ' RegisterStartupScript. window.open('/AuctionQuestion.aspx?i=' + aucid, "_AuctionQuestion", "left=" + ((screen.width - 600) / 2) + ",top=" + ((screen.height - 420) / 2) + ",width=600,height=420,scrollbars=yes,toolbars=no,resizable=yes");
            'ModalPopupExtender.Show()
        End If
        If e.CommandName = "btn_buy_now" Then
            Response.Redirect("/AuctionConfirmation.aspx?t=b&y=buy now&a=" & e.CommandArgument & "&q=" & CType(e.Item.FindControl("ddl_qty"), DropDownList).SelectedValue & "&m=" & CType(e.Item.FindControl("lit_buy_now_amount"), Literal).Text.Trim.Replace("$", "").Replace(",", "") & "")
            'ElseIf e.CommandName = "btn_buy_now_type" Then
            '    Response.Redirect("/AuctionConfirmation.aspx?t=b&a=" & e.CommandArgument & "&q=" & CType(e.Item.FindControl("ddl_qty_confirm"), DropDownList).SelectedValue & "&m=" & CType(e.Item.FindControl("lit_buy_now_amount_confirm"), Literal).Text.Trim.Replace("$", "").Replace(",", "") & "")
        End If
    End Sub
    Protected Sub rep_after_queries_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        If e.CommandName = "deleteQry" Then
            SqlHelper.ExecuteNonQuery("update tbl_auction_queries set is_active=0 where query_id=" & e.CommandArgument)
            e.Item.Visible = False
        End If
    End Sub
    Protected Sub rep_after_queries_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            CType(e.Item.FindControl("btnDelete"), ImageButton).Attributes.Add("onclick", "return confirm('Are you sure to delete this question?');")
            Dim rep_inner As Repeater = DirectCast(e.Item.FindControl("rep_inner_after_queries"), Repeater)
            rep_inner.DataSource = SqlHelper.ExecuteDatatable("select A.message, A.submit_date, ISNULL(C.first_name,'') AS first_name,ISNULL(C.last_name,'') AS last_name,ISNULL(D.first_name,'') AS buyer_first_name,ISNULL(D.last_name,'') AS buyer_last_name,isnull(C.title,'') as title,isnull(D.title,'') as buyer_title,isnull(C.image_path,'') as image_path,C.user_id from tbl_auction_queries A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id left join tbl_sec_users C on A.user_id=C.user_id LEFT JOIN tbl_reg_buyer_users D ON A.buyer_user_id=D.buyer_user_id where A.parent_query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & " order by submit_date DESC")
            rep_inner.DataBind()
        End If
    End Sub
    Private Sub AddtoFab(ByVal auction_id As Integer)
        Dim no_of_fav_auc As Integer = SqlHelper.ExecuteScalar("select count(favourite_id) from tbl_auction_favourites where buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id"))
        If Request.QueryString.Get("t") = "1" Then
            SqlHelper.ExecuteNonQuery("insert into tbl_auction_favourites(auction_id,buyer_user_id) values ('" & auction_id & "','" & CommonCode.Fetch_Cookie_Shared("user_id") & "')")
            If no_of_fav_auc = 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh", "redirectIframe('/SiteTopBar.aspx?t=1','/SiteLeftBar.aspx?t=1','');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh", "redirectIframe('','/SiteLeftBar.aspx?t=1','');", True)
            End If
        Else
            SqlHelper.ExecuteNonQuery("delete tbl_auction_favourites where auction_id='" & auction_id & "' and buyer_user_id='" & CommonCode.Fetch_Cookie_Shared("user_id") & "'")
            If no_of_fav_auc = 1 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh", "redirectIframe('/SiteTopBar.aspx?t=1','/SiteLeftBar.aspx?t=1','/AuctionList.aspx?t=1');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh", "redirectIframe('','/SiteLeftBar.aspx?t=4','');", True)
            End If
        End If
    End Sub
    Private Sub AddtoHidden(ByVal auction_id As Integer)
        Dim no_of_hid_auc As Integer = SqlHelper.ExecuteScalar("select count(hidden_id) from tbl_auction_hidden where buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id"))
        If Request.QueryString.Get("t") = "1" Then
            SqlHelper.ExecuteNonQuery("insert into tbl_auction_hidden(auction_id,buyer_user_id) values ('" & auction_id & "','" & CommonCode.Fetch_Cookie_Shared("user_id") & "')")
            If no_of_hid_auc = 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh", "redirectIframe('/SiteTopBar.aspx?t=1','/SiteLeftBar.aspx?t=1','');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh", "redirectIframe('','/SiteLeftBar.aspx?t=1','');", True)
            End If
        Else
            SqlHelper.ExecuteNonQuery("delete from tbl_auction_hidden where auction_id=" & auction_id & " and buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id"))
            If no_of_hid_auc = 1 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh", "redirectIframe('/SiteTopBar.aspx?t=1','/SiteLeftBar.aspx?t=1','/AuctionList.aspx?t=1');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh", "redirectIframe('','/SiteLeftBar.aspx?t=2','');", True)
            End If
        End If
    End Sub
    Private Sub rebindAuction(ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs, ByVal auction_id As Integer)
        Dim str As String = "SELECT A.auction_id,"
        str = str & "ISNULL(A.code, '') AS code,"
        str = str & "ISNULL(A.title, '') AS title,"
        str = str & "ISNULL(A.sub_title, '') AS sub_title,"
        str = str & "ISNULL(A.product_catetory_id, 0) AS product_catetory_id,"
        str = str & "ISNULL(A.stock_condition_id, 0) AS stock_condition_id,"
        str = str & "ISNULL(A.auction_category_id, 0) AS auction_category_id,"
        str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"
        str = str & "ISNULL(A.is_private_offer, 0) AS is_private_offer,"
        str = str & "ISNULL(A.is_partial_offer, 0) AS is_partial_offer,"
        str = str & "ISNULL(A.auto_acceptance_price_private, 0) AS auto_acceptance_price_private,"
        str = str & "ISNULL(A.auto_acceptance_price_partial, 0) AS auto_acceptance_price_partial,"
        str = str & "ISNULL(A.auto_acceptance_quantity, 0) AS auto_acceptance_quantity,"
        str = str & "ISNULL(A.no_of_clicks, 0) AS no_of_clicks,"
        str = str & "ISNULL(A.start_price, 0) AS start_price,"
        str = str & "ISNULL(A.reserve_price, 0) AS reserve_price,"
        str = str & "ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value,"

        str = str & "ISNULL(A.is_show_reserve_price, 0) AS is_show_reserve_price,"
        str = str & "ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing,"
        str = str & "ISNULL(A.is_show_no_of_bids, 0) AS is_show_no_of_bids,"
        str = str & "ISNULL(A.is_accept_pre_bid, 0) AS is_accept_pre_bid,"
        str = str & "A.use_pcs_shipping, A.use_your_shipping,"
        str = str & "ISNULL(A.increament_amount, 0) AS increament_amount,"
        str = str & "ISNULL(A.buy_now_price, 0) AS buy_now_price,"
        str = str & "ISNULL(A.is_buy_it_now, 0) AS is_buy_it_now,"
        str = str & "ISNULL(A.request_for_quote_message, '') AS request_for_quote_message,"
        str = str & "ISNULL(A.show_price, 0) AS show_price,"
        str = str & "ISNULL(A.short_description, '') AS short_description,"
        str = str & "ISNULL(A.total_qty, 0) AS total_qty,"
        str = str & "ISNULL(A.qty_per_bidder, 0) AS qty_per_bidder,"
        str = str & "dbo.get_auction_status(A.auction_id) as auction_status,"
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            str = str & "case when exists(select favourite_id from tbl_auction_favourites where auction_id=A.auction_id and buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ") then 1 else 0 end as in_fav,"
        Else
            str = str & "0 as in_fav,"
        End If
        str = str & "ISNULL(IMG.stock_image_id, 0) AS stock_image_id,ISNULL(IMG.filename, '') AS filename"
        str = str & " FROM "
        str = str & "tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations L on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B on A.stock_condition_id=B.stock_condition_id "
        str = str & " left join tbl_auction_stock_images IMG on A.auction_id=IMG.auction_id and IMG.stock_image_id=(SELECT top 1 stock_image_id from tbl_auction_stock_images where auction_id=a.auction_id order by position) "
        str = str & " WHERE "
        str = str & "A.auction_id =" & auction_id

        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDatatable(str)

        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)

                Dim bid_amount As Double = 0
                Dim bidder_max_amount As Double = 0
                CType(e.Item.FindControl("hid_auction_type"), HiddenField).Value = .Item("auction_type_id")

                'panel setting
                If .Item("auction_type_id") = 1 Then
                    CType(e.Item.FindControl("pnl_proxy"), Panel).Visible = False
                    CType(e.Item.FindControl("pnl_quote"), Panel).Visible = False
                    CType(e.Item.FindControl("pnl_now"), Panel).Visible = True
                    CType(e.Item.FindControl("lit_buy_now_price"), Literal).Text = "$" & FormatNumber(.Item("show_price"), 2)


                ElseIf .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then
                    CType(e.Item.FindControl("ltr_proxy_verbiage"), Literal).Text = IIf(.Item("auction_type_id") = 3, "<div style='text-align: left; padding-bottom: 5px;'>Your bid is a non-revocable offer to enter into a binding contract with PCS, and if the bid is the winning bid and is accepted by PCS, at PCS' sole discretion, you have entered into a legally binding contract with PCS and are obligated to purchase the item(s).</div>", "")
                    CType(e.Item.FindControl("lbl_bidder_history"), Label).Visible = True
                    CType(e.Item.FindControl("lbl_bidder_history"), Label).Text = "<a href=""javascript:void(0);"" onclick=""return open_pop_win('/Bid_History.aspx','" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&pop=1');"">view bid history</a>"

                    CType(e.Item.FindControl("pnl_now"), Panel).Visible = False
                    CType(e.Item.FindControl("pnl_proxy"), Panel).Visible = True
                    CType(e.Item.FindControl("pnl_quote"), Panel).Visible = False

                    CType(e.Item.FindControl("iframe_price_summary"), System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("src", "/PriceSummary.aspx?j=l&i=" & auction_id)

                    If .Item("auction_status") = 1 Or (.Item("auction_status") = 2 And .Item("is_accept_pre_bid")) Then
                        CType(e.Item.FindControl("lit_text_box_caption"), Literal).Text = "Enter Max Auto Bid&nbsp;<a href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('The bidder will be able to set the maximum amount he is willing to pay and the system will bid on the bidder’s behalf. Other bidders will not know his maximum amount. The system will place bids on his behalf using the automatic bid increment amount in order to surpass the current high bid. The system will bid only as much as is necessary to make sure he remains the high bidder or to meet the reserve price, up to his maximum amount. If another bidder places the same maximum bid or higher, the system will notify the bidder so he can place another bid.','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"
                    Else
                        CType(e.Item.FindControl("lit_text_box_caption"), Literal).Text = ""
                    End If
                    Dim dtHBid As New DataTable()
                    dtHBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & auction_id & " order by bid_amount desc")

                    Dim c_bid_amount As Double = 0
                    If dtHBid.Rows.Count > 0 Then
                        c_bid_amount = dtHBid.Rows(0).Item("bid_amount")
                    End If



                    Dim bidder_price As Double = 0
                    Dim bidder_max_price As Double = 0

                    Dim used_shipping_value As String = ""
                    Dim used_shipping_amount As Double = 0
                    Dim used_bid_amount As Double = 0

                    Dim dtBid As New DataTable()
                    dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount,isnull(shipping_value,'') as shipping_value,isnull(shipping_amount,0) as shipping_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & auction_id & " and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by bid_amount desc")

                    If dtBid.Rows.Count > 0 Then
                        If .Item("auction_type_id") = 3 Then
                            bidder_max_amount = dtBid.Rows(0).Item("max_bid_amount")
                        Else
                            bidder_max_amount = dtBid.Rows(0).Item("bid_amount")
                        End If

                        used_shipping_value = dtBid.Rows(0).Item("shipping_value")
                        used_shipping_amount = dtBid.Rows(0).Item("shipping_amount")
                        used_bid_amount = dtBid.Rows(0).Item("bid_amount")

                        'CType(e.Item.FindControl("your_current_bid"), Label).Text = FormatCurrency(dtBid.Rows(0).Item("bid_amount"), 2)
                        'CType(e.Item.FindControl("hid_shipping_value"), HiddenField).Value = FormatNumber(dtBid.Rows(0).Item("shipping_amount"), 2)
                        'CType(e.Item.FindControl("your_total_bid"), Label).Text = "Total:&nbsp;&nbsp;&nbsp;&nbsp;" & FormatCurrency(dtBid.Rows(0).Item("bid_amount") + dtBid.Rows(0).Item("shipping_amount"), 2)

                    End If
                    dtBid = Nothing


                    'Shipping Setting start
                    If .Item("auction_status") = 1 Or (.Item("auction_status") = 2 And .Item("is_accept_pre_bid")) Then
                        'if currently running
                        If .Item("use_pcs_shipping") = True Then
                            e.Item.FindControl("pnl_fedex").Visible = True
                            Dim rbt As RadioButtonList = CType(e.Item.FindControl("rbt_fedex_options"), RadioButtonList)
                            Dim dtShip As New DataTable
                            dtShip = SqlHelper.ExecuteDatatable("SELECT	ISNULL(A.shipping_options, '') AS shipping_options,ISNULL(A.shipping_amount, 0) AS shipping_amount FROM tbl_auction_fedex_rates A WITH (NOLOCK) WHERE	AUCTION_ID=" & auction_id & " AND BUYER_ID=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and is_error=0 ORDER BY shipping_amount")
                            rbt.Items.Clear()
                            Dim is_f_select As Boolean = False
                            Dim f_value_to_select As Double = 0
                            If dtShip.Rows.Count > 0 Then
                                Dim litm As ListItem
                                For i As Integer = 0 To dtShip.Rows.Count - 1
                                    litm = New ListItem
                                    litm.Value = dtShip.Rows(i)("shipping_options").ToString()
                                    litm.Text = dtShip.Rows(i)("shipping_options").ToString().Replace("_", " ") & " <span style='font-weight:bold;color:Red;'>" & FormatCurrency(dtShip.Rows(i)("shipping_amount"), 2) & "</span>"
                                    litm.Attributes.Add("onclick", "return changeShipping('" & used_bid_amount & "','" & dtShip.Rows(i)("shipping_amount") & "','" & e.Item.FindControl("hid_shipping_option").ClientID & "','" & e.Item.FindControl("hid_fedex_selected").ClientID & "','" & e.Item.FindControl("hid_shipping_value").ClientID & "','" & e.Item.FindControl("your_current_shipping").ClientID & "','" & e.Item.FindControl("your_total_bid").ClientID & "');")
                                    If used_shipping_value.IndexOf("shipping option:") > 0 Then
                                        If used_shipping_amount = dtShip.Rows(i)("shipping_amount") Then
                                            is_f_select = True
                                            litm.Selected = True
                                            CType(e.Item.FindControl("rdo_option"), RadioButtonList).Items.FindByValue(2).Selected = True
                                            CType(e.Item.FindControl("hid_shipping_option"), HiddenField).Value = 2
                                            CType(e.Item.FindControl("hid_shipping_value"), HiddenField).Value = used_shipping_amount
                                            CType(e.Item.FindControl("hid_fedex_selected"), HiddenField).Value = used_shipping_amount
                                        Else
                                            litm.Selected = False
                                        End If
                                    End If
                                    If i = 0 Then
                                        f_value_to_select = dtShip.Rows(i)("shipping_amount")
                                    End If

                                    rbt.Items.Insert(i, litm)
                                Next

                                If is_f_select = False Then
                                    For i As Integer = 0 To rbt.Items.Count - 1
                                        If rbt.Items(i).Text.IndexOf(FormatCurrency(f_value_to_select, 2)) > 0 Then
                                            rbt.Items(i).Selected = True
                                        End If
                                    Next

                                    CType(e.Item.FindControl("rdo_option"), RadioButtonList).Items.FindByValue(2).Selected = True
                                    CType(e.Item.FindControl("hid_shipping_option"), HiddenField).Value = 2
                                    CType(e.Item.FindControl("hid_shipping_value"), HiddenField).Value = f_value_to_select
                                    CType(e.Item.FindControl("hid_fedex_selected"), HiddenField).Value = f_value_to_select
                                End If

                            Else
                                e.Item.FindControl("pnl_fedex").Visible = False
                                CType(e.Item.FindControl("rdo_option"), RadioButtonList).Items(1).Text = "Shipping rates will be calculated later"
                            End If



                            If .Item("use_your_shipping") = True Then
                                'both shipping available
                                e.Item.FindControl("rdo_option").Visible = True
                                Dim rbtOption As RadioButtonList = CType(e.Item.FindControl("rdo_option"), RadioButtonList)
                                For i As Integer = 0 To rbtOption.Items.Count - 1
                                    rbtOption.Items(i).Attributes.Add("onclick", "return changeShippingOption(" & rbtOption.Items(i).Value & ",'" & used_bid_amount & "','" & IIf(i = 0, 0, CType(e.Item.FindControl("hid_fedex_selected"), HiddenField).Value) & "','" & e.Item.FindControl("hid_shipping_option").ClientID & "','" & e.Item.FindControl("hid_fedex_selected").ClientID & "','" & e.Item.FindControl("hid_shipping_value").ClientID & "','" & e.Item.FindControl("your_current_shipping").ClientID & "','" & e.Item.FindControl("your_total_bid").ClientID & "');")
                                Next

                                If used_bid_amount = 0 Then
                                    'first time
                                    CType(e.Item.FindControl("rdo_option"), RadioButtonList).Items.FindByValue(1).Selected = True
                                    CType(e.Item.FindControl("hid_shipping_option"), HiddenField).Value = 1
                                Else
                                    If CType(e.Item.FindControl("hid_shipping_option"), HiddenField).Value = 0 Then
                                        CType(e.Item.FindControl("rdo_option"), RadioButtonList).Items.FindByValue(1).Selected = True
                                        CType(e.Item.FindControl("hid_shipping_option"), HiddenField).Value = 1
                                    End If
                                End If

                            Else
                                'only fedex available
                                e.Item.FindControl("rdo_option").Visible = False
                                If e.Item.FindControl("pnl_fedex").Visible = False Then
                                    CType(e.Item.FindControl("lit_own_shipping"), Literal).Text = "Shipping rates will be calculated later"
                                End If

                                CType(e.Item.FindControl("rdo_option"), RadioButtonList).Items.FindByValue(2).Selected = True
                                CType(e.Item.FindControl("hid_shipping_option"), HiddenField).Value = 2

                            End If
                        Else
                            'only my shipping available
                            CType(e.Item.FindControl("lit_own_shipping"), Literal).Text = "Use my own shipping carrier to ship this item to me"
                            CType(e.Item.FindControl("rdo_option"), RadioButtonList).Items.FindByValue(1).Selected = True
                            CType(e.Item.FindControl("hid_shipping_option"), HiddenField).Value = 1
                        End If


                    Else
                        'if bid over
                        e.Item.FindControl("pnl_your_bid").Visible = True
                        e.Item.FindControl("rdo_option").Visible = False
                        e.Item.FindControl("pnl_fedex").Visible = False
                        CType(e.Item.FindControl("lit_own_shipping"), Literal).Text = used_shipping_value
                    End If

                    CType(e.Item.FindControl("your_current_bid"), Label).Text = FormatCurrency(used_bid_amount, 2)
                    CType(e.Item.FindControl("your_current_shipping"), Label).Text = FormatNumber(used_shipping_amount, 2)
                    CType(e.Item.FindControl("your_total_bid"), Label).Text = "Total:&nbsp;&nbsp;&nbsp;&nbsp;" & FormatCurrency(used_bid_amount + used_shipping_amount, 2)


                    'Shipping Setting End



                    If dtHBid.Rows.Count > 0 Then
                        'running bid
                        bid_amount = dtHBid.Rows(0).Item("bid_amount")
                        If .Item("auction_type_id") = 2 Then
                            ' hid_bid_now_price.Value = dtHBid.Rows(0).Item("bid_amount") + .Item("increament_amount")

                        Else
                            If bidder_max_amount = 0 Then 'bidder bid first time
                                bidder_max_amount = dtHBid.Rows(0).Item("bid_amount") + .Item("increament_amount")

                            Else
                                If bidder_max_amount <= dtHBid.Rows(0).Item("bid_amount") Then '2nd position
                                    bidder_max_amount = dtHBid.Rows(0).Item("bid_amount") + .Item("increament_amount")
                                End If
                            End If
                        End If
                    Else
                        'first bid for this auction
                        bidder_max_amount = .Item("start_price")
                        'hid_bid_now_price.Value = .Item("start_price")
                    End If

                    dtHBid = Nothing


                Else

                    CType(e.Item.FindControl("pnl_quote"), Panel).Visible = True
                    CType(e.Item.FindControl("lit_quote_msg"), Literal).Text = .Item("request_for_quote_message")
                    CType(e.Item.FindControl("pnl_now"), Panel).Visible = False
                    CType(e.Item.FindControl("pnl_proxy"), Panel).Visible = False

                End If

                'bid button setting
                If .Item("auction_status") = 1 Or (.Item("auction_status") = 2 And (.Item("auction_type_id") = 3 Or .Item("auction_type_id") = 2) And Convert.ToBoolean(.Item("is_accept_pre_bid"))) Then
                    Dim buy_it_now_bid As Integer = SqlHelper.ExecuteScalar("select count(*) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and status='Accept' and auction_id=" & auction_id)
                    CType(e.Item.FindControl("img_bid_now"), ImageButton).Visible = True

                    'chk_accept_bid_now.Visible = True
                    If .Item("auction_type_id") = 1 Then
                        CType(e.Item.FindControl("img_bid_now"), ImageButton).AlternateText = "Buy Now"
                        CType(e.Item.FindControl("tr_box_caption"), HtmlTableRow).Visible = False
                        CType(e.Item.FindControl("tr_bid_now"), HtmlTableRow).Visible = False
                        'e.Item.FindControl("tr_bid_now").Visible = False
                        'e.Item.FindControl("tr_box_caption").Visible = False
                        'Response.Write(buy_it_now_bid.ToString & "---" & .Item("no_of_clicks").ToString & "---" & .Item("is_buy_it_now").ToString & "<br>")
                        'chk_accept_bid_now.Visible = False
                        ' If buy_it_now_bid <= .Item("no_of_clicks") Then
                        CType(e.Item.FindControl("img_bid_now"), ImageButton).ImageUrl = "/images/fend/7.png"
                        'code add by ajay
                        'CType(e.Item.FindControl("img_bid_now"), ImageButton).Attributes.Add("onclick", "return openOffer('" & auction_id & "','0','" & .Item("auction_type_id") & "','" & .Item("show_price") & "')")
                        'If bid_amount < .Item("thresh_hold_value") Or .Item("thresh_hold_value") = 0 Then
                        'If Convert.ToBoolean(.Item("is_buy_it_now")) Then

                        Dim rem_qty As Integer = 0
                        If .Item("qty_per_bidder") > 0 Then
                            Dim already_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and auction_id=" & auction_id & "")
                            rem_qty = .Item("qty_per_bidder") - already_buy
                        End If

                        Dim total_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and auction_id=" & auction_id & "")
                        If .Item("total_qty") - total_buy < rem_qty Then
                            rem_qty = .Item("total_qty") - total_buy
                        End If
                        If rem_qty > 0 Then
                            For i = 1 To rem_qty
                                CType(e.Item.FindControl("ddl_qty_confirm"), DropDownList).Items.Insert(i - 1, New ListItem(i, i))
                            Next

                            CType(e.Item.FindControl("pnl_buy_now_confirm"), Panel).Visible = True
                            CType(e.Item.FindControl("lit_buy_now_amount_confirm"), Literal).Text = FormatCurrency(.Item("show_price"), 2)
                            CType(e.Item.FindControl("hid_buy_now_confirm_amount"), HiddenField).Value = .Item("show_price")
                        Else
                            CType(e.Item.FindControl("pnl_buy_now_confirm"), Panel).Visible = False
                            CType(e.Item.FindControl("img_bid_now"), ImageButton).Visible = False
                        End If

                        CType(e.Item.FindControl("img_bid_now"), ImageButton).Attributes.Add("onclick", "return redirectConfirm('" & CType(e.Item.FindControl("ddl_qty_confirm"), DropDownList).ClientID & "'," & .Item("show_price") & "," & auction_id & ",'buy now')")
                        If Convert.ToBoolean(.Item("is_private_offer")) Then
                            CType(e.Item.FindControl("lit_offer"), Literal).Text = "<a href=""javascript:void(0)"" onclick=""return openOffer('" & auction_id & "','2','" & .Item("auction_type_id") & "','" & .Item("show_price") & "');""><img src='/Images/fend/4.png' alt='Private Offer' border='0'></a>"
                        End If
                        If Convert.ToBoolean(.Item("is_partial_offer")) Then
                            CType(e.Item.FindControl("lit_offer"), Literal).Text = "<a href=""javascript:void(0)"" onclick=""return openOffer('" & auction_id & "','3','" & .Item("auction_type_id") & "','" & .Item("show_price") & "');""><img src='/Images/fend/4.png' alt='Partial Offer' border='0'></a>"
                        End If


                    ElseIf .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then

                        Dim img_amt_help As Image = CType(e.Item.FindControl("img_amt_help"), Image)
                        Dim refrid As String = CType(e.Item.FindControl("btn_refresh"), Button).ClientID
                        Dim txtid As String = CType(e.Item.FindControl("txt_bid_now"), TextBox).ClientID
                        Dim cmpVal As String = bidder_max_amount

                        Dim divid As String = CType(e.Item.FindControl("div_Validator"), System.Web.UI.HtmlControls.HtmlGenericControl).ClientID
                        CType(e.Item.FindControl("img_bid_now"), ImageButton).ImageUrl = "/images/fend/6.png"
                        CType(e.Item.FindControl("txt_bid_now"), TextBox).Visible = True
                        CType(e.Item.FindControl("img_bid_now"), ImageButton).Attributes.Add("onclick", "return validBid('" & auction_id & "','" & txtid & "','" & cmpVal & "','" & divid & "','0','" & .Item("auction_type_id") & "','" & refrid & "','" & CType(e.Item.FindControl("hid_shipping_option"), HiddenField).ClientID & "','" & CType(e.Item.FindControl("hid_shipping_value"), HiddenField).ClientID & "','" & img_amt_help.ClientID & "')")
                        If bid_amount < .Item("thresh_hold_value") Or .Item("thresh_hold_value") = 0 Then

                            If Convert.ToBoolean(.Item("is_buy_it_now")) Then

                                Dim rem_qty As Integer = 0
                                If .Item("qty_per_bidder") > 0 Then
                                    Dim already_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and auction_id=" & auction_id & "")
                                    rem_qty = .Item("qty_per_bidder") - already_buy
                                End If

                                Dim total_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and auction_id=" & auction_id & "")
                                If .Item("total_qty") - total_buy < rem_qty Then
                                    rem_qty = .Item("total_qty") - total_buy
                                End If


                                If rem_qty > 0 Then
                                    For i = 1 To rem_qty
                                        CType(e.Item.FindControl("ddl_qty"), DropDownList).Items.Insert(i - 1, New ListItem(i, i))
                                    Next
                                    CType(e.Item.FindControl("pnl_buy_now"), Panel).Visible = True
                                    CType(e.Item.FindControl("lit_buy_now_amount"), Literal).Text = FormatCurrency(.Item("buy_now_price"), 2)
                                    CType(e.Item.FindControl("hid_buy_now_amount"), HiddenField).Value = .Item("buy_now_price")
                                Else
                                    CType(e.Item.FindControl("pnl_buy_now"), Panel).Visible = False
                                End If

                                'vish
                                'CType(e.Item.FindControl("lit_offer"), Literal).Text = "<a href=""javascript:void(0)"" onclick=""return openOffer('" & auction_id & "','1','" & .Item("auction_type_id") & "','" & .Item("buy_now_price") & "');""><img src='/Images/fend/4.png' alt='Buy it Now' border='0'></a>"
                            End If
                            If Convert.ToBoolean(.Item("is_private_offer")) Then
                                CType(e.Item.FindControl("lit_offer"), Literal).Text = "<a href=""javascript:void(0)"" onclick=""return openOffer('" & auction_id & "','2','" & .Item("auction_type_id") & "','" & .Item("buy_now_price") & "');""><img src='/Images/fend/4.png' alt='Private Offer' border='0'></a>"
                            End If

                            If Convert.ToBoolean(.Item("is_partial_offer")) Then
                                CType(e.Item.FindControl("lit_offer"), Literal).Text = "<a href=""javascript:void(0)"" onclick=""return openOffer('" & auction_id & "','3','" & .Item("auction_type_id") & "','" & .Item("buy_now_price") & "');""><img src='/Images/fend/4.png' alt='Partial Offer' border='0'></a>"
                            End If
                        Else
                            CType(e.Item.FindControl("pnl_buy_now"), Panel).Visible = False
                            CType(e.Item.FindControl("lit_offer"), Literal).Text = ""
                        End If

                    Else
                        CType(e.Item.FindControl("img_bid_now"), ImageButton).Attributes.Add("onclick", "return openOffer('" & auction_id & "','0','" & .Item("auction_type_id") & "','0')")
                        CType(e.Item.FindControl("img_bid_now"), ImageButton).ImageUrl = "/images/fend/8.png"
                        CType(e.Item.FindControl("img_bid_now"), ImageButton).AlternateText = "Offer"
                    End If
                End If

                CType(e.Item.FindControl("iframe_price"), System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("src", "/timerframe.aspx?is_frontend=1&i=" & auction_id)

            End With
        Else
            e.Item.Visible = False
        End If
        dtTable = Nothing
    End Sub
    Private Sub setAuction(ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs, ByVal auction_id As Integer, ByVal is_grid_view As Boolean)

        Dim str As String = "SELECT A.auction_id,"
        str = str & "ISNULL(A.code, '') AS code,"
        str = str & "ISNULL(A.title, '') AS title,"
        str = str & "ISNULL(A.sub_title, '') AS sub_title,"
        str = str & "ISNULL(A.product_catetory_id, 0) AS product_catetory_id,"
        str = str & "ISNULL(A.stock_condition_id, 0) AS stock_condition_id,"
        str = str & "ISNULL(A.auction_category_id, 0) AS auction_category_id,"
        str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"
        str = str & "ISNULL(A.is_private_offer, 0) AS is_private_offer,"
        str = str & "ISNULL(A.is_partial_offer, 0) AS is_partial_offer,"
        str = str & "ISNULL(A.auto_acceptance_price_private, 0) AS auto_acceptance_price_private,"
        str = str & "ISNULL(A.auto_acceptance_price_partial, 0) AS auto_acceptance_price_partial,"
        str = str & "ISNULL(A.auto_acceptance_quantity, 0) AS auto_acceptance_quantity,"
        str = str & "ISNULL(A.no_of_clicks, 0) AS no_of_clicks,"
        str = str & "ISNULL(A.start_price, 0) AS start_price,"
        str = str & "ISNULL(A.reserve_price, 0) AS reserve_price,"
        str = str & "ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value,"

        str = str & "ISNULL(A.is_show_reserve_price, 0) AS is_show_reserve_price,"
        str = str & "ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing,"
        str = str & "ISNULL(A.is_show_no_of_bids, 0) AS is_show_no_of_bids,"
        str = str & "ISNULL(A.is_accept_pre_bid, 0) AS is_accept_pre_bid,"
        str = str & "A.use_pcs_shipping, A.use_your_shipping,"
        str = str & "ISNULL(A.increament_amount, 0) AS increament_amount,"
        str = str & "ISNULL(A.buy_now_price, 0) AS buy_now_price,"
        str = str & "ISNULL(A.is_buy_it_now, 0) AS is_buy_it_now,"
        str = str & "ISNULL(A.request_for_quote_message, '') AS request_for_quote_message,"
        str = str & "ISNULL(A.show_price, 0) AS show_price,"
        str = str & "ISNULL(A.short_description, '') AS short_description,"
        str = str & "ISNULL(A.total_qty, 0) AS total_qty,"
        str = str & "ISNULL(A.qty_per_bidder, 0) AS qty_per_bidder,"
        str = str & "dbo.get_auction_status(A.auction_id) as auction_status,"
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            str = str & "case when exists(select favourite_id from tbl_auction_favourites where auction_id=A.auction_id and buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ") then 1 else 0 end as in_fav,"
        Else
            str = str & "0 as in_fav,"
        End If
        str = str & "ISNULL(IMG.stock_image_id, 0) AS stock_image_id,ISNULL(IMG.filename, '') AS filename"
        str = str & " FROM "
        str = str & "tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations L on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B on A.stock_condition_id=B.stock_condition_id "
        str = str & " left join tbl_auction_stock_images IMG on A.auction_id=IMG.auction_id and IMG.stock_image_id=(SELECT top 1 stock_image_id from tbl_auction_stock_images where auction_id=a.auction_id order by position) "
        str = str & " WHERE "
        str = str & "A.auction_id =" & auction_id

        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDatatable(str)

        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)
                CType(e.Item.FindControl("lbl_auction_code"), Label).Text = .Item("code")
                CType(e.Item.FindControl("lbl_title"), Literal).Text = "<a href='/Auction_Pdf.aspx?i=" & auction_id & "&t=" & Request.QueryString.Get("t") & "' style='font-size:20px;'>" & .Item("title") & "</a>"
                CType(e.Item.FindControl("lbl_sub_title"), Label).Text = .Item("sub_title")
                CType(e.Item.FindControl("lbl_short_desc"), Literal).Text = .Item("short_description")

                If CommonCode.Fetch_Cookie_Shared("user_id") <> "" And (Request.QueryString.Get("t") = "1" Or Request.QueryString.Get("t") = "2") Then
                    CType(e.Item.FindControl("img_hide_now"), ImageButton).Visible = True
                    If Request.QueryString.Get("t") = "1" Then
                        CType(e.Item.FindControl("img_hide_now"), ImageButton).ImageUrl = "/images/fend/9.png"
                    Else
                        CType(e.Item.FindControl("img_hide_now"), ImageButton).ImageUrl = "/images/fend/10.png"
                    End If
                Else
                    CType(e.Item.FindControl("img_hide_now"), ImageButton).Visible = False
                End If

                CType(e.Item.FindControl("img_ask_question"), ImageButton).Attributes.Add("onclick", "return openAskQuestion(" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & ");")

                If CommonCode.Fetch_Cookie_Shared("user_id") <> "" And (Request.QueryString.Get("t") = "1" Or Request.QueryString.Get("t") = "4") Then
                    CType(e.Item.FindControl("img_add_favourite"), ImageButton).Visible = True
                    If Request.QueryString.Get("t") = "1" Then
                        If Convert.ToBoolean(.Item("in_fav")) Then
                            CType(e.Item.FindControl("img_add_favourite"), ImageButton).Visible = False
                        Else
                            CType(e.Item.FindControl("img_add_favourite"), ImageButton).ImageUrl = "/images/fend/12.png"
                        End If
                    Else
                        CType(e.Item.FindControl("img_add_favourite"), ImageButton).ImageUrl = "/images/fend/13.png"
                    End If
                Else
                    CType(e.Item.FindControl("img_add_favourite"), ImageButton).Visible = False
                End If



                If .Item("filename").ToString <> "" Then
                    CType(e.Item.FindControl("img_image"), Image).ImageUrl = "/upload/auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString.Replace(".", "_thumb1.")
                    str = "SELECT A.auction_id,convert(varchar,A.auction_id)+'/'+convert(varchar,A.stock_image_id)+'/'+A.filename as path, b.title "
                    str = str & " from tbl_auction_stock_images A inner join tbl_auctions b WITH (NOLOCK) on a.auction_id=b.auction_id where A.auction_id=" & auction_id
                    'Response.Write(str)
                    CType(e.Item.FindControl("rpt_auction_stock_image"), Repeater).DataSource = SqlHelper.ExecuteDatatable(str)
                    CType(e.Item.FindControl("rpt_auction_stock_image"), Repeater).DataBind()
                    CType(e.Item.FindControl("a_img_product"), HtmlAnchor).HRef = "/upload/auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename")
                    CType(e.Item.FindControl("a_img_product"), HtmlAnchor).Title = .Item("title")
                    If is_grid_view Then
                        CType(e.Item.FindControl("a_img_product"), HtmlAnchor).Attributes.Add("rel", "lightbox[PerfumeSize" & auction_id & "1]")

                    Else
                        CType(e.Item.FindControl("a_img_product"), HtmlAnchor).Attributes.Add("rel", "lightbox[PerfumeSize" & auction_id & "0]")

                    End If
                    rel = auction_id
                Else
                    CType(e.Item.FindControl("img_image"), Image).ImageUrl = "/images/imagenotavailable.gif"
                End If

                Dim bid_amount As Double = 0
                Dim bidder_max_amount As Double = 0
                CType(e.Item.FindControl("hid_auction_type"), HiddenField).Value = .Item("auction_type_id")

                'panel setting
                If .Item("auction_type_id") = 1 Then
                    CType(e.Item.FindControl("pnl_proxy"), Panel).Visible = False
                    CType(e.Item.FindControl("pnl_quote"), Panel).Visible = False
                    CType(e.Item.FindControl("pnl_now"), Panel).Visible = True
                    CType(e.Item.FindControl("lit_buy_now_price"), Literal).Text = "$" & FormatNumber(.Item("show_price"), 2)


                ElseIf .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then
                    CType(e.Item.FindControl("ltr_proxy_verbiage"), Literal).Text = IIf(.Item("auction_type_id") = 3, "<div style='text-align: left;font-weight:bold; color:gray'>Your bid is a non-revocable offer to enter into a binding contract with PCS, and if the bid is the winning bid and is accepted by PCS, at PCS' sole discretion, you have entered into a legally binding contract with PCS and are obligated to purchase the item(s).</div>", "")
                    CType(e.Item.FindControl("lbl_bidder_history"), Label).Visible = True
                    CType(e.Item.FindControl("lbl_bidder_history"), Label).Text = "<a href=""javascript:void(0);"" onclick=""return open_pop_win('/Bid_History.aspx','" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&pop=1');"">view bid history</a>"

                    CType(e.Item.FindControl("pnl_now"), Panel).Visible = False
                    CType(e.Item.FindControl("pnl_proxy"), Panel).Visible = True
                    CType(e.Item.FindControl("pnl_quote"), Panel).Visible = False

                    CType(e.Item.FindControl("pnl_your_bid"), Panel).Visible = True


                    If rpt_auctions.Visible = True Then
                        CType(e.Item.FindControl("iframe_price_summary"), System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("src", "/PriceSummary.aspx?j=l&i=" & auction_id)
                    Else
                        CType(e.Item.FindControl("iframe_price_summary"), System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("src", "/PriceSummary.aspx?j=g&i=" & auction_id)
                    End If

                    If .Item("auction_status") = 1 Or (.Item("auction_status") = 2 And .Item("is_accept_pre_bid")) Then
                        CType(e.Item.FindControl("lit_text_box_caption"), Literal).Text = "Enter Max Auto Bid&nbsp;<a href=""javascript:void(0);"" onmouseout=""hide_tip_new();"" onmouseover=""tip_new('The bidder will be able to set the maximum amount he is willing to pay and the system will bid on the bidder’s behalf. Other bidders will not know his maximum amount. The system will place bids on his behalf using the automatic bid increment amount in order to surpass the current high bid. The system will bid only as much as is necessary to make sure he remains the high bidder or to meet the reserve price, up to his maximum amount. If another bidder places the same maximum bid or higher, the system will notify the bidder so he can place another bid.','450','white');""><img src='/Images/fend/help.jpg' alt='' border='0'></a>"
                    Else
                        CType(e.Item.FindControl("lit_text_box_caption"), Literal).Text = ""
                    End If
                   
                    Dim dtHBid As New DataTable()
                    dtHBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & auction_id & " order by bid_amount desc")

                    Dim c_bid_amount As Double = 0
                    If dtHBid.Rows.Count > 0 Then
                        c_bid_amount = dtHBid.Rows(0).Item("bid_amount")
                    End If

                    Dim bidder_price As Double = 0
                    Dim bidder_max_price As Double = 0

                    Dim used_shipping_value As String = ""
                    Dim used_shipping_amount As Double = 0
                    Dim used_bid_amount As Double = 0

                    Dim dtBid As New DataTable()
                    'Response.Write("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount,isnull(shipping_value,'') as shipping_value,isnull(shipping_amount,0) as shipping_amount from tbl_auction_bids where auction_id=" & auction_id & " and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by bid_amount desc")
                    dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount,isnull(shipping_value,'') as shipping_value,isnull(shipping_amount,0) as shipping_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & auction_id & " and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by bid_amount desc")

                    
                    If dtBid.Rows.Count > 0 Then

                        Dim pnl_fedex As Panel = CType(e.Item.FindControl("pnl_fedex"), Panel)
                        If .Item("auction_type_id") = 3 Then
                            bidder_max_amount = dtBid.Rows(0).Item("max_bid_amount")
                        Else
                            bidder_max_amount = dtBid.Rows(0).Item("bid_amount")
                        End If

                        used_shipping_value = dtBid.Rows(0).Item("shipping_value")
                        used_shipping_amount = dtBid.Rows(0).Item("shipping_amount")
                        used_bid_amount = dtBid.Rows(0).Item("bid_amount")

                    End If
                    dtBid = Nothing
                    Dim img_amt_help As Image = CType(e.Item.FindControl("img_amt_help"), Image)
                    img_amt_help.Attributes.Add("onmouseout", "hide_tip_new();")
                    'img_amt_help.Attributes.Add("onmouseover", "tip_new('Why wasn’t my amount accepted?<br />Your maximum bid may not be accepted if the amount is:<br />1. Less than what the auction must start at (" & FormatCurrency(.Item("start_price"), 2) & ")<br />2. Less than the current winning bid (" & FormatCurrency(c_bid_amount, 2) & ").<br />3. An invalid increment amount (" & FormatCurrency(.Item("increament_amount"), 2) & ").<br />4. The same maximum bid of another bidder.;")
                    img_amt_help.Attributes.Add("onmouseover", "tip_new('Why wasn’t my amount accepted?<br />Your maximum bid may not be accepted if the amount is:<br />1. Less than what the auction must start at (" & FormatCurrency(.Item("start_price"), 2) & ")<br />2. Less than the current winning bid (" & FormatCurrency(c_bid_amount, 2) & ").<br />3. An invalid increment amount (" & FormatCurrency(.Item("increament_amount"), 2) & ").<br />4. The same maximum bid of another bidder.','450','white');")
                    If dtHBid.Rows.Count > 0 Then
                        'running bid
                        bid_amount = dtHBid.Rows(0).Item("bid_amount")
                        If .Item("auction_type_id") = 2 Then
                            ' hid_bid_now_price.Value = dtHBid.Rows(0).Item("bid_amount") + .Item("increament_amount")
                        Else
                            If bidder_max_amount = 0 Then 'bidder bid first time
                                bidder_max_amount = dtHBid.Rows(0).Item("bid_amount") + .Item("increament_amount")
                            Else
                                If bidder_max_amount <= dtHBid.Rows(0).Item("bid_amount") Then '2nd position
                                    bidder_max_amount = dtHBid.Rows(0).Item("bid_amount") + .Item("increament_amount")
                                End If
                            End If
                        End If
                    Else
                        'first bid for this auction
                        bidder_max_amount = .Item("start_price")
                        'hid_bid_now_price.Value = .Item("start_price")
                    End If

                    dtHBid = Nothing


                    'Shipping Setting start
                    If .Item("auction_status") = 1 Or (.Item("auction_status") = 2 And .Item("is_accept_pre_bid")) Then
                        'if currently running
                        If .Item("use_pcs_shipping") = True Then
                            e.Item.FindControl("pnl_fedex").Visible = True
                            Dim rbt As RadioButtonList = CType(e.Item.FindControl("rbt_fedex_options"), RadioButtonList)
                            Dim dtShip As New DataTable
                            dtShip = SqlHelper.ExecuteDatatable("SELECT	ISNULL(A.shipping_options, '') AS shipping_options,ISNULL(A.shipping_amount, 0) AS shipping_amount FROM tbl_auction_fedex_rates A WITH (NOLOCK) WHERE	AUCTION_ID=" & auction_id & " AND BUYER_ID=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and is_error=0 ORDER BY shipping_amount")
                            rbt.Items.Clear()
                            Dim is_f_select As Boolean = False
                            Dim f_value_to_select As Double = 0
                            If dtShip.Rows.Count > 0 Then
                                Dim litm As ListItem
                                For i As Integer = 0 To dtShip.Rows.Count - 1
                                    litm = New ListItem
                                    litm.Value = dtShip.Rows(i)("shipping_options").ToString()
                                    litm.Text = dtShip.Rows(i)("shipping_options").ToString().Replace("_", " ") & " <span style='font-weight:bold;color:Red;'>" & FormatCurrency(dtShip.Rows(i)("shipping_amount"), 2) & "</span>"
                                    litm.Attributes.Add("onclick", "return changeShipping('" & used_bid_amount & "','" & dtShip.Rows(i)("shipping_amount") & "','" & e.Item.FindControl("hid_shipping_option").ClientID & "','" & e.Item.FindControl("hid_fedex_selected").ClientID & "','" & e.Item.FindControl("hid_shipping_value").ClientID & "','" & e.Item.FindControl("your_current_shipping").ClientID & "','" & e.Item.FindControl("your_total_bid").ClientID & "');")
                                    If used_shipping_value.IndexOf("shipping option:") > 0 Then
                                        If used_shipping_amount = dtShip.Rows(i)("shipping_amount") Then
                                            is_f_select = True
                                            litm.Selected = True
                                            CType(e.Item.FindControl("rdo_option"), RadioButtonList).Items.FindByValue(2).Selected = True
                                            CType(e.Item.FindControl("hid_shipping_option"), HiddenField).Value = 2
                                            CType(e.Item.FindControl("hid_shipping_value"), HiddenField).Value = used_shipping_amount
                                            CType(e.Item.FindControl("hid_fedex_selected"), HiddenField).Value = used_shipping_amount
                                        Else
                                            litm.Selected = False
                                        End If
                                    End If
                                    If i = 0 Then
                                        f_value_to_select = dtShip.Rows(i)("shipping_amount")
                                    End If

                                    rbt.Items.Insert(i, litm)
                                Next

                                If is_f_select = False Then
                                    For i As Integer = 0 To rbt.Items.Count - 1
                                        If rbt.Items(i).Text.IndexOf(FormatCurrency(f_value_to_select, 2)) > 0 Then
                                            rbt.Items(i).Selected = True
                                        End If
                                    Next

                                    CType(e.Item.FindControl("rdo_option"), RadioButtonList).Items.FindByValue(2).Selected = True
                                    CType(e.Item.FindControl("hid_shipping_option"), HiddenField).Value = 2
                                    CType(e.Item.FindControl("hid_shipping_value"), HiddenField).Value = f_value_to_select
                                    CType(e.Item.FindControl("hid_fedex_selected"), HiddenField).Value = f_value_to_select
                                End If

                            Else
                                e.Item.FindControl("pnl_fedex").Visible = False
                                CType(e.Item.FindControl("rdo_option"), RadioButtonList).Items(1).Text = "Shipping rates will be calculated later"
                            End If



                            If .Item("use_your_shipping") = True Then
                                'both shipping available
                                e.Item.FindControl("rdo_option").Visible = True
                                Dim rbtOption As RadioButtonList = CType(e.Item.FindControl("rdo_option"), RadioButtonList)
                                For i As Integer = 0 To rbtOption.Items.Count - 1
                                    rbtOption.Items(i).Attributes.Add("onclick", "return changeShippingOption(" & rbtOption.Items(i).Value & ",'" & used_bid_amount & "','" & IIf(i = 0, 0, CType(e.Item.FindControl("hid_fedex_selected"), HiddenField).Value) & "','" & e.Item.FindControl("hid_shipping_option").ClientID & "','" & e.Item.FindControl("hid_fedex_selected").ClientID & "','" & e.Item.FindControl("hid_shipping_value").ClientID & "','" & e.Item.FindControl("your_current_shipping").ClientID & "','" & e.Item.FindControl("your_total_bid").ClientID & "');")
                                Next

                                If used_bid_amount = 0 Then
                                    'first time
                                    CType(e.Item.FindControl("rdo_option"), RadioButtonList).Items.FindByValue(1).Selected = True
                                    CType(e.Item.FindControl("hid_shipping_option"), HiddenField).Value = 1
                                Else
                                    If CType(e.Item.FindControl("hid_shipping_option"), HiddenField).Value = 0 Then
                                        CType(e.Item.FindControl("rdo_option"), RadioButtonList).Items.FindByValue(1).Selected = True
                                        CType(e.Item.FindControl("hid_shipping_option"), HiddenField).Value = 1
                                    End If
                                End If

                            Else
                                'only fedex available
                                e.Item.FindControl("rdo_option").Visible = False
                                If e.Item.FindControl("pnl_fedex").Visible = False Then
                                    CType(e.Item.FindControl("lit_own_shipping"), Literal).Text = "Shipping rates will be calculated later"
                                End If

                                CType(e.Item.FindControl("rdo_option"), RadioButtonList).Items.FindByValue(2).Selected = True
                                CType(e.Item.FindControl("hid_shipping_option"), HiddenField).Value = 2

                            End If
                        Else
                            'only my shipping available
                            CType(e.Item.FindControl("lit_own_shipping"), Literal).Text = "Use my own shipping carrier to ship this item to me"
                            CType(e.Item.FindControl("rdo_option"), RadioButtonList).Items.FindByValue(1).Selected = True
                            CType(e.Item.FindControl("hid_shipping_option"), HiddenField).Value = 1
                        End If



                    Else
                        'if bid over
                        e.Item.FindControl("pnl_your_bid").Visible = True
                        e.Item.FindControl("rdo_option").Visible = False
                        e.Item.FindControl("pnl_fedex").Visible = False
                        CType(e.Item.FindControl("lit_own_shipping"), Literal).Text = used_shipping_value
                    End If

                    CType(e.Item.FindControl("your_current_bid"), Label).Text = FormatCurrency(used_bid_amount, 2)
                    CType(e.Item.FindControl("your_current_shipping"), Label).Text = FormatNumber(used_shipping_amount, 2)
                    CType(e.Item.FindControl("your_total_bid"), Label).Text = "Total:&nbsp;&nbsp;&nbsp;&nbsp;" & FormatCurrency(used_bid_amount + used_shipping_amount, 2)


                    'Shipping Setting End


                Else

                    CType(e.Item.FindControl("pnl_quote"), Panel).Visible = True
                    CType(e.Item.FindControl("lit_quote_msg"), Literal).Text = .Item("request_for_quote_message")
                    CType(e.Item.FindControl("pnl_now"), Panel).Visible = False
                    CType(e.Item.FindControl("pnl_proxy"), Panel).Visible = False

                End If

                'bid button setting
                If .Item("auction_status") = 1 Or (.Item("auction_status") = 2 And (.Item("auction_type_id") = 3 Or .Item("auction_type_id") = 2) And Convert.ToBoolean(.Item("is_accept_pre_bid"))) Then
                    Dim buy_it_now_bid As Integer = SqlHelper.ExecuteScalar("select count(*) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and status='Accept' and auction_id=" & auction_id)
                    CType(e.Item.FindControl("img_bid_now"), ImageButton).Visible = True

                    'chk_accept_bid_now.Visible = True
                    If .Item("auction_type_id") = 1 Then
                        CType(e.Item.FindControl("img_bid_now"), ImageButton).AlternateText = "Buy Now"
                        CType(e.Item.FindControl("tr_box_caption"), HtmlTableRow).Visible = False
                        CType(e.Item.FindControl("tr_bid_now"), HtmlTableRow).Visible = False
                        CType(e.Item.FindControl("img_bid_now"), ImageButton).ImageUrl = "/images/fend/7.png"

                        Dim rem_qty As Integer = 0
                        If .Item("qty_per_bidder") > 0 Then
                            Dim already_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and auction_id=" & auction_id & "")
                            rem_qty = .Item("qty_per_bidder") - already_buy
                        End If
                        Dim total_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and auction_id=" & auction_id & "")
                        If .Item("total_qty") - total_buy < rem_qty Then
                            rem_qty = .Item("total_qty") - total_buy
                        End If

                        'Response.Write(rem_qty)
                        If rem_qty > 0 Then
                            For i = 1 To rem_qty
                                CType(e.Item.FindControl("ddl_qty_confirm"), DropDownList).Items.Insert(i - 1, New ListItem(i, i))

                            Next

                            CType(e.Item.FindControl("pnl_buy_now_confirm"), Panel).Visible = True
                            CType(e.Item.FindControl("lit_buy_now_amount_confirm"), Literal).Text = FormatCurrency(.Item("show_price"), 2)
                            CType(e.Item.FindControl("hid_buy_now_confirm_amount"), HiddenField).Value = .Item("show_price")
                        Else
                            CType(e.Item.FindControl("pnl_buy_now_confirm"), Panel).Visible = False
                            CType(e.Item.FindControl("img_bid_now"), ImageButton).Visible = False
                        End If

                        CType(e.Item.FindControl("img_bid_now"), ImageButton).Attributes.Add("onclick", "return redirectConfirm('" & CType(e.Item.FindControl("ddl_qty_confirm"), DropDownList).ClientID & "'," & .Item("show_price") & "," & auction_id & ",'buy now')")
                        If Convert.ToBoolean(.Item("is_private_offer")) Then
                            CType(e.Item.FindControl("lit_offer"), Literal).Text = "<a href=""javascript:void(0)"" onclick=""return openOffer('" & auction_id & "','2','" & .Item("auction_type_id") & "','" & .Item("show_price") & "');""><img src='/Images/fend/4.png' alt='Private Offer' border='0'></a>"
                        End If
                        If Convert.ToBoolean(.Item("is_partial_offer")) Then
                            CType(e.Item.FindControl("lit_offer"), Literal).Text = "<a href=""javascript:void(0)"" onclick=""return openOffer('" & auction_id & "','3','" & .Item("auction_type_id") & "','" & .Item("show_price") & "');""><img src='/Images/fend/4.png' alt='Partial Offer' border='0'></a>"
                        End If

                    ElseIf .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then
                        Dim img_amt_help As Image = CType(e.Item.FindControl("img_amt_help"), Image)
                        Dim refrid As String = CType(e.Item.FindControl("btn_refresh"), Button).ClientID
                        Dim txtid As String = CType(e.Item.FindControl("txt_bid_now"), TextBox).ClientID
                        Dim _qid As String = CType(e.Item.FindControl("rbt_fedex_options"), RadioButtonList).ClientID
                        Dim cmpVal As String = bidder_max_amount

                        Dim divid As String = CType(e.Item.FindControl("div_Validator"), System.Web.UI.HtmlControls.HtmlGenericControl).ClientID
                        CType(e.Item.FindControl("img_bid_now"), ImageButton).ImageUrl = "/images/fend/6.png"
                        CType(e.Item.FindControl("txt_bid_now"), TextBox).Visible = True
                        CType(e.Item.FindControl("img_bid_now"), ImageButton).Attributes.Add("onclick", "return validBid('" & auction_id & "','" & txtid & "','" & cmpVal & "','" & divid & "','0','" & .Item("auction_type_id") & "','" & refrid & "','" & CType(e.Item.FindControl("hid_shipping_option"), HiddenField).ClientID & "','" & CType(e.Item.FindControl("hid_shipping_value"), HiddenField).ClientID & "','" & img_amt_help.ClientID & "')")

                        If bid_amount < .Item("thresh_hold_value") Or .Item("thresh_hold_value") = 0 Then ' CHANGE BY SANDEEP (NEED TO DISCUSS)

                            'And buy_it_now_bid < .Item("no_of_clicks") And bid_amount < .Item("buy_now_price")
                            If Convert.ToBoolean(.Item("is_buy_it_now")) Then

                                Dim rem_qty As Integer = 0
                                If .Item("qty_per_bidder") > 0 Then
                                    Dim already_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and auction_id=" & auction_id & "")
                                    rem_qty = .Item("qty_per_bidder") - already_buy
                                End If

                                Dim total_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and auction_id=" & auction_id & "")
                                If .Item("total_qty") - total_buy < rem_qty Then
                                    rem_qty = .Item("total_qty") - total_buy
                                End If


                                If rem_qty > 0 Then
                                    For i = 1 To rem_qty
                                        CType(e.Item.FindControl("ddl_qty"), DropDownList).Items.Insert(i - 1, New ListItem(i, i))
                                    Next
                                    CType(e.Item.FindControl("pnl_buy_now"), Panel).Visible = True
                                    CType(e.Item.FindControl("lit_buy_now_amount"), Literal).Text = FormatCurrency(.Item("buy_now_price"), 2)
                                    CType(e.Item.FindControl("hid_buy_now_amount"), HiddenField).Value = .Item("buy_now_price")
                                Else
                                    CType(e.Item.FindControl("pnl_buy_now"), Panel).Visible = False
                                End If

                                'vish
                                'CType(e.Item.FindControl("lit_offer"), Literal).Text = "<a href=""javascript:void(0)"" onclick=""return openOffer('" & auction_id & "','1','" & .Item("auction_type_id") & "','" & .Item("buy_now_price") & "');""><img src='/Images/fend/4.png' alt='Buy it Now' border='0'></a>"
                            End If

                            If Convert.ToBoolean(.Item("is_private_offer")) Then
                                CType(e.Item.FindControl("lit_offer"), Literal).Text = "<a href=""javascript:void(0)"" onclick=""return openOffer('" & auction_id & "','2','" & .Item("auction_type_id") & "','" & .Item("buy_now_price") & "');""><img src='/Images/fend/4.png' alt='Private Offer' border='0'></a>"
                            End If

                            If Convert.ToBoolean(.Item("is_partial_offer")) Then
                                CType(e.Item.FindControl("lit_offer"), Literal).Text = "<a href=""javascript:void(0)"" onclick=""return openOffer('" & auction_id & "','3','" & .Item("auction_type_id") & "','" & .Item("buy_now_price") & "');""><img src='/Images/fend/4.png' alt='Partial Offer' border='0'></a>"
                            End If

                        End If

                    Else
                        CType(e.Item.FindControl("img_bid_now"), ImageButton).Attributes.Add("onclick", "return openOffer('" & auction_id & "','0','" & .Item("auction_type_id") & "','0')")
                        CType(e.Item.FindControl("img_bid_now"), ImageButton).ImageUrl = "/images/fend/8.png"
                        CType(e.Item.FindControl("img_bid_now"), ImageButton).AlternateText = "Offer"
                    End If
                End If

                CType(e.Item.FindControl("iframe_price"), System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("src", "/timerframe.aspx?is_frontend=1&i=" & auction_id)

            End With
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select product_item_attachment_id, filename from tbl_auction_product_item_attachments where auction_id=" & auction_id & " order by product_item_attachment_id desc")
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("filename") <> "" Then
                    CType(e.Item.FindControl("lit_item_attachment"), Literal).Text = "<a href='/upload/Auctions/product_items/" & auction_id & "/" & dt.Rows(0).Item("product_item_attachment_id") & "/" & dt.Rows(0).Item("filename") & "' target='_blank'>" & dt.Rows(0).Item("filename") & "</a>"
                End If
            End If
            dt.Dispose()

        Else
            e.Item.Visible = False
        End If
        dtTable = Nothing
    End Sub
    Protected Sub rpt_auctions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_auctions.ItemDataBound
        setAuction(e, CType(e.Item.FindControl("hid_auction_id"), HiddenField).Value, False)
        setAuctiondetails(e, CType(e.Item.FindControl("hid_auction_id"), HiddenField).Value)
        load_auction_bidder_queries(e, CType(e.Item.FindControl("hid_auction_id"), HiddenField).Value)
        Dim ddl As DropDownList = e.Item.FindControl("ddl_qty")
        ddl.AutoPostBack = True

    End Sub
    Public Sub ddl_qty_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        For i = 0 To rpt_auctions.Items.Count - 1
            Dim ddl_qty As DropDownList = CType(rpt_auctions.Items(i).FindControl("ddl_qty"), DropDownList)
            Dim buy_now_amount As Literal = CType(rpt_auctions.Items(i).FindControl("lit_buy_now_amount"), Literal)
            Dim hid_buy_now_amount As HiddenField = CType(rpt_auctions.Items(i).FindControl("hid_buy_now_amount"), HiddenField)
            Dim buy_amount As String = IIf(hid_buy_now_amount.Value = 0, "", hid_buy_now_amount.Value)
            Dim amount As Double = 0
            'Response.Write(buy_amount & "--" & ddl_qty.SelectedValue & "</br>")
            If Not String.IsNullOrEmpty(buy_amount) Then
                If Not String.IsNullOrEmpty(ddl_qty.SelectedValue) Then
                    amount = Convert.ToDouble(buy_amount) * ddl_qty.SelectedValue
                    buy_now_amount.Text = FormatCurrency(amount, 2)
                    'Response.Write(buy_amount & "--" & ddl_qty.SelectedValue & "</br>")
                End If
            End If

        Next
    End Sub
    Public Sub ddl_qty_confirm_SelectedIndexChanged(sender As Object, e As EventArgs)
        For i = 0 To rpt_auctions.Items.Count - 1
            Dim ddl_qty As DropDownList = CType(rpt_auctions.Items(i).FindControl("ddl_qty_confirm"), DropDownList)
            Dim buy_now_amount As Literal = CType(rpt_auctions.Items(i).FindControl("lit_buy_now_amount_confirm"), Literal)
            Dim hid_buy_now_amount As HiddenField = CType(rpt_auctions.Items(i).FindControl("hid_buy_now_confirm_amount"), HiddenField)
            Dim buy_amount As String = IIf(hid_buy_now_amount.Value = 0, "", hid_buy_now_amount.Value)
            Dim amount As Double = 0
            'Response.Write(buy_amount & "--" & ddl_qty.SelectedValue & "</br>")
            If Not String.IsNullOrEmpty(buy_amount) Then
                If Not String.IsNullOrEmpty(ddl_qty.SelectedValue) Then
                    amount = Convert.ToDouble(buy_amount) * ddl_qty.SelectedValue
                    buy_now_amount.Text = FormatCurrency(amount, 2)
                    'Response.Write(buy_amount & "--" & ddl_qty.SelectedValue & "</br>")
                End If
            End If

        Next

    End Sub
    Private Sub setAuctiondetails(ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs, ByVal auction_id As Integer)

        Dim lit_Summary As String = ""
        Dim lit_Details As String = ""
        Dim lit_terms_condition As String = ""
        Dim str As String = ""
        Dim dt As New DataTable()
        Dim i As Integer

        Dim rptItems As Repeater = CType(e.Item.FindControl("rptItems"), Repeater)
        '----------item bind start
        Dim lit_Items As String = ""


        dt = SqlHelper.ExecuteDatatable("select [product_item_id], isnull(m.[description],'') AS manufacturer, isnull([part_no],'')  AS part_no, isnull(p.[description],'') AS description, isnull(quantity,0) as quantity,ISNULL(p.name,'') AS title,ISNULL(p.upc,'') As UPC,ISNULL(p.sku,'') As SKU,ISNULL(estimated_msrp,0) AS estimated_msrp,isnull(total_msrp,0) As total_msrp from [tbl_auction_product_items] p left join tbl_master_manufacturers m on p.manufacturer_id=m.manufacturer_id where auction_id= " & auction_id)
        rptItems.DataSource = dt
        rptItems.DataBind()
        'For i = 0 To dt.Rows.Count - 1
        '    With dt.Rows(i)
        '        lit_Items = lit_Items & "<tr><td><div class='grBackDescription'><b>" & .Item("manufacturer") & "</b>&nbsp;<span class='auctionPartNo'>Part No. " & .Item("part_no") & "</span> &nbsp;<span class='auctionPartNo'>Quantity " & .Item("quantity") & "</span>" & .Item("description") & "</div></td></tr>"
        '    End With
        'Next
        dt.Dispose()
        dt = SqlHelper.ExecuteDatatable("select product_item_attachment_id, filename from tbl_auction_product_item_attachments where auction_id=" & auction_id & " order by product_item_attachment_id desc")
        If dt.Rows.Count > 0 Then
            If dt.Rows(0).Item("filename") <> "" Then
                lit_Items = lit_Items & "<tr><td><a href='/upload/Auctions/product_items/" & auction_id & "/" & dt.Rows(0).Item("product_item_attachment_id") & "/" & dt.Rows(0).Item("filename") & "' target='_blank'>" & dt.Rows(0).Item("filename") & "</a></td></tr>"
            End If
        End If
        dt.Dispose()
        'If lit_Items <> "" Then
        '    lit_Items = "<table width='100%'>" & lit_Items & "</table>"
        '    ' CType(e.Item.FindControl("lit_Items"), Literal).Text = lit_Items
        '    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(1).Visible = True
        'Else
        '    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(1).Visible = False
        'End If
        If rptItems.Items.Count > 0 Then
            'lit_Items = "<table width='100%'>" & lit_Items & "</table>"
            ' CType(e.Item.FindControl("lit_Items"), Literal).Text = lit_Items
            CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(1).Visible = True
        Else
            CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(1).Visible = False
        End If
        '----------item bind end


        Dim strQry As String = "SELECT A.auction_id,"
        strQry = strQry & "ISNULL(A.tax_details, '') AS tax_details,"
        strQry = strQry & "ISNULL(L.name, '') AS stock_location,"
        strQry = strQry & "ISNULL(A.description, '') AS description,"
        strQry = strQry & "ISNULL(A.special_conditions, '') AS special_conditions,"
        strQry = strQry & "ISNULL(A.payment_terms, '') AS payment_terms,"
        strQry = strQry & "ISNULL(A.product_details, '') AS product_details,"
        strQry = strQry & "ISNULL(A.shipping_terms, '') AS shipping_terms,"
        strQry = strQry & "ISNULL(A.other_terms, '') AS other_terms,"
        strQry = strQry & "ISNULL(A.after_launch_message, '') AS after_launch_message,"
        strQry = strQry & "ISNULL(A.start_date, '1/1/1900') AS start_date,"
        strQry = strQry & "ISNULL(A.display_end_time, '1/1/1900') AS display_end_time,"
        strQry = strQry & "ISNULL(A.is_display_start_date, 0) AS is_display_start_date,"
        strQry = strQry & "ISNULL(A.is_display_end_date, 0) AS is_display_end_date,"
        strQry = strQry & "ISNULL(B.name, '') AS stock_condition,"
        strQry = strQry & "ISNULL(L.name, '') AS stock_location,"
        strQry = strQry & "ISNULL(C.name, '') AS packaging,"
        strQry = strQry & "ISNULL(D.company_name, '') AS seller,"
        strQry = strQry & "ISNULL(A.[after_launch_filename],'') as [after_launch_filename]"
        strQry = strQry & " FROM "
        strQry = strQry & "tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations L on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B on A.stock_condition_id=B.stock_condition_id "
        strQry = strQry & "Left Join tbl_master_packaging C on A.packaging_id=C.packaging_id left join tbl_reg_sellers D on A.seller_id=D.seller_id"
        strQry = strQry & " WHERE "
        strQry = strQry & "A.auction_id =" & auction_id

        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDatatable(strQry)
        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)

                '--------- Summary Bind start
                If Convert.ToBoolean(.Item("is_display_start_date")) Or Convert.ToBoolean(.Item("is_display_end_date")) Then

                    lit_Summary = "<div style='padding: 10px;'>"

                    If Convert.ToBoolean(.Item("is_display_start_date")) Then
                        Dim dateTimeInfo As DateTime = .Item("start_date")
                        lit_Summary = lit_Summary & "Bid Start Date : <b>" & dateTimeInfo.ToString("dddd, dd MMMM yyyy HH:mm") & "</b>"
                    End If

                    If Convert.ToBoolean(.Item("is_display_end_date")) Then
                        If Convert.ToBoolean(.Item("is_display_start_date")) Then
                            lit_Summary = lit_Summary & "<br>"
                        End If

                        Dim dateTimeInfo As DateTime = .Item("display_end_time")
                        lit_Summary = lit_Summary & "Bid End Date : <b>" & dateTimeInfo.ToString("dddd, dd MMMM yyyy HH:mm") & "</b>"
                    End If

                    lit_Summary = lit_Summary & "</div>"
                End If


                If .Item("after_launch_message") <> "" Then
                    str = .Item("after_launch_message")
                End If

                If .Item("after_launch_filename").ToString <> "" Then
                    If str <> "" Then str = str & "<br>"
                    str = str & "<a href='/upload/Auctions/after_launch/" & auction_id & "/" & .Item("after_launch_filename").ToString & "' target='_blank'>" & .Item("after_launch_filename").ToString & "</a>"
                End If
                If str <> "" Then
                    lit_Summary = lit_Summary & "<div style='padding: 10px;'>" & str & "</div>"
                End If


                Dim tbl_build As New StringBuilder
                tbl_build.Append("<table width='90%'><tr><td style='width: 150px; padding-left:10px;'>" & IIf(.Item("stock_condition").ToString.Trim <> "", "<b>Stock Condition</b></td><td width='200px'>" & .Item("stock_condition").ToString & "</td>", "&nbsp;</td>"))
                tbl_build.Append("<td style='width: 150px; padding-left:10px;'>" & IIf(.Item("stock_location").ToString.Trim <> "", "<b>Stock Location</b></td><td width='200px'>" & .Item("stock_location").ToString & "</td></tr>", "&nbsp;</td></tr>"))
                tbl_build.Append("<tr><td style='width: 150px; padding-left:10px;'>" & IIf(.Item("packaging").ToString.Trim <> "", "<b>Packaging</b></td><td>" & .Item("packaging").ToString & "</td>", "</td>"))
                tbl_build.Append("<td style='width: 150px; padding-left:10px;'>" & IIf(.Item("seller").ToString.Trim <> "", "<b>Company</b></td><td>" & .Item("seller").ToString & "</td></tr></table>", "&nbsp;</td></tr></table>"))
                lit_Summary = lit_Summary & tbl_build.ToString

                dt = SqlHelper.ExecuteDatatable("SELECT attachment_id,auction_id,title,[filename] FROM tbl_auction_attachments WHERE for_bidder=1 and auction_id = " & auction_id & " order by upload_date desc")
                If dt.Rows.Count > 0 Then
                    lit_Summary = lit_Summary & "<div class='detailsSubHeading' style='padding-left: 10px;'>Attachments</div>"
                    lit_Summary = lit_Summary & "<div style='padding: 0px 10px 10px 10px;'>"
                    For i = 0 To dt.Rows.Count - 1
                        With dt.Rows(i)
                            lit_Summary = lit_Summary & "<a href='/Upload/Auctions/auction_attachments/" & auction_id & "/" & .Item("attachment_id") & "/" & .Item("filename") & "' target='_blank'>" & .Item("filename") & "</a>"
                        End With
                    Next
                    lit_Summary = lit_Summary & "</div>"
                End If
                dt.Dispose()
                If lit_Summary <> "" Then
                    CType(e.Item.FindControl("lit_Summary"), Literal).Text = lit_Summary
                    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(0).Visible = True
                Else
                    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(0).Visible = False
                End If


                '--------- Summary Bind end
                '----------Details Tab Start
                If .Item("product_details") <> "" Then
                    lit_Details = "<div class='detailsSubHeading' style='font-weight:bold;padding:10px 10px 0px 10px;'>Product Detail</div>"
                    lit_Details = lit_Details & "<div style='padding: 0px 10px 0 10px;'>" & .Item("product_details") & "</div>"
                End If
                dt = SqlHelper.ExecuteDatatable("select product_attachment_id ,filename from tbl_auction_product_attachments where auction_id=" & auction_id & " order by product_attachment_id desc")
                If dt.Rows.Count > 0 Then
                    'If lit_Details <> "" Then lit_Details = lit_Details & "<br>"
                    lit_Details = lit_Details & "<div style='padding: 10px 10px 0px 10px; text-align:right;'><a target='_blank' href='/Upload/Auctions/product_attachments/" & auction_id & "/" & dt.Rows(0).Item("product_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a></div>"
                End If
                dt.Dispose()

                If .Item("description") <> "" Then
                    lit_Details = lit_Details & "<div class='detailsSubHeading' style='font-weight:bold;padding:10px 10px 0px 10px;'>Description</div>"
                    lit_Details = lit_Details & "<div style='padding: 0px 10px 10px 10px;'>" & .Item("description") & "</div>"
                End If
                If lit_Details <> "" Then
                    CType(e.Item.FindControl("lit_Details"), Literal).Text = lit_Details
                    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(2).Visible = True
                Else
                    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(2).Visible = False
                End If

                '----------Details Tab end

                '----------Terms & Condition Start
                If .Item("payment_terms") <> "" Then
                    lit_terms_condition = "<div class='detailsSubHeading' style='font-weight:bold; padding:10px 10px 0px 10px;'>Payment Terms</div>"
                    lit_terms_condition = lit_terms_condition & "<div style='padding: 0px 10px 0 10px;'>" & .Item("payment_terms") & "</div>"
                End If
                dt = SqlHelper.ExecuteDatatable("select payment_term_attachment_id ,filename from tbl_auction_payment_term_attachments where auction_id=" & auction_id & " order by payment_term_attachment_id desc")
                If dt.Rows.Count > 0 Then
                    'If lit_terms_condition <> "" Then lit_terms_condition = lit_terms_condition & "<br>"
                    lit_terms_condition = lit_terms_condition & "<div style='padding: 10px 10px 0px 10px; text-align:right;'><a target='_blank' href='/Upload/Auctions/payment_term_attachments/" & auction_id & "/" & dt.Rows(0).Item("payment_term_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a></div>"
                End If
                dt.Dispose()
                'If lit_terms_condition <> "" Then
                '    lit_terms_condition = "<div style='padding: 10px;'>" & lit_terms_condition & "</div>"
                'End If


                If .Item("shipping_terms") <> "" Then
                    str = "<div class='detailsSubHeading' style='font-weight:bold; padding:10px 10px 0px 10px;'>Shipping Terms</div>"
                    str = str & "<div style='padding: 0px 10px 0 10px;'>" & .Item("shipping_terms") & "</div>"

                End If
                dt = SqlHelper.ExecuteDatatable("select top 1 shipping_term_attachment_id,filename from tbl_auction_shipping_term_attachments where auction_id=" & auction_id & " order by shipping_term_attachment_id desc")
                If dt.Rows.Count > 0 Then
                    'If str <> "" Then str = str & "<br>"
                    str = str & "<div style='padding: 10px 10px 0px 10px; text-align:right;'><a target='_blank' href='/Upload/Auctions/shipping_term_attachments/" & auction_id & "/" & dt.Rows(0).Item("shipping_term_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a></div>"

                End If
                dt.Dispose()

                If str <> "" Then
                    'str = "<div style='padding: 10px;'>" & str & "</div>"
                    lit_terms_condition = lit_terms_condition & str
                    str = ""
                End If


                If .Item("special_conditions") <> "" Then
                    str = "<div class='detailsSubHeading' style='font-weight:bold; padding:10px 10px 0px 10px;'>Special Conditions</div>"
                    str = str & "<div style='padding: 0px 10px 0 10px;'>" & .Item("special_conditions") & "</div>"
                    'str = "<div class='detailsSubHeading' style='padding-bottom:10px;'><b>Special Conditions</b></div>" & .Item("special_conditions")
                End If
                dt = SqlHelper.ExecuteDatatable("select top 1 special_condition_id,filename from tbl_auction_special_conditions where auction_id=" & auction_id & " order by special_condition_id desc")
                If dt.Rows.Count > 0 Then
                    'If str <> "" Then str = str & "<br>"
                    str = str & "<div style='padding: 10px 10px 0px 10px; text-align:right;'><a target='_blank' href='/Upload/Auctions/special_conditions/" & auction_id & "/" & dt.Rows(0).Item("special_condition_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a></div>"
                End If
                dt.Dispose()

                If str <> "" Then
                    'str = "<div style='padding: 10px;'>" & str & "</div>"
                    lit_terms_condition = lit_terms_condition & str
                    str = ""
                End If

                If .Item("tax_details") <> "" Then
                    str = "<div class='detailsSubHeading' style='font-weight:bold; padding:10px 10px 0px 10px;'>Tax Details</div>"
                    str = str & "<div style='padding: 0px 10px 0 10px;'>" & .Item("tax_details") & "</div>"
                    ' str = "<div class='detailsSubHeading' style='padding-bottom:10px;'><b>Tax Details</b></div>" & .Item("tax_details")
                End If
                dt = SqlHelper.ExecuteDatatable("select top 1 tax_attachment_id,filename from tbl_auction_tax_attachments where auction_id=" & auction_id & " order by tax_attachment_id desc")
                If dt.Rows.Count > 0 Then
                    'If str <> "" Then str = str & "<br>"
                    str = str & "<div style='padding: 10px 10px 0px 10px; text-align:right;'><a target='_blank' href='/Upload/Auctions/tax_attachments/" & auction_id & "/" & dt.Rows(0).Item("tax_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a></div>"
                End If
                dt.Dispose()

                If str <> "" Then
                    'str = "<div style='padding: 10px;'>" & str & "</div>"
                    lit_terms_condition = lit_terms_condition & str
                    str = ""
                End If


                If .Item("other_terms") <> "" Then
                    str = "<div class='detailsSubHeading' style='font-weight:bold; padding:10px 10px 0px 10px;'>Other Terms</div>"
                    str = str & "<div style='padding: 0px 10px 0 10px;'>" & .Item("other_terms") & "</div>"
                    'str = "<div class='detailsSubHeading' style='padding-bottom:10px;'><b>Other Terms</b></div>" & .Item("other_terms")
                End If
                dt = SqlHelper.ExecuteDatatable("select top 1 term_cond_attachment_id,filename from tbl_auction_terms_cond_attachments where auction_id=" & auction_id & " order by term_cond_attachment_id desc")
                If dt.Rows.Count > 0 Then
                    'If str <> "" Then str = str & "<br>"
                    str = str & "<div style='padding: 10px 10px 0px 10px; text-align:right;'><a target='_blank' href='/Upload/Auctions/terms_cond_attachments/" & auction_id & "/" & dt.Rows(0).Item("term_cond_attachment_id") & "/" & dt.Rows(0).Item("filename") & "'>" & dt.Rows(0).Item("filename") & "</a></div>"
                End If
                dt.Dispose()

                If str <> "" Then
                    'str = "<div style='padding: 10px;'>" & str & "</div>"
                    lit_terms_condition = lit_terms_condition & str
                    str = ""
                End If


                If lit_Details <> "" Then
                    CType(e.Item.FindControl("lit_terms_condition"), Literal).Text = lit_terms_condition
                    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(3).Visible = True
                Else
                    CType(e.Item.FindControl("TabStip1"), RadTabStrip).Tabs(3).Visible = False
                End If

                '----------Terms & Condition Start

            End With
        Else
            e.Item.Visible = False
        End If
        dtTable = Nothing

    End Sub
    Protected Sub load_auction_bidder_queries(ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs, ByVal auction_id As Integer)
        Dim dt As New DataTable
        dt = SqlHelper.ExecuteDatatable("select A.query_id,A.title,A.submit_date,A.buyer_id,B.company_name,isnull(case when B.state_id<>0 then (select name from tbl_master_states where state_id=B.state_id) else B.state_text end,'') as state from tbl_auction_queries A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id inner join tbl_auctions A1 WITH (NOLOCK) on A.auction_id=A1.auction_id where A.parent_query_id=0 and A.is_active=1 and A.auction_id=" & auction_id & " and A.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by submit_date desc")
        CType(e.Item.FindControl("rfv_create_thread"), RequiredFieldValidator).ValidationGroup = "create_" & CType(e.Item.FindControl("img_btn_create_thread"), ImageButton).CommandArgument & "_thread"
        CType(e.Item.FindControl("img_btn_create_thread"), ImageButton).ValidationGroup = "create_" & CType(e.Item.FindControl("img_btn_create_thread"), ImageButton).CommandArgument & "_thread"
        CType(e.Item.FindControl("img_btn_create_thread"), ImageButton).OnClientClick = "question_wait('" & CType(e.Item.FindControl("img_btn_create_thread"), ImageButton).ValidationGroup & "','" & CType(e.Item.FindControl("img_btn_create_thread"), ImageButton).CommandArgument & "');"
        CType(e.Item.FindControl("rep_after_queries"), Repeater).DataSource = dt
        CType(e.Item.FindControl("rep_after_queries"), Repeater).DataBind()
        If dt.Rows.Count = 0 Then
            CType(e.Item.FindControl("lit_your_query"), Literal).Visible = False
        End If
        dt.Dispose()
    End Sub

End Class
