﻿
Partial Class Unsubscription
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Show_Message()
        End If
    End Sub
    Private Sub Show_Message()
        If Not String.IsNullOrEmpty(Request.QueryString("type")) And Not String.IsNullOrEmpty(Request.QueryString("id")) Then
            If Request.QueryString("type") = 1 Then
                Dim buyer_id As Integer = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("id"))
                Dim strQuery As String = "Declare @email varchar(50) set @email=(select email from tbl_reg_buyers WITH (NOLOCK) where buyer_id=" & buyer_id & ") if not exists(select unsubscription_id from tbl_unsubscription where email=@email)Begin Insert into tbl_unsubscription(email,type)Values(@email,1) select 1 End else Begin select 0 End"
                Dim status As Integer = SqlHelper.ExecuteScalar(strQuery)
                Dim str As String = ""
                If status = 1 Then
                    str = "You have been successfully unsubscribed for auction invitaions. From now you won't get invitaion from PCS Bidding.<br /><b>If you want to subscribe again for the auction invitaion please subscribe from My Account section or contact PCS administrator."
                Else
                    str = "You are already unsubscribed for auction invitaions.<br /><b>If you want to subscribe again for the auction invitaion please subscribe from My Account section or contact PCS administrator."
                End If
                lbl_message.Text = str
            Else
                lbl_message.Text = "Invalid Request"
            End If
        Else
            lbl_message.Text = "Invalid Request"
        End If

    End Sub
End Class
