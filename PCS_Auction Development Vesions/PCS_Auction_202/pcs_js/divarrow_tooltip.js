﻿

     var offsetfromcursorX_new = 12; //Customize x offset of tooltip
     var offsetfromcursorY_new = 10; //Customize y offset of tooltip

     var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
     var offsetdivfrompointerY = 14; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

     //document.write('<div id="dhtmltooltip_new">tooltip</div>'); write out tooltip DIV

     //write out divs to hold pointers
     document.write('<DIV id="upper_left_arrow_new">upperleft_new</DIV>');
     document.write('<DIV id="upper_right_arrow_new">upperright_new</DIV>');
     document.write('<DIV id="lower_left_arrow_new">lowerleft_new</DIV>');
     document.write('<DIV id="lower_right_arrow_new">lowerright_new</DIV>');
     //getting ie_new and ns6_new variables
     var ie_new = document.all;
     var ns6_new = document.getElementById && !document.all;

     //initially don't want to show
     var enabletip_new = false;
     if (ie_new || ns6_new) {
         var tipobj_new = document.all ? document.all["dhtmltooltip_new"] : document.getElementById ? document.getElementById("dhtmltooltip_new") : "";
     }
     var pointerobj_new;

     function ietruebody_new() {
         return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
     }
     //function that actually enables the tooltip
     function tip_new(thetext, thewidth, thecolor,_option) {
         if (ns6_new || ie_new) {
             if (typeof thewidth != "undefined") {
                 tipobj_new.style.width = thewidth + "px";
             }
             if (typeof thecolor != "undefined" && thecolor != "") {
                 tipobj_new.style.backgroundColor = thecolor;
             }
             if (_option) {
                 tipobj_new.innerHTML = "<iframe src='" + thetext + "' scrolling='no' frameborder='0' height='185' width='" + thewidth + "'></iframe>";
             }
             else { tipobj_new.innerHTML =  thetext ; }
             
             enabletip_new = true;
             return false;
         }
     }
     //event called on mouse movement
     function positiontip_new(e) {

         //if tip is enabled
         if (enabletip_new) {

             //hide old arrow
             pointerobj_new.style.visibility = "hidden";

             //get current position
             var curX = (ns6_new) ? e.pageX : event.clientX + ietruebody_new().scrollLeft;
             var curY = (ns6_new) ? e.pageY : event.clientY + ietruebody_new().scrollTop;

             //Find out how close the mouse is to the corner of the window
             var winwidth = ie_new && !window.opera ? ietruebody_new().clientWidth : window.innerWidth - 20;
             var winheight = ie_new && !window.opera ? ietruebody_new().clientHeight : window.innerHeight - 20;

             //get edge information
             var rightedge = ie_new && !window.opera ? winwidth - event.clientX - offsetfromcursorX_new : winwidth - e.clientX - offsetfromcursorX_new;
             var bottomedge = ie_new && !window.opera ? winheight - event.clientY - offsetfromcursorY_new : winheight - e.clientY - offsetfromcursorY_new;
             var leftedge = (offsetfromcursorX_new < 0) ? offsetfromcursorX_new * (-1) : -1000;

             //variable to see if right edge has been reached
             var right = false;

             //if the horizontal distance isn't enough to accomodate the width of the context menu
             if (rightedge < tipobj_new.offsetWidth) {

                 //move the horizontal position of the menu to the left by it's width
                 tipobj_new.style.left = curX - tipobj_new.offsetWidth + "px";
                 right = true;

                 //place on right side of tip object
                 pointerobj_new.style.left = curX - offsetfromcursorX_new - (2 * offsetdivfrompointerX) + "px";

             }
             else if (curX < leftedge) {
                 tipobj_new.style.left = "5px";
             }
             else {
                 //position the horizontal position of the menu where the mouse is positioned
                 tipobj_new.style.left = curX + offsetfromcursorX_new - offsetdivfrompointerX + "px";

                 //place on left side of tip object
                 pointerobj_new.style.left = curX + offsetfromcursorX_new + "px";
             }

             //if bottom needs to be displayed
             if (bottomedge < tipobj_new.offsetHeight) {

                 //initial height for tip object
                 tipobj_new.style.top = curY - tipobj_new.offsetHeight - offsetfromcursorY_new + "px";

                 //if right display lower right, otherwise display left
                 if (right) {
                     pointerobj_new = document.all ? document.all["lower_right_arrow_new"] : document.getElementById ? document.getElementById("lower_right_arrow_new") : "";
                 }
                 else {
                     pointerobj_new = document.all ? document.all["lower_left_arrow_new"] : document.getElementById ? document.getElementById("lower_left_arrow_new") : "";
                 }

                 //account for shadow if IE
                 if (ie_new) {
                     //comment out if not using shadow in IE
                     pointerobj_new.style.top = curY - offsetdivfrompointerY - 5 + "px";
                 }
                 //account for 3 pixels lost by drawing tip for FF
                 else {
                     pointerobj_new.style.top = curY - offsetdivfrompointerY + 3 + "px";
                 }
             }

             //otherwise display upper ones
             else {

                 //initial height for tip object
                 tipobj_new.style.top = curY + offsetfromcursorY_new + offsetdivfrompointerY + "px";

                 //if right, display upper right, if not, display left
                 if (right) {
                     pointerobj_new = document.all ? document.all["upper_right_arrow_new"] : document.getElementById ? document.getElementById("upper_right_arrow_new") : "";
                 }
                 else {
                     pointerobj_new = document.all ? document.all["upper_left_arrow_new"] : document.getElementById ? document.getElementById("upper_left_arrow_new") : "";
                 }

                 //upper position of pointer object
                 pointerobj_new.style.top = curY + offsetfromcursorY_new + "px";

             }

             //match background color of pointer to div
             var kids_new = pointerobj_new.childNodes;
             for (var i = 0; i < kids_new.length; i++) {

                 //if ie_new style...
                 if (tipobj_new.currentStyle) {
                     kids_new[i].style.backgroundColor = tipobj_new.currentStyle['backgroundColor'];
                 }
                 //otherwise mozilla?
                 else if (window.getComputedStyle) {
                     if (kids_new[i].className != 'arrow_tip') {
                         kids_new[i].style.backgroundColor = document.defaultView.getComputedStyle(tipobj_new, null).getPropertyValue('background-color');
                     }
                 }
                 else {
                     //other style...
                 }
             }

             //set all to visible
             tipobj_new.style.visibility = "visible";
             pointerobj_new.style.visibility = "visible";
         }
     }

     //hides tooltip
     function hide_tip_new() {
         if (ns6_new || ie_new) {

             //disable tip
             enabletip_new = false;

             //hide all objects
             tipobj_new.style.visibility = "hidden";
             pointerobj_new.style.visibility = "hidden";
             tipobj_new.style.left = "-1000px";
         }
     }
     //function initially draws arrows using only CSS
     function draw_arrows_new() {
         var div = document.getElementById('upper_left_arrow_new');
         var h = '';
         var end = 15;

         //upper left pointer 
         if (!ie_new) {
             //initial tip for FF aarow
             h += "<DIV class='arrow_tip' style='width: 1px;'></DIV>";
             h += "<DIV class='arrow_tip' style='width: 2px;'></DIV>";
             h += "<DIV class='arrow_tip' style='width: 3px;'></DIV>";
             end = 12;
         }

         //printing out main triangle
         for (var i = 0; i < end; i++) {
             h += "<DIV class='arrow' style='width: " + (i) + "px;'>";
             h += "</DIV>";
         }
         div.innerHTML = h;


         //lower left pointer
         div = document.getElementById('lower_left_arrow_new');
         h = '';

         //main triangle
         for (var i = end; i > 0; i--) {
             h += "<DIV class='arrow' style='width: " + (i) + "px;'>";
             h += "</DIV>";
         }

         if (!ie_new) {
             //tip for FF arrow
             h += "<DIV class='arrow_tip' style='width: 3px;'></DIV>";
             h += "<DIV class='arrow_tip' style='width: 2px;'></DIV>";
             h += "<DIV class='arrow_tip' style='width: 1px;'></DIV>";
         }
         div.innerHTML = h;


         //lower right pointer
         div = document.getElementById('lower_right_arrow_new');
         h = '';

         //main triangle
         for (var i = end; i > 0; i--) {
             h += "<DIV class='arrow' style='border-left-width: 2px; border-right-width: 1px; width: " + (i) + "px;'>";
             h += "</DIV>";
         }
         if (!ie_new) {
             //tip for FF arrow
             h += "<DIV class='arrow_tip' style='width: 3px;'></DIV>";
             h += "<DIV class='arrow_tip' style='width: 2px;'></DIV>";
             h += "<DIV class='arrow_tip' style='width: 1px;'></DIV>";
         }
         div.innerHTML = h;


         //upper right pointer
         div = document.getElementById('upper_right_arrow_new');
         h = '';
         if (!ie_new) {
             //tip for FF arrow
             h += "<DIV class='arrow_tip' style='width: 1px;'></DIV>";
             h += "<DIV class='arrow_tip' style='width: 2px;'></DIV>";
             h += "<DIV class='arrow_tip' style='width: 3px;'></DIV>";
         }

         //main triangle
         for (var i = 0; i < end; i++) {
             h += "<DIV class='arrow' style='border-left-width: 2px; border-right-width: 1px; width: " + (i) + "px;'>";
             h += "</DIV>";
         }
         div.innerHTML = h;


         //initially select upper left
         pointerobj_new = document.all ? document.all["upper_left_arrow_new"] : document.getElementById ? document.getElementById("upper_left_arrow_new") : ""
     }
     //initially draw out arrows
     draw_arrows_new();

     //set onmousemove event to position tip
     document.onmousemove = positiontip_new;
 