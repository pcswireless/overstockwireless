﻿
Partial Class change_password
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Write(Security.EncryptionDecryption.DecryptValue("HwuoL9EAnv29hCDfS2KSag=="))
        If CommonCode.Fetch_Cookie_Shared("user_id") = "" Then
            Response.Write("<script>window.close();</script>")
            Response.End()
        End If
        If Page.IsPostBack = False Then
            'Dim _upw As String = String.Empty
            'If Request.QueryString.Get("b") = "0" Then
            '    _upw = SqlHelper.ExecuteScalar("select isnull(password,'') from tbl_reg_buyer_users where buyer_user_id=" & Request.QueryString.Get("i") & "")
            'ElseIf Request.QueryString.Get("b") = "1" Then
            '    _upw = SqlHelper.ExecuteScalar("select isnull(password,'')  from tbl_sec_users where user_id=" & Request.QueryString.Get("i") & "")
            'End If

            If Request.QueryString.Get("o") = 0 Then
                tr_old_pwd.Visible = False
            Else
                tr_old_pwd.Visible = True
                ' CompareValidator1.ValueToCompare = Security.EncryptionDecryption.DecryptValue(_upw)

            End If
        End If

    End Sub
    'Protected Sub imgbtn_update_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_update.Click
    '    If Page.IsValid Then
    '        Dim qry As String = ""
    '        If Request.QueryString.Get("b") = "0" Then
    '            qry = "update tbl_reg_buyer_users set password='" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "' where buyer_user_id=" & Request.QueryString.Get("i") & ""
    '        ElseIf Request.QueryString.Get("b") = "1" Then
    '            qry = "update tbl_sec_users set password='" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "' where user_id=" & Request.QueryString.Get("i")
    '        End If

    '        SqlHelper.ExecuteNonQuery(qry)

    '        If Request.QueryString.Get("b") = "0" Then
    '            qry = "select first_name as name, email from tbl_reg_buyer_users where buyer_user_id=" & Request.QueryString.Get("i") & ""
    '        ElseIf Request.QueryString.Get("b") = "1" Then
    '            qry = "select first_name as name, email from tbl_sec_users where user_id=" & Request.QueryString.Get("i")
    '        End If

    '        Dim dt As New DataTable()
    '        dt = SqlHelper.ExecuteDatatable(qry)

    '        If dt.Rows.Count > 0 Then
    '            Dim obj_email As New Email
    '            obj_email.Send_ChangePassword_Mail(dt.Rows(0).Item("email"), dt.Rows(0).Item("name"))
    '            obj_email = Nothing
    '            lit_message.Text = "<span  class='req_star'>Password changed successfully</span>"
    '        Else
    '            If Request.QueryString.Get("b") = "0" Then
    '                lit_message.Text = "Buyer not exist !"
    '            Else
    '                lit_message.Text = "User not exist !"
    '            End If

    '        End If

    '        dt = Nothing
    '    End If
    'End Sub

    'Protected Sub imgbtn_update_Click(sender As Object, e As System.EventArgs) Handles imgbtn_update.Click
    '    If Page.IsValid Then
    '        Dim _upw As String = String.Empty
    '        If Request.QueryString.Get("b") = "0" Then
    '            _upw = SqlHelper.ExecuteScalar("select isnull(password,'') from tbl_reg_buyer_users where buyer_user_id=" & Request.QueryString.Get("i") & "")
    '        ElseIf Request.QueryString.Get("b") = "1" Then
    '            _upw = SqlHelper.ExecuteScalar("select isnull(password,'')  from tbl_sec_users where user_id=" & Request.QueryString.Get("i") & "")
    '        End If

    '        If txt_old_password.Text.ToString() <> "" AndAlso txt_old_password.Text.ToString() = Security.EncryptionDecryption.DecryptValue(_upw) Then
    '            Dim qry As String = ""
    '            If Request.QueryString.Get("b") = "0" Then
    '                qry = "update tbl_reg_buyer_users set password='" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "' where buyer_user_id=" & Request.QueryString.Get("i") & ""
    '            ElseIf Request.QueryString.Get("b") = "1" Then
    '                qry = "update tbl_sec_users set password='" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "' where user_id=" & Request.QueryString.Get("i")
    '            End If
    '            SqlHelper.ExecuteNonQuery(qry)

    '            If Request.QueryString.Get("b") = "0" Then
    '                qry = "select first_name as name, email from tbl_reg_buyer_users where buyer_user_id=" & Request.QueryString.Get("i") & ""
    '            ElseIf Request.QueryString.Get("b") = "1" Then
    '                qry = "select first_name as name, email from tbl_sec_users where user_id=" & Request.QueryString.Get("i")
    '            End If

    '            Dim dt As New DataTable()
    '            dt = SqlHelper.ExecuteDatatable(qry)

    '            If dt.Rows.Count > 0 Then
    '                Dim obj_email As New Email
    '                obj_email.Send_ChangePassword_Mail(dt.Rows(0).Item("email"), dt.Rows(0).Item("name"))
    '                obj_email = Nothing
    '                lit_message.Text = "<span  class='req_star'>Password changed successfully</span>"
    '            Else
    '                If Request.QueryString.Get("b") = "0" Then
    '                    lit_message.Text = "Buyer not exist !"
    '                Else
    '                    lit_message.Text = "User not exist !"
    '                End If

    '            End If

    '            dt = Nothing
    '        Else
    '            lbl_error.Text = "Error Password!"

    '        End If

    '    End If
    'End Sub

    Protected Sub imgbtn_update_Click(sender As Object, e As System.EventArgs) Handles imgbtn_update.Click
        If Page.IsValid Then
            Dim strError As String = ""
            Dim _upw As String = String.Empty
            If Request.QueryString.Get("b") = "0" Then
                _upw = SqlHelper.ExecuteScalar("select isnull(password,'') from tbl_reg_buyer_users where buyer_user_id=" & Request.QueryString.Get("i") & "")
            ElseIf Request.QueryString.Get("b") = "1" Then
                _upw = SqlHelper.ExecuteScalar("select isnull(password,'')  from tbl_sec_users where user_id=" & Request.QueryString.Get("i") & "")
            End If

            Dim qry As String = ""
            If Request.QueryString.Get("b") = "0" Then
                qry = "update tbl_reg_buyer_users set password='" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "' where buyer_user_id=" & Request.QueryString.Get("i") & ""
            ElseIf Request.QueryString.Get("b") = "1" Then
                qry = "update tbl_sec_users set password='" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "' where user_id=" & Request.QueryString.Get("i")
            End If
            If Request.QueryString.Get("o") <> 0 Then
                If txt_old_password.Text.ToString() <> "" AndAlso txt_old_password.Text.ToString() = Security.EncryptionDecryption.DecryptValue(_upw) Then
                    SqlHelper.ExecuteNonQuery(qry)
                Else
                    'old password mismatch
                    strError = "<span  class='req_star'>Invalid Old Password.</span>"
                End If
            Else
                SqlHelper.ExecuteNonQuery(qry)
            End If
            If strError = "" Then
                If Request.QueryString.Get("b") = "0" Then
                    qry = "select first_name as name, email from tbl_reg_buyer_users where buyer_user_id=" & Request.QueryString.Get("i") & ""
                ElseIf Request.QueryString.Get("b") = "1" Then
                    qry = "select first_name as name, email from tbl_sec_users where user_id=" & Request.QueryString.Get("i")
                End If
                Dim dt As New DataTable()
                dt = SqlHelper.ExecuteDatatable(qry)
                If dt.Rows.Count > 0 Then
                    Dim obj_email As New Email
                    obj_email.Send_ChangePassword_Mail(dt.Rows(0).Item("email"), dt.Rows(0).Item("name"))
                    obj_email = Nothing
                    strError = "<span  class='req_star'>Password changed successfully</span>"
                    ' lit_message.Text = "<span  class='req_star'>Password changed successfully</span>"
                Else
                    If Request.QueryString.Get("b") = "0" Then
                        strError = "<span  class='req_star'>Buyer not exist !</span>"
                        lit_message.Text = "Buyer not exist !"
                    Else
                        strError = "<span  class='req_star'>User not exist !</span>"
                        '  lit_message.Text = "User not exist !"
                    End If
                End If
                dt = Nothing
            End If
            lit_message.Text = strError
          
        End If
    End Sub
 
End Class
