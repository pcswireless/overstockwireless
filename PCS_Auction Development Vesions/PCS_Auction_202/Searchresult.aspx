﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="Searchresult.aspx.vb" Inherits="Searchresult" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
<asp:HiddenField ID="hid_filter_val" Value="" runat="server" /> 
<asp:HiddenField ID="hid_user_id" runat="server" Value="0" />
    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
                else {
                    args.set_enableAjax(true);
                }
            }

        </script>
    </telerik:RadScriptBlock>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
               
                    <asp:Literal ID="page_heading" runat="server"></asp:Literal></div>
            </td>
        </tr>
        <tr>
            <td>
                <br />
               <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Vista">
                    <ClientEvents OnRequestStart="Cancel_Ajax" />
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="RadGrid_Auction">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid_Auction" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                             <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid_Auction" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>

                        <telerik:AjaxSetting AjaxControlID="RadGrid_Buyer">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid_Buyer" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid_Buyer" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>

                        <telerik:AjaxSetting AjaxControlID="RadGrid_User">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid_User" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid_User" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>

                <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" />
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
                    left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
                    <img id="Image8" src="/images/img_loading.gif" />
                </telerik:RadAjaxLoadingPanel>
                <table cellpadding="0" cellspacing="0" >
                <tr>
                <td><div style="color: #302F30; font-weight: bold;font-size:14px; padding-top: 10px;">
                <asp:Label ID="lbl_msg" EnableViewState="true" runat="server"></asp:Label></div>
                </td>
                </tr>
                <tr runat="server" id="tr_auction" visible="false">
                <td>
                <div style="color: #302F30; font-weight: bold;font-size:14px; padding-top: 10px;padding-bottom:6px;" id="Auction_Listing" runat="server" >Auction Listing</div>
             
              <telerik:RadGrid runat="server" ID="RadGrid_Auction" AllowFilteringByColumn="True"
                    AllowSorting="True" PageSize="10" ShowFooter="False" ShowGroupPanel="True" AllowPaging="False"
                    AutoGenerateColumns="false" Skin="Vista" EnableLinqExpressions="false" >
                    <PagerStyle Mode="NextPrevAndNumeric" />
                    <ExportSettings IgnorePaging="true" FileName="Auction_Export" 
                        OpenInNewWindow="true" ExportOnlyData="true" />
                        <ClientSettings AllowDragToGroup="true">
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="true" />
                    <MasterTableView DataKeyNames="auction_id" AutoGenerateColumns="false"
                        ItemStyle-Height="40" AlternatingItemStyle-Height="40" CommandItemDisplay="Top">
                        <CommandItemStyle BackColor="#d6d6d6" />
                        <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false" ShowExportToPdfButton="false"
                            ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                        <NoRecordsTemplate>
                            No Auction to display
                        </NoRecordsTemplate>
                        <SortExpressions>
                            <telerik:GridSortExpression FieldName="title" SortOrder="Ascending" />
                        </SortExpressions>
                            <Columns>
                            <telerik:GridBoundColumn DataField="code" HeaderText="Auction Code" SortExpression="code"
                                UniqueName="auction_code" AutoPostBackOnFilter="false" >
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn DataField="title" HeaderText="Title" SortExpression="title"
                                UniqueName="title" FilterControlWidth="300" GroupByExpression="title [Title] group by title">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');"  onclick="redirectIframe('/Backend_TopBar.aspx?t=<%# CommonCode.getAuctionTabPosition(Eval("auction_id")) %>&a=<%# Eval("auction_id") %>','/Backend_Leftbar.aspx?t=3&a=<%# Eval("auction_id") %>','/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %>')">
                                        <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("title") %>'></asp:Label></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn UniqueName="product_category" DataField="product_category"
                                SortExpression="product_category" HeaderText="Product Category" GroupByExpression="product_category [Product Category] group by product_category">
                                <FilterTemplate>
                                    <telerik:RadComboBox ID="RadComboBoxCategory" DataSourceID="SqlDataSource_auction_product" DataTextField="product_category"
                                        DataValueField="product_category" Height="200px" AppendDataBoundItems="true"
                                        SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("product_category").CurrentFilterValue %>'
                                        runat="server" OnClientSelectedIndexChanged="CategoryIndexChanged">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="All" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                        <script type="text/javascript">
                                            function CategoryIndexChanged(sender, args) {
                                                var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                tableView.filter("product_category", args.get_item().get_value(), "EqualTo");

                                            }
                                        </script>
                                    </telerik:RadScriptBlock>
                                </FilterTemplate>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="status" HeaderText="Status" SortExpression="status" UniqueName="status" HeaderStyle-Width="120px" 
                            DataType="System.String" ItemStyle-Width="120px" GroupByExpression="status [Status] group by status">
                                <FilterTemplate>
                                   
                                        <telerik:RadComboBox ID="RadComboBo_Chk_Status" runat="server" OnClientSelectedIndexChanged="StatusIndexChanged"
                                             Width="120px"  SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("status").CurrentFilterValue %>' >
                                           <Items>
                                           <telerik:RadComboBoxItem Text="All" />
                                            <telerik:RadComboBoxItem Text="Bid Closed" Value="Bid Closed" />
                                            <telerik:RadComboBoxItem Text="Running" Value="Running" />
                                            <telerik:RadComboBoxItem Text="Upcoming" Value="Upcoming" />
                                        </Items>
                                        </telerik:RadComboBox>
                                   
                                    <telerik:RadScriptBlock ID="RadScriptBlock_combo" runat="server">
                                         <script type="text/javascript">
                                             function StatusIndexChanged(sender, args) {
                                                 var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                 tableView.filter("status", args.get_item().get_value(), "Contains");

                                             }
                                         </script>
                                    </telerik:RadScriptBlock>
                                </FilterTemplate>
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false" ItemStyle-Width="50"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onclick="redirectIframe('/Backend_TopBar.aspx?t=<%# CommonCode.getAuctionTabPosition(Eval("auction_id")) %>&a=<%# Eval("auction_id") %>','/Backend_Leftbar.aspx?t=3&a=<%# Eval("auction_id") %>','/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %>')">
                                        <img src="/images/edit.png" style="border: none;" alt="Edit" /></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    
                </telerik:RadGrid>
             
              
                
                </td>
                </tr>
                <tr runat="server" id="tr_buyer" visible="false">
                <td>
                <div style="color: #302F30; font-weight: bold;font-size:14px; padding-top: 10px;padding-bottom:6px;" id="Bidder_Listing" runat="server">Bidder Listing</div>
                <telerik:RadGrid ID="RadGrid_Buyer" GridLines="None" runat="server" PageSize="10" AllowPaging="False"
                        AutoGenerateColumns="False" AllowMultiRowSelection="false" Skin="Vista" AllowFilteringByColumn="True"
                        AllowSorting="true" ShowGroupPanel="True" EnableLinqExpressions="false">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <ExportSettings IgnorePaging="true" FileName="Bidder_Export" Pdf-AllowAdd="true"
                            Pdf-AllowCopy="true" Pdf-AllowPrinting="true" Pdf-PaperSize="A4" Pdf-Creator="a1"
                            OpenInNewWindow="true" ExportOnlyData="true" />
                        <MasterTableView DataKeyNames="buyer_id" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                            AllowFilteringByColumn="True" AllowSorting="true" ItemStyle-Height="40" AlternatingItemStyle-Height="40"
                            CommandItemDisplay="Top">
                            <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false" ShowExportToPdfButton="false"
                                ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            <NoRecordsTemplate>
                                Bidders not available
                            </NoRecordsTemplate>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                            </SortExpressions>
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="Contact Person" DataField="name" UniqueName="name"
                                    HeaderStyle-Width="250" ItemStyle-Width="250" SortExpression="name" FilterControlWidth="120"
                                    GroupByExpression="name [Contact Person] Group By name">
                                    <ItemTemplate>
                                        <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');" onclick="redirectIframe('','/Backend_Leftbar.aspx?t=2&b=<%# Eval("buyer_id")%>','/Bidders/AddEditBuyer.aspx?i=<%# Eval("buyer_id") %>')">
                                            <%# Eval("name")%>
                                        </a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="company_name" FilterControlWidth="125" HeaderText="Company"
                                    SortExpression="company_name" UniqueName="company_name">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="email" SortExpression="email" AllowFiltering="true"
                                    HeaderText="Email Address" ItemStyle-Width="230" HeaderStyle-Width="230" FilterControlWidth="160"
                                    GroupByExpression="email [Email Address] Group By email">
                                    <ItemTemplate>
                                        <a href="mailto:<%# Eval("email")%>">
                                            <%# Eval("email")%></a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="phone1" HeaderText="Phone"
                                    SortExpression="phone1" UniqueName="phone1" HeaderStyle-Width="120" FilterControlWidth="80"
                                    ItemStyle-Width="100">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="status" DataField="status" SortExpression="status"
                                    HeaderText="Status" GroupByExpression="status [Status] group by status">
                                    <FilterTemplate>
                                        <telerik:RadComboBox ID="RadComboBoxStatus" DataSourceID="SqlDataSource_buyer_status" DataTextField="status"
                                            DataValueField="status" Height="200px" AppendDataBoundItems="true" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("status").CurrentFilterValue %>'
                                            runat="server" OnClientSelectedIndexChanged="StatusIndexChanged">
                                            <Items>
                                                <telerik:RadComboBoxItem Text="All" />
                                            </Items>
                                        </telerik:RadComboBox>
                                        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                            <script type="text/javascript">
                                                function StatusIndexChanged(sender, args) {
                                                    var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                    tableView.filter("status", args.get_item().get_value(), "Contains");

                                                }
                                            </script>
                                        </telerik:RadScriptBlock>
                                    </FilterTemplate>
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false"
                                    ItemStyle-Width="50" HeaderStyle-Width="50" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <a href="javascript:void(0);" onclick="redirectIframe('','/Backend_Leftbar.aspx?t=2&b=<%# Eval("buyer_id")%>','/Bidders/AddEditBuyer.aspx?i=<%# Eval("buyer_id") %>')">
                                            <img src="/images/edit.png" style="border: none;" alt="Edit" /></a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings AllowDragToGroup="true">
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                        <GroupingSettings ShowUnGroupButton="true" />
                    </telerik:RadGrid>
                </td>
                </tr>
                <tr runat="server" id="tr_user" visible="false">
                <td>
                <div style="color: #302F30; font-weight: bold;font-size:14px; padding-top: 10px;padding-bottom:6px;" id="User_Listing" runat="server" >User Listing</div>
                <telerik:RadGrid ID="RadGrid_User" AllowFilteringByColumn="True" AutoGenerateColumns="false"
                    AllowSorting="True" PageSize="10" ShowFooter="False" ShowGroupPanel="True" AllowPaging="False"
                    runat="server" Skin="Vista" EnableLinqExpressions="false">
                    <PagerStyle Mode="NextPrevAndNumeric" />
                    <ExportSettings IgnorePaging="true" FileName="User_Export" OpenInNewWindow="true"
                        ExportOnlyData="true" />
                    <MasterTableView DataKeyNames="user_id" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                        AllowFilteringByColumn="True" ItemStyle-Height="40" AlternatingItemStyle-Height="40"
                        CommandItemDisplay="Top">
                        <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false" ShowExportToPdfButton="false"
                            ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                        <NoRecordsTemplate>
                            No users to display
                        </NoRecordsTemplate>
                        <SortExpressions>
                            <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                        </SortExpressions>
                        
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Name" SortExpression="name" UniqueName="name" DataType="System.String"
                                AutoPostBackOnFilter="false" FilterListOptions="VaryByDataType" ShowFilterIcon="true" DataField="name" HeaderStyle-Width="150"
                                FilterControlWidth="150" GroupByExpression="name [name] Group By name">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/users/user_mouse_over.aspx?i=<%# Eval("user_id") %>','270','white','true');" onclick="redirectIframe('','','/Users/Userdetail.aspx?i=<%# Eval("user_id") %>')">
                                        <%# Eval("name")%></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="username" HeaderText="User Name" AutoPostBackOnFilter="false" DataType="System.String"
                                ShowFilterIcon="true" SortExpression="username" FilterListOptions="VaryByDataType" ItemStyle-Width="120" UniqueName="username"
                                FilterControlWidth="100">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="mobile" HeaderText="Phone 1"
                                AutoPostBackOnFilter="false" FilterListOptions="VaryByDataType" ShowFilterIcon="true" SortExpression="mobile" ItemStyle-Width="120"
                                UniqueName="mobile" FilterControlWidth="100">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="is_active" DataType="System.Boolean" SortExpression="is_active"
                                DataField="is_active" HeaderText="Is Active" ItemStyle-Width="65" GroupByExpression="is_active [is_active] Group By is_active">
                                <FilterTemplate>
                                    <telerik:RadComboBox ID="RadComboBo_Chk_Active" runat="server" OnClientSelectedIndexChanged="StatusActiveIndexChanged"
                                        Width="120px" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("is_active").CurrentFilterValue %>'>
                                        <Items>
                                            <telerik:RadComboBoxItem Text="All" Value="" />
                                            <telerik:RadComboBoxItem Text="Active" Value="True" />
                                            <telerik:RadComboBoxItem Text="Inactive" Value="False" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    <telerik:RadScriptBlock ID="RadScriptBlock_combo" runat="server">
                                        <script type="text/javascript">
                                            function StatusActiveIndexChanged(sender, args) {
                                                var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                if (args.get_item().get_value() == "") {
                                                    tableView.filter("is_active", args.get_item().get_value(), "NoFilter");
                                                }
                                                else {
                                                    tableView.filter("is_active", args.get_item().get_value(), "EqualTo");
                                                }
                                            }
                                        </script>
                                    </telerik:RadScriptBlock>
                                </FilterTemplate>
                                <ItemTemplate>
                                    <img src='<%#IIf(Eval("is_active"),"/Images/true.gif","/Images/false.gif") %>' style="border: none;"
                                        alt="" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false"
                                ItemStyle-Width="50">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Users/Userdetail.aspx?i=<%# Eval("user_id") %>')">
                                        <img src="/images/edit.png" style="border: none;" alt="Edit" /></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowDragToGroup="true">
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="true" />
                </telerik:RadGrid>
                </td>
                </tr>
                </table>
                
                
                
               <asp:SqlDataSource ID="SqlDataSource_buyer_status" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                        ProviderName="System.Data.SqlClient" SelectCommand="select status_id,status from [tbl_reg_buyser_statuses]"
                        runat="server"></asp:SqlDataSource>
                 <asp:SqlDataSource ID="SqlDataSource_auction_product" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="select DISTINCT [name] as product_category,product_catetory_id from [tbl_master_product_categories]"
                    runat="server"></asp:SqlDataSource>
               
            </td>
        </tr>
    </table>
</asp:Content>
