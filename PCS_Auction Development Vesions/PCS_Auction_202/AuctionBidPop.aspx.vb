﻿
Partial Class AuctionBidPop
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            hid_auction_id.Value = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("a"))
            ViewState("o") = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("o"))
            ViewState("t") = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("t"))

            If Request.QueryString.Get("p").Length > 0 And Request.QueryString.Get("q").Length > 0 Then
                hid_quantity.Value = Request.QueryString.Get("p") / Request.QueryString.Get("q")
            End If


            Dim title As String = SqlHelper.ExecuteScalar("select title from tbl_auctions WITH (NOLOCK) where auction_id=" & hid_auction_id.Value)
            SetAuctionHeading(title)
            btn_proxy.Visible = False
            heading.Visible = False
            ltrl_shipping_msg.Text = ""
            If (Not String.IsNullOrEmpty(Request.QueryString.Get("setProxy"))) And String.IsNullOrEmpty(Request.QueryString.Get("conf")) Then
                pnl_offer.Visible = False
                pnl_confirm.Visible = False
                pnl_send_quote.Visible = False

                If Request.QueryString.Get("setProxy") = "1" Then
                    lbl_modal_catg.Text = "Proxy Bid : "
                    ltrl_requset_msg.Text = "<p>By setting a Proxy Bid, you are authorizing the system to work as an automated 'agent' on your behalf. This 'agent' will bid the smallest amount necessary to outbid other bidders and maintain your position as high bidder, up to the maximum bid that you set.<br><br>This will enable you to save time and help ensure you get the items you want without having to constantly monitor your auctions to see if you've been outbid.</p><br><br><br>"
                    If Request.QueryString.Get("p").Length > 0 And Request.QueryString.Get("q").Length > 0 Then
                        btn_proxy.Text = "SET AT " & FormatCurrency(Request.QueryString.Get("p"), 2) & "  (" & FormatCurrency(Request.QueryString.Get("q"), 2) & "per unit)"
                    Else
                        btn_proxy.Text = "SET AT " & Request.QueryString.Get("p") & "  (" & Request.QueryString.Get("q") & "per unit)"
                    End If
                Else
                    lbl_modal_catg.Text = "Live Bid : "
                    ltrl_requset_msg.Text = "<p>This is essentially a 'Live Bid'. The bid amount you enter here will set your current bid to that amount.</p><br><br><br>"
                    If Request.QueryString.Get("p").Length > 0 And Request.QueryString.Get("q").Length > 0 Then
                        btn_proxy.Text = "Confirm : " & FormatCurrency(Request.QueryString.Get("p"), 2) & " (" & FormatCurrency(Request.QueryString.Get("q"), 2) & "per unit)"
                    Else
                        btn_proxy.Text = "Confirm : " & Request.QueryString.Get("p") & " (" & Request.QueryString.Get("q") & "per unit)"
                    End If
                End If

                btn_proxy.Visible = True
                heading.Visible = True
                btn_proxy.Attributes.Add("onclick", "window.location.href = '" & Request.Url.ToString & "&conf=1'; return false;")
                If Request.QueryString.Get("sValue") > 0 Then
                    ltrl_shipping_msg.Text = "<p><b>Disclamer:</b> Please be aware the shipping cost of " & FormatCurrency(Request.QueryString.Get("sValue"), 2) & " will be added to final bid amount.</p>"
                Else
                    ltrl_shipping_msg.Text = "<p><b>Disclamer:</b> Please be aware the shipping cost will be added to final bid amount.</p>"
                End If

            Else
                Dim str As String = validOrder(True)
                If IsNumeric(ViewState("o")) AndAlso (str = "" And ViewState("o") <> 0) Then
                    'if offer clicked
                    Dim dtBid As New DataTable
                    dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(B.bid_amount, 0) AS bid_amount,ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value from tbl_auction_bids B WITH (NOLOCK) inner join tbl_auctions A WITH (NOLOCK) on A.auction_id=B.auction_id  where A.auction_id=" & hid_auction_id.Value & " order by bid_amount desc")
                    If dtBid.Rows.Count > 0 Then
                        With dtBid.Rows(0)
                            If Not (.Item("bid_amount") < .Item("thresh_hold_value")) And .Item("thresh_hold_value") > 0 Then
                                str = "Sorry, This offer is currently not available. Please bid to win this product."
                            End If
                        End With
                    End If
                End If
                If str <> "" Then
                    ConfirmOrder(str)
                Else
                    If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                        hid_buyer_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
                        hid_buyer_id.Value = CommonCode.Fetch_Cookie_Shared("buyer_id")
                    End If
                    SetBidNowConfirm()

                End If
            End If

        End If

        rb_total_bid_amt.Attributes.Add("OnChange", "Javascript:CalculateTotal();")
        rb_partial_bid_amt.Attributes.Add("OnChange", "Javascript:CalculatePerUnit();")
        ' txt_max_bid_amt.Attributes.Add("OnChange", "Javascript:recalculatePerUnitTotal();")
        txt_max_bid_amt.Attributes.Add("onblur", "Javascript:recalculatePerUnitTotal();")
        but_main_auction.Attributes.Add("OnClick", "Javascript:RecalculateTotal(); return comparebid();")

    End Sub
    Private Sub SetBidNowConfirm()
        'This function is call when user logged in and ready to bid
        ' Response.Write("This function is call when user logged in and ready to bid")
        pnl_offer.Visible = False
        pnl_confirm.Visible = False
        pnl_send_quote.Visible = False
        tr_price.Visible = False
        tr_qty.Visible = False
        tr_message.Visible = False
        lit_buy_now_amount.Visible = False
        'ImageButtonOffer.ImageUrl = "/Images/fend/4.png"

        Select Case ViewState("o")
            Case 1 'Buy Now Offer
                ViewState("option") = 1
                pnl_offer.Visible = True
                ddl_qty.Visible = True
                tr_qty.Visible = False
                lit_qty.Visible = False
                spn_amt_txt.Visible = False
                tr_price.Visible = True
                Dim total_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and auction_id=" & hid_auction_id.Value & "")
                Dim already_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and auction_id=" & hid_auction_id.Value & "")
                Dim rem_qty As Integer = 0
                Dim dtNow As New DataTable
                dtNow = SqlHelper.ExecuteDatatable("select ISNULL(buy_now_price,0) as buy_now_price,ISNULL(qty_per_bidder,0) as qty_per_bidder,ISNULL(total_qty,0) as total_qty from tbl_auctions WITH (NOLOCK) where auction_id=" & hid_auction_id.Value)
                If dtNow.Rows.Count > 0 Then
                    With dtNow.Rows(0)
                        hid_buy_now_price.Value = .Item("buy_now_price")
                        rem_qty = .Item("total_qty")
                        If .Item("qty_per_bidder") > 0 Then
                            rem_qty = .Item("qty_per_bidder") - already_buy
                        End If
                        If .Item("total_qty") - total_buy < rem_qty Then
                            rem_qty = .Item("total_qty") - total_buy
                        End If

                        hid_buy_avl_qty.Value = rem_qty

                    End With
                End If

                dtNow.Dispose()


                For i As Integer = 1 To hid_buy_avl_qty.Value
                    ddl_buy_now_qty.Items.Add(New ListItem(i, i))
                Next
                lit_buy_now_amount.Text = FormatNumber(hid_buy_now_price.Value, 2)
                lit_buy_now_amount.Visible = True

            Case 2 'private offer
                ViewState("option") = 2
                pnl_offer.Visible = True
                tr_price.Visible = True
                lit_qty.Visible = True
            Case 3 'Partial Offer
                ViewState("option") = 3
                pnl_offer.Visible = True
                tr_price.Visible = True
                tr_qty.Visible = True
                lit_qty.Visible = False
            Case 0 'Buy Now/Trad/Proxy/Quote
                ViewState("option") = 0
                Select Case ViewState("t")
                    Case 1
                        pnl_offer.Visible = True
                        tr_message.Visible = True
                        lit_offer_text.Text = "I want to buy this lot for $" & FormatNumber(Request.QueryString.Get("p"), 2) & " (" & FormatCurrency(Request.QueryString.Get("q"), 2) & "per unit)"
                    Case 2
                        pnl_offer.Visible = True
                        tr_message.Visible = True
                        ' ImageButtonOffer.ImageUrl = "/Images/fend/5.png"
                        lit_offer_text.Text = "I want to bid this lot for $" & FormatNumber(Request.QueryString.Get("p"), 2) & " (" & FormatCurrency(Request.QueryString.Get("q"), 2) & "per unit)"
                    Case 3
                        pnl_offer.Visible = True
                        tr_message.Visible = True
                        'ImageButtonOffer.ImageUrl = "/Images/fend/5.png"
                        lit_offer_text.Text = "I want to bid this lot for $" & FormatNumber(Request.QueryString.Get("p"), 2) & " (" & FormatCurrency(Request.QueryString.Get("q"), 2) & "per unit)"
                    Case 4
                        pnl_send_quote.Visible = True

                End Select

        End Select
       
        If ViewState("o") = 0 And (ViewState("t") = 2 Or ViewState("t") = 3) Then
            BidNow(hid_buyer_user_id.Value, hid_buyer_id.Value)
        End If


    End Sub
    Private Sub SetAuctionHeading(ByVal title As String)
        lbl_modal_caption.Text = title

        Select Case ViewState("o")
            Case 1 'Buy Now Offer

                lbl_modal_catg.Text = "Buy Now : "

                'lbl_modal_caption.Text = "<span style='font-size:" & IIf(title.ToString.Length < 70, "20px", IIf(title.ToString.Length < 150, "18px", IIf(title.ToString.Length < 200, "16px", "14px"))) & ";'>" & title & "</span>"


            Case 2 'private offer

                lbl_modal_catg.Text = "Buy Now : "
                ' lbl_modal_caption.Text = "<span style='font-size:" & IIf(title.ToString.Length < 70, "20px", IIf(title.ToString.Length < 150, "18px", IIf(title.ToString.Length < 200, "16px", "14px"))) & ";'>" & title & "</span>"
            Case 3 'Partial Offer

                lbl_modal_catg.Text = "Buy Now : "
                 'lbl_modal_caption.Text = "<span style='font-size:" & IIf(title.ToString.Length < 70, "20px", IIf(title.ToString.Length < 150, "18px", IIf(title.ToString.Length < 200, "16px", "14px"))) & ";'>" & title & "</span>"


            Case 0 'Buy Now/Trad/Proxy/Quote

                Select Case ViewState("t")
                    Case 1

                        lbl_modal_catg.Text = "Buy Now : "
                        'lbl_modal_caption.Text = "<span style='font-size:" & IIf(title.ToString.Length < 70, "20px", IIf(title.ToString.Length < 150, "18px", IIf(title.ToString.Length < 200, "16px", "14px"))) & ";'>" & title & "</span>"


                    Case 2

                        lbl_modal_catg.Text = "Bid Now : "
                        'lbl_modal_caption.Text = "<span style='font-size:" & IIf(title.ToString.Length < 70, "20px", IIf(title.ToString.Length < 150, "18px", IIf(title.ToString.Length < 200, "16px", "14px"))) & ";'>" & title & "</span>"

                    Case 3

                        lbl_modal_catg.Text = "Bid Now : "
                        'lbl_modal_caption.Text = "<span style='font-size:" & IIf(title.ToString.Length < 70, "20px", IIf(title.ToString.Length < 150, "18px", IIf(title.ToString.Length < 200, "16px", "14px"))) & ";'>" & title & "</span>"
                    Case 4
                        lbl_modal_catg.Text = "Offer : "
                        Dim request_msg As String = SqlHelper.ExecuteScalar("select isnull(request_for_quote_message,'') as request_for_quote_message from tbl_auctions WITH (NOLOCK) where auction_id=" & hid_auction_id.Value)
                        ltrl_requset_msg.Text = "<p>" & request_msg & "</p>"
                        'lbl_modal_caption.Text = "<span style='font-size:" & IIf(title.ToString.Length < 70, "20px", IIf(title.ToString.Length < 150, "18px", IIf(title.ToString.Length < 200, "16px", "14px"))) & ";'>" & title & "</span>"

                End Select

        End Select

    End Sub
    Private Sub ConfirmOrder(ByVal message As String)

        chk_accept_term.Checked = False
        pnl_offer.Visible = False
        pnl_send_quote.Visible = False
        pnl_confirm.Visible = True
        lbl_popmsg_msg.Text = message
        lbl_modal_catg.Visible = True
        lbl_modal_caption.Visible = True


    End Sub
  
    Protected Sub btn_buy_now_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_buy_now.Click
        If Not chk_accept_term.Checked Then
            lbl_terms_error.Visible = True
        Else
            BidNow(hid_buyer_user_id.Value, hid_buyer_id.Value)
        End If

    End Sub


#Region "Funtions"

    Private Sub BidNow(ByVal user_id As Integer, ByVal buyer_id As Integer)
        Dim validate As String = validOrder(False)
        If validate = "" Then
            Dim str As String = ""
            Dim obj As New Bid()
            Dim objEmail As New Email()
            Dim strShipping As String = ""
            Dim strShippingAmount As Double = 0
            If Request.QueryString.Get("sOption") <> "" Then
                strShipping = Request.QueryString.Get("sOption")
                strShippingAmount = Request.QueryString.Get("sValue")
                Dim shipping_preference As String = IIf(strShipping = "Use my own shipping carrier to ship this item to me", "1", "2")
                Dim last_shipping_option As String = strShipping
                If strShipping.IndexOf("$") > 0 Then
                    last_shipping_option = last_shipping_option.Substring(0, strShipping.IndexOf("$"))
                End If

                str = "update tbl_reg_buyers set shipping_preference='" & shipping_preference & "',last_shipping_option='" & last_shipping_option.Trim & "' where buyer_id=" & hid_buyer_id.Value
                SqlHelper.ExecuteNonQuery(str)
            End If


            Select Case ViewState("o")

                Case 0 'main button click
                    Select Case ViewState("t")
                        Case 1
                            Response.Write("<script>window.opener.redirectConfirm(1," & Request.QueryString.Get("p") & "," & hid_auction_id.Value & ",'buy now');window.close();</script>")
                            str = "We will review your request to Buy it Now and get back to you within 2 business days.<br><br>Thank you for participating!"
                        Case 2
                            obj.insert_tbl_auction_bids(hid_auction_id.Value, user_id, hid_bid_now_price.Value, strShipping, strShippingAmount, Request.QueryString.Get("setProxy"))

                            Dim rak As Integer = SqlHelper.ExecuteScalar("select dbo.buyer_bid_rank(" & hid_auction_id.Value & "," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
                            If rak = 1 Then
                                str = "Congratulations! You are now the highest bidder."
                            Else
                                Dim dtRank As DataTable = SqlHelper.ExecuteDatatable("select isnull(is_show_rank,0) as is_show_rank,isnull(increament_amount,0) as increament_amount from tbl_auctions with (nolock) where auction_id=" & hid_auction_id.Value)
                                Dim is_show_rank As Boolean = dtRank.Rows(0).Item("is_show_rank")
                                Dim increament_amount As Double = dtRank.Rows(0).Item("increament_amount")
                                dtRank.Dispose()
                                If is_show_rank Then
                                    str = "You have been outbid.<br><br>Your Current rank is now " & rak
                                Else
                                    str = "You have been outbid."
                                End If
                                dv_bidding.Visible = True
                                'CompareValidator2.ValueToCompare = Request.QueryString.Get("P") + increament_amount

                                hid_accepted_bid.Value = Request.QueryString.Get("P") + increament_amount

                                If Request.QueryString.Get("sValue") > 0 Then
                                    lit_ship_note1.Text = "<p><b>Disclamer:</b> Please be aware the shipping cost of " & FormatCurrency(Request.QueryString.Get("sValue"), 2) & " will be added to final bid amount.</p>"
                                Else
                                    lit_ship_note1.Text = "<p><b>Disclamer:</b> Please be aware the shipping cost will be added to final bid amount.</p>"
                                End If

                            End If

                        Case 3
                             obj.insert_tbl_auction_bids(hid_auction_id.Value, user_id, Request.QueryString.Get("p"), strShipping, strShippingAmount, Request.QueryString.Get("setProxy"))
                            Dim rak As Integer = SqlHelper.ExecuteScalar("select dbo.buyer_bid_rank(" & hid_auction_id.Value & "," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
                            If rak = 1 Then
                                str = "Congratulations! You are now the highest bidder."
                            Else
                                Dim dtRank As DataTable = SqlHelper.ExecuteDatatable("select isnull(is_show_rank,0) as is_show_rank,isnull(increament_amount,0) as increament_amount from tbl_auctions with (nolock) where auction_id=" & hid_auction_id.Value)
                                Dim is_show_rank As Boolean = dtRank.Rows(0).Item("is_show_rank")
                                Dim increament_amount As Double = dtRank.Rows(0).Item("increament_amount")
                                dtRank.Dispose()
                                If is_show_rank Then
                                    str = "You have been outbid.<br><br>Your Current rank is now " & rak
                                Else
                                    str = "You have been outbid."
                                End If
                                dv_bidding.Visible = True
                                ' CompareValidator2.ValueToCompare = Request.QueryString.Get("P") + increament_amount
                                hid_accepted_bid.Value = Request.QueryString.Get("P") + increament_amount
                                If Request.QueryString.Get("sValue") > 0 Then
                                    lit_ship_note1.Text = "<p><b>Disclamer:</b> Please be aware the shipping cost of " & FormatCurrency(Request.QueryString.Get("sValue"), 2) & " will be added to final bid amount.</p>"
                                Else
                                    lit_ship_note1.Text = "<p><b>Disclamer:</b> Please be aware the shipping cost will be added to final bid amount.</p>"
                                End If
                            End If
                        Case Else
                    End Select
                Case 1
                    Response.Write("<script>window.opener.redirectConfirm(" & ddl_buy_now_qty.SelectedValue & "," & hid_buy_now_price.Value & "," & hid_auction_id.Value & ",'buy now');window.close();</script>")
                    str = "Thank you for submitting your order."
                Case 2
                    Response.Write("<script>window.opener.redirectConfirm(1," & txt_amount.Text.Trim & "," & hid_auction_id.Value & ",'private');window.close();</script>")
                    str = "Thank you for submitting private order."
                Case 3
                    Response.Write("<script>window.opener.redirectConfirm(" & txt_qty.Text.Trim & "," & txt_amount.Text.Trim & "," & hid_auction_id.Value & ",'partial');window.close();</script>")
                    str = "Thank you for submitting partial order."
                Case Else

            End Select
            objEmail = Nothing
            obj = Nothing


            ConfirmOrder(str)
        Else
            ConfirmOrder(validate)
        End If

    End Sub
    Private Function validOrder(ByVal bid_over_check_only As Boolean) As String
        Dim flg As String = ""

        Dim auc_stat As Integer = SqlHelper.ExecuteScalar("select dbo.get_auction_status(" & hid_auction_id.Value & ")")
        Select Case auc_stat
            Case 2
                Dim is_accept_pre_bid As Boolean = SqlHelper.ExecuteScalar("SELECT ISNULL(is_accept_pre_bid, 0) from tbl_auctions WITH (NOLOCK) WHERE auction_id =" & hid_auction_id.Value)
                If Not is_accept_pre_bid Then
                    flg = "Sorry, but we’ve sold out of this product. Please check back regularly for the opportunity to participate in future auctions."
                End If
            Case 3
                flg = "Sorry, but we’ve sold out of this product. Please check back regularly for the opportunity to participate in future auctions."
        End Select

        If flg = "" And bid_over_check_only = False Then

            Dim bid_limit As Double = SqlHelper.ExecuteScalar("select case when exists(select buyer_user_id from tbl_reg_buyer_users where buyer_user_id=" & hid_buyer_user_id.Value & " ) then (select isnull(bidding_limit,0) from tbl_reg_buyer_users where buyer_user_id=" & hid_buyer_user_id.Value & ") else 0 end")
            If bid_limit = 0 Then bid_limit = Double.MaxValue
            If ViewState("o") = 0 Then 'if main button clicked

                Select Case ViewState("t")
                    Case 1
                        If bid_limit < hid_buy_now_price.Value Then
                            flg = "Sorry, Bid amount exceeds your bidding limit.<br><br><a href='mailto:" & SqlHelper.of_FetchKey("increase_bid_limit_to") & "?Subject=Increase%20Bidding%20limit' target='_top'>Request bid limit increase</a>"
                        End If
                    Case 2
                        If bid_limit < hid_bid_now_price.Value Then
                            flg = "Sorry, Bid amount exceeds your bidding limit.<br><br><a href='mailto:" & SqlHelper.of_FetchKey("increase_bid_limit_to") & "?Subject=Increase%20Bidding%20limit' target='_top'>Request bid limit increase</a>"
                        End If
                    Case 3
                        If IsNumeric(Request.QueryString.Get("p")) Then
                            If CDbl(Request.QueryString.Get("p")) < CDbl(hid_bid_now_price.Value) Then
                                flg = "Sorry, Invalid Amount."
                            Else
                                If bid_limit < Request.QueryString.Get("p") Then
                                    flg = "Sorry, Bid amount exceeds your bidding limit.<br><br><a href='mailto:" & SqlHelper.of_FetchKey("increase_bid_limit_to") & "?Subject=Increase%20Bidding%20limit' target='_top'>Request bid limit increase</a>"
                                End If

                            End If
                        Else
                            flg = "Sorry, Bid amount is invalid."
                        End If

                End Select
            Else
                'if offer clicked
                Dim dtBid As New DataTable
                dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(B.bid_amount, 0) AS bid_amount,ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value from tbl_auction_bids B WITH (NOLOCK) inner join tbl_auctions A WITH (NOLOCK) on A.auction_id=B.auction_id  where A.auction_id=" & hid_auction_id.Value & " order by bid_amount desc")
                If dtBid.Rows.Count > 0 Then
                    With dtBid.Rows(0)
                        If Not (.Item("bid_amount") < .Item("thresh_hold_value")) And .Item("thresh_hold_value") > 0 Then
                            flg = "Sorry, This offer is currently not available. Please bid to win this product."
                        End If
                    End With
                End If
                If flg = "" Then

                    Select Case ViewState("o")
                        Case 1
                            If bid_limit < hid_buy_now_price.Value Then
                                flg = "Sorry, Bid amount exceeds your bidding limit.<br><br><a href='mailto:" & SqlHelper.of_FetchKey("increase_bid_limit_to") & "?Subject=Increase%20Bidding%20limit' target='_top'>Request bid limit increase</a>"
                            Else
                                Dim total_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and auction_id=" & hid_auction_id.Value & "")
                                Dim already_buy As Integer = SqlHelper.ExecuteScalar("select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and auction_id=" & hid_auction_id.Value & "")
                                Dim rem_qty As Integer = 0
                                Dim dtNow As New DataTable
                                dtNow = SqlHelper.ExecuteDatatable("select ISNULL(buy_now_price,0) as buy_now_price,ISNULL(qty_per_bidder,0) as qty_per_bidder,ISNULL(total_qty,0) as total_qty from tbl_auctions WITH (NOLOCK) where auction_id=" & hid_auction_id.Value)
                                If dtNow.Rows.Count > 0 Then
                                    With dtNow.Rows(0)
                                        hid_buy_now_price.Value = .Item("buy_now_price")
                                        rem_qty = .Item("total_qty")
                                        If .Item("qty_per_bidder") > 0 Then
                                            rem_qty = .Item("qty_per_bidder") - already_buy
                                        End If
                                        If .Item("total_qty") - total_buy < rem_qty Then
                                            rem_qty = .Item("total_qty") - total_buy
                                        End If

                                        hid_buy_avl_qty.Value = rem_qty

                                    End With
                                End If

                                dtNow.Dispose()

                                If rem_qty <= 0 Then
                                    flg = "Sorry, but we’ve sold out of this product. Please check back regularly for the opportunity to participate in future auctions."
                                End If
                            End If
                        Case 2
                            If IsNumeric(txt_amount.Text) Then
                                If bid_limit < txt_amount.Text Then
                                    flg = "Sorry, Bid amount exceeds your bidding limit.<br><br><a href='mailto:" & SqlHelper.of_FetchKey("increase_bid_limit_to") & "?Subject=Increase%20Bidding%20limit' target='_top'>Request bid limit increase</a>"
                                Else
                                    Dim private_lower_amt As Integer = SqlHelper.ExecuteScalar("select isnull(private_lower_amt,0) from tbl_auctions WITH (NOLOCK) where auction_id=" & hid_auction_id.Value & "")
                                    If private_lower_amt > txt_amount.Text Then
                                        flg = "Sorry, Amount too low to be accepted."
                                    End If

                                End If
                            Else
                                flg = "Sorry, Bid amount is invalid."
                            End If

                        Case 3


                            If IsNumeric(txt_qty.Text.Trim) Then
                                If txt_qty.Text.Trim <= 0 Then
                                    flg = "Sorry, quantity is invalid."
                                End If
                            Else
                                flg = "Sorry, quantity is invalid."
                            End If
                            If Not IsNumeric(txt_amount.Text.Trim) Then
                                flg = "Sorry, Bid amount is invalid."
                            End If

                            If flg = "" Then
                                Dim dtlower As DataTable = SqlHelper.ExecuteDatatable("select isnull(partial_lower_qty,0) as partial_lower_qty,isnull(partial_lower_amt,0) as partial_lower_amt from tbl_auctions WITH (NOLOCK) where auction_id=" & hid_auction_id.Value & "")
                                If dtlower.Rows.Count > 0 Then
                                    With dtlower.Rows(0)
                                        If .Item("partial_lower_qty") > txt_qty.Text.Trim Then
                                            flg = "Sorry, quantity too low to be accepted."
                                        Else
                                            If .Item("partial_lower_amt") > txt_amount.Text Then
                                                flg = "Sorry, Amount too low to be accepted."
                                            Else
                                                If bid_limit < txt_amount.Text * txt_qty.Text.Trim Then
                                                    flg = "Sorry, Bid amount exceeds your bidding limit.<br><br><a href='mailto:" & SqlHelper.of_FetchKey("increase_bid_limit_to") & "?Subject=Increase%20Bidding%20limit' target='_top'>Request bid limit increase</a>"
                                                End If
                                            End If
                                        End If
                                    End With

                                End If
                                dtlower.Dispose()
                            End If


                        Case Else

                    End Select
                End If

            End If
        End If


        Return flg
    End Function

#End Region

   
    Protected Sub Button_quote_send_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button_quote_send.Click
        request_quote()
        Dim objEmail As New Email()
        objEmail.Send_Auction_Order_Mail(hid_auction_id.Value, ViewState("option"), CommonCode.Fetch_Cookie_Shared("user_id"))
        objEmail = Nothing

        Dim str As String = ""
        str = "Thank you for submitting your Offer."

        ConfirmOrder(str)
    End Sub


    Private Sub request_quote()
        'Try
        Dim details As String = txt_quote_desc.Text.Replace(Char.ConvertFromUtf32(10), "<br>")
        Dim Quotation_id As Integer = 0

        Dim filename As String = ""
        If fle_quote_upload.HasFile Then
            filename = fle_quote_upload.FileName
            filename = CommonCode.Fetch_Cookie_Shared("user_id") & "_" & filename
        End If
        Dim obj As New Bid()
        Quotation_id = obj.send_quotation(hid_auction_id.Value, details.Trim(), CommonCode.Fetch_Cookie_Shared("user_id"), CommonCode.Fetch_Cookie_Shared("buyer_id"))

        If fle_quote_upload.HasFile Then
            If Quotation_id > 0 Then
                obj.upload_quotation_file(hid_auction_id.Value, Quotation_id, fle_quote_upload, CommonCode.Fetch_Cookie_Shared("user_id"))
            End If
        Else
        End If
        obj = Nothing
        ' Catch ex As Exception

        ' End Try

    End Sub

    Protected Sub but_main_auction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_main_auction.Click
        Dim url As String = Request.Url.ToString()

        If txt_max_bid_amt.Text.Length > 0 Then
            url = url.Replace("&conf=1", "")
            url = url.Replace("p=" & Request.QueryString.Get("p"), "p=" & txt_max_bid_amt.Text.Trim)

            If hid_quantity.Value > 1 And txt_max_bid_amt.Text.Length > 1 Then
                Dim perunit As Integer = Convert.ToInt32(hid_quantity.Value)
                Dim bidamtperunit As Double = txt_max_bid_amt.Text / perunit
                url = url.Replace("q=" & Request.QueryString.Get("q"), "q=" & FormatCurrency(bidamtperunit, 2))
            Else
                url = url.Replace("q=" & Request.QueryString.Get("q"), "q=" & String.Empty)
            End If


            Response.Redirect(url)
        End If

    End Sub
End Class
