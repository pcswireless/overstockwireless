Imports System.Data
Imports System.Data.SqlClient

Public Class aspInput
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Response.Write(Server.MachineName)
    End Sub

    Private Sub cmdSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSubmit.Click

        Dim Con = New SqlConnection(SqlHelper.of_getConnectString())
        Con.Open()

        Dim sql As String
        Dim rsFill As SqlCommand
        Dim dtr As SqlDataReader
        Dim str As String
        Dim n As Integer
        Dim i As Integer

        sql = Me.txtInput.Text
        rsFill = New SqlCommand(sql, Con)
        If UCase(sql.Substring(0, 6)) = "SELECT" Or UCase(sql.Substring(0, 4)) = "EXEC" Then
            dtr = rsFill.ExecuteReader
            str = "<table border=1 cellspacing=0 bordercolor=orange><tr bgcolor=red nowrap> "
            For i = 0 To dtr.FieldCount - 1
                str = str & "<td><b><font color=white face=verdana size=1>" & dtr.GetName(i) & "</font>&nbsp;</b></td>"
            Next
            str = str & "</tr><tr>"

            Do While dtr.Read()
                For i = 0 To dtr.FieldCount - 1
                    If i Mod 2 = 0 Then
                        str = str & "<td><font face=verdana size=1>" & dtr(i) & "&nbsp;</font></td>"
                    Else
                        str = str & "<td bgcolor=#f7f7f7><font face=verdana size=1>" & dtr(i) & "&nbsp;</font></td>"
                    End If
                Next
                str = str & "</tr><tr>"
            Loop
            str = str & "</tr></table>"
            dtr.Close()
        Else
            str = rsFill.ExecuteNonQuery() & " row(s) effected"
        End If
        Me.lblResult.Text = str
        Con.Close()

    End Sub
End Class
