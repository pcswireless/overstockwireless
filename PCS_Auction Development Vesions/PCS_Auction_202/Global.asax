﻿<%@ Application Language="VB" %>

<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
        If String.IsNullOrEmpty(CommonCode.Fetch_Cookie_Shared("_lang")) Then
            CommonCode.Create_Cookie_shared("_lang", "en-US")
        End If
            
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
        If CommonCode.Fetch_Cookie_Shared("is_backend") <> "1" Then
            Dim subject As String = ""
            Dim ip As String = Request.UserHostAddress.ToString()
            Dim url_referrer As String = ""
            Dim buyer_id As String = CommonCode.Fetch_Cookie_Shared("buyer_id")
        
            If Not System.Web.HttpContext.Current.Request.UrlReferrer Is Nothing Then
                url_referrer = System.Web.HttpContext.Current.Request.UrlReferrer.ToString
            End If
            subject = "ERROR FE: Auction Website Error"
            If SqlHelper.of_FetchKey("is_server") = "1" Then
                Dim objCommon As New CommonCode
                objCommon.sendEmail_error("", "", subject, "Error Time : " & DateTime.Now.ToString() & "<br>" & Request.UserHostAddress.ToString() & "<br>" & "Referrer URL:" & url_referrer & "<br>" & "Buyer id:" & buyer_id & "<br>" & Request.Url.ToString() & " Message = " & Server.GetLastError.Message & "<br><br> Description = " & Server.GetLastError.ToString, SqlHelper.of_FetchKey("error_mail_to"), "", "", "", "", "")
                objCommon = Nothing
            End If
        End If
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
       
</script>