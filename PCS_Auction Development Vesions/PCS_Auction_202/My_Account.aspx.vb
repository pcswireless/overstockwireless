﻿Imports Telerik.Web.UI

Partial Class My_Account
    Inherits System.Web.UI.Page
    Dim obj As New Bid
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        lit_refresh.Text = ""
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            If CommonCode.Fetch_Cookie_Shared("is_backend") = "1" Then
                Response.Write("<script language='javascript'>top.location.href = '/Backend_Home.aspx?t=1';</script>")
            End If
        Else
            Response.Write("<script language='javascript'>top.location.href = '/';</script>")
        End If
        hid_flang.Value = System.Threading.Thread.CurrentThread.CurrentCulture.Name
        If Not IsPostBack Then
            hd_buyer_id.Value = 0
            hd_buyer_user_id.Value = 0
            Dim buyer_id As String = CommonCode.Fetch_Cookie_Shared("buyer_id")
            Dim user_id As String = CommonCode.Fetch_Cookie_Shared("user_id")
            If Not String.IsNullOrEmpty(buyer_id) Then
                If buyer_id <> "" Then
                    If IsNumeric(buyer_id) Then
                        If buyer_id > 0 Then

                            If Not String.IsNullOrEmpty(user_id) Then
                                If user_id <> "" Then
                                    If IsNumeric(user_id) Then
                                        If user_id > 0 Then
                                            hd_buyer_id.Value = buyer_id
                                            hd_buyer_user_id.Value = user_id
                                        End If
                                    End If
                                End If
                            End If

                        End If
                    End If
                End If
            End If
            fill_countries()
            fill_how_find_us()
            'fill_business_types()
            'fill_industry_types()
            fill_companies()
            fill_titles()
            If hd_buyer_id.Value > 0 AndAlso hd_buyer_user_id.Value > 0 Then
                lit_add.Text = "<a href='#' class='sbmt_btn' style='text-decoration:none;' onclick=""return Opendetailnew('" & hd_buyer_id.Value & "');"">Add</a>"
                hd_is_buyer.Value = CommonCode.Fetch_Cookie_Shared("is_buyer")


                If hd_is_buyer.Value = 1 Then
                    set_button_visibility(True)
                Else
                    set_button_visibility(False)
                End If
                BasicTabEdit(True)
                'set_button_visibility("View")
                'BindBuyerBasicInfo()

                fillSubInfoTab(hd_buyer_user_id.Value)

                'If Not imgbtn_basic_edit.Visible And hd_buyer_user_id.Value > 0 Then
                '    'imgbtn_basic_pwd.Visible = True
                '    imgbtn_basic_pwd.OnClientClick = "return open_pass_win('/change_password.aspx','b=0&i=" & hd_buyer_user_id.Value & "&o=1');"
                'End If
                imgbtn_basic_pwd.OnClientClick = "return open_pass_win('/change_password.aspx','b=0&i=" & hd_buyer_user_id.Value & "&o=1');"
                imgbtn_sub_change_password.OnClientClick = "return open_pass_win('/change_password.aspx','b=0&i=" & hd_buyer_user_id.Value & "&o=1');"
            Else

                hd_buyer_id.Value = 0
                hd_is_buyer.Value = 0
                hd_buyer_user_id.Value = 0
                'fillSubInfoTab(hd_buyer_user_id.Value)

                'lblPageHeading.Text = "New Bidder"
                BasicTabEdit(False)
                'set_button_visibility("Add")

            End If

            ' Page.ClientScript.RegisterStartupScript(Page.GetType(), "_call_set", "status_drop_txt();", True)
            showComment(hd_buyer_id.Value)
            bind_login()
            End If

    End Sub
    Protected Sub btn_history_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_history.Click
        bind_login()
        lit_refresh.Text = "<script type='text/javascript'>window.location.href='#tabs-7'</script>"
    End Sub
#Region "Basic INFO"
    Protected Sub bind_login()
        Dim strQuery As String = ""
        strQuery = "SELECT " & _
        "buyer_user_id," & _
        "ISNULL(A.buyer_id, 0) AS buyer_id," & _
        "ISNULL(A.first_name, '') + ' ' + ISNULL(A.last_name, '') AS name," & _
        "ISNULL(A.first_name, '') AS first_name," & _
        "ISNULL(A.last_name, '') AS last_name," & _
        "title," & _
        "ISNULL(A.username, '') AS username," & _
        "ISNULL(A.password, '') AS password," & _
        "ISNULL(A.email, '') AS email," & _
        "Convert(decimal(10,2),ISNULL(A.bidding_limit, 0)) AS bidding_limit," & _
        "ISNULL(A.is_active, 0) AS is_active" & _
        " FROM tbl_reg_buyer_users A WITH (NOLOCK) where buyer_id=" & hd_buyer_id.Value & " and is_admin=0 order by buyer_user_id DESC"
        rptItems.DataSource = SqlHelper.ExecuteDatatable(strQuery)
        rptItems.DataBind()
    End Sub
    Private Sub fillBasicTab(ByVal buyer_id As Integer)
        Dim dt As DataTable = New DataTable()
        Dim strQuery As String = ""

        strQuery = "SELECT A.buyer_id, " & _
                      "ISNULL(A.company_name, '') AS company_name, " & _
                      "ISNULL(A.contact_title, '') AS contact_title, " & _
                      "ISNULL(A.contact_first_name, '') AS contact_first_name, " & _
                      "ISNULL(A.contact_last_name, '') AS contact_last_name, " & _
                      "ISNULL(A.email, '') AS email, " & _
                      "ISNULL(A.website, '') AS website, " & _
                      "ISNULL(A.phone, '') AS phone, " & _
                      "ISNULL(A.mobile, '') AS mobile, " & _
                      "ISNULL(A.fax, '') AS fax," & _
                      "ISNULL(A.address1, '') AS address1, " & _
                      "ISNULL(A.address2, '') AS address2, " & _
                      "ISNULL(A.city, '') AS city, " & _
                      "ISNULL(A.state_id, 0) AS state_id," & _
                      "ISNULL(A.state_text,'') As state_text, " & _
                      "ISNULL(A.zip, '') AS zip, " & _
                      "ISNULL(A.max_amt_bid, 0) AS max_amt_bid, " & _
                      "ISNULL(A.country_id, 0) AS country_id, " & _
                      "ISNULL(C.name, '') AS country,ISNULL(A.status_id, 0) AS status_id, " & _
                      "ISNULL(A.comment, '') AS comment, " & _
                      "ISNULL(A.is_phone1_mobile, 0) AS is_phone1_mobile, " & _
                      "ISNULL(A.is_phone2_mobile, 0) AS is_phone2_mobile," & _
                      "(select count(unsubscription_id) from tbl_unsubscription where email=A.email) as is_unsubscribed_invitation " & _
                      "FROM tbl_reg_buyers A WITH (NOLOCK) Left JOIN tbl_master_countries C WITH (NOLOCK) ON A.country_id=C.country_id WHERE A.buyer_id =" & buyer_id.ToString()

        dt = SqlHelper.ExecuteDatatable(strQuery)
        If dt.Rows.Count > 0 Then
            ' lblPageHeading.Text = "Bidder: " & dt.Rows(0)("company_name")
            txt_company.Text = dt.Rows(0)("company_name")
            lbl_company.Text = dt.Rows(0)("company_name")
            hid_buyer_max_bid_amount.Value = dt.Rows(0)("max_amt_bid")
            lbl_title.Text = dt.Rows(0)("contact_title")
            txt_comment.Text = dt.Rows(0)("comment").ToString.Replace("<br/>", Char.ConvertFromUtf32(13)).Replace("<br/>", Char.ConvertFromUtf32(10))
            lbl_comment.Text = dt.Rows(0)("comment").ToString
            txt_first_name.Text = dt.Rows(0)("contact_first_name")
            lbl_first_name.Text = dt.Rows(0)("contact_first_name")
            txt_last_name.Text = dt.Rows(0)("contact_last_name")
            lbl_last_name.Text = dt.Rows(0)("contact_last_name")
            txt_email.Text = dt.Rows(0)("email")
            'txt_confirm_email.Text = dt.Rows(0)("email")
            lbl_email.Text = "<a href='mailto:" & dt.Rows(0)("email") & "'>" & dt.Rows(0)("email") & "</a>"
            ' lbl_confirm_email.Text = dt.Rows(0)("email")
            txt_website.Text = dt.Rows(0)("website")
            lbl_website.Text = dt.Rows(0)("website")
            txt_mobile.Text = dt.Rows(0)("mobile")
            lbl_mobile.Text = dt.Rows(0)("mobile")
            txt_phone.Text = dt.Rows(0)("phone")
            lbl_phone.Text = dt.Rows(0)("phone")
            txt_fax.Text = dt.Rows(0)("fax")
            lbl_fax.Text = dt.Rows(0)("fax")
            txt_address1.Text = dt.Rows(0)("address1").Replace("<br/>", Char.ConvertFromUtf32(13)).Replace("<br/>", Char.ConvertFromUtf32(10))
            lbl_address1.Text = dt.Rows(0)("address1")
            txt_address2.Text = dt.Rows(0)("address2").Replace("<br/>", Char.ConvertFromUtf32(13)).Replace("<br/>", Char.ConvertFromUtf32(10))
            lbl_address2.Text = dt.Rows(0)("address2")
            txt_city.Text = dt.Rows(0)("city")
            lbl_city.Text = dt.Rows(0)("city")
            rdo_ph_1_mobile.Checked = dt.Rows(0)("is_phone1_mobile")
            rdo_ph_2_mobile.Checked = dt.Rows(0)("is_phone2_mobile")
            chk_invite_auction.Checked = Convert.ToBoolean(dt.Rows(0)("is_unsubscribed_invitation"))
            hid_unsubscribe_invite.Value = dt.Rows(0)("is_unsubscribed_invitation")
            If Convert.ToBoolean(dt.Rows(0)("is_unsubscribed_invitation")) Then
                lbl_invite_auction_unsubscribe.Text = "<img src='/Images/true.gif' style='border: none;' alt='' />"
            Else
                lbl_invite_auction_unsubscribe.Text = "<img src='/Images/false.gif' style='border: none;' alt='' />"
            End If
            'chk_is_active.Checked = dt.Rows(0)("is_active")
            'lbl_is_active.Text = "<img border='none' src='" & IIf(dt.Rows(0)("is_active"), "/Images/true.gif", "/Images/false.gif") & "' />"

            txt_zip.Text = dt.Rows(0)("zip")
            lbl_zip.Text = dt.Rows(0)("zip")
            If Not ddl_title.Items.FindByText(dt.Rows(0)("contact_title")) Is Nothing Then
                ddl_title.ClearSelection()
                ddl_title.Items.FindByText(dt.Rows(0)("contact_title")).Selected = True
            End If
            If Not ddl_country.Items.FindByValue(dt.Rows(0)("country_id")) Is Nothing Then
                ddl_country.ClearSelection()
                ddl_country.Items.FindByValue(dt.Rows(0)("country_id")).Selected = True
            End If
            'fill_states()
            txt_other_state.Text = dt.Rows(0)("state_text")
            lbl_state.Text = dt.Rows(0)("state_text")

            lbl_country.Text = dt.Rows(0)("country")
            dt = Nothing

            dt = get_buyer_admin_detail(buyer_id)
            If dt.Rows.Count > 0 Then
                txt_user_id.Text = dt.Rows(0)("username")
                lbl_user_id.Text = dt.Rows(0)("username")
                'txt_password.Attributes.Add("value", Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("password")))
                lit_change_password.Text = "<a href='javascript:void(0);' onclick=""return open_pass_win('/change_password.aspx','b=0&i=" & hd_buyer_user_id.Value & "&o=1');"">Click to Change Password</a>"
                hid_password.Value = Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("password"))
                'txt_retype_password.Attributes.Add("value", Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("password")))

                lbl_password.Text = "******"     'dt.Rows(0)("password").ToString().Substring(0, IIf(dt.Rows(0)("password").ToString().Length > 3, 3, dt.Rows(0)("password").ToString().Length)).PadRight(10, "X")

            End If
            dt = Nothing

            If Not ddl_how_find_us.Items.FindByValue(get_buyer_how_find_us(buyer_id)) Is Nothing Then
                ddl_how_find_us.ClearSelection()
                ddl_how_find_us.Items.FindByValue(get_buyer_how_find_us(buyer_id)).Selected = True
            End If
            ltrl_how_find_us.Text = IIf(ddl_how_find_us.SelectedValue <> "", ddl_how_find_us.SelectedItem.Text, "&nbsp;")

            'dt = get_buyer_business_types(buyer_id)
            '' lst_business_types.DataSource = dt
            ''lst_business_types.DataBind()
            'For i As Integer = 0 To dt.Rows.Count - 1
            '    For Each lst As ListItem In chklst_business_types.Items
            '        If dt.Rows(i)("business_type_id") = lst.Value Then
            '            lst.Selected = True
            '        End If
            '    Next
            'Next
            'dt = Nothing
            Dim str As String = ""
            'str = get_buyer_business_type_string(buyer_id)
            'ltrl_business_types.Text = str

            'dt = get_buyer_industry_types(buyer_id)
            '' lst_industry_types.DataSource = dt
            '' lst_industry_types.DataBind()
            'For i As Integer = 0 To dt.Rows.Count - 1
            '    For Each lst As ListItem In chklst_industry_types.Items
            '        If dt.Rows(i)("industry_type_id") = lst.Value Then
            '            lst.Selected = True
            '        End If
            '    Next
            'Next
            'dt = Nothing
            'str = ""
            'str = get_buyer_industry_type_string(buyer_id)
            'ltrl_industry_type.Text = str

            dt = get_buyer_companies_linked(buyer_id)
            'lst_companies_linked.DataSource = dt
            ' lst_companies_linked.DataBind()
            For i As Integer = 0 To dt.Rows.Count - 1
                For Each lst As ListItem In chklst_companies_linked.Items
                    If dt.Rows(i)("seller_id") = lst.Value Then
                        lst.Selected = True
                    End If
                Next
            Next
            str = ""
            str = get_buyer_companies_linked_string(buyer_id)
            ltrl_linked_companies.Text = str

            ltrl_bucket_link.Text = "<a href=""javascript:void(0);"" onclick=""return open_bucket_assign('" & Security.EncryptionDecryption.EncryptValueFormatted(buyer_id) & "')"">Manage invitation</a>"

            ltrl_buckets.Text = get_buyer_bucket_string(buyer_id)


        End If
        dt.Dispose()

        ' End If
    End Sub

    Private Sub showComment(ByVal buyer_id As Integer)
        Dim dt As DataTable = New DataTable()
        Dim strQuery As String = ""

        strQuery = "SELECT A.buyer_id, " & _
                      "ISNULL(A.comment, '') AS comment " & _
                      "FROM tbl_reg_buyers A WITH (NOLOCK) Left JOIN tbl_master_countries C WITH (NOLOCK) ON A.country_id=C.country_id WHERE A.buyer_id =" & buyer_id.ToString()
        dt = SqlHelper.ExecuteDatatable(strQuery)
        If dt.Rows.Count > 0 Then
            ' lblPageHeading.Text = "Bidder: " & dt.Rows(0)("company_name")
            If dt.Rows(0)("comment") <> "" Then
                lbl_comment_caption.Visible = True

            End If
        End If
        dt = Nothing


        ' End If
    End Sub


    Private Function get_buyer_companies_linked(ByVal buyer_id As Integer) As DataTable
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable()
        strQuery = "SELECT 	ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.seller_id, 0) AS seller_id,ISNULL(S.company_name,'') as company_name " & _
                    "FROM tbl_reg_buyer_seller_mapping A WITH (NOLOCK) INNER JOIN tbl_reg_sellers S WITH (NOLOCK) ON A.seller_id=S.seller_id " & _
                    "WHERE ISNULL(S.is_active,0)=1 and A.buyer_id =" & buyer_id.ToString()
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function
    Private Function get_buyer_companies_linked_string(ByVal buyer_id As Integer) As String
        Dim str As String = ""
        Dim strQuery As String = "Declare @str varchar(2000) select @str=COALESCE(@str + ', ', '') + ISNULL(S.company_name,'') FROM tbl_reg_buyer_seller_mapping A WITH (NOLOCK) INNER JOIN tbl_reg_sellers S WITH (NOLOCK) ON A.seller_id=S.seller_id  " & _
            "WHERE ISNULL(S.is_active,0)=1 and A.buyer_id =" & buyer_id.ToString() & " select ISNULL(@str,'')"
        str = SqlHelper.ExecuteScalar(strQuery)
        If str = "" Then
            str = "No company linked."
        End If
        Return str
    End Function
    Private Function get_buyer_bucket_string(ByVal buyer_id As Integer) As String
        Dim str As String = ""
        Dim strQuery As String = "select B.bucket_name,V.bucket_id,V.bucket_value_id,V.bucket_value from tbl_master_buckets B inner join tbl_master_bucket_values V on B.bucket_id=V.bucket_id inner join tbl_reg_buyer_bucket_mapping M on V.bucket_value_id=M.bucket_value_id and M.buyer_id=" & buyer_id & " where B.is_active=1 and V.is_active=1 order by B.sno,V.bucket_value_id"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataSet(strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim bucket_id As Integer = 0
            str = "<table cellpadding='10' cellspacing='1' style='background-color: Gray;' width='100%'>"
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If bucket_id <> ds.Tables(0).Rows(i)("bucket_id") Then
                    bucket_id = ds.Tables(0).Rows(i)("bucket_id")
                    str = str & "<tr><td valign='top' style='background-color: White;'><b>" & ds.Tables(0).Rows(i)("bucket_name") & "</b></td><td valign='top' style='background-color: White;'>" & ds.Tables(0).Rows(i)("bucket_value") & "</td></tr>"
                Else
                    str = str & "<tr><td valign='top' style='background-color: White;'>&nbsp;</td><td valign='top' style='background-color: White;'>" & ds.Tables(0).Rows(i)("bucket_value") & "</td></tr>"
                End If

            Next
            str = str & "</table>"
        Else
            str = "Bucket not selected."
        End If
        Return str
    End Function
    'Private Function get_buyer_industry_types(ByVal buyer_id As Integer) As DataTable
    '    Dim strQuery As String = ""
    '    Dim dt As DataTable = New DataTable()
    '    strQuery = "SELECT 	ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.industry_type_id, 0) AS industry_type_id,ISNULL(I.name,'') as industry_type " & _
    '                "FROM tbl_reg_buyer_industry_type_mapping A WITH (NOLOCK) INNER JOIN tbl_master_industry_types I WITH (NOLOCK) ON A.industry_type_id=I.industry_type_id " & _
    '                "WHERE A.buyer_id =" & buyer_id.ToString()
    '    dt = SqlHelper.ExecuteDatatable(strQuery)
    '    Return dt
    'End Function
    'Private Function get_buyer_industry_type_string(ByVal buyer_id As Integer) As String
    '    Dim str As String = ""
    '    Dim strQuery As String = "Declare @str varchar(1000) select @str=COALESCE(@str + ', ', '') + ISNULL(I.name,'') FROM tbl_reg_buyer_industry_type_mapping A WITH (NOLOCK) INNER JOIN tbl_master_industry_types I WITH (NOLOCK) ON A.industry_type_id=I.industry_type_id " & _
    '        "WHERE A.buyer_id =" & buyer_id.ToString() & " select ISNULL(@str,'')"
    '    str = SqlHelper.ExecuteScalar(strQuery)
    '    If str = "" Then
    '        str = "Industry type not selected."
    '    End If
    '    Return str
    'End Function
    'Private Function get_buyer_business_types(ByVal buyer_id As Integer) As DataTable
    '    Dim strQuery As String = ""
    '    Dim dt As DataTable = New DataTable()
    '    strQuery = "SELECT 	ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.business_type_id, 0) AS business_type_id,ISNULL(B.name,'') as business_type " & _
    '                "FROM tbl_reg_buyer_business_type_mapping A WITH (NOLOCK) INNER JOIN tbl_master_business_types B WITH (NOLOCK) ON A.business_type_id=B.business_type_id " & _
    '                "WHERE A.buyer_id =" & buyer_id.ToString()
    '    dt = SqlHelper.ExecuteDatatable(strQuery)
    '    Return dt
    'End Function
    'Private Function get_buyer_business_type_string(ByVal buyer_id As Integer) As String

    '    Dim str As String = ""
    '    Dim strQuery As String = "Declare @str varchar(1000) select @str=COALESCE(@str + ', ', '') + ISNULL(B.name,'') FROM tbl_reg_buyer_business_type_mapping A WITH (NOLOCK) INNER JOIN tbl_master_business_types B WITH (NOLOCK) ON A.business_type_id=B.business_type_id " & _
    '        "WHERE A.buyer_id =" & buyer_id.ToString() & " select ISNULL(@str,'')"
    '    str = SqlHelper.ExecuteScalar(strQuery)
    '    If str = "" Then
    '        str = "Business type not selected."
    '    End If
    '    Return str
    'End Function
    Private Function get_buyer_how_find_us(ByVal buyer_id As Integer) As String
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable()
        strQuery = "SELECT 	ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.how_find_us_id, 0) AS how_find_us_id,ISNULL(F.name,'') As how_find_us FROM tbl_reg_buyer_find_us_assignment A WITH (NOLOCK) INNER JOIN tbl_master_how_find_us F WITH (NOLOCK) ON A.how_find_us_id=F.how_find_us_id WHERE buyer_id=" & buyer_id.ToString()
        dt = SqlHelper.ExecuteDatatable(strQuery)
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0)("how_find_us_id")
        Else
            Return ""
        End If
    End Function
    Private Function get_buyer_how_find_us_string(ByVal buyer_id As Integer) As String
        Dim strQuery As String = ""
        Dim str As String = ""
        strQuery = "Declare @str varchar(500) select @str=COALESCE(@str + ', ', '') + ISNULL(F.name,'') FROM tbl_reg_buyer_find_us_assignment A WITH (NOLOCK) INNER JOIN tbl_master_how_find_us F WITH (NOLOCK) ON A.how_find_us_id=F.how_find_us_id WHERE buyer_id=" & buyer_id.ToString() & " select ISNULL(@str,'')"
        str = SqlHelper.ExecuteScalar(strQuery)
        Return str
    End Function
    Private Function get_buyer_admin_detail(ByVal buyer_id As Integer) As DataTable
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable()
        strQuery = "SELECT ISNULL(A.username, '') AS username,ISNULL(A.password, '') AS password FROM tbl_reg_buyer_users A WITH (NOLOCK) WHERE A.buyer_id =" & buyer_id.ToString()
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function
    Private Sub set_button_visibility(ByVal readMode As Boolean)
        If readMode Then
            'imgbtn_basic_edit.Visible = True
            'tr_sublogin.Visible = True
            imgbtn_basic_save.Visible = False
            imgbtn_basic_update.Visible = False
            div_basic_cancel.Visible = False

        Else
            imgbtn_basic_edit.Visible = False
            'tr_sublogin.Visible = False
            If hd_buyer_id.Value = 0 Then
                imgbtn_basic_save.Visible = True
                imgbtn_basic_update.Visible = False
                div_basic_cancel.Visible = False
            Else
                imgbtn_basic_save.Visible = False
                imgbtn_basic_update.Visible = True
                'but_basic_cancel.Visible = True
                div_basic_cancel.Visible = True
            End If
        End If

    End Sub
    Private Sub fill_countries()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select country_id,name,ISNULL(code,'') As code from tbl_master_countries WITH (NOLOCK)"
        ds = SqlHelper.ExecuteDataset(strQuery)
        ddl_country.DataSource = ds
        ddl_country.DataTextField = "name"
        ddl_country.DataValueField = "country_id"
        ddl_country.DataBind()
        ddl_country.Items.Insert(0, New ListItem("--Select--", ""))
        ddl_country.ClearSelection()
        ddl_country.Items.FindByText("USA").Selected = True
        ds.Dispose()
    End Sub

    Private Sub fill_titles()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select isnull(name,'') as name,title_id from tbl_master_titles WITH (NOLOCK) order by name "
        ds = SqlHelper.ExecuteDataset(strQuery)
        ddl_title.DataSource = ds
        ddl_title.DataTextField = "name"
        ddl_title.DataValueField = "name"
        ddl_title.DataBind()
        ddl_title.Items.Insert(0, New ListItem("--Select--", ""))
        ds.Dispose()
    End Sub
    Private Sub fill_how_find_us()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select how_find_us_id,name,ISNULL(description,'') as description,ISNULL(sno,99)as sno from tbl_master_how_find_us WITH (NOLOCK) order by sno"
        ds = SqlHelper.ExecuteDataset(strQuery)
        ddl_how_find_us.DataSource = ds
        ddl_how_find_us.DataTextField = "name"
        ddl_how_find_us.DataValueField = "how_find_us_id"
        ddl_how_find_us.DataBind()
        ddl_how_find_us.Items.Insert(0, New ListItem("--Select--", ""))
        ds.Dispose()
    End Sub
    'Private Sub fill_business_types()
    '    Dim ds As DataSet
    '    Dim strQuery As String = ""
    '    strQuery = "select business_type_id,name,ISNULL(description,'') as description,ISNULL(sno,99)as sno from tbl_master_business_types WITH (NOLOCK) order by sno"
    '    ds = SqlHelper.ExecuteDataset(strQuery)
    '    chklst_business_types.DataSource = ds
    '    chklst_business_types.DataTextField = "name"
    '    chklst_business_types.DataValueField = "business_type_id"
    '    chklst_business_types.DataBind()
    '    ds.Dispose()
    'End Sub
    'Private Sub fill_industry_types()
    '    Dim ds As DataSet
    '    Dim strQuery As String = ""
    '    strQuery = "select industry_type_id,name,ISNULL(description,'') AS description,ISNULL(sno,999) as sno from tbl_master_industry_types WITH (NOLOCK) order by sno"
    '    ds = SqlHelper.ExecuteDataset(strQuery)
    '    chklst_industry_types.DataSource = ds
    '    chklst_industry_types.DataTextField = "name"
    '    chklst_industry_types.DataValueField = "industry_type_id"
    '    chklst_industry_types.DataBind()
    '    ds.Dispose()
    'End Sub
    Private Sub fill_companies()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select seller_id,company_name from tbl_reg_sellers WITH (NOLOCK) where is_active=1 order by company_name"
        ds = SqlHelper.ExecuteDataset(strQuery)
        chklst_companies_linked.DataSource = ds
        chklst_companies_linked.DataTextField = "company_name"
        chklst_companies_linked.DataValueField = "seller_id"
        chklst_companies_linked.DataBind()
        ds.Dispose()
    End Sub
    Private Sub BasicTabEdit(ByVal readMode As Boolean)
        If hd_buyer_id.Value > 0 Then
            fillBasicTab(hd_buyer_id.Value)
        Else

            If Not ddl_country.Items.FindByValue("3") Is Nothing Then
                ddl_country.ClearSelection()
                ddl_country.Items.FindByValue("3").Selected = True

            End If
            EmptyBasicData()
        End If
        ' If hd_buyer_id.Value > 0 AndAlso hi Then

        visibleBasicData(readMode)
        If hd_is_buyer.Value <> 1 And hd_buyer_id.Value > 0 Then
            'pnl_buyer.Visible = False
            pnl_edit_mode.Visible = False
            pnl_edit_mode1.Visible = False
            imgbtn_basic_edit.Visible = False
            imgbtn_basic_update.Visible = False
            imgbtn_basic_cancel.Visible = False
            imgbtn_basic_pwd.Visible = False
        Else
            pnl_buyer_user.Visible = False
            pnl_edit_mode.Visible = True
            pnl_edit_mode1.Visible = True
        End If
    End Sub

    Private Sub visibleBasicData(ByVal readMode As Boolean)
        If readMode Then
            'imgbtn_basic_edit.Visible = True
            imgbtn_basic_save.Visible = False
            imgbtn_basic_update.Visible = False
            div_basic_cancel.Visible = False
        Else
            imgbtn_basic_edit.Visible = False

            If hd_buyer_id.Value = 0 Then
                imgbtn_basic_save.Visible = True
                imgbtn_basic_update.Visible = False
                div_basic_cancel.Visible = False
            Else
                imgbtn_basic_save.Visible = False
                imgbtn_basic_update.Visible = True
                'but_basic_cancel.Visible = True
                div_basic_cancel.Visible = True
            End If
        End If


        txt_company.Visible = Not (readMode)
        ddl_title.Visible = Not (readMode)
        txt_first_name.Visible = Not (readMode)
        txt_last_name.Visible = Not (readMode)
        txt_email.Visible = Not (readMode)
        txt_other_state.Visible = Not (readMode)
        'txt_confirm_email.Visible = Not (readMode)
        txt_website.Visible = Not (readMode)
        txt_mobile.Visible = Not (readMode)
        txt_phone.Visible = Not (readMode)
        rdo_ph_1_landline.Visible = Not (readMode)
        rdo_ph_2_landline.Visible = Not (readMode)
        rdo_ph_1_mobile.Visible = Not (readMode)
        rdo_ph_2_mobile.Visible = Not (readMode)
        txt_fax.Visible = Not (readMode)
        txt_address1.Visible = Not (readMode)
        txt_address2.Visible = Not (readMode)
        txt_city.Visible = Not (readMode)
        txt_comment.Visible = Not (readMode)
        txt_zip.Visible = Not (readMode)
        ddl_country.Visible = Not (readMode)
        txt_user_id.Visible = Not (readMode)
        lit_change_password.Visible = Not (readMode)
        ' txt_retype_password.Visible = Not (readMode)
        ddl_how_find_us.Visible = Not (readMode)
        'chklst_business_types.Visible = Not (readMode)
        'chklst_industry_types.Visible = Not (readMode)
        'chk_is_active.Visible = Not (readMode)
        chklst_companies_linked.Visible = Not (readMode)
        chk_invite_auction.Visible = Not (readMode)
        'lbl_confirm_email_caption.Visible = Not (readMode)
        'lbl_confirm_email.Visible = Not (readMode)
        'lbl_retype_password_caption.Visible = Not (readMode)
        ' trRetypePassword.Visible = Not (readMode)
        'span_retype_passeord.Visible = Not (readMode)
        'span_confirm_email.Visible = Not (readMode)
        span_company_name.Visible = Not (readMode)

        span_first_name.Visible = Not (readMode)
        span_email.Visible = Not (readMode)
        span_mobile.Visible = Not (readMode)

        span_address1.Visible = Not (readMode)
        span_city.Visible = Not (readMode)
        span_state.Visible = Not (readMode)
        span_zip.Visible = Not (readMode)
        span_country.Visible = Not (readMode)
        span_user_id.Visible = Not (readMode)
        span_password.Visible = Not (readMode)

        lbl_company.Visible = readMode
        lbl_title.Visible = readMode
        lbl_first_name.Visible = readMode
        lbl_last_name.Visible = readMode
        lbl_email.Visible = readMode
        ' lbl_confirm_email.Visible = readMode
        lbl_comment.Visible = readMode
        lbl_website.Visible = readMode
        lbl_mobile.Visible = readMode
        lbl_phone.Visible = readMode
        lbl_fax.Visible = readMode
        lbl_address1.Visible = readMode
        lbl_address2.Visible = readMode
        lbl_city.Visible = readMode
        lbl_state.Visible = readMode
        lbl_zip.Visible = readMode
        lbl_country.Visible = readMode
        lbl_user_id.Visible = readMode
        lbl_password.Visible = readMode
        'lst_how_find_us.Visible = readMode
        ltrl_how_find_us.Visible = readMode
        'lst_business_types.Visible = readMode
        'ltrl_business_types.Visible = readMode
        'lst_industry_types.Visible = readMode
        'ltrl_industry_type.Visible = readMode
        'lst_companies_linked.Visible = readMode
        ltrl_linked_companies.Visible = readMode
        'lbl_is_active.Visible = readMode
        lbl_invite_auction_unsubscribe.Visible = readMode
        If hd_buyer_id.Value > 0 Then
            txt_user_id.Visible = False
            lbl_user_id.Visible = True
        Else
            txt_user_id.Visible = True
            lbl_user_id.Visible = False
        End If

    End Sub
    Private Sub EmptyBasicData()

        txt_company.Text = ""
        ddl_title.Text = ""
        txt_first_name.Text = ""
        txt_last_name.Text = ""
        txt_email.Text = ""
        'txt_confirm_email.Text = ""
        txt_website.Text = ""
        txt_mobile.Text = ""
        txt_phone.Text = ""
        txt_fax.Text = ""
        txt_address1.Text = ""
        txt_address2.Text = ""
        txt_city.Text = ""
        txt_comment.Text = ""
        ' ddl_state.Text = ""
        txt_zip.Text = ""
        ddl_country.Text = ""
        txt_user_id.Text = ""
        lit_change_password.Text = ""
        'txt_retype_password.Text = ""
        ddl_how_find_us.Text = ""
        'chklst_business_types.Text = ""
        'chklst_industry_types.Text = ""
        'chk_is_active.Text=""
        chklst_companies_linked.Text = ""
        'lbl_confirm_email_caption.Text = ""
        'lbl_confirm_email.Text=""
        'lbl_retype_password_caption.Text = ""



        lbl_company.Text = ""
        lbl_title.Text = ""
        lbl_first_name.Text = ""
        lbl_last_name.Text = ""
        lbl_email.Text = ""
        ' lbl_confirm_email.Text=""
        lbl_comment.Text = ""
        lbl_website.Text = ""
        lbl_mobile.Text = ""
        lbl_phone.Text = ""
        lbl_fax.Text = ""
        lbl_address1.Text = ""
        lbl_address2.Text = ""
        lbl_city.Text = ""
        lbl_state.Text = ""
        lbl_zip.Text = ""
        lbl_country.Text = ""
        lbl_user_id.Text = ""
        lbl_password.Text = ""
        'lst_how_find_us.Text=""
        ltrl_how_find_us.Text = ""
        'lst_business_types.Text=""
        'ltrl_business_types.Text = ""
        'lst_industry_types.Text=""
        'ltrl_industry_type.Text = ""
        'lst_companies_linked.Text=""
        ltrl_linked_companies.Text = ""
        'lbl_is_active.Visible = Not (readMode)



    End Sub

    Protected Sub imgbtn_basic_save_Click(sender As Object, e As System.EventArgs) Handles imgbtn_basic_save.Click
       
    End Sub
    Private Function check_is_admin_buyer(ByVal buyer_id As Integer) As Integer
        Dim is_admin_buyer As Integer = 0
        Dim strQuery As String = ""
        strQuery = "if exists(select buyer_user_id from tbl_reg_buyer_users where buyer_id=" & buyer_id.ToString() & ") select 0 else select 1"
        Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)
        If i = 0 Then
            is_admin_buyer = False
        Else
            is_admin_buyer = True
        End If
        Return is_admin_buyer
    End Function
    Private Function validate_user_id(ByVal username As String, ByVal id As Integer) As Integer
        Dim strQuery As String = ""
        If id > 0 Then
            strQuery = "if exists(select user_id from vw_login_users where username='" & username & "' and user_id <> " & id & ") select 1 else select 0"
        Else
            strQuery = "if exists(select user_id from vw_login_users where username='" & username & "') select 1 else select 0"
        End If

        Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)

        Return i
    End Function
    Private Function Validate_email(ByVal email As String, ByVal buyer_id As Integer, ByVal buyer_user_id As Integer) As Integer
        Dim strQuery As String = ""

        If buyer_id > 0 Then
            strQuery = "if exists(select buyer_user_id from tbl_reg_buyer_users where upper(email)=upper('" & email & "') and buyer_id<>" & buyer_id & ") or exists(select user_id from tbl_sec_users where upper(email)=upper('" & email & "')) select 1 else select 0"
        ElseIf buyer_user_id > 0 Then
            strQuery = "if exists(select buyer_user_id from tbl_reg_buyer_users where upper(email)=upper('" & email & "') and buyer_user_id<>" & buyer_user_id & ") or exists(select user_id from tbl_sec_users where upper(email)=upper('" & email & "')) select 1 else select 0"
        Else
            strQuery = "if exists(select buyer_user_id from tbl_reg_buyer_users where upper(email)=upper('" & email & "')) or exists(select user_id from tbl_sec_users where upper(email)=upper('" & email & "')) select 1 else select 0"
        End If

        Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)
        Return i
    End Function

    'Private Sub Update_buyer_business_types(ByVal buyer_id As Integer)
    '    For Each lst As ListItem In chklst_business_types.Items
    '        Dim strQuery As String = ""
    '        Dim i As String = lst.Value
    '        If lst.Selected Then
    '            strQuery = "if not exists(select mapping_id from tbl_reg_buyer_business_type_mapping where buyer_id=" & buyer_id.ToString() & " and business_type_id=" & i & ") insert into tbl_reg_buyer_business_type_mapping(buyer_id,business_type_id) Values(" & buyer_id.ToString() & "," & i & ")"
    '        Else
    '            strQuery = "delete from tbl_reg_buyer_business_type_mapping where buyer_id=" & buyer_id.ToString() & " and business_type_id=" & i
    '        End If

    '        SqlHelper.ExecuteNonQuery(strQuery)
    '    Next
    'End Sub
    'Private Sub Update_buyer_industry_types(ByVal buyer_id As Integer)
    '    For Each lst As ListItem In chklst_industry_types.Items
    '        Dim strQuery As String = ""
    '        Dim i As String = lst.Value
    '        If lst.Selected Then
    '            strQuery = "if not exists(select mapping_id from tbl_reg_buyer_industry_type_mapping where buyer_id=" & buyer_id.ToString() & " and industry_type_id=" & i & ") insert into tbl_reg_buyer_industry_type_mapping(buyer_id,industry_type_id) Values(" & buyer_id.ToString() & "," & i & ")"
    '        Else
    '            strQuery = "delete from tbl_reg_buyer_industry_type_mapping where buyer_id=" & buyer_id.ToString() & " and industry_type_id=" & i
    '        End If
    '        SqlHelper.ExecuteNonQuery(strQuery)
    '    Next
    'End Sub
    Private Sub Update_buyer_companies_linked(ByVal buyer_id As Integer)
        For Each lst As ListItem In chklst_companies_linked.Items
            Dim strQuery As String = ""
            Dim i As String = lst.Value
            If lst.Selected Then
                strQuery = "if not exists(select mapping_id from tbl_reg_buyer_seller_mapping where buyer_id=" & buyer_id.ToString() & " and seller_id=" & i & ") insert into tbl_reg_buyer_seller_mapping(buyer_id,seller_id) Values(" & buyer_id.ToString() & "," & i & ")"
            Else
                strQuery = "delete from tbl_reg_buyer_seller_mapping where buyer_id=" & buyer_id.ToString() & " and seller_id=" & i
            End If
            SqlHelper.ExecuteNonQuery(strQuery)
        Next
    End Sub
    Private Sub Update_buyer_how_find_us(ByVal buyer_id As Integer)
        Dim strQuery As String = ""
        If ddl_how_find_us.SelectedValue <> "" Then

            strQuery = "if not exists(select assignment_id from tbl_reg_buyer_find_us_assignment where buyer_id=" & buyer_id.ToString() & " and how_find_us_id=" & ddl_how_find_us.SelectedValue & ") insert into tbl_reg_buyer_find_us_assignment(buyer_id,how_find_us_id) Values(" & buyer_id.ToString() & "," & ddl_how_find_us.SelectedValue & ")"
        Else
            strQuery = "delete from tbl_reg_buyer_find_us_assignment where buyer_id=" & buyer_id.ToString() & " and how_find_us_id=" & ddl_how_find_us.SelectedValue
        End If

        SqlHelper.ExecuteNonQuery(strQuery)

    End Sub

    Protected Sub imgbtn_basic_edit_Click(sender As Object, e As System.EventArgs) Handles imgbtn_basic_edit.Click
        visibleBasicData(False)
        rfv_user_id.Visible = False

    End Sub

    Protected Sub imgbtn_basic_cancel_Click(sender As Object, e As System.EventArgs) Handles imgbtn_basic_cancel.Click
        visibleBasicData(True)

    End Sub

    Protected Sub imgbtn_basic_update_Click(sender As Object, e As System.EventArgs) Handles imgbtn_basic_update.Click
        If Page.IsValid Then

            Dim i As Integer = 0
            i = Validate_email(txt_email.Text.Trim, hd_buyer_id.Value, 0)
            If i = 1 Then
                lblMessage.Text = "Email is already in our record."
                Exit Sub
            End If

            Dim strQuery As String = ""
            strQuery = "UPDATE tbl_reg_buyers SET " & _
                       "company_name ='" & txt_company.Text.Trim() & "' " & _
                       ",contact_title ='" & ddl_title.SelectedValue & "' " & _
                       ",contact_first_name ='" & txt_first_name.Text.Trim() & "' " & _
                       ",contact_last_name = '" & txt_last_name.Text.Trim() & "' " & _
                       ",email ='" & txt_email.Text.Trim() & "' " & _
                       ",website = '" & txt_website.Text.Trim() & "' " & _
                      ",phone ='" & txt_phone.Text.Trim() & "' " & _
                      ",mobile ='" & txt_mobile.Text.Trim() & "' " & _
                      ",fax = '" & txt_fax.Text.Trim() & "' " & _
                      ",address1 ='" & txt_address1.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "' " & _
                      ",address2 ='" & txt_address2.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "' " & _
                      ",city ='" & txt_city.Text.Trim() & "' " & _
                       ",state_id =0" & _
                      ",state_text='" & txt_other_state.Text.Trim & "' " & _
                      ",zip ='" & txt_zip.Text.Trim() & "' " & _
                      ",country_id =" & ddl_country.SelectedItem.Value & _
                      ",is_phone1_mobile =" & IIf(rdo_ph_1_landline.Checked, 0, 1) & _
                      ",is_phone2_mobile =" & IIf(rdo_ph_2_landline.Checked, 0, 1) & _
                      ",comment ='" & txt_comment.Text.Trim.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>").Replace("'", "`") & "'" & _
                       " WHERE buyer_id = " & hd_buyer_id.Value & " select 1"
            ' txt_company.Text = strQuery
            Dim j As Integer = SqlHelper.ExecuteScalar(strQuery)

            If j = 1 Then
                'Dim is_admin_buyer As Integer = check_is_admin_buyer(hd_buyer_user_id.Value)
                strQuery = "update tbl_reg_buyer_users set first_name='" & txt_first_name.Text.Trim() & "',last_name='" & txt_last_name.Text.Trim() & "',title='" & ddl_title.SelectedValue & "',email='" & txt_email.Text.Trim() & "',submit_by_user_id=" & hd_buyer_user_id.Value & " where buyer_user_id=" & hd_buyer_user_id.Value
                SqlHelper.ExecuteNonQuery(strQuery)
                Update_buyer_how_find_us(hd_buyer_id.Value)
                'Update_buyer_business_types(hd_buyer_id.Value)
                'Update_buyer_industry_types(hd_buyer_id.Value)
                Update_buyer_companies_linked(hd_buyer_id.Value)
                If hid_unsubscribe_invite.Value <> chk_invite_auction.Checked Then
                    update_invitation_unsubscription(hd_buyer_id.Value)
                End If
                lblMessage.Text = "Bidder's basic information updated successfully.<br/><br/>"
                BasicTabEdit(True)

                'set_button_visibility("View")
            Else
                lblMessage.Text = "Error in updating. Please contact administrator.<br/><br/>"
            End If
            ' lbl_show.Text = "valid"
        Else
            'lbl_show.Text = "not valid"
        End If
    End Sub
    Private Sub update_invitation_unsubscription(ByVal buyer_id As Integer)
        Dim strQuery As String = ""
        If chk_invite_auction.Checked Then
            strQuery = "Declare @email varchar(50) set @email=(select email from tbl_reg_buyers WITH (NOLOCK) where buyer_id=" & buyer_id & ") if not exists(select unsubscription_id from tbl_unsubscription where email=@email and type=1)Begin Insert into tbl_unsubscription(email,type)Values(@email,1) select 1 End else Begin select 0 End"
        Else
            strQuery = "Declare @email varchar(50) set @email=(select email from tbl_reg_buyers WITH (NOLOCK) where buyer_id=" & buyer_id & ") Delete from tbl_unsubscription where email=@email and type=1 select 1"
        End If
        Dim status As Integer = SqlHelper.ExecuteScalar(strQuery)
    End Sub
#End Region

#Region "Sub-Logins"

    Protected Sub rad_grid_sublogins_InsertCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditFormInsertItem of the RadGrid        
        Dim insertedItem As GridEditFormInsertItem = DirectCast(e.Item, GridEditFormInsertItem)
        TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = ""
        Dim buyer_id As Integer = hd_buyer_id.Value
        Dim first_name As String = "" ' (TryCast(insertedItem("first_name").Controls(0), TextBox)).Text
        Dim last_name As String = "" '(TryCast(insertedItem("last_name").Controls(0), TextBox)).Text
        Dim txt_first_name As TextBox = TryCast(insertedItem.FindControl("txt_sub_first_name"), TextBox)
        Dim txt_email As TextBox = TryCast(insertedItem.FindControl("txt_sub_email"), TextBox)
        Dim txt_last_name As TextBox = TryCast(insertedItem.FindControl("txt_sub_last_name"), TextBox)
        first_name = txt_first_name.Text.Trim()
        last_name = txt_last_name.Text.Trim()
        Dim txt_password As TextBox = TryCast(insertedItem.FindControl("txt_sub_password"), TextBox)
        Dim title As String = TryCast(insertedItem.FindControl("dd_sub_title"), DropDownList).SelectedValue '(TryCast(insertedItem("title").Controls(0), TextBox)).Text
        Dim username As String = (TryCast(insertedItem.FindControl("txt_sub_username"), TextBox)).Text.Trim
        Dim password As String = txt_password.Text.Trim() '(TryCast(insertedItem("password").Controls(0), TextBox)).Text
        Dim bidding_limit As String = TryCast(insertedItem.FindControl("txt_sub_bidding_limit"), TextBox).Text.Trim()
        ' Dim bidding_limit As String = (TryCast(insertedItem("bidding_limit").Controls(0), TextBox)).Text
        ' Dim rdolst_is_active As RadioButtonList = TryCast(insertedItem.FindControl("rdo_is_active"), RadioButtonList)
        Dim chk_is_active As CheckBox = TryCast(insertedItem.FindControl("chk_sub_is_active"), CheckBox)
        Dim is_active As Integer = 0
        If chk_is_active.Checked Then
            is_active = 1
        Else
            is_active = 0
        End If
        Try
            Dim strError As String = ""
            If first_name = "" Then
                strError = "First name required."
                ' ElseIf title = "" Then
                ' strError = "Title required."
            ElseIf username = "" Then
                strError = "Username required."
            ElseIf password = "" Then
                strError = "Password required."
            ElseIf password.Length < 6 Then
                strError = "Password must be atleast 6 characters."
            ElseIf bidding_limit = "" Then
                strError = "Bidding limit required."
            End If
            Dim dblBidding_limit As Double = 0
            If strError <> "" Then
                TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>" & strError & "</font>"
                e.Canceled = True
                Exit Sub
            Else
                If Not Double.TryParse(bidding_limit, dblBidding_limit) Then
                    TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Invalid bidding limit.</font>"

                    e.Canceled = True
                    Exit Sub
                Else
                    If hid_buyer_max_bid_amount.Value < dblBidding_limit Then
                        TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>You can not set Bidding Limit more than your allowed limit</font>"
                        e.Canceled = True
                        Exit Sub
                    End If
                End If
            End If

            If validate_user_id(username, 0) = 1 Then
                TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Username already exists.</font>"

                e.Canceled = True
            ElseIf Validate_email(txt_email.Text.Trim, 0, 0) = 1 Then
                TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Email already exists.</font>"
                e.Canceled = True
            Else
                Dim buyer_user_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_reg_buyer_users(buyer_id, first_name, last_name,title,username,password,bidding_limit,is_active,submit_date,submit_by_user_id,is_admin,email) VALUES (" & buyer_id & ", '" & first_name & "','" & last_name & "','" & title & "','" & username & "','" & Security.EncryptionDecryption.EncryptValue(password) & "'," & bidding_limit & "," & is_active & ",getdate(), " & hd_buyer_user_id.Value & ",0,'" & txt_email.Text.Trim & "')   select scope_identity()")
                Dim objEmail As New Email()
                objEmail.send_bidder_sub_login_creation_email(buyer_user_id)
                objEmail = Nothing
            End If
        Catch ex As Exception
            TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Unable to add Sub-Login. Reason: " + ex.Message & "</font>"

            e.Canceled = True
        End Try

    End Sub

    'Protected Sub rad_grid_sublogins_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles rad_grid_sublogins.ItemCommand

    '    If e.CommandName = RadGrid.InitInsertCommandName Then '"Add new" button clicked

    '        Dim editColumn As GridEditCommandColumn = CType(rad_grid_sublogins.MasterTableView.GetColumn("EditCommandColumn"), GridEditCommandColumn)
    '        editColumn.Visible = False

    '        'ElseIf (e.CommandName = RadGrid.EditCommandName And TypeOf e.Item Is GridEditFormItem) Then
    '        '    e.Canceled = True

    '    ElseIf (e.CommandName = RadGrid.RebindGridCommandName AndAlso e.Item.OwnerTableView.IsItemInserted) Then
    '        e.Canceled = True

    '    Else
    '        Dim editColumn As GridEditCommandColumn = CType(rad_grid_sublogins.MasterTableView.GetColumn("EditCommandColumn"), GridEditCommandColumn)
    '        If Not editColumn.Visible Then
    '            editColumn.Visible = True
    '        End If

    '    End If
    '    If (e.CommandName = RadGrid.EditCommandName And TypeOf e.Item Is GridEditFormItem) Then

    '        Dim item As GridEditFormItem = CType(e.Item, GridEditFormItem)
    '        CType(item.FindControl("txt_sub_username"), TextBox).Enabled = False
    '    End If
    'End Sub

    'Protected Sub rad_grid_sublogins_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles rad_grid_sublogins.NeedDataSource
    '    If Not String.IsNullOrEmpty(hd_buyer_id.Value.ToString) Then
    '        Dim strQuery As String = ""
    '        strQuery = "SELECT " & _
    '        "buyer_user_id," & _
    '        "ISNULL(A.buyer_id, 0) AS buyer_id," & _
    '        "ISNULL(A.first_name, '') + ' ' + ISNULL(A.last_name, '') AS name," & _
    '        "ISNULL(A.first_name, '') AS first_name," & _
    '        "ISNULL(A.last_name, '') AS last_name," & _
    '        "title," & _
    '        "ISNULL(A.username, '') AS username," & _
    '        "ISNULL(A.password, '') AS password," & _
    '        "ISNULL(A.email, '') AS email," & _
    '        "Convert(decimal(10,2),ISNULL(A.bidding_limit, 0)) AS bidding_limit," & _
    '        "ISNULL(A.is_active, 0) AS is_active" & _
    '        " FROM tbl_reg_buyer_users A where buyer_id=" & hd_buyer_id.Value & " and is_admin=0 order by buyer_user_id DESC"
    '        rad_grid_sublogins.DataSource = SqlHelper.ExecuteDatatable(strQuery)
    '        rptItems.DataSource = SqlHelper.ExecuteDatatable(strQuery)
    '        rptItems.DataBind()
    '    End If
    'End Sub

    'Protected Sub rad_grid_sublogins_UpdateCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
    '    'Get the GridEditableItem of the RadGrid   

    '    Dim editedItem As GridEditFormItem = TryCast(e.Item, GridEditFormItem)
    '    'Get the primary key value using the DataKeyValue.        
    '    TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = ""
    '    Dim buyer_user_id As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("buyer_user_id").ToString()
    '    Dim first_name As String = ""
    '    Dim last_name As String = ""
    '    Dim old_active_status As Integer = 0

    '    Dim txt_first_name As TextBox = TryCast(editedItem.FindControl("txt_sub_first_name"), TextBox)
    '    Dim txt_last_name As TextBox = TryCast(editedItem.FindControl("txt_sub_last_name"), TextBox)
    '    Dim hid_old_active_status As HiddenField = TryCast(editedItem.FindControl("hid_old_active_status"), HiddenField)
    '    old_active_status = hid_old_active_status.Value
    '    first_name = txt_first_name.Text.Trim()
    '    last_name = txt_last_name.Text.Trim()
    '    Dim txt_email As TextBox = TryCast(editedItem.FindControl("txt_sub_email"), TextBox)
    '    Dim title As String = TryCast(editedItem.FindControl("dd_sub_title"), DropDownList).SelectedValue

    '    Dim txt_password As TextBox = TryCast(editedItem.FindControl("txt_sub_password"), TextBox)

    '    Dim password As String = txt_password.Text.Trim()
    '    Dim bidding_limit As String = TryCast(editedItem.FindControl("txt_sub_bidding_limit"), TextBox).Text.Trim()
    '    Dim chk_is_active As CheckBox = TryCast(editedItem.FindControl("chk_sub_is_active"), CheckBox)
    '    Dim is_active As Integer = 0

    '    If chk_is_active.Checked Then
    '        is_active = 1
    '    Else
    '        is_active = 0
    '    End If
    '    Try
    '        Dim strError As String = ""
    '        If first_name = "" Then
    '            strError = "First name required."
    '            ' ElseIf title = "" Then
    '            ' strError = "Title required."
    '        ElseIf password = "" Then
    '            strError = "Password required."
    '        ElseIf bidding_limit = "" Then
    '            strError = "Bidding limit required."
    '        ElseIf password.Length < 6 Then
    '            strError = "Password must be atleast 6 characters."
    '        End If
    '        Dim dblBidding_limit As Double = 0
    '        If strError <> "" Then
    '            TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>" & strError & "</font>"
    '            e.Canceled = True
    '            Exit Try
    '        Else
    '            If Not Double.TryParse(bidding_limit, dblBidding_limit) Then
    '                TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Invalid bidding limit.</font>"
    '                e.Canceled = True
    '                Exit Try
    '            Else
    '                If hid_buyer_max_bid_amount.Value < dblBidding_limit Then
    '                    TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>You can not set Bidding Limit more than your allowed limit</font>"
    '                    e.Canceled = True
    '                    Exit Try
    '                End If
    '            End If
    '        End If

    '        If Validate_email(txt_email.Text.Trim, 0, buyer_user_id) = 1 Then
    '            TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Email already exists.</font>"
    '            e.Canceled = True
    '        Else
    '            Dim old_pwd As String = SqlHelper.ExecuteScalar("select password from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id)
    '            old_pwd = Security.EncryptionDecryption.DecryptValue(old_pwd)
    '            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set first_name='" & first_name & "',last_name='" & last_name & "',title='" & title & "',password='" & Security.EncryptionDecryption.EncryptValue(password) & "',bidding_limit=" & bidding_limit & ",is_active=" & is_active & ",email='" & txt_email.Text.Trim & "' where buyer_user_id=" & buyer_user_id)
    '            Dim objEmail As New Email
    '            If password <> old_pwd Then
    '                objEmail.Send_ChangePassword_Mail(txt_email.Text.Trim, first_name)
    '            End If
    '            If is_active <> old_active_status Then
    '                'send email
    '                objEmail.send_bidder_sub_login_active_status_changed_email(buyer_user_id)
    '            End If
    '            objEmail = Nothing
    '        End If

    '    Catch ex As Exception
    '        TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Unable to add Sub-Login. Reason: " + ex.Message & "</font>"
    '        e.Canceled = True
    '    End Try
    'End Sub
    'Protected Sub rad_grid_sublogins_DeleteCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles rad_grid_sublogins.DeleteCommand
    '    Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
    '    Dim buyer_user_id As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("buyer_user_id").ToString()
    '    Dim buyer_id As String = item("buyer_id").Text
    '    Try
    '        SqlHelper.ExecuteNonQuery("DELETE from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id & "; DELETE from tbl_reg_buyers where buyer_id=" & buyer_id & "")
    '        'SqlHelper.ExecuteNonQuery("DELETE from tbl_reg_buyers where buyer_id='" & buyer_id & "'")
    '    Catch ex As Exception
    '        rad_grid_sublogins.Controls.Add(New LiteralControl("Unable to delete Sub-Login. Reason: " + ex.Message))
    '        e.Canceled = True
    '    End Try

    'End Sub

    'Protected Sub rad_grid_sublogins_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles rad_grid_sublogins.ItemDataBound
    '    Dim is_active As Boolean = False
    '    If TypeOf e.Item Is GridEditableItem AndAlso e.Item.IsInEditMode Then
    '        Dim dataItem As GridEditableItem = CType(e.Item, GridEditableItem)
    '        Dim txt_password As TextBox = DirectCast(dataItem.FindControl("txt_sub_password"), TextBox)
    '        Dim hid_password As HiddenField = DirectCast(dataItem.FindControl("hid_sub_password"), HiddenField)
    '        Dim txt_username As TextBox = DirectCast(dataItem.FindControl("txt_sub_username"), TextBox)
    '        Dim txt_email As TextBox = TryCast(dataItem.FindControl("txt_sub_email"), TextBox)
    '        txt_password.Attributes.Add("value", hid_password.Value)

    '    End If
    'End Sub

    Private Function validate_sub_login_id(ByVal sub_login_username As String) As Integer
        Dim strQuery As String = ""
        strQuery = "if exists (select user_id from vw_login_users where username='" & sub_login_username & "') select 1 else select 0"
        Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)
        Return i
    End Function

    Protected Function checked(ByVal r As Object) As Boolean
        If r Is DBNull.Value Then
            Return True
        ElseIf r = True Then
            Return True
        Else
            Return False
        End If
    End Function

#End Region



#Region "Personal-Info"
    Private Sub fillSubInfoTab(ByVal buyer_user_id As Integer)
        If buyer_user_id = 0 Or hd_is_buyer.Value = 1 Then
            pnl_buyer_user.Visible = False
            Exit Sub
        End If
         Dim dt As DataTable = New DataTable()
        Dim strQuery As String = "SELECT " & _
            "buyer_user_id," & _
            "ISNULL(A.buyer_id, 0) AS buyer_id," & _
            "ISNULL(A.first_name, '') AS first_name," & _
            "ISNULL(A.last_name, '') AS last_name," & _
            "ISNULL(A.title, '') AS title," & _
            "ISNULL(A.username, '') AS username," & _
            "ISNULL(A.password, '') AS password," & _
            "ISNULL(A.email, '') AS email," & _
            "Convert(decimal(10,2),ISNULL(A.bidding_limit, 0)) AS bidding_limit," & _
            "ISNULL(A.is_active, 0) AS is_active" & _
            " FROM tbl_reg_buyer_users A WITH (NOLOCK) where buyer_user_id=" & buyer_user_id & " order by buyer_user_id DESC"
        dt = SqlHelper.ExecuteDatatable(strQuery)
        If dt.Rows.Count > 0 Then
            txt_sub_first.Text = dt.Rows(0)("first_name")
            txt_sub_last.Text = dt.Rows(0)("last_name")
            If Not dd_sub_title.Items.FindByText(dt.Rows(0)("title")) Is Nothing Then
                dd_sub_title.ClearSelection()
                dd_sub_title.Items.FindByText(dt.Rows(0)("title")).Selected = True
            End If

            txt_sub_email.Text = dt.Rows(0)("email")
            lit_sub_change_password.Text = "<a style='font-weight:normal' href='javascript:void(0);' onclick=""return open_pass_win('/change_password.aspx','b=0&i=" & buyer_user_id & "&o=1');"">Click to Change Password</a>"
            lbl_sub_first.Text = dt.Rows(0)("first_name")
            lbl_sub_last.Text = dt.Rows(0)("last_name")
            lbl_sub_title.Text = dt.Rows(0)("title")
            lbl_sub_password.Text = "******"
            lbl_sub_username.Text = dt.Rows(0)("username")
            lbl_sub_email.Text = "<a href='mailto:" & dt.Rows(0)("email") & "'>" & dt.Rows(0)("email") & "</a>"
            setvisible_subinfo(True)
        End If
        dt.Dispose()
    End Sub

    Protected Sub setvisible_subinfo(ByVal readmode As Boolean)

        If readmode Then
            'imgbtn_sub_edit.Visible = True
            imgbtn_sub_update.Visible = False
            div_sub_info.Visible = False
        Else
            imgbtn_sub_edit.Visible = False

            If hd_is_buyer.Value = 1 Then
                imgbtn_sub_update.Visible = False
                div_sub_info.Visible = False
            Else
                imgbtn_sub_update.Visible = True
                div_sub_info.Visible = True
            End If
        End If

        txt_sub_first.Visible = Not readmode
        txt_sub_last.Visible = Not readmode
        dd_sub_title.Visible = Not readmode
        txt_sub_email.Visible = Not readmode
        lit_sub_change_password.Visible = Not readmode
        span_sub_first.Visible = Not readmode
        'span_sub_last.Visible = Not readmode
        'span_sub_title.Visible = Not readmode
        span_sub_password.Visible = Not readmode
        span_sub_email.Visible = Not readmode

        lbl_sub_first.Visible = readmode
        lbl_sub_last.Visible = readmode
        lbl_sub_title.Visible = readmode
        lbl_sub_password.Visible = readmode
        lbl_sub_email.Visible = readmode

    End Sub
    Protected Sub imgbtn_sub_edit_Click(sender As Object, e As System.EventArgs) Handles imgbtn_sub_edit.Click
        setvisible_subinfo(False)
    End Sub

    Protected Sub imgbtn_sub_cancel_Click(sender As Object, e As System.EventArgs) Handles imgbtn_sub_cancel.Click
        setvisible_subinfo(True)
    End Sub

    Protected Sub imgbtn_sub_update_Click(sender As Object, e As System.EventArgs) Handles imgbtn_sub_update.Click
        If Page.IsValid Then

            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set first_name='" & txt_sub_first.Text.Trim & "',last_name='" & txt_sub_last.Text.Trim & "',title='" & dd_sub_title.SelectedValue & "',email='" & txt_sub_email.Text.Trim & "' where buyer_user_id=" & hd_buyer_user_id.Value)
            fillSubInfoTab(hd_buyer_user_id.Value)
            setvisible_subinfo(True)

        End If
    End Sub

#End Region

    Protected Sub rptItems_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptItems.ItemCommand
        If e.CommandName = "delete" Then
            'Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Dim buyer_user_id As String = e.CommandArgument.ToString
            'Dim buyer_id As String = item("buyer_id").Text()
            Try
                SqlHelper.ExecuteNonQuery("DELETE from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id)
                bind_login()
            Catch ex As Exception
                'rad_grid_sublogins.Controls.Add(New LiteralControl("Unable to delete Sub-Login. Reason: " + ex.Message))
                'e.Canceled = True
            End Try
        End If
    End Sub

    Protected Sub btn_refresh_buckets_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_refresh_buckets.Click
        ltrl_buckets.Text = get_buyer_bucket_string(hd_buyer_id.Value)
    End Sub
End Class
