﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MyQueries.aspx.vb" Inherits="MyQueries" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>My auction queries</title>
    <link type="text/css" rel="Stylesheet" href="/Style/master_menu.css" />
    <link rel="stylesheet" type="text/css" href="/Style/menu.css" />
    <link type="text/css" rel="Stylesheet" href="/Style/pcs.css" />
</head>
<body style="background-image: none;">
    <form id="form1" runat="server">
    <asp:HiddenField ID="hd_buyer_id" runat="server" Value="0" />
    <asp:HiddenField ID="hd_buyer_user_id" runat="server" Value="0" />
    <asp:HiddenField ID="hd_is_buyer" runat="server" Value="0" />
    <asp:Label ID="lbl_test" runat="server"></asp:Label>
    <div>
        <table cellpadding="0" cellspacing="2" border="0" width="100%">
            <tr>
                <td>
                    <div style="float: left; margin: 10px;">
                        <div class="pageheading">
                            <asp:Label runat="server" ID="lbl_page_heading" meta:resourcekey="lbl_page_heading_my_queries"></asp:Label>
                        </div>
                        <div style="float: right; color: Blue; padding-right: 20px;">
                            <asp:LinkButton ID="lnk_all" runat="server" Text="Show All" />
                            |
                            <asp:LinkButton ID="lnk_pending" runat="server" Text="Pending" />
                            |
                            <asp:LinkButton ID="lnk_ans" runat="server" Text="Answered" /></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow: auto; padding: 10px 5px 5px 20px; text-align: left;">
                        <asp:Label ID="lbl_no_query" runat="server"></asp:Label>
                        <asp:Repeater ID="rep_after_queries" runat="server">
                            <ItemTemplate>
                                <div class="qryTitle">
                                    <%# Container.ItemIndex + 1 & ". " & Eval("title")%>
                                </div>
                                <div class="qryAlt" style="padding-left: 16px;">
                                    <%# Eval("auction_title")%>
                                    <%# IIf(Eval("auction_title").ToString().Trim() = "", "Asked to: " & Eval("ask_to"), "")%>
                                </div>
                                <div style="padding-left: 16px;padding-top: 5px;">
                                    <%# Eval("message")%>
                                   
                                </div>
                                <div style="padding: 20px;">
                                    <asp:Repeater ID="rep_inner_after_queries" runat="server">
                                        <ItemTemplate>
                                            <div class="qryDesc">
                                                <img src='<%# IIF(Eval("image_path")<>"","/upload/users/" & Eval("user_id") & "/" & Eval("image_path"),"/images/buyer_icon.gif") %>'
                                                    alt="" style="float: left; padding: 2px; height: 23px; width: 22px;" />
                                                <%#Eval("message") %>
                                            </div>
                                            <div class="qryAlt">
                                                <%# IIf(Eval("buyer_title") = "", Eval("title"), Eval("buyer_title"))%>
                                                <%# IIf(Eval("first_name") = "", Eval("buyer_first_name") & " " & Eval("buyer_last_name"), Eval("first_name") & " " & Eval("last_name"))%>,
                                                <%# Eval("submit_date")%>
                                            </div>
                                            <br />
                                            <br />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
