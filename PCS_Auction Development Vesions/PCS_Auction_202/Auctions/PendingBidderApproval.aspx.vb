﻿Imports Telerik.Web.UI
Imports System.Drawing
Partial Class Auctions_BidderApproval
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            filldata()
        End If
    End Sub
    Protected Sub filldata()
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            Dim ds2 As DataSet
            Dim strQuery As String = ""
            If CommonCode.is_admin_user() Then
                strQuery = "select B.buyer_id,isnull(B.status_id,0) as status_id,(B.contact_first_name +' ' + B.contact_last_name) as name,B.company_name as company_name,B.phone,isnull(S.status,'') as status " & _
               "from tbl_reg_buyers B left join tbl_reg_buyser_statuses S on B.status_id=S.status_id where B.status_id=1"
            Else
                ' strQuery = "select B.buyer_id,isnull(B.status_id,0) as status_id,(B.contact_first_name +' ' + B.contact_last_name) as name,B.company_name as company_name,B.phone,isnull(S.status,'') as status " & _
                '"from tbl_reg_buyers B left join tbl_reg_buyser_statuses S on B.status_id=S.status_id INNER JOIN tbl_reg_buyer_seller_mapping M ON B.buyer_id=M.buyer_id INNER JOIN tbl_reg_sellers S ON M.seller_id=S.seller_id INNER JOIN tbl_reg_seller_user_mapping UM ON S.seller_id=UM.seller_id " & _
                '" where B.status_id=1 and UM.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
                strQuery = "select B.buyer_id,isnull(B.status_id,0) as status_id,(B.contact_first_name +' ' + B.contact_last_name) as name,B.company_name as company_name,B.phone,isnull(ST.status,'') as status " & _
              "from tbl_reg_buyers B left join tbl_reg_buyser_statuses ST on B.status_id=ST.status_id INNER JOIN tbl_reg_buyer_seller_mapping M ON B.buyer_id=M.buyer_id INNER JOIN tbl_reg_sellers S ON M.seller_id=S.seller_id INNER JOIN tbl_reg_seller_user_mapping UM ON S.seller_id=UM.seller_id " & _
              " where B.status_id=1 and UM.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")

            End If
            ds2 = SqlHelper.ExecuteDataset(strQuery)
            Me.RadGrid2.DataSource = ds2
            Me.RadGrid2.DataBind()
            If RadGrid2.Items.Count = 0 Then
                but_basic_update.Visible = False
            End If
            ds2.Dispose()
            ds2 = Nothing
        End If
       
    End Sub
    Protected Sub but_basic_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_update.Click
         If RadGrid2.Items.Count > 0 Then

            For i = 0 To RadGrid2.Items.Count - 1
                Dim hd_status As HiddenField = CType(RadGrid2.Items(i).FindControl("hid_grd_status_id"), HiddenField)
                Dim dd_status As String = CType(RadGrid2.Items(i).FindControl("ddl_action"), DropDownList).SelectedValue

                If hd_status.Value.ToString <> dd_status Then
                    update_buyer_approval_status(dd_status, RadGrid2.Items(i)("buyer_id").Text)
                End If
            Next
            RadGrid2.Rebind()

            'End If
        End If
    End Sub
    Private Sub update_buyer_approval_status(ByVal status_id As Integer, ByVal buyer_id As Integer)

        Dim status As String = SqlHelper.ExecuteScalar("select status from tbl_reg_buyser_statuses where status_id=" & status_id)

        Dim objEmail As New Email()
        If status_id = 2 Then
            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set is_active=1 where is_admin=1 and buyer_id=" & buyer_id)
            objEmail.send_buyer_approval_email(buyer_id)
        Else
            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set is_active=0 where is_admin=1 and buyer_id=" & buyer_id)
            objEmail.send_buyer_status_changed_email(buyer_id, status)
        End If
        objEmail = Nothing

        Dim strQuery As String = "UPDATE tbl_reg_buyers SET status_id=" & status_id & ", approval_status ='" & status & "',approval_status_date =getdate(),approval_status_by_user_id =" & CommonCode.Fetch_Cookie_Shared("user_id") & " WHERE buyer_id =" & buyer_id
        SqlHelper.ExecuteNonQuery(strQuery)

    End Sub
    
    Protected Sub RadGrid2_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles RadGrid2.PageIndexChanged
        RadGrid2.CurrentPageIndex = e.NewPageIndex
        filldata()
    End Sub

    Protected Sub RadGrid2_SortCommand(sender As Object, e As Telerik.Web.UI.GridSortCommandEventArgs) Handles RadGrid2.SortCommand
        filldata()
    End Sub
End Class
