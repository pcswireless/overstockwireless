﻿Imports Telerik.Web.UI
Imports System.Drawing
Partial Class Auctions_AuctionBulkListing
    Inherits System.Web.UI.Page
    Private Sub setPermission()

        If CommonCode.is_super_admin() Then
            RadGrid1.MasterTableView.Columns.FindByUniqueName("EditCommandColumn").Visible = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
        Else
            Dim View_Auction_Bulk_Listing As Boolean = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 3
                        RadGrid1.MasterTableView.Columns.FindByUniqueName("EditCommandColumn").Visible = True
                    Case 5
                        View_Auction_Bulk_Listing = True
                    Case 35
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
                End Select
            Next
            dt = Nothing

            If View_Auction_Bulk_Listing = False Then Response.Redirect("/NoPermission.aspx")

        End If
    End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Protected Sub RadGrid1_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrid1.ItemCommand
        If e.CommandName = RadGrid.EditCommandName Then
           
        End If
    End Sub

    Protected Sub RadGrid1_ItemCreated(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid1.ItemCreated
        'Dim myCombobox As RadComboBox

        'If TypeOf e.Item Is GridEditFormItem AndAlso e.Item.IsInEditMode Then
        '    Dim EditFormItem As GridEditFormItem = DirectCast(e.Item, GridEditFormItem)
        '    myCombobox = New RadComboBox
        '    myCombobox = DirectCast(EditFormItem.FindControl("RadCombo_stock_location_inner"), RadComboBox)
        '    ' myCombobox.SelectedValue = IIf(CType(e.Item.DataItem, DataRowView).Item("stock_location_id").ToString <> "0", CType(e.Item.DataItem, DataRowView).Item("stock_location_id"), Nothing)
        '    'hid_location_combo_id.Value = myCombobox.ClientID
        'End If
    End Sub
    Protected Sub RadGrid1_ItemUpdated(ByVal source As Object, ByVal e As Telerik.Web.UI.GridUpdatedEventArgs) Handles RadGrid1.ItemUpdated
        Dim item As GridEditableItem = DirectCast(e.Item, GridEditableItem)
        Dim id As String = item.GetDataKeyValue("auction_id").ToString()

        If Not e.Exception Is Nothing Then
            e.KeepInEditMode = True
            e.ExceptionHandled = True
            SetMessage("Auction with ID " + id + " cannot be updated. Reason: " + e.Exception.Message)
        Else
            SetMessage("Auction with ID " + id + " is updated!")
        End If
    End Sub
    Private Sub DisplayMessage(ByVal text As String)
        RadGrid1.Controls.Add(New LiteralControl(String.Format("<span style='color:red'>{0}</span>", text)))
    End Sub
    Private Sub SetMessage(ByVal message As String)
        gridMessage = message
    End Sub

    Private gridMessage As String = Nothing

   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' If Not Page.IsPostBack Then
        If CommonCode.is_admin_user() Then
            SqlDataSource1.SelectCommand = "select isnull(A.auction_id,0) as auction_id,ISNULL(A.code, '') AS code,ISNULL(A.title, '') AS title,dbo.auction_status(A.auction_id) as Status,isnull(A.no_of_clicks,0) as no_of_clicks,t.name as auction_type,isnull(A.stock_location_id,0) as stock_location_id,isnull(s.name,'') as stock_location from tbl_auctions A WITH (NOLOCK) left join tbl_auction_types as t on A.auction_type_id=t.auction_type_id left join tbl_master_stock_locations s on A.stock_location_id=s.stock_location_id"
        End If
        hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
        '  End If
    End Sub
    Protected Sub RadGrid1_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid1.PreRender
        Dim menu As GridFilterMenu = RadGrid1.FilterMenu
      
        Dim i As Integer = 0
        While i < menu.Items.Count
            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
        If (Not Page.IsPostBack) Then
            RadGrid1.MasterTableView.FilterExpression = "([Status] Like '%Running%') "
            Dim column As GridColumn = RadGrid1.MasterTableView.GetColumnSafe("status")
            column.CurrentFilterFunction = GridKnownFunction.Contains
            column.CurrentFilterValue = "Running"
            RadGrid1.MasterTableView.Rebind()
        End If
    End Sub

    Protected Sub RadCombo_stock_location_ItemDataBound(sender As Object, e As Telerik.Web.UI.RadComboBoxItemEventArgs)
        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("name").ToString()
        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("stock_location_id").ToString()

    End Sub
    Protected Sub RadCombo_stock_location_ItemsRequested(sender As Object, e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs)

        Dim sqlSelectCommand As String = "SELECT stock_location_id, name, city,state from tbl_master_stock_locations WHERE name LIKE '" & e.Text & "%' ORDER BY name"
        Dim ddl_stock_location As RadComboBox = DirectCast(sender, RadComboBox)
        'Dim dt As New DataTable()
        radcombo_stock.SelectCommand = sqlSelectCommand
        ddl_stock_location.DataSourceID = "radcombo_stock"
        ' load_stock_location(e.Text)
    End Sub
   
End Class
