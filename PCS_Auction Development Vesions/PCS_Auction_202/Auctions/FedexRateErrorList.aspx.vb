﻿
Partial Class Auctions_FedexRateErrorList
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            BindGrid()
        End If

    End Sub

    Protected Sub BindGrid()
            Dim auction_id As Integer = 281
             Dim dt As DataTable
            dt = SqlHelper.ExecuteDatatable("select A.buyer_id As BuyerId,ISNULL(A.address1, '') + ' ' + ISNULL(A.address2, '')  AS Address, ISNULL(A.city, '') AS City,ISNULL(A.state_text, 0) AS StateCode, ISNULL(A.zip, '') AS Zip, ISNULL(C.CODE, 0) AS CountryCode from tbl_reg_buyers A inner join tbl_master_countries C on A.country_id=C.country_id  where A.buyer_id in(select buyer_id from tbl_auction_fedex_rates_v2 where auction_id=281 AND is_error = 1 ) AND C.code <> 'US' ORDER BY A.buyer_id")
            GVBuyer.DataSource = dt
            GVBuyer.DataBind()
    End Sub

    Private Sub fetchShippingRate(ByVal auction_id As Integer, ByVal buyer_id As Integer, ByVal Destination_StreetLines As String, ByVal Destination_City As String, ByVal Destination_StateOrProvinceCode As String, ByVal destination_zip As String, ByVal destination_country_code As String, ByVal auction_weight As Double)
        'Try
        Dim obj As New FedAllRateServices
        Dim objreq As New FedexRateServices.RateRequest
        Dim objrep As New FedexRateServices.RateReply
        Dim dt As New DataTable()
        Dim i As Integer
        Dim Origin_StreetLines = "", Origin_City = "", Origin_StateOrProvinceCode = "", Origin_PostalCode = "", Origin_CountryCode As String = ""
        Origin_PostalCode = SqlHelper.of_FetchKey("fedex_source_zip") ' 11101
        Origin_CountryCode = SqlHelper.of_FetchKey("fedex_source_country_code") ' "US" 


        dt = obj.GetFedexShippingOptions(auction_id, Origin_StreetLines, Origin_City, Origin_StateOrProvinceCode, Origin_PostalCode, Origin_CountryCode, Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, destination_zip, destination_country_code, 1, 1, auction_weight * 1).DefaultView.ToTable(True, "ServiceType", "Rate", "Error")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                With dt.Rows(i)
                    If .Item("Error").ToString.Trim <> "" Then
                        SqlHelper.ExecuteNonQuery("UPDATE  tbl_auction_fedex_rates_v2 SET is_error = " & .Item("Error") & " where  buyer_id = " & buyer_id & " and is_error = 0")
                        SqlHelper.ExecuteNonQuery("if not exists(select fedex_rate_id from tbl_auction_fedex_rates_v2 where auction_id=" & auction_id & " and buyer_id =" & buyer_id & " and shipping_options='" & .Item("ServiceType") & "' and shipping_amount=" & .Item("rate") & ") begin INSERT INTO tbl_auction_fedex_rates_v2(auction_id, buyer_id, shipping_amount,shipping_options,error_log,is_error) VALUES (" & auction_id & ", " & buyer_id & ",'" & .Item("Rate") & "','" & .Item("ServiceType") & "','" & .Item("Error") & "',1) end")
                        lblMsg.Text = .Item("Error").ToString()
                    Else

                        SqlHelper.ExecuteNonQuery("if not exists(select fedex_rate_id from tbl_auction_fedex_rates_v2 where auction_id=" & auction_id & " and buyer_id =" & buyer_id & " and shipping_options='" & .Item("ServiceType") & "' and shipping_amount=" & .Item("rate") & ") begin INSERT INTO tbl_auction_fedex_rates_v2(auction_id, buyer_id, shipping_amount,shipping_options,error_log,is_error) VALUES (" & auction_id & ", " & buyer_id & ",'" & .Item("Rate") & "','" & .Item("ServiceType") & "','',0) end")
                        SqlHelper.ExecuteNonQuery("Update tbl_reg_buyers SET state_code='" & txtCode.Text & "' WHERE buyer_id =" & buyer_id & "")
                        SqlHelper.ExecuteNonQuery("delete tbl_auction_fedex_rates_v2 where  buyer_id = " & buyer_id & " and is_error = 1")
                        BindGrid()
                        lblMsg.Text = "Rate has been Updated"
                    End If

                End With
            Next
        Else
        End If
        dt.Dispose()
    End Sub


      Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim iBuyerId As Integer = Convert.ToInt32(txtBuyerId.Text)
        Dim sStateCode As String = txtCode.Text

        Dim dt As DataTable
        dt = SqlHelper.ExecuteDatatable("select A.buyer_id As BuyerId,ISNULL(A.address1, '') + ' ' + ISNULL(A.address2, '')  AS Address, ISNULL(A.city, '') AS City,ISNULL(A.state_text, 0) AS StateCode, ISNULL(A.zip, '') AS Zip, ISNULL(C.CODE, 0) AS CountryCode from tbl_reg_buyers A inner join tbl_master_countries C on A.country_id=C.country_id  where A.buyer_id in(select buyer_id from tbl_auction_fedex_rates_v2 where auction_id=281 AND is_error = 1 AND buyer_id =" & iBuyerId & ")")
        If dt.Rows.Count > 0 Then
            fetchShippingRate(281, dt.Rows(0).Item("BuyerId"), dt.Rows(0).Item("Address"), dt.Rows(0).Item("City"), txtCode.Text, dt.Rows(0).Item("Zip"), dt.Rows(0).Item("CountryCode"), 15)
          Else
          lblMsg.Text = "Buyer id is not exist in Error list"
        End If

      End Sub
End Class
