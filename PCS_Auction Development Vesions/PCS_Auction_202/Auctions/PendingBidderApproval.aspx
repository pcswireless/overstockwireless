﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PendingBidderApproval.aspx.vb"
    Inherits="Auctions_BidderApproval" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PCS Bidding</title>
    <meta id="Meta1" http-equiv="refresh" content="60" runat="server" />
    <link type="text/css" rel="Stylesheet" href="/Style/master_menu.css" />
    <link type="text/css" rel="Stylesheet" href="/Style/pcs.css" />
</head>
<body style="margin: 0px;">
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="ScriptManager" runat="server" />
    <table cellpadding="0" cellspacing="0" width="99%" bgcolor="white">
        <tr>
            <td>
                <telerik:RadGrid AutoGenerateColumns="false" ID="RadGrid2" AllowSorting="True" PageSize="10"
                    ShowFooter="False" AllowPaging="true" runat="server" AllowMultiRowSelection="true"
                    GridLines="None" Skin="Vista">
                    <PagerStyle Mode="NumericPages" />
                    <MasterTableView AutoGenerateColumns="false" ShowFooter="False" TableLayout="Auto" ItemStyle-Height="40" AlternatingItemStyle-Height="40">
                         <Columns>
                            <telerik:GridBoundColumn DataField="buyer_id" HeaderText="buyer_id" SortExpression="buyer_id"
                                UniqueName="buyer_id" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="company_name" HeaderText="Company Name" SortExpression="company_name"
                                UniqueName="company_name">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="name" HeaderText="Name" SortExpression="Name"
                                FilterControlWidth="45" UniqueName="name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="status" HeaderText="Status" SortExpression="status"
                                FilterControlWidth="30" AutoPostBackOnFilter="true" UniqueName="status">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="status" HeaderText="Status" SortExpression="status"
                                UniqueName="status" HeaderStyle-Width="120" ItemStyle-Width="110" FilterControlWidth="100">
                                <FilterTemplate>
                                    <telerik:RadComboBox ID="RadComboBo_Chk_Status" DataSourceID="SqlDataSource_Combo"
                                        runat="server" OnClientSelectedIndexChanged="StatusIndexChanged" Width="120px"
                                        DataTextField="status" DataValueField="status" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("status").CurrentFilterValue %>'>
                                        <Items>
                                        </Items>
                                    </telerik:RadComboBox>
                                    <telerik:RadScriptBlock ID="RadScriptBlock_combo" runat="server">
                                        <script type="text/javascript">
                                            function StatusIndexChanged(sender, args) {
                                                var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                tableView.filter("status", args.get_item().get_value(), "EqualTo");

                                            }
                                        </script>
                                    </telerik:RadScriptBlock>
                                </FilterTemplate>
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn AllowFiltering="false" Groupable="false" HeaderStyle-Width="130"
                                ItemStyle-Width="120" UniqueName="change_status">
                                <HeaderTemplate>
                                    Action
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hid_grd_status_id" runat="server" Value='<%# Eval("status_id") %>' />
                                    <asp:DropDownList ID="ddl_action" runat="server" DataSourceID="SqlDataSource_Combo"
                                        DataTextField="status" SelectedValue='<%#IIf(Eval("status_id").ToString <> "0", Eval("status_id"), Nothing) %>'
                                        DataValueField="status_id">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="SqlDataSource_Combo" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="select * from tbl_reg_buyser_statuses"
                    runat="server"></asp:SqlDataSource>
                <span style="float: right">
                    <asp:ImageButton ID="but_basic_update" runat="server" AlternateText="Update" ImageUrl="/images/update.gif" /></span>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
