﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendPopUP.master" AutoEventWireup="false" CodeFile="testUpdateFedExRate.aspx.vb" Inherits="Auctions_testUpdateFedExRate" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
    left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
    <img id="Image8" src="/images/img_loading.gif" alt="" />
</telerik:RadAjaxLoadingPanel>
<asp:Label ID="lbl_fedex_error" runat="server" CssClass="error"></asp:Label>
<asp:Panel runat="server" ID="pnl_fedex">
        <table cellpadding="5" cellspacing="0" style="font-size: 12px; font-weight: bold;">
            <tr>
                <td>
                    Pending :
                    <asp:Label ID="lbl_count_remaining" runat="server" />
                    bidders
                </td>
            </tr>
            <tr>
                <td>
                    Completed :
                    <asp:Label ID="lbl_count_completed" runat="server" />
                    bidders
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px dotted gray; border-bottom: 1px dotted gray; padding: 10px 5px;">
                    Total :
                    <asp:Label ID="lbl_count_total" runat="server" />
                    bidders
                </td>
            </tr>
            <tr>
                <td style="font-size: 10px; font-weight: bold; color: Gray;">
                    <asp:Label ID="Lbl_Id" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="font-size: 10px; font-weight: bold; color: Gray;">
                    <asp:Label id="lbl_close_window" runat="server" >Please dont close this window.</asp:Label>
                </td>
            </tr>
        </table>
<telerik:RadAjaxPanel ID="pnl1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <asp:HiddenField ID="hid_OK" runat="server" Value="-1" />
        <asp:HiddenField ID="hid_weight" runat="server" Value="0" />
        <asp:Button ID="btn_get_rate" runat="server" Text="Click to get Rates" Style="display: none;" />
        <asp:Label ID="ltrl_refresh" runat="server"></asp:Label>
  </telerik:RadAjaxPanel>  
<telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
        <script language="JavaScript">

          //Refresh page script- By Brett Taylor (glutnix@yahoo.com.au)
          //Modified by Dynamic Drive for NS4, NS6+
          //Visit http://www.dynamicdrive.com for this script

          //configure refresh interval (in seconds)
          var countDownInterval = 20;


          var countDownTime = countDownInterval + 1;
          function countDown() {
            countDownTime--;
            if (countDownTime <= 0) {
              countDownTime = countDownInterval;
              clearTimeout(counter)
              window.location.href = window.location.href;
              //window.location.reload(true);
              return
            }
            if (document.all) //if IE 4+
              document.all.countDownText.innerText = countDownTime + " ";
            else if (document.getElementById) //else if NS6+
              document.getElementById("countDownText").innerHTML = countDownTime + " "
            counter = setTimeout("countDown()", 1000);
          }

          function startit() {
            if (document.all || document.getElementById) //CHANGE TEXT BELOW TO YOUR OWN
              document.getElementById('<%=Lbl_Id.ClientID %>').innerHTML = 'The shipping rate calculation is under process. Page will refresh automatically in <b id="countDownText">' + countDownTime + ' </b> seconds';

                countDown()
              }


              if (document.getElementById('<%=hid_OK.ClientID %>').value == '0') {
            if (document.all || document.getElementById)
              startit()
            else
              window.onload = startit

            document.getElementById('<%=btn_get_rate.ClientID %>').click();
              }
              else if (document.getElementById('<%=hid_OK.ClientID %>').value == '1') {
              document.getElementById('<%=Lbl_Id.ClientID %>').innerHTML = '<font color="red">Shipping rates of all invited bidder has been calculated.</font>';
              document.getElementById('<%=lbl_close_window.ClientID %>').innerHTML = '<font color="red">You can close this window now</font>';
              window.opener.refresh_fedex();
            }

        </script>
   </telerik:RadScriptBlock>    
   </asp:Panel>
    <center>
        <div style="padding: 20px;">
            <a href="javascript:window.close();">
                <img src='/Images/close.gif' alt='' border="0" /></a>
        </div>
    </center>
</asp:Content>

