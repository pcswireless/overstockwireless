﻿
Partial Class Auctions_SelectFromInvCriteria
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HID_invitation_id.Value = SqlHelper.ExecuteScalar("if exists(select invitation_id from tbl_auction_invitations where auction_id='" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & "') begin select top 1 invitation_id from tbl_auction_invitations where auction_id='" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & "' end else select 0")
            loadInvitationDropDownList()
            btn_save_filter.Visible = False
        End If
    End Sub

    Private Sub loadInvitationDropDownList()
        If CommonCode.is_admin_user Then
            ddl_invitation.DataSource = SqlHelper.ExecuteDataView("SELECT A.invitation_id, name FROM tbl_auction_invitations A inner join tbl_auctions B WITH (NOLOCK) on A.auction_id=B.auction_id where A.name<>''" & IIf(HID_invitation_id.Value > 0, " And A.invitation_id<>" & HID_invitation_id.Value, "") & " order by A.name")
        Else
            ddl_invitation.DataSource = SqlHelper.ExecuteDataView("SELECT A.invitation_id, name FROM tbl_auction_invitations A inner join tbl_auctions B WITH (NOLOCK) on A.auction_id=B.auction_id inner join tbl_reg_seller_user_mapping C on B.seller_id=C.seller_id where A.name<>''" & IIf(HID_invitation_id.Value > 0, " And A.invitation_id<>" & HID_invitation_id.Value, "") & " and user_id = '" & CommonCode.Fetch_Cookie_Shared("user_id") & "' order by A.name")
        End If
        'HID_invitation_id.Value = "0"
        ddl_invitation.DataBind()
        ddl_invitation.Items.Insert(0, New ListItem("--Select--", "0"))

    End Sub

    Protected Sub ddl_invitation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_invitation.SelectedIndexChanged
        If ddl_invitation.SelectedItem.Value <> "0" Then
            initializeInvitationData(ddl_invitation.SelectedValue)
        Else
            pnl_invitaion_data.Visible = False
            btn_save_filter.Visible = False
        End If
    End Sub

    Private Sub initializeInvitationData(ByVal invitation_id As Integer)

        pnl_invitaion_data.Visible = False
        btn_save_filter.Visible = False

        If invitation_id <> 0 Then
            pnl_invitaion_data.Visible = True
            If Not ddl_invitation.Items.FindByValue(invitation_id) Is Nothing Then
                ddl_invitation.ClearSelection()
                ddl_invitation.Items.FindByValue(invitation_id).Selected = True
            End If

            Dim i As Integer
            Dim dtSource As New DataTable()

            btn_save_filter.Visible = False
            div_item_not.Visible = False
            div_opt_1.Visible = False
            div_opt_2.Visible = False
            RadGrid_SelectedBucketlist.Columns.FindByUniqueName("DeleteColumn").Visible = True
            RadGrid_SelectedBidderlist.Columns.FindByUniqueName("DeleteColumn").Visible = True

            If ddl_invitation.SelectedValue <> HID_invitation_id.Value And ddl_invitation.SelectedValue <> "0" Then
                RadGrid_SelectedBucketlist.Columns.FindByUniqueName("DeleteColumn").Visible = False
                RadGrid_SelectedBidderlist.Columns.FindByUniqueName("DeleteColumn").Visible = False
            End If


            dtSource = SqlHelper.ExecuteDatatable("select invitation_filter_id,invitation_id,filter_type from tbl_auction_invitation_filters where invitation_id = " & invitation_id & " order by invitation_filter_id")

            If dtSource.Rows.Count > 0 Then
                btn_save_filter.Visible = True
                For i = 0 To dtSource.Rows.Count - 1
                    If dtSource.Rows(i).Item("filter_type") = "bucket" Then
                        div_opt_1.Visible = True
                        invitation_filter_id1.Value = dtSource.Rows(i).Item("invitation_filter_id")
                    Else
                        div_opt_2.Visible = True
                        invitation_filter_id2.Value = dtSource.Rows(i).Item("invitation_filter_id")
                    End If
                Next
                RadGrid_SelectedBucketlist.Rebind()
                RadGrid_SelectedBidderlist.Rebind()
                btn_save_filter.Visible = True
            Else
                div_item_not.Visible = True
                btn_save_filter.Visible = False
            End If
        End If

    End Sub

    Protected Sub btn_save_filter_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_save_filter.Click

        If Page.IsValid Then

            If HID_invitation_id.Value = "0" Then insertNewInvitation()

            If ddl_invitation.SelectedValue <> "0" And HID_invitation_id.Value <> ddl_invitation.SelectedValue Then
                copyInvitation(HID_invitation_id.Value, ddl_invitation.SelectedValue)
            End If

            Dim objAuction As New Auction()
            objAuction.update_auction_bidder_invitation(Request.QueryString.Get("i"))
            objAuction = Nothing

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh", "window.opener.refresh_page();window.close();", True)

        End If

    End Sub

    Private Sub copyInvitation(ByVal new_inv_id As Integer, ByVal old_inv_id As Integer)

        'delete existing invitations
        SqlHelper.ExecuteNonQuery("Delete from tbl_auction_invitation_filter_values where invitation_filter_id in (select invitation_filter_id from tbl_auction_invitation_filters where invitation_id=" & new_inv_id & "); Delete from tbl_auction_invitation_filters where invitation_id=" & new_inv_id)


        Dim new_invitation_filter_id As Integer
        Dim old_invitation_filter_id As Integer
        Dim filter_type As String = ""
        Dim qry As String
        Dim i As Integer
        Dim dt As New DataTable
        dt = SqlHelper.ExecuteDatatable("SELECT A.invitation_filter_id,ISNULL(A.filter_type, '') AS filter_type	FROM tbl_auction_invitation_filters A WHERE A.invitation_id = " & old_inv_id & " ORDER BY A.invitation_filter_id")
        For i = 0 To dt.Rows.Count - 1
            With dt.Rows(i)
                old_invitation_filter_id = .Item("invitation_filter_id")
                filter_type = .Item("filter_type")
                new_invitation_filter_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_auction_invitation_filters(invitation_id, filter_type) VALUES ('" & new_inv_id & "', '" & filter_type & "'); SELECT SCOPE_IDENTITY()")

                qry = "INSERT INTO tbl_auction_invitation_filter_values(invitation_filter_id, bucket_id, buyer_id, auction_id) SELECT " & new_invitation_filter_id & ",bucket_id,buyer_id," & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & " from tbl_auction_invitation_filter_values where invitation_filter_id=" & old_invitation_filter_id
                SqlHelper.ExecuteNonQuery(qry)

            End With
        Next

    End Sub

    Private Sub insertNewInvitation()

        Dim qry As String = ""
        Dim invitation_id As Integer = 0
        Dim title As String = ""

        invitation_id = SqlHelper.ExecuteScalar("select isnull(max(invitation_id),0) from tbl_auction_invitations")
        invitation_id = invitation_id + 1

        title = ""

        qry = "INSERT INTO tbl_auction_invitations(auction_id, name, submit_date, submit_by_user_id) " & _
            "VALUES ('" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & "', '" & title & "', getdate(), '" & CommonCode.Fetch_Cookie_Shared("user_id") & "')  select scope_identity()"

        HID_invitation_id.Value = SqlHelper.ExecuteScalar(qry)

    End Sub

End Class
