﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AuctionBasicPop.aspx.vb"
    Inherits="Auctions_AuctionBasicPop" MasterPageFile="~/BackendPopUP.master" Title="PCS Bidding" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <asp:HiddenField ID="Auction_id" runat="server" Value="0" />
        <table cellpadding="10" cellspacing="0" border="0" width="100%">
            <tr>
                <td>
                    <div class="pageheading">
                        <asp:Label ID="lbl_caption1" runat="server" Text=""></asp:Label>
                    </div>
                </td>
            </tr>
            <tr id="tr_detail" runat="server">
                <td>
                    
                    <telerik:RadEditor ID="txt_control1" runat="server" Skin="Sitefinity" Width="675">
                        <Content>
             
                        </Content>
                    </telerik:RadEditor>
                    <asp:Label ID="lbl_control1" runat="server"></asp:Label>
                    &nbsp;
                </td>
            </tr>
            <tr id="tr_attachment" runat="server">
                <td>
                    <div style="color: #302F30; font-weight: bold;font-size:14px; padding-top: 10px;">
                        <asp:Label ID="lbl_caption2" runat="server" Text=""></asp:Label>
                    </div>
                </td>
            </tr>
            <tr id="tr_attachment_detail" runat="server">
                <td>
                    <%--<div style="width: 220px; float: left;">
                        <asp:HiddenField ID="HID_Upload" runat="server" Value="0" />
                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="TextBox" Width="300" size="15" /><br />
                        </div>
                        <asp:Literal runat="server" ID="lit_for_product_item"></asp:Literal>
                    <div style="width: 155px; float: left;">
                        <asp:Label ID="lbl_control2" runat="server" Text=""></asp:Label>
                        
                        <asp:CheckBox ID="remove_check" runat="server" Checked="false" Text="Remove" TextAlign="Right" Visible="false" />
                        &nbsp;
                    </div>--%>
                    
                    <div style="width: 300px; float: left;">
                        <asp:HiddenField ID="HID_Upload" runat="server" Value="0" />
                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="TextBox" Width="250" size="15" /><br />
                        </div>
                        <asp:Literal runat="server" ID="lit_for_product_item"></asp:Literal>
                    <div style="width: 250px; float: left;">
                        <asp:Label ID="lbl_control2" runat="server" Text=""></asp:Label>
                        
                       <span style="padding-left:10px;"><asp:CheckBox ID="remove_check" runat="server" Checked="false" Text="Remove" TextAlign="Right" Visible="false" />
                        &nbsp;</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_msg" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="bottom" align="center">
                    <asp:ImageButton ID="btn_other_update" runat="server" AlternateText="Update" ImageUrl="/images/update.gif" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:ImageButton ID="ImageButton1" OnClientClick="javascript:window.close();" runat="server"
                        AlternateText="Close" ImageUrl="/images/close.gif" />
                    <div style="text-align: center;">
                    </div>
                    <%-- <div class="addButton">
                    </div>
                     <div class="cancelButton">
                        <asp:ImageButton ID="btn_other_edit" runat="server" AlternateText="Edit" ImageUrl="/images/edit.gif" />
                        <asp:ImageButton ID="btn_other_cancel" CausesValidation="false" runat="server" AlternateText="Cancel"
                            ImageUrl="/images/cancel.gif" /></div>--%>
                </td>
            </tr>
        </table>
            <script type ="text/javascript" >
                window.focus();
    </script>
   </asp:Content>
