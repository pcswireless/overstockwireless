﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendPopUP.master" AutoEventWireup="false" CodeFile="InviteBidders.aspx.vb" Inherits="Auctions_InviteBidders" %>
<%@ Register src="UserControls/AuctionInvitation.ascx" tagname="AuctionInvitation" tagprefix="uc1" %>
<%@ Register Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" TagPrefix="Telerik" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Simple">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rad_pnl_invite_bidders">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rad_pnl_invite_bidders" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Skin="Simple" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" style="position:fixed;left:420px;top:180px;z-index:9999" runat="server" BackgroundPosition="Center">
      <img id="Image8" src="/images/img_loading.gif" />
  </telerik:RadAjaxLoadingPanel>
     <Telerik:RadAjaxPanel ID="rad_pnl_invite_bidders" runat="server" scrollbars="Vertical">
    <uc1:AuctionInvitation ID="UC_AuctionInvitation" runat="server" />
    </Telerik:RadAjaxPanel>
</asp:Content>

