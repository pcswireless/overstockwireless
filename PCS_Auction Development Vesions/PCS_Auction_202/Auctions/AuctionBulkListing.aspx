﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="AuctionBulkListing.aspx.vb" Inherits="Auctions_AuctionBulkListing" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <input type="hidden" id="hid_location_combo_id" name="hid_location_combo_id" value="0" />
    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                     args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                     args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {

                    args.set_enableAjax(false);
                }
                else {
                    args.set_enableAjax(true);
                }
            }
            function setdropid(sender) {

                document.getElementById('hid_location_combo_id').value = sender.get_id();

            }
           
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Vista">
        <ClientEvents OnRequestStart="Cancel_Ajax" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Auctions Bulk Listing</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; width: 40%; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    Please select the title to edit from below
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hid_user_id" runat="server" Value="0" />
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
                    left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
                    <img id="Image8" src="/images/img_loading.gif" />
                </telerik:RadAjaxLoadingPanel>
                <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                    EnableAJAX="false">
                    <telerik:RadGrid ID="RadGrid1" runat="server" PageSize="10" AllowPaging="True" AllowAutomaticUpdates="True"
                        AutoGenerateColumns="False" DataSourceID="SqlDataSource1" AllowMultiRowSelection="false" 
                        Skin="Vista" AllowFilteringByColumn="True" ShowGroupPanel="True" EnableLinqExpressions="false">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <ExportSettings IgnorePaging="true" FileName="Auction_Bulk_Export" OpenInNewWindow="true"
                            ExportOnlyData="true" />
                        <ClientSettings AllowDragToGroup="true">
                            <Selecting AllowRowSelect="True"></Selecting>
                            
                        </ClientSettings>
                        <GroupingSettings ShowUnGroupButton="true" />
                        <MasterTableView DataKeyNames="auction_id" DataSourceID="SqlDataSource1" CommandItemDisplay="Top"
                            AllowSorting="true" EditMode="EditForms" AutoGenerateColumns="False" AllowFilteringByColumn="True"
                            ItemStyle-Height="40" AlternatingItemStyle-Height="40">
                            <NoRecordsTemplate>
                                Auctions not available to List
                            </NoRecordsTemplate>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="title" SortOrder="Ascending" />
                            </SortExpressions>
                            <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false"
                                ShowExportToCsvButton="false" ShowExportToPdfButton="false" ShowRefreshButton="false" ShowAddNewRecordButton="false" />
                            <Columns>
                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                    Visible="false">
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                </telerik:GridEditCommandColumn>
                                <telerik:GridBoundColumn DataField="code" HeaderText="Code" DataType="System.String"
                                    SortExpression="code" UniqueName="code" FilterControlWidth="50">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="title" HeaderText="Title" SortExpression="title"
                                    UniqueName="title" FilterControlWidth="300" GroupByExpression="title [Title] group by title">
                                    <ItemTemplate>
                                        <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');"
                                            onclick="redirectIframe('/Backend_TopBar.aspx?t=<%# CommonCode.getAuctionTabPosition(Eval("auction_id")) %>&a=<%# Eval("auction_id") %>','/Backend_Leftbar.aspx?t=3&a=<%# Eval("auction_id") %>','/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %>')">
                                            <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("title") %>'></asp:Label></a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <%-- <telerik:GridBoundColumn DataField="title" HeaderText="Title" SortExpression="title" EditFormHeaderTextFormat="Title"
                                    UniqueName="title" FilterControlWidth="50">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="stock_location" HeaderText="Stock Location" SortExpression="stock_location" EditFormHeaderTextFormat="Stock Location"
                                    UniqueName="stock_location" FilterControlWidth="50">
                                </telerik:GridBoundColumn>--%>
                                <telerik:GridTemplateColumn HeaderText="Stock Location" SortExpression="stock_location"
                                    DataField="stock_location" EditFormHeaderTextFormat="Stock Location" UniqueName="stock_location"
                                    GroupByExpression="stock_location [Stock Location] group by stock_location">
                                    <FilterTemplate>
                                        <telerik:RadComboBox ID="RadComboBo_Stock" runat="server" OnClientSelectedIndexChanged="Stock_location_IndexChanged"
                                            DataSourceID="radcombo_stock_location" Width="120px" AppendDataBoundItems="true"
                                            DataTextField="stock_location" DataValueField="stock_location" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("stock_location").CurrentFilterValue %>'>
                                            <Items>
                                                <telerik:RadComboBoxItem Value="" Text="All" />
                                            </Items>
                                        </telerik:RadComboBox>
                                        <telerik:RadScriptBlock ID="RadScriptBlock_Stock_location" runat="server">
                                            <script type="text/javascript">
                                                function Stock_location_IndexChanged(sender, args) {
                                                    var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                    tableView.filter("stock_location", args.get_item().get_value(), "EqualTo");

                                                }
                                            </script>
                                        </telerik:RadScriptBlock>
                                    </FilterTemplate>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbl_stock_location" Text='<%# Eval("stock_location") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="status" HeaderText="Status" SortExpression="status"
                                    ReadOnly="true" UniqueName="status" FilterControlWidth="50">
                                    <FilterTemplate>
                                        <telerik:RadComboBox ID="RadComboBo_Chk_Status" runat="server" OnClientSelectedIndexChanged="StatusIndexChanged"
                                            Width="120px" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("status").CurrentFilterValue %>'>
                                            <Items>
                                                <telerik:RadComboBoxItem Text="All" />
                                                <telerik:RadComboBoxItem Text="Bid Closed" Value="Bid Over" />
                                                <telerik:RadComboBoxItem Text="Running" Value="Running" />
                                                <telerik:RadComboBoxItem Text="Upcoming" Value="Upcoming" />
                                            </Items>
                                        </telerik:RadComboBox>
                                        <telerik:RadScriptBlock ID="RadScriptBlock_combo" runat="server">
                                            <script type="text/javascript">
                                                function StatusIndexChanged(sender, args) {
                                                    var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                    tableView.filter("status", args.get_item().get_value(), "Contains");

                                                }
                                            </script>
                                        </telerik:RadScriptBlock>
                                    </FilterTemplate>
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="auction_type" HeaderText="Auction Type" SortExpression="auction_type"
                                    ReadOnly="true" UniqueName="auction_type" FilterControlWidth="50">
                                 <FilterTemplate>
                                    <telerik:RadComboBox ID="RadComboBoxCategory" DataSourceID="SqlDataSource2" DataTextField="auction_type"
                                        DataValueField="auction_type" Height="200px" AppendDataBoundItems="true"
                                        SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("auction_type").CurrentFilterValue %>'
                                        runat="server" OnClientSelectedIndexChanged="AuctionTypeIndexChanged">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="All" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                        <script type="text/javascript">
                                            function AuctionTypeIndexChanged(sender, args) {
                                                var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                tableView.filter("auction_type", args.get_item().get_value(), "EqualTo");

                                            }
                                        </script>
                                    </telerik:RadScriptBlock>
                                </FilterTemplate>
                                </telerik:GridBoundColumn>
                            </Columns>
                            <EditFormSettings EditFormType="Template" EditColumn-ItemStyle-Width="100%" EditColumn-HeaderStyle-HorizontalAlign="Left"
                                EditColumn-ItemStyle-HorizontalAlign="Left">
                                <FormTemplate>
                                    <table cellpadding="0" cellspacing="2" border="0" style="padding-top: 10px; text-align: left;"
                                        width="100%">
                                        <tr>
                                            <td class="caption" style="width: 110px;">
                                                Code
                                            </td>
                                            <td class="details" style="width: 270px;">
                                                <asp:TextBox ID="txt_code" runat="server" Text='<%# Bind("code") %>' CssClass="inputtype"
                                                    ValidationGroup="val1"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfv_code" runat="server" ErrorMessage="<br>Code Required"
                                                    CssClass="error" ControlToValidate="txt_code" Display="Dynamic" ValidationGroup="grd_items"></asp:RequiredFieldValidator>
                                            </td>
                                            <td style="width: 145px;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 270px;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 110px;">
                                                Title
                                            </td>
                                            <td class="details" style="width: 270px;">
                                                <asp:TextBox ID="txt_title" runat="server" Text='<%# Bind("title") %>' ValidationGroup="val1"
                                                    CssClass="inputtype"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfv_title" runat="server" ErrorMessage="<br>Title Required"
                                                    CssClass="error" ControlToValidate="txt_title" Display="Dynamic" ValidationGroup="grd_items"></asp:RequiredFieldValidator>
                                            </td>
                                            <td style="width: 145px;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 270px;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 110px;">
                                                Stock Location
                                            </td>
                                            <td class="details" style="width: 270px;">
                                                <telerik:RadComboBox ID="RadCombo_stock_location_inner" OnClientLoad="setdropid"
                                                    runat="server" Height="200px" Width="200px" DataSourceID="radcombo_stock" DropDownWidth="300px"
                                                    EmptyMessage="--Stock Location--" HighlightTemplatedItems="true" SelectedValue='<%# Bind("stock_location_id") %>'
                                                    EnableLoadOnDemand="true" Filter="StartsWith" OnItemsRequested="RadCombo_stock_location_ItemsRequested"
                                                    AllowCustomText="true" DataTextField="name" DataValueField="stock_location_id">
                                                    <HeaderTemplate>
                                                        <table style="width: 290px;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="width: 100px;">
                                                                    Name
                                                                </td>
                                                                <td style="width: 75px;">
                                                                    City
                                                                </td>
                                                                <td style="width: 85px;">
                                                                    State
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table style="width: 290px;" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="width: 100px;">
                                                                    <%# DataBinder.Eval(Container.DataItem, "Name")%>
                                                                </td>
                                                                <td style="width: 75px;">
                                                                    <%# DataBinder.Eval(Container.DataItem, "city")%>
                                                                </td>
                                                                <td style="width: 85px;">
                                                                    <%# DataBinder.Eval(Container.DataItem, "state")%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </telerik:RadComboBox>
                                                <asp:CustomValidator ID="CustomValidate_Location" runat="server" ClientValidationFunction="validate_location"
                                                    ErrorMessage="<br />Please select location fromt the list." ControlToValidate="RadCombo_stock_location_inner"
                                                    ValidationGroup="grd_items" Font-Size="10px" Display="Dynamic">
                                                </asp:CustomValidator>
                                            </td>
                                            <td style="width: 145px;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 270px;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="right">
                                                <asp:ImageButton ID="img_btn_save_comment" runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif", "/images/update.gif") %>'
                                                    CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "PerformInsert", "Update")%>'
                                                    ValidationGroup="grd_items" OnClientClick="return ValidationGroupEnable('grd_items', true);" />
                                                <asp:ImageButton ID="img_btn_cancel" runat="server" ImageUrl="/images/cancel.gif"
                                                    CausesValidation="false" CommandName="cancel" />
                                            </td>
                                        </tr>
                                    </table>
                                </FormTemplate>
                            </EditFormSettings>
                        </MasterTableView>
                    </telerik:RadGrid>
                </telerik:RadAjaxPanel>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="select isnull(A.auction_id,0) as auction_id,ISNULL(A.code, '') AS code,ISNULL(A.title, '') AS title,dbo.auction_status(A.auction_id) as Status,isnull(A.no_of_clicks,0) as no_of_clicks,t.name as auction_type,A.stock_location_id,isnull(s.name,'') as stock_location from tbl_auctions A WITH (NOLOCK) left join tbl_auction_types as t on A.auction_type_id=t.auction_type_id inner join tbl_reg_seller_user_mapping B on A.seller_id=B.seller_id left join tbl_master_stock_locations s on A.stock_location_id=s.stock_location_id where B.user_id=@user_id"
                    UpdateCommand="UPDATE [tbl_auctions] SET [code] = @code, [title] = @title, [stock_location_id]=@stock_location_id WHERE [auction_id] = @auction_id">
                    <SelectParameters>
                        <asp:ControlParameter Name="user_id" Type="Int32" ControlID="hid_user_id" PropertyName="Value" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="code" Type="String" />
                        <asp:Parameter Name="title" Type="String" />
                        <asp:Parameter Name="stock_location_id" />
                        <asp:Parameter Name="auction_id" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="radcombo_stock" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT stock_location_id, name, city,state from tbl_master_stock_locations ORDER BY name">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="radcombo_stock_location" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT name as stock_location from tbl_master_stock_locations">
                </asp:SqlDataSource>
                 <asp:SqlDataSource ID="SqlDataSource2" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="select ISNULL(name,'') As auction_type,auction_type_id from tbl_auction_types where is_active=1 order by sno asc"
                    runat="server"></asp:SqlDataSource>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function validate_location(source, args) {
            args.IsValid = false;

            var combo = $find(document.getElementById('hid_location_combo_id').value);
            var text = combo.get_text();
            if (text.length < 1) {
                args.IsValid = false;
            }
            else {
                var node = combo.findItemByText(text);
                if (node) {
                    var value = node.get_value();
                    if (value.length > 0) {
                        args.IsValid = true;
                    }
                }
                else {
                    args.IsValid = false;
                }
            }
        }

        function HasPageValidators() {
            var hasValidators = false;

            try {
                if (Page_Validators.length > 0) {
                    hasValidators = true;
                }
            }
            catch (error) {
            }

            return hasValidators;
        }

        function ValidationGroupEnable(validationGroupName, isEnable) {
            // alert(document.getElementById('hid_location_combo_id').value);
            if (HasPageValidators()) {

                for (i = 0; i < Page_Validators.length; i++) {
                    if (Page_Validators[i].validationGroup == validationGroupName) {

                        if (Page_ClientValidate(validationGroupName)) {
                            return true;
                        }
                        else { return false; }
                    }
                }
            }
        }
    </script>
</asp:Content>
