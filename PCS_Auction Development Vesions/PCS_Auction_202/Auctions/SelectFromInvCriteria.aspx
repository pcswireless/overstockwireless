﻿<%@ Page Title="PCS Bidding" Language="VB" MasterPageFile="~/BackendPopUP.master" AutoEventWireup="false"
    CodeFile="SelectFromInvCriteria.aspx.vb" Inherits="Auctions_SelectFromInvCriteria" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Skin="Simple" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" alt="" />
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ddl_invitation">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btn_save_filter">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btn_save_filter">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
        CssClass="TabGrid">
        <table cellpadding="10" cellspacing="0" border="0" width="100%">
            <tr>
                <td>
                    <div class="pageheading">
                        <asp:Literal ID="page_heading" runat="server" Text="Invitation Template"></asp:Literal>
                    </div>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table cellpadding="5">
                        <tr>
                            <td>
                                Select Template :
                                <asp:DropDownList ID="ddl_invitation" runat="server" DataValueField="invitation_id"
                                    DataTextField="name" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:HiddenField ID="HID_invitation_id" runat="server" Value="0" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;" valign="top">
                                <asp:Panel ID="pnl_invitaion_data" runat="server" ScrollBars="Auto" Height="200">
                                    <div id="div_opt_1" runat="server" visible='False'>
                                        <div class="greenBackSmall">
                                            <div style="padding-top: 8px;">
                                                Business Type</div>
                                        </div>
                                        <div class="greenBackSmall_down">
                                            <asp:HiddenField ID="invitation_filter_id1" runat="server" Value="0" />
                                            <telerik:RadGrid ID="RadGrid_SelectedBucketlist" GridLines="None" runat="server"
                                                AllowAutomaticDeletes="True" AllowPaging="False" AutoGenerateColumns="False"
                                                AllowMultiRowSelection="false" DataSourceID="SqlDataSource1" Skin="Vista" AllowFilteringByColumn="false"
                                                ShowHeader="false" ShowFooter="false">
                                                <MasterTableView DataKeyNames="invitation_filter_value_id" HorizontalAlign="NotSet"
                                                    AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                                                    <NoRecordsTemplate>
                                                        <div style="height: 19px; padding-top: 3px; padding-left: 5px;">
                                                            Bucket not selected</div>
                                                    </NoRecordsTemplate>
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="bucket_name" HeaderText="name" UniqueName="bucket_name">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridButtonColumn ConfirmText="Delete this bucket?" ConfirmDialogType="RadWindow"
                                                            ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                                        </telerik:GridButtonColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                                ProviderName="System.Data.SqlClient" SelectCommand="select A.invitation_filter_value_id,B.bucket_name from tbl_auction_invitation_filter_values A join tbl_master_buckets B on A.bucket_id=B.bucket_id where A.buyer_id=0 and A.invitation_filter_id= @invitation_filter_id"
                                                DeleteCommand="DELETE FROM [tbl_auction_invitation_filter_values] WHERE [invitation_filter_value_id] = @invitation_filter_value_id">
                                                <SelectParameters>
                                                    <asp:ControlParameter Name="invitation_filter_id" Type="Int32" ControlID="invitation_filter_id1"
                                                        PropertyName="Value" />
                                                </SelectParameters>
                                                <DeleteParameters>
                                                    <asp:Parameter Name="invitation_filter_value_id" Type="Int32" />
                                                </DeleteParameters>
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>
                                    <div id="div_opt_2" runat="server" visible='False' style="padding-top: 10px;">
                                        <div class="greenBackSmall">
                                            <div style="padding-top: 8px;">
                                                Bidders</div>
                                        </div>
                                        <div class="greenBackSmall_down">
                                            <asp:HiddenField ID="invitation_filter_id2" runat="server" Value="0" />
                                            <telerik:RadGrid ID="RadGrid_SelectedBidderlist" GridLines="None" runat="server"
                                                AllowAutomaticDeletes="True" AllowPaging="False" AutoGenerateColumns="False"
                                                AllowMultiRowSelection="false" DataSourceID="SqlDataSource2" Skin="Vista" AllowFilteringByColumn="false"
                                                ShowHeader="false" ShowFooter="false">
                                                <MasterTableView DataKeyNames="invitation_filter_value_id" HorizontalAlign="NotSet"
                                                    AutoGenerateColumns="False" DataSourceID="SqlDataSource2">
                                                    <NoRecordsTemplate>
                                                        <div style="height: 19px; padding-top: 3px; padding-left: 5px;">
                                                            Bidder not selected</div>
                                                    </NoRecordsTemplate>
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="name" HeaderText="Contact Person" SortExpression="name"
                                                            UniqueName="name">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="company_name" HeaderText="company_name" UniqueName="company_name">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="email" HeaderText="email" UniqueName="email">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridButtonColumn ConfirmText="Delete this bidder?" ConfirmDialogType="RadWindow"
                                                            ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                                        </telerik:GridButtonColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                                ProviderName="System.Data.SqlClient" SelectCommand="select A.invitation_filter_value_id,B.contact_first_name +' ' + B.contact_last_name as name,B.company_name,B.email from tbl_auction_invitation_filter_values A join tbl_reg_buyers B on A.buyer_id=B.buyer_id where A.bucket_id=0 and A.invitation_filter_id= @invitation_filter_id"
                                                DeleteCommand="DELETE FROM [tbl_auction_invitation_filter_values] WHERE [invitation_filter_value_id] = @invitation_filter_value_id">
                                                <SelectParameters>
                                                    <asp:ControlParameter Name="invitation_filter_id" Type="Int32" ControlID="invitation_filter_id2"
                                                        PropertyName="Value" />
                                                </SelectParameters>
                                                <DeleteParameters>
                                                    <asp:Parameter Name="invitation_filter_value_id" Type="Int32" />
                                                </DeleteParameters>
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>
                                    <div style="background-color: White; margin-top: 10px;" id="div_item_not" runat="server">
                                        <table cellpadding="5">
                                            <tr>
                                                <td>
                                                    No Item Selected.
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;" valign="top" align="center">
                                <asp:ImageButton ID="btn_save_filter" ValidationGroup="_invitation" runat="server"
                                    AlternateText="Apply" ImageUrl="/Images/apply.png" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
</asp:Content>
