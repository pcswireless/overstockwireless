﻿Imports Telerik.Web.UI
Imports System.Drawing
Partial Class Auctions_AddEditAuction
    Inherits System.Web.UI.Page

    Private Sub setPermission()
        If Not CommonCode.is_super_admin() Then

            btn_auction_delete.Visible = False

            Dim Vew_Auction As Boolean = False
            Dim Edit_Auction As Boolean = False
            Dim New_Auction As Boolean = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))
            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 1
                        New_Auction = True
                    Case 2
                        Vew_Auction = True
                    Case 3
                        Edit_Auction = True
                End Select
            Next
            dt = Nothing
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                If Vew_Auction = False Then Response.Redirect("/NoPermission.aspx")
                If Not Edit_Auction Then
                    Panel1_Content1button_perm.Visible = False
                    div_edit_description.Visible = False
                    div_edit_short_description.Visible = False
                    div_edit_prod_detail.Visible = False
                    div_edit_payment_term.Visible = False
                    div_edit_shipping_term.Visible = False
                    div_edit_other_term.Visible = False
                    div_edit_special_condition.Visible = False
                    div_edit_tax_details.Visible = False
                    RadGrid_Attachment.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None
                    RadGrid_Attachment.Columns.FindByUniqueName("EditCommandColumn").Visible = False
                    RadGrid_Attachment.Columns.FindByUniqueName("DeleteColumn").Visible = False
                    Panel1_Content4button_per.Visible = False
                    Panel1_Content4button_per_schedule_tab.Visible = False
                    pnl_after_launch_buttons.Visible = False


                    'UC_AuctionTemplate.Visible = False
                End If
            Else
                If New_Auction = False Then Response.Redirect("/NoPermission.aspx")
            End If
        End If
    End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            fill_flag_types()
            BindflagInfo()
            If Not Request.QueryString("lang") Is Nothing Then
                hid_language.Value = Request.QueryString("lang")
                rdolst_language.Items.FindByValue(Request.QueryString("lang")).Selected = True
            Else
                hid_language.Value = "1"
                rdolst_language.Items.FindByValue("1").Selected = True
            End If
            If Request.QueryString.Get("e") = "1" Then
                lbl_msg_basic.Text = "New auction created successfully. Please change the bidding details section to schedule it."
            End If
            'txt_weight.Attributes.Add("onkeyup", "on_key_press();")


            loadBasicDropDownList()
            loadDetailsDropDownList()
            hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")

            panelSet()

            ViewState("order_by") = "dateA"
            ViewState("qry_filter") = "pending"
            'load_auction_bidder_queries()


            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                set_form_mode(False)
            Else
                set_form_mode(True)
            End If

        End If
        If IsNumeric(Request.QueryString("i")) Then
            Me.UC_System_log_link.Unique_ID = Request.QueryString("i")
            Me.UC_System_log_link.Module_Name = "Auction"

        Else
            'btn_fedex_report.Visible = False
        End If


    End Sub

    Protected Sub rdolst_language_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdolst_language.SelectedIndexChanged
        ' lbl_sub_title.Text = Request.RawUrl
        If Not Request.QueryString("i") Is Nothing Then
            Response.Redirect("/Auctions/AddEditAuction.aspx?i=" & Request.QueryString.Get("i") & "&lang=" & rdolst_language.SelectedItem.Value)
        Else
            Response.Redirect("/Auctions/AddEditAuction.aspx?lang=" & rdolst_language.SelectedItem.Value)
        End If
        'hid_language.Value = rdolst_language.SelectedItem.Value
        'CType(AuctionItems1.FindControl("hid_language"), HiddenField).Value = rdolst_language.SelectedItem.Value
        'setBasicEdit(True)
        'setOtherDetailsEdit(True)
        'setDetailsEdit(True)
        'setDetailsEditSchedule(True)
        'setAfterLaunchEdit(True)
    End Sub

    Private Sub panelSet()

        If Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then

            pnl_edit_auction.Visible = True

            RadTabStrip1.Tabs(0).Text = "Edit Auction"
            hid_auction_id.Value = Request.QueryString.Get("i")
            hid_basic_form_mode.Value = "view"
            setBasicEdit(True)
            setOtherDetailsEdit(True)
            setDetailsEdit(True)
            'setDetailsEditSchedule(True)
            setAfterLaunchEdit(True)
            Panel1_Content4button_per_schedule_tab.Visible = False
            RadTabStrip1.Tabs(0).CssClass = "auctiontab"
            RadTabStrip1.Tabs(1).CssClass = "auctiontab"
            RadTabStrip1.Tabs(2).CssClass = "auctiontab"
            RadTabStrip1.Tabs(3).CssClass = "auctiontab"
            RadTabStrip1.Tabs(0).SelectedCssClass = "auctiontabSelected"
            RadTabStrip1.Tabs(1).SelectedCssClass = "auctiontabSelected"
            RadTabStrip1.Tabs(2).SelectedCssClass = "auctiontabSelected"
            RadTabStrip1.Tabs(3).SelectedCssClass = "auctiontabSelected"
            RadTabStrip1.Tabs(4).SelectedCssClass = "auctiontabSelected"
            RadTabStrip1.Tabs(5).SelectedCssClass = "auctiontabSelected"
            Select Case Request.QueryString.Get("t")
                Case 1
                    cpe1.Collapsed = False
                    cpe11.Collapsed = False
                    pnl_img1.ImageUrl = "/Images/up_Arrow.gif"
                    'RadTabStrip1.Tabs(0).CssClass = "auctiontabSelected"
                    RadTabStrip1.SelectedIndex = 0
                    RadMultiPage1.SelectedIndex = 0
                Case 8
                    cpe8.Collapsed = False
                    'cpe88.Collapsed = False
                    pnl_img8.ImageUrl = "/Images/up_Arrow.gif"
                    'RadTabStrip1.Tabs(0).CssClass = "auctiontabSelected"
                    RadTabStrip1.SelectedIndex = 0
                    RadMultiPage1.SelectedIndex = 0
                Case 10
                    cpe10.Collapsed = False
                    pnl_img10.ImageUrl = "/Images/up_Arrow.gif"
                    RadTabStrip1.SelectedIndex = 0
                    RadMultiPage1.SelectedIndex = 0
                Case 2
                    cpe2.Collapsed = False
                    pnl_img2.ImageUrl = "/Images/up_Arrow.gif"
                    'RadTabStrip1.Tabs(0).CssClass = "auctiontabSelected"
                    RadTabStrip1.SelectedIndex = 0
                    RadMultiPage1.SelectedIndex = 0
                Case 3
                    cpe3.Collapsed = False
                    pnl_img3.ImageUrl = "/Images/up_Arrow.gif"
                    'RadTabStrip1.Tabs(0).CssClass = "auctiontabSelected"
                    RadTabStrip1.SelectedIndex = 0
                    RadMultiPage1.SelectedIndex = 0
                Case 4
                    cpe4.Collapsed = False
                    cpe44.Collapsed = False
                    pnl_img4.ImageUrl = "/Images/up_Arrow.gif"
                    'RadTabStrip1.Tabs(0).CssClass = "auctiontabSelected"
                    RadTabStrip1.SelectedIndex = 0
                    RadMultiPage1.SelectedIndex = 0
                    Panel1_Content4button_per_schedule_tab.Visible = False
                Case 5
                    'RadTabStrip1.Tabs(1).CssClass = "auctiontabSelected"
                    RadTabStrip1.SelectedIndex = 1
                    RadMultiPage1.SelectedIndex = 1
                Case 6
                    'RadTabStrip1.Tabs(2).CssClass = "auctiontabSelected"
                    RadTabStrip1.SelectedIndex = 2
                    RadMultiPage1.SelectedIndex = 2
                Case 7
                    'RadTabStrip1.Tabs(3).CssClass = "auctiontabSelected"
                    RadTabStrip1.SelectedIndex = 3
                    RadMultiPage1.SelectedIndex = 3
                Case 9
                    'RadTabStrip1.Tabs(3).CssClass = "auctiontabSelected"
                    RadTabStrip1.SelectedIndex = 4
                    RadMultiPage1.SelectedIndex = 4
                Case Else
                    cpe1.Collapsed = False
                    cpe11.Collapsed = False
                    pnl_img1.ImageUrl = "/Images/up_Arrow.gif"
                    'RadTabStrip1.Tabs(0).CssClass = "auctiontabSelected"
                    RadTabStrip1.SelectedIndex = 0
                    RadMultiPage1.SelectedIndex = 0
            End Select
            div_save_pdf.InnerHtml = "<a href='#' class='tabItemHeader1' onclick=""return openpreview('" & Security.EncryptionDecryption.EncryptValueFormatted(Request.QueryString.Get("i")) & "');"">Preview</a>"
        Else
            rdolst_language.Visible = False
            pnl_edit_auction.Visible = False
            RadTabStrip1.Tabs(1).Visible = False
            RadTabStrip1.Tabs(2).Visible = False
            RadTabStrip1.Tabs(3).Visible = False
            RadTabStrip1.Tabs(4).Visible = False
            RadTabStrip1.Tabs(5).Visible = False
            RadTabStrip1.Tabs(0).Text = "New Auction"
            btn_auction_delete.Visible = False
            cpe1.Collapsed = False
            cpe11.Collapsed = False
            pnl_img1.ImageUrl = "/Images/up_Arrow.gif"
            txt_auction_code.Text = get_new_auction_code()
            hid_basic_form_mode.Value = "entry"
            setBasicEdit(False)
            setDetailsEdit(False)
            setDetailsEditSchedule(False)
            setAfterLaunchEdit(False)
            div_save_pdf.Style.Add("display", "none")
        End If
    End Sub

#Region "Basic Information"

    Private Function get_new_auction_code() As String
        Dim strCode As String = ""
        Dim str As String = "select dbo.fn_generate_auction_code()"
        strCode = SqlHelper.ExecuteScalar(str)
        Return strCode
    End Function
    Private Sub loadBasicDropDownList()
        Load_product_category("")
        load_stock_conditions("")
        load_stock_location("")
        Load_packaging("")
        Load_sellers("")
    End Sub
    Protected Sub ddl_seller_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs) Handles ddl_seller.ItemDataBound
        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("company_name").ToString()
        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("seller_id").ToString()
    End Sub
    Protected Sub ddl_seller_ItemsRequested(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles ddl_seller.ItemsRequested
        Load_sellers(e.Text)
    End Sub
    Private Sub Load_sellers(ByVal search_text As String)
        Dim sqlSelectCommand As String = "" '"SELECT stock_location_id, name, city,state from tbl_master_stock_locations WHERE name LIKE '" & search_text & "%' ORDER BY name"
        If CommonCode.is_admin_user() Then
            sqlSelectCommand = "select A.seller_id,A.company_name ,(A.contact_first_name+' '+A.contact_last_name) as contact_name,A.email,isnull(A.contact_title,'') as contact_title,isnull(A.phone,0) as phone,A.is_active from tbl_reg_sellers A  WHERE  ISNULL(A.is_active,0)=1 and A.company_name LIKE '" & search_text & "%' ORDER BY company_name"
        Else
            sqlSelectCommand = "select A.seller_id,A.company_name ,(A.contact_first_name+' '+A.contact_last_name) as contact_name,A.email,isnull(A.contact_title,'') as contact_title,isnull(A.phone,0) as phone,A.is_active from tbl_reg_sellers A inner join [tbl_reg_seller_user_mapping] B on A.seller_id=B.seller_id  WHERE ISNULL(A.is_active,0)=1 and B.user_id= " & CommonCode.Fetch_Cookie_Shared("user_id") & " and A.company_name LIKE '" & search_text & "%' ORDER BY company_name"
        End If
        Dim dt As New DataTable()
        ddl_seller.Items.Clear()
        dt = SqlHelper.ExecuteDataTable(sqlSelectCommand)
        ddl_seller.DataSource = dt
        ddl_seller.DataBind()

        dt = Nothing
    End Sub
    Protected Sub ddl_packaging_ItemsRequested(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles ddl_packaging.ItemsRequested
        Load_packaging(e.Text)
    End Sub
    Private Sub Load_packaging(ByVal search_text As String)
        Dim sqlSelectCommand As String = "SELECT packaging_id,name FROM [tbl_master_packaging] WHERE name LIKE '" & search_text & "%' ORDER BY name"
        ddl_packaging.Items.Clear()
        ddl_packaging.DataSource = SqlHelper.ExecuteDataView(sqlSelectCommand)
        ddl_packaging.DataBind()
    End Sub
    Protected Sub ddl_product_category_ItemsRequested(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles ddl_product_category.ItemsRequested
        Load_product_category(e.Text)
    End Sub
    Private Sub Load_product_category(ByVal search_text As String)
        Dim sqlSelectCommand As String = "SELECT product_catetory_id,name FROM [tbl_master_product_categories] WHERE name LIKE '" & search_text & "%' ORDER BY name"
        Dim dt As New DataTable()
        ddl_product_category.Items.Clear()
        dt = SqlHelper.ExecuteDataTable(sqlSelectCommand)
        ddl_product_category.DataSource = SqlHelper.ExecuteDataView(sqlSelectCommand)
        ddl_product_category.DataBind()
    End Sub
    Protected Sub ddl_stock_condition_ItemsRequested(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles ddl_stock_condition.ItemsRequested
        load_stock_conditions(e.Text)
    End Sub
    Private Sub load_stock_conditions(ByVal search_text As String)
        Dim sqlSelectCommand As String = "SELECT stock_condition_id,name FROM [tbl_master_stock_conditions] WHERE name LIKE '" & search_text & "%' ORDER BY name"
        Dim dt As New DataTable()
        ddl_stock_condition.Items.Clear()
        dt = SqlHelper.ExecuteDataTable(sqlSelectCommand)
        ddl_stock_condition.DataSource = SqlHelper.ExecuteDataView(sqlSelectCommand)
        ddl_stock_condition.DataBind()
    End Sub
    Protected Sub ddl_stock_location_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs) Handles ddl_stock_location.ItemDataBound
        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("name").ToString()
        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("stock_location_id").ToString()

    End Sub
    Protected Sub ddl_stock_location_ItemsRequested(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles ddl_stock_location.ItemsRequested
        load_stock_location(e.Text)
    End Sub
    Private Sub load_stock_location(ByVal search_text As String)
        Dim sqlSelectCommand As String = "SELECT stock_location_id, name, city,state from tbl_master_stock_locations WHERE name LIKE '" & search_text & "%' ORDER BY name"
        Dim dt As New DataTable()
        ddl_stock_location.Items.Clear()
        dt = SqlHelper.ExecuteDataTable(sqlSelectCommand)
        ddl_stock_location.DataSource = dt
        ddl_stock_location.DataBind()

        dt = Nothing
    End Sub
    Private Sub setBasicEdit(ByVal readMode As Boolean)
        If Request.QueryString.Get("i") <> "0" Then
            fillBasicData(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
        Else
            EmptyBasicData()
        End If
        visibleBasicData(readMode)
    End Sub
    Protected Sub btn_refresh_basic_Click(sender As Object, e As EventArgs) Handles btn_refresh_basic.Click
        setBasicEdit(True)
        load_auction_bidder_queries()
    End Sub
    Private Sub fillBasicData(ByVal auction_id As Integer)
        If auction_id > 0 Then
            Dim qry As String = "SELECT A.auction_id, " & _
                "ISNULL(A.code, '') AS code," & _                "ISNULL(A.title, '') AS title," & _                "ISNULL(A.sub_title, '') AS sub_title," & _                "ISNULL(A.product_catetory_id, 0) AS product_catetory_id," & _                "ISNULL(A.stock_condition_id, 0) AS stock_condition_id," & _                "ISNULL(A.packaging_id, 0) AS packaging_id," & _                "ISNULL(A.seller_id, 0) AS seller_id," & _                "ISNULL(A.stock_location_id, '') AS stock_location_id," & _                "ISNULL(B.name, '') AS stock_location," & _                "ISNULL(A.is_active, 0) AS is_active," & _                "ISNULL(A.code_f, '') AS code_f," & _                "ISNULL(A.code_s, '') AS code_s," & _                "ISNULL(A.code_c, '') AS code_c," & _                "ISNULL(A.title_f, '') AS title_f," & _                "ISNULL(A.title_s, '') AS title_s," & _                "ISNULL(A.title_c, '') AS title_c," & _                "ISNULL(A.sub_title_f, '') AS sub_title_f," & _                "ISNULL(A.sub_title_s, '') AS sub_title_s," & _                "ISNULL(A.sub_title_c, '') AS sub_title_c," & _                "ISNULL(A.description_f, '') AS description_f," & _                "ISNULL(A.description_s, '') AS description_s," & _                "ISNULL(A.description_c, '') AS description_c," & _                "ISNULL(A.special_conditions_f, '') AS special_conditions_f," & _                "ISNULL(A.special_conditions_s, '') AS special_conditions_s," & _                "ISNULL(A.special_conditions_c, '') AS special_conditions_c," & _                "ISNULL(A.payment_terms_f, '') AS payment_terms_f," & _                "ISNULL(A.payment_terms_s, '') AS payment_terms_s," & _                "ISNULL(A.payment_terms_c, '') AS payment_terms_c," & _                "ISNULL(A.shipping_terms_f, '') AS shipping_terms_f," & _                "ISNULL(A.shipping_terms_s, '') AS shipping_terms_s," & _                "ISNULL(A.shipping_terms_c, '') AS shipping_terms_c," & _                "ISNULL(A.other_terms_f, '') AS other_terms_f," & _                "ISNULL(A.other_terms_s, '') AS other_terms_s," & _                "ISNULL(A.other_terms_c, '') AS other_terms_c, " & _                "ISNULL(A.start_date, '1/1/1999') as start_date," & _                "ISNULL(A.end_date, '1/1/1999') as end_date," & _
                "ISNULL(A.display_end_time, '1/1/1999') as display_end_time," & _
                "ISNULL(A.show_on_home_page, 0) as show_on_home_page," & _
                "ISNULL(A.show_relevant_bidders_only, 0) as show_relevant_bidders_only," & _
                "ISNULL(A.auction_type_id, 0) AS auction_type_id,dbo.get_auction_status(A.auction_id) As status," & _                "ISNULL((select count(buyer_id) from [dbo].[fn_get_invited_buyers](" & auction_id & ") where buyer_id not in (select buyer_id from tbl_auction_fedex_rates WITH (NOLOCK) where auction_id=" & auction_id & ")),0) as fedex_pending," & _                "ISNULL(A.[weight],0) as [weight]," & _                "isnull((select top 1 convert(varchar(10),product_item_attachment_id) + '#' + filename from tbl_auction_product_item_attachments where auction_id=A.auction_id order by product_item_attachment_id desc),'') as product_item," & _                "ISNULL(A.[after_launch_filename],'') as [after_launch_filename]," & _                "ISNULL(A.[package_type],'') as [package_type]" & _                " FROM tbl_auctions A WITH (NOLOCK) LEFT JOIN tbl_master_stock_locations B ON A.stock_location_id=B.stock_location_id WHERE A.auction_id = " & auction_id

            Dim dt As DataTable = New DataTable()
            dt = SqlHelper.ExecuteDataTable(qry)
            If dt.Rows.Count > 0 Then
                With dt.Rows(0)

                    page_heading.Text = CommonCode.decodeSingleQuote(.Item("title"))
                    page_sub_title.Text = CommonCode.decodeSingleQuote(.Item("sub_title"))

                    If hid_language.Value.ToString = "2" Then 'french
                        lbl_auction_code.Text = CommonCode.decodeSingleQuote(.Item("code_f"))
                        txt_auction_code.Text = lbl_auction_code.Text

                        lbl_auction_title.Text = CommonCode.decodeSingleQuote(.Item("title_f"))
                        txt_auction_title.Text = lbl_auction_title.Text

                        lbl_sub_title.Text = CommonCode.decodeSingleQuote(.Item("sub_title_f"))
                        txt_sub_title.Text = lbl_sub_title.Text

                        lbl_stock_location.Text = CommonCode.decodeSingleQuote(.Item("stock_location"))
                        'If Not ddl_stock_location.Items.FindItemByValue(.Item("stock_location_id")) Is Nothing Then
                        '    ddl_stock_location.Items.FindItemByValue(.Item("stock_location_id")).Selected = True
                        'End If
                        'txt_stock_location.Text = lbl_stock_location.Text
                    ElseIf hid_language.Value.ToString = "3" Then 'spanish
                        lbl_auction_code.Text = CommonCode.decodeSingleQuote(.Item("code_s"))
                        txt_auction_code.Text = lbl_auction_code.Text

                        lbl_auction_title.Text = CommonCode.decodeSingleQuote(.Item("title_s"))
                        txt_auction_title.Text = lbl_auction_title.Text

                        lbl_sub_title.Text = CommonCode.decodeSingleQuote(.Item("sub_title_s"))
                        txt_sub_title.Text = lbl_sub_title.Text

                        lbl_stock_location.Text = CommonCode.decodeSingleQuote(.Item("stock_location"))
                        'txt_stock_location.Text = lbl_stock_location.Text
                    ElseIf hid_language.Value.ToString = "4" Then 'Chinese
                        lbl_auction_code.Text = CommonCode.decodeSingleQuote(.Item("code_c"))
                        txt_auction_code.Text = lbl_auction_code.Text

                        lbl_auction_title.Text = CommonCode.decodeSingleQuote(.Item("title_c"))
                        txt_auction_title.Text = lbl_auction_title.Text

                        lbl_sub_title.Text = CommonCode.decodeSingleQuote(.Item("sub_title_c"))
                        txt_sub_title.Text = lbl_sub_title.Text

                        lbl_stock_location.Text = CommonCode.decodeSingleQuote(.Item("stock_location"))
                        'txt_stock_location.Text = lbl_stock_location.Text
                    Else 'english
                        lbl_auction_code.Text = CommonCode.decodeSingleQuote(.Item("code"))
                        txt_auction_code.Text = lbl_auction_code.Text

                        lbl_auction_title.Text = CommonCode.decodeSingleQuote(.Item("title"))
                        txt_auction_title.Text = lbl_auction_title.Text

                        lbl_sub_title.Text = CommonCode.decodeSingleQuote(.Item("sub_title"))
                        txt_sub_title.Text = lbl_sub_title.Text

                        lbl_stock_location.Text = CommonCode.decodeSingleQuote(.Item("stock_location"))
                        'txt_stock_location.Text = lbl_stock_location.Text
                    End If


                    If Not ddl_stock_location.Items.FindItemByValue(.Item("stock_location_id")) Is Nothing Then
                        ddl_stock_location.Items.FindItemByValue(.Item("stock_location_id")).Selected = True
                    End If
                    'Response.Write(.Item("product_item").ToString)
                    If .Item("product_item").ToString.Contains("#") AndAlso .Item("product_item").ToString.Remove(0, .Item("product_item").ToString.LastIndexOf("#")).Contains(".") Then
                        lit_for_product_item.Text = "<a href='/upload/Auctions/product_items/" & auction_id & "/" & .Item("product_item").ToString.Remove(.Item("product_item").ToString.LastIndexOf("#")) & "/" & .Item("product_item").ToString.Remove(0, .Item("product_item").ToString.LastIndexOf("#") + 1) & "' target='_blank'>" & .Item("product_item").ToString.Remove(0, .Item("product_item").ToString.LastIndexOf("#") + 1) & "</a>&nbsp;&nbsp;&nbsp;&nbsp;"
                    End If
                    If .Item("after_launch_filename").ToString <> "" Then
                        lit_for_attach_afterlaunch.Text = "<a href='/upload/Auctions/after_launch/" & auction_id & "/" & .Item("after_launch_filename").ToString & "' target='_blank'>" & .Item("after_launch_filename").ToString & "</a>&nbsp;&nbsp;&nbsp;&nbsp;"
                    End If
                    If Not ddl_product_category.Items.FindItemByValue(.Item("product_catetory_id")) Is Nothing Then
                        ddl_product_category.ClearSelection()
                        ddl_product_category.Items.FindItemByValue(.Item("product_catetory_id")).Selected = True
                    End If
                    If Not ddl_product_category.SelectedItem Is Nothing Then
                        lbl_product_category.Text = ddl_product_category.SelectedItem.Text
                    Else
                        lbl_product_category.Text = ""
                    End If

                    If Not ddl_stock_condition.Items.FindItemByValue(.Item("stock_condition_id")) Is Nothing Then
                        ddl_stock_condition.ClearSelection()
                        ddl_stock_condition.Items.FindItemByValue(.Item("stock_condition_id")).Selected = True
                    End If
                    If Not ddl_stock_condition.SelectedItem Is Nothing Then
                        lbl_stock_condition.Text = ddl_stock_condition.SelectedItem.Text
                    Else
                        lbl_stock_condition.Text = ""
                    End If

                    If Not ddl_packaging.Items.FindItemByValue(.Item("packaging_id")) Is Nothing Then
                        ddl_packaging.ClearSelection()
                        ddl_packaging.Items.FindItemByValue(.Item("packaging_id")).Selected = True
                        lbl_packaging.Text = ddl_packaging.SelectedItem.Text
                    Else
                        lbl_packaging.Text = ""
                    End If

                    If Not ddl_seller.Items.FindItemByValue(.Item("seller_id")) Is Nothing Then
                        ddl_seller.ClearSelection()
                        ddl_seller.Items.FindItemByValue(.Item("seller_id")).Selected = True
                    End If
                    If Not ddl_seller.SelectedItem Is Nothing Then
                        lbl_seller.Text = ddl_seller.SelectedItem.Text
                    Else
                        lbl_seller.Text = ""
                    End If


                    chk_home_page.Checked = .Item("show_on_home_page")
                    If .Item("show_on_home_page") Then
                        lbl_home_page.Text = "<img src='/Images/true.gif' alt=''>"
                    Else
                        lbl_home_page.Text = "<img src='/Images/false.gif' alt=''>"
                    End If


                    chk_relevant_bidders.Checked = .Item("show_relevant_bidders_only")
                    If .Item("show_relevant_bidders_only") Then
                        lbl_relevant_bidders.Text = "<img src='/Images/true.gif' alt=''>"
                    Else
                        lbl_relevant_bidders.Text = "<img src='/Images/false.gif' alt=''>"
                    End If

                    'chk_pcs_shipping.Checked = .Item("use_pcs_shipping")
                    'If .Item("use_pcs_shipping") Then
                    '    lbl_pcs_shipping.Text = "<img src='/Images/true.gif' alt=''>"
                    'Else
                    '    lbl_pcs_shipping.Text = "<img src='/Images/false.gif' alt=''>"
                    'End If



                    'chk_your_shipping.Checked = .Item("use_your_shipping")
                    'If .Item("use_your_shipping") Then
                    '    lbl_your_shipping.Text = "<img src='/Images/true.gif' alt=''>"
                    'Else
                    '    lbl_your_shipping.Text = "<img src='/Images/false.gif' alt=''>"
                    'End If

                    'If Not ddl_package_type.Items.FindItemByValue(.Item("package_type")) Is Nothing Then
                    '    ddl_package_type.ClearSelection()
                    '    ddl_package_type.Items.FindItemByValue(.Item("package_type")).Selected = True
                    '    lbl_package_type.Text = ddl_package_type.SelectedItem.Text
                    'Else
                    '    lbl_package_type.Text = ""
                    'End If

                    btn_auction_delete.Attributes.Add("onClick", "return confirm('By deleting the auction, You are deleting the data of auction. It will delete auctions, invitation and everything related to auction.')")
                    If Not .Item("is_active") Then
                        hid_auction_status.Value = 0
                        btn_auction_active.Attributes.Add("onClick", "return confirm('Are you sure to activate this auction?')")
                        btn_auction_active.AlternateText = "Activate"
                        btn_auction_active.ImageUrl = "/Images/activate.png"
                        ltrl_current_status.Text = "<span id='sp_auc_status' style='color:red;'> (Inactive) </span>"
                    Else
                        hid_auction_status.Value = 1
                        btn_auction_active.Attributes.Add("onClick", "return confirm('Are you sure to Deactivate this auction?')")
                        btn_auction_active.AlternateText = "Deactivate"
                        btn_auction_active.ImageUrl = "/Images/deactivate.png"
                        Select Case .Item("status")
                            Case 1
                                ltrl_current_status.Text = "<span id='sp_auc_status' style='color:#339933;'> (Running) </span>"
                            Case 2
                                ltrl_current_status.Text = "<span id='sp_auc_status' style='color:#FF7F00;'> (Upcoming) </span>"
                            Case 3
                                If .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then
                                    ltrl_current_status.Text = "<span id='sp_auc_status' style='color:#FF2A2A;'> (Closed) </span>"
                                Else
                                    ltrl_current_status.Text = "<span id='sp_auc_status' style='color:#FF2A2A;'> (Auction Closed) </span>"
                                End If
                        End Select
                    End If
                    'txt_weight.Text = Convert.ToDecimal(.Item("weight")).ToString("F2") 'FormatNumber(.Item("weight"), 2)
                    'lbl_weight.Text = Convert.ToDecimal(.Item("weight")).ToString("F2") 'FormatNumber(.Item("weight"), 2)
                    'lbl_kgs.Text = Convert.ToDecimal(.Item("weight") / 2.2).ToString("F2") 'FormatNumber(.Item("weight") / 2.2, 2)

                    'Response.Write(.Item("auction_type_id") & "----" & .Item("use_pcs_shipping") & "----" & .Item("fedex_pending") & "----" & .Item("show_relevant_bidders_only"))
                    'If (.Item("auction_type_id") <> 4 And .Item("use_pcs_shipping") And .Item("fedex_pending") > 0) Then
                    '    btn_auction_fedex.Visible = True
                    'Else
                    '    If .Item("show_relevant_bidders_only") = False Then
                    '        btn_auction_fedex.Visible = True
                    '    Else
                    '        'btn_auction_fedex.Visible = False
                    '        btn_auction_fedex.Visible = True
                    '    End If

                    'End If

                End With

                Dim no_of_image As Integer = SqlHelper.ExecuteScalar("select count(stock_image_id) from tbl_auction_stock_images where auction_id=" & auction_id)

                lit_image_upload.Text = no_of_image & " images uploaded <a href='javascript:void(0);' onclick='return window_image_upload(" & auction_id & ");'>Edit</a>"

                'IMG_Image_1.ImageUrl = "/images/imagenotavailable.gif"
                'IMG_Image_2.ImageUrl = "/images/imagenotavailable.gif"
                'IMG_Image_3.ImageUrl = "/images/imagenotavailable.gif"
                'IMG_Image_4.ImageUrl = "/images/imagenotavailable.gif"
                'HID_stock_image_id_1.Value = "0"
                'HID_stock_image_id_2.Value = "0"
                'HID_stock_image_id_3.Value = "0"
                'HID_stock_image_id_4.Value = "0"

                'Dim dt6 As DataTable = New DataTable()
                'dt6 = SqlHelper.ExecuteDataTable("select top 4 stock_image_id,filename from tbl_auction_stock_images where auction_id=" & auction_id & " order by stock_image_id")
                'If dt6.Rows.Count > 0 Then
                '    Dim i As Integer
                '    For i = 0 To dt6.Rows.Count - 1
                '        If i = 0 Then
                '            With dt6.Rows(i)
                '                HID_stock_image_id_1.Value = .Item("stock_image_id")
                '                IMG_Image_1.ImageUrl = "/Upload/Auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id") & "/" & .Item("filename")
                '                a_IMG_Image_1.HRef = "/Upload/Auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id") & "/" & .Item("filename")
                '                DIV_IMG_Default_Logo1_Img.Attributes.Add("onmouseover", "tip_new('<img width=250 height=200 src=" & IMG_Image_1.ImageUrl.ToString & " border=0 />','250','white');")
                '            End With
                '        End If
                '        If i = 1 Then
                '            With dt6.Rows(i)
                '                HID_stock_image_id_2.Value = .Item("stock_image_id")
                '                IMG_Image_2.ImageUrl = "/Upload/Auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id") & "/" & .Item("filename")
                '                a_IMG_Image_2.HRef = "/Upload/Auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id") & "/" & .Item("filename")
                '                DIV_IMG_Default_Logo2_Img.Attributes.Add("onmouseover", "tip_new('<img width=250 height=200 src=" & IMG_Image_2.ImageUrl.ToString & " border=0 />','250','white');")
                '            End With
                '        End If
                '        If i = 2 Then
                '            With dt6.Rows(i)
                '                HID_stock_image_id_3.Value = .Item("stock_image_id")
                '                IMG_Image_3.ImageUrl = "/Upload/Auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id") & "/" & .Item("filename")
                '                a_IMG_Image_3.HRef = "/Upload/Auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id") & "/" & .Item("filename")
                '                DIV_IMG_Default_Logo3_Img.Attributes.Add("onmouseover", "tip_new('<img width=250 height=200 src=" & IMG_Image_3.ImageUrl.ToString & " border=0 />','250','white');")
                '            End With
                '        End If
                '        If i = 3 Then
                '            With dt6.Rows(i)
                '                HID_stock_image_id_4.Value = .Item("stock_image_id")
                '                IMG_Image_4.ImageUrl = "/Upload/Auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id") & "/" & .Item("filename")
                '                a_IMG_Image_4.HRef = "/Upload/Auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id") & "/" & .Item("filename")
                '                DIV_IMG_Default_Logo4_Img.Attributes.Add("onmouseover", "tip_new('<img width=250 height=200 src=" & IMG_Image_4.ImageUrl.ToString & " border=0 />','250','white');")
                '            End With
                '        End If
                '    Next

                'End If
                'dt6 = Nothing

            Else
                EmptyBasicData()
                RadWindowManager1.RadAlert("No records to display.", 280, 100, "Data Alert", "")
            End If
            dt = Nothing
        End If




    End Sub
    Private Sub EmptyBasicData()
        txt_auction_title.Text = ""
        'txt_weight.Text = ""
        lbl_auction_title.Text = ""
        txt_sub_title.Text = ""
        lbl_sub_title.Text = ""
        txt_auction_code.Text = ""
        lbl_auction_code.Text = ""
        ddl_product_category.ClearSelection()
        'ddl_product_category.SelectedIndex = 0
        lbl_product_category.Text = ""
        ddl_stock_condition.ClearSelection()
        ddl_stock_condition.SelectedIndex = 0
        lbl_stock_condition.Text = ""
        ' txt_stock_location.Text = ""
        ddl_stock_location.ClearSelection()
        lbl_stock_location.Text = ""
        ddl_packaging.ClearSelection()
        'ddl_packaging.SelectedIndex = 0
        lbl_packaging.Text = ""
        ddl_seller.ClearSelection()
        ' ddl_seller.SelectedIndex = 0
        lbl_seller.Text = ""
        chk_home_page.Checked = False

        lbl_home_page.Text = ""

        chk_relevant_bidders.Checked = False
        'chk_your_shipping.Checked = True
        'chk_pcs_shipping.Checked = True
        lbl_relevant_bidders.Text = ""
        'lbl_pcs_shipping.Text = ""
        'lbl_your_shipping.Text = ""
        'ddl_package_type.ClearSelection()
        'lbl_package_type.Text = ""
    End Sub
    Private Sub visibleBasicData(ByVal readMode As Boolean)

        If readMode Then
            btn_basic_edit.Visible = True
            btn_basic_save.Visible = False
            btn_basic_update.Visible = False
            div_basic_cancel.Visible = False
        Else
            btn_basic_edit.Visible = False
            If String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
                btn_basic_save.Visible = True
                btn_auction_active.Visible = False
                'btn_auction_fedex.Visible = False
                btn_basic_update.Visible = False
                div_basic_cancel.Visible = False
            Else
                btn_basic_save.Visible = False
                btn_basic_update.Visible = True
                div_basic_cancel.Visible = True
            End If
        End If


        span_auction_title.Visible = Not readMode
        txt_auction_title.Visible = Not readMode
        lbl_auction_title.Visible = readMode

        span_sub_title.Visible = Not readMode
        txt_sub_title.Visible = Not readMode
        lbl_sub_title.Visible = readMode

        span_auction_code.Visible = Not readMode
        txt_auction_code.Visible = Not readMode
        lbl_auction_code.Visible = readMode

        'txt_weight.Visible = Not readMode
        'lbl_weight.Visible = readMode


        span_product_category.Visible = Not readMode
        ddl_product_category.Visible = Not readMode
        lbl_product_category.Visible = readMode

        ddl_stock_condition.Visible = Not readMode
        lbl_stock_condition.Visible = readMode

        'txt_stock_location.Visible = Not readMode
        ddl_stock_location.Visible = Not readMode
        lbl_stock_location.Visible = readMode

        ddl_packaging.Visible = Not readMode
        lbl_packaging.Visible = readMode

        span_seller.Visible = Not readMode
        ddl_seller.Visible = Not readMode
        lbl_seller.Visible = readMode


        chk_home_page.Visible = Not readMode

        lbl_home_page.Visible = readMode

        chk_relevant_bidders.Visible = Not readMode
        'chk_pcs_shipping.Visible = Not readMode
        'chk_your_shipping.Visible = Not readMode
        lbl_relevant_bidders.Visible = readMode

        'lbl_your_shipping.Visible = readMode
        'lbl_pcs_shipping.Visible = readMode

        'ddl_package_type.Visible = Not readMode
        'lbl_package_type.Visible = readMode

        'FLUN_Image_1.Visible = Not readMode
        'If HID_stock_image_id_1.Value <> "0" Then
        '    CHK_Image1.Visible = Not readMode
        'Else
        '    CHK_Image1.Visible = False
        'End If

        'FLUN_Image_2.Visible = Not readMode
        'If HID_stock_image_id_2.Value <> "0" Then
        '    CHK_Image2.Visible = Not readMode
        'Else
        '    CHK_Image2.Visible = False
        'End If

        'FLUN_Image_3.Visible = Not readMode
        'If HID_stock_image_id_3.Value <> "0" Then
        '    CHK_Image3.Visible = Not readMode
        'Else
        '    CHK_Image3.Visible = False
        'End If

        'FLUN_Image_4.Visible = Not readMode
        'If HID_stock_image_id_4.Value <> "0" Then
        '    CHK_Image4.Visible = Not readMode
        'Else
        '    CHK_Image4.Visible = False
        'End If

    End Sub
    Protected Sub btn_basic_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_basic_edit.Click
        'Response.Write("asdasdasdasda")
        set_form_mode(True)
        hid_basic_form_mode.Value = "entry"
        visibleBasicData(False)
    End Sub
    Protected Sub btn_basic_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_basic_cancel.Click
        visibleBasicData(True)
        set_form_mode(False)
        hid_basic_form_mode.Value = "view"
    End Sub
    Private Function does_auction_code_exists() As Boolean
        Dim is_code_exists As Boolean = False
        Dim str As String = "select count(auction_id) from tbl_auctions WITH (NOLOCK) where code='" & txt_auction_code.Text.Trim() & "'"
        Dim count As Integer = SqlHelper.ExecuteScalar(str)
        If count > 0 Then
            is_code_exists = True
        End If
        Return is_code_exists
    End Function
    Protected Sub btn_basic_save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_basic_save.Click
        If Page.IsValid Then

            If does_auction_code_exists() Then
                lbl_msg_basic.Text = "Auction code already exists."
                Exit Sub
            End If
            'If chk_pcs_shipping.Checked = False And chk_your_shipping.Checked = False Then
            '    lbl_msg_basic.Text = "Please Checked PCS Shipping or Your Shipping"
            '    Return
            'End If
            Dim code_para As String = ""
            Dim title_para As String = ""
            Dim sub_title_para As String = ""
            Dim stock_location_para As String = ""

            If hid_language.Value.ToString = "2" Then
                code_para = "code_f"
                title_para = "title_f"
                sub_title_para = "sub_title_f"
                'stock_location_para = "stock_location_f"
            ElseIf hid_language.Value.ToString = "3" Then
                code_para = "code_s"
                title_para = "title_s"
                sub_title_para = "sub_title_s"
                ' stock_location_para = "stock_location_s"
            ElseIf hid_language.Value.ToString = "4" Then
                code_para = "code_c"
                title_para = "title_c"
                sub_title_para = "sub_title_c"
                ' stock_location_para = "stock_location_c"
            Else
                code_para = "code"
                title_para = "title"
                sub_title_para = "sub_title"
                'stock_location_para = "stock_location"
            End If
            'If Not ddl_stock_location.SelectedItem Is Nothing Then
            '    stock_location_para = ddl_stock_location.SelectedItem.Value
            'End If
            Dim insParameter As String = code_para '"code"
            Dim insValue As String = "'" & CommonCode.encodeSingleQuote(txt_auction_code.Text.Trim()) & "'"

            If txt_auction_title.Text.Trim() <> "" Then
                insParameter = insParameter & "," & title_para '",title"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_auction_title.Text.Trim()) & "'"
            End If
            If txt_sub_title.Text.Trim() <> "" Then
                insParameter = insParameter & "," & sub_title_para '",sub_title"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_sub_title.Text.Trim()) & "'"
            End If
            insParameter = insParameter & ",auction_type_id"
            insValue = insValue & ",'3'"
            insParameter = insParameter & ",auction_category_id"
            insValue = insValue & ",'1'"
            If Not ddl_product_category.SelectedItem Is Nothing Then
                insParameter = insParameter & ",product_catetory_id"
                insValue = insValue & ",'" & ddl_product_category.SelectedValue & "'"
            End If
            If Not ddl_stock_condition.SelectedItem Is Nothing Then
                insParameter = insParameter & ",stock_condition_id"
                insValue = insValue & ",'" & ddl_stock_condition.SelectedValue & "'"
            End If
            If ddl_packaging.SelectedValue <> "" And ddl_packaging.SelectedValue <> "0" Then
                insParameter = insParameter & ",packaging_id"
                insValue = insValue & ",'" & ddl_packaging.SelectedValue & "'"
            End If
            If Not ddl_seller.SelectedItem Is Nothing Then
                insParameter = insParameter & ",seller_id"
                insValue = insValue & ",'" & ddl_seller.SelectedValue & "'"
            End If
            If Not ddl_stock_location.SelectedItem Is Nothing Then
                insParameter = insParameter & ",stock_location_id" '",stock_location"
                insValue = insValue & ",'" & ddl_stock_location.SelectedItem.Value & "'"
            End If
            'If Not ddl_package_type.SelectedItem Is Nothing Then
            '    insParameter = insParameter & ",package_type"
            '    insValue = insValue & ",'" & ddl_package_type.SelectedValue & "'"
            'End If
            Dim is_on_home_page As Integer = 0

            If chk_home_page.Checked Then
                is_on_home_page = 1
            Else
                is_on_home_page = 0
            End If


            insParameter = insParameter & ",show_on_home_page"
            insValue = insValue & "," & is_on_home_page.ToString()

            insParameter = insParameter & ",show_relevant_bidders_only"
            insValue = insValue & "," & IIf(chk_relevant_bidders.Checked, 1, 0)

            insParameter = insParameter & ",use_pcs_shipping"
            insValue = insValue & ",1"

            insParameter = insParameter & ",use_your_shipping"
            insValue = insValue & ",0"

            insParameter = insParameter & ",start_date"
            insValue = insValue & ",getdate()"

            'insParameter = insParameter & ",weight"
            'insValue = insValue & "," & IIf(txt_weight.Text.Trim = "", "0", txt_weight.Text.Trim)

            insParameter = insParameter & ",is_active"
            insValue = insValue & ",0"

            insParameter = insParameter & ",end_date"
            insValue = insValue & ",getdate()"

            insParameter = insParameter & ",display_end_time"
            insValue = insValue & ",getdate()"

            insParameter = insParameter & ",submit_date"
            insValue = insValue & ",getdate()"

            insParameter = insParameter & ",submit_by_user_id"
            insValue = insValue & "," & CommonCode.Fetch_Cookie_Shared("user_id")

            insParameter = IIf(hid_language.Value.ToString <> "1", insParameter & ",code", insParameter)
            insValue = IIf(hid_language.Value.ToString <> "1", insValue & ",''", insValue)

            insParameter = insParameter & ",proxy_bid_allow"
            insValue = insValue & ",1"

            insParameter = insParameter & ",live_bid_allow"
            insValue = insValue & ",1"


            Dim qry As String = "insert into tbl_auctions(" & insParameter & ") values(" & insValue & ")  select scope_identity()"
            Dim new_auction_id As Integer = 0
            new_auction_id = SqlHelper.ExecuteScalar(qry)

            Dim auction_id As Integer = 0
            Dim strQuery As String = ""
            Update_flag_name(new_auction_id)

            ' txt_auction_title.Text = qry
            'UploadFileStockImages(new_auction_id)
            CommonCode.insert_system_log("New auction created", "btn_basic_save_Click", new_auction_id, "", "Auction")
            'Response.Write("<script>redirectIframe('/Backend_TopBar.aspx?t=4&a=" & new_auction_id & "','/Backend_Leftbar.aspx?t=3&a=" & new_auction_id & "','/Auctions/AddEditAuction.aspx?i=" & new_auction_id & "')</script>")
            Response.Redirect("/Auctions/AddEditAuction.aspx?e=1&i=" & new_auction_id)
        End If
    End Sub
    Protected Sub btn_basic_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_basic_update.Click
        If Page.IsValid Then
            basicUpdate()
            BindflagInfo()
            'If chk_pcs_shipping.Checked = False And chk_your_shipping.Checked = False Then
            '    lbl_msg_basic.Text = "Please Checked PCS Shipping or Your Shipping."
            '    Return
            'End If
            Dim is_update_required As Boolean = False
            Dim updpara As String = ""
            Dim code_para As String = ""
            Dim title_para As String = ""
            Dim sub_title_para As String = ""
            If hid_language.Value.ToString = "2" Then
                code_para = "code_f"
                title_para = "title_f"
                sub_title_para = "sub_title_f"
            ElseIf hid_language.Value.ToString = "3" Then
                code_para = "code_s"
                title_para = "title_s"
                sub_title_para = "sub_title_s"
            ElseIf hid_language.Value.ToString = "4" Then
                code_para = "code_c"
                title_para = "title_c"
                sub_title_para = "sub_title_c"
            Else
                code_para = "code"
                title_para = "title"
                sub_title_para = "sub_title"
            End If

            If txt_auction_code.Text.Trim() <> lbl_auction_code.Text Then
                updpara = updpara & code_para & "='" & CommonCode.encodeSingleQuote(txt_auction_code.Text.Trim()) & "'"
                is_update_required = True
            End If

            If txt_auction_title.Text.Trim() <> lbl_auction_title.Text.Trim() Then
                If is_update_required Then
                    updpara = updpara & "," & title_para & "='" & CommonCode.encodeSingleQuote(txt_auction_title.Text.Trim()) & "'"
                Else
                    updpara = updpara & title_para & "='" & CommonCode.encodeSingleQuote(txt_auction_title.Text.Trim()) & "'"
                End If
                is_update_required = True
            End If

            If txt_sub_title.Text.Trim() <> lbl_sub_title.Text.Trim() Then
                If is_update_required Then
                    updpara = updpara & "," & sub_title_para & "='" & CommonCode.encodeSingleQuote(txt_sub_title.Text.Trim()) & "'"
                Else
                    updpara = updpara & sub_title_para & "='" & CommonCode.encodeSingleQuote(txt_sub_title.Text.Trim()) & "'"
                End If
                is_update_required = True
            End If
            If Not ddl_product_category.SelectedItem Is Nothing Then
                If lbl_product_category.Text <> ddl_product_category.SelectedItem.Text Then
                    If lbl_product_category.Text <> "" Or ddl_product_category.SelectedValue <> "0" Then
                        If is_update_required Then
                            updpara = updpara & ",product_catetory_id='" & ddl_product_category.SelectedValue & "'"
                        Else
                            updpara = updpara & "product_catetory_id='" & ddl_product_category.SelectedValue & "'"
                        End If
                        is_update_required = True
                    End If
                End If
            End If
            If Not ddl_stock_condition.SelectedItem Is Nothing Then
                If lbl_stock_condition.Text <> ddl_stock_condition.SelectedItem.Text Then
                    If lbl_stock_condition.Text <> "" Then
                        If is_update_required Then
                            updpara = updpara & ",stock_condition_id='" & ddl_stock_condition.SelectedValue & "'"
                        Else
                            updpara = updpara & "stock_condition_id='" & ddl_stock_condition.SelectedValue & "'"
                        End If
                        is_update_required = True
                    End If
                End If
            Else
                If is_update_required Then
                    updpara = updpara & "," & "stock_condition_id=0"
                Else
                    updpara = updpara & "stock_condition_id=0"
                End If
                is_update_required = True
            End If

            If ddl_packaging.SelectedValue <> "" And ddl_packaging.SelectedValue <> "0" Then
                If lbl_packaging.Text <> ddl_packaging.SelectedItem.Text Then
                    If is_update_required Then
                        updpara = updpara & ",packaging_id='" & ddl_packaging.SelectedValue & "'"
                    Else
                        updpara = updpara & "packaging_id='" & ddl_packaging.SelectedValue & "'"
                    End If
                    is_update_required = True

                End If
            Else
                If is_update_required Then
                    updpara = updpara & "," & "packaging_id=0"
                Else
                    updpara = updpara & "packaging_id=0"
                End If
                is_update_required = True
            End If
            If Not ddl_seller.SelectedItem Is Nothing Then
                If lbl_seller.Text <> ddl_seller.SelectedItem.Text Then
                    If lbl_seller.Text <> "" Then
                        If is_update_required Then
                            updpara = updpara & ",seller_id='" & ddl_seller.SelectedValue & "'"
                        Else
                            updpara = updpara & "seller_id='" & ddl_seller.SelectedValue & "'"
                        End If
                        is_update_required = True
                    End If
                End If
            End If
            'If Not ddl_seller.SelectedItem Is Nothing Then
            '    If lbl_seller.Text <> ddl_seller.SelectedItem.Text Then
            '        If lbl_seller.Text <> "" Then
            '            If is_update_required Then
            '                updpara = updpara & ",use_your_shipping='" & ddl_seller.SelectedValue & "'"
            '            Else
            '                updpara = updpara & "use_your_shipping='" & ddl_seller.SelectedValue & "'"
            '            End If
            '            is_update_required = True
            '        End If
            '    End If
            'End If

            'If Not ddl_package_type.SelectedItem Is Nothing Then
            '    If lbl_package_type.Text <> ddl_package_type.SelectedItem.Text Then
            '            If is_update_required Then
            '                updpara = updpara & ",package_type='" & ddl_package_type.SelectedValue & "'"
            '            Else
            '                updpara = updpara & "package_type='" & ddl_package_type.SelectedValue & "'"
            '            End If
            '            is_update_required = True
            '    End If
            'Else
            '    If is_update_required Then
            '        updpara = updpara & "," & "package_type=''"
            '    Else
            '        updpara = updpara & "package_type=''"
            '    End If
            '    is_update_required = True
            'End If


            If Not ddl_stock_location.SelectedItem Is Nothing Then
                If ddl_stock_location.SelectedItem.Text <> lbl_stock_location.Text.Trim() Then
                    If is_update_required Then
                        updpara = updpara & "," & "stock_location_id='" & ddl_stock_location.SelectedItem.Value & "'"
                    Else
                        updpara = updpara & "stock_location_id='" & ddl_stock_location.SelectedItem.Value & "'"
                    End If
                    is_update_required = True

                End If
            Else
                If is_update_required Then
                    updpara = updpara & "," & "stock_location_id=0"
                Else
                    updpara = updpara & "stock_location_id=0"
                End If
                is_update_required = True
            End If
            If is_update_required = True Then
                updpara = updpara & ","
            End If

            is_update_required = True
            'updpara = updpara & "use_pcs_shipping='" & chk_pcs_shipping.Checked & "'"
            'updpara = updpara & ",use_your_shipping='" & chk_your_shipping.Checked & "'"
            updpara = updpara & "show_on_home_page='" & chk_home_page.Checked & "'"

            updpara = updpara & ",show_relevant_bidders_only='" & IIf(chk_relevant_bidders.Checked, 1, 0) & "'"
            'updpara = updpara & ",[weight]='" & IIf(txt_weight.Text.Trim = "", 0, txt_weight.Text.Trim) & "'"




            'UploadFileStockImages(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))

            If is_update_required Then
                updpara = updpara & ",update_date=getdate(),update_by_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
                SqlHelper.ExecuteNonQuery("update tbl_auctions set " & updpara & " where auction_id=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
                CommonCode.insert_system_log("Auction Basic Information updated", "btn_basic_update_Click", Request.QueryString.Get("i"), "", "Auction")
                load_auction_bidder_queries()
                lbl_msg_basic.Text = "Auction updated successfully."
            End If
            setBasicEdit(True)
            hid_basic_form_mode.Value = "view"
        End If
    End Sub

    Protected Sub btn_auction_active_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_auction_active.Click
        If hid_auction_status.Value = 1 Then
            SqlHelper.ExecuteNonQuery("update tbl_auctions set is_active=0 where auction_id=" & hid_auction_id.Value)
            Response.Redirect(Request.Url.ToString())
        Else
            Dim str As String = SqlHelper.ExecuteScalar("select dbo.can_auction_active(" & hid_auction_id.Value & ")")
            If str = "" Then
                SqlHelper.ExecuteNonQuery("update tbl_auctions set is_active=1 where auction_id=" & hid_auction_id.Value)
                Response.Redirect(Request.Url.ToString())
            Else
                'If chk_relevant_bidders.Checked = False Then
                '    SqlHelper.ExecuteNonQuery("update tbl_auctions set is_active=1 where auction_id=" & hid_auction_id.Value)
                '    Response.Redirect(Request.Url.ToString())
                'Else
                lbl_msg_basic.Text = str
                'End If


            End If

        End If

    End Sub
#End Region

#Region "Other Details"

    Private Sub setOtherDetailsEdit(ByVal readMode As Boolean)
        If Request.QueryString.Get("i") <> "0" Then

            fillOtherDetailsData(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
        Else
            EmptyOtherDetailsData()
        End If
        'visibleOtherDetailsData(readMode)
    End Sub
    Private Sub fillOtherDetailsData(ByVal auction_id As Integer)
        If auction_id > 0 Then
            Dim qry As String = "SELECT A.auction_id, " & _
              "ISNULL(A.description, '') AS description," & _              "ISNULL(A.description_f, '') AS description_f," & _              "ISNULL(A.description_s, '') AS description_s," & _              "ISNULL(A.description_c, '') AS description_c," & _              "ISNULL(A.short_description, '') AS short_description," & _              "ISNULL(A.short_description_f, '') AS short_description_f," & _              "ISNULL(A.short_description_s, '') AS short_description_s," & _              "ISNULL(A.short_description_c, '') AS short_description_c," & _                "ISNULL(A.special_conditions, '') AS special_conditions," & _                "ISNULL(A.special_conditions_f, '') AS special_conditions_f," & _              "ISNULL(A.special_conditions_s, '') AS special_conditions_s," & _              "ISNULL(A.special_conditions_c, '') AS special_conditions_c," & _              "ISNULL(A.payment_terms, '') AS payment_terms," & _              "ISNULL(A.payment_terms_f, '') AS payment_terms_f," & _              "ISNULL(A.payment_terms_s, '') AS payment_terms_s," & _              "ISNULL(A.payment_terms_c, '') AS payment_terms_c," & _             "ISNULL(A.shipping_terms, '') AS shipping_terms," & _                 "ISNULL(A.shipping_terms_f, '') AS shipping_terms_f," & _              "ISNULL(A.shipping_terms_s, '') AS shipping_terms_s," & _              "ISNULL(A.shipping_terms_c, '') AS shipping_terms_c," & _            "ISNULL(A.other_terms, '') AS other_terms," & _              "ISNULL(A.other_terms_f, '') AS other_terms_f," & _              "ISNULL(A.other_terms_s, '') AS other_terms_s," & _              "ISNULL(A.other_terms_c, '') AS other_terms_c, " & _              "ISNULL(A.tax_details, '') AS tax_details," & _              "ISNULL(A.tax_details_f, '') AS tax_details_f," & _              "ISNULL(A.tax_details_s, '') AS tax_details_s," & _              "ISNULL(A.tax_details_c, '') AS tax_details_c," & _              "ISNULL(A.product_details, '') AS product_details," & _              "ISNULL(A.product_details_f, '') AS product_details_f," & _              "ISNULL(A.product_details_s, '') AS product_details_s," & _              "ISNULL(A.product_details_c, '') AS product_details_c " & _              " FROM tbl_auctions A WITH (NOLOCK) WHERE A.auction_id = " & auction_id

            Dim dt As DataTable = New DataTable()
            dt = SqlHelper.ExecuteDatatable(qry)
            If dt.Rows.Count > 0 Then
                With dt.Rows(0)
                    If hid_language.Value.ToString = "2" Then 'french
                        lbl_description.Text = CommonCode.decodeSingleQuote(.Item("description_f"))
                        lbl_short_description.Text = CommonCode.decodeSingleQuote(.Item("short_description_f"))
                        lbl_special_condition.Text = CommonCode.decodeSingleQuote(.Item("special_conditions_f"))
                        lbl_payment_terms.Text = CommonCode.decodeSingleQuote(.Item("payment_terms_f"))
                        lbl_shipping_terms.Text = CommonCode.decodeSingleQuote(.Item("shipping_terms_f"))
                        lbl_other_terms_and_conditions.Text = CommonCode.decodeSingleQuote(.Item("other_terms_f"))
                        lbl_product_details.Text = CommonCode.decodeSingleQuote(.Item("product_details_f"))
                        lbl_tax_details.Text = CommonCode.decodeSingleQuote(.Item("tax_details_f"))
                    ElseIf hid_language.Value.ToString = "3" Then
                        lbl_description.Text = CommonCode.decodeSingleQuote(.Item("description_s"))
                        lbl_short_description.Text = CommonCode.decodeSingleQuote(.Item("short_description_s"))

                        lbl_special_condition.Text = CommonCode.decodeSingleQuote(.Item("special_conditions_s"))

                        lbl_payment_terms.Text = CommonCode.decodeSingleQuote(.Item("payment_terms_s"))

                        lbl_shipping_terms.Text = CommonCode.decodeSingleQuote(.Item("shipping_terms_s"))

                        lbl_other_terms_and_conditions.Text = CommonCode.decodeSingleQuote(.Item("other_terms_s"))
                        lbl_product_details.Text = CommonCode.decodeSingleQuote(.Item("product_details_s"))
                        lbl_tax_details.Text = CommonCode.decodeSingleQuote(.Item("tax_details_s"))
                    ElseIf hid_language.Value.ToString = "4" Then
                        lbl_description.Text = CommonCode.decodeSingleQuote(.Item("description_c"))
                        lbl_short_description.Text = CommonCode.decodeSingleQuote(.Item("short_description_c"))

                        lbl_special_condition.Text = CommonCode.decodeSingleQuote(.Item("special_conditions_c"))

                        lbl_payment_terms.Text = CommonCode.decodeSingleQuote(.Item("payment_terms_c"))

                        lbl_shipping_terms.Text = CommonCode.decodeSingleQuote(.Item("shipping_terms_c"))

                        lbl_other_terms_and_conditions.Text = CommonCode.decodeSingleQuote(.Item("other_terms_c"))
                        lbl_product_details.Text = CommonCode.decodeSingleQuote(.Item("product_details_c"))
                        lbl_tax_details.Text = CommonCode.decodeSingleQuote(.Item("tax_details_c"))
                    Else
                        lbl_description.Text = CommonCode.decodeSingleQuote(.Item("description"))
                        lbl_short_description.Text = CommonCode.decodeSingleQuote(.Item("short_description"))

                        lbl_special_condition.Text = CommonCode.decodeSingleQuote(.Item("special_conditions"))

                        lbl_payment_terms.Text = CommonCode.decodeSingleQuote(.Item("payment_terms"))

                        lbl_shipping_terms.Text = CommonCode.decodeSingleQuote(.Item("shipping_terms"))

                        lbl_other_terms_and_conditions.Text = CommonCode.decodeSingleQuote(.Item("other_terms"))
                        lbl_product_details.Text = CommonCode.decodeSingleQuote(.Item("product_details"))
                        lbl_tax_details.Text = CommonCode.decodeSingleQuote(.Item("tax_details"))
                    End If

                End With

            End If
            lbl_description.Text = IIf(lbl_description.Text.Trim <> "", lbl_description.Text.Trim, "<span style='color:red;'>Description not available.</span>")
            lbl_short_description.Text = IIf(lbl_short_description.Text.Trim <> "", lbl_short_description.Text.Trim, "<span style='color:red;'>Condition not available.</span>")
            lbl_special_condition.Text = IIf(lbl_special_condition.Text.Trim <> "", lbl_special_condition.Text.Trim, "<span style='color:red;'>Special condition not available.</span>")
            lbl_payment_terms.Text = IIf(lbl_payment_terms.Text.Trim <> "", lbl_payment_terms.Text.Trim, "<span style='color:red;'>Payment terms not available.</span>")
            lbl_shipping_terms.Text = IIf(lbl_shipping_terms.Text.Trim <> "", lbl_shipping_terms.Text.Trim, "<span style='color:red;'>Shipping terms not available.</span>")
            lbl_other_terms_and_conditions.Text = IIf(lbl_other_terms_and_conditions.Text.Trim <> "", lbl_other_terms_and_conditions.Text.Trim, "<span style='color:red;'>Other terms not available.</span>")
            lbl_product_details.Text = IIf(lbl_product_details.Text.Trim <> "", lbl_product_details.Text.Trim, "<span style='color:red;'>Product details not available.</span>")
            lbl_tax_details.Text = IIf(lbl_tax_details.Text.Trim <> "", lbl_tax_details.Text.Trim, "<span style='color:red;'>Tax details not available.</span>")

            dt = Nothing
        End If

        Dim dt3 As DataTable = New DataTable()
        dt3 = SqlHelper.ExecuteDatatable("select top 1 payment_term_attachment_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_payment_term_attachments where auction_id=" & auction_id & " order by payment_term_attachment_id desc")
        If dt3.Rows.Count > 0 Then
            With dt3.Rows(0)
                Dim file_name As String = ""

                If hid_language.Value = "2" Then
                    file_name = .Item("filename_f")
                ElseIf hid_language.Value = "3" Then
                    file_name = .Item("filename_s")
                ElseIf hid_language.Value = "4" Then
                    file_name = .Item("filename_c")
                Else
                    file_name = .Item("filename")
                End If
                'HID_payment_term_attachment_id.Value = .Item("payment_term_attachment_id")
                If file_name <> "" Then
                    lbl_attach_payment_terms.Text = "<a href='/Upload/Auctions/payment_term_attachments/" & auction_id & "/" & .Item("payment_term_attachment_id") & "/" & file_name & "' target='_blank'>" & file_name & "</a>"
                Else
                    lbl_attach_payment_terms.Text = ""
                End If

            End With

        End If
        dt3 = Nothing

        Dim dt4 As DataTable = New DataTable()
        dt4 = SqlHelper.ExecuteDatatable("select top 1 shipping_term_attachment_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_shipping_term_attachments where auction_id=" & auction_id & " order by shipping_term_attachment_id desc")
        If dt4.Rows.Count > 0 Then
            With dt4.Rows(0)
                Dim file_name As String = ""
                If hid_language.Value = "2" Then
                    file_name = .Item("filename_f")
                ElseIf hid_language.Value = "3" Then
                    file_name = .Item("filename_s")
                ElseIf hid_language.Value = "4" Then
                    file_name = .Item("filename_c")
                Else
                    file_name = .Item("filename")
                End If
                'HID_shipping_term_attachment_id.Value = .Item("shipping_term_attachment_id")
                If file_name <> "" Then
                    lbl_attach_shipping_terms.Text = "<a href='/Upload/Auctions/shipping_term_attachments/" & auction_id & "/" & .Item("shipping_term_attachment_id") & "/" & file_name & "' target='_blank'>" & file_name & "</a>"
                Else
                    lbl_attach_shipping_terms.Text = ""
                End If
            End With
        End If
        dt4 = Nothing

        Dim dt5 As DataTable = New DataTable()
        dt5 = SqlHelper.ExecuteDatatable("select top 1 term_cond_attachment_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_terms_cond_attachments where auction_id=" & auction_id & " order by term_cond_attachment_id desc")
        If dt5.Rows.Count > 0 Then
            With dt5.Rows(0)
                Dim file_name As String = ""
                If hid_language.Value = "2" Then
                    file_name = .Item("filename_f")
                ElseIf hid_language.Value = "3" Then
                    file_name = .Item("filename_s")
                ElseIf hid_language.Value = "4" Then
                    file_name = .Item("filename_c")
                Else
                    file_name = .Item("filename")
                End If
                'HID_term_cond_attachment_id.Value = .Item("term_cond_attachment_id")
                If file_name <> "" Then
                    lbl_attach_other_terms_and_conditions.Text = "<a href='/Upload/Auctions/terms_cond_attachments/" & auction_id & "/" & .Item("term_cond_attachment_id") & "/" & file_name & "' target='_blank'>" & file_name & "</a>"
                Else
                    lbl_attach_other_terms_and_conditions.Text = ""
                End If
            End With

        End If
        dt5 = Nothing

        Dim dt6 As DataTable = New DataTable()
        dt6 = SqlHelper.ExecuteDatatable("select top 1 product_attachment_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_product_attachments where auction_id=" & auction_id & " order by product_attachment_id desc")
        If dt6.Rows.Count > 0 Then
            With dt6.Rows(0)
                Dim file_name As String = ""
                If hid_language.Value = "2" Then
                    file_name = .Item("filename_f")
                ElseIf hid_language.Value = "3" Then
                    file_name = .Item("filename_s")
                ElseIf hid_language.Value = "4" Then
                    file_name = .Item("filename_c")
                Else
                    file_name = .Item("filename")
                End If
                'HID_term_cond_attachment_id.Value = .Item("term_cond_attachment_id")
                If file_name <> "" Then
                    lbl_attach_product_details.Text = "<a href='/Upload/Auctions/product_attachments/" & auction_id & "/" & .Item("product_attachment_id") & "/" & file_name & "' target='_blank'>" & file_name & "</a>"
                Else
                    lbl_attach_product_details.Text = ""
                End If
            End With

        End If
        dt6 = Nothing

        Dim dt7 As DataTable = New DataTable()
        dt7 = SqlHelper.ExecuteDatatable("select top 1 tax_attachment_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_tax_attachments where auction_id=" & auction_id & " order by tax_attachment_id desc")
        If dt7.Rows.Count > 0 Then
            With dt7.Rows(0)
                Dim file_name As String = ""
                If hid_language.Value = "2" Then
                    file_name = .Item("filename_f")
                ElseIf hid_language.Value = "3" Then
                    file_name = .Item("filename_s")
                ElseIf hid_language.Value = "4" Then
                    file_name = .Item("filename_c")
                Else
                    file_name = .Item("filename")
                End If
                'HID_term_cond_attachment_id.Value = .Item("term_cond_attachment_id")
                If file_name <> "" Then
                    lbl_attach_tax_details.Text = "<a href='/Upload/Auctions/tax_attachments/" & auction_id & "/" & .Item("tax_attachment_id") & "/" & file_name & "' target='_blank'>" & file_name & "</a>"
                Else
                    lbl_attach_tax_details.Text = ""
                End If
            End With

        End If
        dt7 = Nothing

        Dim dt8 As DataTable = New DataTable()
        dt8 = SqlHelper.ExecuteDatatable("select top 1 special_condition_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_special_conditions where auction_id=" & auction_id & " order by special_condition_id desc")
        If dt8.Rows.Count > 0 Then
            With dt8.Rows(0)
                Dim file_name As String = ""
                If hid_language.Value = "2" Then
                    file_name = .Item("filename_f")
                ElseIf hid_language.Value = "3" Then
                    file_name = .Item("filename_s")
                ElseIf hid_language.Value = "4" Then
                    file_name = .Item("filename_c")
                Else
                    file_name = .Item("filename")
                End If
                'HID_term_cond_attachment_id.Value = .Item("term_cond_attachment_id")
                If file_name <> "" Then
                    lbl_attach_special_conditions.Text = "<a href='/Upload/Auctions/special_conditions/" & auction_id & "/" & .Item("special_condition_id") & "/" & file_name & "' target='_blank'>" & file_name & "</a>"
                Else
                    lbl_attach_special_conditions.Text = ""
                End If
            End With

        End If
        dt8 = Nothing
    End Sub
    Private Sub EmptyOtherDetailsData()
        'lbl_product_details.Text = ""
        'lbl_tax_details.Text = ""

        'txt_description.Text = ""
        'lbl_description.Text = ""
        'txt_special_condition.Text = ""
        'lbl_special_condition.Text = ""
        'txt_payment_terms.Text = ""
        'lbl_payment_terms.Text = ""
        'txt_shipping_terms.Text = ""
        'lbl_shipping_terms.Text = ""
        'txt_other_terms_and_conditions.Text = ""
        'lbl_other_terms_and_conditions.Text = ""

        'lbl_attach_payment_terms.Text = ""
        'lbl_attach_shipping_terms.Text = ""
        'lbl_attach_other_terms_and_conditions.Text = ""

    End Sub
    'Private Sub visibleOtherDetailsData(ByVal readMode As Boolean)

    'If readMode Then
    '    'btn_other_edit.Visible = True
    '    'btn_other_update.Visible = False
    '    'div_other_cancel.Visible = False
    '    lbl_view_description.Text = "<a href='javascript:void(0);' onclick=""return open_pop_win('d','v');"">Edit</a>"
    '    lbl_view_payment_terms.Text = "<a href='javascript:void(0);' onclick=""return open_pop_win('p','v');"">Edit</a>"
    '    lbl_view_shipping_terms.Text = "<a href='javascript:void(0);' onclick=""return open_pop_win('s','v');"">Edit</a>"
    '    lbl_view_special_condition.Text = "<a href='javascript:void(0);' onclick=""return open_pop_win('c','v');"">Edit</a>"
    '    lbl_view_other_terms_and_conditions.Text = "<a href='javascript:void(0);' onclick=""return open_pop_win('o','v');"">Edit</a>"
    '    lbl_view_short_description.Text = "<a href='javascript:void(0);' onclick=""return open_pop_win('h','v');"">Edit</a>"
    'Else
    '    'btn_other_edit.Visible = False
    '    'btn_other_update.Visible = True
    '    'div_other_cancel.Visible = True
    '    lbl_view_description.Text = "<a href='javascript:void(0);' onclick=""return open_pop_win('d','e');"">Edit</a>"
    '    lbl_view_payment_terms.Text = "<a href='javascript:void(0);' onclick=""return open_pop_win('p','e');"">Edit</a>"
    '    lbl_view_shipping_terms.Text = "<a href='javascript:void(0);' onclick=""return open_pop_win('s','e');"">Edit</a>"
    '    lbl_view_special_condition.Text = "<a href='javascript:void(0);' onclick=""return open_pop_win('c','e');"">Edit</a>"
    '    lbl_view_other_terms_and_conditions.Text = "<a href='javascript:void(0);' onclick=""return open_pop_win('o','e');"">Edit</a>"
    '    lbl_view_short_description.Text = "<a href='javascript:void(0);' onclick=""return open_pop_win('h','e');"">Edit</a>"
    'End If

    'txt_description.Visible = Not readMode
    'lbl_description.Visible = readMode

    'txt_special_condition.Visible = Not readMode
    'lbl_special_condition.Visible = readMode

    'txt_payment_terms.Visible = Not readMode
    'lbl_payment_terms.Visible = readMode
    'txt_shipping_terms.Visible = Not readMode
    'lbl_shipping_terms.Visible = readMode
    'txt_other_terms_and_conditions.Visible = Not readMode
    'lbl_other_terms_and_conditions.Visible = readMode



    'File_attach_payment_terms.Visible = Not readMode
    'If lbl_attach_payment_terms.Text <> "" Then
    '    remove_attach_payment_terms.Visible = Not readMode
    'Else
    '    remove_attach_payment_terms.Visible = False
    'End If

    'File_attach_shipping_terms.Visible = Not readMode
    'If lbl_attach_shipping_terms.Text <> "" Then
    '    remove_attach_shipping_terms.Visible = Not readMode
    'Else
    '    remove_attach_shipping_terms.Visible = False
    'End If


    'File_attach_other_terms_and_conditions.Visible = Not readMode
    'If lbl_attach_other_terms_and_conditions.Text <> "" Then
    '    remove_attach_other_terms_and_conditions.Visible = Not readMode
    'Else
    '    remove_attach_other_terms_and_conditions.Visible = False
    'End If



    'End Sub
    'Protected Sub btn_other_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_other_edit.Click
    '    visibleOtherDetailsData(False)
    'End Sub
    'Protected Sub btn_other_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_other_cancel.Click
    '    visibleOtherDetailsData(True)
    'End Sub
    'Protected Sub btn_other_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_other_update.Click
    '    If Page.IsValid Then

    '        Dim is_update_required As Boolean = False
    '        Dim updpara As String = ""


    '        'If txt_description.Text.Trim() <> lbl_description.Text.Trim() Then
    '        '    updpara = updpara & "description='" & CommonCode.encodeSingleQuote(txt_description.Text.Trim()) & "'"
    '        '    is_update_required = True
    '        'End If

    '        'If txt_special_condition.Text.Trim() <> lbl_special_condition.Text.Trim() Then
    '        '    If is_update_required Then
    '        '        updpara = updpara & ",special_conditions='" & CommonCode.encodeSingleQuote(txt_special_condition.Text.Trim()) & "'"
    '        '    Else
    '        '        updpara = updpara & "special_conditions='" & CommonCode.encodeSingleQuote(txt_special_condition.Text.Trim()) & "'"
    '        '    End If
    '        '    is_update_required = True
    '        'End If

    '        'If txt_payment_terms.Text.Trim() <> lbl_payment_terms.Text.Trim() Then
    '        '    If is_update_required Then
    '        '        updpara = updpara & ",payment_terms='" & CommonCode.encodeSingleQuote(txt_payment_terms.Text.Trim()) & "'"
    '        '    Else
    '        '        updpara = updpara & "payment_terms='" & CommonCode.encodeSingleQuote(txt_payment_terms.Text.Trim()) & "'"
    '        '    End If
    '        '    is_update_required = True
    '        'End If

    '        'If txt_shipping_terms.Text.Trim() <> lbl_shipping_terms.Text.Trim() Then
    '        '    If is_update_required Then
    '        '        updpara = updpara & ",shipping_terms='" & CommonCode.encodeSingleQuote(txt_shipping_terms.Text.Trim()) & "'"
    '        '    Else
    '        '        updpara = updpara & "shipping_terms='" & CommonCode.encodeSingleQuote(txt_shipping_terms.Text.Trim()) & "'"
    '        '    End If
    '        '    is_update_required = True
    '        'End If

    '        'If txt_other_terms_and_conditions.Text.Trim() <> lbl_other_terms_and_conditions.Text.Trim() Then
    '        '    If is_update_required Then
    '        '        updpara = updpara & ",other_terms='" & CommonCode.encodeSingleQuote(txt_other_terms_and_conditions.Text.Trim()) & "'"
    '        '    Else
    '        '        updpara = updpara & "other_terms='" & CommonCode.encodeSingleQuote(txt_other_terms_and_conditions.Text.Trim()) & "'"
    '        '    End If
    '        '    is_update_required = True
    '        'End If


    '        UploadFileProductDetails(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
    '        UploadFileTaxDetails(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
    '        'UploadFilePaymentTerms(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
    '        'UploadFileShippingTerms(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
    '        ' UploadFileTermsConditions(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))

    '        If is_update_required Then
    '            updpara = updpara & ",update_date=getdate(),update_by_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")

    '            SqlHelper.ExecuteNonQuery("update tbl_auctions set " & updpara & " where auction_id=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))

    '        End If
    '        setOtherDetailsEdit(True)
    '    End If
    'End Sub
    'Private Sub UploadFileProductDetails(ByVal auction_id As Integer)

    '    Try
    '        Dim filename As String = ""
    '        Dim pathToCreate As String = "../Upload/Auctions/product_attachments/" + auction_id.ToString()
    '        Dim filename_para As String = ""
    '        If hid_language.Value.ToString = "2" Then
    '            filename_para = "filename_f"
    '        ElseIf hid_language.Value.ToString = "3" Then
    '            filename_para = "filename_s"
    '        ElseIf hid_language.Value.ToString = "4" Then
    '            filename_para = "filename_c"
    '        Else
    '            filename_para = "filename"
    '        End If
    '        If remove_product_details.Checked And HID_product_attachment_id.Value <> "0" Then
    '            lbl_product_details.Text = ""

    '            filename = SqlHelper.ExecuteScalar("select " & filename_para & " from tbl_auction_product_attachments where product_attachment_id=" & HID_product_attachment_id.Value & " order by product_attachment_id desc")
    '            SqlHelper.ExecuteNonQuery("update tbl_auction_product_attachments set " & filename_para & "='' where product_attachment_id=" & HID_product_attachment_id.Value)
    '            Dim is_deleted As Integer = 0
    '            is_deleted = SqlHelper.ExecuteScalar("if exists(select product_attachment_id from tbl_auction_product_attachments where product_attachment_id=" & HID_product_attachment_id.Value & " AND ISNULL(filename,'')='' AND ISNULL(filename_f,'')='' AND ISNULL(filename_s,'')='' AND ISNULL(filename_c,'')='') Begin delete tbl_auction_product_attachments where product_attachment_id=" & HID_product_attachment_id.Value & " select 1 End else Begin select 0 End")
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & HID_product_attachment_id.Value & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & HID_product_attachment_id.Value & "/" & filename)
    '            End If
    '            If is_deleted = 1 Then
    '                HID_product_attachment_id.Value = "0"
    '            End If

    '        End If
    '        remove_product_details.Checked = False
    '        filename = ""

    '        If File_product_details.HasFile Then

    '            filename = File_product_details.FileName
    '            lbl_product_details.Text = filename
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If

    '            Dim product_attachment_id As Integer = 0

    '            If HID_product_attachment_id.Value <> "0" Then
    '                product_attachment_id = HID_product_attachment_id.Value
    '                SqlHelper.ExecuteNonQuery("update tbl_auction_product_attachments set " & filename_para & "='" & filename & "' where product_attachment_id=" & product_attachment_id)
    '            Else
    '                Dim qry As String = "insert into tbl_auction_product_attachments(auction_id," & filename_para & ",submit_date,submit_by_user_id) values(" & auction_id & ",'" & filename & "',getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ")  select scope_identity()"

    '                product_attachment_id = SqlHelper.ExecuteScalar(qry)
    '            End If

    '            pathToCreate = pathToCreate & "/" & product_attachment_id
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
    '            End If
    '            File_product_details.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

    '        End If
    '    Catch ex As Exception

    '        'RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))
    '    End Try
    'End Sub
    'Private Sub UploadFileTaxDetails(ByVal auction_id As Integer)
    '    Try

    '        Dim filename As String = ""
    '        Dim pathToCreate As String = "../Upload/Auctions/tax_attachments/" + auction_id.ToString()
    '        Dim filename_para As String = ""
    '        If hid_language.Value.ToString = "2" Then
    '            filename_para = "filename_f"
    '        ElseIf hid_language.Value.ToString = "3" Then
    '            filename_para = "filename_s"
    '        ElseIf hid_language.Value.ToString = "4" Then
    '            filename_para = "filename_c"
    '        Else
    '            filename_para = "filename"
    '        End If
    '        If remove_tax_details.Checked And HID_tax_attachment_id.Value <> "0" Then
    '            lbl_tax_details.Text = ""
    '            filename = SqlHelper.ExecuteScalar("select " & filename_para & " from tbl_auction_tax_attachments where tax_attachment_id=" & HID_tax_attachment_id.Value & " order by tax_attachment_id desc")
    '            SqlHelper.ExecuteNonQuery("update tbl_auction_tax_attachments set " & filename_para & "='' where tax_attachment_id=" & HID_tax_attachment_id.Value)
    '             Dim is_deleted As Integer = 0
    '            is_deleted = SqlHelper.ExecuteScalar("if exists(select tax_attachment_id from tbl_auction_tax_attachments where tax_attachment_id=" & HID_tax_attachment_id.Value & " AND filename='' AND filename_f='' AND filename_s='' AND filename_c='') Begin delete tbl_auction_tax_attachments where tax_attachment_id=" & HID_tax_attachment_id.Value & " Select 1 End Else Begin Select 0 End")
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & HID_tax_attachment_id.Value & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & HID_tax_attachment_id.Value & "/" & filename)
    '            End If
    '            If is_deleted = 1 Then
    '                HID_tax_attachment_id.Value = "0"
    '            End If

    '        End If
    '        remove_tax_details.Checked = False
    '        filename = ""

    '        If File_tax_details.HasFile Then
    '            filename = File_tax_details.FileName
    '            lbl_tax_details.Text = filename
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If

    '            Dim tax_attachment_id As Integer = 0

    '            If HID_tax_attachment_id.Value <> "0" Then
    '                tax_attachment_id = HID_tax_attachment_id.Value
    '                SqlHelper.ExecuteNonQuery("update tbl_auction_tax_attachments set " & filename_para & "='" & filename & "' where tax_attachment_id=" & tax_attachment_id)
    '            Else
    '                Dim qry As String = "insert into tbl_auction_tax_attachments(auction_id," & filename_para & ",submit_date,submit_by_user_id) values(" & auction_id & ",'" & filename & "',getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ")  select scope_identity()"
    '                tax_attachment_id = SqlHelper.ExecuteScalar(qry)
    '            End If

    '            pathToCreate = pathToCreate & "/" & tax_attachment_id
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
    '            End If
    '            File_tax_details.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

    '        End If
    '    Catch ex As Exception
    '        'RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))
    '    End Try
    'End Sub
    'Private Sub UploadFilePaymentTerms(ByVal auction_id As Integer)
    '    Try

    '        Dim filename As String = ""
    '        Dim pathToCreate As String = "../Upload/Auctions/payment_term_attachments/" + auction_id.ToString()

    '        If remove_attach_payment_terms.Checked And HID_payment_term_attachment_id.Value <> "0" Then
    '            lbl_attach_payment_terms.Text = ""
    '            filename = SqlHelper.ExecuteScalar("select filename from tbl_auction_payment_term_attachments where payment_term_attachment_id=" & HID_payment_term_attachment_id.Value & " order by payment_term_attachment_id desc")
    '            SqlHelper.ExecuteNonQuery("delete tbl_auction_payment_term_attachments where payment_term_attachment_id=" & HID_payment_term_attachment_id.Value)
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & HID_payment_term_attachment_id.Value & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & HID_payment_term_attachment_id.Value & "/" & filename)
    '            End If
    '            HID_payment_term_attachment_id.Value = "0"
    '        End If
    '        remove_attach_payment_terms.Checked = False
    '        filename = ""


    '        If File_attach_payment_terms.HasFile Then
    '            filename = File_attach_payment_terms.FileName
    '            lbl_attach_payment_terms.Text = filename
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If

    '            Dim payment_term_attachment_id As Integer = 0

    '            If HID_payment_term_attachment_id.Value <> "0" Then
    '                payment_term_attachment_id = HID_payment_term_attachment_id.Value
    '                SqlHelper.ExecuteNonQuery("update tbl_auction_payment_term_attachments set filename='" & filename & "' where payment_term_attachment_id=" & payment_term_attachment_id)
    '            Else
    '                Dim qry As String = "insert into tbl_auction_payment_term_attachments(auction_id,filename,submit_date,submit_by_user_id) values(" & auction_id & ",'" & filename & "',getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ")  select scope_identity()"
    '                payment_term_attachment_id = SqlHelper.ExecuteScalar(qry)
    '            End If

    '            pathToCreate = pathToCreate & "/" & payment_term_attachment_id
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
    '            End If
    '            File_attach_payment_terms.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

    '        End If
    '    Catch ex As Exception
    '        'RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))
    '    End Try
    'End Sub
    'Private Sub UploadFileShippingTerms(ByVal auction_id As Integer)
    '    Try

    '        Dim filename As String = ""
    '        Dim pathToCreate As String = "../Upload/Auctions/shipping_term_attachments/" + auction_id.ToString()

    '        If remove_attach_shipping_terms.Checked And HID_shipping_term_attachment_id.Value <> "0" Then
    '            lbl_attach_shipping_terms.Text = ""
    '            filename = SqlHelper.ExecuteScalar("select filename from tbl_auction_shipping_term_attachments where shipping_term_attachment_id=" & HID_shipping_term_attachment_id.Value & " order by shipping_term_attachment_id desc")
    '            SqlHelper.ExecuteNonQuery("delete tbl_auction_shipping_term_attachments where shipping_term_attachment_id=" & HID_shipping_term_attachment_id.Value)
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & HID_shipping_term_attachment_id.Value & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & HID_shipping_term_attachment_id.Value & "/" & filename)
    '            End If
    '            HID_shipping_term_attachment_id.Value = "0"
    '        End If
    '        remove_attach_shipping_terms.Checked = False
    '        filename = ""


    '        If File_attach_shipping_terms.HasFile Then
    '            filename = File_attach_shipping_terms.FileName
    '            lbl_attach_shipping_terms.Text = filename
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If

    '            Dim shipping_term_attachment_id As Integer = 0

    '            If HID_shipping_term_attachment_id.Value <> "0" Then
    '                shipping_term_attachment_id = HID_shipping_term_attachment_id.Value
    '                SqlHelper.ExecuteNonQuery("update tbl_auction_shipping_term_attachments set filename='" & filename & "' where shipping_term_attachment_id=" & shipping_term_attachment_id)
    '            Else
    '                Dim qry As String = "insert into tbl_auction_shipping_term_attachments(auction_id,filename,submit_date,submit_by_user_id) values(" & auction_id & ",'" & filename & "',getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ")  select scope_identity()"
    '                shipping_term_attachment_id = SqlHelper.ExecuteScalar(qry)
    '            End If

    '            pathToCreate = pathToCreate & "/" & shipping_term_attachment_id
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
    '            End If
    '            File_attach_shipping_terms.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

    '        End If
    '    Catch ex As Exception
    '        'RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))
    '    End Try
    'End Sub
    'Private Sub UploadFileTermsConditions(ByVal auction_id As Integer)
    '    Try

    '        Dim filename As String = ""
    '        Dim pathToCreate As String = "../Upload/Auctions/terms_cond_attachments/" + auction_id.ToString()

    '        If remove_attach_other_terms_and_conditions.Checked And HID_term_cond_attachment_id.Value <> "0" Then
    '            lbl_attach_other_terms_and_conditions.Text = ""
    '            filename = SqlHelper.ExecuteScalar("select filename from tbl_auction_terms_cond_attachments where term_cond_attachment_id=" & HID_term_cond_attachment_id.Value & " order by term_cond_attachment_id desc")
    '            SqlHelper.ExecuteNonQuery("delete tbl_auction_terms_cond_attachments where term_cond_attachment_id=" & HID_term_cond_attachment_id.Value)
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & HID_term_cond_attachment_id.Value & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & HID_term_cond_attachment_id.Value & "/" & filename)
    '            End If
    '            HID_term_cond_attachment_id.Value = "0"
    '        End If
    '        remove_attach_other_terms_and_conditions.Checked = False
    '        filename = ""

    '        If File_attach_other_terms_and_conditions.HasFile Then
    '            filename = File_attach_other_terms_and_conditions.FileName
    '            lbl_attach_other_terms_and_conditions.Text = filename
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If

    '            Dim term_cond_attachment_id As Integer = 0

    '            If HID_term_cond_attachment_id.Value <> "0" Then
    '                term_cond_attachment_id = HID_term_cond_attachment_id.Value
    '                SqlHelper.ExecuteNonQuery("update tbl_auction_terms_cond_attachments set filename='" & filename & "' where term_cond_attachment_id=" & term_cond_attachment_id)
    '            Else
    '                Dim qry As String = "insert into tbl_auction_terms_cond_attachments(auction_id,filename,submit_date,submit_by_user_id) values(" & auction_id & ",'" & filename & "',getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ")  select scope_identity()"
    '                term_cond_attachment_id = SqlHelper.ExecuteScalar(qry)
    '            End If

    '            pathToCreate = pathToCreate & "/" & term_cond_attachment_id
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
    '            End If
    '            File_attach_other_terms_and_conditions.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

    '        End If
    '    Catch ex As Exception
    '        'RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))
    '    End Try
    'End Sub

#End Region

#Region "Attachments"

    Protected Sub RadGrid_Attachment_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        RadGrid_Attachment.DataSource = SqlHelper.ExecuteDatatable("SELECT attachment_id,title,filename,isnull(for_bidder,'True') as for_bidder FROM tbl_auction_attachments WHERE auction_id = '" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & "' order by upload_date desc")
    End Sub
    Protected Sub RadGrid_Attachment_DeleteCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)

        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        Dim attachment_id As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("attachment_id").ToString()
        Try
            SqlHelper.ExecuteNonQuery("DELETE from tbl_auction_attachments where attachment_id='" & attachment_id & "'")
            CommonCode.insert_system_log("Auction attachment deleted", "RadGrid_Attachment_DeleteCommand", Request.QueryString.Get("i"), "", "Auction")
        Catch ex As Exception
            RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to delete Attachment. Reason: " + ex.Message))
            e.Canceled = True
        End Try

    End Sub
    Protected Sub RadGrid_Attachment_UpdateCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditableItem of the RadGrid   

        Dim editedItem As GridEditFormItem = TryCast(e.Item, GridEditFormItem)
        ''Get the primary key value using the DataKeyValue.        
        Dim attachment_id As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("attachment_id").ToString()
        ''Access the textbox from the edit form template and store the values in string variables.        

        Dim Title As String = (TryCast(editedItem.FindControl("txt_attach_title"), TextBox)).Text

        Dim filename As String = ""
        Dim visibleto As String = (TryCast(editedItem.FindControl("drop_visibleto"), RadComboBox)).SelectedValue
        ' Response.Write("<script>alert('" & visibleto & "')</script>")
        Dim FileUpload As System.Web.UI.WebControls.FileUpload = TryCast(editedItem.FindControl("FileUpload1"), System.Web.UI.WebControls.FileUpload)
        If FileUpload.HasFile Then filename = FileUpload.FileName

        If filename = "" Then

            SqlHelper.ExecuteNonQuery("update tbl_auction_attachments set title='" & CommonCode.encodeSingleQuote(Title) & "',for_bidder='" & visibleto & "' where attachment_id=" & attachment_id)
            CommonCode.insert_system_log("Auction attachment updated", "RadGrid_Attachment_UpdateCommand", Request.QueryString.Get("i"), "", "Auction")
        Else
            SqlHelper.ExecuteNonQuery("update tbl_auction_attachments set title='" & CommonCode.encodeSingleQuote(Title) & "', filename='" & filename & "',for_bidder='" & visibleto & "' where attachment_id=" & attachment_id)
            UploadFileAttachment(FileUpload, IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")), attachment_id)
            CommonCode.insert_system_log("Auction attachment updated", "RadGrid_Attachment_UpdateCommand", Request.QueryString.Get("i"), "", "Auction")
        End If


    End Sub
    Protected Sub RadGrid_Attachment_InsertCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditFormInsertItem of the RadGrid        
        Dim insertedItem As GridEditFormInsertItem = DirectCast(e.Item, GridEditFormInsertItem)
        Dim attachment_id As Integer = 0
        Dim Title As String = (TryCast(insertedItem.FindControl("txt_attach_title"), TextBox)).Text
        Dim filename As String = ""
        Dim FileUpload As System.Web.UI.WebControls.FileUpload = TryCast(insertedItem.FindControl("FileUpload1"), System.Web.UI.WebControls.FileUpload)
        Dim visibleto As String = (TryCast(insertedItem.FindControl("drop_visibleto"), RadComboBox)).SelectedItem.Value
        If FileUpload.HasFile Then
            filename = FileUpload.FileName
        End If

        Try
            If filename <> "" Then
                attachment_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_auction_attachments(auction_id, title, filename, upload_date, upload_by_user_id,for_bidder) VALUES (" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & ", '" & CommonCode.encodeSingleQuote(Title) & "', '" & filename & "', getdate(), " & CommonCode.Fetch_Cookie_Shared("user_id") & ",'" & visibleto & "')   select scope_identity()")
                UploadFileAttachment(FileUpload, IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")), attachment_id)
                CommonCode.insert_system_log("Auction attachment uploaded", "RadGrid_Attachment_InsertCommand", Request.QueryString.Get("i"), "", "Auction")
            End If
        Catch ex As Exception
            RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to save attachement. Reason: " + ex.Message))
            e.Canceled = True
        End Try

    End Sub
    Private Sub UploadFileAttachment(ByVal FileUpload As System.Web.UI.WebControls.FileUpload, ByVal auction_id As Integer, ByVal attachment_id As Integer)
        Try
            Dim filename As String = ""
            If FileUpload.HasFile Then
                filename = FileUpload.FileName

            End If

            Dim pathToCreate As String = "../Upload/Auctions/auction_attachments/" + IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i"))
            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
            End If

            pathToCreate = pathToCreate & "/" & attachment_id
            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
            End If
            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
            If infoFile.Exists Then
                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
            End If
            FileUpload.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

        Catch ex As Exception
            RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))
        End Try
    End Sub

#End Region

#Region "Bidding Details"

    Private Sub loadDetailsDropDownList()
        'ddl_auction_category.DataSource = SqlHelper.ExecuteDataView("SELECT auction_category_id,name FROM [tbl_auction_categories] Order By name")
        'ddl_auction_category.DataBind()

        ddl_auction_type.DataSource = SqlHelper.ExecuteDataView("SELECT auction_type_id,name FROM [tbl_auction_types] where is_active=1 Order By sno")
        ddl_auction_type.DataBind()

    End Sub

    Private Sub setDetailsEdit(ByVal readMode As Boolean)
        If Request.QueryString.Get("i") <> "0" Then
            fillDetailsData(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
        End If
        visibleDetailsData(readMode)
    End Sub
    Private Sub setDetailsEditSchedule(ByVal readMode As Boolean)
        'If Request.QueryString.Get("i") <> "0" Then
        '    fillDetailsData(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
        'End If
        visibleDetailsDataSchedule(readMode)
    End Sub
    Private Sub fillDetailsData(ByVal auction_id As Integer)
        Dim qry As String = "SELECT A.auction_id, " & _
    "ISNULL(A.auction_category_id, 0) AS auction_category_id," & _    "ISNULL(A.auction_type_id, 0) AS auction_type_id," & _    "ISNULL(A.is_private_offer, 0) AS is_private_offer," & _    "ISNULL(A.is_partial_offer, 0) AS is_partial_offer," & _    "ISNULL(A.is_buy_it_now, 0) AS is_buy_it_now," & _    "ISNULL(A.auto_acceptance_price_private, 0) AS auto_acceptance_price_private," & _    "ISNULL(A.private_lower_amt, 0) AS private_lower_amt," & _    "ISNULL(A.auto_acceptance_price_partial, 0) AS auto_acceptance_price_partial," & _    "ISNULL(A.partial_lower_amt, 0) AS partial_lower_amt," & _    "ISNULL(A.auto_acceptance_quantity, 0) AS auto_acceptance_quantity," & _    "ISNULL(A.partial_lower_qty, 0) AS partial_lower_qty," & _    "ISNULL(A.show_price, 0) AS show_price," & _    "ISNULL(A.no_of_clicks, 0) AS no_of_clicks," & _    "ISNULL(A.start_price, 0) AS start_price," & _    "ISNULL(A.reserve_price, 0) AS reserve_price," & _    "ISNULL(A.increament_amount, 0) AS increament_amount," & _    "ISNULL(A.bid_time_interval, '') AS bid_time_interval," & _    "ISNULL(A.is_show_reserve_price, 0) AS is_show_reserve_price," & _    "ISNULL(A.buy_now_price, 0) AS buy_now_price," & _    "ISNULL(A.is_show_no_of_bids, 0) AS is_show_no_of_bids," & _    "ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing," & _    "ISNULL(A.is_accept_pre_bid, 0) AS is_accept_pre_bid," & _    "ISNULL(A.start_date, '1/1/1900') AS start_date," & _    "ISNULL(A.end_date, '1/1/1900') AS end_date," & _    "ISNULL(A.display_end_time, '1/1/1900') AS display_end_time," & _    "ISNULL(A.is_display_start_date, 0) AS is_display_start_date," & _    "ISNULL(A.is_display_end_date, 0) AS is_display_end_date," & _    "ISNULL(A.request_for_quote_message, '') AS request_for_quote_message," & _    "ISNULL(A.bid_over_before_time, 0) AS bid_over_before_time," & _    "ISNULL(A.bid_over_increase_time, 0) AS bid_over_increase_time," & _    "ISNULL(A.is_show_increment_price, 0) AS is_show_increment_price," & _    "ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value," & _    "ISNULL(A.is_show_rank, 0) AS is_show_rank," & _    "ISNULL(A.time_zone, 0) AS time_zone," & _    "ISNULL(A.total_qty, 0) AS total_qty," & _    "ISNULL(A.qty_per_bidder, 0) AS qty_per_bidder," & _    "ISNULL(A.live_bid_allow, 0) AS live_bid_allow," & _    "ISNULL(A.proxy_bid_allow, 0) AS proxy_bid_allow," & _    "ISNULL(A.shipping_amount, 0) AS shipping_amount" & _    " FROM tbl_auctions A WITH (NOLOCK) WHERE" & _    " A.auction_id = " & auction_id
        Dim no_of_bid As Integer = 0
        Dim dt As DataTable = New DataTable()
        dt = SqlHelper.ExecuteDatatable(qry)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)

                HID_auction_category_id.Value = .Item("auction_category_id")

                HID_auction_type_id.Value = .Item("auction_type_id")
                If Not ddl_auction_type.Items.FindByValue(.Item("auction_type_id")) Is Nothing Then
                    ddl_auction_type.ClearSelection()
                    ddl_auction_type.Items.FindByValue(.Item("auction_type_id")).Selected = True
                    lbl_auction_type.Text = ddl_auction_type.SelectedItem.Text
                    lbl_auction_type_desc.Text = ddl_auction_type.SelectedItem.Text
                Else
                    lbl_auction_type.Text = ""
                End If
                tr_bid_time_increse.Visible = True
                Select Case .Item("auction_type_id")
                    Case 1
                        no_of_bid = SqlHelper.ExecuteScalar("select count(buy_id) from tbl_auction_buy WITH (NOLOCK) where auction_id=" & auction_id)
                        tr_bid_time_increse.Visible = False
                    Case 2, 3
                        no_of_bid = SqlHelper.ExecuteScalar("select count(bid_id) from tbl_auction_bids WITH (NOLOCK) where auction_id=" & auction_id)
                    Case 4
                        no_of_bid = SqlHelper.ExecuteScalar("select count(quotation_id) from tbl_auction_quotations where auction_id=" & auction_id)
                        tr_bid_time_increse.Visible = False
                End Select


                'first panel bind
                lbl_total_qty.Text = .Item("total_qty")
                txt_total_qty.Text = .Item("total_qty")
                lbl_max_allowed.Text = .Item("qty_per_bidder")
                txt_max_allowed.Text = .Item("qty_per_bidder")


                lbl_show_price.Text = "$" & FormatNumber(.Item("show_price"), 2)
                txt_show_price.Text = CommonCode.FormatMoney(.Item("show_price"))

                chk_is_private_offer.Checked = .Item("is_private_offer")
                If .Item("is_private_offer") Then
                    lbl_is_private_offer.Text = "<img src='/Images/true.gif' alt=''>&nbsp&nbsp We will accept private offer for this auction with auto acceptance at"
                Else
                    lbl_is_private_offer.Text = "<img src='/Images/false.gif' alt=''>&nbsp&nbsp We will accept private offer for this auction with auto acceptance at"
                End If

                lbl_auto_acceptance_price_private.Text = "$" & FormatNumber(.Item("auto_acceptance_price_private"))
                lbl_auto_reject_price_private.Text = "$" & FormatNumber(.Item("private_lower_amt"))
                txt_auto_acceptance_price_private.Text = CommonCode.FormatMoney(.Item("auto_acceptance_price_private"))
                txt_auto_reject_price_private.Text = CommonCode.FormatMoney(.Item("private_lower_amt"))

                chk_is_partial_offer.Checked = .Item("is_partial_offer")
                If .Item("is_partial_offer") Then
                    lbl_is_partial_offer.Text = "<img src='/Images/true.gif' alt=''>&nbsp&nbspWe will accept partial offer with auto acceptance of"
                Else
                    lbl_is_partial_offer.Text = "<img src='/Images/false.gif' alt=''>&nbsp&nbspWe will accept partial offer with auto acceptance of"
                End If

                If .Item("is_private_offer") Or .Item("is_partial_offer") Then
                    chk_is_none.Checked = False
                Else
                    chk_is_none.Checked = True
                End If

                lbl_auto_acceptance_price_partial.Text = "$" & FormatNumber(.Item("auto_acceptance_price_partial"), 2)
                lbl_auto_rejection_price_partial.Text = "$" & FormatNumber(.Item("partial_lower_amt"), 2)

                txt_auto_acceptance_price_partial.Text = CommonCode.FormatMoney(.Item("auto_acceptance_price_partial"))
                txt_auto_rejection_price_partial.Text = CommonCode.FormatMoney(.Item("partial_lower_amt"))

                lbl_auto_acceptance_qty.Text = .Item("auto_acceptance_quantity")
                lbl_auto_rejection_qty.Text = .Item("partial_lower_qty")
                txt_auto_acceptance_qty.Text = .Item("auto_acceptance_quantity")
                txt_auto_rejection_qty.Text = .Item("partial_lower_qty")

                'first panel end

                'second panel start
                lbl_start_price_traditional.Text = "$" & FormatNumber(.Item("start_price"), 2)
                txt_start_price_traditional.Text = CommonCode.FormatMoney(.Item("start_price"))
                lbl_reserve_price_traditional.Text = "$" & FormatNumber(.Item("reserve_price"), 2)
                txt_reserve_price_traditional.Text = CommonCode.FormatMoney(.Item("reserve_price"))
                lbl_increament_amount_traditional.Text = "$" & FormatNumber(.Item("increament_amount"), 2)
                txt_increament_amount_traditional.Text = CommonCode.FormatMoney(.Item("increament_amount"))
                'lbl_bid_time_interval_traditional.Text = .Item("bid_time_interval")
                'txt_bid_time_interval_traditional.Text = .Item("bid_time_interval")
                chk_is_show_reserve_price_traditional.Checked = .Item("is_show_reserve_price")
                chk_is_show_increment_amount.Checked = .Item("is_show_increment_price")
                chk_is_show_rank.Checked = .Item("is_show_rank")

                chk_is_show_increment_amount_proxy.Checked = .Item("is_show_increment_price")
                chk_is_show_rank_proxy.Checked = .Item("is_show_rank")

                If .Item("is_show_reserve_price") Then
                    lbl_is_show_reserve_price_traditional.Text = "<img src='/Images/true.gif' alt=''> Reserve price will be shown to the bidders. The system will not allow the lot to be sold below this price. "
                Else
                    lbl_is_show_reserve_price_traditional.Text = "<img src='/Images/false.gif' alt=''> Reserve price will be shown to the bidders. The system will not allow the lot to be sold below this price. "
                End If

                If .Item("is_show_increment_price") Then
                    lbl_is_show_increment_amount.Text = "<img src='/Images/true.gif' alt=''> Increment Amount will be shown to the bidders."
                Else
                    lbl_is_show_increment_amount.Text = "<img src='/Images/false.gif' alt=''> Increment Amount will be shown to the bidders."
                End If
                If .Item("is_show_rank") Then
                    lbl_is_show_rank.Text = "<img src='/Images/true.gif' alt=''> Rank will be shown to the bidders."
                Else
                    lbl_is_show_rank.Text = "<img src='/Images/false.gif' alt=''> Rank will be shown to the bidders."
                End If

                chk_is_show_no_of_bids_traditional.Checked = .Item("is_show_no_of_bids")
                If .Item("is_show_no_of_bids") Then
                    lbl_is_show_no_of_bids_traditional.Text = "<img src='/Images/true.gif' alt=''> Number of bids on the auction will be shown to the bidders. "
                Else
                    lbl_is_show_no_of_bids_traditional.Text = "<img src='/Images/false.gif' alt=''> Number of bids on the auction will be shown to the bidders. "
                End If
                chk_is_show_actual_pricing_traditional.Checked = .Item("is_show_actual_pricing")
                If .Item("is_show_actual_pricing") Then
                    lbl_is_show_actual_pricing_traditional.Text = "<img src='/Images/true.gif' alt=''> Bid amount will be shown to bidders. "
                Else
                    lbl_is_show_actual_pricing_traditional.Text = "<img src='/Images/false.gif' alt=''> Bid amount will be shown to bidders. "
                End If
                'chk_is_accept_pre_bid_traditional.Checked = .Item("is_accept_pre_bid")
                'If .Item("is_accept_pre_bid") Then
                '    lbl_is_accept_pre_bid_traditional.Text = "<img src='/Images/true.gif' alt=''> We will accept any bid before start date. "
                'Else
                '    lbl_is_accept_pre_bid_traditional.Text = "<img src='/Images/false.gif' alt=''> We will accept any bid before start date. "
                'End If
                chk_is_private_offer_traditional.Checked = .Item("is_private_offer")
                If .Item("is_private_offer") Then
                    lbl_is_private_offer_traditional.Text = "<img src='/Images/true.gif' alt=''> We will accept private offer for this auction with auto acceptance at "
                Else
                    lbl_is_private_offer_traditional.Text = "<img src='/Images/false.gif' alt=''> We will accept private offer for this auction with auto acceptance at "
                End If

                chk_is_partial_offer_traditional.Checked = .Item("is_partial_offer")
                If .Item("is_partial_offer") Then
                    lbl_is_partial_offer_traditional.Text = "<img src='/Images/true.gif' alt=''> We will accept partial offer with auto acceptance of "
                Else
                    lbl_is_partial_offer_traditional.Text = "<img src='/Images/false.gif' alt=''> We will accept partial offer with auto acceptance of "
                End If
                chk_is_buy_it_now_traditional.Checked = .Item("is_buy_it_now")
                If .Item("is_buy_it_now") Then
                    lbl_is_buy_it_now_traditional.Text = "<img src='/Images/true.gif' alt=''> Bidders will be able to buy this lot for "
                Else
                    lbl_is_buy_it_now_traditional.Text = "<img src='/Images/false.gif' alt=''> Bidders will be able to buy this lot for "
                End If

                lbl_auto_acceptance_price_private_traditional.Text = "$" & FormatNumber(.Item("auto_acceptance_price_private"), 2) 'CommonCode.FormatMoney(.Item("auto_acceptance_price_private"))
                txt_auto_acceptance_price_private_traditional.Text = CommonCode.FormatMoney(.Item("auto_acceptance_price_private"))
                lbl_auto_acceptance_price_partial_traditional.Text = "$" & FormatNumber(.Item("auto_acceptance_price_partial"), 2) ' CommonCode.FormatMoney(.Item("auto_acceptance_price_partial"))
                txt_auto_acceptance_price_partial_traditional.Text = CommonCode.FormatMoney(.Item("auto_acceptance_price_partial"))
                lbl_auto_acceptance_price_now_traditional.Text = "$" & FormatNumber(.Item("buy_now_price"), 2) & "." 'CommonCode.FormatMoney(.Item("buy_now_price"))
                txt_auto_acceptance_price_now_traditional.Text = CommonCode.FormatMoney(.Item("buy_now_price"))
                lbl_thresh_hold_traditional.Text = "$" & FormatNumber(.Item("thresh_hold_value"))
                txt_thresh_hold_traditional.Text = CommonCode.FormatMoney(.Item("thresh_hold_value"))
                lbl_auto_acceptance_qty_traditional.Text = .Item("auto_acceptance_quantity")
                txt_auto_acceptance_qty_traditional.Text = .Item("auto_acceptance_quantity")

                lbl_total_qty_traditional.Text = .Item("total_qty")
                txt_total_qty_traditional.Text = .Item("total_qty")
                lbl_max_allowed_traditional.Text = .Item("qty_per_bidder")
                txt_max_allowed_traditional.Text = .Item("qty_per_bidder")

                'lbl_no_of_clicks_traditional.Text = .Item("no_of_clicks")
                'txt_no_of_clicks_traditional.Text = .Item("no_of_clicks")
                'second panel end

                'third panel start
                lbl_start_price_proxy.Text = "$" & FormatNumber(.Item("start_price"), 2)  'CommonCode.FormatMoney(.Item("start_price"))
                txt_start_price_proxy.Text = CommonCode.FormatMoney(.Item("start_price"))
                lbl_reserve_price_proxy.Text = "$" & FormatNumber(.Item("reserve_price"), 2) 'CommonCode.FormatMoney(.Item("reserve_price"))
                txt_reserve_price_proxy.Text = CommonCode.FormatMoney(.Item("reserve_price"))
                lbl_increament_amount_proxy.Text = "$" & FormatNumber(.Item("increament_amount"), 2) 'CommonCode.FormatMoney(.Item("increament_amount"))
                txt_increament_amount_proxy.Text = CommonCode.FormatMoney(.Item("increament_amount"))
                lbl_thresh_hold_proxy.Text = "$" & FormatNumber(.Item("thresh_hold_value"))
                txt_thresh_hold_proxy.Text = CommonCode.FormatMoney(.Item("thresh_hold_value"))
                chk_is_show_reserve_price_proxy.Checked = .Item("is_show_reserve_price")
                chk_proxy_bid_allow.Checked = .Item("proxy_bid_allow")
                chk_live_bid_allow.Checked = .Item("live_bid_allow")
                If .Item("proxy_bid_allow") Then
                    lbl_proxy_bid_allow.Text = "<img src='/Images/true.gif' alt=''> Proxy Bid allow "
                Else
                    lbl_proxy_bid_allow.Text = "<img src='/Images/false.gif' alt=''> Proxy Bid allow "
                End If

                If .Item("live_bid_allow") Then
                    lbl_live_bid_allow.Text = "<img src='/Images/true.gif' alt=''> Live Bid allow "
                Else
                    lbl_live_bid_allow.Text = "<img src='/Images/false.gif' alt=''> Live Bid allow "
                End If

                If .Item("is_show_reserve_price") Then
                    lbl_is_show_reserve_price_proxy.Text = "<img src='/Images/true.gif' alt=''> Reserve price will be shown to the bidders. The system will not allow the lot to be sold below this price. "
                Else
                    lbl_is_show_reserve_price_proxy.Text = "<img src='/Images/false.gif' alt=''> Reserve price will be shown to the bidders. The system will not allow the lot to be sold below this price. "
                End If

                If .Item("is_show_increment_price") Then
                    lbl_is_show_increment_amount_proxy.Text = "<img src='/Images/true.gif' alt=''> Increment Amount will be shown to the bidders."
                Else
                    lbl_is_show_increment_amount_proxy.Text = "<img src='/Images/false.gif' alt=''> Increment Amount will be shown to the bidders."
                End If
                If .Item("is_show_rank") Then
                    lbl_is_show_rank_proxy.Text = "<img src='/Images/true.gif' alt=''> Rank will be shown to the bidders."
                Else
                    lbl_is_show_rank_proxy.Text = "<img src='/Images/false.gif' alt=''> Rank will be shown to the bidders."
                End If


                chk_is_show_no_of_bids_proxy.Checked = .Item("is_show_no_of_bids")
                If .Item("is_show_no_of_bids") Then
                    lbl_is_show_no_of_bids_proxy.Text = "<img src='/Images/true.gif' alt=''> Number of bids on the auction will be shown to the bidders. "
                Else
                    lbl_is_show_no_of_bids_proxy.Text = "<img src='/Images/false.gif' alt=''> Number of bids on the auction will be shown to the bidders. "
                End If
                chk_is_show_actual_pricing_proxy.Checked = .Item("is_show_actual_pricing")
                If .Item("is_show_actual_pricing") Then
                    lbl_is_show_actual_pricing_proxy.Text = "<img src='/Images/true.gif' alt=''> Bid amount will be shown to bidders. "
                Else
                    lbl_is_show_actual_pricing_proxy.Text = "<img src='/Images/false.gif' alt=''> Bid amount will be shown to bidders. "
                End If
                'chk_is_accept_pre_bid_proxy.Checked = .Item("is_accept_pre_bid")
                'If .Item("is_accept_pre_bid") Then
                '    lbl_is_accept_pre_bid_proxy.Text = "<img src='/Images/true.gif' alt=''> We will accept any bid before start date. "
                'Else
                '    lbl_is_accept_pre_bid_proxy.Text = "<img src='/Images/false.gif' alt=''> We will accept any bid before start date. "
                'End If
                chk_is_private_offer_proxy.Checked = .Item("is_private_offer")
                If .Item("is_private_offer") Then
                    lbl_is_private_offer_proxy.Text = "<img src='/Images/true.gif' alt=''> We will accept private offer for this auction with auto acceptance at "
                Else
                    lbl_is_private_offer_proxy.Text = "<img src='/Images/false.gif' alt=''> We will accept private offer for this auction with auto acceptance at "
                End If

                If .Item("is_private_offer") Then
                    lbl_is_reject_private_offer_proxy.Text = "We will reject private offer for this auction with auto rejection at "
                Else
                    lbl_is_reject_private_offer_proxy.Text = "We will reject private offer for this auction with auto rejection at "
                End If


                chk_is_partial_offer_proxy.Checked = .Item("is_partial_offer")
                If .Item("is_partial_offer") Then
                    lbl_is_partial_offer_proxy.Text = "<img src='/Images/true.gif' alt=''> We will accept partial offer with auto acceptance of "
                Else
                    lbl_is_partial_offer_proxy.Text = "<img src='/Images/false.gif' alt=''> We will accept partial offer with auto acceptance of "
                End If

                If .Item("is_partial_offer") Then
                    lbl_is_reject_partial_offer_proxy.Text = "We will reject partial offer with auto rejection of "
                Else
                    lbl_is_reject_partial_offer_proxy.Text = "We will reject partial offer with auto rejection of "
                End If


                chk_is_buy_it_now_proxy.Checked = .Item("is_buy_it_now")
                If .Item("is_buy_it_now") Then
                    lbl_is_buy_it_now_proxy.Text = "<img src='/Images/true.gif' alt=''> Bidders will be able to buy this lot for "
                Else
                    lbl_is_buy_it_now_proxy.Text = "<img src='/Images/false.gif' alt=''> Bidders will be able to buy this lot for "
                End If

                lbl_auto_acceptance_price_private_proxy.Text = "$" & FormatNumber(.Item("auto_acceptance_price_private"), 2) 'CommonCode.FormatMoney(.Item("auto_acceptance_price_private"))
                lbl_auto_rejection_price_private_proxy.Text = "$" & FormatNumber(.Item("private_lower_amt"), 2) 'CommonCode.FormatMoney(.Item("auto_acceptance_price_private"))
                txt_auto_acceptance_price_private_proxy.Text = CommonCode.FormatMoney(.Item("auto_acceptance_price_private"))
                txt_auto_rejection_price_private_proxy.Text = CommonCode.FormatMoney(.Item("private_lower_amt"))
                lbl_auto_acceptance_price_partial_proxy.Text = "$" & FormatNumber(.Item("auto_acceptance_price_partial"), 2) 'CommonCode.FormatMoney(.Item("auto_acceptance_price_partial"))
                lbl_auto_rejection_price_partial_proxy.Text = "$" & FormatNumber(.Item("partial_lower_amt"), 2) 'CommonCode.FormatMoney(.Item("auto_acceptance_price_partial"))
                txt_auto_acceptance_price_partial_proxy.Text = CommonCode.FormatMoney(.Item("auto_acceptance_price_partial"))
                txt_auto_rejection_price_partial_proxy.Text = CommonCode.FormatMoney(.Item("partial_lower_amt"))
                lbl_auto_acceptance_price_now_proxy.Text = "$" & FormatNumber(.Item("buy_now_price"), 2) & "." 'CommonCode.FormatMoney(.Item("buy_now_price"))
                txt_auto_acceptance_price_now_proxy.Text = CommonCode.FormatMoney(.Item("buy_now_price"))
                lbl_auto_acceptance_qty_proxy.Text = .Item("auto_acceptance_quantity")
                lbl_auto_rejection_qty_proxy.Text = .Item("partial_lower_qty")
                txt_auto_acceptance_qty_proxy.Text = .Item("auto_acceptance_quantity")
                txt_auto_rejection_qty_proxy.Text = .Item("partial_lower_qty")

                lbl_total_qty_proxy.Text = .Item("total_qty")
                txt_total_qty_proxy.Text = .Item("total_qty")
                lbl_max_allowed_proxy.Text = .Item("qty_per_bidder")
                txt_max_allowed_proxy.Text = .Item("qty_per_bidder")

                'third panel end

                'fourth panel bind
                lbl_request_for_quote.Text = CommonCode.decodeSingleQuote(.Item("request_for_quote_message"))
                txt_request_for_quote.Text = CommonCode.decodeSingleQuote(.Item("request_for_quote_message")).Replace("<br/>", Char.ConvertFromUtf32(13)).Replace("<br/>", Char.ConvertFromUtf32(10))
                'fourth panel end

                lblStartDate.Text = Convert.ToDateTime(.Item("start_date")).ToString("MM/dd/yyyy hh:mm tt")
                If CommonCode.FormatDate(.Item("start_date")) <> "" Then
                    dtStartDate.SelectedDate = .Item("start_date")
                End If
                If .Item("is_display_start_date") Then
                    lbl_display_start_date.Text = "<table><tr><td><img src='/Images/true.gif' alt=''></td><td valign='top'>Show with auction</td></tr></table>"
                Else
                    lbl_display_start_date.Text = "<table><tr><td><img src='/Images/false.gif' alt=''></td><td valign='top'>Show with auction</td></tr></table>"
                End If

                chk_is_display_start_date.Checked = .Item("is_display_start_date")

                lblEndDate.Text = Convert.ToDateTime(.Item("display_end_time")).ToString("MM/dd/yyyy hh:mm tt")
                If CommonCode.FormatDate(.Item("display_end_time")) <> "" Then
                    dtEndDate.SelectedDate = .Item("display_end_time")
                End If


                If .Item("is_display_end_date") Then
                    lbl_display_end_date.Text = "<table><tr><td><img src='/Images/true.gif' alt=''></td><td valign='top'>Show with auction</td></tr></table>"
                Else
                    lbl_display_end_date.Text = "<table><tr><td><img src='/Images/false.gif' alt=''></td><td valign='top'>Show with auction</td></tr></table>"
                End If

                chk_is_display_end_date.Checked = .Item("is_display_end_date")

                lbl_before_time.Text = .Item("bid_over_before_time")
                txt_before_time.Text = IIf(.Item("bid_over_before_time") = "0", "", .Item("bid_over_before_time"))
                lbl_after_time.Text = .Item("bid_over_increase_time")
                txt_after_time.Text = IIf(.Item("bid_over_increase_time") = "0", "", .Item("bid_over_increase_time"))

                'txt_time_zone.Text = .Item("time_zone")
                'lbl_time_zone.Text = .Item("time_zone")

            End With


        End If
        dt = Nothing

        setdetailsmiddlesection()
        If no_of_bid > 0 Then
            ddl_auction_type.Enabled = False
        Else
            ddl_auction_type.Enabled = True
        End If
    End Sub

    Private Sub visibleDetailsData(ByVal readMode As Boolean)
        Panel1_Content4button_per.Visible = True
        Panel1_Content4button_per_schedule_tab.Visible = False
        If readMode Then
            btnEditBiddingDetails.Visible = True
            btnEditBiddingDetailsSchedule.Visible = False
            btnUpdateBiddingDetails.Visible = False
            divCancelBiddingDetails.Visible = False
            tr_auction_type.Visible = False
        Else
            btnEditBiddingDetails.Visible = False
            btnUpdateBiddingDetails.Visible = True
            divCancelBiddingDetails.Visible = True
            btnEditBiddingDetailsSchedule.Visible = False
            tr_auction_type.Visible = True
        End If

        'lbl_auction_category.Visible = readMode
        'ddl_auction_category.Visible = Not readMode
        lbl_auction_type.Visible = readMode
        ddl_auction_type.Visible = Not readMode

        'panel 1 setting
        lbl_total_qty.Visible = readMode
        txt_total_qty.Visible = Not readMode

        lbl_max_allowed.Visible = readMode
        txt_max_allowed.Visible = Not readMode

        lbl_show_price.Visible = readMode
        txt_show_price.Visible = Not readMode
        lbl_is_private_offer.Visible = readMode
        chk_is_private_offer.Visible = Not readMode
        lbl_auto_acceptance_price_private.Visible = readMode
        lbl_auto_reject_price_private.Visible = readMode
        txt_auto_acceptance_price_private.Visible = Not readMode
        txt_auto_reject_price_private.Visible = Not readMode
        lbl_is_partial_offer.Visible = readMode
        chk_is_partial_offer.Visible = Not readMode
        chk_is_none.ValidationGroup = Not readMode
        lbl_auto_acceptance_price_partial.Visible = readMode
        lbl_auto_rejection_price_partial.Visible = readMode
        txt_auto_acceptance_price_partial.Visible = Not readMode
        txt_auto_rejection_price_partial.Visible = Not readMode
        chk_is_none.Visible = Not readMode
        lbl_auto_acceptance_qty.Visible = readMode
        lbl_auto_rejection_qty.Visible = readMode
        txt_auto_acceptance_qty.Visible = Not readMode
        txt_auto_rejection_qty.Visible = Not readMode

        'panel 2 setting
        lbl_start_price_traditional.Visible = readMode
        txt_start_price_traditional.Visible = Not readMode
        lbl_reserve_price_traditional.Visible = readMode
        txt_reserve_price_traditional.Visible = Not readMode
        lbl_increament_amount_traditional.Visible = readMode
        txt_increament_amount_traditional.Visible = Not readMode
        lbl_thresh_hold_traditional.Visible = readMode
        txt_thresh_hold_traditional.Visible = Not readMode
        lbl_is_show_reserve_price_traditional.Visible = readMode
        chk_is_show_reserve_price_traditional.Visible = Not readMode

        lbl_is_show_increment_amount.Visible = readMode
        chk_is_show_increment_amount.Visible = Not readMode

        lbl_is_show_increment_amount_proxy.Visible = readMode
        chk_is_show_increment_amount_proxy.Visible = Not readMode

        lbl_is_show_rank.Visible = readMode
        chk_is_show_rank.Visible = Not readMode

        lbl_is_show_rank_proxy.Visible = readMode
        chk_is_show_rank_proxy.Visible = Not readMode


        lbl_is_show_no_of_bids_traditional.Visible = readMode
        chk_is_show_no_of_bids_traditional.Visible = Not readMode
        lbl_is_show_actual_pricing_traditional.Visible = readMode
        chk_is_show_actual_pricing_traditional.Visible = Not readMode
        'lbl_is_accept_pre_bid_traditional.Visible = readMode
        'chk_is_accept_pre_bid_traditional.Visible = Not readMode
        lbl_is_private_offer_traditional.Visible = readMode
        chk_is_private_offer_traditional.Visible = Not readMode
        lbl_is_partial_offer_traditional.Visible = readMode
        chk_is_partial_offer_traditional.Visible = Not readMode
        chk_is_none_traditional.Visible = Not readMode
        lbl_is_buy_it_now_traditional.Visible = readMode
        chk_is_buy_it_now_traditional.Visible = Not readMode
        lbl_total_qty_traditional.Visible = readMode
        txt_total_qty_traditional.Visible = Not readMode
        lbl_max_allowed_traditional.Visible = readMode
        txt_max_allowed_traditional.Visible = Not readMode
        lbl_auto_acceptance_price_private_traditional.Visible = readMode
        txt_auto_acceptance_price_private_traditional.Visible = Not readMode
        lbl_auto_acceptance_price_partial_traditional.Visible = readMode
        txt_auto_acceptance_price_partial_traditional.Visible = Not readMode
        lbl_auto_acceptance_price_now_traditional.Visible = readMode
        txt_auto_acceptance_price_now_traditional.Visible = Not readMode
        lbl_auto_acceptance_qty_traditional.Visible = readMode
        txt_auto_acceptance_qty_traditional.Visible = Not readMode

        'panel 3 setting
        lbl_start_price_proxy.Visible = readMode
        txt_start_price_proxy.Visible = Not readMode
        lbl_reserve_price_proxy.Visible = readMode
        txt_reserve_price_proxy.Visible = Not readMode
        lbl_increament_amount_proxy.Visible = readMode
        txt_increament_amount_proxy.Visible = Not readMode
        lbl_thresh_hold_proxy.Visible = readMode
        txt_thresh_hold_proxy.Visible = Not readMode
        lbl_is_show_reserve_price_proxy.Visible = readMode
        chk_is_show_reserve_price_proxy.Visible = Not readMode
        lbl_proxy_bid_allow.Visible = readMode
        chk_proxy_bid_allow.Visible = Not readMode
        lbl_live_bid_allow.Visible = readMode
        chk_live_bid_allow.Visible = Not readMode
        lbl_is_show_no_of_bids_proxy.Visible = readMode
        chk_is_show_no_of_bids_proxy.Visible = Not readMode
        lbl_is_show_actual_pricing_proxy.Visible = readMode
        chk_is_show_actual_pricing_proxy.Visible = Not readMode
        'lbl_is_accept_pre_bid_proxy.Visible = readMode
        'chk_is_accept_pre_bid_proxy.Visible = Not readMode
        lbl_is_private_offer_proxy.Visible = readMode
        lbl_is_reject_private_offer_proxy.Visible = readMode
        chk_is_private_offer_proxy.Visible = Not readMode
        chk_is_reject_private_offer_proxy.Visible = Not readMode
        lbl_is_partial_offer_proxy.Visible = readMode
        lbl_is_reject_partial_offer_proxy.Visible = readMode
        chk_is_partial_offer_proxy.Visible = Not readMode
        chk_is_reject_partial_offer_proxy.Visible = Not readMode
        chk_is_none_proxy.Visible = Not readMode
        lbl_is_buy_it_now_proxy.Visible = readMode
        chk_is_buy_it_now_proxy.Visible = Not readMode
        lbl_auto_acceptance_price_private_proxy.Visible = readMode
        lbl_auto_rejection_price_private_proxy.Visible = readMode
        txt_auto_acceptance_price_private_proxy.Visible = Not readMode
        txt_auto_rejection_price_private_proxy.Visible = Not readMode
        lbl_auto_acceptance_price_partial_proxy.Visible = readMode
        lbl_auto_rejection_price_partial_proxy.Visible = readMode
        txt_auto_acceptance_price_partial_proxy.Visible = Not readMode
        txt_auto_rejection_price_partial_proxy.Visible = Not readMode
        lbl_auto_acceptance_price_now_proxy.Visible = readMode
        txt_auto_acceptance_price_now_proxy.Visible = Not readMode
        lbl_auto_acceptance_qty_proxy.Visible = readMode
        lbl_auto_rejection_qty_proxy.Visible = readMode
        txt_auto_acceptance_qty_proxy.Visible = Not readMode
        txt_auto_rejection_qty_proxy.Visible = Not readMode
        lbl_total_qty_proxy.Visible = readMode
        txt_total_qty_proxy.Visible = Not readMode
        lbl_max_allowed_proxy.Visible = readMode
        txt_max_allowed_proxy.Visible = Not readMode

        'panel 4 setting
        lbl_request_for_quote.Visible = readMode
        txt_request_for_quote.Visible = Not readMode
         'common setting
        'dtStartDate.Visible = Not readMode
        'lblStartDate.Visible = readMode
        'lbl_display_start_date.Visible = readMode
        'chk_is_display_start_date.Visible = Not readMode

        'lblEndDate.Visible = readMode
        'dtEndDate.Visible = Not readMode
        'lbl_display_end_date.Visible = readMode
        'chk_is_display_end_date.Visible = Not readMode

        'lblDisplayEndDate.Visible = readMode
        'dtDisplayEndDate.Visible = Not readMode

        'lbl_before_time.Visible = readMode
        'txt_before_time.Visible = Not readMode
        'lbl_after_time.Visible = readMode
        'txt_after_time.Visible = Not readMode

        'lbl_time_zone.Visible = readMode
        'txt_time_zone.Visible = Not readMode

    End Sub

    Private Sub visibleDetailsDataSchedule(ByVal readMode As Boolean)
        Panel1_Content4button_per.Visible = False
        Panel1_Content4button_per_schedule_tab.Visible = True
        If readMode Then
            btnEditBiddingDetailsSchedule.Visible = True
            btnUpdateBiddingDetailsSchedule.Visible = False
            divCancelBiddingDetailsSchedule.Visible = False
            tr_auction_type.Visible = False
        Else
            btnEditBiddingDetailsSchedule.Visible = False
            btnUpdateBiddingDetailsSchedule.Visible = True
            divCancelBiddingDetailsSchedule.Visible = True
            tr_auction_type.Visible = True
        End If

        'common setting
        dtStartDate.Visible = Not readMode
        lblStartDate.Visible = readMode
        lbl_display_start_date.Visible = readMode
        chk_is_display_start_date.Visible = Not readMode

        lblEndDate.Visible = readMode
        dtEndDate.Visible = Not readMode

        lbl_display_end_date.Visible = readMode
        chk_is_display_end_date.Visible = Not readMode

        lbl_before_time.Visible = readMode
        txt_before_time.Visible = Not readMode
        lbl_after_time.Visible = readMode
        txt_after_time.Visible = Not readMode

        'lbl_time_zone.Visible = readMode
        'txt_time_zone.Visible = Not readMode

    End Sub

    Protected Sub btnCancelBiddingDetails_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelBiddingDetails.Click
        If Not ddl_auction_type.Items.FindByValue(HID_auction_type_id.Value) Is Nothing Then
            ddl_auction_type.ClearSelection()
            ddl_auction_type.Items.FindByValue(HID_auction_type_id.Value).Selected = True
        Else
            ddl_auction_type.ClearSelection()
            ddl_auction_type.SelectedIndex = 0
        End If
        setdetailsmiddlesection()
        visibleDetailsData(True)
    End Sub

    Protected Sub btnEditBiddingDetails_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditBiddingDetails.Click
        visibleDetailsData(False)
    End Sub

    Protected Sub btnUpdateBiddingDetails_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpdateBiddingDetails.Click
        If Page.IsValid Then

            Dim is_update_required As Boolean = False

            Dim updpara As String = ""

            If HID_auction_category_id.Value <> SqlHelper.of_FetchKey("auction_category_id") Then
                updpara = updpara & "auction_category_id='" & SqlHelper.of_FetchKey("auction_category_id") & "'"
                is_update_required = True
            End If

            If HID_auction_type_id.Value <> ddl_auction_type.SelectedValue Then
                If is_update_required Then
                    updpara = updpara & ",auction_type_id='" & ddl_auction_type.SelectedValue & "'"
                Else
                    updpara = updpara & "auction_type_id='" & ddl_auction_type.SelectedValue & "'"
                End If
                is_update_required = True
            End If

            If pnl_details1.Visible Then
                If lbl_total_qty.Text.Trim() <> CommonCode.GetValidNumeric(txt_total_qty.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",total_qty='" & CommonCode.GetValidNumeric(txt_total_qty.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "total_qty='" & CommonCode.GetValidNumeric(txt_total_qty.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If

                If lbl_max_allowed.Text.Trim() <> CommonCode.GetValidNumeric(txt_max_allowed.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",qty_per_bidder='" & CommonCode.GetValidNumeric(txt_max_allowed.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "qty_per_bidder='" & CommonCode.GetValidNumeric(txt_max_allowed.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If

                If lbl_show_price.Text.Trim() <> CommonCode.GetValidNumeric(txt_show_price.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",show_price='" & CommonCode.GetValidNumeric(txt_show_price.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "show_price='" & CommonCode.GetValidNumeric(txt_show_price.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If
                If is_update_required Then
                    updpara = updpara & ",is_private_offer='" & chk_is_private_offer.Checked & "'"
                Else
                    updpara = updpara & "is_private_offer='" & chk_is_private_offer.Checked & "'"
                End If
                is_update_required = True

                updpara = updpara & ",is_partial_offer='" & chk_is_partial_offer.Checked & "'"
                If lbl_auto_acceptance_price_private.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_private.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_price_private='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_private.Text.Trim()) & "'"
                End If

                If lbl_auto_reject_price_private.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_reject_price_private.Text.Trim()) Then
                    updpara = updpara & ",private_lower_amt='" & CommonCode.GetValidNumeric(txt_auto_reject_price_private.Text.Trim()) & "'"
                End If

                If lbl_auto_acceptance_price_partial.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_partial.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_price_partial='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_partial.Text.Trim()) & "'"
                End If

                If lbl_auto_rejection_price_partial.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_rejection_price_partial.Text.Trim()) Then
                    updpara = updpara & ",partial_lower_amt='" & CommonCode.GetValidNumeric(txt_auto_rejection_price_partial.Text.Trim()) & "'"
                End If

                If lbl_auto_acceptance_qty.Text.Trim() <> txt_auto_acceptance_qty.Text.Trim() Then
                    updpara = updpara & ",auto_acceptance_quantity='" & txt_auto_acceptance_qty.Text.Trim() & "'"
                End If

                If lbl_auto_rejection_qty.Text.Trim() <> txt_auto_rejection_qty.Text.Trim() Then
                    updpara = updpara & ",partial_lower_qty='" & txt_auto_rejection_qty.Text.Trim() & "'"
                End If


            End If

            If pnl_details2.Visible Then
                If lbl_start_price_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_start_price_traditional.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",start_price='" & CommonCode.GetValidNumeric(txt_start_price_traditional.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "start_price='" & CommonCode.GetValidNumeric(txt_start_price_traditional.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If

                If lbl_reserve_price_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_reserve_price_traditional.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",reserve_price='" & CommonCode.GetValidNumeric(txt_reserve_price_traditional.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "reserve_price='" & CommonCode.GetValidNumeric(txt_reserve_price_traditional.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If
                If lbl_increament_amount_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_increament_amount_traditional.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",increament_amount='" & CommonCode.GetValidNumeric(txt_increament_amount_traditional.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "increament_amount='" & CommonCode.GetValidNumeric(txt_increament_amount_traditional.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If
                If lbl_thresh_hold_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_thresh_hold_traditional.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",thresh_hold_value='" & CommonCode.GetValidNumeric(txt_thresh_hold_traditional.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "thresh_hold_value='" & CommonCode.GetValidNumeric(txt_thresh_hold_traditional.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If

                If is_update_required Then
                    updpara = updpara & ",is_show_reserve_price='" & chk_is_show_reserve_price_traditional.Checked & "'"
                Else
                    updpara = updpara & "is_show_reserve_price='" & chk_is_show_reserve_price_traditional.Checked & "'"
                End If

                is_update_required = True
                updpara = updpara & ",is_show_increment_price='" & chk_is_show_increment_amount.Checked & "'"
                updpara = updpara & ",is_show_rank='" & chk_is_show_rank.Checked & "'"

                updpara = updpara & ",is_show_no_of_bids='" & chk_is_show_no_of_bids_traditional.Checked & "'"
                updpara = updpara & ",is_show_actual_pricing='" & chk_is_show_actual_pricing_traditional.Checked & "'"
                'updpara = updpara & ",is_accept_pre_bid='" & chk_is_accept_pre_bid_traditional.Checked & "'"

                updpara = updpara & ",is_private_offer='" & chk_is_private_offer_traditional.Checked & "'"
                updpara = updpara & ",is_partial_offer='" & chk_is_partial_offer_traditional.Checked & "'"
                updpara = updpara & ",is_buy_it_now='" & chk_is_buy_it_now_traditional.Checked & "'"

                If lbl_auto_acceptance_price_private_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_private_traditional.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_price_private='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_private_traditional.Text.Trim()) & "'"
                End If
                If lbl_auto_acceptance_price_partial_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_partial_traditional.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_price_partial='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_partial_traditional.Text.Trim()) & "'"
                End If
                If lbl_auto_acceptance_price_now_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_now_traditional.Text.Trim()) Then
                    updpara = updpara & ",buy_now_price='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_now_traditional.Text.Trim()) & "'"
                End If

                If lbl_auto_acceptance_qty_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_qty_traditional.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_quantity='" & CommonCode.GetValidNumeric(txt_auto_acceptance_qty_traditional.Text.Trim()) & "'"
                End If
                If lbl_total_qty_traditional.Text.Trim() <> txt_total_qty_traditional.Text.Trim() Then
                    updpara = updpara & ",total_qty='" & txt_total_qty_traditional.Text.Trim() & "'"
                End If
                If lbl_max_allowed_traditional.Text.Trim() <> txt_max_allowed_traditional.Text.Trim() Then
                    updpara = updpara & ",qty_per_bidder='" & txt_max_allowed_traditional.Text.Trim() & "'"
                End If

            End If
            If pnl_details3.Visible Then
                If lbl_start_price_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_start_price_proxy.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",start_price='" & CommonCode.GetValidNumeric(txt_start_price_proxy.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "start_price='" & CommonCode.GetValidNumeric(txt_start_price_proxy.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If

                If lbl_reserve_price_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_reserve_price_proxy.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",reserve_price='" & CommonCode.GetValidNumeric(txt_reserve_price_proxy.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "reserve_price='" & CommonCode.GetValidNumeric(txt_reserve_price_proxy.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If
                If lbl_increament_amount_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_increament_amount_proxy.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",increament_amount='" & CommonCode.GetValidNumeric(txt_increament_amount_proxy.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "increament_amount='" & CommonCode.GetValidNumeric(txt_increament_amount_proxy.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If
                If lbl_thresh_hold_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_thresh_hold_proxy.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",thresh_hold_value='" & CommonCode.GetValidNumeric(txt_thresh_hold_proxy.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "thresh_hold_value='" & CommonCode.GetValidNumeric(txt_thresh_hold_proxy.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If
                If is_update_required Then
                    updpara = updpara & ",is_show_reserve_price='" & chk_is_show_reserve_price_proxy.Checked & "'"
                Else
                    updpara = updpara & "is_show_reserve_price='" & chk_is_show_reserve_price_proxy.Checked & "'"
                End If
                is_update_required = True


                updpara = updpara & ",is_show_increment_price='" & chk_is_show_increment_amount_proxy.Checked & "'"
                updpara = updpara & ",is_show_rank='" & chk_is_show_rank_proxy.Checked & "'"

                updpara = updpara & ",is_show_no_of_bids='" & chk_is_show_no_of_bids_proxy.Checked & "'"
                updpara = updpara & ",is_show_actual_pricing='" & chk_is_show_actual_pricing_proxy.Checked & "'"
                updpara = updpara & ",proxy_bid_allow='" & chk_proxy_bid_allow.Checked & "'"
                updpara = updpara & ",live_bid_allow='" & chk_live_bid_allow.Checked & "'"
                updpara = updpara & ",is_private_offer='" & chk_is_private_offer_proxy.Checked & "'"
                updpara = updpara & ",is_partial_offer='" & chk_is_partial_offer_proxy.Checked & "'"
                updpara = updpara & ",is_buy_it_now='" & chk_is_buy_it_now_proxy.Checked & "'"

                If lbl_auto_acceptance_price_private_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_private_proxy.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_price_private='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_private_proxy.Text.Trim()) & "'"
                End If

                If lbl_auto_rejection_price_private_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_rejection_price_private_proxy.Text.Trim()) Then
                    updpara = updpara & ",private_lower_amt='" & CommonCode.GetValidNumeric(txt_auto_rejection_price_private_proxy.Text.Trim()) & "'"
                End If

                If lbl_auto_acceptance_price_partial_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_partial_proxy.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_price_partial='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_partial_proxy.Text.Trim()) & "'"
                End If

                If lbl_auto_rejection_price_partial_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_rejection_price_partial_proxy.Text.Trim()) Then
                    updpara = updpara & ",partial_lower_amt='" & CommonCode.GetValidNumeric(txt_auto_rejection_price_partial_proxy.Text.Trim()) & "'"
                End If

                If lbl_auto_acceptance_price_now_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_now_proxy.Text.Trim()) Then
                    updpara = updpara & ",buy_now_price='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_now_proxy.Text.Trim()) & "'"
                End If


                If lbl_auto_acceptance_qty_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_qty_proxy.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_quantity='" & CommonCode.GetValidNumeric(txt_auto_acceptance_qty_proxy.Text.Trim()) & "'"
                End If

                If lbl_auto_rejection_qty_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_rejection_qty_proxy.Text.Trim()) Then
                    updpara = updpara & ",partial_lower_qty='" & CommonCode.GetValidNumeric(txt_auto_rejection_qty_proxy.Text.Trim()) & "'"
                End If

                If lbl_total_qty_proxy.Text.Trim() <> txt_total_qty_proxy.Text.Trim() Then
                    updpara = updpara & ",total_qty='" & txt_total_qty_proxy.Text.Trim() & "'"
                End If
                If lbl_max_allowed_proxy.Text.Trim() <> txt_max_allowed_proxy.Text.Trim() Then
                    updpara = updpara & ",qty_per_bidder='" & txt_max_allowed_proxy.Text.Trim() & "'"
                End If
            End If
            If pnl_details4.Visible Then

                If lbl_request_for_quote.Text.Trim() <> CommonCode.GetValidNumeric(txt_request_for_quote.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",request_for_quote_message='" & CommonCode.GetValidNumeric(txt_request_for_quote.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "request_for_quote_message='" & CommonCode.GetValidNumeric(txt_request_for_quote.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If
               
            End If

            If lblStartDate.Text.Trim() <> dtStartDate.SelectedDate.Value Then

                If is_update_required Then
                    updpara = updpara & ",start_date='" & dtStartDate.SelectedDate.Value & "'"
                Else
                    updpara = updpara & "start_date='" & dtStartDate.SelectedDate.Value & "'"
                End If
                is_update_required = True

            End If
            If lblEndDate.Text.Trim() <> dtEndDate.SelectedDate.Value Then

                If is_update_required Then
                    updpara = updpara & ",end_date='" & dtEndDate.SelectedDate.Value & "',display_end_time='" & dtEndDate.SelectedDate.Value & "'"
                Else
                    updpara = updpara & "end_date='" & dtEndDate.SelectedDate.Value & "',display_end_time='" & dtEndDate.SelectedDate.Value & "'"
                End If
                is_update_required = True

            End If

            If is_update_required Then
                updpara = updpara & ",is_display_start_date='" & chk_is_display_start_date.Checked & "'"
            Else
                updpara = updpara & "is_display_start_date='" & chk_is_display_start_date.Checked & "'"
            End If
            is_update_required = True

            updpara = updpara & ",is_display_end_date='" & chk_is_display_end_date.Checked & "'"

            If lbl_before_time.Text <> txt_before_time.Text Then
                If is_update_required Then
                    updpara = updpara & ",bid_over_before_time='" & txt_before_time.Text & "'"
                Else
                    updpara = updpara & "bid_over_before_time='" & txt_before_time.Text & "'"
                End If
                is_update_required = True
            End If
            If lbl_after_time.Text <> txt_after_time.Text Then
                If is_update_required Then
                    updpara = updpara & ",bid_over_increase_time='" & txt_after_time.Text & "'"
                Else
                    updpara = updpara & "bid_over_increase_time='" & txt_after_time.Text & "'"
                End If
                is_update_required = True
            End If
            
            If is_update_required Then
                updpara = updpara & ",update_date=getdate(),update_by_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
                SqlHelper.ExecuteNonQuery("update tbl_auctions set " & updpara & " where auction_id=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
                CommonCode.insert_system_log("Auction bidding details updated", "btnUpdateBiddingDetails_Click", Request.QueryString.Get("i"), "", "Auction")
            End If

            setDetailsEdit(True)



        End If


    End Sub
    Protected Sub btnEditBiddingDetailsSchedule_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditBiddingDetailsSchedule.Click
        visibleDetailsDataSchedule(False)
    End Sub
    Protected Sub btnCancelBiddingDetailsSchedule_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelBiddingDetailsSchedule.Click
        If Not ddl_auction_type.Items.FindByValue(HID_auction_type_id.Value) Is Nothing Then
            ddl_auction_type.ClearSelection()
            ddl_auction_type.Items.FindByValue(HID_auction_type_id.Value).Selected = True
        Else
            ddl_auction_type.ClearSelection()
            ddl_auction_type.SelectedIndex = 0
        End If
        setdetailsmiddlesection()
        'visibleDetailsData(True)
        visibleDetailsDataSchedule(True)
    End Sub
    Protected Sub RadTabStrip3_TabClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadTabStripEventArgs)

        If e.Tab.Text.Trim().ToLower = "auction details" Then
            'Panel1_Content4button_per.Visible = True
            'Panel1_Content4button_per_schedule_tab.Visible = False
            setDetailsEdit(True)
        Else
            'Panel1_Content4button_per.Visible = False
            'Panel1_Content4button_per_schedule_tab.Visible = True
            setDetailsEditSchedule(True)
        End If

    End Sub

    Protected Sub btnUpdateBiddingDetailsSchedule_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpdateBiddingDetailsSchedule.Click
        If Page.IsValid Then
            Dim is_time_change As Boolean = False
            Dim is_update_required As Boolean = False
            Dim updpara As String = ""

            'If HID_auction_category_id.Value <> ddl_auction_category.SelectedValue Then
            '    updpara = updpara & "auction_category_id='" & ddl_auction_category.SelectedValue & "'"
            '    is_update_required = True
            'End If

            If HID_auction_type_id.Value <> ddl_auction_type.SelectedValue Then
                If is_update_required Then
                    updpara = updpara & ",auction_type_id='" & ddl_auction_type.SelectedValue & "'"
                Else
                    updpara = updpara & "auction_type_id='" & ddl_auction_type.SelectedValue & "'"
                End If
                is_update_required = True
            End If

            If pnl_details1.Visible Then
                If lbl_total_qty.Text.Trim() <> CommonCode.GetValidNumeric(txt_total_qty.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",total_qty='" & CommonCode.GetValidNumeric(txt_total_qty.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "total_qty='" & CommonCode.GetValidNumeric(txt_total_qty.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If

                If lbl_max_allowed.Text.Trim() <> CommonCode.GetValidNumeric(txt_max_allowed.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",qty_per_bidder='" & CommonCode.GetValidNumeric(txt_max_allowed.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "qty_per_bidder='" & CommonCode.GetValidNumeric(txt_max_allowed.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If
                If lbl_show_price.Text.Trim() <> CommonCode.GetValidNumeric(txt_show_price.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",show_price='" & CommonCode.GetValidNumeric(txt_show_price.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "show_price='" & CommonCode.GetValidNumeric(txt_show_price.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If
                If is_update_required Then
                    updpara = updpara & ",is_private_offer='" & chk_is_private_offer.Checked & "'"
                Else
                    updpara = updpara & "is_private_offer='" & chk_is_private_offer.Checked & "'"
                End If
                is_update_required = True

                updpara = updpara & ",is_partial_offer='" & chk_is_partial_offer.Checked & "'"
                If lbl_auto_acceptance_price_private.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_private.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_price_private='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_private.Text.Trim()) & "'"
                End If

                If lbl_auto_reject_price_private.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_reject_price_private.Text.Trim()) Then
                    updpara = updpara & ",private_lower_amt='" & CommonCode.GetValidNumeric(txt_auto_reject_price_private.Text.Trim()) & "'"
                End If

                If lbl_auto_acceptance_price_partial.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_partial.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_price_partial='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_partial.Text.Trim()) & "'"
                End If

                If lbl_auto_rejection_price_partial.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_rejection_price_partial.Text.Trim()) Then
                    updpara = updpara & ",partial_lower_amt='" & CommonCode.GetValidNumeric(txt_auto_rejection_price_partial.Text.Trim()) & "'"
                End If


                If lbl_auto_acceptance_qty.Text.Trim() <> txt_auto_acceptance_qty.Text.Trim() Then
                    updpara = updpara & ",auto_acceptance_quantity='" & txt_auto_acceptance_qty.Text.Trim() & "'"
                End If
                If lbl_auto_rejection_qty.Text.Trim() <> txt_auto_rejection_qty.Text.Trim() Then
                    updpara = updpara & ",partial_lower_qty='" & txt_auto_rejection_qty.Text.Trim() & "'"
                End If

            End If

            If pnl_details2.Visible Then
                If lbl_start_price_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_start_price_traditional.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",start_price='" & CommonCode.GetValidNumeric(txt_start_price_traditional.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "start_price='" & CommonCode.GetValidNumeric(txt_start_price_traditional.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If

                If lbl_reserve_price_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_reserve_price_traditional.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",reserve_price='" & CommonCode.GetValidNumeric(txt_reserve_price_traditional.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "reserve_price='" & CommonCode.GetValidNumeric(txt_reserve_price_traditional.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If
                If lbl_increament_amount_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_increament_amount_traditional.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",increament_amount='" & CommonCode.GetValidNumeric(txt_increament_amount_traditional.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "increament_amount='" & CommonCode.GetValidNumeric(txt_increament_amount_traditional.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If
                'If lbl_bid_time_interval_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_bid_time_interval_traditional.Text.Trim()) Then
                '    If is_update_required Then
                '        updpara = updpara & ",bid_time_interval='" & CommonCode.GetValidNumeric(txt_bid_time_interval_traditional.Text.Trim()) & "'"
                '    Else
                '        updpara = updpara & "bid_time_interval='" & CommonCode.GetValidNumeric(txt_bid_time_interval_traditional.Text.Trim()) & "'"
                '    End If
                '    is_update_required = True
                'End If

                If is_update_required Then
                    updpara = updpara & ",is_show_reserve_price='" & chk_is_show_reserve_price_traditional.Checked & "'"
                Else
                    updpara = updpara & "is_show_reserve_price='" & chk_is_show_reserve_price_traditional.Checked & "'"
                End If
                is_update_required = True
                updpara = updpara & ",is_show_increment_price='" & chk_is_show_increment_amount.Checked & "'"
                updpara = updpara & ",is_show_rank='" & chk_is_show_rank.Checked & "'"
                updpara = updpara & ",is_show_no_of_bids='" & chk_is_show_no_of_bids_traditional.Checked & "'"
                updpara = updpara & ",is_show_actual_pricing='" & chk_is_show_actual_pricing_traditional.Checked & "'"
                'updpara = updpara & ",is_accept_pre_bid='" & chk_is_accept_pre_bid_traditional.Checked & "'"

                updpara = updpara & ",is_private_offer='" & chk_is_private_offer_traditional.Checked & "'"
                updpara = updpara & ",is_partial_offer='" & chk_is_partial_offer_traditional.Checked & "'"
                updpara = updpara & ",is_buy_it_now='" & chk_is_buy_it_now_traditional.Checked & "'"

                If lbl_auto_acceptance_price_private_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_private_traditional.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_price_private='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_private_traditional.Text.Trim()) & "'"
                End If
                If lbl_auto_acceptance_price_partial_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_partial_traditional.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_price_partial='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_partial_traditional.Text.Trim()) & "'"
                End If
                If lbl_auto_acceptance_price_now_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_now_traditional.Text.Trim()) Then
                    updpara = updpara & ",buy_now_price='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_now_traditional.Text.Trim()) & "'"
                End If
                If lbl_total_qty_traditional.Text.Trim() <> txt_total_qty_traditional.Text.Trim() Then
                    updpara = updpara & ",total_qty='" & txt_total_qty_traditional.Text.Trim() & "'"
                End If
                If lbl_max_allowed_traditional.Text.Trim() <> txt_max_allowed_traditional.Text.Trim() Then
                    updpara = updpara & ",qty_per_bidder='" & txt_max_allowed_traditional.Text.Trim() & "'"
                End If
                If lbl_auto_acceptance_qty_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_qty_traditional.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_quantity='" & CommonCode.GetValidNumeric(txt_auto_acceptance_qty_traditional.Text.Trim()) & "'"
                End If
                'If lbl_no_of_clicks_traditional.Text.Trim() <> CommonCode.GetValidNumeric(txt_no_of_clicks_traditional.Text.Trim()) Then
                '    updpara = updpara & ",no_of_clicks='" & CommonCode.GetValidNumeric(txt_no_of_clicks_traditional.Text.Trim()) & "'"
                'End If

            End If
            If pnl_details3.Visible Then
                If lbl_start_price_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_start_price_proxy.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",start_price='" & CommonCode.GetValidNumeric(txt_start_price_proxy.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "start_price='" & CommonCode.GetValidNumeric(txt_start_price_proxy.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If

                If lbl_reserve_price_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_reserve_price_proxy.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",reserve_price='" & CommonCode.GetValidNumeric(lbl_reserve_price_proxy.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "reserve_price='" & CommonCode.GetValidNumeric(lbl_reserve_price_proxy.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If
                If lbl_increament_amount_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_increament_amount_proxy.Text.Trim()) Then
                    If is_update_required Then
                        updpara = updpara & ",increament_amount='" & CommonCode.GetValidNumeric(txt_increament_amount_proxy.Text.Trim()) & "'"
                    Else
                        updpara = updpara & "increament_amount='" & CommonCode.GetValidNumeric(txt_increament_amount_proxy.Text.Trim()) & "'"
                    End If
                    is_update_required = True
                End If
                'If lbl_bid_time_interval_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_bid_time_interval_proxy.Text.Trim()) Then
                '    If is_update_required Then
                '        updpara = updpara & ",bid_time_interval='" & CommonCode.GetValidNumeric(txt_bid_time_interval_proxy.Text.Trim()) & "'"
                '    Else
                '        updpara = updpara & "bid_time_interval='" & CommonCode.GetValidNumeric(txt_bid_time_interval_proxy.Text.Trim()) & "'"
                '    End If
                '    is_update_required = True
                'End If

                If is_update_required Then
                    updpara = updpara & ",is_show_reserve_price='" & chk_is_show_reserve_price_proxy.Checked & "'"
                Else
                    updpara = updpara & "is_show_reserve_price='" & chk_is_show_reserve_price_proxy.Checked & "'"
                End If
                is_update_required = True

                updpara = updpara & ",is_show_no_of_bids='" & chk_is_show_no_of_bids_proxy.Checked & "'"
                updpara = updpara & ",is_show_actual_pricing='" & chk_is_show_actual_pricing_proxy.Checked & "'"
                'updpara = updpara & ",is_accept_pre_bid='" & chk_is_accept_pre_bid_proxy.Checked & "'"

                updpara = updpara & ",is_private_offer='" & chk_is_private_offer_proxy.Checked & "'"
                updpara = updpara & ",is_partial_offer='" & chk_is_partial_offer_proxy.Checked & "'"
                updpara = updpara & ",is_buy_it_now='" & chk_is_buy_it_now_proxy.Checked & "'"

                If lbl_auto_acceptance_price_private_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_private_proxy.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_price_private='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_private_proxy.Text.Trim()) & "'"
                End If

                If lbl_auto_rejection_price_private_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_rejection_price_private_proxy.Text.Trim()) Then
                    updpara = updpara & ",private_lower_amt='" & CommonCode.GetValidNumeric(txt_auto_rejection_price_private_proxy.Text.Trim()) & "'"
                End If

                If lbl_auto_acceptance_price_partial_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_partial_proxy.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_price_partial='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_partial_proxy.Text.Trim()) & "'"
                End If

                If lbl_auto_rejection_price_partial_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_rejection_price_partial_proxy.Text.Trim()) Then
                    updpara = updpara & ",partial_lower_amt='" & CommonCode.GetValidNumeric(txt_auto_rejection_price_partial_proxy.Text.Trim()) & "'"
                End If


                If lbl_auto_acceptance_price_now_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_price_now_proxy.Text.Trim()) Then
                    updpara = updpara & ",buy_now_price='" & CommonCode.GetValidNumeric(txt_auto_acceptance_price_now_proxy.Text.Trim()) & "'"
                End If

                If lbl_total_qty_proxy.Text.Trim() <> txt_total_qty_proxy.Text.Trim() Then
                    updpara = updpara & ",total_qty='" & txt_total_qty_proxy.Text.Trim() & "'"
                End If
                If lbl_max_allowed_proxy.Text.Trim() <> txt_max_allowed_proxy.Text.Trim() Then
                    updpara = updpara & ",qty_per_bidder='" & txt_max_allowed_proxy.Text.Trim() & "'"
                End If

                If lbl_auto_acceptance_qty_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_acceptance_qty_proxy.Text.Trim()) Then
                    updpara = updpara & ",auto_acceptance_quantity='" & CommonCode.GetValidNumeric(txt_auto_acceptance_qty_proxy.Text.Trim()) & "'"
                End If
                If lbl_auto_rejection_qty_proxy.Text.Trim() <> CommonCode.GetValidNumeric(txt_auto_rejection_qty_proxy.Text.Trim()) Then
                    updpara = updpara & ",partial_lower_qty='" & CommonCode.GetValidNumeric(txt_auto_rejection_qty_proxy.Text.Trim()) & "'"
                End If

            End If
            If pnl_details4.Visible Then
                If lbl_request_for_quote.Text.Trim() <> txt_request_for_quote.Text.Trim() Then
                    If is_update_required Then
                        updpara = updpara & ",request_for_quote_message='" & CommonCode.encodeSingleQuote(txt_request_for_quote.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "'"
                    Else
                        updpara = updpara & "request_for_quote_message='" & CommonCode.encodeSingleQuote(txt_request_for_quote.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "'"
                    End If
                    is_update_required = True
                End If
            End If

            If lblStartDate.Text.Trim() <> dtStartDate.SelectedDate.Value Then

                If is_update_required Then
                    updpara = updpara & ",start_date='" & dtStartDate.SelectedDate.Value & "'"
                Else
                    updpara = updpara & "start_date='" & dtStartDate.SelectedDate.Value & "'"
                End If
                is_update_required = True
                is_time_change = True
            End If
            If lblEndDate.Text.Trim() <> dtEndDate.SelectedDate.Value Then
                If is_update_required Then
                    updpara = updpara & ",end_date='" & dtEndDate.SelectedDate.Value & "',display_end_time='" & dtEndDate.SelectedDate.Value & "'"
                Else
                    updpara = updpara & "end_date='" & dtEndDate.SelectedDate.Value & "',display_end_time='" & dtEndDate.SelectedDate.Value & "'"
                End If
                is_update_required = True
                is_time_change = True
            End If

            If is_update_required Then
                updpara = updpara & ",is_display_start_date='" & chk_is_display_start_date.Checked & "'"
            Else
                updpara = updpara & "is_display_start_date='" & chk_is_display_start_date.Checked & "'"
            End If
            is_update_required = True

            updpara = updpara & ",is_display_end_date='" & chk_is_display_end_date.Checked & "'"

            If lbl_before_time.Text <> txt_before_time.Text Then
                If is_update_required Then
                    updpara = updpara & ",bid_over_before_time='" & txt_before_time.Text & "'"
                Else
                    updpara = updpara & "bid_over_before_time='" & txt_before_time.Text & "'"
                End If
                is_update_required = True
            End If
            If lbl_after_time.Text <> txt_after_time.Text Then
                If is_update_required Then
                    updpara = updpara & ",bid_over_increase_time='" & txt_after_time.Text & "'"
                Else
                    updpara = updpara & "bid_over_increase_time='" & txt_after_time.Text & "'"
                End If
                is_update_required = True
            End If
            'If lbl_time_zone.Text <> txt_time_zone.Text Then
            '    If is_update_required Then
            '        updpara = updpara & ",time_zone='" & txt_time_zone.Text & "'"
            '    Else
            '        updpara = updpara & "time_zone='" & txt_time_zone.Text & "'"
            '    End If
            '    is_update_required = True
            'End If

            If is_update_required Then
                updpara = updpara & ",update_date=getdate(),update_by_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
                SqlHelper.ExecuteNonQuery("update tbl_auctions set " & updpara & " where auction_id=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
                CommonCode.insert_system_log("Auction bidding details updated", "btnUpdateBiddingDetails_Click", Request.QueryString.Get("i"), "", "Auction")
            End If


            If is_time_change = True Then
                Response.Redirect(Request.Url.ToString())
            Else
                setDetailsEdit(True)
                setDetailsEditSchedule(True)
            End If
        End If


    End Sub

    Protected Sub ddl_auction_type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_auction_type.SelectedIndexChanged
        setdetailsmiddlesection()
    End Sub

    Private Sub setdetailsmiddlesection()

        pnl_details1.Visible = False
        pnl_details2.Visible = False
        pnl_details3.Visible = False
        pnl_details4.Visible = False
        div_auction_type.Visible = False

        Select Case ddl_auction_type.SelectedValue
            Case "1"
                pnl_details1.Visible = True
            Case "2"
                pnl_details2.Visible = True
                div_auction_type.Visible = True
            Case "3"
                pnl_details3.Visible = True
                div_auction_type.Visible = True
            Case "4"
                pnl_details4.Visible = True
            Case Else
                pnl_details3.Visible = True
                div_auction_type.Visible = True

        End Select
        lbl_auction_type_desc.Text = ddl_auction_type.SelectedItem.Text
    End Sub

    Protected Sub chk_proxy_bid_allow_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_proxy_bid_allow.CheckedChanged
        If chk_proxy_bid_allow.Checked = False Then
            If chk_live_bid_allow.Checked = False Then
                chk_live_bid_allow.Checked = True
            End If
        End If
    End Sub

    Protected Sub chk_live_bid_allow_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_live_bid_allow.CheckedChanged
        If chk_live_bid_allow.Checked = False Then
            If chk_proxy_bid_allow.Checked = False Then
                chk_proxy_bid_allow.Checked = True
            End If
        End If
    End Sub

#End Region

#Region "After Launch"

    Private Sub setAfterLaunchEdit(ByVal readMode As Boolean)
        If Request.QueryString.Get("i") <> "0" Then
            fillAfterLaunchData(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
        End If
        visibleAfterLaunchData(readMode)
    End Sub
    Private Sub fillAfterLaunchData(ByVal auction_id As Integer)

        Dim dt As DataTable = New DataTable()
        dt = SqlHelper.ExecuteDatatable("SELECT isnull(A.after_launch_message,'') as after_launch_message FROM tbl_auctions A WITH (NOLOCK) WHERE A.auction_id = " & auction_id)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lbl_after_launch.Text = CommonCode.decodeSingleQuote(.Item("after_launch_message"))
                RadEditor2.Content = lbl_after_launch.Text
            End With
        End If
        dt = Nothing

    End Sub

    Private Sub visibleAfterLaunchData(ByVal readMode As Boolean)

        If readMode Then

            btnEditAfterLaunch.Visible = True
            btnUpdateAfterLaunch.Visible = False
            divCancelAfterLaunch.Visible = False
        Else
            btnEditAfterLaunch.Visible = False
            btnUpdateAfterLaunch.Visible = True
            divCancelAfterLaunch.Visible = True
        End If

        lbl_after_launch.Visible = readMode
        pnl_after_launch_editor.Visible = Not readMode
        If Not CommonCode.is_admin_user Then
            btnEditAfterLaunch.Visible = False
            btnUpdateAfterLaunch.Visible = False
            divCancelAfterLaunch.Visible = False
            dv_after_launch_attach.Visible = False
        End If
    End Sub

    Protected Sub btnEditAfterLaunch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditAfterLaunch.Click
        visibleAfterLaunchData(False)
    End Sub
    Protected Sub btnCancelAfterLaunch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelAfterLaunch.Click
        visibleAfterLaunchData(True)
    End Sub
    Protected Sub btnUpdateAfterLaunch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpdateAfterLaunch.Click
        If Page.IsValid Then
            Dim is_update_required As Boolean = False
            Dim updpara As String = ""

            If RadEditor2.Content.Trim() <> lbl_after_launch.Text.Trim() Then

                If is_update_required Then
                    updpara = updpara & ",after_launch_message='" & CommonCode.encodeSingleQuote(RadEditor2.Content.Trim()) & "'"
                Else
                    updpara = updpara & "after_launch_message='" & CommonCode.encodeSingleQuote(RadEditor2.Content.Trim()) & "'"
                End If
                is_update_required = True
            End If

            If is_update_required Then
                updpara = updpara & ",update_date=getdate(),update_by_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
                SqlHelper.ExecuteNonQuery("update tbl_auctions set " & updpara & " where auction_id=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
                CommonCode.insert_system_log("After launch comment updated.", "btnUpdateAfterLaunch_Click", Request.QueryString.Get("i"), "", "Auction")
            End If

            setAfterLaunchEdit(True)
        End If
    End Sub

    Protected Sub load_auction_bidder_queries()
        If hid_auction_id.Value > 0 Then
            'Dim rem_qty As Integer = SqlHelper.ExecuteScalar(" select count(A.buyer_id) from dbo.fn_get_invited_buyers(" & hid_auction_id.Value & ") A INNER JOIN tbl_reg_buyers B ON A.buyer_id=B.buyer_id and A.buyer_id not in( select buyer_id from tbl_auction_fedex_rates where auction_id=" & hid_auction_id.Value & ")")
            'If rem_qty > 0 Then
            '    lbl_count_remaining.Text = "<a style=""color:red;"" href=""javascript:void(0);"" onclick=""javascript:return invited_bidders(" & Request.QueryString.Get("i") & ",1);"">" & rem_qty & " bidder left for Shipping rates</a>"
            'Else
            '    rem_qty = SqlHelper.ExecuteScalar(" select count(A.buyer_id) from dbo.fn_get_invited_buyers(" & hid_auction_id.Value & ") A INNER JOIN tbl_reg_buyers B ON A.buyer_id=B.buyer_id and A.buyer_id in( select buyer_id from tbl_auction_fedex_rates where is_error=1 and auction_id=" & hid_auction_id.Value & ")")
            '    If rem_qty > 0 Then
            '        lbl_count_remaining.Text = rem_qty & " bidder have error on Shipping rates"
            '    Else
            '        lbl_count_remaining.Text = ""
            '    End If

            'End If


            Dim qry As String = "select A.query_id,A.title,A.submit_date,A.buyer_id,B.company_name,isnull(case when B.state_id<>0 then (select name from tbl_master_states where state_id=B.state_id) else B.state_text end,'') as state,isnull(A.is_active,0) as is_active,ISNULL(admin_marked,0) As admin_marked,(select count(query_id) from tbl_auction_queries where parent_query_id=A.query_id) As answered_count from tbl_auction_queries A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id inner join tbl_auctions A1 WITH (NOLOCK) on A.auction_id=A1.auction_id where A.parent_query_id=0 and A.auction_id=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i"))
            lnk_all.CssClass = "filterBy"
            lnk_pending.CssClass = "filterBy"
            lnk_ans.CssClass = "filterBy"

            If ViewState("qry_filter") = "all" Then
                lnk_all.CssClass = "filterByBold"
                lbl_no_record.Text = "Currently bidder query is not available"
            End If
            If ViewState("qry_filter") = "answered" Then
                qry = qry & " and (ISNULL(admin_marked,0)=1 or A.query_id in (select parent_query_id from tbl_auction_queries))"
                lnk_ans.CssClass = "filterByBold"
                lbl_no_record.Text = "Currently answered query is not available"
            End If
            If ViewState("qry_filter") = "pending" Then
                qry = qry & " and (ISNULL(A.admin_marked,0)=0 and A.query_id not in (select parent_query_id from tbl_auction_queries))"
                lnk_pending.CssClass = "filterByBold"
                lbl_no_record.Text = "Currently pending query is not available"
            End If

            If ViewState("order_by") = "dateA" Then
                qry = qry & " order by submit_date"
            End If
            If ViewState("order_by") = "dateD" Then
                qry = qry & " order by submit_date desc"
            End If
            If ViewState("order_by") = "sales_rep" Then
                qry = qry & " order by S.first_name asc"
            End If
            If ViewState("order_by") = "customer" Then
                qry = qry & " order by B.company_name"
            End If
            'qry = qry & " order by submit_date desc"
            rep_after_queries.DataSource = SqlHelper.ExecuteDatatable(qry)
            rep_after_queries.DataBind()
            If rep_after_queries.Items.Count > 0 Then
                rep_after_queries.Visible = True
                lbl_no_record.Visible = False
            Else
                rep_after_queries.Visible = False
                lbl_no_record.Visible = True
            End If

        End If
    End Sub

    Protected Sub lnk_all_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_all.Click
        ViewState("qry_filter") = "all"
        load_auction_bidder_queries()
    End Sub
    Protected Sub lnk_pending_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_pending.Click
        ViewState("qry_filter") = "pending"
        load_auction_bidder_queries()
    End Sub
    Protected Sub lnk_ans_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_ans.Click
        ViewState("qry_filter") = "answered"
        load_auction_bidder_queries()
    End Sub

    Protected Sub rep_after_queries_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rep_after_queries.ItemCommand
        If e.CommandName = "send_query" Then
            Dim Rsp As String = DirectCast(e.Item.FindControl("rep_txt_message"), TextBox).Text.Trim.Replace("'", "''").Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")
            If Rsp <> "Response" Then
                SqlHelper.ExecuteNonQuery("insert into tbl_auction_queries (sales_rep_id,auction_id,message,buyer_id,user_id,parent_query_id,submit_date) values (0," & hid_auction_id.Value & ",'" & Rsp & "'," & DirectCast(e.Item.FindControl("rep_hid_buyer"), HiddenField).Value & "," & hid_user_id.Value & "," & e.CommandArgument & ",getdate())")
                CommonCode.insert_system_log("Auction After Launch Query Send", "rep_after_queries_ItemCommand", Request.QueryString.Get("i"), "", "Auction")
                Dim obj_email As New Email
                obj_email.Send_Bidder_Reply(CommonCode.Fetch_Cookie_Shared("user_id"), e.CommandArgument, Rsp)
                load_auction_bidder_queries()
            End If
        ElseIf e.CommandName = "mark" Then
            SqlHelper.ExecuteNonQuery("update tbl_auction_queries set admin_marked=1 where query_id=" & e.CommandArgument)
            load_auction_bidder_queries()
        ElseIf e.CommandName = "send_note" Then

            Dim nts As String = DirectCast(e.Item.FindControl("txt_note"), TextBox).Text
            If nts <> "" Then
                SqlHelper.ExecuteNonQuery("INSERT INTO tbl_auction_query_note(query_id, message, created_date, created_by) VALUES (" & e.CommandArgument & ", '" & nts & "', getdate(), " & hid_user_id.Value & ")")
                DirectCast(e.Item.FindControl("txt_note"), TextBox).Text = ""
                Dim rep_notes As Repeater = DirectCast(e.Item.FindControl("rptr_note"), Repeater)
                rep_notes.DataSource = SqlHelper.ExecuteDatatable("SELECT ISNULL(A.message, '') AS message,ISNULL(U.first_name, '') + ' ' + ISNULL(U.last_name, '') as full_name FROM tbl_auction_query_note A inner join tbl_sec_users U on A.created_by=U.user_id	WHERE A.query_id=" & e.CommandArgument & " order by A.qry_note_id ")
                rep_notes.DataBind()
            End If
        End If

    End Sub
    Protected Sub ddlSortBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlSortBy.SelectedIndexChanged
        ViewState("order_by") = ddlSortBy.SelectedItem.Value
        load_auction_bidder_queries()
    End Sub
    Protected Sub rep_after_queries_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rep_after_queries.ItemDataBound
        Dim rep_inner As Repeater = DirectCast(e.Item.FindControl("rep_inner_after_queries"), Repeater)
        rep_inner.DataSource = SqlHelper.ExecuteDatatable("select A.message, A.submit_date, ISNULL(C.first_name,'') AS first_name,ISNULL(C.last_name,'') AS last_name,ISNULL(D.first_name,'') AS buyer_first_name,ISNULL(D.last_name,'') AS buyer_last_name,isnull(C.title,'') as title,isnull(D.title,'') as buyer_title,isnull(C.image_path,'') as image_path,C.user_id from tbl_auction_queries A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id left join tbl_sec_users C on A.user_id=C.user_id LEFT JOIN tbl_reg_buyer_users D ON A.buyer_user_id=D.buyer_user_id where A.parent_query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & " order by submit_date DESC")
        rep_inner.DataBind()

        Dim rep_notes As Repeater = DirectCast(e.Item.FindControl("rptr_note"), Repeater)
        rep_notes.DataSource = SqlHelper.ExecuteDatatable("SELECT ISNULL(A.message, '') AS message,ISNULL(U.first_name, '') + ' ' + ISNULL(U.last_name, '') as full_name FROM tbl_auction_query_note A inner join tbl_sec_users U on A.created_by=U.user_id	WHERE A.query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & " order by A.qry_note_id ")
        rep_notes.DataBind()

    End Sub

#End Region

    Protected Sub but_bind_tab_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_bind_tab.Click
        setOtherDetailsEdit(True)
        TabStip1.SelectedIndex = hid_other_detail_tab.Value
    End Sub

    Protected Sub but_bind_tab_terms_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_bind_tab_terms.Click
        setOtherDetailsEdit(True)
        TabStip2.SelectedIndex = hid_other_detail_tab.Value
    End Sub

    Protected Sub btn_auction_delete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_auction_delete.Click
        Dim obj As New Auction
        If obj.delete_auction(Request.QueryString.Get("i")) > 0 Then
            SqlHelper.ExecuteNonQuery("if exists(select auction_id from tbl_auctions WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i") & ") begin delete from tbl_login_user_auctions where auction_id=" & Request.QueryString.Get("i") & "; update tbl_auctions set is_active=0,discontinue=1  where auction_id=" & Request.QueryString.Get("i") & " end")

            Response.Redirect("/Auctions/AuctionListing.aspx")
        End If
        obj = Nothing

    End Sub
    Private Sub set_form_mode(ByVal flag As Boolean)
        chklst_flag_name.Visible = flag
        ltrl_flag_name.Visible = Not (flag)

    End Sub
    Private Sub fill_flag_types()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select ISNULL(A.flag_id, 0) AS flag_id, ISNULL(A.flag, '') AS flag from tbl_master_flags A where is_active=1 order by A.flag"
        ds = SqlHelper.ExecuteDataset(strQuery)
        chklst_flag_name.DataSource = ds
        chklst_flag_name.DataTextField = "flag"
        chklst_flag_name.DataValueField = "flag_id"
        chklst_flag_name.DataBind()
        ds.Dispose()
    End Sub
    Private Function get_flag_name_types(ByVal auction_id As Integer) As DataTable
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable()
        strQuery = "SELECT 	ISNULL(A.auction_id, 0) AS auction_id,ISNULL(A.flag_id, 0) AS flag_id,ISNULL(B.flag,'') as flag " & _
                    "FROM tbl_auction_flags A INNER JOIN tbl_master_flags B ON A.flag_id=B.flag_id " & _
                    "WHERE A.auction_id =" & auction_id.ToString()
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function

    Private Function get_flag_name_string(ByVal auction_id As Integer) As String
        Dim str As String = ""
        Dim strQuery As String = "Declare @str varchar(1000) select @str=COALESCE(@str + ', ', '') + ISNULL(B.flag,'') FROM tbl_auction_flags A INNER JOIN tbl_master_flags B ON A.flag_id=B.flag_id " & _
            "WHERE A.auction_id =" & auction_id.ToString() & " select ISNULL(@str,'')"
        str = SqlHelper.ExecuteScalar(strQuery)
        'Response.Write(strQuery)
        If str = "" Then
            str = "No Flag Choosen."
        End If
        Return str
    End Function

    Private Sub BindflagInfo()
        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            Dim auction_id As Integer = Request.QueryString.Get("i")
            'Response.Write(auction_id)
            Dim dt As DataTable = New DataTable()
            Dim strQuery As String = ""
            strQuery = "SELECT ISNULL(A.flag_id, 0) AS flag_id, ISNULL(A.flag, '') AS flag, ISNULL(A.is_active, 0) AS is_active, ISNULL(B.mapping_id, 0) AS mapping_id, ISNULL(B.auction_id, 0) AS auction_id FROM tbl_master_flags A INNER JOIN tbl_auction_flags B ON A.flag_id=B.flag_id WHERE A.is_active=1"
            dt = SqlHelper.ExecuteDatatable(strQuery)

            If dt.Rows.Count > 0 Then
                dt = get_flag_name_types(auction_id)
                For i As Integer = 0 To dt.Rows.Count - 1
                    For Each lst As ListItem In chklst_flag_name.Items
                        If dt.Rows(i)("flag_id") = lst.Value Then
                            lst.Selected = True
                        End If
                    Next
                Next
                dt = Nothing
                Dim str As String = ""
                str = get_flag_name_string(auction_id)
                ltrl_flag_name.Text = str
            End If
        End If

    End Sub
    Private Sub Update_flag_name(ByVal auction_id As Integer)
        For Each lst As ListItem In chklst_flag_name.Items
            Dim strQuery As String = ""
            Dim i As String = lst.Value
            If lst.Selected Then
                strQuery = "if not exists(select mapping_id from tbl_auction_flags where auction_id=" & auction_id.ToString() & " and flag_id=" & i & ") insert into tbl_auction_flags(auction_id,flag_id) Values(" & auction_id.ToString() & "," & i & ")"
            Else
                strQuery = "delete from tbl_auction_flags where auction_id=" & auction_id.ToString() & " and flag_id=" & i
            End If

            SqlHelper.ExecuteNonQuery(strQuery)
        Next
    End Sub
    Private Sub basicUpdate()
        Dim auction_id As Integer = IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i"))
        Dim i As Integer = 0


        Dim strQuery As String = ""
        Update_flag_name(auction_id)
        set_form_mode(False)
        visibleBasicData(False)
       
    End Sub

    Protected Sub but_bind_afterlaunch_Click(sender As Object, e As System.EventArgs) Handles but_bind_afterlaunch.Click
        load_auction_bidder_queries()
    End Sub

   
End Class
