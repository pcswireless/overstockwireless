﻿Imports Telerik.Web.UI
Imports System.Drawing
Partial Class Auctions_AuctionEmailSchedule
    Inherits System.Web.UI.Page
    Private Sub setPermission()

        If CommonCode.is_super_admin() Then
            rad_grid_sublogins.Visible = True
            lbl_no_permit.Visible = False
            rad_grid_sublogins.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
            rad_grid_sublogins.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
            rad_grid_sublogins.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
        Else

            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 4
                        rad_grid_sublogins.Visible = True
                        lbl_no_permit.Visible = False
                    Case 35
                        rad_grid_sublogins.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
                        rad_grid_sublogins.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
                        rad_grid_sublogins.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
                End Select
            Next
            dt = Nothing
        End If
    End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        setPermission()
        Page.Server.ScriptTimeout = 720000
    End Sub
    Protected Function checked(ByVal r As Object) As Boolean
        If r Is DBNull.Value Then
            Return True
        ElseIf r = "After" Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function getcheckedValue(ByVal r As Object) As String
        If r Is DBNull.Value Then
            Return "Before Start"
        ElseIf r = "Before End" Then
            Return "Before End"
        ElseIf r = "After End" Then
            Return "After End"
        Else
            Return "Before Start"
        End If
    End Function
    Protected Sub rad_grid_sublogins_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)

        Dim strQuery As String = "SELECT [email_schedule_id],[title],[days],[hours],[schedule_type],ISNULL(is_active,0) As is_active FROM tbl_auction_email_schedules"
        rad_grid_sublogins.DataSource = SqlHelper.ExecuteDatatable(strQuery)

    End Sub
    Protected Sub rad_grid_sublogins_InsertCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)

        'Get the GridEditFormInsertItem of the RadGrid        
        ' Dim insertedItem As GridDataInsertItem = DirectCast(e.Item, GridDataInsertItem)
        Dim insertedItem As GridEditFormInsertItem = DirectCast(e.Item, GridEditFormInsertItem)
        'Dim title As String = (TryCast(insertedItem("title").Controls(0), TextBox)).Text
        Dim title As String = (TryCast(insertedItem.FindControl("txt_title"), TextBox)).Text

        ' Dim days As Integer = Convert.ToInt32((TryCast(insertedItem("days").Controls(0), TextBox)).Text)
        Dim days As Integer = Convert.ToInt32((TryCast(insertedItem.FindControl("txt_days"), TextBox)).Text)
        '  Dim hours As Integer = Convert.ToInt32((TryCast(insertedItem("hours").Controls(0), TextBox)).Text)
        Dim hours As Integer = Convert.ToInt32((TryCast(insertedItem.FindControl("txt_hours"), TextBox)).Text)
        Dim ddl As DropDownList = (TryCast(insertedItem.FindControl("ddl_schedule_type"), DropDownList))

        Dim chk_is_active As CheckBox = TryCast(insertedItem.FindControl("chk_is_active"), CheckBox)
        Dim is_active As Integer = 0
        If chk_is_active.Checked Then
            is_active = 1
        Else
            is_active = 0
        End If
        Dim email_schedule_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_auction_email_schedules(title,days,hours,schedule_type,submit_date,submit_by_user_id,is_active) VALUES ('" & title & "'," & days & "," & hours & ",'" & ddl.SelectedItem.Text & "',getdate(), " & CommonCode.Fetch_Cookie_Shared("user_id") & "," & is_active & ") select scope_identity()")
        rad_grid_sublogins.Controls.Add(New LiteralControl(String.Format("<span style='color:red'>New Item Inserted</span>")))

    End Sub
    Protected Sub rad_grid_sublogins_UpdateCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        Dim editedItem As GridEditFormItem = TryCast(e.Item, GridEditFormItem)
        Dim email_schedule_id As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("email_schedule_id").ToString()
        'Dim title As String = (TryCast(editedItem("title").Controls(0), TextBox)).Text
        Dim title As String = (TryCast(editedItem.FindControl("txt_title"), TextBox)).Text
        'Dim days As Integer = Convert.ToInt32((TryCast(editedItem("days").Controls(0), TextBox)).Text)
        Dim days As Integer = Convert.ToInt32((TryCast(editedItem.FindControl("txt_days"), TextBox)).Text)
        ' Dim hours As Integer = Convert.ToInt32((TryCast(editedItem("hours").Controls(0), TextBox)).Text)
        Dim hours As Integer = Convert.ToInt32((TryCast(editedItem.FindControl("txt_hours"), TextBox)).Text)

        Dim chk_is_active As CheckBox = TryCast(editedItem.FindControl("chk_is_active"), CheckBox)
        Dim is_active As Integer = 0
        If chk_is_active.Checked Then
            is_active = 1
        Else
            is_active = 0
        End If
        Dim ddl As DropDownList = (TryCast(editedItem.FindControl("ddl_schedule_type"), DropDownList))


        SqlHelper.ExecuteNonQuery("update tbl_auction_email_schedules set title='" & title & "',days=" & days & ",hours=" & hours & ",schedule_type='" & ddl.SelectedItem.Text & "',is_active=" & is_active & " where email_schedule_id=" & email_schedule_id)
        rad_grid_sublogins.Controls.Add(New LiteralControl(String.Format("<span style='color:red'>Email Schedule Updated</span>")))
    End Sub
    Protected Sub rad_grid_sublogins_DeleteCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        Dim email_schedule_id As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("email_schedule_id").ToString()
        SqlHelper.ExecuteNonQuery("delete from tbl_auction_email_schedule_statuses where email_schedule_id = " & email_schedule_id & "; DELETE from tbl_auction_email_schedules where email_schedule_id='" & email_schedule_id & "'")
        rad_grid_sublogins.Controls.Add(New LiteralControl(String.Format("<span style='color:red'>Item Deleted</span>")))

    End Sub
    
    Protected Sub rad_grid_sublogins_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles rad_grid_sublogins.PreRender
        Dim menu As GridFilterMenu = rad_grid_sublogins.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Or menu.Items(i).Text.ToLower = "equalto" Or menu.Items(i).Text.ToLower = "notequalto" Or menu.Items(i).Text.ToLower = "greaterthan" Or menu.Items(i).Text.ToLower = "lessthan" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub


    Protected Sub btn_send_email_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_send_email.Click

        'Dim dt As New DataTable
        'dt = SqlHelper.ExecuteDatatable("select F.email_schedule_id,F.auction_id,isnull(E.title,'') as email_title,isnull(A.title,'') as auction_title,F.email_id,(B.contact_first_name+' '+B.contact_first_name) as name from dbo.fn_get_email_schedule_list() F inner join tbl_auction_email_schedules E on E.email_schedule_id=F.email_schedule_id inner join tbl_auctions A on A.auction_id=F.auction_id inner join tbl_reg_buyers B on B.buyer_id=F.buyer_id")


        'Dim objEmail As New Email()
        'Dim str As String = "select E.schedule_type,E.email_schedule_id from tbl_auction_email_schedules E where E.is_active=1"
        'dt = SqlHelper.ExecuteDatatable(str)
        'If dt.Rows.Count > 0 Then

        '    For i As Integer = 0 To dt.Rows.Count - 1

        '        Select Case dt.Rows(i)("schedule_type").ToString().ToUpper()
        '            Case "BEFORE START"
        '                objEmail.sendInvitationBeforeMail()
        '            Case "BEFORE END"
        '                objEmail.sendInvitationBeforeEndMail()
        '            Case "AFTER END"
        '                objEmail.sendInvitationAfterMail()
        '        End Select
        '    Next

        'End If

        'dt = Nothing

        'objEmail = Nothing
        Dim invitation_ids As String = ""
        Dim reminder_ids As String = ""
        Dim thanks_ids As String = ""
        Dim i As Integer

        For i = 0 To RadGrid_email_schedule.Items.Count - 1
            If RadGrid_email_schedule.Items(i).ItemType = GridItemType.AlternatingItem Or RadGrid_email_schedule.Items(i).ItemType = GridItemType.Item Then
                If CType(RadGrid_email_schedule.Items(i).FindControl("chk_select"), CheckBox).Checked Then
                    If CType(RadGrid_email_schedule.Items(i).FindControl("lit_email_type"), Literal).Text.Trim().ToUpper() = "BEFORE START" Then
                        If invitation_ids = "" Then
                            invitation_ids = CType(RadGrid_email_schedule.Items(i).FindControl("lit_email_schedule_id"), Literal).Text & "," & CType(RadGrid_email_schedule.Items(i).FindControl("lit_auction_id"), Literal).Text
                        Else
                            invitation_ids = invitation_ids & ";" & CType(RadGrid_email_schedule.Items(i).FindControl("lit_email_schedule_id"), Literal).Text & "," & CType(RadGrid_email_schedule.Items(i).FindControl("lit_auction_id"), Literal).Text
                        End If
                    End If
                    If CType(RadGrid_email_schedule.Items(i).FindControl("lit_email_type"), Literal).Text.Trim().ToUpper() = "BEFORE END" Then
                        If reminder_ids = "" Then
                            reminder_ids = CType(RadGrid_email_schedule.Items(i).FindControl("lit_email_schedule_id"), Literal).Text & "," & CType(RadGrid_email_schedule.Items(i).FindControl("lit_auction_id"), Literal).Text
                        Else
                            reminder_ids = reminder_ids & ";" & CType(RadGrid_email_schedule.Items(i).FindControl("lit_email_schedule_id"), Literal).Text & "," & CType(RadGrid_email_schedule.Items(i).FindControl("lit_auction_id"), Literal).Text
                        End If
                    End If
                    If CType(RadGrid_email_schedule.Items(i).FindControl("lit_email_type"), Literal).Text.Trim().ToUpper() = "AFTER END" Then
                        If thanks_ids = "" Then
                            thanks_ids = CType(RadGrid_email_schedule.Items(i).FindControl("lit_email_schedule_id"), Literal).Text & "," & CType(RadGrid_email_schedule.Items(i).FindControl("lit_auction_id"), Literal).Text
                        Else
                            thanks_ids = thanks_ids & ";" & CType(RadGrid_email_schedule.Items(i).FindControl("lit_email_schedule_id"), Literal).Text & "," & CType(RadGrid_email_schedule.Items(i).FindControl("lit_auction_id"), Literal).Text
                        End If
                    End If
                End If
            End If
        Next

        If invitation_ids <> "" Or reminder_ids <> "" Or thanks_ids <> "" Then

            Dim objEmail As New Email()
            If invitation_ids <> "" Then objEmail.sendInvitationBeforeMail(invitation_ids)
            If reminder_ids <> "" Then objEmail.sendInvitationBeforeEndMail(reminder_ids)
            If thanks_ids <> "" Then objEmail.sendInvitationAfterMail(thanks_ids)
            objEmail = Nothing

            bind_queue()
            lbl_msg.Visible = True
            pnl_email_schedule.Visible = False

        End If


    End Sub
    Protected Function checkedActive(ByVal r As Object) As Boolean
        If r Is DBNull.Value Then
            Return True
        ElseIf r = True Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            bind_queue()
        End If
    End Sub

    Private Sub bind_queue()
        Dim dt As New DataTable
        dt = SqlHelper.ExecuteDatatable("select distinct F.email_schedule_id,F.auction_id,isnull(A.title,'') as auction_name,Upper(E.schedule_type) as email_type,COUNT(F.buyer_id) as no_of_email from dbo.fn_get_email_schedule_list() F inner join tbl_auction_email_schedules E on E.email_schedule_id=F.email_schedule_id inner join tbl_auctions A WITH (NOLOCK) on A.auction_id=F.auction_id group by F.email_schedule_id,F.auction_id,isnull(A.title,''),E.schedule_type")
        If dt.Rows.Count > 0 Then
            RadGrid_email_schedule.DataSource = dt
            RadGrid_email_schedule.DataBind()
        Else

            pnl_email_schedule.Visible = False

        End If
        dt.Dispose()
    End Sub
End Class
