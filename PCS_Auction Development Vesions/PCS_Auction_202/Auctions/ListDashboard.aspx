﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ListDashboard.aspx.vb" Inherits="Auctions_ListDashboard" %>

<%@ Register Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" TagPrefix="Telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="refresh" content="60" runat="server" />
    <link type="text/css" rel="Stylesheet" href="/Style/master_menu.css" />
    <link type="text/css" rel="Stylesheet" href="/Style/pcs.css" />
    <script type="text/javascript" language="javascript">
        function invite_bidders(id) {
            window.open('/Auctions/InviteBidders.aspx?i=' + id, 'invite_bidders', 'left=' + ((screen.width - 1000) / 2) + ',top=' + (0) + ',width=1000,height=700,scrollbars=yes,resizable=no,toolbars=no');
            return false;
        }
        function bidder_querries(id) {
            window.open('/Auctions/BidderQueries.aspx?i=' + id, 'bidder_querries', 'left=' + ((screen.width - 600) / 2) + ',top=' + (0) + ',width=600,height=500,scrollbars=yes,resizable=no,toolbars=no');
            return false;
        }
        function after_launch_comments(id) {
            window.open('/Auctions/ViewAddAfterLaunchComment.aspx?i=' + id, 'bidder_comments', 'left=' + ((screen.width - 800) / 2) + ',top=' + (0) + ',width=800,height=700,scrollbars=yes,resizable=no,toolbars=no');
            return false;
        }
        function invited_bidders(id) {
            window.open('/Auctions/ListInvitedBidders.aspx?i=' + id, 'invited_bidder', 'left=' + ((screen.width - 800) / 2) + ',top=' + (0) + ',width=800,height=700,scrollbars=yes,resizable=no,toolbars=no');
            return false;
        }
        function auction_items(id) {
            window.open('/Auctions/ListAuctionItems.aspx?i=' + id, 'invited_bidder', 'left=' + ((screen.width - 800) / 2) + ',top=' + (0) + ',width=800,height=700,scrollbars=yes,resizable=no,toolbars=no');
            return false;
        }       
    </script>
</head>
<body style="margin: 0px;">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptManagerChild" runat="server">
    </asp:ScriptManager>
    <Telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Simple">
        <AjaxSettings>
            <Telerik:AjaxSetting AjaxControlID="rad_pnl_dashboard">
                <UpdatedControls>
                    <Telerik:AjaxUpdatedControl ControlID="rad_pnl_dashboard" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </Telerik:AjaxSetting>
        </AjaxSettings>
    </Telerik:RadAjaxManager>
    <Telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" style="position:fixed;left:420px;top:180px;z-index:9999" runat="server" BackgroundPosition="Center">
      <img id="Image8" src="/images/img_loading.gif" />
  </telerik:RadAjaxLoadingPanel>
    <Telerik:RadAjaxPanel ID="rad_pnl_dashboard" runat="server" scrollbars="Vertical">
        <asp:Repeater ID="rad_grid_dash_board" runat="server">
            <ItemTemplate>
                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
                    <tr>
                        <td colspan="3" class="blue_bar" valign="top">
                            <div class="div_heading_left">
                                <asp:Label ID="lbl_title" runat="server" Text='<%# Eval("title") %>'></asp:Label>
                            </div>
                            <div class="div_heading_right">
                                <asp:Label ID="lbl_code" runat="server" Text='<%# Eval("code") %>'></asp:Label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 0px; width: 10%;" valign="top">
                            <div style="margin-top: 0px; height: 16px; margin-left: 15px;">
                                <img src="/Images/arrow_down.gif" alt="" border="none" /></div>
                        </td>
                        <td valign="top">
                            <div style="float: left; padding-top: 4px;">
                                <img src="/Images/view_icon.gif" border="none" /></div>
                            <div style="float: left; padding-left: 6px; padding-top: 8px; font-weight: bold;">
                                <a href="javascript:void(0);" onclick='javascript:return auction_items(<%# Eval("auction_id") %>);'
                                    style="text-decoration: none;">View Lot Details </a>
                            </div>
                        </td>
                        <td width="50%" align="right" style="font-size: 11px; font-weight: bold; padding-right: 10px;
                            padding-top: 3px;">
                            <span id="spanProxyStartDate" runat="server" visible='<%# IIF(Eval("is_display_start_date"),"True","False") %>'>
                                Started:&nbsp;<asp:Literal ID="ltrl_start_date" runat="server" Text='<%# Convert.ToDateTime(Eval("start_date")).ToString("MM/dd/yyyy HH:mm tt") %>'></asp:Literal>
                                <br />
                            </span><span style="color: Red; font-size: 11px;" id="spanProxyEndDate" runat="server"
                                visible='<%# IIF(Eval("is_display_end_date"),"True","False") %>'>Ending:&nbsp;<asp:Literal
                                    ID="ltrl_end_date" runat="server" Text='<%# Convert.ToDateTime(Eval("end_date")).ToString("MM/dd/yyyy HH:MM tt")  %>'></asp:Literal></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top" style="font-size:11px;">
                            <table cellpadding="0" cellspacing="0" style="width: 100%; border: 1px solid #F0F1EB;">
                                <tr>
                                    <td colspan="2" style="height: 27px; background-image: url(/Images/grdnt_green1-hdng.gif);
                                        background-repeat: repeat-x; border-bottom: 1px solid #F9FAF4" valign="top">
                                        <div style="float: left; color: #79921A; font-weight: bold; font-size: 12px; margin-top: 6px;
                                            margin-left: 8px;">
                                            <%# Eval("auction_category")%></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 91px; font-size: 11px; font-weight: bold; width: 45px; background-image: url(/Images/grdnt_green2.gif);
                                        background-repeat: repeat-x; padding-left: 8px; padding-top: 3px; padding-right: 3px;"
                                        valign="top">
                                        <%# Eval("auction_type")%>
                                    </td>
                                    <td valign="top" align="left" style="height: 91px; font-size: 11px; background-image: url(/Images/grdnt_green3.gif);
                                        background-repeat: repeat-x; padding: 3px;">
                                        <asp:Panel ID="pnlProxy" runat="server" Width="100%" Visible='<%# IIF(Eval("auction_type_id")="3","True","False") %>'>
                                            <table cellspacing="0" width="100%" cellpadding="2" border="0">
                                                <tr id="trProxyPrivateOffer" runat="server" visible='<%# IIF(Eval("is_private_offer"),"True","False") %>'>
                                                    <td width="28%" valign="top" style="font-size: 11px;">
                                                        <b>Private Offer</b>
                                                    </td>
                                                    <td>
                                                        Auto Acceptance Price: &nbsp;<%# "$" & Convert.ToDouble(Eval("auto_acceptance_price_private")).ToString("F2")%>
                                                    </td>
                                                </tr>
                                                <tr id="trProxyPartialOffer" runat="server" visible='<%# IIF(Eval("is_partial_offer"),"True","False") %>'>
                                                    <td>
                                                        <b>Partial Offer</b>
                                                    </td>
                                                    <td>
                                                        Auto Acceptance Price: &nbsp;<%# "$" & Convert.ToDouble(Eval("auto_acceptance_price_partial")).ToString("F2")%>
                                                        &nbsp;| Qty:
                                                        <%# Eval("auto_acceptance_quantity")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Start Price</b>
                                                    </td>
                                                    <td>
                                                        <%# "$" & Convert.ToDouble(Eval("start_price")).ToString("F2")%>
                                                        &nbsp;&nbsp;&nbsp;<span id="spanProxyReservePrice" runat="server" visible='<%# IIF(Eval("is_show_reserve_price"),"True","False") %>'>Reserve
                                                            Price:&nbsp;<%# "$" & Convert.ToDouble(Eval("reserve_price")).ToString("F2")%></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Increaments</b>
                                                    </td>
                                                    <td>
                                                        <%# "$" & Convert.ToDouble(Eval("increament_amount")).ToString("F2")%>
                                                        &nbsp;&nbsp;&nbsp; Bid Time Interval:&nbsp;<%#Eval("bid_time_interval")%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlTraditional" runat="server" Width="100%" Visible='<%# IIF(Eval("auction_type_id")="2","True","False") %>'>
                                            <table cellspacing="0" width="100%" cellpadding="2" border="0">
                                                <tr id="trTradPrivateOffer" runat="server" visible='<%# IIF(Eval("is_private_offer"),"True","False") %>'>
                                                    <td width="28%" valign="top">
                                                        <b>Private Offer</b>
                                                    </td>
                                                    <td>
                                                        Auto Acceptance Price: &nbsp;<%# "$" & Convert.ToDouble(Eval("auto_acceptance_price_private")).ToString("F2")%>
                                                    </td>
                                                </tr>
                                                <tr id="trTradPartialOffer" runat="server" visible='<%# IIF(Eval("is_partial_offer"),"True","False") %>'>
                                                    <td>
                                                        <b>Partial Offer</b>
                                                    </td>
                                                    <td>
                                                        Auto Acceptance Price: &nbsp;<%# "$" & Convert.ToDouble(Eval("auto_acceptance_price_partial")).ToString("F2")%>
                                                        &nbsp;| Qty:
                                                        <%# Eval("auto_acceptance_quantity")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Start Price</b>
                                                    </td>
                                                    <td>
                                                        <%# "$" & Convert.ToDouble(Eval("start_price")).ToString("F2")%>
                                                        &nbsp;&nbsp;&nbsp;<span id="spanProxyReservePrice1" runat="server" visible='<%# IIF(Eval("is_show_reserve_price"),"True","False") %>'>Reserve
                                                            Price:&nbsp;<%# "$" & Convert.ToDouble(Eval("reserve_price")).ToString("F2")%></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Increaments</b>
                                                    </td>
                                                    <td>
                                                        <%# "$" & Convert.ToDouble(Eval("increament_amount")).ToString("F2")%>
                                                        &nbsp;&nbsp;&nbsp; Bid Time Interval:&nbsp;<%#Eval("bid_time_interval")%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlBuyNow" runat="server" Width="100%" Visible='<%# IIF(Eval("auction_type_id")="1","True","False") %>'>
                                            <table cellspacing="0" width="100%" cellpadding="2" border="0">
                                                <tr id="trBuyNowPrivateOffer" runat="server" visible='<%# IIF(Eval("is_private_offer"),"True","False") %>'>
                                                    <td width="28%" valign="top">
                                                        <b>Private Offer</b>
                                                    </td>
                                                    <td>
                                                        Auto Acceptance Price: &nbsp;<%# "$" & Convert.ToDouble(Eval("auto_acceptance_price_private")).ToString("F2")%>
                                                    </td>
                                                </tr>
                                                <tr id="trBuyNowPartialOffer1" runat="server" visible='<%# IIF(Eval("is_partial_offer"),"True","False") %>'>
                                                    <td>
                                                        <b>Partial Offer</b>
                                                    </td>
                                                    <td>
                                                        Auto Acceptance Price: &nbsp;<%# "$" & Convert.ToDouble(Eval("auto_acceptance_price_partial")).ToString("F2")%>
                                                        &nbsp;| Qty:
                                                        <%# Eval("auto_acceptance_quantity")%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlReqQuote" runat="server" Width="100%" Visible='<%# IIF(Eval("auction_type_id")="4","True","False") %>'>
                                            <table cellspacing="0" width="100%" cellpadding="2" border="0">
                                                <tr>
                                                    <td>
                                                        <%# Eval("request_for_quote_message")%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="font-weight: bold; padding-top: 20px; padding-left: 30px;">
                            <% If Request.QueryString("st") = "r" Then%>
                            <span style="color: #585858; font-size: 18px;">Time Left</span><br />
                            <%# GetTimeLeft(Eval("end_date"))%>
                            <br />
                            <br />
                            <span style="color: #19479A; font-size: 16px;">Top Bid $7000</span>
                            <% Else%>
                            <span style="color: #D81B18; font-size: 18px;">Bid Closed</span>
                            <% End If%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top">
                            <table cellpadding="0" cellspacing="0" style="width: 100%; border: 1px solid #F0F1EB;">
                                <tr>
                                    <td colspan="2" style="height: 27px; background-image: url(/Images/grdnt_green1-hdng.gif);
                                        background-repeat: repeat-x; border-bottom: 1px solid #F9FAF4;" valign="top">
                                        <div style="float: left; color: #153A70; font-weight: bold; font-size: 12px; margin-left: 8px;
                                            font-weight: bold; padding-top: 6px;">
                                            Current Top Bidders</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top">
                            <div class="ull">
                                <ul class="ul">
                                    <li><a href="javascript:void(0);" onclick='javascript:return invited_bidders(<%# Eval("auction_id") %>);'>
                                        View Invited Bidders</a></li>
                                    <li><a href="javascript:void(0);" onclick='javascript:return invite_bidders(<%# Eval("auction_id") %>);'>
                                        Invite More Bidders</a></li>
                                    <li><a href="javascript:void(0);" onclick='javascript:return bidder_querries(<%# Eval("auction_id") %>);'>
                                        View Bidder Queries</a></li>
                                    <li><a href="javascript:void(0);" onclick='javascript:return after_launch_comments(<%# Eval("auction_id") %>);'>
                                        View/Add After launch comments</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="padding: 10px;">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td>
                                        <img src="/Images/viewmore.png" alt="" />
                                    </td>
                                    <td align="right">
                                        <a href="/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %>" target="_parent">
                                            <img src="/Images/view_edit_auction.gif" border="none" alt="" /></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:Repeater>
        <div style="text-align: center; font-weight: bold; font-size: 13px; padding-top: 10px;">
            <asp:Label ID="lbl_no_record" runat="server" EnableViewState="false"></asp:Label>
        </div>
    </Telerik:RadAjaxPanel>
    </form>
</body>
</html>
