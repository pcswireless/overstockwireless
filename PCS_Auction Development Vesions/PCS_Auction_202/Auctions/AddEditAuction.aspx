﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    MaintainScrollPositionOnPostback="true" CodeFile="AddEditAuction.aspx.vb" Inherits="Auctions_AddEditAuction"
    ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/Auctions/UserControls/AuctionInvitation.ascx" TagName="AuctionInvitation"
    TagPrefix="uc" %>
<%@ Register Src="~/Auctions/UserControls/CurrentStatus.ascx" TagName="CurrentStatus"
    TagPrefix="uc" %>
<%@ Register Src="~/Auctions/UserControls/AuctionTemplate.ascx" TagName="AuctionTemplate"
    TagPrefix="uc" %>
<%@ Register Src="~/Auctions/UserControls/AuctionItems.ascx" TagName="AuctionItems"
    TagPrefix="uc1" %>
<%@ Register Src="~/Auctions/UserControls/Auction_Comments.ascx" TagName="AuctionComments"
    TagPrefix="uc3" %>
<%@ Register Src="~/Auctions/UserControls/AuctionEmailScheduleStatus.ascx" TagName="AuctionEmailScheduleStatus"
    TagPrefix="uc3" %>
<%@ Register Src="~/Log/UserControls/SystemLogLink.ascx" TagName="SystemLogLink"
    TagPrefix="uc4" %>
<%@ Register Src="~/Auctions/UserControls/auction_fedex.ascx" TagName="AuctionFedex"
    TagPrefix="ucf" %>
<%@ Register Src="~/Auctions/UserControls/LeftbarBucket.ascx" TagName="LeftbarBucket"
    TagPrefix="uc7" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
        <script type="text/javascript">

            //Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(Cancel_Ajax);
            function refresh_auction() {
                document.getElementById('<%=btn_refresh_basic.ClientID%>').click();
                return true;
            }

            function open_pop_download() {

                w1 = window.open('/Auctions/DownloadItemSheet.aspx?i=<%=Request("i") %>', '_DownloadProductSheet', 'top=' + ((screen.height - 300) / 2) + ', left=' + ((screen.width - 350) / 2) + ', height=300, width=350,scrollbars=no,toolbars=no,resizable=1;');
                w1.focus();
                return false;

            }

            function open_pop_upload() {

                w1 = window.open('/Auctions/UploadItems.aspx?i=<%=Request("i") %>', '_UploadItems', 'top=' + ((screen.height - 300) / 2) + ', left=' + ((screen.width - 350) / 2) + ', height=300, width=350,scrollbars=no,toolbars=no,resizable=1;');
                w1.focus();
                return false;

            }


            function openallquery(bidder_id) {
                window.open("/Auctions/BidderAllQuery.aspx?i=" + bidder_id, "_BidderAllQuery", "left=" + ((screen.width - 800) / 2) + ",top=" + ((screen.height - 400) / 2) + ",width=800,height=400,scrollbars=yes,toolbars=no,resizable=yes");
                return false;
            }
            function openpdf(auction_id) {
                window.open("/Auction_PdfNew.aspx?i=" + auction_id, "_auction_PDF", "left=" + ((screen.width - 800) / 2) + ",top=" + ((screen.height - 400) / 2) + ",width=800,height=400,scrollbars=yes,toolbars=no,resizable=yes");
                window.focus();
                return false;
            }
            function openpreview(auction_id) {
                window.open("/Auction_Details.aspx?a=" + auction_id + "&t=1&source=preview", "_openpreview", "left=" + ((screen.width - 800) / 2) + ",top=" + ((screen.height - 400) / 2) + ",width=800,height=400,scrollbars=yes,toolbars=no,resizable=yes");
                window.focus();
                return false;
            }
            function window_image_upload(auction_id) {
                window.open("/Auctions/UploadImage.aspx?i=" + auction_id, "_UploadImage", "left=" + ((screen.width - 800) / 2) + ",top=" + ((screen.height - 400) / 2) + ",width=800,height=400,scrollbars=yes,toolbars=no,resizable=yes");
                window.focus();
                return false;
            }
            //On insert and update buttons click temporarily disables ajax to perform upload actions
            function Cancel_Ajax(sender, args) {
                // args.set_enableAjax(false);
                args.set_cancel(true);
                sender._form.submit();
                return;

            }
            function conditionalPostback(e, sender) {
                sender.set_enableAjax(false);
            }


            //<![CDATA[
            var dtStartDate; // = document.getElementById("<%= dtStartDate.ClientID %>");
            var dtEndDate; // = document.getElementById("<%= dtEndDate.ClientID %>");
            var dtDisplayEndDate;

            function validate(sender, args) {
                //alert(dtEndDate);
                // return false;
                var Date1 = new Date(dtStartDate.get_selectedDate());
                var Date2 = new Date(dtEndDate.get_selectedDate());

                if ((Date2 - Date1) < 0) {

                    args.IsValid = false;
                }
                else {

                    args.IsValid = true;
                }
            }
            function validate_display(sender, args) {
                //alert(dtEndDate);
                // return false;
                var Date1 = new Date(dtEndDate.get_selectedDate());
                var Date2 = new Date(dtDisplayEndDate.get_selectedDate());

                if ((Date2 - Date1) <= 0) {

                    args.IsValid = false;
                }
                else {

                    args.IsValid = true;
                }
            }
            function onLoaddtStartDate(sender, args) {
                dtStartDate = sender;
            }

            function onLoaddtEndDate(sender, args) {
                dtEndDate = sender;
            }
            function onLoaddtDisplayEndDate(sender, args) {
                dtDisplayEndDate = sender;
            }
            function set_tab_index() {
                var _tabStrip = $find('<%=TabStip1.ClientID%>');
                var selectedTab = _tabStrip.get_selectedTab();
                if (selectedTab.get_index() > 0) {
                    document.getElementById('<%=hid_other_detail_tab.ClientID%>').value = selectedTab.get_index();
                }
                else { document.getElementById('<%=hid_other_detail_tab.ClientID%>').value = 0; }
                //alert("You have select tab at the index " + selectedTab.get_index() + " of tabstrip.");
            }
            function set_tab_index_terms() {
                var _tabStrip = $find('<%=TabStip2.ClientID%>');
                var selectedTab = _tabStrip.get_selectedTab();
                if (selectedTab.get_index() > 0) {
                    document.getElementById('<%=hid_other_detail_tab.ClientID%>').value = selectedTab.get_index();
                }
                else { document.getElementById('<%=hid_other_detail_tab.ClientID%>').value = 0; }
                //alert("You have select tab at the index " + selectedTab.get_index() + " of tabstrip.");
            }
            function tab_select_name() {
                if (document.getElementById('sp_auc_status')) {
                    var _status = document.getElementById('sp_auc_status').innerHTML;
                    if (_status.indexOf('Running') > 0) {
                        var _tabStrip = $find('<%=RadTabStrip1.ClientID%>');
                        var selectedTab = _tabStrip.get_selectedTab();
                        if (selectedTab.get_index() > 0) {
                            //alert(selectedTab.get_index());
                            document.getElementById('<%=hid_tab_name.ClientID%>').value = selectedTab.get_index();
                        }
                        else { document.getElementById('<%=hid_tab_name.ClientID%>').value = 0; }
                    }
                    else { document.getElementById('<%=hid_tab_name.ClientID%>').value = 0; }
                }
                else { document.getElementById('<%=hid_tab_name.ClientID%>').value = 0; }

                refresh_afterlaunch_currentstatus();
            }
            function refresh_afterlaunch(_option) {
                //alert('invitation');
                document.getElementById('<%=but_bind_afterlaunch.ClientID %>').disabled = false;
                document.getElementById('<%=but_bind_afterlaunch.ClientID %>').click();
                if (_option > 0) {
                    _invitation = setTimeout('refresh_afterlaunch_currentstatus()', 30000);
                }
                else if (_invitation) {
                    // alert(_refreshme);
                    clearTimeout(_invitation);
                    tab_select_name()
                }
            }

            //]]>
        </script>
    </telerik:RadScriptBlock>
    <asp:HiddenField ID="hid_other_detail_tab" runat="server" Value="0" />
    <asp:HiddenField ID="hid_terms_tab" runat="server" Value="0" />
    <asp:HiddenField ID="hid_tab_name" runat="server" Value="0" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btn_refresh_basic">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btn_basic_save">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btn_basic_edit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btn_basic_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btn_basic_cancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btn_auction_active">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_bind_tab">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel8" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_bind_tab_terms">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel5" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnEditBiddingDetails">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel44"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel4" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnEditBiddingDetailsSchedule">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel44"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel4" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateBiddingDetails">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel44"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel4" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateBiddingDetailsSchedule">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel44"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel4" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancelBiddingDetails">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel44"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel4" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancelBiddingDetailsSchedule">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel44"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel4" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadTabStrip3">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel44" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel4" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxPanel44">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadTabStrip3" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel4" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnEditAfterLaunch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel66"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel6" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateAfterLaunch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel66"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel6" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancelAfterLaunch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel66"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel6" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxPanel7">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel7" LoadingPanelID="RadAjaxLoadingPanel1">
                    </telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Skin="Simple" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" alt="" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
        <tr>
            <td valign="top">
                <div style="float: left;">
                    <div class="pageheading">
                        <asp:Literal ID="page_heading" runat="server" Text="New Auction"></asp:Literal>
                        <asp:Literal ID="ltrl_current_status" runat="server"></asp:Literal></div>
                    <div class="pagesubtitle">
                        <asp:Literal ID="page_sub_title" runat="server" Text=""></asp:Literal></div>
                </div>
                <div style="float: right; padding-right: 20px; width: 180px;">
                    <div style="float: left;">
                        <uc4:SystemLogLink ID="UC_System_log_link" runat="server" />
                    </div>
                    <div style="float: left; padding-left: 10px;">
                        <a href="javascript:void(0);" onclick="javascript:open_help('2');">
                            <img src="/Images/help.gif" border="none" />
                        </a>
                    </div>
                </div>
                <div style="float: right; width: 215px; padding-right: 10px;">
                    <iframe id="iframe_price" src='/timerframe.aspx?i=<%=Request.QueryString.Get("i") %>'
                        scrolling="no" frameborder="0" width="205px" height="23px"></iframe>
                </div>
            </td>
        </tr>
        <tr>
            <td align="left">
                <div style="float: left; width: 680px;">
                    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="" MultiPageID="RadMultiPage1"
                        SelectedIndex="0" Align="Left" OnClientTabSelected="tab_select_name">
                        <Tabs>
                            <telerik:RadTab CssClass="auctiontabSelected">
                                <TabTemplate>
                                    <span>
                                        <%= IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), "Add Auction Details", "Edit Auction")%></span>
                                </TabTemplate>
                            </telerik:RadTab>
                            <telerik:RadTab CssClass="auctiontab">
                                <TabTemplate>
                                    <span>Invitations</span>
                                </TabTemplate>
                            </telerik:RadTab>
                            <telerik:RadTab CssClass="auctiontab">
                                <TabTemplate>
                                    <span>FedEx</span>
                                </TabTemplate>
                            </telerik:RadTab>
                            <telerik:RadTab CssClass="auctiontab">
                                <TabTemplate>
                                    <span>After Launch</span>
                                </TabTemplate>
                            </telerik:RadTab>
                            <telerik:RadTab CssClass="auctiontab">
                                <TabTemplate>
                                    <span>Current Status</span>
                                </TabTemplate>
                            </telerik:RadTab>
                            <telerik:RadTab CssClass="auctiontab">
                                <TabTemplate>
                                    <span>Comments</span>
                                </TabTemplate>
                            </telerik:RadTab>
                            <telerik:RadTab CssClass="auctiontab">
                                <TabTemplate>
                                    <span>Filter Assign</span>
                                </TabTemplate>
                            </telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip>
                </div>
                <div style="float: right;">
                    <uc:AuctionTemplate ID="UC_AuctionTemplate" runat="server" Visible="true" />
                </div>
            </td>
        </tr>
        <tr>
            <td class="PageTabContentEdit">
                <asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
                <asp:HiddenField ID="hid_user_id" runat="server" Value="0" />
                <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" CssClass="multiPage">
                    <telerik:RadPageView ID="RadPageView1" runat="server">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td align="left">
                                    <div style="float: left; padding-left: 10px; padding-top: 0px; padding-bottom: 0px;">
                                        <div style="float: left; padding-top: 14px; font-weight: bold;">
                                            Select Language:</div>
                                        <div style="float: left;">
                                            <%--<telerik:RadAjaxPanel ID="RadAjaxPanel21" runat="server">--%>
                                            <asp:RadioButtonList ID="rdolst_language" runat="server" RepeatColumns="4" RepeatDirection="Horizontal"
                                                CellSpacing="0" CellPadding="10" AutoPostBack="true">
                                                <asp:ListItem Value="1">English</asp:ListItem>
                                                <asp:ListItem Value="2">French</asp:ListItem>
                                                <asp:ListItem Value="3">Spanish</asp:ListItem>
                                                <asp:ListItem Value="4">Chinese</asp:ListItem>
                                            </asp:RadioButtonList>
                                            <%-- </telerik:RadAjaxPanel>--%>
                                        </div>
                                    </div>
                                </td>
                                <td class="fixedcolumn" valign="top" align="center">
                                    <div id="div_save_pdf" runat="server">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdTabItem">
                                    <a name="1"></a>
                                    <asp:Panel ID="Panel1_Header1" runat="server" CssClass="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/basic_info.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            Basic Information
                                        </div>
                                        <asp:Image ID="pnl_img1" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                            CssClass="panelimage" />
                                    </asp:Panel>
                                    <ajax:CollapsiblePanelExtender ID="cpe1" BehaviorID="cpe1" runat="server" Enabled="True"
                                        TargetControlID="Panel1_Content1" CollapseControlID="Panel1_Header1" ExpandControlID="Panel1_Header1"
                                        Collapsed="true" ImageControlID="pnl_img1" CollapsedImage="/Images/down_Arrow.gif"
                                        ExpandedImage="/Images/up_Arrow.gif">
                                    </ajax:CollapsiblePanelExtender>
                                    <ajax:CollapsiblePanelExtender ID="cpe11" BehaviorID="cpe1" runat="server" Enabled="True"
                                        TargetControlID="Panel1_Content1button" CollapseControlID="Panel1_Header1" ExpandControlID="Panel1_Header1"
                                        Collapsed="true" ImageControlID="pnl_img1" CollapsedImage="/Images/down_Arrow.gif"
                                        ExpandedImage="/Images/up_Arrow.gif">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="Panel1_Content1" runat="server" CssClass="collapsePanel">
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                            CssClass="TabGrid">
                                            <table cellpadding="0" cellspacing="2" border="0" width="838">
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Label ID="lbl_msg_basic" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 145px;" class="caption">
                                                        Auction Title<span id="span_auction_title" runat="server" class="req_star"> *</span>
                                                        <asp:HiddenField ID="hid_basic_form_mode" runat="server" Value="view" />
                                                        <asp:HiddenField ID="hid_language" runat="server" Value="1" />
                                                    </td>
                                                    <td style="width: 270px;" class="details">
                                                        <asp:TextBox CssClass="inputtype" ID="txt_auction_title" MaxLength="100" runat="server" />
                                                        <asp:RequiredFieldValidator ID="rfv_auction_title" runat="server" ControlToValidate="txt_auction_title"
                                                            ValidationGroup="_basic" Display="Dynamic" ErrorMessage="<br>Title Required"
                                                            CssClass="error"></asp:RequiredFieldValidator>
                                                        <asp:Label ID="lbl_auction_title" runat="server" />&nbsp;
                                                    </td>
                                                    <td style="width: 145px;" class="caption">
                                                        Sub Title<span id="span_sub_title" runat="server" class="req_star"> *</span>
                                                    </td>
                                                    <td style="width: 270px;" class="details">
                                                        <asp:TextBox CssClass="inputtype" ID="txt_sub_title" runat="server" />
                                                        <asp:RequiredFieldValidator ID="rfv_sub_title" runat="server" ControlToValidate="txt_sub_title"
                                                            ValidationGroup="_basic" Display="Dynamic" ErrorMessage="<br>Sub Title Required"
                                                            CssClass="error"></asp:RequiredFieldValidator>
                                                        <asp:Label ID="lbl_sub_title" runat="server" />&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="caption">
                                                        Auction Code<span id="span_auction_code" runat="server" class="req_star"> *</span>
                                                    </td>
                                                    <td class="details">
                                                        <asp:TextBox CssClass="inputtype" ID="txt_auction_code" runat="server" />
                                                        <asp:RequiredFieldValidator ID="rfv_auction_code" runat="server" ControlToValidate="txt_auction_code"
                                                            ValidationGroup="_basic" Display="Dynamic" ErrorMessage="<br>Code Required" CssClass="error"></asp:RequiredFieldValidator>
                                                        <asp:Label ID="lbl_auction_code" runat="server" />&nbsp;
                                                    </td>
                                                    <td class="caption">
                                                        Product Category<span id="span_product_category" runat="server" class="req_star"> *</span>
                                                    </td>
                                                    <td class="details">
                                                        <telerik:RadComboBox ID="ddl_product_category" runat="server" Height="200px" Width="200px"
                                                            EmptyMessage="--Product Category--" MarkFirstMatch="true" EnableLoadOnDemand="true"
                                                            DataTextField="name" DataValueField="product_catetory_id" AllowCustomText="true">
                                                        </telerik:RadComboBox>
                                                        <asp:RequiredFieldValidator ID="rfv_product_category" runat="server" ControlToValidate="ddl_product_category"
                                                            ValidationGroup="_basic" Display="Dynamic" ErrorMessage="<br>Category Required"
                                                            CssClass="error"></asp:RequiredFieldValidator>
                                                        <asp:CustomValidator ID="CustomValidator3" runat="server" ClientValidationFunction="validateCombo"
                                                            ErrorMessage="<br />Please select product category from the list." ControlToValidate="ddl_product_category"
                                                            ValidationGroup="_basic" CssClass="error" Display="Dynamic">
                                                        </asp:CustomValidator>
                                                        <asp:Label ID="lbl_product_category" runat="server" />&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="caption">
                                                        Stock Condition
                                                    </td>
                                                    <td class="details">
                                                        <telerik:RadComboBox ID="ddl_stock_condition" runat="server" Height="200px" Width="200px"
                                                            EmptyMessage="--Stock Condition--" MarkFirstMatch="true" EnableLoadOnDemand="true"
                                                            DataTextField="name" DataValueField="stock_condition_id" AllowCustomText="true">
                                                        </telerik:RadComboBox>
                                                        <asp:CustomValidator ID="CustomValidator4" runat="server" ClientValidationFunction="validate_stock_condition"
                                                            ErrorMessage="<br />Please select an stock condition from the list." ControlToValidate="ddl_stock_condition"
                                                            ValidationGroup="_basic" CssClass="error" Display="Dynamic">
                                                        </asp:CustomValidator>
                                                        <asp:Label ID="lbl_stock_condition" runat="server" />&nbsp;
                                                    </td>
                                                    <td class="caption">
                                                        Stock Location
                                                    </td>
                                                    <td class="details">
                                                        <telerik:RadComboBox ID="ddl_stock_location" runat="server" Height="200px" Width="200px"
                                                            DropDownWidth="300px" EmptyMessage="--Stock Location--" HighlightTemplatedItems="true"
                                                            EnableLoadOnDemand="true" Filter="StartsWith" OnItemsRequested="ddl_stock_location_ItemsRequested"
                                                            AllowCustomText="true" DataValueField="stock_location_id">
                                                            <HeaderTemplate>
                                                                <table style="width: 290px;" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td style="width: 100px;">
                                                                            Name
                                                                        </td>
                                                                        <td style="width: 75px;">
                                                                            City
                                                                        </td>
                                                                        <td style="width: 85px;">
                                                                            State
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <table style="width: 290px;" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td style="width: 100px;">
                                                                            <%# DataBinder.Eval(Container.DataItem, "Name")%>
                                                                        </td>
                                                                        <td style="width: 75px;">
                                                                            <%# DataBinder.Eval(Container.DataItem, "city")%>
                                                                        </td>
                                                                        <td style="width: 85px;">
                                                                            <%# DataBinder.Eval(Container.DataItem, "state")%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </telerik:RadComboBox>
                                                        <asp:Label ID="lbl_stock_location" runat="server" />
                                                        <asp:CustomValidator ID="CustomValidator7" runat="server" ClientValidationFunction="validate_stock_location"
                                                            ErrorMessage="<br />Please select stock location from the list." ControlToValidate="ddl_stock_location"
                                                            ValidationGroup="_basic" CssClass="error" Display="Dynamic">
                                                        </asp:CustomValidator>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="caption">
                                                        Packaging
                                                    </td>
                                                    <td class="details">
                                                        <telerik:RadComboBox ID="ddl_packaging" runat="server" Height="200px" Width="200px"
                                                            EmptyMessage="--Packaging--" MarkFirstMatch="true" EnableLoadOnDemand="true"
                                                            DataTextField="name" DataValueField="packaging_id" AllowCustomText="true">
                                                        </telerik:RadComboBox>
                                                        <asp:CustomValidator ID="CustomValidator5" runat="server" ClientValidationFunction="validate_packaging"
                                                            ErrorMessage="<br />Please select an item from the list." ControlToValidate="ddl_packaging"
                                                            ValidationGroup="_basic" CssClass="error" Display="Dynamic">
                                                        </asp:CustomValidator>
                                                        <asp:Label ID="lbl_packaging" runat="server" />&nbsp;
                                                    </td>
                                                    <td class="caption">
                                                        Seller<span id="span_seller" runat="server" class="req_star"> *</span>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lbl_seller" runat="server" />
                                                        <telerik:RadComboBox ID="ddl_seller" runat="server" Height="200px" Width="200px"
                                                            DropDownWidth="300px" EmptyMessage="--Select Seller--" HighlightTemplatedItems="true"
                                                            EnableLoadOnDemand="true" Filter="StartsWith" OnItemsRequested="ddl_stock_location_ItemsRequested"
                                                            AllowCustomText="true" DataValueField="seller_id">
                                                            <HeaderTemplate>
                                                                <table style="width: 290px" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td style="width: 120px;">
                                                                            Company Name
                                                                        </td>
                                                                        <td style="width: 100px;">
                                                                            Contact Name
                                                                        </td>
                                                                        <td style="width: 120px;">
                                                                            Contact Title
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <table style="width: 290px" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td style="width: 120px;">
                                                                            <%# DataBinder.Eval(Container.DataItem, "company_name")%>
                                                                        </td>
                                                                        <td style="width: 100px;">
                                                                            <%# DataBinder.Eval(Container.DataItem, "contact_name")%>
                                                                        </td>
                                                                        <td style="width: 120px;">
                                                                            <%# DataBinder.Eval(Container.DataItem, "contact_title")%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </telerik:RadComboBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddl_seller"
                                                            ValidationGroup="_basic" Display="Dynamic" ErrorMessage="<br>Seller Required"
                                                            CssClass="error"></asp:RequiredFieldValidator>
                                                        <asp:CustomValidator ID="CustomValidator6" runat="server" ClientValidationFunction="validate_seller"
                                                            ErrorMessage="<br />Please select seller from the list." ControlToValidate="ddl_seller"
                                                            ValidationGroup="_basic" CssClass="error" Display="Dynamic">
                                                        </asp:CustomValidator>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="caption">
                                                        Is upcoming visible?
                                                    </td>
                                                    <td class="details">
                                                        <asp:CheckBox ID="chk_home_page" runat="server" />
                                                        <asp:Label ID="lbl_home_page" runat="server" />&nbsp;
                                                    </td>
                                                    <td class="caption">
                                                        View this auction to relevant bidders only
                                                    </td>
                                                    <td class="details">
                                                        <asp:CheckBox ID="chk_relevant_bidders" runat="server" />
                                                        <asp:Label ID="lbl_relevant_bidders" runat="server" />&nbsp;&nbsp;<a href="javascript:void(0);"
                                                            onmouseout="hide_tip_new();" onmouseover="tip_new('This will check and show this auction to invited bidders and bidding limit','250','white');"><img
                                                                src='/Images/fend/help.jpg' alt='' border='0'></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="caption">
                                                        Flag Choosen
                                                    </td>
                                                    <td class="details">
                                                        <asp:CheckBoxList ID="chklst_flag_name" runat="server" CellPadding="0" CellSpacing="0"
                                                            BorderWidth="0" Width="100%">
                                                        </asp:CheckBoxList>
                                                        <asp:Literal ID="ltrl_flag_name" runat="server"></asp:Literal>
                                                    </td>
                                                    <td class="caption">
                                                        Auction Image(s)
                                                    </td>
                                                    <td class="details">
                                                        <asp:Literal ID="lit_image_upload" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </telerik:RadAjaxPanel>
                                    </asp:Panel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <img src="/Images/spacer1.gif" width="120" height="1" alt="" /><br />
                                    <asp:Panel ID="Panel1_Content1button" runat="server" CssClass="collapsePanel">
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel11" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                                            <asp:Panel ID="Panel1_Content1button_perm" runat="server">
                                                <div class="addButton">
                                                    <asp:ImageButton ID="btn_basic_edit" runat="server" AlternateText="Save" ImageUrl="/images/edit.gif" />
                                                    <asp:ImageButton ID="btn_basic_save" ValidationGroup="_basic" runat="server" AlternateText="Save"
                                                        ImageUrl="/images/save.gif" />
                                                    <asp:ImageButton ID="btn_basic_update" OnClientClick="ValidationGroupEnable('_basic',true)"
                                                        ValidationGroup="_basic" runat="server" AlternateText="Update" ImageUrl="/images/update.gif" />
                                                </div>
                                                <div class="cancelButton" id="div_basic_cancel" runat="server">
                                                    <asp:ImageButton ID="btn_basic_cancel" runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                                </div>
                                                <div style="padding: 10px 0px 5px 15px;">
                                                    <asp:HiddenField ID="hid_auction_status" runat="server" Value="0" />
                                                    <asp:ImageButton ID="btn_auction_active" CausesValidation="false" runat="server" />
                                                </div>
                                                <div style="padding: 10px 0px 5px 15px;">
                                                    <asp:ImageButton ID="btn_auction_delete" ImageUrl="/images/delete.png" CausesValidation="false"
                                                        runat="server" />
                                                    <asp:Button ID="btn_refresh_basic" runat="server" Style="display: none;" Text="Refresh" />
                                                </div>
                                            </asp:Panel>
                                        </telerik:RadAjaxPanel>
                                    </asp:Panel>
                                    <%--<div class="cancelButton">
                                        <a href="javascript:void(0);" onclick="javascript:open_help('1');">
                                            <img src="/Images/help-button.gif" border="none" />
                                        </a>
                                    </div>--%>
                                </td>
                            </tr>
                            <asp:Panel ID="pnl_edit_auction" runat="server">
                                <tr>
                                    <td class="tdTabItem">
                                        <a name="8"></a>
                                        <asp:Panel ID="Panel1_Header8" runat="server" CssClass="pnlTabItemHeader">
                                            <div class="tabItemHeader" style="background: url(/Images/others_icon.gif) no-repeat;
                                                background-position: 10px 0px;">
                                                Other Details
                                            </div>
                                            <asp:Image ID="pnl_img8" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                                CssClass="panelimage" />
                                        </asp:Panel>
                                        <ajax:CollapsiblePanelExtender ID="cpe8" BehaviorID="cpe1" runat="server" Enabled="True"
                                            TargetControlID="Panel1_Content8" CollapseControlID="Panel1_Header8" ExpandControlID="Panel1_Header8"
                                            Collapsed="true" ImageControlID="pnl_img8" CollapsedImage="/Images/down_Arrow.gif"
                                            ExpandedImage="/Images/up_Arrow.gif">
                                        </ajax:CollapsiblePanelExtender>
                                        <%--<ajax:CollapsiblePanelExtender ID="cpe88" BehaviorID="cpe1" runat="server" Enabled="True"
                                            TargetControlID="Panel1_Content8button" CollapseControlID="Panel1_Header8" ExpandControlID="Panel1_Header8"
                                            Collapsed="true" ImageControlID="pnl_img8" CollapsedImage="/Images/down_Arrow.gif"
                                            ExpandedImage="/Images/up_Arrow.gif">
                                        </ajax:CollapsiblePanelExtender>--%>
                                        <asp:Panel ID="Panel1_Content8" runat="server" CssClass="collapsePanel">
                                            <div style="padding: 0px 10px 5px 10px;">
                                                <telerik:RadAjaxPanel ID="RadAjaxPanel8" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                    CssClass="TabGrid">
                                                    <asp:Button ID="but_bind_tab" runat="server" Enabled="true" BorderStyle="None" BackColor="Transparent"
                                                        ForeColor="Transparent" Width="0" Height="0" />
                                                    <telerik:RadTabStrip OnClientTabSelected="set_tab_index" runat="server" ID="TabStip1"
                                                        MultiPageID="Multipage1" SelectedIndex="0" Width="100%">
                                                        <Tabs>
                                                            <telerik:RadTab runat="server" Text="Condition" PageViewID="PageView2">
                                                            </telerik:RadTab>
                                                            <telerik:RadTab runat="server" Text="Description" PageViewID="PageView1">
                                                            </telerik:RadTab>
                                                            <telerik:RadTab runat="server" Text="Product Details" PageViewID="PageView7">
                                                            </telerik:RadTab>
                                                        </Tabs>
                                                    </telerik:RadTabStrip>
                                                    <telerik:RadMultiPage runat="server" ID="Multipage1" SelectedIndex="0" RenderSelectedPageOnly="false"
                                                        CssClass="rad_multipage_detail">
                                                        <telerik:RadPageView runat="server" ID="PageView2">
                                                            <div class="editLink" id="div_edit_short_description" runat="server">
                                                                <a href='javascript:void(0);' onclick="return open_pop_win('h','e');">
                                                                    <img src="/images/edit_icon.png" alt="" border="0" /></a>
                                                            </div>
                                                            <asp:Panel ID="pnl_other2" runat="server" ScrollBars="Vertical" CssClass="pnlGray">
                                                                <asp:Literal ID="lbl_short_description" runat="server"></asp:Literal>
                                                            </asp:Panel>
                                                        </telerik:RadPageView>
                                                        <telerik:RadPageView runat="server" ID="PageView1">
                                                            <div class="editLink" id="div_edit_description" runat="server">
                                                                <a href="javascript:void(0);" onclick="return open_pop_win('d','e');">
                                                                    <img src="/images/edit_icon.png" alt="" border="0" /></a>
                                                            </div>
                                                            <asp:Panel ID="pnl_other1" runat="server" ScrollBars="Vertical" CssClass="pnlGray">
                                                                <asp:Literal ID="lbl_description" runat="server"></asp:Literal>
                                                            </asp:Panel>
                                                        </telerik:RadPageView>
                                                        <telerik:RadPageView runat="server" ID="PageView7">
                                                            <div style="float: left; padding-top: 6px;">
                                                                <asp:Label ID="lbl_attach_product_details" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="editLink" id="div_edit_prod_detail" runat="server">
                                                                <a href='javascript:void(0);' onclick="return open_pop_win('r','e');">
                                                                    <img src="/images/edit_icon.png" alt="" border="0" /></a>
                                                            </div>
                                                            <asp:Panel ID="pnl_other7" runat="server" ScrollBars="Vertical" CssClass="pnlGray">
                                                                <div>
                                                                    <asp:Literal ID="lbl_product_details" runat="server"></asp:Literal>
                                                                </div>
                                                            </asp:Panel>
                                                        </telerik:RadPageView>
                                                    </telerik:RadMultiPage>
                                                </telerik:RadAjaxPanel>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                    <td class="fixedcolumn" valign="top">
                                        <img src="/Images/spacer1.gif" width="120" height="1" alt="" /><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdTabItem">
                                        <a name="10"></a>
                                        <asp:Panel ID="Panel1_Header10" runat="server" CssClass="pnlTabItemHeader">
                                            <div class="tabItemHeader" style="background: url(/Images/terms-conditions-icon.png) no-repeat;
                                                background-position: 10px 0px;">
                                                Terms and Conditions
                                            </div>
                                            <asp:Image ID="pnl_img10" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                                CssClass="panelimage" />
                                        </asp:Panel>
                                        <ajax:CollapsiblePanelExtender ID="cpe10" BehaviorID="cpe10" runat="server" Enabled="True"
                                            TargetControlID="Panel1_Content10" CollapseControlID="Panel1_Header10" ExpandControlID="Panel1_Header10"
                                            Collapsed="true" ImageControlID="pnl_img10" CollapsedImage="/Images/down_Arrow.gif"
                                            ExpandedImage="/Images/up_Arrow.gif">
                                        </ajax:CollapsiblePanelExtender>
                                        <asp:Panel ID="Panel1_Content10" runat="server" CssClass="collapsePanel">
                                            <div style="padding: 0px 10px 5px 10px;">
                                                <telerik:RadAjaxPanel ID="RadAjaxPanel5" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                    CssClass="TabGrid">
                                                    <asp:Button ID="but_bind_tab_terms" runat="server" Enabled="true" BorderStyle="None"
                                                        BackColor="Transparent" ForeColor="Transparent" Width="0" Height="0" />
                                                    <telerik:RadTabStrip OnClientTabSelected="set_tab_index_terms" runat="server" ID="TabStip2"
                                                        MultiPageID="RadMultiPage2" SelectedIndex="0">
                                                        <Tabs>
                                                            <telerik:RadTab runat="server" Text="Payment Terms" PageViewID="PageView3">
                                                            </telerik:RadTab>
                                                            <telerik:RadTab runat="server" Text="Shipping Terms" PageViewID="PageView4">
                                                            </telerik:RadTab>
                                                            <telerik:RadTab runat="server" Text="Other Terms" PageViewID="PageView5">
                                                            </telerik:RadTab>
                                                            <telerik:RadTab runat="server" Text="Special Condition" PageViewID="PageView6">
                                                            </telerik:RadTab>
                                                            <telerik:RadTab runat="server" Text="Tax Details" PageViewID="PageView8">
                                                            </telerik:RadTab>
                                                        </Tabs>
                                                    </telerik:RadTabStrip>
                                                    <telerik:RadMultiPage runat="server" ID="RadMultiPage2" SelectedIndex="0" RenderSelectedPageOnly="false"
                                                        CssClass="rad_multipage_detail">
                                                        <telerik:RadPageView runat="server" ID="PageView3">
                                                            <div style="float: left; padding-top: 6px;">
                                                                <asp:Label ID="lbl_attach_payment_terms" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="editLink" id="div_edit_payment_term" runat="server">
                                                                <a href='javascript:void(0);' onclick="return open_pop_win('p','e');">
                                                                    <img src="/images/edit_icon.png" alt="" border="0" /></a>
                                                            </div>
                                                            <asp:Panel ID="pnl_other3" runat="server" ScrollBars="Vertical" CssClass="pnlGray">
                                                                <div>
                                                                    <asp:Literal ID="lbl_payment_terms" runat="server"></asp:Literal>
                                                                </div>
                                                            </asp:Panel>
                                                        </telerik:RadPageView>
                                                        <telerik:RadPageView runat="server" ID="PageView4">
                                                            <div style="float: left; padding-top: 6px;">
                                                                <asp:Label ID="lbl_attach_shipping_terms" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="editLink" id="div_edit_shipping_term" runat="server">
                                                                <a href='javascript:void(0);' onclick="return open_pop_win('s','e');">
                                                                    <img src="/images/edit_icon.png" alt="" border="0" /></a>
                                                            </div>
                                                            <asp:Panel ID="pnl_other4" runat="server" ScrollBars="Vertical" CssClass="pnlGray">
                                                                <div>
                                                                    <asp:Literal ID="lbl_shipping_terms" runat="server"></asp:Literal>
                                                                </div>
                                                            </asp:Panel>
                                                        </telerik:RadPageView>
                                                        <telerik:RadPageView runat="server" ID="PageView5">
                                                            <div style="float: left; padding-top: 6px;">
                                                                <asp:Label ID="lbl_attach_other_terms_and_conditions" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="editLink" id="div_edit_other_term" runat="server">
                                                                <a href='javascript:void(0);' onclick="return open_pop_win('o','e');">
                                                                    <img src="/images/edit_icon.png" alt="" border="0" /></a>
                                                            </div>
                                                            <asp:Panel ID="pnl_other5" runat="server" ScrollBars="Vertical" CssClass="pnlGray">
                                                                <div>
                                                                    <asp:Literal ID="lbl_other_terms_and_conditions" runat="server"></asp:Literal>
                                                                </div>
                                                            </asp:Panel>
                                                        </telerik:RadPageView>
                                                        <telerik:RadPageView runat="server" ID="PageView6">
                                                            <div style="float: left; padding-top: 6px;">
                                                                <asp:Label ID="lbl_attach_special_conditions" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="editLink" id="div_edit_special_condition" runat="server">
                                                                <a href='javascript:void(0);' onclick="return open_pop_win('c','e');">
                                                                    <img src="/images/edit_icon.png" alt="" border="0" /></a>
                                                            </div>
                                                            <asp:Panel ID="pnl_other6" runat="server" ScrollBars="Vertical" CssClass="pnlGray">
                                                                <div>
                                                                    <asp:Literal ID="lbl_special_condition" runat="server"></asp:Literal>
                                                                </div>
                                                            </asp:Panel>
                                                        </telerik:RadPageView>
                                                        <telerik:RadPageView runat="server" ID="PageView8">
                                                            <div style="float: left; padding-top: 6px;">
                                                                <asp:Label ID="lbl_attach_tax_details" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="editLink" id="div_edit_tax_details" runat="server">
                                                                <a href='javascript:void(0);' onclick="return open_pop_win('t','e');">
                                                                    <img src="/images/edit_icon.png" alt="" border="0" /></a>
                                                            </div>
                                                            <asp:Panel ID="pnl_other8" runat="server" ScrollBars="Vertical" CssClass="pnlGray">
                                                                <div>
                                                                    <asp:Literal ID="lbl_tax_details" runat="server"></asp:Literal>
                                                                </div>
                                                            </asp:Panel>
                                                        </telerik:RadPageView>
                                                    </telerik:RadMultiPage>
                                                </telerik:RadAjaxPanel>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                    <td class="fixedcolumn" valign="top">
                                        <img src="/Images/spacer1.gif" width="120" height="1" alt="" /><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdTabItem">
                                        <a name="2"></a>
                                        <asp:Panel ID="Panel1_Header2" runat="server" CssClass="pnlTabItemHeader">
                                            <div class="tabItemHeader" style="background: url(/Images/items.gif) no-repeat; background-position: 10px 0px;">
                                                Items
                                            </div>
                                            <asp:Image ID="pnl_img2" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                                CssClass="panelimage" />
                                        </asp:Panel>
                                        <ajax:CollapsiblePanelExtender ID="cpe2" BehaviorID="cpe2" runat="server" Enabled="True"
                                            TargetControlID="Panel1_Content2" CollapseControlID="Panel1_Header2" ExpandControlID="Panel1_Header2"
                                            Collapsed="true" ImageControlID="pnl_img2" CollapsedImage="/Images/down_Arrow.gif"
                                            ExpandedImage="/Images/up_Arrow.gif">
                                        </ajax:CollapsiblePanelExtender>
                                        <asp:Panel ID="Panel1_Content2" runat="server" CssClass="collapsePanel">
                                            <%-- <telerik:RadAjaxPanel ID="RadAjaxPanel20" LoadingPanelID="RadAjaxLoadingPanel1" runat="server">--%>
                                            <div class="TabGrid">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td style="font-size: 11px; font-weight: bold; padding-left: 15px;">
                                                            &nbsp;Attach Item List
                                                        </td>
                                                        <td class="dv_md_sub_heading" style="padding-right: 10px; text-align: right;" id="div_assign_bidders">
                                                            <asp:Literal runat="server" ID="lit_for_product_item"></asp:Literal>
                                                            <a href='javascript:void(0);' onclick="return open_pop_items('m','e');" id="a_attach_item"
                                                                style="text-decoration: none; cursor: pointer;">
                                                                <img src="/images/attached_items.gif" alt="Attached Items" style="border: none;" /></a>
                                                            <a href="DownloadProductSheet.aspx" onclick="return open_pop_download();">
                                                                <img src="/images/downloadtemplate.gif" border="0" /></a> <a href="Upload_Product.aspx"
                                                                    onclick="return open_pop_upload();">
                                                                    <img src="/images/uploaditms.png" border="0" /></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" class="sub_details">
                                                            <uc1:AuctionItems ID="AuctionItems1" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <%--</telerik:RadAjaxPanel>--%>
                                        </asp:Panel>
                                    </td>
                                    <td class="fixedcolumn" valign="top">
                                        <%-- <div class="cancelButton">
                                            <a href="javascript:void(0);" onclick="javascript:open_help('8');">
                                                <img src="/Images/help-button.gif" border="none" />
                                            </a>
                                        </div>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdTabItem">
                                        <a name="3"></a>
                                        <asp:Panel ID="Panel1_Header3" runat="server" CssClass="pnlTabItemHeader">
                                            <div class="tabItemHeader" style="background: url(/Images/attachment.gif) no-repeat;
                                                background-position: 10px 0px;">
                                                Attachments
                                            </div>
                                            <asp:Image ID="pnl_img3" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                                CssClass="panelimage" />
                                        </asp:Panel>
                                        <ajax:CollapsiblePanelExtender ID="cpe3" BehaviorID="cpe3" runat="server" Enabled="True"
                                            TargetControlID="Panel1_Content3" CollapseControlID="Panel1_Header3" ExpandControlID="Panel1_Header3"
                                            Collapsed="true" ImageControlID="pnl_img3" CollapsedImage="/Images/down_Arrow.gif"
                                            ExpandedImage="/Images/up_Arrow.gif">
                                        </ajax:CollapsiblePanelExtender>
                                        <asp:Panel ID="Panel1_Content3" runat="server" CssClass="collapsePanel">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel3" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                ClientEvents-OnRequestStart="conditionalPostback" CssClass="TabGrid">
                                                <telerik:RadProgressManager ID="RadProgressManager1" runat="server" Skin="Simple" />
                                                <telerik:RadProgressArea ID="RadProgressArea1" runat="server" Skin="Simple" />
                                                <div style="padding: 10px; text-align: left;">
                                                    <telerik:RadGrid ID="RadGrid_Attachment" runat="server" Skin="Vista" GridLines="None"
                                                        AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                                        PageSize="10" OnNeedDataSource="RadGrid_Attachment_NeedDataSource" OnDeleteCommand="RadGrid_Attachment_DeleteCommand"
                                                        OnInsertCommand="RadGrid_Attachment_InsertCommand" OnUpdateCommand="RadGrid_Attachment_UpdateCommand"
                                                        enableajax="true">
                                                        <PagerStyle Mode="NextPrevAndNumeric" />
                                                        <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="attachment_id"
                                                            HorizontalAlign="NotSet" AutoGenerateColumns="False" SkinID="Vista">
                                                            <NoRecordsTemplate>
                                                                Attachments not available
                                                            </NoRecordsTemplate>
                                                            <SortExpressions>
                                                                <telerik:GridSortExpression FieldName="title" SortOrder="Ascending" />
                                                            </SortExpressions>
                                                            <CommandItemSettings AddNewRecordText="Add New Attachment" />
                                                            <Columns>
                                                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                    EditImageUrl="/Images/edit_grid.gif">
                                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                                                </telerik:GridEditCommandColumn>
                                                                <telerik:GridBoundColumn DataField="attachment_id" HeaderText="attachment_id" UniqueName="attachment_id"
                                                                    ReadOnly="True" Visible="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="title" HeaderText="Title" UniqueName="Title"
                                                                    SortExpression="title">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridTemplateColumn HeaderText="File Name" SortExpression="filename" UniqueName="filename"
                                                                    EditFormHeaderTextFormat="Attachment">
                                                                    <ItemTemplate>
                                                                        <a href="/Upload/Auctions/auction_attachments/<%= Request.QueryString("i") %>/<%#Eval("attachment_id") %>/<%#Eval("filename") %>"
                                                                            target="_blank">
                                                                            <asp:Label runat="server" ID="lblFile" Text='<%# Eval("filename") %>'></asp:Label></a>
                                                                    </ItemTemplate>
                                                                    <%-- <EditItemTemplate>
                                                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                                                    </EditItemTemplate>--%>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Visible To" SortExpression="for_bidder" UniqueName="for_bidder">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" ID="lblfor" Text='<%# iif(Eval("for_bidder"),"For Bidders","For Users") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%-- <EditItemTemplate>
                                                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                                                    </EditItemTemplate>--%>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"
                                                                    ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                                                    CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                                                </telerik:GridButtonColumn>
                                                            </Columns>
                                                            <%--<EditFormSettings ColumnNumber="2" CaptionDataField="title" CaptionFormatString="Edit properties of Attachment {0}"
                                                                InsertCaption="New Attachment">
                                                                <FormTableItemStyle Wrap="False"></FormTableItemStyle>
                                                                <FormCaptionStyle CssClass="EditFormHeader" Font-Bold="true"></FormCaptionStyle>
                                                                <FormMainTableStyle GridLines="None" CellSpacing="0" CellPadding="3" BackColor="White"
                                                                    Width="100%" />
                                                                <FormTableStyle CellSpacing="0" CellPadding="2" BackColor="White" />
                                                                <FormTableAlternatingItemStyle Wrap="False"></FormTableAlternatingItemStyle>
                                                                <EditColumn ButtonType="ImageButton" InsertText="Insert Order" UpdateText="Update record"
                                                                    UniqueName="EditCommandColumn1" CancelText="Cancel edit" CancelImageUrl="/images/cancel.gif"
                                                                    InsertImageUrl="/images/save.gif" UpdateImageUrl="/images/update.gif">
                                                                </EditColumn>
                                                                <FormTableButtonRowStyle HorizontalAlign="Right" CssClass="EditFormButtonRow"></FormTableButtonRowStyle>
                                                            </EditFormSettings>--%>
                                                            <EditFormSettings EditFormType="Template" EditColumn-ItemStyle-Width="100%" EditColumn-HeaderStyle-HorizontalAlign="Left"
                                                                EditColumn-ItemStyle-HorizontalAlign="Left">
                                                                <FormTemplate>
                                                                    <table cellpadding="0" cellspacing="2" border="0" width="950px">
                                                                        <tr>
                                                                            <td style="font-weight: bold; padding-top: 10px;" colspan="8">
                                                                                Attachment Detail
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="caption" style="width: 75px;">
                                                                                Title
                                                                            </td>
                                                                            <td class="details" style="width: 125px;">
                                                                                <asp:TextBox ID="txt_attach_title" runat="server" Text='<%# Bind("title") %>'></asp:TextBox>
                                                                            </td>
                                                                            <td class="caption" style="width: 80px;">
                                                                                Attachment
                                                                            </td>
                                                                            <td class="details" style="width: 110px;">
                                                                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                                                            </td>
                                                                            <td class="caption" style="width: 110px;">
                                                                                Visible To
                                                                            </td>
                                                                            <td class="details" style="width: 99px;">
                                                                                <telerik:RadComboBox ID="drop_visibleto" Width="95px" runat="server" SelectedValue='<%# Bind("for_bidder") %>'>
                                                                                    <Items>
                                                                                        <telerik:RadComboBoxItem Value="False" Text="For Users" />
                                                                                        <telerik:RadComboBoxItem Value="True" Text="For Bidders" />
                                                                                    </Items>
                                                                                </telerik:RadComboBox>
                                                                            </td>
                                                                            <td valign="middle" style="width: 120px;">
                                                                                <asp:ImageButton ID="img_btn_save_comment" runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif", "/images/update.gif") %>'
                                                                                    CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "PerformInsert", "Update")%>'
                                                                                    OnClientClick="conditionalPostback" />
                                                                            </td>
                                                                            <td valign="middle" style="width: 110px;">
                                                                                <asp:ImageButton ID="img_btn_cancel" runat="server" ImageUrl="/images/cancel.gif"
                                                                                    CausesValidation="false" CommandName="cancel" />
                                                                                <%-- <asp:Button ID="btnUpdate" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'>
                            </asp:Button>--%>&nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </FormTemplate>
                                                            </EditFormSettings>
                                                        </MasterTableView>
                                                    </telerik:RadGrid>
                                                    <telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditor7" runat="server" TextBoxStyle-Width="150px" />
                                                </div>
                                            </telerik:RadAjaxPanel>
                                        </asp:Panel>
                                    </td>
                                    <td class="fixedcolumn" valign="top">
                                        <%--<div class="cancelButton">
                                            <a href="javascript:void(0);" onclick="javascript:open_help('9');">
                                                <img src="/Images/help-button.gif" border="none" />
                                            </a>
                                        </div>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdTabItem">
                                        <a name="4"></a>
                                        <asp:Panel ID="Panel1_Header4" runat="server" CssClass="pnlTabItemHeader">
                                            <div class="tabItemHeader" style="background: url(/Images/biding_details.gif) no-repeat;
                                                background-position: 10px 0px;">
                                                Bidding Details
                                            </div>
                                            <asp:Image ID="pnl_img4" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                                CssClass="panelimage" />
                                        </asp:Panel>
                                        <ajax:CollapsiblePanelExtender ID="cpe4" BehaviorID="cpe4" runat="server" Enabled="True"
                                            TargetControlID="Panel1_Content4" CollapseControlID="Panel1_Header4" ExpandControlID="Panel1_Header4"
                                            Collapsed="true" ImageControlID="pnl_img4" CollapsedImage="/Images/down_Arrow.gif"
                                            ExpandedImage="/Images/up_Arrow.gif">
                                        </ajax:CollapsiblePanelExtender>
                                        <ajax:CollapsiblePanelExtender ID="cpe44" BehaviorID="cpe4" runat="server" Enabled="True"
                                            TargetControlID="Panel1_Content4button" CollapseControlID="Panel1_Header4" ExpandControlID="Panel1_Header4"
                                            Collapsed="true" ImageControlID="pnl_img4" CollapsedImage="/Images/down_Arrow.gif"
                                            ExpandedImage="/Images/up_Arrow.gif">
                                        </ajax:CollapsiblePanelExtender>
                                        <asp:Panel ID="Panel1_Content4" runat="server" CssClass="collapsePanel">
                                            <div style="padding: 0px 10px 5px 10px;">
                                                <telerik:RadAjaxPanel ID="RadAjaxPanel4" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                    CssClass="TabGrid">
                                                    <telerik:RadTabStrip runat="server" ID="RadTabStrip3" MultiPageID="RadMultiPage3"
                                                        SelectedIndex="0" AutoPostBack="true" OnTabClick="RadTabStrip3_TabClick" CausesValidation="false">
                                                        <Tabs>
                                                            <telerik:RadTab runat="server" Text="Auction Details" PageViewID="PageView9">
                                                            </telerik:RadTab>
                                                            <telerik:RadTab runat="server" Text="Schedule Auction" PageViewID="PageView10">
                                                            </telerik:RadTab>
                                                        </Tabs>
                                                    </telerik:RadTabStrip>
                                                    <telerik:RadMultiPage runat="server" ID="RadMultiPage3" SelectedIndex="0" RenderSelectedPageOnly="false"
                                                        CssClass="rad_multipage_detail">
                                                        <telerik:RadPageView runat="server" ID="PageView9">
                                                            <asp:Label ID="lbl_auction_type_desc" runat="server" Font-Bold="true" Font-Size="10"
                                                                Text="Proxy Auction"></asp:Label><br />
                                                            <div style="width: 838px; text-align: justify; line-height: 17px;" id="div_auction_type"
                                                                runat="server">
                                                                The bidder will be able to set the maximum amount he is willing to pay and the system
                                                                will bid on the bidder’s behalf. Other bidders will not know his maximum amount.
                                                                The system will place bids on his behalf using the automatic bid increment amount
                                                                in order to surpass the current high bid. The system will bid only as much as is
                                                                necessary to make sure he remains the high bidder or to meet the reserve price,
                                                                up to his maximum amount. If another bidder places the same maximum bid or higher,
                                                                the system will notify the bidder so he can place another bid.
                                                            </div>
                                                            <table cellpadding="0" cellspacing="0" border="0" width="838">
                                                                <tr>
                                                                    <td style="width: 145px;" class="caption">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 270px;" class="caption">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 145px;" class="caption">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 270px;" class="caption">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr id="tr_auction_type" runat="server">
                                                                    <td colspan="4">
                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                            <tr>
                                                                                <td class="caption_bid_detail">
                                                                                    Auction Type:
                                                                                </td>
                                                                                <td class="caption_bid_detail" colspan="3">
                                                                                    <asp:HiddenField ID="HID_auction_category_id" runat="server" Value="0" />
                                                                                    <asp:HiddenField ID="HID_auction_type_id" runat="server" Value="0" />
                                                                                    <asp:Label ID="lbl_auction_type" runat="server" />
                                                                                    <asp:DropDownList ID="ddl_auction_type" runat="server" DataTextField="name" DataValueField="auction_type_id"
                                                                                        RepeatDirection="Horizontal" AutoPostBack="true" Width="203">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                                <asp:Panel ID="pnl_details1" runat="server">
                                                                    <tr>
                                                                        <td colspan="4">
                                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                                            <tr>
                                                                                                <td class="caption_bid_detail">
                                                                                                    QTY Available:
                                                                                                </td>
                                                                                                <td class="caption_bid_detail">
                                                                                                    <asp:Label ID="lbl_total_qty" runat="server" />
                                                                                                    <asp:TextBox CssClass="inputtype" ID="txt_total_qty" runat="server" MaxLength="4"
                                                                                                        Width="80" />
                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txt_total_qty"
                                                                                                        ValidationExpression="[0-9]*" EnableClientScript="false" ErrorMessage="Invalid"
                                                                                                        runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                </td>
                                                                                                <td class="caption_bid_detail">
                                                                                                    Max Allowed:
                                                                                                </td>
                                                                                                <td class="caption_bid_detail">
                                                                                                    <asp:Label ID="lbl_max_allowed" runat="server" />
                                                                                                    <asp:TextBox CssClass="inputtype" ID="txt_max_allowed" runat="server" MaxLength="2"
                                                                                                        Width="80" />
                                                                                                    <asp:RegularExpressionValidator ID="REV_max_allowed" ControlToValidate="txt_max_allowed"
                                                                                                        ValidationExpression="[0-9]*" EnableClientScript="false" ErrorMessage="Invalid"
                                                                                                        runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                </td>
                                                                                                <td class="caption_bid_detail">
                                                                                                    lots per customer
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                                            <tr>
                                                                                                <td class="caption_bid_detail">
                                                                                                    Show Price:
                                                                                                </td>
                                                                                                <td class="caption_bid_detail">
                                                                                                    <asp:Label ID="lbl_show_price" runat="server" />
                                                                                                    <telerik:RadNumericTextBox ID="txt_show_price" runat="server" Type="Currency" Width="80">
                                                                                                    </telerik:RadNumericTextBox>
                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txt_show_price"
                                                                                                        ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid"
                                                                                                        runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div style="background-color: lightgray; margin-top: 20px; padding: 5px 0px 5px 0px;">
                                                                                            <table cellspacing="0" cellpadding="0" border="0">
                                                                                                <tr>
                                                                                                    <td class="caption_bid_detail" width="415">
                                                                                                        <asp:Label ID="lbl_is_private_offer" runat="server" />
                                                                                                        <asp:RadioButton GroupName="now_offer" ID="chk_is_private_offer" runat="server" Text="&nbspWe will accept private offer for this auction with auto acceptance at"
                                                                                                            TextAlign="Right" />
                                                                                                    </td>
                                                                                                    <td style="padding-bottom: 3px;" valign="bottom">
                                                                                                        <asp:Label ID="lbl_auto_acceptance_price_private" runat="server" />
                                                                                                        <telerik:RadNumericTextBox ID="txt_auto_acceptance_price_private" runat="server"
                                                                                                            Type="Currency" Width="80">
                                                                                                        </telerik:RadNumericTextBox>
                                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txt_auto_acceptance_price_private"
                                                                                                            ErrorMessage="Invalid" ValidationExpression="[-+]?[0-9]*\.?[0-9]*" ValidationGroup="_BiddingDetails"
                                                                                                            Display="Dynamic"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <div style="background-color: lightgray; padding-left: 0px; margin-top: 1px; margin-right: 20px;">
                                                                                                <table cellspacing="0" cellpadding="0" border="0">
                                                                                                    <tr>
                                                                                                        <td class="caption_bid_detail" width="415">
                                                                                                            <asp:Label ID="lbl_is_reject_private_offer" runat="server" />
                                                                                                            <asp:Label GroupName="now_offer" ID="lbl_reject_private" runat="server" Text="&nbspWe will reject private offer for this auction with auto rejection at"
                                                                                                                TextAlign="Right" Style="padding-left: 23px;" />
                                                                                                        </td>
                                                                                                        <td style="padding-bottom: 3px;" valign="bottom">
                                                                                                            <asp:Label ID="lbl_auto_reject_price_private" runat="server" />
                                                                                                            <telerik:RadNumericTextBox ID="txt_auto_reject_price_private" runat="server" Type="Currency"
                                                                                                                Width="80">
                                                                                                            </telerik:RadNumericTextBox>
                                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator23" runat="server"
                                                                                                                ControlToValidate="txt_auto_reject_price_private" ErrorMessage="Invalid" ValidationExpression="[-+]?[0-9]*\.?[0-9]*"
                                                                                                                ValidationGroup="_BiddingDetails" Display="Dynamic"></asp:RegularExpressionValidator>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                            <hr style="width: 100%; border-style: dotted; border: #000 1px dotted;" />
                                                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="caption_bid_detail" width="40%">
                                                                                                        <asp:Label ID="lbl_is_partial_offer" runat="server" />
                                                                                                        <asp:RadioButton GroupName="now_offer" ID="chk_is_partial_offer" runat="server" Text="&nbspWe will accept partial offer with auto acceptance of"
                                                                                                            TextAlign="Right" />
                                                                                                    </td>
                                                                                                    <td style="padding-bottom: 3px;" valign="bottom">
                                                                                                        <asp:Label ID="lbl_auto_acceptance_price_partial" runat="server" />
                                                                                                        <telerik:RadNumericTextBox ID="txt_auto_acceptance_price_partial" runat="server"
                                                                                                            Type="Currency" Width="50">
                                                                                                        </telerik:RadNumericTextBox>
                                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txt_auto_acceptance_price_partial"
                                                                                                            ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid "
                                                                                                            runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                        atleast&nbsp;
                                                                                                        <asp:Label ID="lbl_auto_acceptance_qty" runat="server" />
                                                                                                        <asp:TextBox CssClass="inputtype" ID="txt_auto_acceptance_qty" runat="server" MaxLength="3"
                                                                                                            Width="50" />
                                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ControlToValidate="txt_auto_acceptance_qty"
                                                                                                            ValidationExpression="[0-9]*" EnableClientScript="false" ErrorMessage="Invalid "
                                                                                                            runat="server" ValidationGroup="_BiddingDetails" />quantity.
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <div style="background-color: lightgray; padding: 0px 0px 5px 0px; margin-left: 0px;
                                                                                                margin-top: 2px;">
                                                                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td class="caption_bid_detail" width="40%">
                                                                                                            <asp:Label ID="lbl_is_reject_partial_offer" runat="server" />
                                                                                                            <asp:Label GroupName="now_offer" ID="chk_is_reject_partial_offer" runat="server"
                                                                                                                Text="We will reject partial offer with auto rejection of" TextAlign="Right"
                                                                                                                Style="padding-left: 25px" />
                                                                                                        </td>
                                                                                                        <td style="padding-bottom: 3px;" valign="bottom">
                                                                                                            <asp:Label ID="lbl_auto_rejection_price_partial" runat="server" />
                                                                                                            <telerik:RadNumericTextBox ID="txt_auto_rejection_price_partial" runat="server" Type="Currency"
                                                                                                                Width="50">
                                                                                                            </telerik:RadNumericTextBox>
                                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator24" ControlToValidate="txt_auto_rejection_price_partial"
                                                                                                                ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid "
                                                                                                                runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                            atleast&nbsp;
                                                                                                            <asp:Label ID="lbl_auto_rejection_qty" runat="server" />
                                                                                                            <asp:TextBox CssClass="inputtype" ID="txt_auto_rejection_qty" runat="server" MaxLength="3"
                                                                                                                Width="50" />
                                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator25" ControlToValidate="txt_auto_rejection_qty"
                                                                                                                ValidationExpression="[0-9]*" EnableClientScript="false" ErrorMessage="Invalid "
                                                                                                                runat="server" ValidationGroup="_BiddingDetails" />quantity.
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                            <%-- <hr style=" width:100%; border-style:dotted;border:#000 1px dotted; " />--%>
                                                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="caption_bid_detail" width="40%">
                                                                                                        <asp:RadioButton GroupName="now_offer" ID="chk_is_none" runat="server" Text="None"
                                                                                                            TextAlign="Right" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnl_details2" runat="server">
                                                                    <tr>
                                                                        <td colspan="4">
                                                                            <table cellspacing="0" cellpadding="0" border="0" width="70%">
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                                            <tr>
                                                                                                <td class="caption_bid_detail">
                                                                                                    Bid Start at:
                                                                                                </td>
                                                                                                <td class="caption_bid_detail">
                                                                                                    <asp:Label ID="lbl_start_price_traditional" runat="server" />
                                                                                                    <telerik:RadNumericTextBox ID="txt_start_price_traditional" CssClass="inputtype"
                                                                                                        runat="server" Type="Currency" Width="80">
                                                                                                    </telerik:RadNumericTextBox>
                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ControlToValidate="txt_start_price_traditional"
                                                                                                        ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid"
                                                                                                        runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                                            <tr>
                                                                                                <td class="caption_bid_detail">
                                                                                                    Reserve Price:
                                                                                                </td>
                                                                                                <td class="caption_bid_detail">
                                                                                                    <asp:Label ID="lbl_reserve_price_traditional" runat="server" />
                                                                                                    <telerik:RadNumericTextBox ID="txt_reserve_price_traditional" CssClass="inputtype"
                                                                                                        runat="server" Type="Currency" Width="80">
                                                                                                    </telerik:RadNumericTextBox>
                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ControlToValidate="txt_reserve_price_traditional"
                                                                                                        ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid"
                                                                                                        runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                                            <tr>
                                                                                                <td class="caption_bid_detail">
                                                                                                    Increments:
                                                                                                </td>
                                                                                                <td class="caption_bid_detail">
                                                                                                    <asp:Label ID="lbl_increament_amount_traditional" runat="server" />
                                                                                                    <telerik:RadNumericTextBox ID="txt_increament_amount_traditional" CssClass="inputtype"
                                                                                                        runat="server" Type="Currency" Width="80">
                                                                                                    </telerik:RadNumericTextBox>
                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" ControlToValidate="txt_increament_amount_traditional"
                                                                                                        ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid"
                                                                                                        runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                                            <tr>
                                                                                                <td class="caption_bid_detail">
                                                                                                    Threshold:
                                                                                                </td>
                                                                                                <td class="caption_bid_detail">
                                                                                                    <asp:Label ID="lbl_thresh_hold_traditional" runat="server" />
                                                                                                    <telerik:RadNumericTextBox ID="txt_thresh_hold_traditional" CssClass="inputtype"
                                                                                                        runat="server" Type="Currency" Width="80">
                                                                                                    </telerik:RadNumericTextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" class="caption_bid_detail">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" class="caption_bid_detail">
                                                                                        <asp:Label ID="lbl_is_show_reserve_price_traditional" runat="server" />
                                                                                        <asp:CheckBox ID="chk_is_show_reserve_price_traditional" runat="server" Text="Reserve price will be shown to the bidders. The system will not allow the lot to be sold below this price."
                                                                                            TextAlign="Right" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" class="caption_bid_detail">
                                                                                        <asp:Label ID="lbl_is_show_increment_amount" runat="server" />
                                                                                        <asp:CheckBox ID="chk_is_show_increment_amount" runat="server" Text="Increment Amount will be shown to the bidders."
                                                                                            TextAlign="Right" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" class="caption_bid_detail">
                                                                                        <asp:Label ID="lbl_is_show_rank" runat="server" />
                                                                                        <asp:CheckBox ID="chk_is_show_rank" runat="server" Text="Rank will be shown to the bidders."
                                                                                            TextAlign="Right" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" class="caption_bid_detail">
                                                                                        <asp:Label ID="lbl_is_show_no_of_bids_traditional" runat="server" />
                                                                                        <asp:CheckBox ID="chk_is_show_no_of_bids_traditional" runat="server" Text="Number of bids on the auction will be shown to the bidders."
                                                                                            TextAlign="Right" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" class="caption_bid_detail">
                                                                                        <asp:Label ID="lbl_is_show_actual_pricing_traditional" runat="server" />
                                                                                        <asp:CheckBox ID="chk_is_show_actual_pricing_traditional" runat="server" Text="Bid amount will be shown to bidders."
                                                                                            TextAlign="Right" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" class="caption_bid_detail">
                                                                                        &nbsp;
                                                                                        <%--<asp:Label ID="lbl_is_accept_pre_bid_traditional" runat="server" />
                                                                                        <asp:CheckBox ID="chk_is_accept_pre_bid_traditional" runat="server" Text="We will accept any bid before start date."
                                                                                            TextAlign="Right" />--%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <div style="background-color: lightgray; margin-top: 20px; padding: 5px 0px 5px 0px;">
                                                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="caption_bid_detail" width="73%">
                                                                                                        <asp:Label ID="lbl_is_private_offer_traditional" runat="server" />
                                                                                                        <asp:RadioButton GroupName="traditional_offer" ID="chk_is_private_offer_traditional"
                                                                                                            runat="server" Text="We will accept private offer for this auction with auto acceptance at"
                                                                                                            TextAlign="Right" />
                                                                                                    </td>
                                                                                                    <td style="padding-bottom: 3px;" valign="bottom">
                                                                                                        <asp:Label ID="lbl_auto_acceptance_price_private_traditional" runat="server" />
                                                                                                        <telerik:RadNumericTextBox ID="txt_auto_acceptance_price_private_traditional" CssClass="inputtype"
                                                                                                            runat="server" Type="Currency" Width="50">
                                                                                                        </telerik:RadNumericTextBox>
                                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ControlToValidate="txt_auto_acceptance_price_private_traditional"
                                                                                                            ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid "
                                                                                                            runat="server" ValidationGroup="_BiddingDetails" Display="Dynamic" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="caption_bid_detail" width="56%">
                                                                                                        <asp:Label ID="lbl_is_partial_offer_traditional" runat="server" />
                                                                                                        <asp:RadioButton GroupName="traditional_offer" ID="chk_is_partial_offer_traditional"
                                                                                                            runat="server" Text="We will accept partial offer with auto acceptance of" TextAlign="Right" />
                                                                                                    </td>
                                                                                                    <td style="padding-bottom: 3px;" valign="bottom">
                                                                                                        <asp:Label ID="lbl_auto_acceptance_price_partial_traditional" runat="server" />
                                                                                                        <telerik:RadNumericTextBox ID="txt_auto_acceptance_price_partial_traditional" CssClass="inputtype"
                                                                                                            runat="server" Type="Currency" Width="50">
                                                                                                        </telerik:RadNumericTextBox>
                                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" ControlToValidate="txt_auto_acceptance_price_partial_traditional"
                                                                                                            ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid "
                                                                                                            runat="server" ValidationGroup="_BiddingDetails" />&nbsp;atleast&nbsp;
                                                                                                        <asp:Label ID="lbl_auto_acceptance_qty_traditional" runat="server" />
                                                                                                        <asp:TextBox CssClass="inputtype" ID="txt_auto_acceptance_qty_traditional" Width="50"
                                                                                                            runat="server" />
                                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator11" ControlToValidate="txt_auto_acceptance_qty_traditional"
                                                                                                            ValidationExpression="[0-9]*" EnableClientScript="false" ErrorMessage="Invalid "
                                                                                                            runat="server" ValidationGroup="_BiddingDetails" Display="Dynamic" />quantity.
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="caption_bid_detail" width="45%">
                                                                                                        <asp:Label ID="lbl_is_buy_it_now_traditional" runat="server" />
                                                                                                        <asp:RadioButton GroupName="traditional_offer" ID="chk_is_buy_it_now_traditional"
                                                                                                            runat="server" Text="Bidders will be able to buy this lot for" TextAlign="Right" />
                                                                                                    </td>
                                                                                                    <td style="padding-bottom: 3px;" valign="bottom">
                                                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                                                            <tr>
                                                                                                                <td style="white-space: nowrap;">
                                                                                                                    <asp:Label ID="lbl_auto_acceptance_price_now_traditional" runat="server" />
                                                                                                                    <telerik:RadNumericTextBox ID="txt_auto_acceptance_price_now_traditional" CssClass="inputtype"
                                                                                                                        runat="server" Type="Currency" Width="50">
                                                                                                                    </telerik:RadNumericTextBox>
                                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator20" ControlToValidate="txt_auto_acceptance_price_now_traditional"
                                                                                                                        ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid"
                                                                                                                        runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                                </td>
                                                                                                                <td style="width: 100px;">
                                                                                                                    QTY Available:
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:Label ID="lbl_total_qty_traditional" runat="server" />
                                                                                                                    <asp:TextBox CssClass="inputtype" ID="txt_total_qty_traditional" runat="server" Width="50" />
                                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" ControlToValidate="txt_total_qty_traditional"
                                                                                                                        ValidationExpression="[0-9]*" EnableClientScript="false" ErrorMessage="Invalid"
                                                                                                                        runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                                </td>
                                                                                                                <td style="width: 80px;">
                                                                                                                    Max Allowed:
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:Label ID="lbl_max_allowed_traditional" runat="server" />
                                                                                                                    <asp:TextBox CssClass="inputtype" ID="txt_max_allowed_traditional" runat="server"
                                                                                                                        Width="50" />
                                                                                                                    <asp:RegularExpressionValidator ID="REValidator12" ControlToValidate="txt_max_allowed_traditional"
                                                                                                                        ValidationExpression="[0-9]*" EnableClientScript="false" ErrorMessage="Invalid"
                                                                                                                        runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    lots per customer
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="caption_bid_detail" width="40%">
                                                                                                        <asp:RadioButton GroupName="traditional_offer" ID="chk_is_none_traditional" runat="server"
                                                                                                            Text="None" TextAlign="Right" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnl_details3" runat="server">
                                                                    <tr>
                                                                        <td colspan="4">
                                                                            <table cellspacing="0" cellpadding="0" border="0">
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                                            <tr>
                                                                                                <td class="caption_bid_detail">
                                                                                                    Bid Start at:
                                                                                                </td>
                                                                                                <td class="caption_bid_detail">
                                                                                                    <asp:Label ID="lbl_start_price_proxy" runat="server" />
                                                                                                    <telerik:RadNumericTextBox ID="txt_start_price_proxy" CssClass="inputtype" runat="server"
                                                                                                        Type="Currency" Width="80">
                                                                                                    </telerik:RadNumericTextBox>
                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator13" ControlToValidate="txt_start_price_proxy"
                                                                                                        ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid"
                                                                                                        runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                                            <tr>
                                                                                                <td class="caption_bid_detail">
                                                                                                    Reserve Price:
                                                                                                </td>
                                                                                                <td class="caption_bid_detail">
                                                                                                    <asp:Label ID="lbl_reserve_price_proxy" runat="server" />
                                                                                                    <telerik:RadNumericTextBox ID="txt_reserve_price_proxy" CssClass="inputtype" runat="server"
                                                                                                        Type="Currency" Width="80">
                                                                                                    </telerik:RadNumericTextBox>
                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator14" ControlToValidate="txt_reserve_price_proxy"
                                                                                                        ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid"
                                                                                                        runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                                            <tr>
                                                                                                <td class="caption_bid_detail">
                                                                                                    Increments:
                                                                                                </td>
                                                                                                <td class="caption_bid_detail">
                                                                                                    <asp:Label ID="lbl_increament_amount_proxy" runat="server" />
                                                                                                    <telerik:RadNumericTextBox ID="txt_increament_amount_proxy" CssClass="inputtype"
                                                                                                        runat="server" Type="Currency" Width="80">
                                                                                                    </telerik:RadNumericTextBox>
                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator15" ControlToValidate="txt_increament_amount_proxy"
                                                                                                        ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid"
                                                                                                        runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                                            <tr>
                                                                                                <td class="caption_bid_detail">
                                                                                                    Threshold:
                                                                                                </td>
                                                                                                <td class="caption_bid_detail">
                                                                                                    <asp:Label ID="lbl_thresh_hold_proxy" runat="server" />
                                                                                                    <telerik:RadNumericTextBox ID="txt_thresh_hold_proxy" CssClass="inputtype" runat="server"
                                                                                                        Type="Currency" Width="80">
                                                                                                    </telerik:RadNumericTextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4" class="caption_bid_detail">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="caption_bid_detail">
                                                                            <asp:Label ID="lbl_proxy_bid_allow" runat="server" />
                                                                            <asp:CheckBox ID="chk_proxy_bid_allow" runat="server" Text="Proxy Bid allow" TextAlign="Right"
                                                                                AutoPostBack="true" />
                                                                        </td>
                                                                        <td colspan="2">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="caption_bid_detail">
                                                                            <asp:Label ID="lbl_live_bid_allow" runat="server" />
                                                                            <asp:CheckBox ID="chk_live_bid_allow" runat="server" Text="Live Bid allow" TextAlign="Right"
                                                                                AutoPostBack="true" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4" class="caption_bid_detail">
                                                                            <asp:Label ID="lbl_is_show_reserve_price_proxy" runat="server" />
                                                                            <asp:CheckBox ID="chk_is_show_reserve_price_proxy" runat="server" Text="Reserve price will be shown to the bidders. The system will not allow the lot to be sold below this price."
                                                                                TextAlign="Right" /><sup style="color: Green; font-weight: bold;">(2)</sup>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="caption_bid_detail">
                                                                            <asp:Label ID="lbl_is_show_increment_amount_proxy" runat="server" />
                                                                            <asp:CheckBox ID="chk_is_show_increment_amount_proxy" runat="server" Text="Increment Amount will be shown to the bidders. "
                                                                                TextAlign="Right" /><sup style="color: Green; font-weight: bold;">(3)</sup>
                                                                        </td>
                                                                        <td colspan="2" rowspan="5" align="right">
                                                                            <img src="/Images/explain_image1.jpg" alt="" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="caption_bid_detail">
                                                                            <asp:Label ID="lbl_is_show_rank_proxy" runat="server" />
                                                                            <asp:CheckBox ID="chk_is_show_rank_proxy" runat="server" Text="Rank will be shown to the bidders."
                                                                                TextAlign="Right" /><sup style="color: Green; font-weight: bold;">(6)</sup>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="caption_bid_detail" style="white-space: nowrap;">
                                                                            <asp:Label ID="lbl_is_show_no_of_bids_proxy" runat="server" />
                                                                            <asp:CheckBox ID="chk_is_show_no_of_bids_proxy" runat="server" Text="Number of bids on the auction will be shown to the bidders."
                                                                                TextAlign="Right" /><sup style="color: Green; font-weight: bold;">(4)</sup>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="caption_bid_detail">
                                                                            <asp:Label ID="lbl_is_show_actual_pricing_proxy" runat="server" />
                                                                            <asp:CheckBox ID="chk_is_show_actual_pricing_proxy" runat="server" Text="Bid amount will be shown to bidders."
                                                                                TextAlign="Right" /><sup style="color: Green; font-weight: bold;">(1, 5)</sup>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="caption_bid_detail">
                                                                            &nbsp;
                                                                            <%-- <asp:Label ID="lbl_is_accept_pre_bid_proxy" runat="server" />
                                                                            <asp:CheckBox ID="chk_is_accept_pre_bid_proxy" runat="server" Text="We will accept any bid before start date."
                                                                                TextAlign="Right" />--%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4">
                                                                            <div style="background-color: lightgray; margin-top: 20px; padding: 5px 0px 5px 0px;">
                                                                                <table cellspacing="0" cellpadding="0" border="0">
                                                                                    <tr>
                                                                                        <td class="caption_bid_detail" width="415">
                                                                                            <asp:Label ID="lbl_is_private_offer_proxy" runat="server" />
                                                                                            <asp:RadioButton GroupName="proxy_offer" ID="chk_is_private_offer_proxy" runat="server"
                                                                                                Text="We will accept private offer for this auction with auto acceptance at"
                                                                                                TextAlign="Right" />
                                                                                        </td>
                                                                                        <td style="padding-bottom: 3px;" valign="bottom">
                                                                                            <asp:Label ID="lbl_auto_acceptance_price_private_proxy" runat="server" />
                                                                                            <telerik:RadNumericTextBox ID="txt_auto_acceptance_price_private_proxy" CssClass="inputtype"
                                                                                                runat="server" Type="Currency" Width="80">
                                                                                            </telerik:RadNumericTextBox>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator16" ControlToValidate="txt_auto_acceptance_price_private_proxy"
                                                                                                ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid "
                                                                                                runat="server" ValidationGroup="_BiddingDetails" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <div style="background-color: lightgray; padding-left: 0px; margin-top: 1px; margin-right: 20px;">
                                                                                    <table cellspacing="0" cellpadding="0" border="0">
                                                                                        <tr>
                                                                                            <td class="caption_bid_detail" width="415">
                                                                                                <asp:Label ID="lbl_is_reject_private_offer_proxy" runat="server" Style="padding-left: 23px;" />
                                                                                                <asp:Label GroupName="proxy_offer" ID="chk_is_reject_private_offer_proxy" runat="server"
                                                                                                    Text="&nbspWe will reject private offer for this auction with auto rejection at"
                                                                                                    TextAlign="Right" Style="padding-left: 18px;" />
                                                                                            </td>
                                                                                            <td style="padding-bottom: 3px;" valign="bottom">
                                                                                                <asp:Label ID="lbl_auto_rejection_price_private_proxy" runat="server" />
                                                                                                <telerik:RadNumericTextBox ID="txt_auto_rejection_price_private_proxy" runat="server"
                                                                                                    Type="Currency" Width="80">
                                                                                                </telerik:RadNumericTextBox>
                                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator26" runat="server"
                                                                                                    ControlToValidate="txt_auto_rejection_price_private_proxy" ErrorMessage="Invalid"
                                                                                                    ValidationExpression="[-+]?[0-9]*\.?[0-9]*" ValidationGroup="_BiddingDetails"
                                                                                                    Display="Dynamic"></asp:RegularExpressionValidator>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <hr style="width: 100%; border-style: dotted; border: #000 1px dotted;" />
                                                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="caption_bid_detail" width="40%">
                                                                                            <asp:Label ID="lbl_is_partial_offer_proxy" runat="server" />
                                                                                            <asp:RadioButton GroupName="proxy_offer" ID="chk_is_partial_offer_proxy" runat="server"
                                                                                                Text="We will accept partial offer with auto acceptance of" TextAlign="Right" />
                                                                                        </td>
                                                                                        <td style="padding-bottom: 3px;" valign="bottom">
                                                                                            <asp:Label ID="lbl_auto_acceptance_price_partial_proxy" runat="server" />
                                                                                            <telerik:RadNumericTextBox ID="txt_auto_acceptance_price_partial_proxy" CssClass="inputtype"
                                                                                                runat="server" Type="Currency" Width="80">
                                                                                            </telerik:RadNumericTextBox>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator17" ControlToValidate="txt_auto_acceptance_price_partial_proxy"
                                                                                                ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid "
                                                                                                runat="server" ValidationGroup="_BiddingDetails" />
                                                                                            atleast&nbsp;
                                                                                            <asp:Label ID="lbl_auto_acceptance_qty_proxy" runat="server" />
                                                                                            <asp:TextBox CssClass="inputtype" ID="txt_auto_acceptance_qty_proxy" runat="server"
                                                                                                Width="50" />
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator18" ControlToValidate="txt_auto_acceptance_qty_proxy"
                                                                                                ValidationExpression="[0-9]*" EnableClientScript="false" ErrorMessage="Invalid "
                                                                                                runat="server" ValidationGroup="_BiddingDetails" />
                                                                                            quantity.
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <div style="background-color: lightgray; padding: 0px 0px 5px 0px; margin-left: 0px;
                                                                                    margin-top: 2px;">
                                                                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="caption_bid_detail" width="40%">
                                                                                                <asp:Label ID="lbl_is_reject_partial_offer_proxy" runat="server" Style="padding-left: 18px" />
                                                                                                <asp:Label GroupName="proxy_offer" ID="chk_is_reject_partial_offer_proxy" runat="server"
                                                                                                    Text="We will reject partial offer with auto rejection of" TextAlign="Right"
                                                                                                    Style="padding-left: 23px" />
                                                                                            </td>
                                                                                            <td style="padding-bottom: 3px;" valign="bottom">
                                                                                                <asp:Label ID="lbl_auto_rejection_price_partial_proxy" runat="server" />
                                                                                                <telerik:RadNumericTextBox ID="txt_auto_rejection_price_partial_proxy" CssClass="inputtype"
                                                                                                    runat="server" Type="Currency" Width="80">
                                                                                                </telerik:RadNumericTextBox>
                                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator27" ControlToValidate="txt_auto_rejection_price_partial_proxy"
                                                                                                    ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid "
                                                                                                    runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                atleast&nbsp;
                                                                                                <asp:Label ID="lbl_auto_rejection_qty_proxy" runat="server" />
                                                                                                <asp:TextBox CssClass="inputtype" ID="txt_auto_rejection_qty_proxy" runat="server"
                                                                                                    Width="50" />
                                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator28" ControlToValidate="txt_auto_rejection_qty_proxy"
                                                                                                    ValidationExpression="[0-9]*" EnableClientScript="false" ErrorMessage="Invalid "
                                                                                                    runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                quantity.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <table cellspacing="0" cellpadding="0" border="0">
                                                                                    <tr>
                                                                                        <td class="caption_bid_detail">
                                                                                            <asp:Label ID="lbl_is_buy_it_now_proxy" runat="server" />
                                                                                            <asp:RadioButton GroupName="proxy_offer" ID="chk_is_buy_it_now_proxy" runat="server"
                                                                                                Text="Bidders will be able to buy this lot for " TextAlign="Right" />
                                                                                        </td>
                                                                                        <td style="padding-bottom: 3px;" valign="bottom">
                                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td style="white-space: nowrap;">
                                                                                                        <asp:Label ID="lbl_auto_acceptance_price_now_proxy" runat="server" />
                                                                                                        <telerik:RadNumericTextBox ID="txt_auto_acceptance_price_now_proxy" CssClass="inputtype"
                                                                                                            runat="server" Type="Currency" Width="50">
                                                                                                        </telerik:RadNumericTextBox>
                                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator21" ControlToValidate="txt_auto_acceptance_price_now_proxy"
                                                                                                            ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="false" ErrorMessage="Invalid "
                                                                                                            runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                    </td>
                                                                                                    <td style="width: 100px;">
                                                                                                        QTY Available:
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lbl_total_qty_proxy" runat="server" />
                                                                                                        <asp:TextBox CssClass="inputtype" ID="txt_total_qty_proxy" runat="server" Width="50" />
                                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator19" ControlToValidate="txt_total_qty_proxy"
                                                                                                            ValidationExpression="[0-9]*" EnableClientScript="false" ErrorMessage="Invalid"
                                                                                                            runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                    </td>
                                                                                                    <td style="width: 80px;">
                                                                                                        Max Allowed:
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lbl_max_allowed_proxy" runat="server" />
                                                                                                        <asp:TextBox CssClass="inputtype" ID="txt_max_allowed_proxy" runat="server" Width="50" />
                                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator22" ControlToValidate="txt_max_allowed_proxy"
                                                                                                            ValidationExpression="[0-9]*" EnableClientScript="false" ErrorMessage="Invalid"
                                                                                                            runat="server" ValidationGroup="_BiddingDetails" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        lots per customer
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="caption_bid_detail" width="40%">
                                                                                            <asp:RadioButton GroupName="proxy_offer" ID="chk_is_none_proxy" runat="server" Text="None"
                                                                                                TextAlign="Right" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnl_details4" runat="server">
                                                                    <tr>
                                                                        <td colspan="4" class="caption_bid_detail">
                                                                            <table cellspacing="0" cellpadding="0" border="0">
                                                                                <tr>
                                                                                    <td class="caption_bid_detail">
                                                                                        Message
                                                                                    </td>
                                                                                    <td style="padding-left: 30px;">
                                                                                        <asp:Label ID="lbl_request_for_quote" runat="server"></asp:Label>
                                                                                        <asp:TextBox CssClass="inputtype" ID="txt_request_for_quote" runat="server" TextMode="MultiLine"
                                                                                            Rows="5" Columns="75" Height="40" Width="200"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </asp:Panel>
                                                            </table>
                                                        </telerik:RadPageView>
                                                        <telerik:RadPageView runat="server" ID="PageView10">
                                                            <table cellpadding="0" cellspacing="2" border="0" width="838">
                                                                <tr>
                                                                    <td class="caption">
                                                                        Start Date
                                                                    </td>
                                                                    <td class="details">
                                                                        <asp:Label ID="lblStartDate" runat="server"></asp:Label>
                                                                        <telerik:RadDateTimePicker ID="dtStartDate" runat="server" TimeView-Columns="5" TimeView-RenderDirection="Vertical"
                                                                            TimeView-Interval="0:15:0" Skin="Simple" DateInput-Enabled="False">
                                                                            <DateInput ID="DateInput2" runat="server">
                                                                                <ClientEvents OnLoad="onLoaddtStartDate" />
                                                                            </DateInput>
                                                                            <Calendar ID="Calendar1" runat="server" EnableKeyboardNavigation="true">
                                                                            </Calendar>
                                                                        </telerik:RadDateTimePicker>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="dtStartDate"
                                                                            Text="Required" runat="server" ValidationGroup="_BiddingDetailsSchedule" />
                                                                    </td>
                                                                    <td class="details">
                                                                        <asp:Label ID="lbl_display_start_date" runat="server" />
                                                                        <asp:CheckBox ID="chk_is_display_start_date" Text="Show with auction" runat="server"
                                                                            TextAlign="Right" />
                                                                    </td>
                                                                    <td class="caption">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="caption">
                                                                        End Date
                                                                    </td>
                                                                    <td class="details">
                                                                        <asp:Label ID="lblEndDate" runat="server"></asp:Label>
                                                                        <telerik:RadDateTimePicker ID="dtEndDate" runat="server" Skin="Simple" TimeView-RenderDirection="Vertical"
                                                                            TimeView-Columns="5" TimeView-Interval="0:15:0" DateInput-Enabled="False">
                                                                            <DateInput ID="DateInput1" runat="server">
                                                                                <ClientEvents OnLoad="onLoaddtEndDate" />
                                                                            </DateInput>
                                                                            <Calendar ID="Calendar2" runat="server" EnableKeyboardNavigation="true">
                                                                            </Calendar>
                                                                        </telerik:RadDateTimePicker>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="dtEndDate"
                                                                            Text="Required" runat="server" ValidationGroup="_BiddingDetailsSchedule" />
                                                                        <asp:CustomValidator ID="CustomValidator1" ErrorMessage="<br />To date must be later than from date."
                                                                            EnableClientScript="true" runat="server" ValidationGroup="_BiddingDetailsSchedule"
                                                                            ControlToValidate="dtEndDate" ClientValidationFunction="validate"></asp:CustomValidator>
                                                                    </td>
                                                                    <td class="details">
                                                                        <asp:Label ID="lbl_display_end_date" runat="server" />
                                                                        <asp:CheckBox ID="chk_is_display_end_date" Text="Show with auction" runat="server"
                                                                            TextAlign="Right" />
                                                                    </td>
                                                                    <td class="caption">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr id="tr_bid_time_increse" runat="server">
                                                                    <td class="caption">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td class="details" colspan="2">
                                                                        If bid placed
                                                                        <asp:Label ID="lbl_before_time" runat="server"></asp:Label>
                                                                        <asp:TextBox CssClass="TextBox" ID="txt_before_time" runat="server" Width="20"></asp:TextBox>
                                                                        minutes before close time, increase bid time for
                                                                        <asp:Label ID="lbl_after_time" runat="server"></asp:Label>
                                                                        <asp:TextBox CssClass="TextBox" ID="txt_after_time" runat="server" Width="20"></asp:TextBox>
                                                                        minutes
                                                                        <asp:CompareValidator ID="comp_valid_incre_time" ControlToValidate="txt_after_time"
                                                                            ControlToCompare="txt_before_time" Type="Integer" Operator="GreaterThan" ErrorMessage="<br>Increase bid time always greater than bid place time"
                                                                            runat="server" ValidationGroup="_BiddingDetailsSchedule" />
                                                                    </td>
                                                                    <td class="caption">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4">
                                                                        <b>Email Schedule Sent status</b><br />
                                                                        <br />
                                                                        <uc3:AuctionEmailScheduleStatus ID="AuctionEmailScheduleStatus1" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </telerik:RadPageView>
                                                    </telerik:RadMultiPage>
                                                </telerik:RadAjaxPanel>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                    <td class="fixedcolumn" valign="top">
                                        <br />
                                        <asp:Panel ID="Panel1_Content4button" runat="server" CssClass="collapsePanel">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel44" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                                                <asp:Panel ID="Panel1_Content4button_per" runat="server">
                                                    <div class="addButton">
                                                        <asp:ImageButton ID="btnEditBiddingDetails" runat="server" AlternateText="Edit" ImageUrl="/images/edit.gif" />
                                                        <asp:ImageButton ID="btnUpdateBiddingDetails" ValidationGroup="_BiddingDetails" runat="server"
                                                            AlternateText="Update" ImageUrl="/images/update.gif" />
                                                    </div>
                                                    <div class="cancelButton" id="divCancelBiddingDetails" runat="server">
                                                        <asp:ImageButton ID="btnCancelBiddingDetails" runat="server" AlternateText="Cancel"
                                                            ImageUrl="/images/cancel.gif" />
                                                    </div>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel1_Content4button_per_schedule_tab" runat="server">
                                                    <div class="addButton">
                                                        <asp:ImageButton ID="btnEditBiddingDetailsSchedule" runat="server" AlternateText="Edit Schedule"
                                                            ImageUrl="/images/edit.gif" />
                                                        <asp:ImageButton ID="btnUpdateBiddingDetailsSchedule" ValidationGroup="_BiddingDetailsSchedule"
                                                            runat="server" AlternateText="Update Schedule" ImageUrl="/images/update.gif" />
                                                    </div>
                                                    <div class="cancelButton" id="divCancelBiddingDetailsSchedule" runat="server">
                                                        <asp:ImageButton ID="btnCancelBiddingDetailsSchedule" runat="server" AlternateText="Cancel Schedule"
                                                            ImageUrl="/images/cancel.gif" />
                                                    </div>
                                                </asp:Panel>
                                            </telerik:RadAjaxPanel>
                                        </asp:Panel>
                                        <%--<div class="cancelButton">
                                            <a href="javascript:void(0);" onclick="javascript:open_help('10');">
                                                <img src="/Images/help-button.gif" border="none" />
                                            </a>
                                        </div>--%>
                                    </td>
                                </tr>
                            </asp:Panel>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView2" runat="server" CssClass="pageViewEducation">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td class="tdTabItem">
                                    <div class="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/invetions.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            Invitations
                                        </div>
                                    </div>
                                    <div style="padding: 10px;">
                                        <%-- <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                            CssClass="TabGrid">--%>
                                        <uc:AuctionInvitation ID="UC_AuctionInvitation" runat="server" />
                                        <%--</telerik:RadAjaxPanel>--%>
                                    </div>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView3" runat="server">
                        <ucf:AuctionFedex ID="AuctionFedex1" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView4" runat="server">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td class="tdTabItem">
                                    <div class="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/after_lounch.gif) no-repeat;
                                            background-position: 10px 0px; width: 185px; float: left;">
                                            After Launch
                                        </div>
                                        <div style="float: right; width: 150px; padding-top: 5px;">
                                            <a id="a_refresh_afterlaunch" name="a_refresh_afterlaunch" title="Refresh" onclick="refresh_afterlaunch(0);"
                                                href="javascript:void(0);">
                                                <img alt="Refresh" src="/images/refresh.png" /></a>
                                        </div>
                                        <div id="dv_after_launch_attach" runat="server" style="float: right; padding: 5px 10px 5px 0px;
                                            width: 106px; height: 30px; display: block;">
                                            <a href='javascript:void(0);' onclick="return open_pop_items('l','e');" style="text-decoration: none;
                                                cursor: pointer;">
                                                <img src="/images/attach_file.gif" alt="Attach File" style="border: none;" /></a>
                                        </div>
                                        <div style="float: right; padding: 10px 10px 0px 0px;">
                                            <asp:Literal runat="server" ID="lit_for_attach_afterlaunch"></asp:Literal>
                                        </div>
                                    </div>
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel6" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                        CssClass="TabGrid">
                                        <asp:Button ID="but_bind_afterlaunch" runat="server" BorderStyle="None" BackColor="Transparent"
                                            ForeColor="Transparent" Width="0" Height="0" />
                                        <table cellpadding="0" cellspacing="2" border="0" width="838">
                                            <tr>
                                                <td style="width: 145px;" class="caption">
                                                    Additional Comments
                                                    <asp:TextBox ID="txt_test" runat="server" Visible="false"></asp:TextBox>
                                                </td>
                                                <td style="width: 675px;" class="details">
                                                    <asp:Label ID="lbl_after_launch" runat="server"></asp:Label>
                                                    <asp:Panel ID="pnl_after_launch_editor" runat="server">
                                                        <telerik:RadEditor ID="RadEditor2" runat="server" Skin="Sitefinity" Width="675">
                                                            <Content>
             
                                                            </Content>
                                                        </telerik:RadEditor>
                                                    </asp:Panel>
                                                    <asp:RequiredFieldValidator ID="rfv_additionalMessage" runat="server" ControlToValidate="RadEditor2"
                                                        ValidationGroup="after_launch" ErrorMessage="Message Required" CssClass="error"
                                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <div style="padding: 10px; display: none;">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="aucQryHeading">
                                                        Bidder Queries
                                                        <div style="float: right; color: Blue; padding-right: 20px;">
                                                            <span>Sort By:
                                                                <asp:DropDownList ID="ddlSortBy" runat="server" AutoPostBack="true">
                                                                    <asp:ListItem Value="dateA">Date ASC</asp:ListItem>
                                                                    <asp:ListItem Value="dateD">Date DESC</asp:ListItem>
                                                                    <asp:ListItem Value="sales_rep">Sales Rep</asp:ListItem>
                                                                    <asp:ListItem Value="customer">Customer</asp:ListItem>
                                                                </asp:DropDownList>
                                                                &nbsp;<span style="color: white;">------</span> </span>
                                                            <asp:LinkButton ID="lnk_all" runat="server" Text="Show All" />
                                                            |
                                                            <asp:LinkButton ID="lnk_pending" runat="server" Text="Pending" />
                                                            |
                                                            <asp:LinkButton ID="lnk_ans" runat="server" Text="Answered" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 10px 5px 5px 20px; text-align: left;">
                                                        <asp:Label ID="lbl_no_record" runat="server" Text="Currently bidder query is not available"></asp:Label>
                                                        <asp:Repeater ID="rep_after_queries" runat="server">
                                                            <ItemTemplate>
                                                                <div class="qryTitle">
                                                                    <%# Container.ItemIndex + 1 & ". " & Eval("title")%><%# IIf(Eval("is_active"), "", " <font color='red'>(Deleted)</font>")%>
                                                                </div>
                                                                <div class="qryAlt" style="padding-left: 16px;">
                                                                    <a onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');">
                                                                        <%# Eval("company_name")%></a><%#  ", " & Eval("state") & ", " & Eval("submit_date")%>
                                                                </div>
                                                                <div style="padding: 20px;">
                                                                    <asp:Repeater ID="rep_inner_after_queries" runat="server">
                                                                        <ItemTemplate>
                                                                            <div class="qryDesc">
                                                                                <img src='<%# IIF(Eval("image_path")<>"","/upload/users/" & Eval("user_id") & "/" & Eval("image_path"),"/images/buyer_icon.gif") %>'
                                                                                    alt="" style="float: left; padding: 2px; height: 23px; width: 22px;" />
                                                                                <%#Eval("message") %>
                                                                            </div>
                                                                            <div class="qryAlt">
                                                                                <%# IIf(Eval("buyer_title") = "", Eval("title"), Eval("buyer_title"))%>
                                                                                <%# IIf(Eval("first_name") = "", Eval("buyer_first_name") & " " & Eval("buyer_last_name"), Eval("first_name") & " " & Eval("last_name"))%>,
                                                                                <%# Eval("submit_date")%>
                                                                            </div>
                                                                            <br />
                                                                            <br />
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                    <br />
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tbl_reply" runat="server"
                                                                        visible='<%# Eval("is_active")%>'>
                                                                        <tr>
                                                                            <td style="width: 74%;">
                                                                                <asp:TextBox runat="server" TextMode="MultiLine" onfocus="Focus(this.id,'Response');"
                                                                                    onblur="Blur(this.id,'Response');" CssClass="WaterMarkedTextBox" Text="Response"
                                                                                    ID="rep_txt_message"></asp:TextBox>
                                                                                <asp:HiddenField ID="rep_hid_buyer" Value='<%#Eval("buyer_id") %>' runat="server" />
                                                                            </td>
                                                                            <td valign="bottom" style="padding-left: 15px;">
                                                                                <asp:ImageButton ID="rep_but_send" runat="server" CommandName="send_query" CommandArgument='<%#Eval("query_id") %>'
                                                                                    ImageUrl="/images/send.gif" AlternateText="Send" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <br />
                                                                    <a href="javascript:void(0);" onclick="return openallquery(<%#Eval("buyer_id") %>)">
                                                                        <img border="0" src="/Images/view_all.gif" alt="View all query" /></a>&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton
                                                                            ImageUrl="/Images/markeasanswered.png" ID="imgBtnClose" runat="server" AlternateText="Mark as Answered"
                                                                            CommandArgument='<%# Eval("query_id")%>' OnClientClick="return confirm('Are you sure to mark as answered without reply?');"
                                                                            CommandName="mark" Visible='<%# IIf(CommonCode.is_admin_user() And CBool(Eval("admin_marked"))=False And Eval("answered_count") = 0, True, False)%>' />
                                                                    <br />
                                                                    <br />
                                                                    <div style="background-color: #FAFAFA; padding: 5px;" id="tbl_note" runat="server"
                                                                        visible='<%# Eval("is_active")%>'>
                                                                        <font color="black" size="2"><b>INTERNAL ACCOUNT NOTES</b></font><br />
                                                                        <br />
                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td valign="bottom" style="padding-left: 15px;">
                                                                                    <asp:Repeater ID="rptr_note" runat="server">
                                                                                        <ItemTemplate>
                                                                                            <div style='padding: 5px 0px; <%#IIf(container.itemindex<>0,"border-top:2px solid white;","") %>'>
                                                                                                <b>
                                                                                                    <%# Eval("full_name")%></b>-<%# Eval("message")%></div>
                                                                                        </ItemTemplate>
                                                                                    </asp:Repeater>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="padding-left: 15px;">
                                                                                    <asp:Panel ID="pnl_note" runat="server" DefaultButton="rep_note_send">
                                                                                        <asp:TextBox runat="server" CssClass="noteBox" Text="Enter Note" ID="txt_note"></asp:TextBox>
                                                                                        <ajax:TextBoxWatermarkExtender ID="wtm_order" runat="server" TargetControlID="txt_note"
                                                                                            WatermarkCssClass="noteMark" WatermarkText="Enter Note" />
                                                                                        <asp:ImageButton ID="rep_note_send" runat="server" CommandName="send_note" CommandArgument='<%#Eval("query_id") %>'
                                                                                            ImageUrl="/images/send.gif" AlternateText="Send" Style="display: none;" />
                                                                                    </asp:Panel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </telerik:RadAjaxPanel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                    <br />
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel66" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                                        <asp:Panel ID="pnl_after_launch_buttons" runat="server">
                                            <div class="addButton">
                                                <asp:ImageButton ID="btnEditAfterLaunch" runat="server" AlternateText="Save" ImageUrl="/images/edit.gif" />
                                                <asp:ImageButton ID="btnUpdateAfterLaunch" runat="server" AlternateText="Update"
                                                    ImageUrl="/images/update.gif" ValidationGroup="after_launch" />
                                            </div>
                                            <div class="cancelButton" id="divCancelAfterLaunch" runat="server">
                                                <asp:ImageButton ID="btnCancelAfterLaunch" runat="server" AlternateText="Cancel"
                                                    ImageUrl="/images/cancel.gif" />
                                            </div>
                                        </asp:Panel>
                                    </telerik:RadAjaxPanel>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView5" runat="server">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td class="tdTabItemLast">
                                    <div class="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/current_stutas.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            Current Status
                                        </div>
                                        <div style="float: right; width: 150px; padding: 5px;">
                                            <a id="a_refresh_currentstatus" name="a_refresh_currentstatus" title="Refresh" onclick="refresh_currentstatus(0);"
                                                href="javascript:void(0);">
                                                <img alt="Refresh" src="/images/refresh.png" /></a>
                                        </div>
                                    </div>
                                    <div style="padding: 10px;">
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel7" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                            CssClass="TabGrid">
                                            <uc:CurrentStatus ID="UC_CurrentStatus" runat="server" />
                                        </telerik:RadAjaxPanel>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView6" runat="server">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td class="tdTabItem">
                                    <div class="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/comments_icon.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            Comments
                                        </div>
                                    </div>
                                    <div style="padding: 10px;">
                                        <uc3:AuctionComments ID="UC_auction_comments" runat="server" />
                                    </div>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView7" runat="server">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td class="tdTabItem">
                                    <div class="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/icon-filter.png) no-repeat;
                                            background-position: 10px 0px;">
                                            Left bar Bucket
                                        </div>
                                    </div>
                                    <div style="padding: 10px;">
                                        <uc7:LeftbarBucket ID="UC_LeftbarBucket" runat="server" />
                                    </div>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
    <script language="JavaScript" type="text/javascript">
        if (document.getElementById('ctl00_Child_Content_rdolst_language_0').checked == false && document.getElementById('ctl00_Child_Content_rdolst_language_1').checked == false && document.getElementById('ctl00_Child_Content_rdolst_language_2').checked == false && document.getElementById('ctl00_Child_Content_rdolst_language_3').checked == false)
            document.getElementById('ctl00_Child_Content_rdolst_language_0').checked = true; 
    </script>
    <script type="text/javascript">
            function validateCombo(source, args) 
            {
                args.IsValid = false;
                var combo = $find("<%= ddl_product_category.ClientID %>");
                var text = combo.get_text();
                if (text.length < 1) 
                {
                    args.IsValid = false;
                }
                else 
                {
                    var node = combo.findItemByText(text);
                    if (node) 
                    {
                        var value = node.get_value();
                        if (value.length > 0) 
                        {
                            args.IsValid = true;
                        }
                    }
                    else 
                    {
                        args.IsValid = false;
                    }
                } 
           }
            function validate_seller(source, args) 
            {
                args.IsValid = false;
                var combo = $find("<%= ddl_seller.ClientID %>");
                var text = combo.get_text();
                if (text.length < 1) 
                {
                    args.IsValid = false;
                }
                else 
                {
                    var node = combo.findItemByText(text);
                    if (node) 
                    {
                        var value = node.get_value();
                        if (value.length > 0) 
                        {
                            args.IsValid = true;
                        }
                    }
                    else 
                    {
                        args.IsValid = false;
                    }
                } 
           }
            function validate_stock_location(source, args) 
            {
                args.IsValid = false;
                var combo = $find("<%= ddl_stock_location.ClientID %>");
                var text = combo.get_text();
                if (text.length < 1) 
                {
                    args.IsValid = false;
                }
                else 
                {
                    var node = combo.findItemByText(text);
                    if (node) 
                    {
                        var value = node.get_value();
                        if (value.length > 0) 
                        {
                            args.IsValid = true;
                        }
                    }
                    else 
                    {
                        args.IsValid = false;
                    }
                } 
           }
            function validate_stock_condition(source, args) 
            {
                
                args.IsValid = false;
                var combo1 = $find("<%= ddl_stock_condition.ClientID %>");
                var text = combo1.get_text();
                
                if (text.length > 0) 
                {
                    var node = combo1.findItemByText(text);
                    if (node) 
                    {
                        var value = node.get_value();
                        if (value.length > 0) 
                        {
                            args.IsValid = true;
                        }
                    }
                    else 
                    {
                        args.IsValid = false;
                    }
                }
            }
            function validate_packaging(source, args) 
            {
                
                args.IsValid = false;
                var combo1 = $find("<%= ddl_packaging.ClientID %>");
                var text = combo1.get_text();
                
                if (text.length > 0) 
                {
                    var node = combo1.findItemByText(text);
                    if (node) 
                    {
                        var value = node.get_value();
                        if (value.length > 0) 
                        {
                            args.IsValid = true;
                        }
                    }
                    else 
                    {
                        args.IsValid = false;
                    }
                }
            }

        function open_buyer(_path,_id) {
            w1=window.open(_path + '?i=' + _id, '_assign_b', 'top=' + ((screen.height - 510) / 2) + ', left=' + ((screen.width - 800) / 2) + ', height=580, width=800,scrollbars=yes,toolbars=no,resizable=1;');
         w1.focus();
         return false;

        }
        function Focus(objname, waterMarkText) {
            obj = document.getElementById(objname);
            if (obj.value == waterMarkText) {
                obj.value = "";
                obj.className = "WaterMarkText";
                if (obj.value == "Response" || obj.value == "" || obj.value == null) {
                    obj.style.color = "black";
                }
            }
        }
        function tab_PostBackFunction()
         {
       
          document.getElementById('<%=but_bind_tab.ClientID %>').click();
       }
         function tab_PostBackFunction_terms()
         {
       
          document.getElementById('<%=but_bind_tab_terms.ClientID %>').click();
       }
        function Blur(objname, waterMarkText) {

            obj = document.getElementById(objname);
            if (obj.value == "") {
                obj.value = waterMarkText;
                //                if (objname != "txtPwd") {
                //                    obj.className = "WaterMarkedTextBox";
                //                }
                //                else {
                obj.className = "WaterMarkedTextBox";
                //                }
            }
            else {
                obj.className = "WaterMarkText";
            }

            if (obj.value == "Response" || obj.value == "" || obj.value == null) {
                obj.style.color = "gray";
            }
        }
               
        function open_pop_win(_type, _mode) {
       
           w1= window.open('/Auctions/AuctionBasicPop.aspx' + '?i=' + <%=Request("i") %> + '&mode=' + _mode + '&type=' + _type + '&l=' + document.getElementById('<%=hid_language.ClientID %>').value , '_assign8', 'top=' + ((screen.height - 650) / 2) + ', left=' + ((screen.width - 725) / 2) + ', height=650, width=725,scrollbars=yes,toolbars=no,resizable=1;');
              w1.focus();
            return false;

        }
        
          function open_pop_items(_type, _mode) {
       
           w1= window.open('/Auctions/AuctionBasicPop.aspx' + '?i=' + <%=Request("i") %> + '&mode=' + _mode + '&type=' + _type , '_assign15', 'top=' + ((screen.height - 450) / 2) + ', left=' + ((screen.width - 500) / 2) + ', height=300, width=350,scrollbars=no,toolbars=no,resizable=1;');
              w1.focus();
            return false;

        }

        function invited_bidders(id,op) {
            window.open('/Auctions/ListInvitedBidders.aspx?i=' + id + '&j=' + op, 'invited_bidder', 'left=' + ((screen.width - 800) / 2) + ',top=' + (0) + ',width=800,height=700,scrollbars=yes,resizable=no,toolbars=no');
            window.focus();
            return false;
        }
        
            var _refreshctrl;
            function refresh_afterlaunch_currentstatus() {
               var _option=document.getElementById('<%=hid_tab_name.ClientID%>').value;
       if (_option == 3) {
                //alert(_option);
                    _refreshctrl = setTimeout('refresh_afterlaunch(' + _option + ')', 30000);
                }
                else if (_option == 4) {
                //alert(_option);
                    _refreshctrl = setTimeout('refresh_currentstatus(' + _option + ')', 30000);
                }
                else if (_refreshctrl) {
            //alert(_refreshme);
                    clearTimeout(_refreshctrl);
                }
            }
    </script>
</asp:Content>
