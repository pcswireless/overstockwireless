﻿
Partial Class Auctions_MessageBoard
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim str As String = ""
        Dim ds As New DataSet
        str = "select (U.first_name +' ' + U.last_name) as name, M.[message_board_id],M.[description],M.[submit_date] from " & _
        "tbl_message_boards M inner join tbl_sec_users U on U.user_id=M.submit_by_user_id " & _
        "INNER JOIN tbl_message_board_user_mappings UM ON M.message_board_id=UM.message_board_id " & _
        "where Convert(varchar,getDate(),101) between Convert(varchar,display_from,101) and Convert(varchar,display_to,101) and M.is_active=1 and UM.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " order by M.submit_date"
        ds.Tables.Clear()
        ds = SqlHelper.ExecuteDataset(str)
        dl_message.DataSource = ds
        dl_message.DataBind()
        If ds.Tables(0).Rows.Count = 0 Then
            lbl_no_record.Text = "There is no record."
        End If

    End Sub
End Class
