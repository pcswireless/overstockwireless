﻿
Partial Class Auctions_BidderAllQuery
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            load_auction_bidder_queries()
        End If
    End Sub
    Protected Sub load_auction_bidder_queries()

        If Not String.IsNullOrEmpty(Request.QueryString("i")) And IsNumeric(Request.QueryString("i").ToString) Then
            Dim qry As String = ""

            If CommonCode.is_admin_user() Then
                qry = "select A.query_id,A.title,isnull(A.message,'') as message,A.submit_date,A.buyer_id,B.company_name,ISNULL(A1.title,'') as auction_name,ISNULL(A1.auction_id,0) as auction_id,ISNULL(S.first_name,'') + ' ' + ISNULL(S.last_name,'') As ask_to,ISNULL(A.sales_rep_id,0) AS sales_rep_id,isnull(A.is_active,0) as is_active from tbl_auction_queries A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id LEFT join tbl_auctions A1 WITH (NOLOCK) on A.auction_id=A1.auction_id Left JOIN tbl_sec_users S ON A.sales_rep_id=S.user_id  where A.parent_query_id=0 and A.buyer_id=" & Request.QueryString("i") & " order by A.submit_date desc"
            Else
                qry = "select A.query_id,A.title,isnull(A.message,'') as message,A.submit_date,A.buyer_id,B.company_name,ISNULL(A1.title,'') as auction_name,ISNULL(A1.auction_id,0) as auction_id,ISNULL(S.first_name,'') + ' ' + ISNULL(S.last_name,'') As ask_to,ISNULL(A.sales_rep_id,0) AS sales_rep_id,isnull(A.is_active,0) as is_active from tbl_auction_queries A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id LEFT join tbl_auctions A1 WITH (NOLOCK) on A.auction_id=A1.auction_id inner join tbl_reg_seller_user_mapping C on A1.seller_id=C.seller_id Left JOIN tbl_sec_users S ON A.sales_rep_id=S.user_id where A.parent_query_id=0 and C.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " and A.buyer_id=" & Request.QueryString("i") & " order by A.submit_date desc"
            End If
            'If Not String.IsNullOrEmpty(Request.QueryString("b")) And IsNumeric(Request.QueryString("b").ToString) Then
            '    qry = qry & " and A.buyer_id=" & Request.QueryString("b") & ""
            'End If
            'qry = qry & " order by A.submit_date desc"
            'Response.Write(qry)
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDataTable(qry)
            rep_after_queries.DataSource = dt
            rep_after_queries.DataBind()
            If dt.Rows.Count = 0 Then
                lblNoRecord.Visible = True
            End If
        End If
    End Sub

    Protected Sub rep_after_queries_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rep_after_queries.ItemDataBound
        Dim rep_inner As Repeater = DirectCast(e.Item.FindControl("rep_inner_after_queries"), Repeater)
        rep_inner.DataSource = SqlHelper.ExecuteDataTable("select A.message,A.submit_date,C.first_name from tbl_auction_queries A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id left join tbl_sec_users C on A.user_id=C.user_id where A.parent_query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & "")
        rep_inner.DataBind()
    End Sub
End Class
