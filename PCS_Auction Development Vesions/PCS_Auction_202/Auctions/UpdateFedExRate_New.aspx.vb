﻿
Partial Class Auctions_UpdateFedExRate_New
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Lbl_Id.Text = ""
            Dim auction_id As Integer = 0
            If IsNumeric(Request.QueryString.Get("i")) Then
                auction_id = Request.QueryString.Get("i")
            End If
            pnl_fedex.Visible = True
            Dim str_weight As String = SqlHelper.ExecuteScalar("select cast(count(P.fedex_pagkage_id) as varchar(15))+'#'+cast(isnull(sum(P.package_weight),0) as varchar(15))+'#'+cast(isnull(A.weight,0) as varchar(15)) from tbl_auction_fedex_packages P right join tbl_auctions A WITH (NOLOCK) on P.auction_id=A.auction_id where A.auction_id=" & auction_id & " group by A.weight")
            If str_weight.Contains("#") Then
                Dim str() As String
                str = str_weight.Split("#")
                If str.Length = 3 AndAlso (str(1) <> str(2) And str(0) > 0) Then
                    lbl_fedex_error.Text = "Packages weight (" & FormatNumber(CDbl(str(0)), 2) & " lbs) and Auction weight (" & FormatNumber(CDbl(str(1)), 2) & " lbs) should match!!"
                    pnl_fedex.Visible = False
                End If
            End If
            Dim auction_weight As Double = SqlHelper.ExecuteScalar("select isnull(weight,0) from tbl_auctions WITH (NOLOCK) where auction_id=" & auction_id)

            If auction_weight > 0 Then

                Dim total_invitation As Integer = SqlHelper.ExecuteScalar("select count(*) from [dbo].[fn_get_invited_buyers](" & auction_id & ")")
                Dim remaining_bidder As Integer = SqlHelper.ExecuteScalar(" select count(A.buyer_id) from dbo.fn_get_invited_buyers(" & auction_id & ") A INNER JOIN tbl_reg_buyers B ON A.buyer_id=B.buyer_id and A.buyer_id not in( select buyer_id from tbl_auction_fedex_rates WITH (NOLOCK) where auction_id=" & auction_id & ")")

                lbl_count_total.Text = total_invitation
                lbl_count_completed.Text = total_invitation - remaining_bidder
                lbl_count_remaining.Text = remaining_bidder

                hid_weight.Value = auction_weight
                If total_invitation > 0 Then
                    If remaining_bidder > 0 Then
                        'Dim meta As New HtmlMeta
                        'meta = New HtmlMeta
                        'meta.Attributes.Add("http-equiv", "Refresh")
                        'meta.Attributes.Add("content", "20")
                        'Me.Page.Header.Controls.Add(meta)
                        'meta = Nothing

                        hid_OK.Value = 0
                    Else
                        hid_OK.Value = 1

                        Lbl_Id.Text = "<font color='red'>Shipping rates of all invited bidder has been calculated.</font>"
                        lbl_close_window.Text = "<font color='red'>You can close this window now</font>"
                    End If
                Else
                    hid_OK.Value = 2
                    Lbl_Id.Text = "Bidder not invited."
                    lbl_close_window.Text = ""
                End If
            Else
                hid_OK.Value = 3
                Lbl_Id.Text = "Auction weight not defined."
                lbl_close_window.Text = ""
            End If

        End If

    End Sub

    Private Sub fetchShippingRate(ByVal auction_id As Integer, ByVal buyer_id As Integer, ByVal Destination_StreetLines As String, ByVal Destination_City As String, ByVal Destination_StateOrProvinceCode As String, ByVal destination_zip As String, ByVal destination_country_code As String, ByVal auction_weight As Double)
        'Try
        Dim obj As New FedAllRateServices
        Dim objreq As New FedexRateServices.RateRequest
        Dim objrep As New FedexRateServices.RateReply
        Dim dt As New DataTable()
        Dim i As Integer
        Dim Origin_StreetLines = "", Origin_City = "", Origin_StateOrProvinceCode = "", Origin_PostalCode = "", Origin_CountryCode As String = ""
        ''Origin_StreetLines = "3940 30th street" ' SqlHelper.of_FetchKey("fedex_source_street")
        ''Origin_City = "Long island city" 'SqlHelper.of_FetchKey("fedex_source_city")
        ''Origin_StateOrProvinceCode = "NY" ' SqlHelper.of_FetchKey("fedex_source_state")
        Origin_PostalCode = SqlHelper.of_FetchKey("fedex_source_zip") ' 11101
        Origin_CountryCode = SqlHelper.of_FetchKey("fedex_source_country_code") ' "US" 


        dt = obj.GetFedexShippingOptions(auction_id, Origin_StreetLines, Origin_City, Origin_StateOrProvinceCode, Origin_PostalCode, Origin_CountryCode, Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, destination_zip, destination_country_code, 1, 1, auction_weight * 1).DefaultView.ToTable(True, "ServiceType", "Rate", "Error")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                With dt.Rows(i)
                    If .Item("Error").ToString.Trim <> "" Then
                        'HttpContext.Current.Response.Write("if not exists(select fedex_rate_id from tbl_auction_fedex_rates where auction_id=" & auction_id & " and buyer_id =" & buyer_id & " and shipping_options='" & .Item("ServiceType") & "' and shipping_amount=" & .Item("rate") & ") begin INSERT INTO tbl_auction_fedex_rates(auction_id, buyer_id, shipping_amount,shipping_options,error_log,is_error) VALUES (" & auction_id & ", " & buyer_id & ",'" & .Item("Rate") & "','" & .Item("ServiceType") & "','" & .Item("Error") & "',1) end---" & .Item("Error") & "<br>")
                        SqlHelper.ExecuteNonQuery("if not exists(select fedex_rate_id from tbl_auction_fedex_rates WITH (NOLOCK) where auction_id=" & auction_id & " and buyer_id =" & buyer_id & " and shipping_options='" & .Item("ServiceType") & "' and shipping_amount=" & .Item("rate") & ") begin INSERT INTO tbl_auction_fedex_rates(auction_id, buyer_id, shipping_amount,shipping_options,error_log,is_error) VALUES (" & auction_id & ", " & buyer_id & ",'" & .Item("Rate") & "','" & .Item("ServiceType") & "','" & .Item("Error") & "',1) end")
                    Else
                        SqlHelper.ExecuteNonQuery("if not exists(select fedex_rate_id from tbl_auction_fedex_rates WITH (NOLOCK) where auction_id=" & auction_id & " and buyer_id =" & buyer_id & " and shipping_options='" & .Item("ServiceType") & "' and shipping_amount=" & .Item("rate") & ") begin INSERT INTO tbl_auction_fedex_rates(auction_id, buyer_id, shipping_amount,shipping_options,error_log,is_error) VALUES (" & auction_id & ", " & buyer_id & ",'" & .Item("Rate") & "','" & .Item("ServiceType") & "','',0) end")
                    End If

                End With
            Next
        Else
            'SqlHelper.ExecuteNonQuery("if not exists(select fedex_rate_id from tbl_auction_fedex_rates where auction_id=" & auction_id & " and buyer_id =" & buyer_id & " and shipping_options='') begin INSERT INTO tbl_auction_fedex_rates(auction_id, buyer_id, shipping_amount,shipping_options,error_log,is_error) VALUES (" & auction_id & ", " & buyer_id & ",0,'',1) end")
        End If
        dt.Dispose()

        'Catch ex As Exception
        'Response.Write("p-" & ex.Message)
        'SqlHelper.ExecuteNonQuery("if not exists(select fedex_rate_id from tbl_auction_fedex_rates where auction_id=" & auction_id & " and buyer_id =" & buyer_id & " and shipping_options='') begin INSERT INTO tbl_auction_fedex_rates(auction_id, buyer_id, shipping_amount,shipping_options,error_log,is_error) VALUES (" & auction_id & ", " & buyer_id & ",0,'','p-" & ex.Message & "',1) end")
        'End Try
    End Sub

    Protected Sub btn_get_rate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_get_rate.Click


        Dim auction_id As Integer = Request.QueryString.Get("i")
        Dim i As Integer
        Dim dt As DataTable
        dt = SqlHelper.ExecuteDatatable("select A.buyer_id,ISNULL(A.address1, '') + ' ' + ISNULL(A.address2, '')  AS address, ISNULL(A.city, '') AS city,ISNULL(A.state_text, 0) AS state_text,ISNULL(A.zip, '') AS zip,ISNULL(C.CODE, 0) AS country_code from tbl_reg_buyers A inner join tbl_master_countries C on A.country_id=C.country_id where status_id=2 and ISNULL(discontinue,0)=0 and A.buyer_id not in (select buyer_id from tbl_auction_fedex_rates WITH (NOLOCK) where auction_id=" & auction_id & ")")

        Dim cal_total As Integer = 10
        If dt.Rows.Count < cal_total Then cal_total = dt.Rows.Count

        For i = 0 To cal_total - 1
            With dt.Rows(i)
                'Response.Write(.Item("address") & "-" & .Item("city") & "-" & .Item("state_text") & "-" & .Item("zip") & "-" & .Item("country_code"))
                fetchShippingRate(auction_id, .Item("buyer_id"), .Item("address"), .Item("city"), .Item("state_text"), .Item("zip"), .Item("country_code"), hid_weight.Value)
                'fetchShippingRate(auction_id, .Item("buyer_id"), "1000 Bishop St", "Honolulu", "HI", "96813-4202", "US", "50")
            End With
        Next
        'ltrl_refresh.Text = "<script language='javascript' type='text/javascript'>window.opener.refresh_auction();</script>"
        'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "fedex", "<script language='javascript' type='text/javascript'>window.opener.refresh_auction();</script>", True)
        'ltrl_refresh.Text = "<script language='javascript' type='text/javascript'>window.opener.refresh_auction();</script>"
        'cal_total = SqlHelper.ExecuteScalar("select count(B.buyer_id) from [dbo].[fn_get_invited_buyers](" & auction_id & ") B where B.buyer_id not in (select buyer_id from tbl_auction_fedex_rates where auction_id=" & auction_id & ")")
        'If cal_total > 0 Then
        '    lbl_count_remaining.Text = cal_total
        '    lbl_count_completed.Text = lbl_count_total.Text - cal_total
        '    'lbl_test.Text = cal_total & " Bidders left and calculate automatically in next refresh"
        'Else
        '    Lbl_Id.Text = "Shipping rates of all invited bidder has been calculated."

        'End If
    End Sub

End Class
