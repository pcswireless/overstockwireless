﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BidderQueries.ascx.vb"
    Inherits="Auctions_UserControls_BidderQueries" %>
    <div style="padding-bottom: 10px; padding-top:8px;">
         <div class="pageheading">
            &nbsp;  Bidder Queries</div>
</div>
    <table width="100%" cellpadding="0" cellspacing="0">
       <tr>
            <td>
                <div style="padding: 0; margin: 0; text-align: center; padding-bottom:10px; background-color: rgb(251,251,251);">
                    <center>
                        <table width="90%" style="text-align: left;">
                            <tr>
                                <td style="padding-left: 10px;">
                                    <asp:Repeater ID="rep_after_queries" runat="server">
                                        <ItemTemplate>
                                            <table width="100%" style="border: 1px solid black; padding: 5px; margin-top: 15px;
                                                text-align: left; border: 2px solid #fff; background-color: #fff">
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Label runat="server" Font-Bold="true" ForeColor="#194467" Text='<%#Eval("title") %>'
                                                            ID="rep_lbl_title"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div style="float: left; width: 20%;">
                                                            <asp:Label runat="server" Font-Bold="true" Text='<%#Eval("company_name") %>' ID="rep_lbl_name"></asp:Label>
                                                        </div>
                                                        <div style="float: left; width: 80%;">
                                                            <asp:Label runat="server" Font-Size="13px" ForeColor="Gray" Text='<%#Eval("submit_date") %>'
                                                                ID="rep_lbl_date"></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 74%;">
                                                        <asp:Repeater ID="rep_inner_after_queries" runat="server">
                                                            <ItemTemplate>
                                                                <table width="100%" style="border: 1px solid black; padding: 5px; margin-top: 15px;
                                                                    text-align: left; border: 1px solid #C1C1C1;">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label runat="server" Font-Bold="true" Text='<%#Eval("message") %>' ID="rep_inner_lbl_msg"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span style="font-style: italic; font-size: 12px; color: gray;">by&nbsp;<asp:Label
                                                                                runat="server" Text='<%#Eval("first_name") %>' ID="rep_inner_lbl_user"></asp:Label>&nbsp;on&nbsp;<asp:Label
                                                                                    runat="server" Text='<%#Eval("submit_date") %>' ID="rep_inner_lbl_date"></asp:Label></span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                              
                                            </table>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    
                                </td>
                            </tr>
                        </table>
                    </center>
                     <asp:Label ID="lblNoRecord" runat="server" Text="Currently, there is no bidder query." Visible="false" ForeColor="Red"></asp:Label>
                </div>
               
            </td>
        </tr>
    </table>
