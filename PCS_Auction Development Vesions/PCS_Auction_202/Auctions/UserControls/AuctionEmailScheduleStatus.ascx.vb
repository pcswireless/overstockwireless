﻿Imports Telerik.Web.UI
Imports System.Drawing
Partial Class Auctions_UserControls_Auction_Email_Schedule
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Private Sub setPermission()

        If Not CommonCode.is_super_admin() Then
            Dim Edit_Auction As Boolean = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))
            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 3
                        Edit_Auction = True
                End Select
            Next
            dt = Nothing

            If Not Edit_Auction Then
                RadGrid_Email_Schedule.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None
            End If
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                hid_auction_id.Value = Request.QueryString.Get("i")
            End If
        End If
    End Sub

End Class
