﻿Imports Telerik.Web.UI
Imports System.Drawing
Partial Class Auctions_UserControls_AddViewAfterLaunchComment
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
                setAfterLaunchEdit(True)
            Else
                setAfterLaunchEdit(False)
            End If
        End If
    End Sub
    Private Sub setAfterLaunchEdit(ByVal readMode As Boolean)
        If Request.QueryString.Get("i") <> "0" Then
            fillAfterLaunchData(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
        End If
        visibleAfterLaunchData(readMode)
    End Sub
    Private Sub fillAfterLaunchData(ByVal auction_id As Integer)
        Dim dt As DataTable = New DataTable()
        dt = SqlHelper.ExecuteDatatable("SELECT isnull(A.after_launch_message,'') as after_launch_message FROM tbl_auctions A WITH (NOLOCK) WHERE A.auction_id = " & auction_id)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lbl_after_launch.Text = CommonCode.decodeSingleQuote(.Item("after_launch_message"))
                RadEditor2.Content = lbl_after_launch.Text
            End With
        End If
        dt = Nothing

    End Sub

    Private Sub visibleAfterLaunchData(ByVal readMode As Boolean)

        If readMode Then
            btnEditAfterLaunch.Visible = True
            btnUpdateAfterLaunch.Visible = False
            btnCancelAfterLaunch.Visible = False
        Else
            btnEditAfterLaunch.Visible = False
            btnUpdateAfterLaunch.Visible = True
            btnCancelAfterLaunch.Visible = True
        End If

        lbl_after_launch.Visible = readMode
        pnl_after_launch_editor.Visible = Not readMode

    End Sub

    Protected Sub btnEditAfterLaunch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditAfterLaunch.Click
        visibleAfterLaunchData(False)
    End Sub
    Protected Sub btnCancelAfterLaunch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelAfterLaunch.Click
        visibleAfterLaunchData(True)
    End Sub
    Protected Sub btnUpdateAfterLaunch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpdateAfterLaunch.Click
        If Page.IsValid Then
            Dim is_update_required As Boolean = False
            Dim updpara As String = ""

            If RadEditor2.Content.Trim() <> lbl_after_launch.Text.Trim() Then
                If is_update_required Then
                    updpara = updpara & ",after_launch_message='" & CommonCode.encodeSingleQuote(RadEditor2.Content.Trim()) & "'"
                Else
                    updpara = updpara & "after_launch_message='" & CommonCode.encodeSingleQuote(RadEditor2.Content.Trim()) & "'"
                End If
                is_update_required = True
            End If

            If is_update_required Then
                updpara = updpara & ",update_date=getdate(),update_by_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
                SqlHelper.ExecuteNonQuery("update tbl_auctions set " & updpara & " where auction_id=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
            End If

            setAfterLaunchEdit(True)
        End If
    End Sub

    
End Class
