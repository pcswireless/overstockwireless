﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AuctionEmailScheduleStatus.ascx.vb"
    Inherits="Auctions_UserControls_Auction_Email_Schedule" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
    Skin="Simple" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" alt="" />
    </telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="RadAjaxPanel9" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
    CssClass="TabGrid">
    <asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
    
    <telerik:RadGrid ID="RadGrid_Email_Schedule" GridLines="None" runat="server" AllowAutomaticDeletes="True"
        AllowAutomaticInserts="True" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True"
        AutoGenerateColumns="False" DataSourceID="SqlDataSource1" AllowMultiRowSelection="false"
        AllowMultiRowEdit="false" Skin="Vista" AllowSorting="true" ShowGroupPanel="false">
        <PagerStyle Mode="NextPrevAndNumeric" />
        <MasterTableView Width="100%"  DataSourceID="SqlDataSource1"
            ItemStyle-Height="40" AlternatingItemStyle-Height="40" HorizontalAlign="NotSet"
            AutoGenerateColumns="False" SkinID="Vista" CommandItemDisplay="Top" EditMode="EditForms">
            <CommandItemStyle BackColor="#E1DDDD" />
            <NoRecordsTemplate>
                No Email has sent
            </NoRecordsTemplate>
            <CommandItemSettings  ShowRefreshButton="false" ShowAddNewRecordButton="false" />
            <Columns>
                <telerik:GridBoundColumn DataField="title" HeaderText="Schedule Name" SortExpression="title" ItemStyle-HorizontalAlign="Left"
                    UniqueName="title" >
                </telerik:GridBoundColumn> 
                <telerik:GridBoundColumn DataField="email_id" HeaderText="Email ID" SortExpression="email_id" ItemStyle-HorizontalAlign="Left"
                    UniqueName="email_id" >
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="created_date" HeaderText="Date" SortExpression="created_date" ItemStyle-HorizontalAlign="Left"
                    UniqueName="created_date" >
                </telerik:GridBoundColumn>
            </Columns>
            
        </MasterTableView>
        <ClientSettings AllowDragToGroup="false">
            <Selecting AllowRowSelect="True"></Selecting>
        </ClientSettings>
        <GroupingSettings ShowUnGroupButton="false" />
    </telerik:RadGrid>
    <%--<telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditor1" runat="server" TextBoxStyle-Width="400px"
    TextBoxMode="MultiLine" TextBoxStyle-Height="70px" />--%>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
        ProviderName="System.Data.SqlClient" SelectCommand="select isnull(b.title,'') as title,a.email_id,created_date from tbl_auction_email_schedule_statuses a join tbl_auction_email_schedules b  on a.email_schedule_id=b.email_schedule_id  where a.auction_id=@auction_id">
        <SelectParameters>
            <asp:ControlParameter Name="auction_id" Type="Int32" ControlID="hid_auction_id" PropertyName="Value" />
        </SelectParameters>
    </asp:SqlDataSource>
</telerik:RadAjaxPanel>
