﻿
Partial Class Auctions_UserControls_AuctionTemplate
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
                pnl_duplicate.Visible = False
                pnl_save_template.Visible = False


                ddl_auction_template.DataSource= SqlHelper.ExecuteDataTable("select auction_id,name from tbl_auction_templates where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id"))
                ddl_auction_template.DataBind()
                If ddl_auction_template.Items.Count > 0 Then
                    pnl_from_template.Visible = True
                    ddl_auction_template.Items.Insert(0, New ListItem("--Select--", "0"))
                Else
                    pnl_from_template.Visible = False
                End If
            Else
                pnl_from_template.Visible = False
                pnl_duplicate.Visible = True
                pnl_save_template.Visible = True

                Dim dt As New DataTable
                dt = SqlHelper.ExecuteDataTable("select name from tbl_auction_templates where auction_id=" & Request.QueryString.Get("i") & " and user_id=" & CommonCode.Fetch_Cookie_Shared("user_id"))
                If dt.Rows.Count > 0 Then
                    txt_template.Text = dt.Rows(0).Item("name")
                    lnk_save.Text = "Update"
                End If
                dt = Nothing
                setEdit(False)
            End If
        End If
    End Sub

    Protected Sub lnk_duplicate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_duplicate.Click
        Dim new_id As Integer = 0
        Dim objAuc As New Auction
        new_id = objAuc.copyAuction(Request.QueryString.Get("i"), CommonCode.Fetch_Cookie_Shared("user_id"))

        CommonCode.insert_system_log("Duplicate auction created", "lnk_duplicate_Click", Request.QueryString.Get("i"), "", "Auction")
        objAuc = Nothing
        redirectPage(new_id)

    End Sub

    Private Sub setEdit(ByVal EditMode As Boolean)
        txt_template.Visible = EditMode
        rfv_template.Enabled = EditMode
        lnk_save.Visible = EditMode
        lnk_cancel.Visible = EditMode
        lnk_save_template.Visible = Not EditMode
    End Sub

    Protected Sub lnk_save_template_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_save_template.Click

        setEdit(True)
    End Sub

    Protected Sub lnk_save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_save.Click
        If Page.IsValid Then
            SqlHelper.ExecuteNonQuery("if exists(select template_id from tbl_auction_templates where auction_id=" & Request.QueryString.Get("i") & " and user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ") begin update tbl_auction_templates set name='" & txt_template.Text.Trim & "' where auction_id=" & Request.QueryString.Get("i") & " and user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " end else begin INSERT INTO tbl_auction_templates(auction_id, user_id, name) VALUES (" & Request.QueryString.Get("i") & ", " & CommonCode.Fetch_Cookie_Shared("user_id") & ", '" & txt_template.Text.Trim & "') end")
            CommonCode.insert_system_log("Auction template name updated.", "lnk_save_Click", Request.QueryString.Get("i"), "", "Auction")
            setEdit(False)
        End If

    End Sub

    Protected Sub lnk_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_cancel.Click
        setEdit(False)
    End Sub


    Protected Sub btn_copy_from_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_copy_from.Click
        If Page.IsValid Then
            Dim new_id As Integer = 0
            Dim objAuc As New Auction
            new_id = objAuc.copyAuction(ddl_auction_template.SelectedValue, CommonCode.Fetch_Cookie_Shared("user_id"))
            CommonCode.insert_system_log("A new auction copied from this auction", "btn_copy_from_Click", Request.QueryString.Get("i"), "", "Auction")
            objAuc = Nothing
            redirectPage(new_id)
        End If
    End Sub

    Private Sub redirectPage(ByVal new_auction_id As Integer)
        If new_auction_id = 0 Then
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('An error during this process. Please try again')", True)

        Else
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "redirectIframe('/Backend_TopBar.aspx?t=" & CommonCode.getAuctionTabPosition(new_auction_id) & "&a=" & new_auction_id & "','/Backend_Leftbar.aspx?t=3&a=" & new_auction_id & "','/Auctions/AddEditAuction.aspx?i=" & new_auction_id & "')", True)

        End If
    End Sub
    
End Class
