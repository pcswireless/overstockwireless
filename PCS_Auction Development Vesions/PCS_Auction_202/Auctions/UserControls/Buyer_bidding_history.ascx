﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Buyer_bidding_history.ascx.vb"
    Inherits="Auctions_UserControls_Buyer_bidding_history" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<div class="pageheading">
    Bidder History
</div>
<div class="statusCompany" style="padding-left: 0px;">
    <asp:Literal ID="ltrl_buyer" runat="server"></asp:Literal>
</div>
<div style="padding-left: 0px; padding-top: 12px; font-size: 12PX; font-weight: bold;">
    Winning Lot
</div>
<div style="font-weight: bold; padding-top: 10px;">
    <telerik:RadGrid ID="RadGrid_BidHistory" DataSourceID="RadGrid_BidHistory_SqlDataSource"
        runat="server" Skin="Vista" AllowSorting="false" AllowPaging="True" PageSize="10"
        OnPreRender="RadGrid_BidHistory_PreRender" ShowHeader="false">
        <MasterTableView TableLayout="Fixed" DataKeyNames="bid_id">
            <NoRecordsTemplate>
                No bidding has been done for any auction by the buyer.
            </NoRecordsTemplate>
            <ItemTemplate>
                <%#IIf(Not CType(Container, GridItem).ItemIndex = 0, "</td></tr></table>", "")%>
                <asp:Panel ID="ItemContainer" CssClass='<%# IIf(CType(Container, GridItem).ItemType = GridItemType.Item, "cmp_auc_item_history", "cmp_auc_alternatingItem_history") %>'
                    runat="server">
                    <div style="height: 110px; display: block; overflow: hidden; border: solid 0px red;">
                        <div style="border: solid 0px green;">
                            <a style="text-decoration: underline;" onmouseout="hide_tip_new();" onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');">
                                <%# Eval("title")%></a>
                            <br />
                            <%# Eval("code")%>
                            <br />
                            <%# Eval("bid_date")%>
                        </div>
                        <div style="overflow: hidden; border: solid 0px red; padding-top: 10px;">
                            <span style="font-weight: bold;">Bid Amt.:
                                <%# "$" & FormatNumber(Eval("bid_amount"), 2)%></span> &nbsp;&nbsp;<span style="font-weight: bold;"
                                    id="tr_qty" runat="server" visible='<%# IIF(Eval("buy_type")="Partial Offer","True",False) %>'>
                                    Qty:&nbsp;<%#Eval("quantity")%></span></div>
                    </div>
                </asp:Panel>
            </ItemTemplate>
        </MasterTableView>
    </telerik:RadGrid>
    <asp:SqlDataSource ID="RadGrid_BidHistory_SqlDataSource" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
        ProviderName="System.Data.SqlClient" SelectCommand="Select C.* from
        (
            select B.bid_id as bid_id,B.bid_amount,B.auction_id,B.bid_date,'' As buy_type, 0 as quantity,ISNULL(A.code,'') AS code,ISNULL(A.title,'') AS title,ISNULL(A.sub_title,'') AS sub_title,dbo.auction_status(A.auction_id) as Status from tbl_auction_bids B WITH (NOLOCK) INNER JOIN tbl_auctions A WITH (NOLOCK) ON B.auction_id=A.auction_id where B.buyer_id=@buyer_id and B.action='Awarded' 
            union
            select B.buy_id as bid_id,ISNULL(B.price,0) AS bid_amount, B.auction_id,B.bid_date,case ISNULL(B.buy_type,'') when 'partial' then 'Partial Offer' when 'private' then 'Private Offer' when 'buy now' then 'Buy Now' end  AS buy_type,ISNULL(B.quantity,0) As quantity,ISNULL(A.code,'') AS code,ISNULL(A.title,'') AS title,ISNULL(A.sub_title,'') AS sub_title,dbo.auction_status(A.auction_id) as Status from tbl_auction_buy B WITH (NOLOCK) INNER JOIN tbl_auctions A WITH (NOLOCK) ON B.auction_id=A.auction_id where B.buyer_id=@buyer_id and B.action='Awarded'
        ) As C order by C.bid_date" runat="server">
        <SelectParameters>
            <asp:QueryStringParameter Name="buyer_id" QueryStringField="i" />
        </SelectParameters>
    </asp:SqlDataSource>
</div>
<div style="padding-top:20px; text-align:center;">
    <asp:ImageButton ID="ImageButton1" OnClientClick="javascript:window.close();" runat="server"
        AlternateText="Close" ImageUrl="/images/close.gif" />
</div>
<div style="margin-top: 20px">
    <asp:Panel ID="pnl_partial_offer" runat="server" Visible="false">
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td style="font-weight: bold; font-size: 13px; padding-left: 7px; padding-top: 10px;
                    padding-bottom: 10px; text-decoration: underline;">
                    <asp:Label ID="Label1" runat="server" Text="Case of Buy Now / Private / Partial Offer"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="details">
                    <asp:Label ID="lbl_no_buy_now_partial_offers" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:DataList ID="dl_buy_partial_offers" runat="server" RepeatColumns="4" RepeatDirection="Horizontal"
                        DataKeyField="buy_id" CellSpacing="2">
                        <ItemStyle VerticalAlign="Top" />
                        <ItemTemplate>
                            <table cellspacing="2" cellpadding="0" border="0" width="235">
                                <tr>
                                    <td class="details" height="40">
                                        <div style="font-weight: bold; font-size: 12px;">
                                            <a  style="text-decoration: none;" onmouseout="hide_tip_new();" onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');">
                                                <%# Eval("title")%>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="details">
                                        <b>
                                            <%# Eval("buy_type")%></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="details">
                                        <span style="font-weight: bold;">Bid Amt.:
                                            <%# "$" & FormatNumber(Eval("price"), 2)%></span> &nbsp;&nbsp;<span style="font-weight: bold;"
                                                id="tr_qty" runat="server" visible='<%# IIF(Eval("buy_type")="Partial Offer","True",False) %>'>
                                                Qty:&nbsp;<%#Eval("quantity")%></span>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:DataList>
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>
