﻿Imports Telerik.Web.UI
Partial Class Auctions_UserControls_Buyer_bidding_history
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If IsNumeric(Request.QueryString("i")) Then
                bind_buyer()
                'bind_partial()
            End If
        End If

    End Sub
    Protected Sub RadGrid_BidHistory_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        Dim itemCount As Integer = TryCast(sender, RadGrid).MasterTableView.GetItems(GridItemType.Item).Length + TryCast(sender, RadGrid).MasterTableView.GetItems(GridItemType.AlternatingItem).Length
        For Each item As GridItem In TryCast(sender, RadGrid).Items
            If TypeOf item Is GridDataItem AndAlso item.ItemIndex < itemCount - 1 Then
                TryCast(TryCast(item, GridDataItem)("code"), TableCell).Controls.Add(New LiteralControl("<table style='display:none;'><tr><td>"))
            End If
        Next
    End Sub
    Private Sub bind_buyer()
        Dim dt As DataTable = New DataTable()
        dt = SqlHelper.ExecuteDataTable("select ISNULL(company_name,'') AS company_name,ISNULL(email,'') AS email,ISNULL(mobile,'') AS mobile from tbl_reg_buyers where buyer_id=" & Request.QueryString("i"))
        Dim company_name As String = ""
        Dim email As String = ""
        Dim mobile As String = ""
        If dt.Rows.Count > 0 Then
            company_name = dt.Rows(0)("company_name")
            email = dt.Rows(0)("email")
            mobile = dt.Rows(0)("mobile")
        End If
        ltrl_buyer.Text = "<div><a onmouseout=""hide_tip_new();"" onmouseover=""tip_new('/Bidders/bidder_mouse_over.aspx?i=" & Request.QueryString("i") & "','200','white','true');"">" & company_name & "</a></div><div style='padding-top:4px;'><a href='mailto:" & email & "'>" & email & "</a></div><div style='padding-top:4px;'>" & mobile & "</div>"
        'lbl_buyer.Text = company_name
    End Sub
    Private Sub bind_partial()
        Dim str As String = "select	B.auction_id,A.buy_id,A.buyer_id,A.buyer_user_id,ISNULL(A.status,'') As status,ISNULL(A.action,'') As action,ISNULL(A.price,0) As price," & _
        "ISNULL(A.quantity,0) AS quantity," & _
        "case ISNULL(A.buy_type,'') when 'partial' then 'Partial Offer' when 'private' then 'Private Offer' when 'buy now' then 'Buy Now' end  AS buy_type," & _
        "ISNULL(B.code,'') AS code," & _
        "ISNULL(B.title,'') As title," & _
        "ISNULL(B.sub_title,'') AS sub_title " & _
        "from tbl_auction_buy A WITH (NOLOCK)  INNER JOIN tbl_auctions B WITH (NOLOCK) ON A.auction_id=B.auction_id where A.buyer_id=" & Request.QueryString("i") & " Order by A.buy_type, A.price desc "
        Dim dt As DataTable = New DataTable()
        dt = SqlHelper.ExecuteDataTable(str)
        dl_buy_partial_offers.DataSource = dt
        dl_buy_partial_offers.DataBind()

    End Sub
End Class
