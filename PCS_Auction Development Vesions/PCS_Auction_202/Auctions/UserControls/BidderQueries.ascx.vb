﻿
Partial Class Auctions_UserControls_BidderQueries
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            load_auction_bidder_queries()
        End If
    End Sub
    Protected Sub load_auction_bidder_queries()

        If Not String.IsNullOrEmpty(Request.QueryString("i")) > 0 Then
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDatatable("select A.query_id,A.title,A.submit_date,A.buyer_id,B.company_name from tbl_auction_queries A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id inner join tbl_auctions A1 WITH (NOLOCK) on A.auction_id=A1.auction_id inner join tbl_reg_seller_user_mapping C on A1.seller_id=C.seller_id where A.parent_query_id=0 and C.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " and A.auction_id=" & Request.QueryString.Get("i") & "")
            rep_after_queries.DataSource = dt
            rep_after_queries.DataBind()
            If dt.Rows.Count = 0 Then
                lblNoRecord.Visible = True
            End If
        End If
    End Sub

    Protected Sub rep_after_queries_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rep_after_queries.ItemDataBound
        Dim rep_inner As Repeater = DirectCast(e.Item.FindControl("rep_inner_after_queries"), Repeater)
        rep_inner.DataSource = SqlHelper.ExecuteDataTable("select A.message,A.submit_date,C.first_name from tbl_auction_queries A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id left join tbl_sec_users C on A.user_id=C.user_id where A.parent_query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & "")
        rep_inner.DataBind()
    End Sub
End Class
