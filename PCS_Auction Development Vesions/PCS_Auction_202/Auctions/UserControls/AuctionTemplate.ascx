﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AuctionTemplate.ascx.vb"
    Inherits="Auctions_UserControls_AuctionTemplate" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
    Skin="Simple" />
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" alt="" />
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxPanel ID="RadAjaxPanel21" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:Panel ID="pnl_save_template" runat="server">
                        <table cellpadding="2" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txt_template" CssClass="inputtype" runat="server" MaxLength="100"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_template" runat="server" Text="*" ValidationGroup="vl_template"
                                        ControlToValidate="txt_template" Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnk_save" runat="server" Text="Save" ValidationGroup="vl_template"></asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnk_save_template" runat="server" Text="Save as template" CausesValidation="false"
                                        CssClass="duplAuction"></asp:LinkButton>
                                    <asp:LinkButton ID="lnk_cancel" runat="server" Text="Cancel" CausesValidation="false"></asp:LinkButton>
                                </td>
                                <td class="duplAuction" style="padding: 0px 10px 0px 10px;">
                                    |
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td align="right">
                    <asp:Panel ID="pnl_duplicate" runat="server">
                        <asp:LinkButton ID="lnk_duplicate" runat="server" CssClass="duplAuction" Text="Duplicate"
                            OnClientClick="javascript:return confirm('You wish to Duplicate this Auction?');"></asp:LinkButton>
                    </asp:Panel>
                    <asp:Panel ID="pnl_from_template" runat="server">
                        <table cellpadding="2" cellspacing="0">
                            <tr>
                                <td class="duplAuction">
                                    Select template
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddl_auction_template" runat="server" DataValueField="auction_id"
                                        DataTextField="name">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfv_ddls" runat="server" ControlToValidate="ddl_auction_template"
                                        Text="*" InitialValue="0" ValidationGroup="vl_intemplate" Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:ImageButton ID="btn_copy_from" runat="server" ValidationGroup="vl_intemplate"
                                        ImageUrl="/images/fend/search_button.gif" AlternateText="Go" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
