﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AuctionInvitation.ascx.vb"
    Inherits="Auctions_UserControls_AuctionInvitation" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
    Skin="Simple" />
<telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
    <script type="text/javascript">
        var _invitation;
        function populate() {

            w1 = window.open('/Auctions/SelectFromInvCriteria.aspx?i=<%=Request("i") %>', '_populateCriteria', 'top=' + ((screen.height - 400) / 2) + ', left=' + ((screen.width - 500) / 2) + ', height=400, width=500,scrollbars=no,toolbars=no,resizable=1;');
            w1.focus();
            return false;

        }
    </script>
</telerik:RadScriptBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
    CssClass="TabGrid">
    <%--<div style="color: Red;">
        <i>Note : If selected list is empty, system will assume all active bidders are invited.</i>
    </div>--%>
    <table cellpadding="0" cellspacing="0" border="0" width="950">
        <tr>
            <td style="width: 450px;" class="invSubHeading">
                Available Options
            </td>
            <td style="width: 50px;">
                &nbsp;
            </td>
            <td style="width: 450px;" class="invSubHeading">
                Selected Options<span class="invLink"><asp:Literal ID="lit_no_of_bidder" runat="server"></asp:Literal></span>
            </td>
        </tr>
        <tr>
            <td class="greenBackSmall">
                <div style="float: left; padding-top: 3px;">
                    <asp:RadioButton ID="RDO_filter_type1" Checked="true" AutoPostBack="true" runat="server"
                        Text="Buckets" TextAlign="Right" GroupName="grpType" />
                </div>
                <div style="float: left; padding-left: 20px; padding-top: 3px;">
                    <asp:RadioButton ID="RDO_filter_type2" AutoPostBack="true" runat="server" Text="Bidders"
                        TextAlign="Right" GroupName="grpType" />
                </div>
            </td>
            <td valign="top">
                &nbsp;
            </td>
            <td class="greenBackSmall">
                <asp:LinkButton ID="lnk_populate" runat="server" Text="Populate from available template"
                    OnClientClick="javascript:return populate();"></asp:LinkButton>
                <%-- <asp:DropDownList ID="ddl_invitation" runat="server" DataValueField="invitation_id"
                    DataTextField="name" AutoPostBack="true">
                </asp:DropDownList>--%>
                <asp:HiddenField ID="HID_invitation_id" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px; border-left: 1px solid #E4E4E5; border-right: 1px solid #E4E4E5;
                border-bottom: 1px solid #E4E4E5;" valign="top">
            </td>
            <td>
                &nbsp;
            </td>
            <td style="padding: 5px; border-left: 1px solid #E4E4E5; border-right: 1px solid #E4E4E5;
                border-bottom: 1px solid #E4E4E5; background-color: #F6F6F6;" valign="top">
                <div style="margin-top: 5px;" id="div_save" runat="server">
                    <table cellpadding="5">
                        <tr>
                            <td colspan="3">
                                <asp:Label ID="lbl_msg" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Save this criteria as template
                            </td>
                            <td>
                                <asp:HiddenField ID="HID_invitation_name" runat="server" Value="" />
                                <asp:TextBox ID="txt_invitation_name" runat="server" CssClass="TextBox" Width="100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfv_invitation_name" runat="server" ControlToValidate="txt_invitation_name"
                                    ValidationGroup="_invitation" Display="Dynamic" ErrorMessage="*" CssClass="error"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:ImageButton ID="btn_save_filter" ValidationGroup="_invitation" runat="server"
                                    AlternateText="Save" ImageUrl="/images/save.gif" />
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px; border-left: 1px solid #E4E4E5; border-right: 1px solid #E4E4E5;"
                valign="top">
                <table width="100%" cellpadding="5" cellspacing="0" border="0">
                    <tr>
                        <td valign="top">
                            <asp:Panel ID="pnl_type1" runat="server" Height="430" ScrollBars="Vertical">
                                <telerik:RadGrid ID="RadGrid_SelectedBucketlist_left" GridLines="None" runat="server"
                                    AllowPaging="False" AutoGenerateColumns="False" AllowMultiRowSelection="false"
                                    Skin="Vista" AllowFilteringByColumn="false" ShowHeader="false" ShowFooter="false"
                                    DataSourceID="SqlDataSource4">
                                    <MasterTableView DataKeyNames="bucket_value_id" HorizontalAlign="NotSet" AutoGenerateColumns="False">
                                        <NoRecordsTemplate>
                                            Available Bucket has been selected
                                        </NoRecordsTemplate>
                                        <Columns>
                                            <telerik:GridTemplateColumn>
                                                <ItemTemplate>
                                                    <asp:Literal ID="lit_bucket_id" runat="server" Text='<%# Eval("bucket_id") %>' Visible="false"></asp:Literal>
                                                    <asp:Literal ID="lit_bucket_value_id" runat="server" Text='<%# Eval("bucket_value_id") %>'
                                                        Visible="false"></asp:Literal>
                                                    <asp:Literal ID="lit_bucket" runat="server" Text='<%# Eval("bucket_name") %>'></asp:Literal>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chk" runat="server" Text='<%#Eval("bucket_value") %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                    ProviderName="System.Data.SqlClient" SelectCommand="select B.bucket_name,V.bucket_id,V.bucket_value_id,V.bucket_value from tbl_master_buckets B inner join tbl_master_bucket_values V on B.bucket_id=V.bucket_id where B.is_active=1 and V.is_active=1 and V.bucket_value_id not in (select isnull(bucket_value_id,0) from tbl_auction_invitation_filter_values where auction_id=@auction_id) order by B.sno,V.bucket_value_id">
                                    <SelectParameters>
                                        <asp:QueryStringParameter Name="auction_id" QueryStringField="i" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:Panel>
                            <asp:Panel ID="pnl_type2" runat="server" Height="430" ScrollBars="Vertical" Visible="false">
                                <telerik:RadGrid ID="RadGrid_buyerlist" GridLines="None" runat="server" AllowPaging="False"
                                    AutoGenerateColumns="False" DataSourceID="SqlDataSource6" AllowMultiRowSelection="true"
                                    Skin="Vista" AllowFilteringByColumn="True" Width="400">
                                    <MasterTableView DataKeyNames="buyer_id" DataSourceID="SqlDataSource6" HorizontalAlign="NotSet"
                                        AutoGenerateColumns="False" AllowFilteringByColumn="True" AllowSorting="true">
                                        <HeaderStyle BackColor="#BEBEBE" />
                                        <SortExpressions>
                                            <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                                        </SortExpressions>
                                        <NoRecordsTemplate>
                                            Available Bidder has been selected
                                        </NoRecordsTemplate>

                                        <Columns>
                                            <telerik:GridTemplateColumn HeaderText="Select" Groupable="true" AllowFiltering="false" UniqueName="CheckBoxTemplateColumn">
                                                <HeaderTemplate>
                                                  <asp:CheckBox ID="headerChkbox" runat="server" OnCheckedChanged="ToggleSelectedState" AutoPostBack="True" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chk_select" runat="server" />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="name" HeaderText="Contact Person" SortExpression="name"
                                                UniqueName="name" CurrentFilterFunction="StartsWith" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn HeaderText="Company" SortExpression="company_name" ShowSortIcon="true"
                                                UniqueName="company_name" DataField="company_name" ShowFilterIcon="false" FilterControlWidth="80"
                                                CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="true" GroupByExpression="company_name [Company] group by company_name">
                                                <ItemTemplate>
                                                    <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');"
                                                        onclick="return open_buyer('/Bidders/AddEditBuyer.aspx','<%# Eval("buyer_id") %>');">
                                                        <%# Eval("company_name")%></a>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Email Address" UniqueName="email" DataField="email"
                                                FilterControlWidth="80" SortExpression="email" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                                AutoPostBackOnFilter="true" GroupByExpression="email [Email Address] group by email">
                                                <ItemTemplate>
                                                    <a href="mailto:<%# Eval("email")%>">
                                                        <%# Eval("email")%></a>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                                <asp:HiddenField ID="hid_reserve_price" runat="server" Value="0" />
                                <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                    ProviderName="System.Data.SqlClient" SelectCommand="select B.contact_first_name +' ' + B.contact_last_name as name,B.buyer_id, B.company_name, B.email from tbl_reg_buyers B WITH (NOLOCK) inner join tbl_reg_buyer_seller_mapping C on B.buyer_id=C.buyer_id inner join tbl_auctions A WITH (NOLOCK) on C.seller_id=A.seller_id and A.auction_id=@auction_id where B.status_id=2 and (B.max_amt_bid >@reserve_price or B.max_amt_bid=0) and B.buyer_id not in (select isnull(buyer_id,0) from tbl_auction_invitation_filter_values where auction_id=@auction_id)">
                                    <SelectParameters>
                                        <asp:QueryStringParameter Name="auction_id" QueryStringField="i" />
                                        <asp:ControlParameter Name="reserve_price" ControlID="hid_reserve_price" PropertyName="value"
                                            Type="Double" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="middle" align="center" style="padding: 5px;">
                <asp:ImageButton ID="btn_add_filter" runat="server" AlternateText="Save" ImageUrl="/images/add_filter.gif" />
            </td>
            <td style="padding: 5px; border-left: 1px solid #E4E4E5; border-right: 1px solid #E4E4E5;"
                valign="top">
                <asp:Panel ID="pnl_invitaion_data" runat="server">
                    <div id="div_opt_1" runat="server" visible='False'>
                        <div class="greenBackSmall">
                            <div style="padding-top: 8px;">
                                Bucket</div>
                        </div>
                        <div class="greenBackSmall_down">
                            <asp:HiddenField ID="invitation_filter_id1" runat="server" Value="0" />
                            <telerik:RadGrid ID="RadGrid_SelectedBucketlist" GridLines="None" runat="server"
                                AllowAutomaticDeletes="True" AllowPaging="False" AutoGenerateColumns="False"
                                AllowMultiRowSelection="false" DataSourceID="SqlDataSource2" Skin="Vista" AllowFilteringByColumn="false"
                                ShowHeader="false" ShowFooter="false">
                                <MasterTableView DataKeyNames="invitation_filter_value_id" HorizontalAlign="NotSet"
                                    AutoGenerateColumns="False" DataSourceID="SqlDataSource2">
                                    <NoRecordsTemplate>
                                        <div style="height: 19px; padding-top: 3px; padding-left: 5px;">
                                            Bucket not selected</div>
                                    </NoRecordsTemplate>
                                    <Columns>
                                        <telerik:GridTemplateColumn HeaderText="Bucket" UniqueName="bucket_name">
                                            <ItemTemplate>
                                            <asp:Literal ID="lit_bucket_id" runat="server" Text='<%# Eval("bucket_id") %>' Visible="false"></asp:Literal>
                                                <asp:Literal ID="lit_bucket" runat="server" Text='<%# Eval("bucket_name") %>'></asp:Literal>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="bucket_value" HeaderText="Option" UniqueName="bucket_value">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridButtonColumn ConfirmText="Delete this bucket?" ConfirmDialogType="RadWindow"
                                            ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                        </telerik:GridButtonColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                ProviderName="System.Data.SqlClient" SelectCommand="select A.invitation_filter_value_id,B.bucket_name,V.bucket_id,V.bucket_value from tbl_auction_invitation_filter_values A  inner join tbl_master_bucket_values V on A.bucket_value_id=V.bucket_value_id inner join tbl_master_buckets B on V.bucket_id=B.bucket_id where A.buyer_id=0 and A.invitation_filter_id= @invitation_filter_id"
                                DeleteCommand="DELETE FROM [tbl_auction_invitation_filter_values] WHERE [invitation_filter_value_id] = @invitation_filter_value_id">
                                <SelectParameters>
                                    <asp:ControlParameter Name="invitation_filter_id" Type="Int32" ControlID="invitation_filter_id1"
                                        PropertyName="Value" />
                                </SelectParameters>
                                <DeleteParameters>
                                    <asp:Parameter Name="invitation_filter_value_id" Type="Int32" />
                                </DeleteParameters>
                            </asp:SqlDataSource>
                        </div>
                    </div>
                    <div id="div_opt_2" runat="server" visible='False' style="padding-top: 10px;">
                        <div class="greenBackSmall">
                            <div style="padding-top: 8px;">
                                Bidders</div>
                        </div>
                        <div class="greenBackSmall_down">
                            <asp:HiddenField ID="invitation_filter_id2" runat="server" Value="0" />
                            <telerik:RadGrid ID="RadGrid_SelectedBidderlist" GridLines="None" runat="server"
                                AllowAutomaticDeletes="True" AllowPaging="False" AutoGenerateColumns="False"
                                AllowMultiRowSelection="false"  Skin="Vista" AllowFilteringByColumn="false"
                                ShowHeader="false" ShowFooter="false">
                                <MasterTableView DataKeyNames="invitation_filter_value_id" HorizontalAlign="NotSet"
                                    AutoGenerateColumns="False" DataSourceID="SqlDataSource3">
                                    <NoRecordsTemplate>
                                        <div style="height: 19px; padding-top: 3px; padding-left: 5px;">
                                            Bidder not selected</div>
                                    </NoRecordsTemplate>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="invitation_filter_value_id" HeaderText="invitation_filter_value_id"
                                            Visible="false" UniqueName="invitation_filter_value_id">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="name" HeaderText="Contact Person" SortExpression="name"
                                            UniqueName="name">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="company_name" HeaderText="company_name" UniqueName="company_name">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="email" HeaderText="email" UniqueName="email">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridButtonColumn ConfirmText="Delete this bidder?" ConfirmDialogType="RadWindow"
                                            ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                        </telerik:GridButtonColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                ProviderName="System.Data.SqlClient" SelectCommand="select A.invitation_filter_value_id,B.contact_first_name +' ' + B.contact_last_name as name,B.company_name,B.email from tbl_auction_invitation_filter_values A join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id where A.bucket_id=0 and A.invitation_filter_id= @invitation_filter_id"
                                DeleteCommand="DELETE FROM [tbl_auction_invitation_filter_values] WHERE [invitation_filter_value_id] = @invitation_filter_value_id">
                                <SelectParameters>
                                    <asp:ControlParameter Name="invitation_filter_id" Type="Int32" ControlID="invitation_filter_id2"
                                        PropertyName="Value" />
                                </SelectParameters>
                                <DeleteParameters>
                                    <asp:Parameter Name="invitation_filter_value_id" Type="Int32" />
                                </DeleteParameters>
                            </asp:SqlDataSource>
                        </div>
                    </div>
                    <div style="background-color: White; margin-top: 10px;" id="div_item_not" runat="server">
                        <table cellpadding="5">
                            <tr>
                                <td>
                                    No Item Selected.
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>
