﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AuctionItems.ascx.vb"
    Inherits="Auctions_UserControls_AuctionItems" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" 
    Skin="Simple" />
<asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
<asp:HiddenField ID="hid_user_id" runat="server" Value="0" />
<asp:HiddenField ID="hid_is_bid_over" runat="server" Value="0" />
<asp:HiddenField ID="hid_language" runat="server" Value="1" />
<input type="hidden" id="hid_manufacturer_combo_id" name="hid_manufacturer_combo_id"
    value="0" />
<input type="hidden" id="hid_location_combo_id" name="hid_location_combo_id" value="0" />
<input type="hidden" id="hid_packaging_combo_id" name="hid_packaging_combo_id" value="0" />
<input type="hidden" id="hid_condition_combo_id" name="hid_condition_combo_id" value="0" />
<asp:TextBox ID="txt_test" runat="server" Visible="false"></asp:TextBox>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
    left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
    <img id="Image8" src="/images/img_loading.gif" alt="" />
</telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="pnl1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1" ClientEvents-OnRequestStart="Cancel_Ajax">
  <asp:Button ID="but_bind_items" runat="server" Enabled="true" BorderStyle="None"
                                                        BackColor="Transparent" ForeColor="Transparent" Width="0" Height="0" />
    <telerik:RadGrid ID="RadGrid_Items" GridLines="None" runat="server" AllowAutomaticDeletes="True"
        AllowAutomaticInserts="True" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True"
        AllowSorting="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
        AllowMultiRowSelection="false" AllowMultiRowEdit="false" Skin="Vista" ShowGroupPanel="true">
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle BackColor="#BEBEBE" />
        <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="product_item_id"
            DataSourceID="SqlDataSource1" HorizontalAlign="NotSet" AutoGenerateColumns="False"
            SkinID="Vista" ItemStyle-Height="40" AlternatingItemStyle-Height="40">
            <CommandItemStyle BackColor="#E1DDDD" />
            <NoRecordsTemplate>
                Auction items not available
            </NoRecordsTemplate>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="manufacturer" SortOrder="Ascending" />
            </SortExpressions>
            <CommandItemSettings AddNewRecordText="Add New Auction Item" />
            <Columns>
                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                    EditImageUrl="/Images/edit_grid.gif">
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                </telerik:GridEditCommandColumn>
                <telerik:GridTemplateColumn HeaderText="Product Title" SortExpression="manufacturer"
                    UniqueName="manufacturer" DataField="manufacturer" GroupByExpression="manufacturer [Product Title] Group By manufacturer"
                    EditFormHeaderTextFormat="Manufacturer">
                    <ItemTemplate>
                        <%# Eval("manufacturer")%>&nbsp;&nbsp;<%# Eval("name")%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Part No" SortExpression="part_no" UniqueName="part_no"
                    HeaderStyle-Width="150px" DataField="part_no" GroupByExpression="part_no [Part No] Group By part_no"
                    EditFormHeaderTextFormat="Part No">
                    <ItemTemplate>
                        <asp:Label ID="lbl_part_no" runat="server" Text='<%#Bind("part_no") %>' />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Quantity" SortExpression="quantity" UniqueName="quantity"
                    HeaderStyle-Width="100px" DataField="quantity" GroupByExpression="quantity [Quantity] Group By quantity">
                    <ItemTemplate>
                        <asp:Label ID="lbl_quantity" runat="server" Text='<%#Bind("quantity") %>' />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Estimated MSRP" SortExpression="estimated_msrp" UniqueName="estimated_msrp" DataField="estimated_msrp" Groupable="false"  ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="lbl_estimated_msrp" runat="server" Text='<%#Bind("estimated_msrp") %>' />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Total MSRP" SortExpression="total_msrp" UniqueName="total_msrp" DataField="total_msrp" Groupable="false" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" >
                    <ItemTemplate>
                        <asp:Label ID="lbl_total_msrp" runat="server" Text='<%#Bind("total_msrp") %>' />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"
                    ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                    CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                </telerik:GridButtonColumn>
            </Columns>
            <EditFormSettings InsertCaption="New Items" EditFormType="Template">
                <FormTemplate>
                    <table id="tbl_grd_item_edit" cellpadding="0" cellspacing="2" border="0" width="800px">
                        <tr>
                            <td style="font-weight: bold; padding-top: 10px;" colspan="4">
                                Item Details
                            </td>
                        </tr>
                        <tr>
                            <td class="caption" style="width: 135px;">
                                Manufacturer&nbsp;<span class="req_star">*</span>
                            </td>
                            <td style="width: 260px;" class="details">
                                <telerik:RadComboBox ID="dd_grd_manufacturer" runat="server" OnClientLoad="set_manu_dropid"
                                    Height="200px" Width="200px" OnItemsRequested="dd_grd_manufacturer_ItemsRequested"
                                    SelectedValue='<%# Bind("manufacturer_id") %>' EmptyMessage="--Manufacturer--"
                                    MarkFirstMatch="true" EnableLoadOnDemand="true" DataSourceID="DataSource_Manufacturer"
                                    DataTextField="name" DataValueField="manufacturer_id" AllowCustomText="true">
                                </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="Requ_dd_manufacturer" runat="server" Font-Size="10px"
                                    ControlToValidate="dd_grd_manufacturer" ValidationGroup="grd_items" Display="Dynamic"
                                    ErrorMessage="<br>Manufacturer Required"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="CustomValidate_Manufacturer" runat="server" ClientValidationFunction="validate_manufacturer"
                                    ErrorMessage="<br />Please select manufacturer fromt the list." ControlToValidate="dd_grd_manufacturer"
                                    ValidationGroup="grd_items" Font-Size="10px" Display="Dynamic">
                                </asp:CustomValidator>
                            </td>
                            <td class="caption" style="width: 135px;">
                                Title&nbsp;<span class="req_star">*</span>
                            </td>
                            <td style="width: 260px;" class="details">
                                <asp:TextBox ID="txt_grd_name" runat="server" Text='<%#Bind("name") %>' CssClass="inputtype" />
                                <asp:RequiredFieldValidator ID="Req_txt_grd_name" runat="server" Font-Size="10px"
                                    ValidationGroup="grd_items" ControlToValidate="txt_grd_name" Display="Dynamic"
                                    ErrorMessage="<br>Name Required"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                Part no.
                            </td>
                            <td class="details">
                                <asp:TextBox ID="txt_grd_part_no" runat="server" Text='<%#Bind("part_no") %>' CssClass="inputtype" />
                            </td>
                                <td class="caption">
                                Quantity
                            </td>
                            <td class="details">
                                <asp:TextBox ID="txt_grd_quantity" runat="server" Text='<%#Bind("quantity") %>' CssClass="inputtype" />
                                <asp:RegularExpressionValidator ID="Reg_txt_grd_quantity" runat="server" Font-Size="10px"
                                    ControlToValidate="txt_grd_quantity" ValidationExpression="[0-9]{1,10}" ValidationGroup="grd_items"
                                    Display="Dynamic" ErrorMessage="<br>Wrong Quantity"></asp:RegularExpressionValidator>
                            </td>
                     
                            </tr>
                        <tr>
                               <tr>
                            <td class="caption">
                                UPC
                            </td>
                            <td class="details">
                                <asp:TextBox ID="txt_grd_upc" runat="server" Text='<%#Bind("upc") %>' CssClass="inputtype" />
                            </td>
                            <td class="caption">
                                SKU
                            </td>
                            <td class="details">
                                <asp:TextBox ID="txt_grd_sku" runat="server" Text='<%#Bind("sku") %>' CssClass="inputtype" />
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                Estimated MSRP
                            </td>
                            <td class="details">
                                <asp:TextBox ID="txt_grd_estimated_msrp" runat="server" Text='<%# Bind("estimated_msrp") %>' CssClass="inputtype" />
                                <asp:RegularExpressionValidator ID="Reg_txt_grd_estimated_msrp" runat="server" Font-Size="10px"
                                    ControlToValidate="txt_grd_estimated_msrp" ValidationExpression="^\d{1,8}(?:\.\d{1,4})?$" ValidationGroup="grd_items"
                                    Display="Dynamic" ErrorMessage="<br>Wrong MSRP"></asp:RegularExpressionValidator>
                            </td>
                            <td class="caption">
                                Total MSRP
                            </td>
                            <td class="details">
                                <asp:TextBox ID="txt_grd_total_msrp" runat="server" Text='<%#Bind("total_msrp") %>' CssClass="inputtype" />
                                <asp:RegularExpressionValidator ID="Reg_txt_grd_total_msrp" runat="server" Font-Size="10px"
                                    ControlToValidate="txt_grd_total_msrp" ValidationExpression="^\d{1,8}(?:\.\d{1,4})?$" ValidationGroup="grd_items"
                                    Display="Dynamic" ErrorMessage="<br>Wrong MSRP"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                            <td class="caption">
                                Location
                            </td>
                            <td class="details">
                                <telerik:RadComboBox ID="dd_grd_location" runat="server" OnClientLoad="set_loca_dropid"
                                    Height="200px" Width="200px" DropDownWidth="300px" EmptyMessage="--Stock Location--"
                                    HighlightTemplatedItems="true" OnItemDataBound="dd_grd_location_ItemDataBound"
                                    EnableLoadOnDemand="true" Filter="StartsWith" OnItemsRequested="dd_grd_location_ItemsRequested"
                                    DataSourceID="DataSource_Location" AllowCustomText="true" DataValueField="stock_location_id"
                                    SelectedValue='<%# Bind("stock_location_id" ) %>'>
                                    <HeaderTemplate>
                                        <table style="width: 290px;" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="width: 100px;">
                                                    Name
                                                </td>
                                                <td style="width: 75px;">
                                                    City
                                                </td>
                                                <td style="width: 85px;">
                                                    State
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table style="width: 290px;" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="width: 100px;">
                                                    <%# DataBinder.Eval(Container.DataItem, "Name")%>
                                                </td>
                                                <td style="width: 75px;">
                                                    <%# DataBinder.Eval(Container.DataItem, "city")%>
                                                </td>
                                                <td style="width: 85px;">
                                                    <%# DataBinder.Eval(Container.DataItem, "state")%>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </telerik:RadComboBox>
                                <asp:Label ID="lbl_error_loc" runat="server" Font-Size="10px" ForeColor="Red"></asp:Label>
                                <asp:CustomValidator ID="CustomValidate_Location" runat="server" ClientValidationFunction="validate_location"
                                    ErrorMessage="<br />Please select location fromt the list." ControlToValidate="dd_grd_location"
                                    ValidationGroup="grd_items" Font-Size="10px" Display="Dynamic">
                                </asp:CustomValidator>
                               </td>
                           <td class="caption">
                                Packaging
                            </td>
                            <td class="details">
                                <telerik:RadComboBox ID="dd_grd_packaging" runat="server" OnClientLoad="set_pack_dropid"
                                    Height="200px" Width="200px" OnItemsRequested="dd_grd_packaging_ItemsRequested"
                                    SelectedValue='<%# Bind("packaging_id") %>' EmptyMessage="--Packaging--" MarkFirstMatch="true"
                                    EnableLoadOnDemand="true" DataSourceID="DataSource_Packaging" DataTextField="name"
                                    DataValueField="packaging_id" AllowCustomText="true">
                                </telerik:RadComboBox>
                                <asp:CustomValidator ID="CustomValidate_Packaging" runat="server" ClientValidationFunction="validate_packaging"
                                    ErrorMessage="<br />Please select packaging fromt the list." ControlToValidate="dd_grd_packaging"
                                    ValidationGroup="grd_items" Font-Size="10px" Display="Dynamic">
                                </asp:CustomValidator>
                                <%--<asp:DropDownList ID="dd_grd_packaging" runat="server" DataTextField="name" DataValueField="packaging_id"
                                SelectedValue='<%# Bind("packaging_id") %>' DataSourceID="DataSource_Packaging"
                                CssClass="inputtype" AppendDataBoundItems="True">
                                <asp:ListItem Selected="True" Text="--Packaging--" Value=""></asp:ListItem>
                            </asp:DropDownList>--%>
                            </td>
                                 </tr>
                        <tr>
                    <td class="caption" style="width: 145px;">
                                Stock Condition
                            </td>
                            <td class="details">
                                <telerik:RadComboBox ID="dd_grd_condition" runat="server" OnClientLoad="set_cond_dropid"
                                    Height="200px" Width="200px" OnItemsRequested="dd_grd_condition_ItemsRequested"
                                    SelectedValue='<%# Bind("stock_condition_id") %>' EmptyMessage="--Stock Condition--"
                                    MarkFirstMatch="true" EnableLoadOnDemand="true" DataSourceID="DataSource_Condition"
                                    DataTextField="name" DataValueField="stock_condition_id" AllowCustomText="true">
                                </telerik:RadComboBox>
                                <asp:CustomValidator ID="CustomValidate_Condition" runat="server" ClientValidationFunction="validate_condition"
                                    ErrorMessage="<br />Please select condition fromt the list." ControlToValidate="dd_grd_condition"
                                    ValidationGroup="grd_items" Font-Size="10px" Display="Dynamic">
                                </asp:CustomValidator>
                                <%--<asp:DropDownList ID="dd_grd_condition" runat="server" DataTextField="name" DataValueField="stock_condition_id"
                                SelectedValue='<%# Bind("stock_condition_id") %>' DataSourceID="DataSource_Condition"
                                CssClass="inputtype" AppendDataBoundItems="True">
                                <asp:ListItem Selected="True" Text="--Condition--" Value=""></asp:ListItem>
                            </asp:DropDownList>--%>
                            </td>
                              <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                         <tr>
                            <td class="caption">
                                Description
                            </td>
                            <td class="details">
                                <asp:TextBox ID="txt_grd_description" runat="server" TextMode="MultiLine" Height="50px"
                                    Text='<%#Bind("description") %>' CssClass="inputtype" />
                            </td>
                            <td class="caption">
                                Comment
                            </td>
                            <td class="details">
                                <asp:TextBox ID="txt_grd_comment" runat="server" Text='<%#Bind("comment") %>' TextMode="MultiLine"
                                    Height="50px" CssClass="inputtype" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td align="right" colspan="2">
                                <asp:ImageButton ID="but_grd_submit" CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem),"PerformInsert","Update") %>'
                                    ValidationGroup="grd_items" OnClientClick="return ValidationGroupEnable('grd_items', true);"
                                    AlternateText='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "Save" , "Update") %>'
                                    runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif" , "/images/update.gif") %>' />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="but_grd_cancel" CausesValidation="false" CommandName="Cancel"
                                    runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
                <%--       <FormTableItemStyle Wrap="False"></FormTableItemStyle>
            <FormCaptionStyle CssClass="EditFormHeader" Font-Bold="true"></FormCaptionStyle>
            <FormMainTableStyle GridLines="None" CellSpacing="0" CellPadding="3" BackColor="White"
                Width="100%" />
            <FormTableStyle CellSpacing="0" CellPadding="2" Height="110px" BackColor="White" />
            <FormTableAlternatingItemStyle Wrap="False"></FormTableAlternatingItemStyle>
            <EditColumn ButtonType="ImageButton" InsertText="Insert Order" UpdateText="Update record"
                UniqueName="EditCommandColumn1" CancelText="Cancel edit" CancelImageUrl="/images/cancel.gif"
                InsertImageUrl="/images/save.gif" UpdateImageUrl="/images/update.gif">
            </EditColumn>
            <FormTableButtonRowStyle HorizontalAlign="Right" CssClass="EditFormButtonRow"></FormTableButtonRowStyle>--%>
            </EditFormSettings>
        </MasterTableView>
        <ClientSettings AllowDragToGroup="true">
            <Selecting AllowRowSelect="True"></Selecting>
        </ClientSettings>
        <GroupingSettings ShowUnGroupButton="true" />
    </telerik:RadGrid>
    
</telerik:RadAjaxPanel>
<%--<telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditor1" runat="server" TextBoxStyle-Width="150px" />
<telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditor2" runat="server" TextBoxStyle-Width="150px" />
<telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditor3" runat="server" TextBoxStyle-Width="240px"
    TextBoxMode="MultiLine" TextBoxStyle-Height="70px" />

<telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditor5" runat="server" TextBoxStyle-Width="150px" />
<telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditor6" runat="server" TextBoxStyle-Width="150px"
    TextBoxMode="MultiLine" TextBoxStyle-Height="35" />
<telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditor1" runat="server"
    DropDownStyle-Width="244px" />
<telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditor2" runat="server"
    DropDownStyle-Width="154px" />
    <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditor3" runat="server"
    DropDownStyle-Width="154px" />
    <telerik:GridDropDownListColumnEditor ID="GridDropDownColumnEditor4" runat="server"
    DropDownStyle-Width="154px" />--%>
<%--<telerik:GridNumericColumnEditor ID="GridNumericColumnEditor1" runat="server" NumericTextBox-Width="152px" />--%>
         
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
    ProviderName="System.Data.SqlClient" 
    SelectCommand="select P.[product_item_id],P.[auction_id],isnull(
    case @language_id when 2 then P.[part_no_f] when 3 then [part_no_s] when 4 then [part_no_c] else [part_no] End,'') AS part_no,
    isnull(case @language_id when 2 then P.[description_f] when 3 then P.[description_s] when 4 then P.[description_c] else P.[description] End,'') AS description,
    [stock_condition_id],[packaging_id],M.manufacturer_id,P.stock_location_id,
    isnull(case @language_id when 2 then P.[name_f] when 3 then P.[name_s] when 4 then P.[name_c] else P.[name] End,'') AS name,
    isnull(case @language_id when 2 then [comment_f] when 3 then [comment_s] when 4 then [comment_c] else [comment] End,'') AS comment,isnull(M.name,'') as manufacturer,
    [quantity],isnull(P.upc,'') as upc,isnull(P.sku,'') as sku,isnull(CONVERT(DECIMAL(10,2),P.estimated_msrp),0) as estimated_msrp,isnull(CONVERT(DECIMAL(10,2),P.total_msrp),0.0) as total_msrp 
    from [tbl_auction_product_items] P left join tbl_master_manufacturers M on P.manufacturer_id=M.manufacturer_id where auction_id= @auction_id"
    DeleteCommand="DELETE FROM [tbl_auction_product_items] WHERE [product_item_id] = @product_item_id"
    InsertCommand="INSERT INTO tbl_auction_product_items(auction_id, part_no,
    part_no_f,part_no_s,part_no_c, description,description_f,description_s,description_c, stock_condition_id, packaging_id,
    name,name_f,name_s,name_c, comment,comment_f,comment_s,comment_c, quantity, submit_date, submit_by_user_id,manufacturer_id,stock_location_id,upc,sku,estimated_msrp,total_msrp) 
    VALUES (@auction_id,
     case @language_id when 1 then isnull(@part_no,'') else '' end,
     case @language_id when 2 then isnull(@part_no,'') else '' end,
     case @language_id when 3 then isnull(@part_no,'') else '' end,
     case @language_id when 4 then isnull(@part_no,'') else '' end,
     case @language_id when 1 then  replace(isnull(@description,''),CHAR(10),'<br/>') else '' end,
     case @language_id when 2 then  replace(isnull(@description,''),CHAR(10),'<br/>') else '' end,
     case @language_id when 3 then  replace(isnull(@description,''),CHAR(10),'<br/>') else '' end,
     case @language_id when 4 then  replace(isnull(@description,''),CHAR(10),'<br/>') else '' end,
      @stock_condition_id, @packaging_id,
     case @language_id when 1 then isnull(@name,'') else '' end,
     case @language_id when 2 then isnull(@name,'') else '' end,
     case @language_id when 3 then isnull(@name,'') else '' end,
     case @language_id when 4 then isnull(@name,'') else '' end,
     case @language_id when 1 then  replace(isnull(@comment,''),CHAR(10),'<br/>') else '' end,
     case @language_id when 2 then  replace(isnull(@comment,''),CHAR(10),'<br/>') else '' end,
     case @language_id when 3 then  replace(isnull(@comment,''),CHAR(10),'<br/>') else '' end,
     case @language_id when 4 then  replace(isnull(@comment,''),CHAR(10),'<br/>') else '' end,
     isnull(@quantity,0), getdate(), @submit_by_user_id,@manufacturer_id,@stock_location_id,isnull(@upc,''),isnull(@sku,''),isnull(@estimated_msrp,0),isnull(@total_msrp,0))"
    UpdateCommand="UPDATE tbl_auction_product_items SET 
       part_no_f= case @language_id when 2 then @part_no else part_no_f end, 
    part_no_s= case @language_id when 3 then @part_no else part_no_s end, 
    part_no_c= case @language_id when 4 then @part_no else part_no_c end, 
    part_no= case @language_id when 1 then @part_no else part_no end, 
    description_f= case @language_id when 2 then replace(@description,CHAR(10),'<br/>') else description_f end, 
    description_s= case @language_id when 3 then replace(@description,CHAR(10),'<br/>') else description_s end, 
    description_c= case @language_id when 4 then replace(@description,CHAR(10),'<br/>') else description_c end, 
    description= case @language_id when 1 then replace(@description,CHAR(10),'<br/>') else description end, 
    stock_condition_id = @stock_condition_id, packaging_id = @packaging_id,
    name_f= case @language_id when 2 then @name else name_f end, 
    name_s= case @language_id when 3 then @name else name_s end, 
    name_c= case @language_id when 4 then @name else name_c end, 
    name= case @language_id when 1 then @name else comment end, 
    comment_f= case @language_id when 2 then replace(@comment,CHAR(10),'<br/>') else comment_f end, 
    comment_s= case @language_id when 3 then replace(@comment,CHAR(10),'<br/>') else comment_s end, 
    comment_c= case @language_id when 4 then replace(@comment,CHAR(10),'<br/>') else comment_c end, 
    comment= case @language_id when 1 then replace(@comment,CHAR(10),'<br/>') else comment end, 
    quantity = @quantity,manufacturer_id=@manufacturer_id,
    stock_location_id=@stock_location_id,upc=@upc,sku=@sku,estimated_msrp=@estimated_msrp,total_msrp=@total_msrp 
       
     WHERE product_item_id = @product_item_id">
    <SelectParameters>
        <asp:ControlParameter Name="auction_id" Type="Int32" ControlID="hid_auction_id" PropertyName="Value" />
        <asp:ControlParameter Name="language_id" Type="Int32" ControlID="hid_language" PropertyName="Value" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="product_item_id" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:ControlParameter Name="language_id" Type="Int32" ControlID="hid_language" PropertyName="Value" />
        <asp:Parameter Name="part_no" Type="String" />
        <asp:Parameter Name="description" Type="String" />
        <asp:Parameter Name="stock_condition_id" Type="Int32" />
        <asp:Parameter Name="packaging_id" Type="Int32" />
        <asp:Parameter Name="name" Type="String" />
        <asp:Parameter Name="comment" Type="String" />
        <asp:Parameter Name="quantity" Type="Int32" />
        <asp:Parameter Name="product_item_id" Type="Int32" />
        <asp:Parameter Name="manufacturer_id" Type="Int32" />
        <asp:Parameter Name="stock_location_id" Type="Int32" />
        <asp:Parameter Name="upc" Type="String" />
        <asp:Parameter Name="sku" Type="String" />
        <asp:Parameter Name="estimated_msrp" Type="Double" />
        <asp:Parameter Name="total_msrp" Type="Double" />
    </UpdateParameters>
    <InsertParameters>
        <asp:ControlParameter Name="language_id" Type="Int32" ControlID="hid_language" PropertyName="Value" />
        <asp:Parameter Name="part_no" Type="String" />
        <asp:Parameter Name="description" Type="String" />
        <asp:Parameter Name="stock_condition_id" Type="Int32" />
        <asp:Parameter Name="packaging_id" Type="Int32" />
        <asp:Parameter Name="name" Type="String" />
        <asp:Parameter Name="comment" Type="String" />
        <asp:Parameter Name="quantity" Type="Int32" />
        <asp:Parameter Name="upc" Type="String" />
        <asp:Parameter Name="sku" Type="String" />
        <asp:Parameter Name="estimated_msrp" Type="Double" />
        <asp:Parameter Name="total_msrp" Type="Double" />
        <asp:ControlParameter Name="auction_id" Type="Int32" ControlID="hid_auction_id" PropertyName="Value" />
        <asp:ControlParameter Name="submit_by_user_id" Type="Int32" ControlID="hid_user_id"
            PropertyName="Value" />
        <asp:Parameter Name="manufacturer_id" Type="Int32" />
        <asp:Parameter Name="stock_location_id" Type="Int32" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="DataSource_Condition" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT [stock_condition_id], isnull([name],'') as [name] FROM [tbl_master_stock_conditions] order by name">
</asp:SqlDataSource>
<asp:SqlDataSource ID="DataSource_Packaging" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT [packaging_id], isnull([name],'') as [name] FROM [tbl_master_packaging] order by name">
</asp:SqlDataSource>
<asp:SqlDataSource ID="DataSource_Manufacturer" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT [manufacturer_id], isnull([name],'') as [name] from tbl_master_manufacturers ORDER BY name">
</asp:SqlDataSource>
<asp:SqlDataSource ID="DataSource_Location" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT [stock_location_id], isnull([name],'') as [Name], isnull([city],'') as [city],isnull([state],'') as [state] from tbl_master_stock_locations ORDER BY name">
</asp:SqlDataSource>
<script type="text/javascript">
            function Cancel_Ajax(sender, args) {

                    //args.set_enableAjax(false);
            }
    function validate_location(source, args) {
        args.IsValid = false;
        var combo = $find(document.getElementById('hid_location_combo_id').value);
        //alert(combo);
        var text = combo.get_text();
        if (text.length < 1) {
            args.IsValid = false;
        }
        else {
            var node = combo.findItemByText(text);
            if (node) {
                var value = node.get_value();
                if (value.length > 0) {
                    args.IsValid = true;
                }
            }
            else {
                args.IsValid = false;
            }
        }
    }
    function validate_packaging(source, args) {
        args.IsValid = false;

        var combo = $find(document.getElementById('hid_packaging_combo_id').value);

        var text = combo.get_text();
        if (text.length < 1) {
            args.IsValid = false;
        }
        else {
            var node = combo.findItemByText(text);
            if (node) {
                var value = node.get_value();
                if (value.length > 0) {
                    args.IsValid = true;
                }
            }
            else {
                args.IsValid = false;
            }
        }
    }
    function validate_manufacturer(source, args) {
        args.IsValid = false;
        var combo = $find(document.getElementById('hid_manufacturer_combo_id').value);
        var text = combo.get_text();
        if (text.length < 1) {
            args.IsValid = false;
        }
        else {
            var node = combo.findItemByText(text);
            if (node) {
                var value = node.get_value();
                if (value.length > 0) {
                    args.IsValid = true;
                }
            }
            else {
                args.IsValid = false;
            }
        }
    }
    function validate_condition(source, args) {
        args.IsValid = false;
        var combo = $find(document.getElementById('hid_condition_combo_id').value);
       // alert(combo);
        var text = combo.get_text();
        if (text.length < 1) {
            args.IsValid = false;
        }
        else {
            var node = combo.findItemByText(text);
            if (node) {
                var value = node.get_value();
                if (value.length > 0) {
                    args.IsValid = true;
                }
            }
            else {
                args.IsValid = false;
            }
        }
    }
    function HasPageValidators() {
        var hasValidators = false;

        try {
            if (Page_Validators.length > 0) {
                hasValidators = true;
            }
        }
        catch (error) {
        }

        return hasValidators;
    }

    function ValidationGroupEnable(validationGroupName, isEnable) {
        if (HasPageValidators()) {
            for (i = 0; i < Page_Validators.length; i++) {
                if (Page_Validators[i].validationGroup == validationGroupName) {
                    //ValidatorEnable(Page_Validators[i], isEnable);
                    if (Page_ClientValidate(validationGroupName)) {
                        return true;
                    }
                    else { return false; }
                }
            }
        }
    }
</script>
<telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
    <script type="text/javascript">

        function set_loca_dropid(sender) {

            document.getElementById('hid_location_combo_id').value = sender.get_id();
           // alert(document.getElementById('hid_location_combo_id').value);
        }
        function set_cond_dropid(sender) {

            document.getElementById('hid_condition_combo_id').value = sender.get_id();

        }
        function set_manu_dropid(sender) {

           document.getElementById('hid_manufacturer_combo_id').value = sender.get_id();

        }
        function set_pack_dropid(sender) {

            document.getElementById('hid_packaging_combo_id').value = sender.get_id();
           // alert(document.getElementById('hid_packaging_combo_id').value);

        }
        function PostBackFunction_items() {

            document.getElementById('<%=but_bind_items.ClientID %>').click();
        }
    </script>
</telerik:RadScriptBlock>
