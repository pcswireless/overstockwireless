﻿Imports Telerik.Web.UI
Partial Class Auctions_UserControls_AuctionItems
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Private Sub setPermission()

        If Not CommonCode.is_admin_user() Then
            Dim Edit_Auction As Boolean = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDataTable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))
            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 3
                        Edit_Auction = True
                End Select
            Next
            dt = Nothing

            If Not Edit_Auction Then
                RadGrid_Items.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None
                RadGrid_Items.Columns.FindByUniqueName("EditCommandColumn").Visible = False
                RadGrid_Items.Columns.FindByUniqueName("DeleteColumn").Visible = False
            End If
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
            If Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
                hid_auction_id.Value = Request.QueryString.Get("i")
            End If
            If Not Request.QueryString("lang") Is Nothing Then
                hid_language.Value = Request.QueryString("lang")
            End If
            'txt_test.Text = hid_language.Value
            'set_grid_bid_over_mode()
        End If
    End Sub
#Region "Items"
    Private Sub set_grid_bid_over_mode()
        Dim auction_id As Integer = 0
        If hid_auction_id.Value <> "" Then
            auction_id = hid_auction_id.Value
        End If
        If auction_id <> 0 Then
            Dim strQuery As String = "if((select Convert(varchar,ISNULL(end_date,'1/1/1999'),101) from tbl_auctions WITH (NOLOCK) where auction_id=" & auction_id.ToString() & ")<CONVERT(varchar,GETDATE(),101)) select 1 else select 0"
            Dim is_bid_over As Integer = SqlHelper.ExecuteScalar(strQuery)
            hid_is_bid_over.Value = is_bid_over
            If is_bid_over = 1 Then
                RadGrid_Items.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None
                RadGrid_Items.MasterTableView.Columns(0).Visible = False
                RadGrid_Items.MasterTableView.Columns(10).Visible = False
            End If
        End If
    End Sub

    Protected Sub RadGrid_Items_ItemDeleted(ByVal source As Object, ByVal e As Telerik.Web.UI.GridDeletedEventArgs) Handles RadGrid_Items.ItemDeleted
        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        Dim id As String = item.GetDataKeyValue("product_item_id").ToString()
        Dim Str As String
        If Not e.Exception Is Nothing Then
            e.ExceptionHandled = True
            Str = "Item cannot be deleted. Reason: " + e.Exception.Message
        Else
            Str = "Item is deleted!"
            CommonCode.insert_system_log("Auction Item Deleted", "RadGrid_Items_ItemDeleted", Request.QueryString.Get("i"), "", "Auction")
        End If
        RadGrid_Items.Controls.Add(New LiteralControl(String.Format("<span style='color:red'>" & Str & "</span>")))
    End Sub

    Protected Sub RadGrid_Items_ItemUpdated(ByVal source As Object, ByVal e As Telerik.Web.UI.GridUpdatedEventArgs) Handles RadGrid_Items.ItemUpdated
        Dim item As GridEditableItem = DirectCast(e.Item, GridEditableItem)
        Dim id As String = item.GetDataKeyValue("product_item_id").ToString()
        'For i = 0 To SqlDataSource1.UpdateParameters.Count - 1
        '    Response.Write(hid_manufacturer_combo_id.ValueType & "<br>")
        'Next
        Dim Str As String = ""
        If Not e.Exception Is Nothing Then
            e.KeepInEditMode = True
            e.ExceptionHandled = True
            Str = "Item cannot be updated. Reason: " + e.Exception.StackTrace
        Else
            Str = "Item is updated!"
            CommonCode.insert_system_log("Auction Item Updated", "RadGrid_Items_ItemUpdated", Request.QueryString.Get("i"), "", "Auction")
        End If
        RadGrid_Items.Controls.Add(New LiteralControl(String.Format("<span style='color:red'>" & Str & "</span>")))
    End Sub

    Protected Sub RadGrid_Items_ItemInserted(ByVal source As Object, ByVal e As Telerik.Web.UI.GridInsertedEventArgs) Handles RadGrid_Items.ItemInserted
        Dim Str As String
        txt_test.Text = hid_language.Value
        If Not e.Exception Is Nothing Then
            e.ExceptionHandled = True
            e.KeepInInsertMode = True
            Str = "Item cannot be inserted. Reason: " + e.Exception.Message
        Else
            Str = "New item inserted"
            CommonCode.insert_system_log("Auction Item Inserted", "RadGrid_Items_ItemInserted", Request.QueryString.Get("i"), "", "Auction")
        End If
        RadGrid_Items.Controls.Add(New LiteralControl(String.Format("<span style='color:red'>" & Str & "</span>")))
    End Sub

    Protected Sub RadGrid_Items_ItemCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrid_Items.ItemCommand
       
        If e.CommandName = RadGrid.InitInsertCommandName Then '"Add new" button clicked

            Dim editColumn As GridEditCommandColumn = CType(RadGrid_Items.MasterTableView.GetColumn("EditCommandColumn"), GridEditCommandColumn)
            editColumn.Visible = False

            'ElseIf (e.CommandName = RadGrid.EditCommandName) Then
            '    Dim comb As New RadComboBox
            '    comb = CType(e.Item.FindControl("dd_grd_location"), RadComboBox)
            '    hid_location_combo_id.Value = comb.ClientID
        ElseIf (e.CommandName = RadGrid.RebindGridCommandName AndAlso e.Item.OwnerTableView.IsItemInserted) Then
            e.Canceled = True

        Else
            Dim editColumn As GridEditCommandColumn = CType(RadGrid_Items.MasterTableView.GetColumn("EditCommandColumn"), GridEditCommandColumn)
            If Not editColumn.Visible Then
                editColumn.Visible = True
            End If

        End If

    End Sub
#End Region
    Protected Sub dd_grd_location_ItemsRequested(sender As Object, e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs)
        ' 
        Dim ddl_stock_location As RadComboBox
        ddl_stock_location = DirectCast(sender, RadComboBox)
        Dim sqlSelectCommand As String = "SELECT [stock_location_id], isnull([name],'') as [Name], isnull([city],'') as [city],isnull([state],'') as [state] from tbl_master_stock_locations WHERE name LIKE '" & e.Text & "%' ORDER BY name"
        DataSource_Location.SelectCommand = sqlSelectCommand
        ddl_stock_location.DataSourceID = "DataSource_Location"
       
    End Sub
    Protected Sub dd_grd_location_ItemDataBound(sender As Object, e As Telerik.Web.UI.RadComboBoxItemEventArgs)
        'Response.Write((DirectCast(e.Item.DataItem, DataRowView))("name").ToString())
        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("name").ToString()
        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("stock_location_id").ToString()

    End Sub
  
    Protected Sub dd_grd_condition_ItemsRequested(sender As Object, e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs)

        Dim ddl_stock_condition As RadComboBox
        ddl_stock_condition = DirectCast(sender, RadComboBox)
        Dim sqlSelectCommand As String = "SELECT [stock_condition_id], isnull([name],'') as [name] FROM [tbl_master_stock_conditions] WHERE name LIKE '" & e.Text & "%' ORDER BY name"
        DataSource_Condition.SelectCommand = sqlSelectCommand
        ddl_stock_condition.DataSourceID = "DataSource_Condition"
    End Sub

    Protected Sub dd_grd_packaging_ItemsRequested(sender As Object, e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs)

        Dim ddl_packaging As RadComboBox
        ddl_packaging = DirectCast(sender, RadComboBox)
        Dim sqlSelectCommand As String = "SELECT [packaging_id], isnull([name],'') as [name] FROM [tbl_master_packaging] WHERE name LIKE '" & e.Text & "%' ORDER BY name"
        DataSource_Packaging.SelectCommand = sqlSelectCommand
        ddl_packaging.DataSourceID = "DataSource_Packaging"
    End Sub


    Protected Sub dd_grd_manufacturer_ItemsRequested(sender As Object, e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs)

        Dim ddl_manufacturer As RadComboBox
        ddl_manufacturer = DirectCast(sender, RadComboBox)
        Dim sqlSelectCommand As String = "SELECT [manufacturer_id], isnull([name],'') as [name] from tbl_master_manufacturers WHERE name LIKE '" & e.Text & "%' ORDER BY name"
        DataSource_Manufacturer.SelectCommand = sqlSelectCommand
        ddl_manufacturer.DataSourceID = "DataSource_Manufacturer"
    End Sub

    Protected Sub but_bind_items_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_bind_items.Click
        RadGrid_Items.Rebind()
    End Sub
End Class
