﻿Imports Telerik.Web.UI
Imports System.Drawing
Partial Class Auctions_UserControls_Auction_Comments
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Private Sub setPermission()

        If Not CommonCode.is_super_admin() Then
            Dim Edit_Auction As Boolean = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))
            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 3
                        Edit_Auction = True
                End Select
            Next
            dt = Nothing

            If Not Edit_Auction Then
                RadGrid_Comments.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None
            End If
        End If
    End Sub
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                hid_auction_id.Value = Request.QueryString.Get("i")
                hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
            End If
        End If
    End Sub

    Protected Sub RadGrid_Comments_ItemInserted(ByVal source As Object, ByVal e As Telerik.Web.UI.GridInsertedEventArgs) Handles RadGrid_Comments.ItemInserted
        Dim Str As String

        If Not e.Exception Is Nothing Then
            e.ExceptionHandled = True
            e.KeepInInsertMode = True
            Str = "Item cannot be inserted. Reason : " + e.Exception.Message
        Else
            Str = "New comment inserted"
            CommonCode.insert_system_log("Auction comment Inserted", "RadGrid_Comments_ItemInserted", Request.QueryString.Get("i"), "", "Auction")
        End If
        RadGrid_Comments.Controls.Add(New LiteralControl(String.Format("<span style='color:red'>" & Str & "</span>")))
    End Sub
End Class
