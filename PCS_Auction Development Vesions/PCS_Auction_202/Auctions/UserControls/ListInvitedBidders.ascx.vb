﻿Imports Telerik.Web.UI

Partial Class Auctions_UserControls_ListInvitedBidders
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If IsNumeric(Request.QueryString.Get("j")) AndAlso Request.QueryString.Get("j") > 0 Then
                lit_heading.Text = "Bidder left for Shipping rates"
            End If
        End If
    End Sub
    Private Sub bind_auction_invited_bidders()

    End Sub

    Protected Sub rad_grid_invited_bidders_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles rad_grid_invited_bidders.NeedDataSource
        Dim dt As DataTable = New DataTable()
        dt = CommonCode.GetAuctionInvitedBidders(Request.QueryString.Get("i"), IIf(lit_heading.Text = "Bidder left for Shipping rates", True, False))
        rad_grid_invited_bidders.DataSource = dt
        
    End Sub

    Protected Sub rad_grid_invited_bidders_PreRender(sender As Object, e As System.EventArgs) Handles rad_grid_invited_bidders.PreRender
        Dim menu As GridFilterMenu = rad_grid_invited_bidders.FilterMenu
        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub
End Class
