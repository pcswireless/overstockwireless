﻿Imports Telerik.Web.UI

Partial Class Auctions_UserControls_auction_fedex
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
            If Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
                hid_auction_id.Value = Request.QueryString.Get("i")
            Else
                hid_auction_id.Value = 0
            End If
            setFedexEdit(True)
            txt_auction_weight.Attributes.Add("onkeyup", "on_key_press();")
            'txt_test.Text = hid_language.Value
            'set_grid_bid_over_mode()
        End If
    End Sub
    Private Sub setFedexEdit(ByVal readMode As Boolean)
        If CInt(hid_auction_id.Value) > 0 Then
            fillFedexData(hid_auction_id.Value)
        End If
        visibleFedexData(readMode)
    End Sub
    Private Sub fillFedexData(ByVal auction_id As Integer)

        If auction_id > 0 Then
            Dim qry As String = "SELECT A.auction_id, " & _
                "ISNULL(A.use_pcs_shipping, 0) as use_pcs_shipping," & _
                "ISNULL(A.use_your_shipping, 0) as use_your_shipping," & _
                "ISNULL(A.auction_type_id, 0) AS auction_type_id,dbo.get_auction_status(A.auction_id) As status," & _                "ISNULL((select count(buyer_id) from [dbo].[fn_get_invited_buyers](" & auction_id & ") where buyer_id not in (select buyer_id from tbl_auction_fedex_rates WITH (NOLOCK) where auction_id=" & auction_id & ")),0) as fedex_pending," & _                "ISNULL(A.[weight],0) as [weight]," & _                "isnull((select top 1 convert(varchar(10),product_item_attachment_id) + '#' + filename from tbl_auction_product_item_attachments where auction_id=A.auction_id order by product_item_attachment_id desc),'') as product_item," & _                "ISNULL(A.[after_launch_filename],'') as [after_launch_filename]," & _                "ISNULL(A.[package_type],'') as [package_type]," & _                "ISNULL(A.shipping_amount, 0) AS shipping_amount" & _                " FROM tbl_auctions A WITH (NOLOCK) LEFT JOIN tbl_master_stock_locations B ON A.stock_location_id=B.stock_location_id WHERE A.auction_id = " & auction_id

            Dim dt As DataTable = New DataTable()
            dt = SqlHelper.ExecuteDatatable(qry)
            If dt.Rows.Count > 0 Then
                With dt.Rows(0)

                    lbl_shipping_amount.Text = "$" & FormatNumber(.Item("shipping_amount"), 2)
                    txt_shipping_amount.Text = CommonCode.FormatMoney(.Item("shipping_amount"))

                    chk_pcs_shipping.Checked = .Item("use_pcs_shipping")
                    If .Item("use_pcs_shipping") Then
                        lbl_pcs_shipping.Text = "<img src='/Images/true.gif' alt=''>"
                    Else
                        lbl_pcs_shipping.Text = "<img src='/Images/false.gif' alt=''>"
                    End If



                    chk_your_shipping.Checked = .Item("use_your_shipping")
                    If .Item("use_your_shipping") Then
                        lbl_your_shipping.Text = "<img src='/Images/true.gif' alt=''>"
                    Else
                        lbl_your_shipping.Text = "<img src='/Images/false.gif' alt=''>"
                    End If

                    If Not ddl_package_type.Items.FindItemByValue(.Item("package_type")) Is Nothing Then
                        ddl_package_type.ClearSelection()
                        ddl_package_type.Items.FindItemByValue(.Item("package_type")).Selected = True
                        lbl_package_type.Text = ddl_package_type.SelectedItem.Text
                    Else
                        lbl_package_type.Text = ""
                    End If

                    
                    txt_auction_weight.Text = Convert.ToDecimal(.Item("weight")).ToString("F2") 'FormatNumber(.Item("weight"), 2)
                    lbl_auction_weight.Text = Convert.ToDecimal(.Item("weight")).ToString("F2") 'FormatNumber(.Item("weight"), 2)
                    lbl_kgs.Text = Convert.ToDecimal(.Item("weight") / 2.2).ToString("F2") 'FormatNumber(.Item("weight") / 2.2, 2)             

                End With

                load_auction_bidder_queries()
            End If
            dt = Nothing
        End If

    End Sub

    Protected Sub load_auction_bidder_queries()
        If hid_auction_id.Value > 0 Then
            Dim rem_qty As Integer = SqlHelper.ExecuteScalar("select count(buyer_id) from  tbl_reg_buyers WITH (NOLOCK) where status_id=2 and ISNULL(discontinue,0)=0 and buyer_id not in( select buyer_id from tbl_auction_fedex_rates WITH (NOLOCK) where auction_id=" & hid_auction_id.Value & ")")
            If rem_qty > 0 Then
                lbl_count_remaining.Text = "<a style=""color:red;"" href=""javascript:void(0);"" onclick=""javascript:return invited_bidders(" & Request.QueryString.Get("i") & ",1);"">" & rem_qty & " bidder left for Shipping rates</a>"
            Else
                rem_qty = SqlHelper.ExecuteScalar(" select count(A.buyer_id) from dbo.fn_get_invited_buyers(" & hid_auction_id.Value & ") A INNER JOIN tbl_reg_buyers B WITH (NOLOCK) ON A.buyer_id=B.buyer_id and A.buyer_id in( select buyer_id from tbl_auction_fedex_rates WITH (NOLOCK) where is_error=1 and auction_id=" & hid_auction_id.Value & ")")
                If rem_qty > 0 Then
                    lbl_count_remaining.Text = rem_qty & " bidder have error on Shipping rates"
                Else
                    lbl_count_remaining.Text = ""
                End If

            End If

        End If

    End Sub

    Private Sub visibleFedexData(ByVal readMode As Boolean)
        If readMode Then
            btnEditFedex.Visible = True
            btnUpdateFedex.Visible = False
            divCancelFedex.Visible = False
        Else
            btnEditFedex.Visible = False
            If String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
                btn_auction_fedex.Visible = False
                btnUpdateFedex.Visible = False
                divCancelFedex.Visible = False
            Else
                btnUpdateFedex.Visible = True
                divCancelFedex.Visible = True
            End If
        End If

        span_auction_weight.Visible = Not readMode
        txt_auction_weight.Visible = Not readMode
        lbl_auction_weight.Visible = readMode

        chk_pcs_shipping.Visible = Not readMode
        chk_your_shipping.Visible = Not readMode

        lbl_your_shipping.Visible = readMode
        lbl_pcs_shipping.Visible = readMode

        ddl_package_type.Visible = Not readMode
        lbl_package_type.Visible = readMode

        txt_shipping_amount.Visible = Not readMode
        lbl_shipping_amount.Visible = readMode
    End Sub

    Protected Sub btnEditFedex_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditFedex.Click
        visibleFedexData(False)
    End Sub
    Protected Sub btnCancelFedex_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelFedex.Click
        visibleFedexData(True)
    End Sub
    Protected Sub btnUpdateFedex_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpdateFedex.Click
        If Page.IsValid Then
            If chk_pcs_shipping.Checked = False And chk_your_shipping.Checked = False Then
                lbl_msg.Text = "Please Checked PCS Shipping or Your Shipping."
                Return
            End If
            Dim updpara As String = ""
            Dim code_para As String = ""
            Dim title_para As String = ""

            updpara = updpara & "use_pcs_shipping=1" ' & IIf(chk_pcs_shipping.Checked, 1, 0) & ""
            updpara = updpara & ",use_your_shipping=0" ' & IIf(chk_your_shipping.Checked, 1, 0) & ""
            updpara = updpara & ",shipping_amount='" & CommonCode.GetValidNumeric(txt_shipping_amount.Text.Trim()) & "'"
            updpara = updpara & ",[weight]=" & IIf(txt_auction_weight.Text.Trim = "", 0, txt_auction_weight.Text.Trim) & ""

            If Not ddl_package_type.SelectedItem Is Nothing Then
                If lbl_package_type.Text <> ddl_package_type.SelectedItem.Text Then
                                updpara = updpara & ",package_type='" & ddl_package_type.SelectedValue & "'"

                End If
            Else
                updpara = updpara & "," & "package_type=''"
            End If

            'UploadFileStockImages(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))


            SqlHelper.ExecuteNonQuery("update tbl_auctions set " & updpara & " where auction_id=" & hid_auction_id.Value)
            CommonCode.insert_system_log("Fedex Basic Information updated", "btnUpdateFedex_Click", hid_auction_id.Value, "", "Auction")
            rad_grid_fedex.Rebind()
            lbl_msg.Text = "Auction updated successfully."
            load_auction_bidder_queries()

            setFedexEdit(True)
        End If
    End Sub

    Protected Sub rad_grid_fedex_InsertCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditFormInsertItem of the RadGrid        
        Dim insertedItem As GridEditFormInsertItem = DirectCast(e.Item, GridEditFormInsertItem)
        TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = ""
        Dim txt_grd_weight As TextBox = TryCast(insertedItem.FindControl("txt_grd_weight"), TextBox)
        Dim txt_grd_insured As TextBox = TryCast(insertedItem.FindControl("txt_grd_insured"), TextBox)
        Dim txt_grd_length As TextBox = TryCast(insertedItem.FindControl("txt_grd_length"), TextBox)
        Dim txt_grd_width As TextBox = TryCast(insertedItem.FindControl("txt_grd_width"), TextBox)
        Dim txt_grd_height As TextBox = TryCast(insertedItem.FindControl("txt_grd_height"), TextBox)
        Dim auction_weight As String = txt_grd_weight.Text.Trim
        Dim package_type As String = hid_package_type.Value
        Dim insured_amount As String = txt_grd_insured.Text.Trim
        Dim package_length As String = txt_grd_length.Text.Trim
        Dim package_height As String = txt_grd_height.Text.Trim
        Dim package_width As String = txt_grd_width.Text.Trim
        Dim qry = "", str_weight As String = ""
        Try
            Dim strError As String = ""
            If auction_weight = "" Then
                strError = "Package weight required."
            End If
            Dim dblauction_weight As Double = 0
            Dim dblauction_length As Int32 = 0
            If strError <> "" Then
                TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>" & strError & "</font>"
                e.Canceled = True
                Exit Sub
            Else
                If Not Double.TryParse(auction_weight, dblauction_weight) Then
                    TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Invalid auction weight.</font>"
                    e.Canceled = True
                    Exit Sub

                End If
            End If
            If strError <> "" Then
                TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>" & strError & "</font>"
                e.Canceled = True
                Exit Sub
            Else
                If insured_amount <> "" AndAlso Not Double.TryParse(insured_amount, dblauction_weight) Then
                    TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Invalid insured amount.</font>"
                    e.Canceled = True
                    Exit Sub

                End If
            End If
            If strError <> "" Then
                TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>" & strError & "</font>"
                e.Canceled = True
                Exit Sub
            Else
                If package_height <> "" AndAlso Not Double.TryParse(package_height, dblauction_length) Then
                    TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Invalid package height.</font>"
                    e.Canceled = True
                    Exit Sub

                End If
            End If
            If strError <> "" Then
                TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>" & strError & "</font>"
                e.Canceled = True
                Exit Sub
            Else
                If package_length <> "" AndAlso Not Double.TryParse(package_length, dblauction_length) Then
                    TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Invalid package length.</font>"
                    e.Canceled = True
                    Exit Sub

                End If
            End If
            If package_type.ToUpper <> "YOUR PACKAGING" Then
                package_height = 0
                package_width = 0
                package_length = 0
            End If
            package_height = IIf(package_height = "", 0, package_height)
            package_width = IIf(package_width = "", 0, package_width)
            package_length = IIf(package_length = "", 0, package_length)
            insured_amount = IIf(insured_amount = "", 0, insured_amount)
            qry = "INSERT INTO tbl_auction_fedex_packages (auction_id, package_weight,insured_amount,package_height,package_width,package_length,row_create_date,row_create_user_id) VALUES (" & hid_auction_id.Value & ", " & auction_weight & "," & insured_amount & "," & package_height & "," & package_width & "," & package_length & ",getdate(), " & CommonCode.Fetch_Cookie_Shared("user_id") & ") select cast(isnull(sum(P.package_weight),0) as varchar(15))+'#'+cast(isnull(A.weight,0) as varchar(15)) from tbl_auction_fedex_packages P right join tbl_auctions A WITH (NOLOCK) on P.auction_id=A.auction_id where A.auction_id=" & hid_auction_id.Value & " group by A.weight;"
            str_weight = SqlHelper.ExecuteScalar(qry)
            If str_weight.Contains("#") Then
                Dim str() As String
                str = str_weight.Split("#")
                If str.Length = 2 AndAlso (str(0) <> str(1)) Then
                    lbl_fedex_error.Text = "Packages weight (" & FormatNumber(CDbl(str(0)), 2) & " lbs) and Auction weight (" & FormatNumber(CDbl(str(1)), 2) & " lbs) should match!!"
                End If
            End If
        Catch ex As Exception
            TryCast(insertedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Unable to add Package. Reason: " + ex.Message & "</font>"
            e.Canceled = True
        End Try

    End Sub

    Protected Sub rad_grid_fedex_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles rad_grid_fedex.ItemCommand

        If e.CommandName = RadGrid.InitInsertCommandName Then '"Add new" button clicked

            Dim editColumn As GridEditCommandColumn = CType(rad_grid_fedex.MasterTableView.GetColumn("EditCommandColumn"), GridEditCommandColumn)
            editColumn.Visible = False

            'ElseIf (e.CommandName = RadGrid.EditCommandName And TypeOf e.Item Is GridEditFormItem) Then
            '    e.Canceled = True

        ElseIf (e.CommandName = RadGrid.RebindGridCommandName AndAlso e.Item.OwnerTableView.IsItemInserted) Then
            e.Canceled = True

        Else
            Dim editColumn As GridEditCommandColumn = CType(rad_grid_fedex.MasterTableView.GetColumn("EditCommandColumn"), GridEditCommandColumn)
            If Not editColumn.Visible Then
                editColumn.Visible = True
            End If

        End If
    End Sub

    Protected Sub rad_grid_fedex_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles rad_grid_fedex.NeedDataSource
        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then

            Dim strQuery As String = ""
            strQuery = "select cast(isnull(sum(P.package_weight),0) as varchar(15))+'#'+cast(isnull(A.weight,0) as varchar(15))+'#'+isnull(A.package_type,'') from tbl_auction_fedex_packages P right join tbl_auctions A WITH (NOLOCK) on P.auction_id=A.auction_id where A.auction_id=" & hid_auction_id.Value & " group by A.weight,A.package_type;" & _
                "SELECT ROW_NUMBER() " & _
        "OVER (ORDER BY fedex_pagkage_id) AS package_no," & _
            "fedex_pagkage_id, package_weight,isnull((select package_type from tbl_auctions WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i") & "),'') as package_type,isnull(package_length,0) as package_length,isnull(package_width,0) as package_width,isnull(package_height,0) as package_height,isnull(insured_amount,0) as insured_amount " & _
"FROM tbl_auction_fedex_packages where auction_id= " & Request.QueryString.Get("i")
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(strQuery)
            If ds.Tables(0).Rows(0)(0).Contains("#") Then
                Dim str() As String
                str = ds.Tables(0).Rows(0)(0).Split("#")
                If str.Length = 3 AndAlso ((str(0) <> str(1)) And ds.Tables(1).Rows.Count > 0) Then
                    lbl_fedex_error.Text = "Packages weight (" & FormatNumber(CDbl(str(0)), 2) & " lbs) and Auction weight (" & FormatNumber(CDbl(str(1)), 2) & " lbs) should match!!"
                End If
                If str.Length = 3 Then
                    hid_package_type.Value = str(2)
                End If
            End If
            rad_grid_fedex.DataSource = ds.Tables(1)
            'lbl_fedex_error.Text = strQuery
        End If
    End Sub

    Protected Sub rad_grid_fedex_UpdateCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditableItem of the RadGrid   

        Dim editedItem As GridEditFormItem = TryCast(e.Item, GridEditFormItem)
        TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = ""
        Dim fedex_pagkage_id As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("fedex_pagkage_id").ToString()
        Dim txt_grd_weight As TextBox = TryCast(editedItem.FindControl("txt_grd_weight"), TextBox)
        Dim txt_grd_insured As TextBox = TryCast(editedItem.FindControl("txt_grd_insured"), TextBox)
        Dim txt_grd_length As TextBox = TryCast(editedItem.FindControl("txt_grd_length"), TextBox)
        Dim txt_grd_width As TextBox = TryCast(editedItem.FindControl("txt_grd_width"), TextBox)
        Dim txt_grd_height As TextBox = TryCast(editedItem.FindControl("txt_grd_height"), TextBox)
        Dim auction_weight As String = txt_grd_weight.Text.Trim
        Dim package_type As String = hid_package_type.Value
        Dim insured_amount As String = txt_grd_insured.Text.Trim
        Dim package_length As String = txt_grd_length.Text.Trim
        Dim package_height As String = txt_grd_height.Text.Trim
        Dim package_width As String = txt_grd_width.Text.Trim
        Dim qry = "", str_weight As String = ""
        Try
            Dim strError As String = ""
            If auction_weight = "" Then
                strError = "Package weight required."
            End If
            Dim dblauction_weight As Double = 0
            Dim dblauction_length As Int32 = 0
            If strError <> "" Then
                TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>" & strError & "</font>"
                e.Canceled = True
                Exit Sub
            Else
                If Not Double.TryParse(auction_weight, dblauction_weight) Then
                    TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Invalid auction weight.</font>"
                    e.Canceled = True
                    Exit Sub

                End If
            End If
            If strError <> "" Then
                TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>" & strError & "</font>"
                e.Canceled = True
                Exit Sub
            Else
                If insured_amount <> "" AndAlso Not Double.TryParse(insured_amount, dblauction_weight) Then
                    TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Invalid insured amount.</font>"
                    e.Canceled = True
                    Exit Sub

                End If
            End If
            If strError <> "" Then
                TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>" & strError & "</font>"
                e.Canceled = True
                Exit Sub
            Else
                If package_height <> "" AndAlso Not Double.TryParse(package_height, dblauction_length) Then
                    TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Invalid package height.</font>"
                    e.Canceled = True
                    Exit Sub

                End If
            End If
            If strError <> "" Then
                TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>" & strError & "</font>"
                e.Canceled = True
                Exit Sub
            Else
                If package_length <> "" AndAlso Not Double.TryParse(package_length, dblauction_length) Then
                    TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Invalid package length.</font>"
                    e.Canceled = True
                    Exit Sub

                End If
            End If
            If strError <> "" Then
                TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>" & strError & "</font>"
                e.Canceled = True
                Exit Sub
            Else
                If package_width <> "" AndAlso Not Double.TryParse(package_width, dblauction_length) Then
                    TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Invalid package width.</font>"
                    e.Canceled = True
                    Exit Sub

                End If
            End If
            If package_type.ToUpper <> "YOUR PACKAGING" Then
                package_height = 0
                package_width = 0
                package_length = 0
            End If
            package_height = IIf(package_height = "", 0, package_height)
            package_width = IIf(package_width = "", 0, package_width)
            package_length = IIf(package_length = "", 0, package_length)
            insured_amount = IIf(insured_amount = "", 0, insured_amount)
            qry = "UPDATE tbl_auction_fedex_packages SET package_weight=" & auction_weight & ",insured_amount=" & insured_amount & ",package_height=" & package_height & ",package_width=" & package_width & ",package_length=" & package_length & " WHERE fedex_pagkage_id = " & fedex_pagkage_id & "; select cast(isnull(sum(P.package_weight),0) as varchar(15))+'#'+cast(isnull(A.weight,0) as varchar(15)) from tbl_auction_fedex_packages P right join tbl_auctions A WITH (NOLOCK) on P.auction_id=A.auction_id where A.auction_id=" & hid_auction_id.Value & " group by A.weight;"
            str_weight = SqlHelper.ExecuteScalar(qry)
            If str_weight.Contains("#") Then
                Dim str() As String
                str = str_weight.Split("#")
                If str.Length = 2 AndAlso (str(0) <> str(1)) Then
                    lbl_fedex_error.Text = "Packages weight (" & FormatNumber(CDbl(str(0)), 2) & " lbs) and Auction weight (" & FormatNumber(CDbl(str(1)), 2) & " lbs) should match!!"
                End If
            End If
            'lbl_fedex_error.Text = qry
        Catch ex As Exception
            TryCast(editedItem.FindControl("lbl_grid_error"), Literal).Text = "<font color='red'>Unable to update package. Reason: " + ex.Message & "</font>"
            e.Canceled = True
        End Try
    End Sub
    Protected Sub rad_grid_fedex_DeleteCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles rad_grid_fedex.DeleteCommand
        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        Dim fedex_pagkage_id As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("fedex_pagkage_id").ToString()
        ' Dim buyer_id As String = item("buyer_id").Text
        Try
            SqlHelper.ExecuteNonQuery("DELETE FROM [tbl_auction_fedex_packages] WHERE [fedex_pagkage_id] =" & fedex_pagkage_id & "")
        Catch ex As Exception
            rad_grid_fedex.Controls.Add(New LiteralControl("Unable to delete Package. Reason: " + ex.Message))
            e.Canceled = True
        End Try

    End Sub

    Protected Sub rad_grid_fedex_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles rad_grid_fedex.ItemDataBound
        Dim package_type As String = ""
        package_type = hid_package_type.Value
        If TypeOf e.Item Is GridEditableItem AndAlso e.Item.IsInEditMode Then
            Dim dataItem As GridEditableItem = CType(e.Item, GridEditableItem)
            Dim txt_grd_weight As TextBox = DirectCast(dataItem.FindControl("txt_grd_weight"), TextBox)
            ' Dim dd_grd_package As RadComboBox = TryCast(dataItem.FindControl("dd_grd_package"), RadComboBox)
            Dim tr_dim As HtmlTableRow = DirectCast(dataItem.FindControl("tr_grd_dimension"), HtmlTableRow)
            'hid_row_dimension.Value = tr_dim.ClientID
            'lbl_fedex_error.Text = hid_package_type.Value
            If package_type.ToUpper = "YOUR PACKAGING" Then
                tr_dim.Attributes.Add("style", "display:")
            Else
                tr_dim.Attributes.Add("style", "display:none")
            End If
            ' txt_grd_weight.Text = "45"
        End If
    End Sub
    Protected Sub btn_refresh_fedex_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_refresh_fedex.Click
        setFedexEdit(True)
    End Sub
End Class
