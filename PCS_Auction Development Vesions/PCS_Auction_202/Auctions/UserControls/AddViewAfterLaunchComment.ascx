﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddViewAfterLaunchComment.ascx.vb"
    Inherits="Auctions_UserControls_AddViewAfterLaunchComment" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<div style="padding-bottom: 10px; padding-top: 8px;">
    <div class="pageheading">
        &nbsp;Additional Comments</div>
</div>
<telerik:RadAjaxPanel ID="RadAjaxPanel6" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
    CssClass="TabGrid">
    <table cellpadding="5" cellspacing="0" border="0" width="750">
        <tr>
            <td colspan="2">
                <asp:Label ID="lbl_after_launch" runat="server" Visible="false"></asp:Label>
                <asp:Panel ID="pnl_after_launch_editor" runat="server">
                    <telerik:RadEditor ID="RadEditor2" runat="server" Skin="Sitefinity" Width="750">
                        <Content>
             
                        </Content>
                    </telerik:RadEditor>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnEditAfterLaunch" runat="server" AlternateText="Save" ImageUrl="/images/edit.gif" />
                <asp:ImageButton ID="btnUpdateAfterLaunch" ValidationGroup="_basic" runat="server"
                    AlternateText="Update" ImageUrl="/images/update.gif" />
            </td>
            <td align="right">
                <asp:ImageButton ID="btnCancelAfterLaunch" runat="server" AlternateText="Cancel"
                    ImageUrl="/images/cancel.gif" />
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>
