﻿Imports Telerik.Web.UI
Partial Class Auctions_UserControls_AuctionInvitation_Old
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub

    Private Sub setPermission()
        If Not CommonCode.is_super_admin() Then
            Dim Edit_Auction As Boolean = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))
            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 3
                        Edit_Auction = True
                End Select
            Next
            dt = Nothing
            If Not Edit_Auction Then
                btn_add_filter.Visible = False
                RadGrid_SelectedBusinesslist.Columns.FindByUniqueName("DeleteColumn").Visible = False
                RadGrid_SelectedIndustrylist.Columns.FindByUniqueName("DeleteColumn").Visible = False
                RadGrid_SelectedBidderlist.Columns.FindByUniqueName("DeleteColumn").Visible = False
                btn_save_filter.Visible = False
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            'loadInvitationDropDownList()
            HID_invitation_id.Value = SqlHelper.ExecuteScalar("if exists(select invitation_id from tbl_auction_invitations where auction_id='" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & "') begin select top 1 invitation_id from tbl_auction_invitations where auction_id='" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & "' end else select 0")
            hid_reserve_price.Value = SqlHelper.ExecuteScalar("select ISNULL(reserve_price,0) from tbl_auctions WITH (NOLOCK) where auction_id=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
            If HID_invitation_id.Value <> "0" Then
                HID_invitation_name.Value = SqlHelper.ExecuteScalar("select name from tbl_auction_invitations where invitation_id = " & HID_invitation_id.Value)
                txt_invitation_name.Text = HID_invitation_name.Value
            End If
            initializeInvitationData(HID_invitation_id.Value)
        End If
    End Sub
#Region "Invitations"
    'Private Sub loadInvitationDropDownList()
    '    If CommonCode.is_admin_user Then
    '        ddl_invitation.DataSource = SqlHelper.ExecuteDataView("SELECT A.invitation_id, name FROM tbl_auction_invitations A inner join tbl_auctions B on A.auction_id=B.auction_id order by A.name")
    '    Else
    '        ddl_invitation.DataSource = SqlHelper.ExecuteDataView("SELECT A.invitation_id, name FROM tbl_auction_invitations A inner join tbl_auctions B on A.auction_id=B.auction_id inner join tbl_reg_seller_user_mapping C on B.seller_id=C.seller_id where user_id = '" & CommonCode.Fetch_Cookie_Shared("user_id") & "' order by A.name")
    '    End If

    '    ddl_invitation.DataBind()
    '    ddl_invitation.Items.Insert(0, New ListItem("--Select--", "0"))

    'End Sub
    Protected Sub RDO_filter_type1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RDO_filter_type1.CheckedChanged
        If RDO_filter_type1.Checked Then
            pnl_type1.Visible = True
            pnl_type2.Visible = False
            pnl_type3.Visible = False
        End If
    End Sub
    Protected Sub RDO_filter_type2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RDO_filter_type2.CheckedChanged
        If RDO_filter_type2.Checked Then
            pnl_type2.Visible = True
            pnl_type1.Visible = False
            pnl_type3.Visible = False
        End If
    End Sub
    Protected Sub RDO_filter_type3_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RDO_filter_type3.CheckedChanged
        If RDO_filter_type3.Checked Then
            pnl_type3.Visible = True
            pnl_type1.Visible = False
            pnl_type2.Visible = False
        End If
    End Sub
    Protected Sub RadGrid_SelectedBusinesslist_ItemDeleted(ByVal source As Object, ByVal e As Telerik.Web.UI.GridDeletedEventArgs) Handles RadGrid_SelectedBusinesslist.ItemDeleted
        If e.Exception Is Nothing Then
            noOfInvitation()
            RadGrid_SelectedBusinesslist_left.Rebind()
        End If
    End Sub
    Protected Sub RadGrid_SelectedIndustrylist_ItemDeleted(ByVal source As Object, ByVal e As Telerik.Web.UI.GridDeletedEventArgs) Handles RadGrid_SelectedIndustrylist.ItemDeleted
        If e.Exception Is Nothing Then
            noOfInvitation()
            RadGrid_SelectedIndustrylist_left.Rebind()
        End If
    End Sub
    Protected Sub RadGrid_SelectedBidderlist_ItemDeleted(ByVal source As Object, ByVal e As Telerik.Web.UI.GridDeletedEventArgs) Handles RadGrid_SelectedBidderlist.ItemDeleted
        If e.Exception Is Nothing Then
            noOfInvitation()
            RadGrid_buyerlist.Rebind()
        End If
    End Sub
    Protected Sub btn_add_filter_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_add_filter.Click
        If validInvitationSelection() Then
            Dim qry As String = ""
            'insert new filter
            If HID_invitation_id.Value = "0" Then
                insertNewInvitation()
                CommonCode.insert_system_log("New invitation created for the auction", "btn_add_filter_Click", Request.QueryString.Get("i"), "", "Auction")
            End If
            'save filter type

            If pnl_type1.Visible Then
                qry = "if exists(select invitation_filter_id from tbl_auction_invitation_filters where invitation_id=" & HID_invitation_id.Value & " and filter_type='business') begin select invitation_filter_id from tbl_auction_invitation_filters where invitation_id=" & HID_invitation_id.Value & " and filter_type='business' end else begin INSERT INTO tbl_auction_invitation_filters(invitation_id, filter_type) VALUES ('" & HID_invitation_id.Value & "','business') SELECT SCOPE_IDENTITY() end"
            ElseIf pnl_type2.Visible Then
                qry = "if exists(select invitation_filter_id from tbl_auction_invitation_filters where invitation_id=" & HID_invitation_id.Value & " and filter_type='industry') begin select invitation_filter_id from tbl_auction_invitation_filters where invitation_id=" & HID_invitation_id.Value & " and filter_type='industry' end else begin INSERT INTO tbl_auction_invitation_filters(invitation_id, filter_type) VALUES ('" & HID_invitation_id.Value & "','industry') SELECT SCOPE_IDENTITY() end"
            Else
                qry = "if exists(select invitation_filter_id from tbl_auction_invitation_filters where invitation_id=" & HID_invitation_id.Value & " and filter_type='bidder') begin select invitation_filter_id from tbl_auction_invitation_filters where invitation_id=" & HID_invitation_id.Value & " and filter_type='bidder' end else begin INSERT INTO tbl_auction_invitation_filters(invitation_id, filter_type) VALUES ('" & HID_invitation_id.Value & "','bidder') SELECT SCOPE_IDENTITY() end"
            End If

            Dim invitation_filter_id As Integer = SqlHelper.ExecuteScalar(qry)

            'save filter values
            saveFilterSelection(invitation_filter_id)

            If pnl_type1.Visible Then
                RadGrid_SelectedBusinesslist_left.Rebind()
            ElseIf pnl_type2.Visible Then
                RadGrid_SelectedIndustrylist_left.Rebind()
            Else
                RadGrid_buyerlist.Rebind()
            End If

            'bind values
            clearInvitationSelection()
            initializeInvitationData(HID_invitation_id.Value)

        Else
            RadWindowManager1.RadAlert("Please select items before add.", 280, 100, "Data Alert", "")
        End If

    End Sub
    Protected Sub btn_save_filter_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_save_filter.Click
        If Page.IsValid Then
            If HID_invitation_id.Value = "0" Then insertNewInvitation()

            'If ddl_invitation.SelectedValue <> "0" And HID_invitation_id.Value <> ddl_invitation.SelectedValue Then

            '    copyInvitation(HID_invitation_id.Value, ddl_invitation.SelectedValue)
            '    SqlHelper.ExecuteNonQuery("update [tbl_auction_invitations] set name='" & txt_invitation_name.Text.Trim() & "' where invitation_id = " & HID_invitation_id.Value)
            '    initializeInvitationData(HID_invitation_id.Value)
            'Else
            '    If HID_invitation_name.Value <> txt_invitation_name.Text.Trim() Then
            '        SqlHelper.ExecuteNonQuery("update [tbl_auction_invitations] set name='" & txt_invitation_name.Text.Trim() & "' where invitation_id = " & HID_invitation_id.Value)
            '        HID_invitation_name.Value = txt_invitation_name.Text.Trim()
            '    End If
            'End If
            If HID_invitation_name.Value <> txt_invitation_name.Text.Trim() Then
                SqlHelper.ExecuteNonQuery("update [tbl_auction_invitations] set name='" & txt_invitation_name.Text.Trim() & "' where invitation_id = " & HID_invitation_id.Value)
                HID_invitation_name.Value = txt_invitation_name.Text.Trim()
                lbl_msg.Text = "Template saved successfully."
            End If
            'loadInvitationDropDownList()
            'ddl_invitation.ClearSelection()
            'If Not ddl_invitation.Items.FindByText(txt_invitation_name.Text) Is Nothing Then
            '    ddl_invitation.Items.FindByText(txt_invitation_name.Text).Selected = True
            'End If
        End If
    End Sub
    Private Sub copyInvitation(ByVal new_inv_id As Integer, ByVal old_inv_id As Integer)

        'delete existing invitations
        SqlHelper.ExecuteNonQuery("Delete from tbl_auction_invitation_filter_values where invitation_filter_id in (select invitation_filter_id from tbl_auction_invitation_filters where invitation_id=" & new_inv_id & "); Delete from tbl_auction_invitation_filters where invitation_id=" & new_inv_id)


        Dim new_invitation_filter_id As Integer
        Dim old_invitation_filter_id As Integer
        Dim filter_type As String = ""
        Dim qry As String
        Dim i As Integer
        Dim dt As New DataTable
        dt = SqlHelper.ExecuteDatatable("SELECT A.invitation_filter_id,ISNULL(A.filter_type, '') AS filter_type	FROM tbl_auction_invitation_filters A WHERE A.invitation_id = " & old_inv_id & " ORDER BY A.invitation_filter_id")
        For i = 0 To dt.Rows.Count - 1
            With dt.Rows(i)
                old_invitation_filter_id = .Item("invitation_filter_id")
                filter_type = .Item("filter_type")
                new_invitation_filter_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_auction_invitation_filters(invitation_id, filter_type) VALUES ('" & new_inv_id & "', '" & filter_type & "'); SELECT SCOPE_IDENTITY()")

                qry = "INSERT INTO tbl_auction_invitation_filter_values(invitation_filter_id, business_type_id, industry_type_id, buyer_id, auction_id) SELECT " & new_invitation_filter_id & ",business_type_id,industry_type_id,buyer_id," & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & " from tbl_auction_invitation_filter_values where invitation_filter_id=" & old_invitation_filter_id
                SqlHelper.ExecuteNonQuery(qry)

            End With
        Next

    End Sub

    Private Sub insertNewInvitation()
        Dim qry As String = ""
        Dim invitation_id As Integer = 0
        Dim title As String = ""
        invitation_id = SqlHelper.ExecuteScalar("select isnull(max(invitation_id),0) from tbl_auction_invitations")
        invitation_id = invitation_id + 1
        'title = "My Fav " & invitation_id
        title = ""

        qry = "INSERT INTO tbl_auction_invitations(auction_id, name, submit_date, submit_by_user_id) " & _
            "VALUES ('" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & "', '" & title & "', getdate(), '" & CommonCode.Fetch_Cookie_Shared("user_id") & "')  select scope_identity()"
        HID_invitation_id.Value = SqlHelper.ExecuteScalar(qry)
    End Sub
    'Protected Sub ddl_invitation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_invitation.SelectedIndexChanged
    '    If ddl_invitation.SelectedValue = "0" Then
    '        initializeInvitationData(HID_invitation_id.Value)
    '    Else
    '        initializeInvitationData(ddl_invitation.SelectedValue)
    '    End If
    'End Sub
    Private Sub initializeInvitationData(ByVal invitation_id As Integer)

        pnl_invitaion_data.Visible = False
        div_save.Visible = False

        If invitation_id <> 0 Then
            pnl_invitaion_data.Visible = True
            'If Not ddl_invitation.Items.FindByValue(invitation_id) Is Nothing Then
            '    ddl_invitation.ClearSelection()
            '    ddl_invitation.Items.FindByValue(invitation_id).Selected = True
            'End If

            Dim i As Integer
            Dim dtSource As New DataTable()

            div_save.Visible = False
            div_item_not.Visible = False
            div_opt_1.Visible = False
            div_opt_2.Visible = False
            div_opt_3.Visible = False
            RadGrid_SelectedBusinesslist.Columns.FindByUniqueName("DeleteColumn").Visible = True
            RadGrid_SelectedIndustrylist.Columns.FindByUniqueName("DeleteColumn").Visible = True
            RadGrid_SelectedBidderlist.Columns.FindByUniqueName("DeleteColumn").Visible = True
            'If ddl_invitation.SelectedValue <> HID_invitation_id.Value And ddl_invitation.SelectedValue <> "0" Then
            '    RadGrid_SelectedBusinesslist.Columns.FindByUniqueName("DeleteColumn").Visible = False
            '    RadGrid_SelectedIndustrylist.Columns.FindByUniqueName("DeleteColumn").Visible = False
            '    RadGrid_SelectedBidderlist.Columns.FindByUniqueName("DeleteColumn").Visible = False
            'End If


            dtSource = SqlHelper.ExecuteDatatable("select invitation_filter_id,invitation_id,filter_type from tbl_auction_invitation_filters where invitation_id = " & invitation_id & " order by invitation_filter_id")

            If dtSource.Rows.Count > 0 Then
                div_save.Visible = True
                For i = 0 To dtSource.Rows.Count - 1
                    If dtSource.Rows(i).Item("filter_type") = "business" Then
                        div_opt_1.Visible = True
                        invitation_filter_id1.Value = dtSource.Rows(i).Item("invitation_filter_id")
                    ElseIf dtSource.Rows(i).Item("filter_type") = "industry" Then
                        div_opt_2.Visible = True
                        invitation_filter_id2.Value = dtSource.Rows(i).Item("invitation_filter_id")
                    Else
                        div_opt_3.Visible = True
                        invitation_filter_id3.Value = dtSource.Rows(i).Item("invitation_filter_id")
                    End If
                Next
                RadGrid_SelectedBusinesslist.Rebind()
                RadGrid_SelectedIndustrylist.Rebind()
                RadGrid_SelectedBidderlist.Rebind()
            Else
                div_item_not.Visible = True
            End If
        End If
        noOfInvitation()
    End Sub
    Private Sub noOfInvitation()
        lit_no_of_bidder.Text = "<a href=""javascript:void(0);"" onclick=""javascript:return invited_bidders(" & Request.QueryString.Get("i") & ",0);"">" & CommonCode.GetAuctionInvitedBidders(Request.QueryString.Get("i"), 0).Rows.Count & " Invitations</a>"
    End Sub

    Private Sub saveFilterSelection(ByVal invitation_filter_id As Integer)
        Dim i As Integer = 0
        Dim qry As String = ""
        If pnl_type1.Visible Then
            Dim chk As New CheckBox
            Dim business_type_id As Integer
            For i = 0 To RadGrid_SelectedBusinesslist_left.Items.Count - 1

                chk = CType(RadGrid_SelectedBusinesslist_left.Items(i).Cells(0).FindControl("chk"), CheckBox)
                If chk.Checked = True Then
                    business_type_id = RadGrid_SelectedBusinesslist_left.Items(i).GetDataKeyValue("business_type_id")

                    qry = "if not exists(select invitation_filter_value_id from tbl_auction_invitation_filter_values where invitation_filter_id=" & invitation_filter_id & " and business_type_id=" & business_type_id & " and industry_type_id=0 and buyer_id=0) begin INSERT INTO tbl_auction_invitation_filter_values(invitation_filter_id, business_type_id, industry_type_id, buyer_id, auction_id)" & _
                    "VALUES (" & invitation_filter_id & "," & business_type_id & ", 0, 0, " & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & ") end"
                    SqlHelper.ExecuteNonQuery(qry)
                End If
            Next
            CommonCode.insert_system_log("Invitaion filter updated for business.", "saveFilterSelection", Request.QueryString.Get("i"), "", "Auction")
        ElseIf pnl_type2.Visible Then
            Dim chk As New CheckBox
            Dim industry_type_id As Integer
            For i = 0 To RadGrid_SelectedIndustrylist_left.Items.Count - 1
                chk = CType(RadGrid_SelectedIndustrylist_left.Items(i).Cells(0).FindControl("chk"), CheckBox)
                If chk.Checked = True Then
                    industry_type_id = RadGrid_SelectedIndustrylist_left.Items(i).GetDataKeyValue("industry_type_id")

                    qry = "if not exists(select invitation_filter_value_id from tbl_auction_invitation_filter_values where invitation_filter_id=" & invitation_filter_id & " and business_type_id=0 and industry_type_id=" & industry_type_id & " and buyer_id=0) begin INSERT INTO tbl_auction_invitation_filter_values(invitation_filter_id, business_type_id, industry_type_id, buyer_id, auction_id)" & _
                        "VALUES (" & invitation_filter_id & ", 0, " & industry_type_id & ", 0, " & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & ") end"
                    SqlHelper.ExecuteNonQuery(qry)
                    CommonCode.insert_system_log("Invitaion filter updated for industry.", "saveFilterSelection", Request.QueryString.Get("i"), "", "Auction")
                End If
            Next
        Else
            Dim buyer_id As Integer = 0

            For i = 0 To RadGrid_buyerlist.Items.Count - 1
                If RadGrid_buyerlist.Items(i).ItemType = GridItemType.Item Or RadGrid_buyerlist.Items(i).ItemType = GridItemType.AlternatingItem Then
                    If CType(RadGrid_buyerlist.Items(i).FindControl("chk_select"), CheckBox).Checked Then
                        buyer_id = RadGrid_buyerlist.Items(i).OwnerTableView.DataKeyValues(RadGrid_buyerlist.Items(i).ItemIndex)("buyer_id").ToString()
                        qry = "if not exists(select invitation_filter_value_id from tbl_auction_invitation_filter_values where invitation_filter_id=" & invitation_filter_id & " and business_type_id=0 and industry_type_id=0 and buyer_id=" & buyer_id & ") begin INSERT INTO tbl_auction_invitation_filter_values(invitation_filter_id, business_type_id, industry_type_id, buyer_id, auction_id)" & _
                        "VALUES (" & invitation_filter_id & ",0, 0, " & buyer_id & ", " & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & ") end"
                        SqlHelper.ExecuteNonQuery(qry)
                    End If
                End If
            Next
            CommonCode.insert_system_log("Invitaion filter updated for buyers.", "saveFilterSelection", Request.QueryString.Get("i"), "", "Auction")
        End If
    End Sub
    Private Sub clearInvitationSelection()
        If pnl_type1.Visible Then

            For i = 0 To RadGrid_SelectedBusinesslist_left.Items.Count - 1
                Dim chk As New CheckBox
                chk = CType(RadGrid_SelectedBusinesslist_left.Items(i).Cells(0).FindControl("chk"), CheckBox)
                chk.Checked = False
            Next
        ElseIf pnl_type2.Visible Then

            For i = 0 To RadGrid_SelectedIndustrylist_left.Items.Count - 1
                Dim chk As New CheckBox
                chk = CType(RadGrid_SelectedIndustrylist_left.Items(i).Cells(0).FindControl("chk"), CheckBox)
                chk.Checked = False
            Next
        Else
            Dim i As Integer = 0
            For i = 0 To RadGrid_buyerlist.Items.Count - 1
                If RadGrid_buyerlist.Items(i).ItemType = GridItemType.Item Or RadGrid_buyerlist.Items(i).ItemType = GridItemType.AlternatingItem Then
                    CType(RadGrid_buyerlist.Items(i).FindControl("chk_select"), CheckBox).Checked = False
                End If
            Next
        End If
    End Sub
    Private Function validInvitationSelection() As Boolean
        Dim flg As Boolean = False
        Dim i As Integer = 0

        If pnl_type1.Visible Then
            For i = 0 To RadGrid_SelectedBusinesslist_left.Items.Count - 1
                Dim chk As New CheckBox
                chk = CType(RadGrid_SelectedBusinesslist_left.Items(i).Cells(0).FindControl("chk"), CheckBox)
                If chk.Checked = True Then flg = True
            Next

        ElseIf pnl_type2.Visible Then

            For i = 0 To RadGrid_SelectedIndustrylist_left.Items.Count - 1
                Dim chk As New CheckBox
                chk = CType(RadGrid_SelectedIndustrylist_left.Items(i).Cells(0).FindControl("chk"), CheckBox)
                If chk.Checked = True Then flg = True
            Next
        Else
            For i = 0 To RadGrid_buyerlist.Items.Count - 1
                If RadGrid_buyerlist.Items(i).ItemType = GridItemType.Item Or RadGrid_buyerlist.Items(i).ItemType = GridItemType.AlternatingItem Then
                    If CType(RadGrid_buyerlist.Items(i).FindControl("chk_select"), CheckBox).Checked Then flg = True
                End If
            Next
        End If

        Return flg
    End Function

#End Region

    Protected Sub RadGrid_buyerlist_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid_buyerlist.PreRender
        RadGrid_buyerlist.GroupingSettings.CaseSensitive = False
        Dim menu As GridFilterMenu = RadGrid_buyerlist.FilterMenu
        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub

    Protected Sub btn_refresh_Click(sender As Object, e As System.EventArgs) Handles btn_refresh.Click
        HID_invitation_id.Value = SqlHelper.ExecuteScalar("if exists(select invitation_id from tbl_auction_invitations where auction_id='" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & "') begin select top 1 invitation_id from tbl_auction_invitations where auction_id='" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & "' end else select 0")
        hid_reserve_price.Value = SqlHelper.ExecuteScalar("select ISNULL(reserve_price,0) from tbl_auctions WITH (NOLOCK) where auction_id=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
        If HID_invitation_id.Value <> "0" Then
            HID_invitation_name.Value = SqlHelper.ExecuteScalar("select name from tbl_auction_invitations where invitation_id = " & HID_invitation_id.Value)
            txt_invitation_name.Text = HID_invitation_name.Value
        End If
        initializeInvitationData(HID_invitation_id.Value)
    End Sub
End Class
