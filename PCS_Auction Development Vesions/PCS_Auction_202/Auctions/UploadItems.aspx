﻿<%@ Page Title="PCS Bidding" Language="VB" MasterPageFile="~/BackendPopUP.master" AutoEventWireup="false" CodeFile="UploadItems.aspx.vb" Inherits="Auctions_UploadItems" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table cellpadding="10" cellspacing="0" border="0" width="100%">
        <tr>
            <td valign="top">
                <table cellpadding="5">
                    <tr>
                        <td class="caption">
                            <b style="font-size: 14px;">
                                <asp:Label ID="lbl_retailer_type" runat="server"></asp:Label></b><br />
                            <br />
                            Attach File (only in csv format)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:FileUpload ID="FileUpload1" runat="server" CssClass="TextBox" Width="230" size="15" /><br /><br /><asp:ImageButton
                                ID="btn_Image_Upload" runat="server" AlternateText="Update" ImageUrl="/images/upload.gif"
                                ValidationGroup="DV2" /><%--<asp:ImageButton ID="ImageButton1" OnClientClick="javascript:window.close();"
                                    runat="server" AlternateText="Close" ImageUrl="/images/close.gif" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RequiredFieldValidator ID="req_image" runat="server" InitialValue="" ValidationGroup="DV2"
                                ControlToValidate="FileUpload1" ErrorMessage="Excel is Required" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                       
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_msg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left">
              
                <asp:GridView ID="dg" runat="server">
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>

