﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FedexRateErrorList.aspx.vb" Inherits="Auctions_FedexRateErrorList" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellpadding="5" cellspacing="0" style="font-size: 12px; font-weight: bold;">
            <tr>
               <td width="100px;">Buyer Id :<span style="color:red">*</span></td>
                <td>
                    <asp:TextBox ID="txtBuyerId" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtBuyer" runat="server" 
                      ErrorMessage="Please enter buyer id" ControlToValidate="txtBuyerId"></asp:RequiredFieldValidator><br />
                    <asp:RegularExpressionValidator ID="REVBuyerId" runat="server" ErrorMessage="Enter numeric value" 
                      ControlToValidate="txtBuyerId" ValidationExpression="[1-9][0-9]*|0"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  width="100px;">State Code :<span style="color:red">*</span></td>
                <td>
                    <asp:TextBox ID="txtCode" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="rfvtCode" runat="server" 
                      ErrorMessage="Please enter StateCode" ControlToValidate="txtCode"></asp:RequiredFieldValidator><br />
                </td>
            </tr>
            <tr><td colspan="2"><asp:Button ID="btnSubmit" runat="server" Text="Check Staus" /> </td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td colspan="2"><asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
              <td colspan="2">
                 <asp:GridView ID="GVBuyer" runat="server" AutoGenerateColumns="true" BackColor="White"  
                        BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" HeaderStyle-BackColor="#2F97B5"
                       AlternatingRowStyle-BackColor="#A9ECFF" HeaderStyle-ForeColor="White"> 
                        <RowStyle BackColor="White" ForeColor="#000" Font-Bold="false"/> 
                </asp:GridView>
              </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
