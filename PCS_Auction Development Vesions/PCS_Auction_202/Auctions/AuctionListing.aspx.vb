﻿Imports Telerik.Web.UI
Imports System.Drawing
Partial Class Auctions_AuctionListing
    Inherits System.Web.UI.Page
    
    Private Sub setPermission()

        If CommonCode.is_super_admin() Then
            new_auction.Visible = True
            RadGrid_Auction_List.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
            RadGrid_Auction_List.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
            RadGrid_Auction_List.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
        Else

            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 1
                        new_auction.Visible = True
                    Case 35
                        RadGrid_Auction_List.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
                        RadGrid_Auction_List.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
                        RadGrid_Auction_List.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
                End Select
            Next
            dt = Nothing
        End If

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
            ViewState("status") = "both"
        End If
        'Dim column As GridColumn = RadGrid_Auction_List.MasterTableView.GetColumnSafe("status")
        'If (Not Page.IsPostBack) Then
        '    RadGrid_Auction_List.MasterTableView.FilterExpression = "([Status] like '%Running%') "
        '    column.CurrentFilterFunction = GridKnownFunction.Contains
        '    column.CurrentFilterValue = "Running"
        'Else
        '    If txt_confirmation_number.Text.Trim() <> "" Then
        '        RadGrid_Auction_List.MasterTableView.FilterExpression = "([Status] like '%%') "
        '        column.CurrentFilterFunction = GridKnownFunction.Contains
        '        column.CurrentFilterValue = ""
        '    Else
        '        RadGrid_Auction_List.MasterTableView.FilterExpression = "([Status] like '%Running%') "
        '        column.CurrentFilterFunction = GridKnownFunction.Contains
        '        column.CurrentFilterValue = "Running"
        '    End If
        'End If
        'If Not IsPostBack Then
        '    txt_test.Text = column.CurrentFilterValue
        'End If
        
    End Sub
    Protected Sub RadGrid_Auction_List_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid_Auction_List.NeedDataSource
        
        Dim str As String = ""
        If CommonCode.is_admin_user() Then
            str = "select A.auction_id,A.code,A.title,isnull(C.name,'') as product_category,isnull(D.name,'') as auction_type,A.start_date,A.end_date,A.display_end_time, dbo.auction_status(a.auction_id) as Status from [tbl_auctions] A WITH (NOLOCK) left join tbl_master_product_categories C on A.product_catetory_id=C.product_catetory_id left join tbl_auction_types D on A.auction_type_id=D.auction_type_id " & _
                " where isnull(A.discontinue,0)=0 and A.auction_id in (select auction_id from tbl_auctions WITH (NOLOCK) where ISNULL(cast(rank1_bid_id as varchar),'') like '%" & txt_confirmation_number.Text.Trim() & "%') or A.auction_id in (select distinct auction_id from tbl_auction_buy WITH (NOLOCK) where ISNULL(confirmation_no,'') like '%" & txt_confirmation_number.Text.Trim() & "%')"
            RadGrid_Auction_List.DataSource = SqlHelper.ExecuteDatatable(str)
        Else            str = "select A.auction_id,A.code,A.title,isnull(C.name,'') as product_category,isnull(D.name,'') as auction_type,A.start_date,A.end_date,A.display_end_time, dbo.auction_status(a.auction_id) as Status from [tbl_auctions] A WITH (NOLOCK) inner join [tbl_reg_seller_user_mapping] B on A.seller_id=B.seller_id left join tbl_master_product_categories C on A.product_catetory_id=C.product_catetory_id left join tbl_auction_types D on A.auction_type_id=D.auction_type_id where isnull(A.discontinue,0)=0 and B.user_id=" & hid_user_id.Value & "" & _                " And (A.auction_id in (select auction_id from tbl_auctions WITH (NOLOCK) where ISNULL(cast(rank1_bid_id as varchar),'') like '%" & txt_confirmation_number.Text.Trim() & "%') or A.auction_id in (select distinct auction_id from tbl_auction_buy WITH (NOLOCK) where ISNULL(confirmation_no,'') like '%" & txt_confirmation_number.Text.Trim() & "%'))"            RadGrid_Auction_List.DataSource = SqlHelper.ExecuteDatatable(str)
        End If


        If (Not Page.IsPostBack) Then
            Dim column As GridColumn = RadGrid_Auction_List.MasterTableView.GetColumnSafe("status")
            RadGrid_Auction_List.MasterTableView.FilterExpression = "([Status] like '%Running%') "
            column.CurrentFilterFunction = GridKnownFunction.Contains
            column.CurrentFilterValue = "Running"
            'Else
            '    If txt_confirmation_number.Text.Trim() <> "" Then
            '        RadGrid_Auction_List.MasterTableView.FilterExpression = "([Status] like '%%') "
            '        column.CurrentFilterFunction = GridKnownFunction.Contains
            '        column.CurrentFilterValue = ""
            '    Else
            '        'RadGrid_Auction_List.MasterTableView.FilterExpression = "([Status] like '%Running%') "
            '        'column.CurrentFilterFunction = GridKnownFunction.Contains
            '        'column.CurrentFilterValue = "Running"
            '    End If
        End If

    End Sub

    Protected Sub RadGrid_Auction_List_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadGrid_Auction_List.PreRender
        Dim menu As GridFilterMenu = RadGrid_Auction_List.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
       
        'If (Not Page.IsPostBack) Then
        '    RadGrid_Auction_List.MasterTableView.FilterExpression = "([product_catetory] LIKE \'%Product Category1%\') "
        '    Dim column As GridColumn = RadGrid_Auction_List.MasterTableView.GetColumnSafe("product_catetory")
        '    column.CurrentFilterFunction = GridKnownFunction.Contains
        '    column.CurrentFilterValue = "Product Category1"
        '    RadGrid_Auction_List.MasterTableView.Rebind()
        'End If
    End Sub
   
    
    Protected Sub imgBtn_Search_Click(sender As Object, e As ImageClickEventArgs) Handles imgBtn_Search.Click
        If txt_confirmation_number.Text.Trim() <> "" Then
            Dim column As GridColumn = RadGrid_Auction_List.MasterTableView.GetColumnSafe("status")
            RadGrid_Auction_List.MasterTableView.FilterExpression = "([Status] like '%%') "
            column.CurrentFilterFunction = GridKnownFunction.Contains
            column.CurrentFilterValue = ""
            RadGrid_Auction_List.Rebind()
        End If

    End Sub
End Class
