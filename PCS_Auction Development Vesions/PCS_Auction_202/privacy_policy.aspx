﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AuctionMain.master" AutoEventWireup="false" CodeFile="privacy_policy.aspx.vb" Inherits="privacy_policy" %>
<%@ Register Src="~/UserControls/bidder_salesrep.ascx" TagName="SalesRep" TagPrefix="UC1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <div class="detail">
        <div class="innerwraper">
            <UC1:SalesRep runat="server" ID="UC_SalesRep"></UC1:SalesRep>
            <div class="paging">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li>Privacy Policy</li>
                </ul>
            </div>
           <%-- <div class="printbx">
                <ul>
                    <li class="print"><a href="javascript:window.print();">Print</a></li>
                    <li>
                        <asp:Literal ID="lit_pdf" runat="server" /></li>
                </ul>
            </div>--%>
            <div class="clear">
            </div>
            <div class="products">
                <h1 class="title">
                    <asp:Literal ID="tab_sel" runat="server" /></h1>
                <!--seprator -->
                <div class="clear">
                </div>
                <!--detailbxwrap -->
                <div class="summrybx dtltxtbx_abt">
                    <div id="maincontent">
                        <h1 class="pgtitle" style="margin-top: 2px;">
                            Privacy Policy</h1>
                        <p>
                            PCS Wireless is committed to respecting and protecting your privacy. To that end,
                            we have prepared this Privacy Policy to disclose and describe our information gathering
                            and dissemination practices for this website. Because of the business-to-business
                            nature of most of PCS Wireless’s interactions, the overwhelming majority of information
                            collected on our website is business contact information obtained through a professional
                            or company-to-company interaction. While this type of business information has not
                            been the focus of recent consumer-oriented privacy efforts, PCS Wireless believes
                            that there is a need to address privacy issues in this realm as well. PCS Wireless
                            reserves the right to modify or update this Privacy Policy at any time without notice.
                            This Privacy Policy should be read in conjunction with PCS Wireless <a href="/Privacy-Policy/Terms-of-use">
                                Terms of Use</a>.</p>
                        <span class="priv_span">General Principles</span>
                        <p>
                            PCS Wireless follows five principles in accordance with worldwide practices for
                            the protection and privacy of our customers:</p>
                        <span class="priv_span">Principle 1 - Notice and Disclosure</span>
                        <p>
                            In general, you may visit this website without identifying yourself or revealing
                            any personal information. Some portions of this website may require you to give
                            us personally identifiable information, which is information that enables us to
                            identify you, such as your name, email or other address, or other personal information
                            ("Personal Information"). If you opt to provide us Personal Information, we may
                            use it for purposes such as verifying your identity, sending you information, making
                            the site easier for you to use by not having to enter information more than once,
                            helping you quickly find services or information on the website, helping us create
                            content most relevant to you, alerting you to product upgrades, special offers,
                            updated information and other new services from PCS Wireless or contacting you.
                            For certain programs, we may make your Personal Information available to our business
                            partners, for marketing and other purposes which we believe may be beneficial to
                            you. In the event of any merger or acquisition of our business in whole or in part,
                            all rights to your Personal Information may be transferred to the acquiring entity.</p>
                        <span class="priv_span">Principle 2 - Choice/Opt-Out</span>
                        <p>
                            It is our intent to inform you before we collect Personal Information, and tell
                            you what we intend to do with it. You will have the option of not providing the
                            information, in which case you may still be able to access other portions of this
                            website, although you may not be able to access certain programs or services. In
                            certain portions of this website, we also may enable you to "opt out" of certain
                            uses of your information, or elect not to receive future communications or services.
                            Users who no longer wish to receive communications from us may opt-out of receiving
                            these communications by emailing us at
                        </p>
                        <span class="priv_span">Principle 3 - Data Quality and Access</span>
                        <p>
                            PCS Wireless strives to keep your Personal Information accurate, complete and current.
                            We will promptly take steps to correct any inaccuracies in your personally identifiable
                            information of which we are made aware.</p>
                        <span class="priv_span">Principle 4 - Data Security</span>
                        <p>
                            PCS Wireless’s intent is to strictly protect the security of your Personal Information,
                            honor your choice for its intended use and carefully protect your data from loss,
                            misuse, unauthorized access or disclosure, alteration, or destruction. We have put
                            in place appropriate physical, electronic and managerial procedures to safeguard
                            and secure Personal Information we collect online.</p>
                        <span class="priv_span">Principle 5 - Enforcement</span>
                        <p>
                            If for some reason you believe PCS Wireless has not adhered to these principles,
                            please notify us by email at <a href="mailto:info@pcsww.com">info@pcsww.com</a>,
                            and we will do our best to determine and correct the problem promptly. Please include
                            the words "Privacy Policy" in the subject line.</p>
                        <span class="priv_span">Links to Third Party Sites</span>
                        <p>
                            This website contains links to other web sites that are not controlled by PCS Wireless
                            ("Third Party Sites"). Please be aware that PCS Wireless is not responsible for
                            the privacy practices of such other sites. We encourage our users to be aware when
                            they leave our site and to read the privacy statements of each and every website
                            that collects personally identifiable information. The PCS Wireless Privacy Policy
                            applies solely to information collected by this website.</p>
                        <span class="priv_span">Cookies</span>
                        <p>
                            Portions of this website use cookies to keep track of your visit, or to deliver
                            content specific to your interests. A "cookie" is a small amount of data transferred
                            to your browser and read by the Web server that placed it there, recording your
                            preferences and previously entered information. A cookie cannot retrieve any other
                            data from your hard drive, pass on computer viruses, or capture your email address.
                            Currently, web sites use cookies to enhance the user's visit; in general, cookies
                            can securely store a user's ID and password, personalize home pages, identify which
                            parts of a site have been visited or keep track of selections in a "shopping cart."
                            You can set your browser to notify you when you receive a cookie, giving you the
                            chance to accept or reject it. If you reject a cookie you may still use this website;
                            however, you may not be able to participate in certain sweepstakes, contests or
                            drawings that take place. Some of the Third Party Sites that you may link to through
                            this website may use cookies; however, we have no access to or control over these
                            cookies.</p>
                        <span class="priv_span">Surveys and Contests</span>
                        <p>
                            From time-to-time we may request information from users via surveys or contests
                            conducted on this website. Participation in these surveys or contests is completely
                            voluntary and the user therefore has a choice whether or not to disclose this information.
                            Information requested may include contact information (such as name and shipping
                            address) and demographic information (such as zip code). Contact information will
                            be used to notify the winners and award prizes. Survey information will be used
                            for purposes of monitoring or improving the use and satisfaction of this website.</p>
                        <span class="priv_span">Children</span>
                        <p>
                            While PCS Wireless’s product ad campaigns and marketing materials may be viewed
                            by children, we do not wish to receive data from children. PCS Wireless encourages
                            parents and guardians to spend time online with their children and to participate
                            in the interactive activities offered on the sites their children visit. No information
                            should be submitted to, or posted at, PCS Wireless’s web site by visitors under
                            18 years of age without the consent of their parent or guardian.</p>
                        <span class="priv_span">Contacting Us</span>
                        <p>
                            If you have any questions or comments about our Privacy Policy or practices, please
                            contact us via email at <a href="mailto:info@pcsww.com">info@pcsww.com</a> and include
                            the words "Privacy Policy" in the subject line.
                        </p>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>

