﻿
Partial Class buyer_rank_query
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            'Response.Write("select isnull((select dbo.buyer_bid_rank(" & Request.QueryString.Get("a") & "," & Request.QueryString.Get("b") & ")),0) as rank,isnull((select max(bid_amount) from tbl_auction_bids where buyer_id=" & Request.QueryString.Get("b") & " and auction_id =" & Request.QueryString.Get("a") & "),0) as amount ,isnull((select count(Q.query_id) As query_num from tbl_auction_queries Q where Q.auction_id=" & Request.QueryString.Get("a") & " and Q.buyer_id=" & Request.QueryString.Get("b") & " and ISNULL(parent_query_id,0)=0 and not exists(select query_id from tbl_auction_queries where parent_query_id=Q.query_id)),0) as query_count")
            If Request.QueryString.Get("a") <> Nothing And Request.QueryString.Get("b") <> Nothing AndAlso IsNumeric(Request.QueryString.Get("a")) AndAlso IsNumeric(Request.QueryString.Get("b")) Then
                Dim dt As New DataTable
                dt = SqlHelper.ExecuteDatatable("select isnull((select dbo.buyer_bid_rank(" & Request.QueryString.Get("a") & "," & Request.QueryString.Get("b") & ")),0) as rank,isnull((select max(bid_amount) from tbl_auction_bids WITH (NOLOCK) where buyer_id=" & Request.QueryString.Get("b") & " and auction_id =" & Request.QueryString.Get("a") & "),0) as amount ,isnull((select count(Q.query_id) As query_num from tbl_auction_queries Q where Q.auction_id=" & Request.QueryString.Get("a") & " and Q.buyer_id=" & Request.QueryString.Get("b") & " and ISNULL(parent_query_id,0)=0 and isnull(admin_marked,0)=0 and not exists(select query_id from tbl_auction_queries where parent_query_id=Q.query_id)),0) as query_count")
                If dt.Rows.Count > 0 Then
                    'lit_rank_amount.Text = FormatCurrency(dt.Rows(0)("amount"), 2)
                    lit_rank_amount.Text = "Current Rank: " & FormatNumber(dt.Rows(0)("rank"), 0)
                    ltrl_reply_pending.Text = "<a style=""color: #243E5A; text-decoration: none;"" onclick=""window.parent.open_pop_query('" & Request.QueryString.Get("a") & "','" & Request.QueryString.Get("b") & "');"" href=""javascript:void(0);"">" & FormatNumber(dt.Rows(0)("query_count"), 0) & " Pending Query" & "</a>"
                End If
            End If
        End If
    End Sub
End Class
