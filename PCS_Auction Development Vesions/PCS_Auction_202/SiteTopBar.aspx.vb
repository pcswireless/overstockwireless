﻿
Partial Class SiteTopBar
    Inherits BasePage
    Public is_running_auction As Boolean = False
    Public is_hidden_auction As Boolean = False
    Public is_upcoming_auction As Boolean = False
    Public is_bid_auction As Boolean = False
    Public mnu_fav As Boolean = False
     Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If String.IsNullOrEmpty(Request.QueryString.Get("f")) Then
            If CommonCode.Fetch_Cookie_Shared("mnu_fav") <> "" Then
                mnu_fav = CommonCode.Fetch_Cookie_Shared("mnu_fav")
            End If
        Else
            mnu_fav = Request.QueryString.Get("f")
        End If


        If Not String.IsNullOrEmpty(CommonCode.Fetch_Cookie_Shared("_lang")) Then
            For Each c As Control In Page.Controls
                If c.Controls.Count > 0 Then

                    If c.[GetType]().ToString().ToLower = "system.web.ui.webcontrols.imagebutton" Then
                        Dim str As String = DirectCast(c, ImageButton).CommandName

                        If Not String.IsNullOrEmpty(str) AndAlso str = CommonCode.Fetch_Cookie_Shared("_lang") Then
                            DirectCast(c, ImageButton).CssClass = "lanflagactive"
                            DirectCast(c, ImageButton).BorderWidth = 1
                        ElseIf Not String.IsNullOrEmpty(str) Then
                            DirectCast(c, ImageButton).CssClass = "lanflag"

                        End If
                    Else
                        For Each c2 As Control In c.Controls
                            'Response.Write("<script>alert('" & c2.[GetType]().ToString() & "')</script>")
                            If c2.[GetType]().ToString().ToLower = "system.web.ui.webcontrols.imagebutton" Then
                                ' Response.Write("<script>alert('" & CommonCode.Fetch_Cookie_Shared("_lang") & "')</script>")
                                Dim str1 As String = DirectCast(c2, ImageButton).CommandName

                                If Not String.IsNullOrEmpty(str1) AndAlso str1 = CommonCode.Fetch_Cookie_Shared("_lang") Then

                                    DirectCast(c2, ImageButton).CssClass = "lanflagactive"
                                    DirectCast(c2, ImageButton).BorderWidth = 1

                                ElseIf Not String.IsNullOrEmpty(str1) Then
                                    DirectCast(c2, ImageButton).CssClass = "lanflag"
                                    'img_eng_flag.borderwidth = 2
                                    ' img_eng_flag.cssclass = "lanflag_active"
                                    'response.write("<script>alert('" & directcast(c2, imagebutton).cssclass & "')</script>")
                                End If
                            End If
                        Next
                    End If


                End If
            Next
        Else
            'img_eng_flag.BorderWidth = 1
            'img_eng_flag.CssClass = "lanflagactive"
            'img_frn_flag.CssClass = "lanflag"
            'img_spn_flag.CssClass = "lanflag"
            'img_chn_flag.CssClass = "lanflag"
        End If
        If CommonCode.Fetch_Cookie_Shared("user_id") = "" Then
            lit_username.Text = "Guest"
            lit_logout.Text = "Login"
        Else
            'Dim company_name As String = SqlHelper.ExecuteScalar("select company_name from tbl_reg_buyers where buyer_id=(select top 1 buyer_id from tbl_reg_buyer_users where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
            ' If company_name <> "" Then
            '    lit_username.Text = company_name
            'End If
            lit_username.Text = CommonCode.Fetch_Cookie_Shared("displayname")

            lit_logout.Text = "LogOut"
        End If


        'lit_systime.Text = String.Format("{0:t}", Now()) & ", " & String.Format("{0:MMM dd, yyyy}", Now())
        If Not Page.IsPostBack Then
            tabsetting()
        End If

    End Sub

    Private Sub tabsetting()
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDatatable("select ISNULL((select COUNT(auction_id) from tbl_auctions WITH (NOLOCK) where dbo.get_auction_status(auction_id)=1),0) as running_auction," & _
                                        "ISNULL((select COUNT(A.auction_id) from tbl_auctions A WITH (NOLOCK) inner join tbl_auction_hidden H on A.auction_id=H.auction_id and H.buyer_user_id=" & IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("user_id")), CommonCode.Fetch_Cookie_Shared("user_id"), 0) & " where dbo.get_auction_status(A.auction_id)=1),0) as hidden_auction," & _
                                         "ISNULL((select COUNT(A.auction_id) from tbl_auctions A WITH (NOLOCK) inner join tbl_auction_bids B WITH (NOLOCK) on A.auction_id=B.auction_id where dbo.get_auction_status(A.auction_id) in (1,2) and B.buyer_id=" & IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("buyer_id")), CommonCode.Fetch_Cookie_Shared("buyer_id"), 0) & "),0) as bid_auction," & _
                                        "ISNULL((select COUNT(auction_id) from tbl_auctions WITH (NOLOCK) where dbo.get_auction_status(auction_id)=2),0) as upcoming_auction")
        '"ISNULL((select COUNT(H.favourite_id) from tbl_auctions A inner join tbl_auction_favourites H on A.auction_id=H.auction_id and H.buyer_user_id=" & IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("user_id")), CommonCode.Fetch_Cookie_Shared("user_id"), 0) & " where dbo.get_auction_status(A.auction_id)=1),0) as fav_auction," & _

        With dt.Rows(0)
            If .Item("running_auction") > 0 Then is_running_auction = True
            If .Item("hidden_auction") > 0 Then is_hidden_auction = True
            If .Item("upcoming_auction") > 0 Then is_upcoming_auction = True
             If .Item("bid_auction") > 0 Then is_bid_auction = True
        End With
        dt = Nothing
    End Sub
    Public Function getRedirectPath(ByVal opt As String) As String
        Dim _url As String = String.Empty
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            If opt = "m" Then
                _url = "redirectIframe('/SiteTopBar.aspx?t=0','/SiteLeftBar.aspx?t=0','/MyAccount.aspx?t=1#1');"
            ElseIf opt = "l" Then
                _url = "redirectIframe('/SiteTopBar.aspx?t=0','/SiteLeftBar.aspx?t=0','/logout.aspx');"
            End If

        Else
            _url = "redirectPage('/login.html');"
        End If
        Return _url
    End Function
    Public Function getCss(ByVal tabid As Integer, ByVal index As Integer) As String
        Dim strCss As String = ""
        Dim strUrl As String = Request.Url.ToString().ToLower()
        If tabid = Request.QueryString.Get("t") Then
            strCss = getcssString(index)
        End If

        Return strCss
    End Function
    Private Function getcssString(ByVal index As Integer) As String
        If index = 0 Then
            Return "m"
        Else
            Return "n"
        End If
    End Function
    'Protected Sub lang_flag_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_chn_flag.Click, img_eng_flag.Click, img_frn_flag.Click, img_spn_flag.Click
    '    Dim img_but As ImageButton = TryCast(sender, ImageButton)
    '    'img_but.CssClass = "lanFlag_active"
    '    ' img_but.BorderStyle = BorderStyle.Solid
    '    Threading.Thread.CurrentThread.CurrentUICulture = New Globalization.CultureInfo(img_but.CommandName.ToString)
    '    Threading.Thread.CurrentThread.CurrentCulture = Globalization.CultureInfo.CreateSpecificCulture(img_but.CommandName.ToString)

    '    CommonCode.Create_Cookie_shared("_lang", img_but.CommandName.ToString)
    '    Response.Write("<script language='javascript'>top.location = '/SiteHome.aspx';</script>")
    'End Sub
  End Class
