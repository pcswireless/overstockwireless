﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Auction_Pdf.aspx.vb" EnableViewState="true"
    Inherits="Auction_Pdf" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%-- <link type="text/css" rel="Stylesheet" href="/Style/AuctionBody.css" />--%>
</head>
<body style="font-size: 12px; font-family: UniSansRegular,Trebuchet MS, Arial, Helvetica, sans-serif;">
    <form id="form1" runat="server">
    <%--<center>--%>
    <div style="width: 900px; text-align: center;">
        <table cellspacing="0" cellpadding="0" border="1" width="100%">
            <tr>
                <td align="center" valign="top">
                    <div style="padding-left: 15px; padding-top: 15px; padding-right: 10px;">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="text-align: center; height: 120px;" valign="top">
                                    <img alt="logo pcs wireless" src='<%=SqlHelper.of_FetchKey("ServerHttp") %>/Images/logo.gif' />
                                    <div style="float:right;">
                                    <asp:ImageButton ID="img_close" runat="server" AlternateText="Close" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <table cellspacing="2" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td align="center" width="200">
                                                <asp:Image ID="auc_image" runat="server" BorderColor="gray" />
                                            </td>
                                            <td width="650" style="padding-left: 10px;">
                                                <asp:Label runat="server" ID="lbl_auc_title" ForeColor="#006FD5" Font-Size="16"></asp:Label><br />
                                                <asp:Literal runat="server" ID="lbl_auc_subtitle"></asp:Literal><br />
                                                Auction #:
                                                <asp:Literal runat="server" ID="lbl_auc_code"></asp:Literal><br />
                                                <asp:Literal ID="ltrl_short_desc" runat="server"></asp:Literal>
                                            </td>
                                            <td align="right" valign="top">
                                                <asp:Button runat="server" ID="but_to_pdf" Text="Save as pdf" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="padding-top: 20px;">
                                    <asp:Panel runat="server" ID="pnl_auc_summary">
                                        <table cellspacing="2" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td style="font-weight: bold; font-size: 14px; width: 100%; padding-bottom: 8px;
                                                    margin-top: 20px;">
                                                    Summary
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                        <tr>
                                                            <td width="50%" bgcolor="#F2F2F2" height="30">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;Stock Condition:&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal runat="server"
                                                                    ID="lbl_auc_condition"></asp:Literal>
                                                            </td>
                                                            <td bgcolor="#F2F2F2" height="30">
                                                                Stock Location:&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal runat="server" ID="lbl_auc_location"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" height="4" bgcolor="white">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F2F2F2" height="30">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;Packaging:&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal runat="server"
                                                                    ID="lbl_auc_packaging"></asp:Literal>
                                                            </td>
                                                            <td bgcolor="#F2F2F2" height="30">
                                                                Company:&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal runat="server" ID="lbl_auc_company"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="padding-top: 15px;">
                                    <asp:Panel runat="server" ID="pnl_auc_item">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td style="color: #009DB7; font-size: 17px; font-weight: bold;">
                                                    Items
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 0px 0px 20px 0px; line-height: 1.2em; color: Black;">
                                                    <asp:Repeater ID="Grid_Items" runat="server">
                                                        <ItemTemplate>
                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%" style="padding: 0px 0px 20px 0px;
                                                                line-height: 1.2em;">
                                                                <tr>
                                                                    <td>
                                                                        <span class="auctionPartNo" style="color: Black;">
                                                                            <%# Eval("manufacturer")%></span>&nbsp;<span class="auctionPartNo" style="color: #E66100;">
                                                                                <%# "Part No. " & Eval("part_no")%></span> &nbsp;<span class="auctionPartNo" style="color: #E66100;">
                                                                                    <%# "Quantity " & Eval("quantity")%></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%# Eval("description").ToString().Trim()%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td height="5" style="background-image: url(<%=SqlHelper.of_FetchKey("ServerHttp") %>/Images/separator_bar.jpg);
                                    background-repeat: no-repeat;">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="padding: 0px 0px 10px 0px;">
                                    <asp:Panel runat="server" ID="pnl_auc_detail">
                                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td>
                                                    <span style="color: #009DB7; font-size: 17px; font-weight: bold;">Product Details
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="line-height: 16px; padding-top: 10px;">
                                                    <asp:Literal runat="server" ID="lbl_auc_prodetail"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="line-height: 16px; padding-top: 10px;">
                                                    <asp:Literal runat="server" ID="lbl_auc_description"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" style="line-height: 16px; padding-top: 10px;">
                                                    &nbsp;
                                                    <asp:Label ID="lbl_attach_product_details" runat="server"></asp:Label>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:Panel runat="server" ID="pnl_auc_term">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td style="color: #009DB7; font-size: 17px; font-weight: bold;">
                                                                Terms & Conditions
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="color: #006FD5; text-align: left; font-weight: bold; clear: both; padding: 5px 0px 5px 0px;">
                                                                Payment Terms
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: justify;">
                                                                <asp:Literal runat="server" ID="lbl_auc_payment"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right; padding-top: 0px;">
                                                                &nbsp;
                                                                <asp:Label ID="lbl_attach_payment_terms" runat="server"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="color: #006FD5; text-align: left; font-weight: bold; clear: both; padding: 5px 0px 5px 0px;">
                                                                Shipping Terms
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: justify;">
                                                                <asp:Literal runat="server" ID="lbl_auc_shipping"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right; padding-top: 0px;">
                                                                &nbsp;
                                                                <asp:Label ID="lbl_attach_shipping_terms" runat="server"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="color: #006FD5; text-align: left; font-weight: bold; clear: both; padding: 5px 0px 5px 0px;">
                                                                Other Terms
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: justify;">
                                                                <asp:Literal runat="server" ID="lbl_auc_other"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right; padding-top: 0px;">
                                                                &nbsp;<asp:Label ID="lbl_attach_other_terms_and_conditions" runat="server"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="color: #006FD5; text-align: left; font-weight: bold; clear: both; padding: 5px 0px 5px 0px;">
                                                                Special Condition
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: justify;">
                                                                <asp:Literal runat="server" ID="lbl_auc_special"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right; padding-top: 0px;">
                                                                &nbsp;
                                                                <asp:Literal ID="lbl_attach_special_conditions" runat="server"></asp:Literal>&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="color: #006FD5; text-align: left; font-weight: bold; clear: both; padding: 5px 0px 5px 0px;">
                                                                Tax Details
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: justify;">
                                                                <asp:Literal runat="server" ID="lbl_auc_tax"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right; padding-top: 0px;">
                                                                &nbsp;<asp:Literal ID="lbl_attach_tax_details" runat="server"></asp:Literal>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
