﻿
Partial Class AuctionMain
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Request.QueryString.Get("u") <> Nothing Then
            Dim _user As String = "0"
            Try
                _user = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("u"))
                If IsNumeric(_user) AndAlso _user > 0 Then
                    CommonCode.Create_Cookie_shared("user_id", _user)
                End If

            Catch ex As Exception

            End Try
        End If

        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" And String.IsNullOrEmpty(Request.QueryString.Get("source")) Then
            If CommonCode.Fetch_Cookie_Shared("is_backend") = "1" Then
                Response.Redirect("/Backend_Home.aspx?t=1")
            End If
        End If

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            form1.Action = Request.RawUrl

            If Not String.IsNullOrEmpty(Request.QueryString.Get("source")) Then
                If Request.QueryString.Get("source") = "preview" Then
                    form1.Attributes.Add("onclick", "return false;")
                End If
            End If

            If IsNumeric((Request.QueryString.Get("t"))) Then
                'tabsetting()
            End If

            Dim obj As New Auction
            Dim dv As DataView = obj.fetch_auction_Listing_new(2, 0, 0, IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("user_id")), CommonCode.Fetch_Cookie_Shared("user_id"), 0), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("buyer_id")), CommonCode.Fetch_Cookie_Shared("buyer_id"), 0), SqlHelper.of_FetchKey("frontend_login_needed"), 0).Tables(0).DefaultView
            If dv.Count > 0 Then
                AutionHistory.Visible = True
            Else
                AutionHistory.Visible = False
            End If
        End If
    End Sub
    Private Sub tabsetting()
        Dim url As String = Request.RawUrl
        If url.Contains("?") Then
            url = url.Remove(url.IndexOf("?"))
        End If

        ltr_category.Text = ""
        Dim obj As New Auction
        Dim dt As New DataSet()
        dt = obj.fetch_auction_Listing_new(IIf(String.IsNullOrEmpty(Request.QueryString.Get("t")), 1, Request.QueryString.Get("t")), 0, 0, IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("user_id")), CommonCode.Fetch_Cookie_Shared("user_id"), 0), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("buyer_id")), CommonCode.Fetch_Cookie_Shared("buyer_id"), 0), SqlHelper.of_FetchKey("frontend_login_needed"))
        Dim dv As DataView
        If Not dt.Tables(0).Rows.Count > 0 Then
            dt.Clear()
            dt = obj.fetch_auction_Listing_new(1, 0, 0, IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("user_id")), CommonCode.Fetch_Cookie_Shared("user_id"), 0), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("buyer_id")), CommonCode.Fetch_Cookie_Shared("buyer_id"), 0), SqlHelper.of_FetchKey("frontend_login_needed"))
        End If
        dv = dt.Tables(1).DefaultView
        dv.RowFilter = "no_of_auction > 0"
        Dim count As Integer = 0
        For Each rv As DataRowView In dv
            count = count + rv.Row.Item("no_of_auction")
        Next
        'Response.Write(dv.Count)

        If count > 5 And (Not Request.Url.ToString.ToLower.Contains("auction_details.aspx")) Then
            ltr_category.Text = "<div class='categories'><a href='javascript:void(0)' >" & IIf((Request.QueryString.Get("s") <> ""), Request.QueryString.Get("s"), "Categories") & "</a><ul class='sub-categories'><li><a href='" & url & "'>All Auctions (" & count & ")</a></li>"

            For Each rv As DataRowView In dv
                ltr_category.Text = ltr_category.Text & "<li><a onclick='change_category();' href='" & url & "?p=" & rv.Row.Item("product_catetory_id") & "&s=" & rv.Row.Item("name") & "'>" & rv.Row.Item("name") & " (" & rv.Row.Item("no_of_auction") & ")</a></li>"
            Next

            ltr_category.Text = ltr_category.Text & "</ul></div>"

        End If

        dt.Dispose()
        dt = Nothing

    End Sub

End Class

