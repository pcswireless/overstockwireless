﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AuctionMain.master" AutoEventWireup="false" CodeFile="products.aspx.vb" Inherits="products" %>
<%@ Register Src="~/UserControls/bidder_salesrep.ascx" TagName="SalesRep" TagPrefix="UC1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <div class="innerwraper">
            <UC1:SalesRep runat="server" ID="UC_SalesRep"></UC1:SalesRep>
            <div class="paging">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li>Products</li>
                </ul>
            </div>
            <%--<div class="printbx">
                <ul>
                    <li class="print"><a href="javascript:window.print();">Print</a></li>
                    <li>
                        <asp:Literal ID="lit_pdf" runat="server" /></li>
                </ul>
            </div>--%>
            <div class="clear">
            </div>
            <div class="products">
                <h1 class="title">
                    <asp:Literal ID="tab_sel" runat="server" /></h1>
                <!--seprator -->
                <div class="clear">
                </div>
        <div class="summrybx dtltxtbx_abt">
            <div id="maincontent">
                <h1 class="pgtitle" style="margin-top: 2px;">
                    Your source for mobile products.</h1>
                <p>As your inside expert on the open market, PCS Wireless can help you source mobile devices&nbsp;&mdash; including phones, tablets, and accessories&nbsp;&mdash; and turn unwanted assets into revenue. We use our long-term relationships and industry expertise to locate the new, refurbished, and end-of-life products that your customers demand, at prices that benefit your bottom line.</p>

              <h3 class="prod_h3" >We have what you need&nbsp;&mdash; right here, in our warehouse.</h3>
              <p>Our warehouse is fully stocked with large inventories of&nbsp;new and used mobile phones, tablets, and accessories that we own. That means when you place an order, you can count on prompt fulfillment of the exact products you want. No substitutions. No delays. Our knowledgeable warehouse staff preloads your order with the necessary software, packages it per your requirements, and applies any customization you need to make a fast and positive impact on your market.<br>
<br>
<%--<img src="/pcsww/media/images/page-level/product-and-brands_1.jpg" style="border-bottom: 0px solid; border-left: 0px solid; width: 625px; height: 464px; border-top: 0px solid; border-right: 0px solid" alt="Mobile Products, Mobile devices, Mobile accessories, Mobile phones, Smartphones, Mobile phone accessories, Tablets, Mobile phone parts, Featured mobile brands, Nokia, Motorola, Sony Ericsson, Samsung, LG, BlackBerry, HTC">&nbsp;</p>--%>
<h3 class="prod_h3" >When you call, we respond.</h3>
<p>Because you matter to us, we will take the time to listen to you, address your concerns, and make sure our relationship continues to be a beneficial one. Our staff is ready to help&nbsp;&mdash; from tailoring credit lines that meet your financial needs to providing responsive customer support.</p>
<h3 class="prod_h3">We stand behind our products.</h3>
<p>Along with the manufacturer’s warranty, our phones come with an in-house warranty option. With this kind of protection&nbsp;&mdash; and a comprehensive Return Material Authorization (RMA) program that expedites product returns&nbsp;&mdash; you can be completely confident in every purchase you make from us.</p>
<h3 class="prod_h3">Your success matters to us.</h3>
<p>We hope you will choose us to be your long-term partner&nbsp;&mdash; the company you can depend on for the quality products your customers demand at competitive prices that benefit your bottom line. Because we know that by giving you the tools and support you need to achieve your business goals, we’re also helping ourselves succeed. And that’s the benefit of a true partnership.&nbsp;<br>
<br>
* If you don't see the product or brand you are looking for, <a href="/about/contact-us">give us a call</a>. Our inventory changes daily, and chances are we have what you need.<br>
<br>
&nbsp;</p>
            </div>
        </div>
    </div>
</asp:Content>

