﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="SalesRepAuctionReport.aspx.vb" Inherits="Reports_SalesRepAuctionReport" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Sales Representative Report</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; width: 40%; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    Please select the criteria to filter the report
                </div>
                
            </td>
        </tr>
        <tr>
            <td>
                    <table cellpadding="0" cellspacing="5" width="80%" border="0">
                        <tr>
                            <td width="13%">Auction Item:</td>
                            <td width="30%"><asp:DropDownList ID="ddl_auction_items" runat="server" Width="200"> </asp:DropDownList></td>
                            <td width="10%">Sales Rep.</td>
                            <td><asp:DropDownList ID="ddl_sales_rep" runat="server" Width="200"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>Bidder:</td>
                            <td><asp:DropDownList ID="ddl_bidders" runat="server" Width="200"></asp:DropDownList></td>
                            <td>Region:</td>
                            <td><asp:DropDownList ID="ddl_states" runat="server" Width="200"></asp:DropDownList></td>
                        </tr>
                        
                         <tr>
                            <td><b>Period</b>&nbsp;&nbsp;&nbsp; From:</td>
                            <td>
                                <telerik:RadDatePicker ID="rad_from_date" runat="server" Width="200"></telerik:RadDatePicker>
                            </td>
                            <td>To:</td>
                            <td><telerik:RadDatePicker ID="rad_to_date" runat="server" Width="200"></telerik:RadDatePicker></td>
                        </tr>
                         <tr>
                            <td>Bidding Status:</td>
                            <td>
                                <asp:DropDownList ID="ddl_bidding_status" runat="server" Width="200"></asp:DropDownList>
                            </td>
                            <td></td>
                            <td><asp:ImageButton ID="img_btn_search" runat="server" AlternateText="Search" /></td>
                        </tr>
                    </table>
            </td>
        </tr>
    </table>
</asp:Content>
