﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AuctionMain.master" AutoEventWireup="false" CodeFile="about.aspx.vb" Inherits="about" %>
<%@ Register Src="~/UserControls/bidder_salesrep.ascx" TagName="SalesRep" TagPrefix="UC1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" Runat="Server">

<div class="innerwraper">
            <UC1:SalesRep runat="server" ID="UC_SalesRep"></UC1:SalesRep>
            <div class="paging">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li>About Us</li>
                </ul>
            </div>
           <%-- <div class="printbx">
                <ul>
                    <li class="print"><a href="javascript:window.print();">Print</a></li>
                    <li>
                        <asp:Literal ID="lit_pdf" runat="server" /></li>
                </ul>
            </div>--%>
            <div class="clear">
            </div>
            <div class="products">
                <h1 class="title">
                    <asp:Literal ID="tab_sel" runat="server" /></h1>
                <!--seprator -->
                <div class="clear">
                </div>
<div class="summrybx dtltxtbx_abt">
<div id="maincontent">
<h1 class="pgtitle" style="margin-top:2px;">About PCS Wireless</h1>

<p><%--<img src="/pcsww/media/images/page-level/warehouse6.jpg" style="margin: auto 20px; border: 0px solid currentColor; width: 217px; height: 247px; float: right;" alt="">--%>When sourcing and selling mobile products on the open market, you want to know you’re working with a company that you can trust. As a leading open market distributor, PCS Wireless puts an end to this uncertainty by doing what we say we’re going to do. That means standing behind the products we distribute and supporting your business. Never making a commitment we can’t keep. And giving you competitive prices, rapid fulfillment, and added value. Because we want to be your partner, your inside expert on the market. We don’t want to work with you once and walk away. We want to be here for the long run &mdash; assisting you with sourcing quality product, selling excess inventories, refurbishing older models &mdash; and ultimately, helping you grow your business as we grow ours.<br>
<%--<br>
<br>
<br>
Please watch our video for a quick overview<br>
<object width="560" height="340">
<param value="http://www.youtube.com/v/AENBYUMQmrk&amp;fs=1&amp;hd=1&amp;rel=0&amp;border=1&amp;enablejsapi=1&amp;color1=0x666666&amp;color2=0xefefef" name="movie">
<param value="true" name="allowFullScreen">
<param value="always" name="allowscriptaccess">
<param value="transparent" name="wmode">
<embed width="560" height="340" allowfullscreen="true" allowscriptaccess="always" wmode="transparent" type="application/x-shockwave-flash" src="http://www.youtube.com/v/AENBYUMQmrk&amp;fs=1&amp;hd=1&amp;rel=0&amp;border=1&amp;enablejsapi=1&amp;color1=0x666666&amp;color2=0xefefef">

</object>
<br>
<br>&nbsp;--%>
</p>

<div class="txt6">
<h3>Company highlights</h3>
<p>Years in business: 12 years<br>
Warehouse: Company-owned facility in Florham Park, NJ<br>
</div>
<%--<br>
<img src="/getmedia/a9db7599-f277-4f04-bde0-20299f6db717/bbbab-seal-horizontal-large.aspx?width=150&amp;height=56" style="border-width: 0px; border-style: solid; width: 150px; height: 56px;" alt=""><br>
&nbsp;--%></p>
<h1 class="pgtitle" style="text-align: left;">PCS Wireless is ISO Certified:</h1>
<h1 class="pgtitle" style="text-align: left;"><span style="font-size: x-large;">ISO 9001:2008 &amp; 14001:2004</span></h1>
<%--<h1 style="text-align: left;"><br>
<img src="/getattachment/about/web-logos-(1).jpg.aspx?width=449&amp;height=330" style="width: 449px; height: 330px;" alt=""></h1>--%>
				</div>
                </div></div>
               
</asp:Content>

