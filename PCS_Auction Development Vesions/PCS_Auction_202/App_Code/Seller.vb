﻿Imports Microsoft.VisualBasic

Public Class Seller
  
    '---------------------------------------------------------
    '-- Method Name : insert_tbl_reg_sellers
    '-- Store PROCEDURE Called : insert_tbl_reg_sellers
    '-- Description : 
    '-- Creation Date : 8/12/2011
    '---------------------------------------------------------
    Public Sub insert_tbl_reg_sellers(ByVal company_name As String, ByVal contact_title As String, ByVal email As String, ByVal website As String, ByVal phone As String, ByVal fax As String, ByVal address1 As String, ByVal address2 As String, ByVal city As String, ByVal state_id As Integer, ByVal zip As String, ByVal country_id As Integer, ByVal vat_no As String, ByVal tax_id_ssn As String, ByVal time_zone As String, ByVal auto_approve_bidders As String, ByVal logo1 As String, ByVal logo2 As String, ByVal logo3 As String, ByVal is_active As Int16, ByVal submit_date As DateTime, ByVal submit_by_user_id As Integer, ByVal id_generated As Integer)
        Dim SqlParameter() As SqlParameter = New SqlParameter(22) {}

        SqlParameter(0) = New SqlParameter("@company_name", SqlDbType.varchar)
        SqlParameter(0).Size = 100
        SqlParameter(0).Direction = ParameterDirection.Input
        SqlParameter(0).Value = company_name

        SqlParameter(1) = New SqlParameter("@contact_title", SqlDbType.varchar)
        SqlParameter(1).Size = 50
        SqlParameter(1).Direction = ParameterDirection.Input
        SqlParameter(1).Value = contact_title

        SqlParameter(2) = New SqlParameter("@email", SqlDbType.varchar)
        SqlParameter(2).Size = 50
        SqlParameter(2).Direction = ParameterDirection.Input
        SqlParameter(2).Value = email

        SqlParameter(3) = New SqlParameter("@website", SqlDbType.varchar)
        SqlParameter(3).Size = 100
        SqlParameter(3).Direction = ParameterDirection.Input
        SqlParameter(3).Value = website

        SqlParameter(4) = New SqlParameter("@phone", SqlDbType.varchar)
        SqlParameter(4).Size = 50
        SqlParameter(4).Direction = ParameterDirection.Input
        SqlParameter(4).Value = phone

        SqlParameter(5) = New SqlParameter("@fax", SqlDbType.varchar)
        SqlParameter(5).Size = 50
        SqlParameter(5).Direction = ParameterDirection.Input
        SqlParameter(5).Value = fax

        SqlParameter(6) = New SqlParameter("@address1", SqlDbType.varchar)
        SqlParameter(6).Size = 100
        SqlParameter(6).Direction = ParameterDirection.Input
        SqlParameter(6).Value = address1

        SqlParameter(7) = New SqlParameter("@address2", SqlDbType.varchar)
        SqlParameter(7).Size = 100
        SqlParameter(7).Direction = ParameterDirection.Input
        SqlParameter(7).Value = address2

        SqlParameter(8) = New SqlParameter("@city", SqlDbType.varchar)
        SqlParameter(8).Size = 50
        SqlParameter(8).Direction = ParameterDirection.Input
        SqlParameter(8).Value = city

        SqlParameter(9) = New SqlParameter("@state_id", SqlDbType.int)
        SqlParameter(9).Direction = ParameterDirection.Input
        SqlParameter(9).Value = state_id

        SqlParameter(10) = New SqlParameter("@zip", SqlDbType.varchar)
        SqlParameter(10).Size = 10
        SqlParameter(10).Direction = ParameterDirection.Input
        SqlParameter(10).Value = zip

        SqlParameter(11) = New SqlParameter("@country_id", SqlDbType.int)
        SqlParameter(11).Direction = ParameterDirection.Input
        SqlParameter(11).Value = country_id

        SqlParameter(12) = New SqlParameter("@vat_no", SqlDbType.varchar)
        SqlParameter(12).Size = 50
        SqlParameter(12).Direction = ParameterDirection.Input
        SqlParameter(12).Value = vat_no

        SqlParameter(13) = New SqlParameter("@tax_id_ssn", SqlDbType.varchar)
        SqlParameter(13).Size = 50
        SqlParameter(13).Direction = ParameterDirection.Input
        SqlParameter(13).Value = tax_id_ssn

        SqlParameter(14) = New SqlParameter("@time_zone", SqlDbType.varchar)
        SqlParameter(14).Size = 100
        SqlParameter(14).Direction = ParameterDirection.Input
        SqlParameter(14).Value = time_zone

        SqlParameter(15) = New SqlParameter("@auto_approve_bidders", SqlDbType.varchar)
        SqlParameter(15).Size = 100
        SqlParameter(15).Direction = ParameterDirection.Input
        SqlParameter(15).Value = auto_approve_bidders

        SqlParameter(16) = New SqlParameter("@logo1", SqlDbType.varchar)
        SqlParameter(16).Size = 100
        SqlParameter(16).Direction = ParameterDirection.Input
        SqlParameter(16).Value = logo1

        SqlParameter(17) = New SqlParameter("@logo2", SqlDbType.varchar)
        SqlParameter(17).Size = 100
        SqlParameter(17).Direction = ParameterDirection.Input
        SqlParameter(17).Value = logo2

        SqlParameter(18) = New SqlParameter("@logo3", SqlDbType.varchar)
        SqlParameter(18).Size = 100
        SqlParameter(18).Direction = ParameterDirection.Input
        SqlParameter(18).Value = logo3

        SqlParameter(19) = New SqlParameter("@is_active", SqlDbType.bit)
        SqlParameter(19).Direction = ParameterDirection.Input
        SqlParameter(19).Value = is_active

        SqlParameter(20) = New SqlParameter("@submit_date", SqlDbType.datetime)
        SqlParameter(20).Direction = ParameterDirection.Input
        SqlParameter(20).Value = submit_date

        SqlParameter(21) = New SqlParameter("@submit_by_user_id", SqlDbType.int)
        SqlParameter(21).Direction = ParameterDirection.Input
        SqlParameter(21).Value = submit_by_user_id

        SqlParameter(22) = New SqlParameter("@id_generated", SqlDbType.int)
        SqlParameter(22).Direction = ParameterDirection.OUTPUT


        SqlHelper.ExecuteNonQuery(SqlHelper.of_getConnectString(), CommandType.StoredProcedure, "insert_tbl_reg_sellers", SqlParameter)

        SqlParameter = Nothing
    End Sub


End Class
