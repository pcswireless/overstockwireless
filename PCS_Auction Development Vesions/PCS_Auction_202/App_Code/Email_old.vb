﻿Imports Microsoft.VisualBasic

Public Class Email_old

    'Key use in Email_Html

    'User Name -- <<user_name>> eg. user_first_name + user_last_name
    'Server URL -- <<site_url>> eg: http://eplanet1-pc:150
    'Company Name -- <<company_name>> eg. PCS
    'Company Website --<<company_website>> eg. website
    'Bidder Name -- <<bidder_name>> eg: contact_first_name
    'Bidder First Name -- <<bidder_first_name>> eg. contact_first_name
    'Bidder Last Name -- <<bidder_last_name>> eg. contact_last_name
    'Bidder Tittle -- <<bidder_title>> eg. contact_title
    'Bidder Address1 --<<bidder_address1>> eg. address1
    'Bidder Address2 --<<bidder_address2>> eg. address2
    'Bidder City --<<bidder_city>>  eg. city
    'Bidder State --<<bidder_state>>    eg. state_text
    'Bidder Country --<<bidder_country>> eg. country
    'Bidder Zip --<<bidder_zip>>    eg. zip
    'Bidder Phone1 --<<bidder_mobile>>  eg. mobile
    'Bidder Phone2 --<<bidder_phone>>   eg. phone
    'Bidder Fax --<<bidder_fax>>    eg. fax
    'Bidder Comment --<<bidder_comment>>    eg. comment
    'Bidder Status --<<bidder_status>>  eg. status
    'Bidder Business Type --<<bidder_business_type>>    eg. business_type
    'Bidder Industry Type --<<bidder_industry_type>>    eg. industry_type
    'Bidder Find us --<<bidder_find_us>>    eg. Friend, Website
    'Bidder Email --<<bidder_email>>    eg. email
    'Bidding Date -- <<bid_date>>   eg. bid_date
    'Bidding Start Price -- <<bidding_start_price>> eg. start_price
    'Max Bid Amount -- <<max_bid_amount>>   eg. max_bid_amount
    'Auction Title -- <<auction_title>> eg. auction_title
    'Buyer ID -- <<bidder_id>> eg. buyer_id
    'Auction Start Date -- <<auction_start_date>>   eg. start_date 
    'Auction Start Time -- <<auction_start_time>>   eg. start_date converted to ToShortTimeString
    'Auction End Time -- <<auction_end_time>>   eg. display_end_time converted to ToShortTimeString
    'Auction End Date -- <<auction_end_date>>   eg. display_end_time
    'Auction Status -- <<auction_status>>   eg. auction_status
    'Auction Price -- <<auction_price>> eg. price
    'Auction Quantity -- <<auction_quantity>>   eg. quantity
    'Buy Type -- <<buy_type>>   eg.Buy it Now  
    'Query/Ask Message -- <<query_message>> eg. message
    'Dynamic/Reply Message -- <<reply_message>> eg.msg
    'Query Id -- <<query_code>> eg.Dynamic query_id
    'Customer Question -- <<customer_question>> eg. _key
    'Winning Bid Amount -- <<win_bid_amount>>   eg.bid_amount
    Public Function Header() As String
        Dim str As String = ""

        str = str & "<table cellpadding='0' cellspacing='0' border='0' width='700' style='border: solid 1px #d0d0d0;'>"
        str = str & "<tr><td style='padding:5px;'>"
        str = str & "<img src='" & SqlHelper.of_FetchKey("ServerHttp") & "/images/logo.gif'>"
        str = str & "<table cellpadding='0' cellspacing='0' border='0' width='700' style='padding: 3px 3px 3px 3px;'>"
        str = str & "<tr height='5'><td></td></tr>"
        str = str & "<tr><td align='left' style='font-size:12px; color:black;font-family:arial;'>"

        Return str
    End Function

    Public Function Footer() As String
        Dim str As String = ""

        str = str & "<table cellpadding='0' cellspacing='0' border='0' width='692' style='font-family:arial;'>"

        str = str & "<tr><td style='font-size:12px; color:black; padding:5px 0px;' align='left'>"
        str = str & "<br><br><b>Moderator</b><br><b>PCS Auctions</b><br><a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank' style='color:#0082c4;text-decoration:none;'>auctions.pcsww.com</a><br><br>"
        str = str & "</td></tr>"
        str = str & "<tr><td style='font-size:11px; font-family:arial;' align='left'><br />If you have any questions, please Contact Customer Service by e-mail at <a href='mailto:auctions@pcsww.com' style='color:#0082c4;text-decoration:none;'>auctions@pcsww.com</a> or by phone at 973-805-7400 ext. 249."
        str = str & "</td></tr>"
        str = str & "<tr><td style='font-size:11px; color:gray; font-family:arial;' align='left'>"
        str = str & "<hr>"
        str = str & "NOTICE:This E-mail (including attachments) is covered by the Electronic Communications Privacy Act, 18 U.S.C. §§ 2510-2521, is confidential, and may be legally privileged. The information is solely for the use of the addressee named above. If you are not the intended recipient, any disclosure, copying, distribution or other use of the contents of this information is strictly prohibited. If you have received this e-mail in error, please forward it to <a href='mailto:auctions@pcsww.com' style='color:#0082c4;text-decoration:none;'>auctions@pcsww.com</a> and delete this message."
        str = str & "<hr>"
        str = str & "</td></tr>"
        str = str & "<tr><td style='font-size:11px; color:gray; font-family:arial;' align='center'>"
        str = str & "<br><br>"
        str = str & "Copyright &copy; PCS Auction"
        str = str & "<br>"
        str = str & "</td></tr>"

        str = str & "</table>"

        ' CLOSE HEADER TABLE
        str = str & "</td></tr>"
        str = str & "</table>"
        str = str & "</td></tr>"
        str = str & "</table>"

        Return str
    End Function
    'DONE
    Public Function Send_Registration_Mail(ByVal user_email As String, ByVal user_name As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim strSubject As String = "Welcome to PCS Wireless Auctions!"
        Dim body As String = Header()
        body = body & "<br /><br />Dear " & user_name.Trim() & ","
        body = body & "<br /><br />Thank you for registering as a bidder on <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>auctions.pcsww.com</a>.<br /><br />We have received your request and are reviewing. Upon our review we will send out a follow up e-mail in regards to your activation."
        body = body & Footer()
        If for_mail_content = False Then
            Dim comm As New CommonCode()
            comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), strSubject, body, user_email, "", "", "", "", "")
            comm = Nothing
        End If

        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body
    End Function

    'DONE
    Public Function Bidderd_Admin_Active(ByVal buyer_id As Int32, Optional ByVal for_mail_content As Boolean = False) As String
        Dim comm As New CommonCode()
        Dim ds As DataSet = New DataSet()
        Dim strQuery As String = ""
        Dim strSubject As String = "Activate New Bidder"
        strQuery = "SELECT A.buyer_id, " & _
                      "ISNULL(U.username, '') AS username, " & _
                      "ISNULL(A.company_name, '') AS company_name, " & _
                      "ISNULL(A.contact_title, '') AS contact_title, " & _
                      "ISNULL(A.contact_first_name, '') AS contact_first_name, " & _
                      "ISNULL(A.contact_last_name, '') AS contact_last_name, " & _
                      "ISNULL(A.email, '') AS email, " & _
                      "ISNULL(A.website, '') AS website, " & _
                      "ISNULL(A.phone, '') AS phone, " & _
                      "ISNULL(A.mobile, '') AS mobile, " & _
                      "ISNULL(A.fax, '') AS fax," & _
                      "ISNULL(A.address1, '') AS address1, " & _
                      "ISNULL(A.address2, '') AS address2, " & _
                      "ISNULL(A.city, '') AS city, " & _
                      "ISNULL(state_text, '') AS state," & _
                      "ISNULL(A.zip, '') AS zip, " & _
                     "ISNULL(C.name, '') AS country, " & _
                     "ISNULL(A.comment, '') AS comment, " & _
                     "ISNULL(S.status, '') AS status, " & _
        "isnull((Stuff((Select ', ' + name From tbl_master_how_find_us Where  how_find_us_id in (select how_find_us_id from tbl_reg_buyer_find_us_assignment where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as find_us," & _
        "isnull((Stuff((Select ', ' + name From tbl_master_business_types Where  business_type_id in (select business_type_id from tbl_reg_buyer_business_type_mapping where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as business_type," & _
        "isnull((Stuff((Select ', ' + name From tbl_master_industry_types Where  industry_type_id in (select industry_type_id from tbl_reg_buyer_industry_type_mapping where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as industry_type," & _
        "isnull((Stuff((Select ', ' + company_name From tbl_reg_sellers Where  seller_id in (select seller_id from tbl_reg_buyer_seller_mapping where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as company_name " & _
                      " FROM tbl_reg_buyers A Left JOIN tbl_master_countries C ON A.country_id=C.country_id Left join tbl_reg_buyer_users U on A.buyer_id=U.buyer_id Left join tbl_reg_buyser_statuses S on A.status_id=S.status_id WHERE A.buyer_id =" & buyer_id & " and U.is_admin=1"
        ds = SqlHelper.ExecuteDataset(strQuery)
        Dim body As String = ""
        If ds.Tables(0).Rows.Count > 0 Then
            body = body & Header()
            body = body & "Dear Moderator,"
            body = body & "<br /><br />The following bidder has registered and is waiting your approval.<br /><br />"
            body = body & "<table cellpadding='2' cellspacing='2' style='font-family: Verdana; font-size: 11px;' border='0' width='630'>"
            body = body & "<tr><td style='padding-bottom:5px;color:#24C6F5;'><b>User Name:</b></td><td style='padding-bottom:5px;'><b>" & ds.Tables(0).Rows(0)("username") & "</b></td><td>&nbsp;</td><td>&nbsp;</td></tr>"
            body = body & "<tr><td colspan='4'><b>Company details</b></td></tr>"
            body = body & "<tr><td style='width: 155px;'><b>Company:</b></td><td style='width: 275px;'>" & ds.Tables(0).Rows(0)("company_name") & "</td><td><b>Company Website:</b></td><td>" & ds.Tables(0).Rows(0)("website") & "</td></tr>"
            body = body & "<tr><td colspan='4'><b>Contact details</b></td></tr>"
            body = body & "<tr><td><b>First Name:</b></td><td>" & ds.Tables(0).Rows(0)("contact_first_name") & "</td><td><b>Last Name:</b></td><td>" & ds.Tables(0).Rows(0)("contact_last_name") & "</td></tr>"
            body = body & "<tr><td style='width: 155px;'><b>Title:</b></td><td style='width: 275px;'>" & ds.Tables(0).Rows(0)("contact_title") & "</td><td>&nbsp;</td><td>&nbsp;</td></tr>"
            body = body & "<tr><td><b>Street:</b></td><td>" & ds.Tables(0).Rows(0)("address1").Replace("<br/>", Char.ConvertFromUtf32(10)).Replace("<br/>", Char.ConvertFromUtf32(10)) & "</td><td><b>Street Address 2:</td><td>" & ds.Tables(0).Rows(0)("address2").Replace("<br/>", Char.ConvertFromUtf32(10)).Replace("<br/>", Char.ConvertFromUtf32(10)) & "</td></tr>"
            body = body & "<tr><td><b>City:</b></td><td>" & ds.Tables(0).Rows(0)("city") & "</td><td><b>Country:</b></td><td>" & ds.Tables(0).Rows(0)("country") & "</td></tr>"
            body = body & "<tr><td><b>State/Province:</td><td>" & ds.Tables(0).Rows(0)("state") & "</td><td><b>Postal Code:</td><td>" & ds.Tables(0).Rows(0)("zip") & "</td></tr>"
            body = body & "<tr><td><b>Phone 1:</td><td>" & ds.Tables(0).Rows(0)("mobile") & "</td><td><b>Phone 2:</b></td><td>" & ds.Tables(0).Rows(0)("phone") & "</td></tr>"
            body = body & "<tr><td><b>Email:</b></td><td>" & ds.Tables(0).Rows(0)("email") & "</td><td><b>Fax:</td><td>" & ds.Tables(0).Rows(0)("fax") & "</td></tr>"
            body = body & "<tr><td colspan='4'><b>Account details</b></td></tr>"
            body = body & "<tr><td><b>Add comments here:</b></td><td>" & ds.Tables(0).Rows(0)("comment").Replace("<br/>", Char.ConvertFromUtf32(10)).Replace("<br/>", Char.ConvertFromUtf32(10)) & "</td><td><b>Status:</b></td><td>" & ds.Tables(0).Rows(0)("status") & "</td></tr>"
            body = body & "<tr><td colspan='4'><b>Other details</b></td></tr>"

            body = body & "<tr><td><b>Business Type:</b></td><td>" & ds.Tables(0).Rows(0)("business_type") & "</td><td><b>Industry Type:</td><td>" & ds.Tables(0).Rows(0)("industry_type") & "</td><td></tr>"

            body = body & "<tr><td><b>Linked Companies:</b></td><td>" & ds.Tables(0).Rows(0)("company_name") & "</td><td><b>How did you find us?:</td><td>" & ds.Tables(0).Rows(0)("find_us") & "</td></tr>"

            body = body & "<tr style='font-size:12px;font-family:arial;'><td><br><br><b>Moderator</b><br><b>PCS Auctions</b><br><a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank' style='color:#0082c4;text-decoration:none;'>auctions.pcsww.com</a><br><br></td></tr>"
            'body = body & "<tr><td colspan='4' align='center'><b><a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/login.html?bi=" & Security.EncryptionDecryption.EncryptValueFormatted(buyer_id.ToString) & "'>Click here</a> to activate this bidder.</b></td></tr>"
            body = body & "</table>"
            ' body = body & Footer()
            If for_mail_content = False Then
                comm.sendEmail("", "", strSubject, body, SqlHelper.of_FetchKey("employee_to"), "", "", "", "", "")
            End If


        End If
        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body
    End Function
    Public Function send_Bidderd_SemiAproval_Admin_Active(ByVal buyer_id As Int32, Optional ByVal for_mail_content As Boolean = False) As String
        Dim comm As New CommonCode()
        Dim ds As DataSet = New DataSet()
        Dim strQuery As String = ""
        Dim strSubject As String = "Approved Bidder from Status Semi Approval"
        strQuery = "SELECT A.buyer_id, " & _
                      "ISNULL(U.username, '') AS username, " & _
                      "ISNULL(A.company_name, '') AS company_name, " & _
                      "ISNULL(A.contact_title, '') AS contact_title, " & _
                      "ISNULL(A.contact_first_name, '') AS contact_first_name, " & _
                      "ISNULL(A.contact_last_name, '') AS contact_last_name, " & _
                      "ISNULL(A.email, '') AS email, " & _
                      "ISNULL(A.website, '') AS website, " & _
                      "ISNULL(A.phone, '') AS phone, " & _
                      "ISNULL(A.mobile, '') AS mobile, " & _
                      "ISNULL(A.fax, '') AS fax," & _
                      "ISNULL(A.address1, '') AS address1, " & _
                      "ISNULL(A.address2, '') AS address2, " & _
                      "ISNULL(A.city, '') AS city, " & _
                      "ISNULL(state_text, '') AS state," & _
                      "ISNULL(A.zip, '') AS zip, " & _
                     "ISNULL(C.name, '') AS country, " & _
                     "ISNULL(A.comment, '') AS comment, " & _
                     "ISNULL(S.status, '') AS status, " & _
        "isnull((Stuff((Select ', ' + name From tbl_master_how_find_us Where  how_find_us_id in (select how_find_us_id from tbl_reg_buyer_find_us_assignment where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as find_us," & _
        "isnull((Stuff((Select ', ' + name From tbl_master_business_types Where  business_type_id in (select business_type_id from tbl_reg_buyer_business_type_mapping where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as business_type," & _
        "isnull((Stuff((Select ', ' + name From tbl_master_industry_types Where  industry_type_id in (select industry_type_id from tbl_reg_buyer_industry_type_mapping where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as industry_type," & _
        "isnull((Stuff((Select ', ' + company_name From tbl_reg_sellers Where  seller_id in (select seller_id from tbl_reg_buyer_seller_mapping where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as company_name " & _
                      " FROM tbl_reg_buyers A Left JOIN tbl_master_countries C ON A.country_id=C.country_id Left join tbl_reg_buyer_users U on A.buyer_id=U.buyer_id Left join tbl_reg_buyser_statuses S on A.status_id=S.status_id WHERE A.buyer_id =" & buyer_id & " and U.is_admin=1"
        ds = SqlHelper.ExecuteDataset(strQuery)
        Dim body As String = ""
        If ds.Tables(0).Rows.Count > 0 Then
            body = body & Header()
            body = body & "Dear Moderator,"
            body = body & "<br /><br />The following bidder has Status Semi which done by Bidder Approval Agent.<br /><br />"
            body = body & "<table cellpadding='2' cellspacing='2' style='font-family: Verdana; font-size: 11px;' border='0' width='630'>"
            body = body & "<tr><td style='padding-bottom:5px;color:#24C6F5;'><b>User Name:</b></td><td style='padding-bottom:5px;'><b>" & ds.Tables(0).Rows(0)("username") & "</b></td><td>&nbsp;</td><td>&nbsp;</td></tr>"
            body = body & "<tr><td colspan='4'><b>Company details</b></td></tr>"
            body = body & "<tr><td style='width: 155px;'><b>Company:</b></td><td style='width: 275px;'>" & ds.Tables(0).Rows(0)("company_name") & "</td><td><b>Company Website:</b></td><td>" & ds.Tables(0).Rows(0)("website") & "</td></tr>"
            body = body & "<tr><td colspan='4'><b>Contact details</b></td></tr>"
            body = body & "<tr><td><b>First Name:</b></td><td>" & ds.Tables(0).Rows(0)("contact_first_name") & "</td><td><b>Last Name:</b></td><td>" & ds.Tables(0).Rows(0)("contact_last_name") & "</td></tr>"
            body = body & "<tr><td style='width: 155px;'><b>Title:</b></td><td style='width: 275px;'>" & ds.Tables(0).Rows(0)("contact_title") & "</td><td>&nbsp;</td><td>&nbsp;</td></tr>"
            body = body & "<tr><td><b>Street:</b></td><td>" & ds.Tables(0).Rows(0)("address1").Replace("<br/>", Char.ConvertFromUtf32(10)).Replace("<br/>", Char.ConvertFromUtf32(10)) & "</td><td><b>Street Address 2:</td><td>" & ds.Tables(0).Rows(0)("address2").Replace("<br/>", Char.ConvertFromUtf32(10)).Replace("<br/>", Char.ConvertFromUtf32(10)) & "</td></tr>"
            body = body & "<tr><td><b>City:</b></td><td>" & ds.Tables(0).Rows(0)("city") & "</td><td><b>Country:</b></td><td>" & ds.Tables(0).Rows(0)("country") & "</td></tr>"
            body = body & "<tr><td><b>State/Province:</td><td>" & ds.Tables(0).Rows(0)("state") & "</td><td><b>Postal Code:</td><td>" & ds.Tables(0).Rows(0)("zip") & "</td></tr>"
            body = body & "<tr><td><b>Phone 1:</td><td>" & ds.Tables(0).Rows(0)("mobile") & "</td><td><b>Phone 2:</b></td><td>" & ds.Tables(0).Rows(0)("phone") & "</td></tr>"
            body = body & "<tr><td><b>Email:</b></td><td>" & ds.Tables(0).Rows(0)("email") & "</td><td><b>Fax:</td><td>" & ds.Tables(0).Rows(0)("fax") & "</td></tr>"
            body = body & "<tr><td colspan='4'><b>Account details</b></td></tr>"
            body = body & "<tr><td><b>Add comments here:</b></td><td>" & ds.Tables(0).Rows(0)("comment").Replace("<br/>", Char.ConvertFromUtf32(10)).Replace("<br/>", Char.ConvertFromUtf32(10)) & "</td><td><b>Status:</b></td><td>" & ds.Tables(0).Rows(0)("status") & "</td></tr>"
            body = body & "<tr><td colspan='4'><b>Other details</b></td></tr>"

            body = body & "<tr><td><b>Business Type:</b></td><td>" & ds.Tables(0).Rows(0)("business_type") & "</td><td><b>Industry Type:</td><td>" & ds.Tables(0).Rows(0)("industry_type") & "</td><td></tr>"

            body = body & "<tr><td><b>Linked Companies:</b></td><td>" & ds.Tables(0).Rows(0)("company_name") & "</td><td><b>How did you find us?:</td><td>" & ds.Tables(0).Rows(0)("find_us") & "</td></tr>"

            body = body & "<tr style='font-size:12px;font-family:arial;'><td><br><br><b>Moderator</b><br><b>PCS Auctions</b><br><a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank' style='color:#0082c4;text-decoration:none;'>auctions.pcsww.com</a><br><br></td></tr>"
            'body = body & "<tr><td colspan='4' align='center'><b><a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/login.html?bi=" & Security.EncryptionDecryption.EncryptValueFormatted(buyer_id.ToString) & "'>Click here</a> to activate this bidder.</b></td></tr>"
            body = body & "</table>"
            ' body = body & Footer()
            If for_mail_content = False Then
                comm.sendEmail("", "", strSubject, body, SqlHelper.of_FetchKey("employee_to"), "", "", "", "", "")
            End If


        End If
        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body
    End Function
    'DONE
    Public Function Send_Registration_Approve_Mail(ByVal buyer_user_id As Integer, ByVal user_email As String, ByVal user_name As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim strSubject As String = "Welcome to PCS Wireless Auctions"
        Dim str As String = "select first_name As name,email,username,password,ISNULL(is_active,0) As is_active  from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id
        Dim dt As DataTable
        dt = SqlHelper.ExecuteDatatable(str)
        Dim username As String = ""
        Dim password As String = ""
        Dim email As String = ""
        Dim name As String = ""
        Dim is_active As Boolean = False

        Dim body As String = Header()
        If dt.Rows.Count > 0 Then
            username = dt.Rows(0)("username")
            password = Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("password"))
            email = dt.Rows(0)("email")
            name = dt.Rows(0)("name")

            body = body & "<br /><br />Dear " & user_name.Trim() & ","
            body = body & "<br /><br />Thank you for registering as a bidder on <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>auctions.pcsww.com</a>."
            body = body & "<br /><br />Your account is now active. Please login to our site to participate in your invited auctions, by using the following details:"
            body = body & "<br /><br /><b>User Name: </b> " & username
            body = body & "<br /><b>Password: </b> " & password
            body = body & Footer()
            If for_mail_content = False Then
                Dim comm As New CommonCode()
                comm.sendEmail("", "", strSubject, body, user_email, "", "", "", "", "")
                comm = Nothing
            End If

        End If
        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body
    End Function
    'DONE
    Public Function Send_Registration_Approve_Mail_New(ByVal buyer_user_id As Integer, ByVal user_email As String, ByVal user_name As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim strSubject As String = ""
        Dim str As String = "select first_name As name,email,username,password,ISNULL(is_active,0) As is_active  from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id
        Dim comm As New CommonCode()
        Dim username As String = ""
        Dim password As String = ""
        Dim email_caption = "Email for Bidder Registration Approval", email As String = ""
        Dim name As String = ""
        Dim is_active As Boolean = False

        Dim body As String = ""
        Dim ds As New DataSet
        ds = comm.GetEmail_ByCode(13, str)
        If ds.Tables.Count = 2 AndAlso (ds.Tables(0).Rows.Count > 0 And ds.Tables(1).Rows.Count > 0) Then
            username = ds.Tables(1).Rows(0)("username")
            password = Security.EncryptionDecryption.DecryptValue(ds.Tables(1).Rows(0)("password"))
            email = ds.Tables(1).Rows(0)("email")
            name = ds.Tables(1).Rows(0)("name")

            body = ds.Tables(0).Rows(0)("html_body")
            email_caption = ds.Tables(0).Rows(0)("email_name")
            strSubject = ds.Tables(0).Rows(0)("email_subject")

            body = body.Replace("<<bidder_name>>", name)
            body = body.Replace("<<user_name>>", user_name)
            body = body.Replace("<<password>>", password)
            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))

            If for_mail_content = False Then
                comm.sendEmail("", "", strSubject, body, user_email, "", "", "", "", "")
            End If
        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
    'DONE
    Public Function Send_ChangePassword_Mail(ByVal user_email As String, ByVal user_name As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim strSubject As String = "Password Change Confirmation"
        Dim body As String = Header()
        body = body & "<br /><br />Dear " & user_name.Trim() & ","
        body = body & " <br /><br />Your password has been changed successfully."
        body = body & "<br /><br /><a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/login.aspx' target='_blank'>Click here to login</a>"
        body = body & Footer()
        If for_mail_content = False Then
            Dim comm As New CommonCode()
            comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), strSubject, body, user_email, "", "", "", "", "")
            comm = Nothing
        End If

        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body
    End Function
    Public Function Send_Forgot_Pwd(ByVal pwd As String, ByVal user_email As String, ByVal usr As String, ByVal user_name As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim strSubject As String = "Forgotten Password"
        Dim body As String = Header()
        body = body & "<br /><br />Dear " & user_name.Trim() & ","
        body = body & "<br /><br />Your new Login details are as follows:"
        body = body & "<br /><br />User Name : " & usr & "<br />Password : " & pwd
        body = body & "<br /><br />Please <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/login.aspx' target='_blank'>click here</a> to login to your account and reset your password."
        body = body & Footer()
        If for_mail_content = False Then
            Dim comm As New CommonCode()
            comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), strSubject, body, user_email, "", "", "", "", "")
            comm = Nothing
        End If

        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body
    End Function
    'DONE
    Public Function send_buyer_approval_email(ByVal buyer_id As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim strSubject As String = "Welcome to PCS Wireless Auctions!"

        Dim body As String = ""
        If IsNumeric(buyer_id) Then
            Dim str1 As String = "select username,password from tbl_reg_buyer_users where ISNULL(is_admin,0)=1 and buyer_id=" & buyer_id
            Dim dt2 As DataTable = SqlHelper.ExecuteDatatable(str1)
            Dim user_name As String = ""
            Dim password As String = ""
            If dt2.Rows.Count > 0 Then
                user_name = dt2.Rows(0)("username")
                password = Security.EncryptionDecryption.DecryptValue(dt2.Rows(0)("password"))
            End If
            Dim buyer_email As String = ""
            Dim buyer_name As String = ""
            Dim str As String = "select buyer_id,ISNULL(contact_first_name,'') AS name,ISNULL(email,'') As email from tbl_reg_buyers where buyer_id=" & buyer_id
            Dim dt As DataTable = New DataTable()
            dt = SqlHelper.ExecuteDatatable(str)
            If dt.Rows.Count > 0 Then
                buyer_email = dt.Rows(0)("email")
                buyer_name = dt.Rows(0)("name").ToString().Trim()
                body = body & Header()
                body = body & "Dear " & buyer_name.Trim() & ","
                body = body & "<br /><br />Congratulations!<br /><br />Your registration has been approved and your account is now active."
                body = body & "<br /><br />You may now <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/login.aspx' target='_blank'>login</a> to participate in various upcoming auctions, by using the following details:"
                body = body & "<br /><br /><b>User Name: </b> " & user_name
                body = body & "<br /><b>Password: </b> " & password
                body = body & "<br /><br />We look forward to your participation!"

                body = body & Footer()
                If for_mail_content = False Then
                    Dim comm As New CommonCode()
                    comm.sendEmail(SqlHelper.of_FetchKey("email_from"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, buyer_email, buyer_name, "", "", SqlHelper.of_FetchKey("email_CC"), "")
                    comm = Nothing
                End If

            End If
        End If
        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body
    End Function
    Public Function send_buyer_semi_approval_email_to_admin(ByVal buyer_id As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim strSubject As String = "Semi Approval Email for Bidder"

        Dim body As String = ""
        If IsNumeric(buyer_id) Then
            Dim str1 As String = "select username,password from tbl_reg_buyer_users where ISNULL(is_admin,0)=1 and buyer_id=" & buyer_id

            Dim dt2 As DataTable = SqlHelper.ExecuteDatatable(str1)
            str1 = "select username,password from tbl_sec_users where lower(username)=admin "
            Dim email_admin As String = SqlHelper.ExecuteScalar(str1)
            Dim user_name As String = ""
            Dim password As String = ""
            If dt2.Rows.Count > 0 Then
                user_name = dt2.Rows(0)("username")
                password = Security.EncryptionDecryption.DecryptValue(dt2.Rows(0)("password"))
            End If
            Dim buyer_email As String = ""
            Dim buyer_name As String = ""
            Dim str As String = "select buyer_id,ISNULL(contact_first_name,'') AS name,ISNULL(email,'') As email from tbl_reg_buyers where buyer_id=" & buyer_id
            Dim dt As DataTable = New DataTable()
            dt = SqlHelper.ExecuteDatatable(str)
            If dt.Rows.Count > 0 Then
                buyer_email = dt.Rows(0)("email")
                buyer_name = dt.Rows(0)("name").ToString().Trim()
                body = body & Header()
                body = body & "Dear " & buyer_name.Trim() & ","
                body = body & "<br /><br />Congratulations!<br /><br />Your registration has been approved and your account is now active."
                body = body & "<br /><br />You may now <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/login.aspx' target='_blank'>login</a> to participate in various upcoming auctions, by using the following details:"
                body = body & "<br /><br /><b>User Name: </b> " & user_name
                body = body & "<br /><b>Password: </b> " & password
                body = body & "<br /><br />We look forward to your participation!"

                body = body & Footer()
                If for_mail_content = False Then
                    Dim comm As New CommonCode()
                    comm.sendEmail(SqlHelper.of_FetchKey("email_from"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, buyer_email, buyer_name, "", "", SqlHelper.of_FetchKey("email_CC"), "")
                    comm = Nothing
                End If

            End If
        End If
        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body
    End Function
    'DONE
    Public Function send_buyer_status_changed_email(ByVal buyer_id As String, ByVal status As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim body As String = ""
        Dim strSubject As String = "Access restricted on PCS Auction"
        If IsNumeric(buyer_id) Then

            Dim buyer_email As String = ""
            Dim buyer_name As String = ""
            Dim str As String = "select buyer_id,ISNULL(contact_first_name,'') AS name,ISNULL(email,'') As email from tbl_reg_buyers where buyer_id=" & buyer_id
            Dim dt As DataTable = New DataTable()
            dt = SqlHelper.ExecuteDatatable(str)
            If dt.Rows.Count > 0 Then
                buyer_email = dt.Rows(0)("email")
                buyer_name = dt.Rows(0)("name")
                body = body & Header()
                body = body & "Dear " & buyer_name.Trim() & ","
                'body = body & "<br /><br />Sorry! Your PCS login account has been " & IIf(status.ToLower() = "on hold", "put on hold", "") & IIf(status = "Reject", "deactivated", status) & ".<br /><br />You will no longer be able to participate in any auctions by PCS Auction."
                body = body & "<br /><br />Sorry! Your PCS account has been " & IIf(status.ToLower() = "on hold", "put on hold", "") & IIf(status = "Reject", "deactivated", status) & ".<br /><br />Should this be in error please contact us to review."
                body = body & Footer()
                If for_mail_content = False Then
                    Dim comm As New CommonCode()
                    comm.sendEmail(SqlHelper.of_FetchKey("email_from"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, buyer_email, buyer_name, "", "", SqlHelper.of_FetchKey("email_CC"), "")
                    comm = Nothing
                End If


            End If
        End If
        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body
    End Function
    'DONE
    Public Function send_buyer_status_changed_email_New(ByVal buyer_id As String, ByVal status As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim email_caption As String = "Access restricted on PCS Auction"
        Dim body = "", strSubject As String = ""
        Dim buyer_email As String = ""
        Dim buyer_name As String = ""
        Dim comm As New CommonCode()
        If IsNumeric(buyer_id) Then
            Dim ds As New DataSet
            ds = comm.GetEmail_ByCode(17, "select buyer_id,ISNULL(contact_first_name,'') AS name,ISNULL(email,'') As email from tbl_reg_buyers where buyer_id=" & buyer_id)
            If ds.Tables.Count = 2 AndAlso (ds.Tables(0).Rows.Count > 0 And ds.Tables(1).Rows.Count > 0) Then
                body = ds.Tables(0).Rows(0)("html_body")
                email_caption = ds.Tables(0).Rows(0)("email_name")
                strSubject = ds.Tables(0).Rows(0)("email_subject")

                buyer_email = ds.Tables(1).Rows(0)("email")
                buyer_name = ds.Tables(1).Rows(0)("name")
                status = IIf(status.ToLower() = "on hold", "put on hold", IIf(status = "Reject", "deactivated", status))

                body = body.Replace("<<bidder_name>>", buyer_name)
                body = body.Replace("<<bidder_status>>", status)
                body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))

                If for_mail_content = False Then
                    comm.sendEmail(SqlHelper.of_FetchKey("email_from"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, buyer_email, buyer_name, "", "", SqlHelper.of_FetchKey("email_CC"), "")
                End If
            End If
        End If

        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
    'DONE
    Public Function send_bidder_sub_login_creation_email(ByVal buyer_user_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String
        Dim str As String = "select first_name As name,email,username,password,ISNULL(is_active,0) As is_active  from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id
        Dim dt As DataTable
        dt = SqlHelper.ExecuteDatatable(str)
        Dim strSubject As String = ""
        Dim user_name As String = ""
        Dim password As String = ""
        Dim email As String = ""
        Dim name As String = ""
        Dim is_active As Boolean = False

        Dim body As String = Header()
        If dt.Rows.Count > 0 Then
            user_name = dt.Rows(0)("username")
            password = Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("password"))
            email = dt.Rows(0)("email")
            name = dt.Rows(0)("name")

            is_active = dt.Rows(0)("is_active")

            body = body & "<br /><br />Dear " & name & ","
            If is_active Then
                body = body & "<br /><br />Congratulations!<br /><br />Your sub-login has been created and is now active. You may now <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/login.aspx' target='_blank'>log in</a> to participate in your invited auctions, by using the following details:"
            Else
                body = body & "<br /><br />Your sub-login has been created.<br /><br />But your account is in-active now. When your account gets activated you will get a confirmation email."
            End If

            If is_active Then
                body = body & "<br /><br /><b>User Name: </b> " & user_name
                body = body & "<br /><b>Password: </b> " & password
                strSubject = "Bidder Sub-Login Details"
            Else
                strSubject = "Bidder Sub-Login Deactivation"
            End If
            body = body & Footer()
            If for_mail_content = False Then
                Dim comm As New CommonCode()
                comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), strSubject, body, email, "", "", "", "", "")
                comm = Nothing
            End If

        End If

        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body

    End Function
    'DONE
    Public Function send_bidder_sub_login_creation_email_New(ByVal buyer_user_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String
        Dim str As String = "select first_name As name,email,username,password,ISNULL(is_active,0) As is_active  from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id
        Dim dt As DataTable
        dt = SqlHelper.ExecuteDatatable(str)
        Dim strSubject As String = ""
        Dim user_name As String = ""
        Dim password As String = ""
        Dim email As String = ""
        Dim name As String = ""
        Dim email_caption As String = ""
        Dim is_active As Boolean = False

        Dim comm As New CommonCode()

        Dim ds As New DataSet
        Dim body As String = ""
        If dt.Rows.Count > 0 Then
            user_name = dt.Rows(0)("username")
            password = Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("password"))
            email = dt.Rows(0)("email")
            name = dt.Rows(0)("name")

            is_active = dt.Rows(0)("is_active")


            If is_active Then
                ds = comm.GetEmail_ByCode(20)
            Else
                ds = comm.GetEmail_ByCode(21)
            End If
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                body = ds.Tables(0).Rows(0)("html_body")
                email_caption = ds.Tables(0).Rows(0)("email_name")
                strSubject = ds.Tables(0).Rows(0)("email_subject")

                body = body.Replace("<<bidder_name>>", dt.Rows(0)("name"))
                body = body.Replace("<<user_name>>", user_name)
                body = body.Replace("<<password>>", password)
                body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
            End If
            If for_mail_content = False Then
                comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), strSubject, body, email, "", "", "", "", "")
            End If

        End If

        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body

    End Function
    'DONE
    Public Function send_bidder_sub_login_active_status_changed_email(ByVal buyer_user_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String
        Dim str As String = "select first_name As name,email,username,password,ISNULL(is_active,0) As is_active  from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id
        Dim dt As DataTable
        dt = SqlHelper.ExecuteDatatable(str)
        Dim strSubject As String = "Bidder Sub-Login Details"
        Dim user_name As String = ""
        Dim password As String = ""
        Dim email As String = ""
        Dim name As String = ""
        Dim is_active As Boolean = False

        Dim body As String = Header()
        If dt.Rows.Count > 0 Then
            user_name = dt.Rows(0)("username")
            password = Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("password"))
            email = dt.Rows(0)("email")
            name = dt.Rows(0)("name")

            is_active = dt.Rows(0)("is_active")

            body = body & "<br /><br />Dear " & name & ","
            If is_active Then
                body = body & "<br /><br />Congratulation! Your sub-login account has been approved and your account is now active. You may now Login to participate in your invited auctions by using the following details."
                body = body & "<br /><br /><b>User Name: </b> " & user_name
                body = body & "<br /><b>Password: </b> " & password
                body = body & "<br /><br /><a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/login.aspx' target='_blank'>" & "Please click the following link to access your account.</a>"
            Else
                body = body & "<br /><br />Your sub-login account has been de-activated with PCS Bidding. You are no longer will be able to login your account."
            End If

            body = body & Footer()
            If for_mail_content = False Then
                Dim comm As New CommonCode()
                comm.sendEmail("", "", strSubject, body, email, "", "", "", "", "")
                comm = Nothing
            End If

        End If

        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body

    End Function
    'DONE
    Public Function send_bidder_sub_login_active_status_changed_email_New(ByVal buyer_user_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String
        Dim str As String = "select first_name As name,email,username,password,ISNULL(is_active,0) As is_active  from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id
        Dim dt As DataTable
        dt = SqlHelper.ExecuteDatatable(str)
        Dim strSubject As String = "Bidder Sub-Login Details"
        Dim user_name As String = ""
        Dim password As String = ""
        Dim email As String = ""
        Dim name As String = ""
        Dim email_caption As String = ""
        Dim is_active As Boolean = False

        Dim comm As New CommonCode()

        Dim ds As New DataSet
        Dim body As String = ""
        If dt.Rows.Count > 0 Then
            user_name = dt.Rows(0)("username")
            password = Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("password"))
            email = dt.Rows(0)("email")
            name = dt.Rows(0)("name")

            is_active = dt.Rows(0)("is_active")

            If is_active Then
                ds = comm.GetEmail_ByCode(22)
            Else
                ds = comm.GetEmail_ByCode(21)
            End If
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                body = ds.Tables(0).Rows(0)("html_body")
                email_caption = ds.Tables(0).Rows(0)("email_name")
                strSubject = ds.Tables(0).Rows(0)("email_subject")

                body = body.Replace("<<bidder_name>>", dt.Rows(0)("name"))
                body = body.Replace("<<user_name>>", user_name)
                body = body.Replace("<<password>>", password)
                body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
            End If
            If for_mail_content = False Then
                comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), strSubject, body, email, "", "", "", "", "")
            End If

        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body

    End Function

    'Done
    Public Function send_employee_creation_email(ByVal user_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String
        Dim str As String = "select user_id,first_name As name,email,username,password,ISNULL(is_active,0) As is_active from tbl_sec_users where user_id=" & user_id
        Dim dt As DataTable
        dt = SqlHelper.ExecuteDatatable(str)
        Dim strSubject As String = "Employee Login Detail"
        Dim user_name As String = ""
        Dim password As String = ""
        Dim email As String = ""
        Dim name As String = ""
        Dim is_active As Boolean = False
        Dim body As String = Header()
        If dt.Rows.Count > 0 Then
            user_name = dt.Rows(0)("username")
            password = Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("password"))
            email = dt.Rows(0)("email")
            name = dt.Rows(0)("name")
            is_active = dt.Rows(0)("is_active")

            body = body & "<br /><br />Dear " & name & ","
            If is_active Then
                body = body & "<br /><br />Congratulation! Your login has been created and is now active. You now have the ability to view your customer's participation in our auctions, as well as answer any questions they may have. Please login by using the following detail."
            Else
                body = body & "<br /><br />Your login has been created with PCS Bidding. The login detail has been given below:"
            End If

            body = body & "<br /><br /><b>User Name: </b> " & user_name
            body = body & "<br /><b>Password: </b> " & password
            If is_active Then
                body = body & "<br /><br /><a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/login.aspx' target='_blank'>Please click here to access your account.</a>"
            Else
                body = body & "<br /><br />But your account is in-active now. When your account gets activated you will get a confirmation email."
            End If


            body = body & Footer()
            If for_mail_content = False Then
                Dim comm As New CommonCode()
                comm.sendEmail("", "", strSubject, body, email, "", "", "", "", "")
                comm = Nothing
            End If

        End If

        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body

    End Function
    'DONE

    Public Function send_employee_active_status_changed_email(ByVal user_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String
        Dim str As String = "select user_id,first_name As name,email,username,password,ISNULL(is_active,0) As is_active from tbl_sec_users where user_id=" & user_id
        Dim dt As DataTable
        dt = SqlHelper.ExecuteDatatable(str)
        Dim strSubject As String = "Employee Login Detail"
        Dim user_name As String = ""
        Dim password As String = ""
        Dim email As String = ""
        Dim name As String = ""
        Dim is_active As Boolean = False
        Dim body As String = Header()
        If dt.Rows.Count > 0 Then
            user_name = dt.Rows(0)("username")
            password = Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("password"))
            email = dt.Rows(0)("email")
            name = dt.Rows(0)("name")
            is_active = dt.Rows(0)("is_active")
            body = body & "<br /><br />Dear " & name & ","
            If is_active Then
                body = body & "<br /><br />Congratulation! Your login has been activated with PCS Bidding. Now you will be able to manage the auctions and biddings after login by using the following details."
                body = body & "<br /><br /><b>User Name: </b> " & user_name
                body = body & "<br /><b>Password: </b> " & password
                body = body & "<br /><br /><a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/login.aspx' target='_blank'>Please click here to access your account.</a>"
            Else
                body = body & "<br /><br />Your login has been de-activated with PCS Bidding. You are no longer will be able to login to your PCS Bidding account."
            End If

            body = body & Footer()
            If for_mail_content = False Then
                Dim comm As New CommonCode()
                comm.sendEmail("", "", strSubject, body, email, "", "", "", "", "")
                comm = Nothing
            End If

        End If

        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body

    End Function
    'DONE
    Public Function sendOutOfBidMail(ByVal strMailTo As String, ByVal strMailToName As String, ByVal aucName As String, ByVal aucCode As String, ByVal bidamount As String, ByVal bid_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String

        Dim strSubject As String = "You have been outbid on " & aucName & "[" & aucCode & "]"
        Dim strBody As String = Header()
        strBody = strBody & "Dear " & strMailToName.Trim() & ","
        strBody = strBody & "<br><br>Your maximum bid of " & bidamount & " has been outbid for <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>" & aucName & "[" & aucCode & "]</a>"
        strBody = strBody & "<br><br>Please <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>log in</a> now and raise your bid in order to win."
        strBody = strBody & Footer()
        If bid_id > 0 And for_mail_content = False Then
            Dim Comm As New CommonCode()
            Comm.sendEmail("", "", strSubject, strBody, strMailTo, strMailToName, "", "", "", "")
            Comm = Nothing
            strBody = strBody.Replace("'", "''")
            strSubject = strSubject.Replace("'", "''")
            Dim sqlstr As String = "INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & strMailTo & "','" & strSubject & "','" & strBody & "',''); select SCOPE_IDENTITY()"
            Dim mailid As Integer = SqlHelper.ExecuteScalar(sqlstr)
            SqlHelper.ExecuteNonQuery("update tbl_auction_bids set send_email=1,email_id=" & mailid & " where bid_id=" & bid_id)
        End If

        Return "<b>Subject:</b> " & strSubject & "<br><br>" & strBody

    End Function
    'DONE
    Public Function set_rep_contact_request_to_salesrep(ByVal sales_rep_id As Integer, ByVal _key As String, ByVal question As String, Optional ByVal for_mail_content As Boolean = False, Optional ByVal query_id As Integer = 1) As String
        Dim subj As String = ""
        question = IIf(question = "", "Customer Ask Question", question)
        If question.Length > 30 Then
            subj = "BQ: " & question.Substring(0, 30)
        Else
            subj = "BQ: " & question
        End If

        Dim user_name As String = ""
        Dim user_email As String = ""
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDatatable("SELECT ISNULL(A.first_name, '') AS NAME, ISNULL(A.email, '') AS email FROM tbl_sec_users A WHERE A.user_id=" & sales_rep_id)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                user_name = .Item("NAME")
                user_email = .Item("email")
            End With
        End If
        dt.Dispose()
        Dim body As String = Header()
        If user_email <> "" Then

            body = body & "<br /><br />Dear " & user_name.Trim() & ","
            body = body & "<br /><br />One of your customers has a question."
            If _key.Trim <> "" And question.Trim <> "" Then
                body = body & "<br /><br /><b>Subject:</b> " & _key
                body = body & "<br /><b>Question:</b> " & question
            Else
                body = body & "<br /><b>Question:</b> " & _key & " " & question
            End If

            body = body & "<br />You can <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/login.aspx?q=" & Security.EncryptionDecryption.EncryptValueFormatted(query_id) & "' target='_blank'>click here</a> to login to your account."
            body = body & Footer()
            If for_mail_content = False Then
                Dim comm As New CommonCode()
                comm.sendEmail("", "", subj, body, user_email, "", "", "", "", "")
                comm = Nothing
            End If
        End If

        Return "<b>Subject:</b> " & subj & "<br><br>" & body

    End Function
    'DONE
    Public Function set_rep_contact_request_to_user(ByVal user_id As Integer, ByVal _key As String, ByVal question As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim subj As String = "Your Question to sales rep"
        Dim user_name As String = ""
        Dim user_email As String = ""
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDatatable("SELECT ISNULL(A.first_name, '') AS NAME, ISNULL(A.email, '') AS email FROM tbl_reg_buyer_users A WHERE A.buyer_user_id=" & user_id)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                user_name = .Item("NAME")
                user_email = .Item("email")
            End With
        End If
        dt.Dispose()

        Dim body As String = ""
        If user_email <> "" Then
            body = Header()
            body = body & "<br /><br />Dear " & user_name.Trim() & ","
            body = body & "<br /><br />Your inquiry has been received and will be answered shortly by your sales representative."
            If _key.Trim <> "" And question.Trim <> "" Then
                body = body & "<br /><br /><b>Subject:</b> " & _key
                body = body & "<br /><b>Question:</b> " & question
            Else
                body = body & "<br /><b>Question:</b> " & _key & " " & question
            End If

            body = body & Footer()
            If for_mail_content = False Then
                Dim comm As New CommonCode()
                comm.sendEmail("", "", subj, body, user_email, "", "", "", "", "")
                comm = Nothing
            End If

        End If

        Return "<b>Subject:</b> " & subj & "<br><br>" & body
    End Function
    'DONE
    Public Function Send_Bidder_Reply(ByVal parent_query_id As Int32, ByVal msg As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim body As String = ""
        Dim strSubject As String = ""
        Dim dt As DataTable = New DataTable()
        Dim strQuery As String = ""

        strQuery = "select ISNULL(U.first_name,'') as first_name,ISNULL(U.last_name,'') as last_name,U.email,ISNULL(Q.title,'') as title,ISNULL(Q.message,'') as message from tbl_reg_buyer_users U inner join tbl_auction_queries Q on U.buyer_user_id=Q.buyer_user_id WHERE Q.query_id =" & parent_query_id
        dt = SqlHelper.ExecuteDatatable(strQuery)
        If dt.Rows.Count = 1 Then

            body = Header()

            strSubject = "Response to your Inquiry on PCS Wireless Auctions"
            body = body & "Dear " & dt.Rows(0)("first_name").ToString().Trim() & ","
            body = body & "<br /><br />Your sales representative’s response to your inquiry is below:"
            If dt.Rows(0)("title") <> "" And dt.Rows(0)("message") <> "" Then
                body = body & "<br /><br /><b>Your subject:</b> " & dt.Rows(0)("title")
                body = body & "<br /><br /><b>Your query:</b> " & dt.Rows(0)("message")
            ElseIf dt.Rows(0)("title") <> "" And dt.Rows(0)("message") = "" Then
                body = body & "<br /><br /><b>Your query:</b> " & dt.Rows(0)("title")
            ElseIf dt.Rows(0)("title") = "" And dt.Rows(0)("message") <> "" Then
                body = body & "<br /><br /><b>Your query:</b> " & dt.Rows(0)("title")
            End If

            body = body & "<br /><br /><b>Reply:</b> " & msg
            ' body = body & "<br /><br /><span style='font-size:10px;'>If you have any further questions, please do not respond to this e-mail. Contact your sales representative directly or <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/login.html' target='_blank'>log in</a> to use the " & """CONTACT YOUR SALES REP""" & " Quick Contact Form.</span>"
            body = body & Footer()
            If for_mail_content = False Then
                Dim comm As New CommonCode()
                comm.sendEmail("", "", strSubject, body, dt.Rows(0)("email"), "", "", "", "", "")
                comm = Nothing
            End If

        End If
        dt = Nothing
        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body
    End Function
    'DONE
    Public Function Send_Auction_Order_Mail(ByVal auction_id As Integer, ByVal click_option As Integer, ByVal buyer_user_id As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim user_name As String = ""
        Dim body As String = Header()
        Dim strSubject As String = "Order Confirmation"
        Dim user_email As String = ""
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDatatable("select first_name as name ,email from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                user_name = .Item("name").ToString().Trim()
                user_email = .Item("email").ToString().Trim()
            End With
        End If
        dt.Dispose()

        dt = SqlHelper.ExecuteDatatable("select title , auction_type_id from tbl_auctions where auction_id=" & auction_id)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                body = body & "<br /><br />Dear " & user_name.Trim() & ","

                Select Case click_option

                    Case 0 'main button click
                        Select Case .Item("auction_type_id")
                            Case 1
                                'strSubject = "PCS Bidding - buy now order"
                                body = body & "<br /><br />Your buy now order has been successfully posted for the auction " & .Item("title") & ".<br><br>Your sales representative will contact you soon regarding your offer."
                            Case 2, 3
                                ' strSubject = "PCS Bidding - bid"
                                body = body & "<br /><br />Your bidding has been successfully posted for the auction " & .Item("title") & ".<br><br>Your sales representative will contact you soon regarding your offer."
                            Case 4
                                'strSubject = "PCS Bidding - offer"
                                strSubject = "Thank you for submitting your offer!"
                                body = body & "<br /><br />We have received your offer for <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>" & .Item("title") & "</a>."
                                body = body & "<br /><br />Your sales representative will contact you soon regarding your offer."

                        End Select
                    Case 1
                        ' strSubject = "PCS Bidding - buy now order"
                        body = body & "<br /><br />Your buy now order has been successfully posted for the auction " & .Item("title") & ".<br><br>Your sales representative will contact you soon regarding your offer."
                    Case 2
                        ' strSubject = "PCS Bidding - private order"
                        body = body & "<br /><br />Your private order has been successfully posted for the auction " & .Item("title") & ".<br><br>Your sales representative will contact you soon regarding your offer."
                    Case 3
                        ' strSubject = "PCS Bidding - partial order"
                        body = body & "<br /><br />Your partial order has been successfully posted for the auction " & .Item("title") & ".<br><br>Your sales representative will contact you soon regarding your offer."


                End Select
                dt = Nothing

                body = body & Footer()

                If for_mail_content = False Then
                    Dim comm As New CommonCode()
                    comm.sendEmail("", "", strSubject, body, user_email, "", "", "", "", "")
                    comm = Nothing
                    SqlHelper.ExecuteNonQuery("INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & user_email & "','" & strSubject & "','" & body & "','')")

                End If

            End With
        End If

        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body
    End Function
    'DONE
    Public Function Send_Mail_Confirm_Receipt(ByVal _id As String, ByVal type As String, ByVal strBody As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim user_name As String = ""
        Dim user_email As String = ""
        Dim strSubject As String = ""
        Dim buy_type As String = ""
        Dim auction_name As String = ""
        Dim body As String = Header()
        Dim dt As New DataTable()
        If _id > 0 Then
            If type.ToLower = "b" Then
                dt = SqlHelper.ExecuteDatatable("select U.first_name as name ,U.email,B.buy_type,A.title as auction_name from tbl_reg_buyer_users U inner join tbl_auction_buy B on U.buyer_user_id=B.buyer_user_id inner join tbl_auctions A on B.auction_id=A.auction_id where B.buy_id=" & _id & "")
            Else
                dt = SqlHelper.ExecuteDatatable("select U.first_name as name ,U.email,'' as buy_type,A.title as auction_name from tbl_reg_buyer_users U inner join tbl_auction_bids B on U.buyer_user_id=B.buyer_user_id inner join tbl_auctions A on B.auction_id=A.auction_id where B.bid_id=" & _id & "")
            End If

            If dt.Rows.Count > 0 Then
                With dt.Rows(0)
                    user_name = .Item("name").ToString().Trim()
                    user_email = .Item("email").ToString().Trim()
                    buy_type = .Item("buy_type").ToString().Trim()
                    auction_name = .Item("auction_name").ToString().Trim()
                End With

                body = body & "<br /><br />Dear " & user_name.Trim() & ","

                Select Case buy_type.ToLower()
                    Case "buy now"
                        strSubject = "Order Confirmation"
                        body = body & "<br /><br />Your Buy it Now order has been successfully received. Please see the confirmation details below."
                        body = body & "<br /><br />We will contact you soon regarding your order."
                        If strBody.Trim <> "" Then
                            body = body & "<br /><br />" & strBody
                        End If

                    Case "private"
                        strSubject = "Thank you for submitting your offer!"
                        body = body & "<br /><br />We have received your private Offer for <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>" & auction_name & "</a>"
                        body = body & "<br /><br />We will contact you shortly regarding your offer."
                        If strBody.Trim <> "" Then
                            body = body & "<br /><br />" & strBody
                        End If
                    Case "partial"
                        strSubject = "Thank you for submitting your offer!"
                        body = body & "<br /><br />We have received your partial Offer for <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>" & auction_name & "</a>"
                        body = body & "<br /><br />We will contact you shortly regarding your offer."
                        If strBody.Trim <> "" Then
                            body = body & "<br /><br />" & strBody
                        End If
                    Case Else
                        strSubject = "Thank you for submitting your order"
                        body = body & "<br /><br />We have received your order for <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>" & auction_name & "</a>"
                        body = body & "<br /><br />Your sales representative will contact you soon regarding your order."
                        If strBody.Trim <> "" Then
                            body = body & "<br /><br />" & strBody
                        End If
                End Select


                body = body & Footer()

                If for_mail_content = False Then
                    Dim comm As New CommonCode()
                    comm.sendEmail("", "", strSubject, body, user_email, "", "", "", "", "")
                    comm = Nothing
                    SqlHelper.ExecuteNonQuery("INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & user_email & "','" & strSubject & "','" & body & "','')")
                End If

            End If
            dt.Dispose()
        End If

        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body
    End Function
    Public Function SendWinningBidEmail(ByVal bid_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String
        'Done
        Dim body As String = ""
        Dim str As String = ""
        Dim strSubject As String = ""
        If bid_id > 0 Then
            Dim dt As New DataTable()
            Dim buyer_email As String = ""
            Dim buyer_name As String = ""
            Dim bidder_email As String = ""
            Dim bidder_name As String = ""

            Dim bid_date As DateTime = DateTime.Now
            Dim strBid As String = "select A.bid_date,ISNULL(A.max_bid_amount,0) AS max_bid_amount,ISNULL(A.bid_amount,0) AS bid_amount,ISNULL(A.action,'') AS action," & _
                "ISNULL(B.contact_first_name,'') As buyer_name,ISNULL(B.email,'') As email, " & _
                "ISNULL(C.title,'') AS auction_title,C.auction_type_id, rtrim(ISNULL(U.first_name,'') + ' ' + ISNULL(U.last_name,'')) as bidder_name,ISNULL(U.email,'') as bidder_email" & _
                " from tbl_auction_bids A INNER JOIN tbl_reg_buyers B ON A.buyer_id=B.buyer_id INNER JOIN tbl_reg_buyer_users U on A.buyer_user_id=U.buyer_user_id INNER JOIN tbl_auctions C ON A.auction_id=C.auction_id where bid_id=" & bid_id
            dt = SqlHelper.ExecuteDatatable(strBid)
            If dt.Rows.Count > 0 Then
                buyer_email = dt.Rows(0)("email")
                buyer_name = dt.Rows(0)("buyer_name").ToString().Trim()
                bidder_email = dt.Rows(0)("bidder_email")
                bidder_name = dt.Rows(0)("bidder_name").ToString().Trim()

                bid_date = dt.Rows(0)("bid_date")
                body = body & Header()

                If dt.Rows(0)("action").ToString().ToUpper() = "Awarded".ToUpper() Then
                    strSubject = "Congratulations! Your bid won!"

                    body = body & "Dear " & bidder_name.Trim() & ","
                    body = body & "<br /><br />You have won the lot for <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>" & dt.Rows(0)("auction_title").ToString().Trim() & "</a>.<br /> Your bidding details are as follows:"
                    body = body & "<br /><br /><table cellspacing='0' cellpadding='0' width='100%' border='0'>"
                    body = body & "<tr><td width='22%'>Auction: " & "</td><td>" & dt.Rows(0)("auction_title") & "</td></tr>"
                    If dt.Rows(0)("auction_type_id") = 3 Then
                        body = body & "<tr><td>Your Max. Bid: " & "</td><td>$" & FormatNumber(dt.Rows(0)("max_bid_amount"), 2) & "</td></tr>"
                    End If
                    body = body & "<tr><td>Winning Bid: " & "</td><td>" & "$" & FormatNumber(dt.Rows(0)("bid_amount")) & "</td></tr>"
                    body = body & "<tr><td>Auction Date: " & "</td><td>" & Convert.ToDateTime(dt.Rows(0)("bid_date")).ToShortDateString() & "</td></tr>"
                    body = body & "<tr><td colspan='2'><br /><br />Your sales representative will contact you soon regarding this auction.</td></tr>"
                    body = body & "</table>"
                Else
                    strSubject = "Sorry, you were not the highest bidder."

                    body = body & "Dear " & bidder_name.Trim() & ","
                    body = body & "<br /><br />You were outbid on the " & dt.Rows(0)("auction_title").ToString().Trim() & " and the auction is now closed .<br /> Your bidding details are as follows:"
                    body = body & "<br /><br /><table cellspacing='0' cellpadding='0' width='100%' border='0'>"
                    body = body & "<tr><td width='22%'>Auction: " & "</td><td>" & dt.Rows(0)("auction_title") & "</td></tr>"
                    If dt.Rows(0)("auction_type_id") = 3 Then
                        body = body & "<tr><td>Your Max. Bid: " & "</td><td>$" & FormatNumber(dt.Rows(0)("max_bid_amount"), 2) & "</td></tr>"
                    End If
                    body = body & "<tr><td>Auction Date: " & "</td><td>" & Convert.ToDateTime(dt.Rows(0)("bid_date")).ToShortDateString() & "</td></tr>"
                    body = body & "<tr><td colspan='2'><br /><br />Please check back soon for more upcoming auctions. Thank you for your participation!</td></tr>"
                    body = body & "</table>"

                End If


                body = body & Footer()

                If for_mail_content = False Then
                    Dim comm As New CommonCode()
                    comm.sendEmail(SqlHelper.of_FetchKey("email_from"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, bidder_email, bidder_name, "", "", buyer_email & "," & SqlHelper.of_FetchKey("email_CC") & "," & SqlHelper.of_FetchKey("employee_to"), buyer_name)
                    str = "INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & buyer_email & "','" & strSubject & "','" & CommonCode.encodeSingleQuote(body) & "','') select scope_identity()"
                    Dim email_id As Integer = SqlHelper.ExecuteScalar(str.Replace("'", "'''"))
                    If email_id > 0 Then
                        SqlHelper.ExecuteNonQuery("update tbl_auction_bids set send_email=1,email_id=" & email_id & " where bid_id=" & bid_id)
                    End If
                    comm = Nothing
                End If

            End If
        End If
        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body

    End Function
    Public Function SendBuyNowAcceptEmail(ByVal buy_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String
        'Done
        Dim body As String = ""
        Dim strSubject As String = "Order Confirmation"
        If buy_id > 0 Then
            Dim dt As DataTable = New DataTable()
            Dim buyer_email As String = ""
            Dim buyer_name As String = ""
            Dim bid_date As DateTime = DateTime.Now
            Dim buy_type As String = ""
            Dim strBuy As String = "select A.bid_date,ISNULL(A.price,0) AS price,ISNULL(A.quantity,0) AS quantity,ISNULL(A.buy_type,'') As buy_type, C.auction_type_id," & _
                                    "ISNULL(B.contact_first_name,'') As buyer_name,ISNULL(B.email,'') As email," & _
                                    "ISNULL(C.title,'') AS auction_title from tbl_auction_buy A INNER JOIN tbl_reg_buyers B ON A.buyer_id=B.buyer_id " & _
                                    "INNER JOIN tbl_auctions C ON A.auction_id=C.auction_id where buy_id=" & buy_id
            dt = SqlHelper.ExecuteDatatable(strBuy)
            If dt.Rows.Count > 0 Then
                buyer_email = dt.Rows(0)("email")
                buyer_name = dt.Rows(0)("buyer_name").ToString().Trim()
                bid_date = dt.Rows(0)("bid_date")
                buy_type = IIf(dt.Rows(0)("buy_type").ToString() = "BUY NOW", "Buy it Now", dt.Rows(0)("buy_type").ToString())
                body = body & Header()
                body = body & "Dear " & buyer_name.Trim() & ","
                body = body & "<br /><br />Your " & buy_type & " order has been successfully received for <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>" & dt.Rows(0)("auction_title").ToString().Trim() & "</a>. Please see the confirmation details below."
                body = body & "<br /><br /><table cellspacing='0' cellpadding='0' width='100%' border='0'>"
                body = body & "<tr><td width='22%'>Auction: " & "</td><td>" & dt.Rows(0)("auction_title") & "</td></tr>"
                body = body & "<tr><td>Price: " & "</td><td>" & "$" & FormatNumber(dt.Rows(0)("price")) & "</td></tr>"
                If buy_type.ToUpper() = "PARTIAL" Then
                    body = body & "<tr><td>Quantity: " & "</td><td>" & FormatNumber(dt.Rows(0)("quantity"), 2) & "</td></tr>"
                End If

                body = body & "<td>Bidding date: " & "</td><td>" & Convert.ToDateTime(dt.Rows(0)("bid_date")).ToShortDateString() & "</td>"
                body = body & "</table>"
                body = body & "<br /><br />We will contact you shortly regarding your order."
                body = body & Footer()
                If for_mail_content = False Then
                    Dim comm As New CommonCode()
                    comm.sendEmail(SqlHelper.of_FetchKey("email_from"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, buyer_email, buyer_name, "", "", SqlHelper.of_FetchKey("email_CC") & "," & SqlHelper.of_FetchKey("employee_to"), "")
                    SqlHelper.ExecuteNonQuery("INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & buyer_email & "','Congratulation! your buy request has been accepted.','" & body & "','')")
                    comm = Nothing
                End If

            End If
        End If
        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body

    End Function
    Public Function SendQuotationAcceptEmail(ByVal quotation_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String
        'Done
        Dim body As String = ""
        Dim strSubject As String = "Thank you for submitting your offer!"
        If quotation_id > 0 Then
            Dim dt As DataTable = New DataTable()
            Dim buyer_email As String = ""
            Dim buyer_name As String = ""
            Dim bid_date As DateTime = DateTime.Now

            Dim strBuy As String = "select A.bid_date, C.auction_type_id,ISNULL(B.contact_first_name,'') As buyer_name,ISNULL(B.email,'') As email, " & _
                                    "ISNULL(C.title,'') AS auction_title from tbl_auction_quotations A INNER JOIN tbl_reg_buyers B ON A.buyer_id=B.buyer_id " & _
                                    "INNER JOIN tbl_auctions C ON A.auction_id=C.auction_id where quotation_id =" & quotation_id
            dt = SqlHelper.ExecuteDatatable(strBuy)
            If dt.Rows.Count > 0 Then
                buyer_email = dt.Rows(0)("email")
                buyer_name = dt.Rows(0)("buyer_name").ToString().Trim()
                bid_date = dt.Rows(0)("bid_date")

                body = body & Header()
                body = body & "Dear " & buyer_name.Trim() & ","
                body = body & "<br /><br />We have received your offer for <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>" & dt.Rows(0)("auction_title").ToString().Trim() & "</a>."
                body = body & "<br /><br />Your sales representative will contact you soon regarding your offer."
                body = body & Footer()

                If for_mail_content = False Then
                    Dim comm As New CommonCode()
                    comm.sendEmail(SqlHelper.of_FetchKey("email_from"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, buyer_email, buyer_name, "", "", SqlHelper.of_FetchKey("email_CC") & "," & SqlHelper.of_FetchKey("employee_to"), "")
                    SqlHelper.ExecuteNonQuery("INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & buyer_email & "','Congratulation! your request for offer has been approved.','" & body & "','')")

                    comm = Nothing
                End If

            End If
        End If
        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body

    End Function
    Public Sub sendInvitationBeforeMail()
        'Done
        Dim strQuery As String = "select E.schedule_type,E.email_schedule_id,A.auction_id,A.auction_type_id,A.title,A.start_date,A.display_end_time,A.start_price,B.first_name as name,B.email,B.buyer_id from dbo.fn_get_email_schedule_list() F inner join tbl_auction_email_schedules E on E.email_schedule_id=F.email_schedule_id inner join tbl_auctions A on A.auction_id=F.auction_id inner join tbl_reg_buyer_users B on B.buyer_id=F.buyer_id and B.is_admin=1"
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDatatable(strQuery)

        Dim dtBidders As New DataTable()
        Dim strBidders As String = "select distinct buyer_id from dbo.fn_get_email_schedule_list() "
        dtBidders = SqlHelper.ExecuteDatatable(strBidders)

        Dim buyer_email As String = ""
        Dim buyer_name As String = ""
        Dim auction_title As String = ""
        Dim start_price As Double = 0
        Dim start_date As DateTime = "1/1/1999"
        Dim end_date As DateTime = "1/1/1999"

        Dim span As TimeSpan
        Dim time_left As String = ""
        Dim no_of_days As Integer = 0
        Dim auc_id As Integer = 0
        Dim sce_id As Integer = 0

        Dim body As String = ""
        Dim strSubject As String = "Exciting Opportunity – Upcoming Auction"

        If dtBidders.Rows.Count > 0 Then
            For j As Integer = 0 To dtBidders.Rows.Count - 1
                body = ""
                body = body & Header()
                body = body & "Dear " & "$buyer$" & ","
                buyer_name = ""
                buyer_email = ""
                Dim buyer_id As Integer = 0
                buyer_id = dtBidders.Rows(j)("buyer_id")
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "buyer_id=" & dtBidders.Rows(j)("buyer_id") & " and schedule_type='Before Start'"
                For i As Integer = 0 To dv.Count - 1
                    With dv(i)
                        If auc_id <> .Item("auction_id") Or sce_id <> .Item("email_schedule_id") Then
                            SqlHelper.ExecuteNonQuery("INSERT INTO tbl_auction_email_schedule_statuses(email_schedule_id, auction_id,email_id) VALUES (" & .Item("email_schedule_id") & ", " & .Item("auction_id") & ",'" & .Item("email") & "')")
                        End If

                        auc_id = .Item("auction_id")
                        sce_id = .Item("email_schedule_id")
                        span = .Item("start_date").Subtract(DateTime.Now)

                        auction_title = .Item("title")
                        no_of_days = span.Days
                        start_date = .Item("start_date")
                        end_date = .Item("display_end_time")
                        If buyer_name.Trim() = "" Then
                            buyer_name = .Item("name").ToString().Trim()
                        End If
                        If buyer_email.Trim() = "" Then
                            buyer_email = .Item("email")
                        End If

                    End With

                    'body = body & "<br /><br />PCS Wireless Auctions would like to invite you to participate in the upcoming auction for " & auction_title & " " & IIf(no_of_days > 1, " in " & no_of_days & " more days", " tomorrow") & ".  This is an excellent deal that you could obtain at really promising prices. Please remember to log in on " & start_date.ToString() & " to begin the bid. "
                    body = body & "<br /><br />PCS Wireless Auctions would like to invite you to participate in the upcoming auction for " & auction_title & ".<br /><br /> Please remember to log in on " & Convert.ToDateTime(start_date.ToString()).ToShortDateString() & " at " & Convert.ToDateTime(start_date).ToShortTimeString() & " when bidding first opens. "
                    If start_price > 0 Then
                        body = body & "The minimum bidding amount is $" & FormatNumber(start_price, 2) & ". "
                    End If
                    body = body & "Bidding is scheduled to close on " & Convert.ToDateTime(end_date.ToString()).ToShortDateString() & " at " & Convert.ToDateTime(end_date).ToShortTimeString() & "."
                    body = body & "<br /><br />We look forward to having you participate in this exciting opportunity!"
                Next

                'body = body & "<br /><br /><a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/Unsubscription.aspx?id=" & Security.EncryptionDecryption.EncryptValueFormatted(buyer_id) & "&type=1' target='_blank'>To unsubscribe for invitation please click here</a>"
                body = body.Replace("$buyer$", buyer_name)
                body = body & Footer()
                Dim comm As New CommonCode()
                comm.sendEmail("", "", strSubject, body, buyer_email, buyer_name, "", "", SqlHelper.of_FetchKey("email_CC"), "")
                comm = Nothing
            Next
        End If
    End Sub
    Public Function sendInvitationBeforeMail_static() As String
        'not call from function call from EmailContent.aspx only
        'Done
        Dim buyer_email As String = ""
        Dim buyer_name As String = ""
        Dim auction_title As String = "[Title of the Auction]"
        Dim start_price As Double = 100
        Dim start_date As DateTime = Now.AddDays(1).AddHours(5).AddMinutes(15)
        Dim end_date As DateTime = Now.AddDays(3).AddHours(3).AddMinutes(5)

        Dim span As TimeSpan
        Dim time_left As String = ""
        Dim no_of_days As Integer = 0
        Dim auc_id As Integer = 0
        Dim sce_id As Integer = 0

        span = start_date.Subtract(DateTime.Now)
        no_of_days = span.Days

        Dim strSubject As String = "Exciting Opportunity – Upcoming Auction"
        Dim body As String = Header()
        body = body & "Dear [First Name],"

        body = body & "<br /><br />PCS Wireless Auctions would like to invite you to participate in the upcoming auction for " & auction_title & ".<br /><br /> Please remember to log in on " & Convert.ToDateTime(start_date.ToString()).ToShortDateString() & " at " & Convert.ToDateTime(start_date).ToShortTimeString() & " when bidding first opens. "
        If start_price > 0 Then
            body = body & "The minimum bidding amount is $" & FormatNumber(start_price, 2) & ". "
        End If
        body = body & "Bidding is scheduled to close on " & Convert.ToDateTime(end_date.ToString()).ToShortDateString() & " at " & Convert.ToDateTime(end_date).ToShortTimeString() & "."
        body = body & "<br /><br />We look forward to having you participate in this exciting opportunity!"

        body = body & Footer()

        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body

    End Function
    Public Sub sendInvitationBeforeEndMail()
        'Done
        Dim strQuery As String = "select E.schedule_type,E.email_schedule_id,A.auction_id,A.auction_type_id,A.title,A.start_date,A.display_end_time,A.start_price,B.first_name as name,B.email,B.buyer_id,ISNULL(IMG.stock_image_id, 0) AS stock_image_id,ISNULL(IMG.filename, '') AS filename from dbo.fn_get_email_schedule_list() F inner join tbl_auction_email_schedules E on E.email_schedule_id=F.email_schedule_id inner join tbl_auctions A on A.auction_id=F.auction_id  left join tbl_auction_stock_images IMG on A.auction_id=IMG.auction_id and IMG.stock_image_id=(SELECT top 1 stock_image_id from tbl_auction_stock_images where auction_id=a.auction_id order by position) inner join tbl_reg_buyer_users B on B.buyer_id=F.buyer_id and B.is_admin=1 "
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDatatable(strQuery)

        Dim dtBidders As New DataTable()
        Dim strBidders As String = "select distinct buyer_id from dbo.fn_get_email_schedule_list() "
        dtBidders = SqlHelper.ExecuteDatatable(strBidders)

        Dim buyer_email As String = ""
        Dim buyer_name As String = ""
        Dim auction_title As String = ""
        Dim start_price As Double = 0
        Dim start_date As DateTime = "1/1/1999"
        Dim end_date As DateTime = "1/1/1999"

        Dim span As TimeSpan
        Dim time_left As String = ""
        Dim no_of_days As Integer = 0
        Dim auc_id As Integer = 0
        Dim sce_id As Integer = 0
        Dim stock_image_id As Integer
        Dim filename As String
        Dim body As String = ""


        If dtBidders.Rows.Count > 0 Then
            For j As Integer = 0 To dtBidders.Rows.Count - 1
                body = ""
                body = body & Header()
                body = body & "Dear " & "$buyer$" & ","
                buyer_name = ""
                buyer_email = ""
                Dim buyer_id As Integer = 0
                buyer_id = dtBidders.Rows(j)("buyer_id")
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "buyer_id=" & dtBidders.Rows(j)("buyer_id") & " and schedule_type='Before End'"
                For i As Integer = 0 To dv.Count - 1
                    With dv(i)
                        If auc_id <> .Item("auction_id") Or sce_id <> .Item("email_schedule_id") Then
                            SqlHelper.ExecuteNonQuery("INSERT INTO tbl_auction_email_schedule_statuses(email_schedule_id, auction_id,email_id) VALUES (" & .Item("email_schedule_id") & ", " & .Item("auction_id") & ",'" & .Item("email") & "')")
                        End If

                        auc_id = .Item("auction_id")
                        sce_id = .Item("email_schedule_id")
                        span = .Item("start_date").Subtract(DateTime.Now)

                        auction_title = .Item("title")
                        no_of_days = span.Days
                        start_date = .Item("start_date")
                        end_date = .Item("display_end_time")
                        If buyer_name.Trim() = "" Then
                            buyer_name = .Item("name").ToString().Trim()
                        End If
                        If buyer_email.Trim() = "" Then
                            buyer_email = .Item("email")
                        End If

                        filename = .Item("filename")
                        stock_image_id = .Item("stock_image_id")
                    End With
                    body = body & "<br /><br />Time is running out to bid on this auction! The auction is scheduled to close at " & end_date & "."
                    body = body & "<br /><br />Please <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>log in</a> now to submit your bid."
                Next

                body = body.Replace("$buyer$", buyer_name)
                body = body & Footer()
                Dim comm As New CommonCode()
                comm.sendEmail("", "", "Time is running out! Bid now on " & auction_title & "!", body, buyer_email, buyer_name, "", "", SqlHelper.of_FetchKey("email_CC"), "")
                comm = Nothing
            Next
        End If
    End Sub
    Public Function sendInvitationBeforeEndMail_static() As String
        'Done
        Dim buyer_email As String = ""
        Dim buyer_name As String = ""
        Dim auction_title As String = "[Title of the Auction]"
        Dim start_price As Double = 100
        Dim start_date As DateTime = Now.AddDays(-1).AddHours(5).AddMinutes(15)
        Dim end_date As DateTime = Now.AddDays(3).AddHours(3).AddMinutes(5)

        Dim time_left As String = ""
        Dim no_of_days As Integer = 0
        Dim auc_id As Integer = 0
        Dim sce_id As Integer = 0

        Dim strSubject As String = "Time is running out! Bid now on " & auction_title & "!"
        Dim body As String = Header()
        body = body & "Dear [first name],"


        body = body & "<br /><br />Time is running out to bid on this auction! The auction is scheduled to close at " & end_date & "."
        body = body & "<br /><br />Please <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>log in</a> now to submit your bid."
        body = body & Footer()

        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body

    End Function
    Public Sub sendInvitationAfterMail()
        'Done
        Dim strQuery As String = "select E.schedule_type,E.email_schedule_id,A.auction_id,A.auction_type_id,A.title,A.start_date,A.display_end_time,A.start_price,B.first_name as name,B.email,B.buyer_id from dbo.fn_get_email_schedule_list() F inner join tbl_auction_email_schedules E on E.email_schedule_id=F.email_schedule_id inner join tbl_auctions A on A.auction_id=F.auction_id inner join tbl_reg_buyer_users B on B.buyer_id=F.buyer_id and B.is_admin=1"
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDatatable(strQuery)

        Dim dtBidders As New DataTable()
        Dim strBidders As String = "select distinct buyer_id from dbo.fn_get_email_schedule_list() "
        dtBidders = SqlHelper.ExecuteDatatable(strBidders)

        Dim buyer_email As String = ""
        Dim buyer_name As String = ""
        Dim auction_title As String = ""

        Dim span As TimeSpan
        Dim time_left As String = ""
        Dim no_of_days As Integer = 0
        Dim auc_id As Integer = 0
        Dim sce_id As Integer = 0

        Dim body As String = ""


        If dtBidders.Rows.Count > 0 Then
            For j As Integer = 0 To dtBidders.Rows.Count - 1
                body = ""
                buyer_name = ""
                buyer_email = ""
                body = body & Header()
                body = body & "Hello " & "$buyer$" & ","
                body = body & "<br /><br />PCS thanks to you for bidding on the following auctions: "
                Dim buyer_id As Integer = 0
                buyer_id = dtBidders.Rows(j)("buyer_id")
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "buyer_id=" & dtBidders.Rows(j)("buyer_id") & " and schedule_type='After End'"

                For i As Integer = 0 To dv.Count - 1
                    With dv(i)
                        If auc_id <> .Item("auction_id") Or sce_id <> .Item("email_schedule_id") Then
                            SqlHelper.ExecuteNonQuery("INSERT INTO tbl_auction_email_schedule_statuses(email_schedule_id, auction_id,email_id) VALUES (" & .Item("email_schedule_id") & ", " & .Item("auction_id") & ",'" & .Item("email") & "')")
                        End If

                        auc_id = .Item("auction_id")
                        sce_id = .Item("email_schedule_id")
                        span = .Item("start_date").Subtract(DateTime.Now)

                        auction_title = .Item("title")
                        no_of_days = span.Days

                        If buyer_name.Trim() = "" Then
                            buyer_name = .Item("name").ToString().Trim()
                        End If
                        If buyer_email.Trim() = "" Then
                            buyer_email = .Item("email")
                        End If
                    End With
                    body = body & "<br />" & auction_title
                Next
                body = body & "<br /><br />This is a really lucrative opportunity. We will periodically keep coming up with such profitable ventures."
                body = body.Replace("$buyer$", buyer_name)
                body = body & "<br /><br />It was your enthusiastic participation that made bidding such a delightful experience. We will keep you posted on many more bids that will open on the only premiere business bidding site: <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>auctions.pcsww.com</a>."
                body = body & "<br /><br />With us, you could count on excellent deal & promising prices. We will periodically keep coming up with such profitable ventures."
                body = body & "<br /><br /><a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/Unsubscription.aspx?id=" & Security.EncryptionDecryption.EncryptValueFormatted(buyer_id) & "&type=1' target='_blank' >" & "To unsubscribe for invitation please click here</a>"
                body = body & Footer()
                Dim comm As New CommonCode()
                comm.sendEmail("", "", "Thank you for bidding.", body, buyer_email, buyer_name, "", "", SqlHelper.of_FetchKey("email_CC"), "")
                comm = Nothing
            Next
        End If

    End Sub
    Public Function sendInvitationAfterMail_static(ByVal buyer_id As Integer) As String
        'Done
        Dim auction_title As String = "[Title of the Auction]"

        Dim strSubject As String = "Thank you for bidding."
        Dim body As String = Header()
        body = body & "Hello [first name],"
        body = body & "<br /><br />PCS thanks to you for bidding on the following auctions: "

        body = body & "<br />" & auction_title
        body = body & "<br /><br />This is a really lucrative opportunity. We will periodically keep coming up with such profitable ventures."
        body = body & "<br /><br />It was your enthusiastic participation that made bidding such a delightful experience. We will keep you posted on many more bids that will open on the only premiere business bidding site: <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank'>auctions.pcsww.com</a>."
        body = body & "<br /><br />With us, you could count on excellent deal & promising prices. We will periodically keep coming up with such profitable ventures."
        body = body & "<br /><br /><a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/Unsubscription.aspx?id=" & Security.EncryptionDecryption.EncryptValueFormatted(buyer_id) & "&type=1' target='_blank' >" & "To unsubscribe for invitation please click here</a>"
        body = body & Footer()


        Return "<b>Subject:</b> " & strSubject & "<br><br>" & body
    End Function

End Class
