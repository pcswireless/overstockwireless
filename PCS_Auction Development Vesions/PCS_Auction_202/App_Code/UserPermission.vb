﻿Imports Microsoft.VisualBasic

Public Class UserPermission
    Private _CanCreateSubSeller As Boolean = False
    Private _CanCreateAuction As Boolean = False
    Private _CanViewReport As Boolean = False
    Private _CanManageMessageBoard As Boolean = False
    Private _CanEditSeller As Boolean = False
    Private _CanEditAuctionier As Boolean = False
    Private _CanEditBidder As Boolean = False
    Private _CanAssignAuctioneer As Boolean = False
    Private _CanManageBuyerTab As Boolean = False
    Private _CanViewEditAssignBidders As Boolean = False

    Public Property CanCreateSubSeller() As Boolean
        Get
            Return _CanCreateSubSeller
        End Get
        Set(ByVal value As Boolean)
            _CanCreateSubSeller = value
        End Set
    End Property

    Public Property CanCreateAuction() As Boolean
        Get
            Return _CanCreateAuction
        End Get
        Set(ByVal value As Boolean)
            _CanCreateAuction = value
        End Set
    End Property

    Public Property CanViewReport() As Boolean
        Get
            Return _CanViewReport
        End Get
        Set(ByVal value As Boolean)
            _CanViewReport = value
        End Set
    End Property
    Public Property CanManageMessageBoard() As Boolean
        Get
            Return _CanManageMessageBoard
        End Get
        Set(ByVal value As Boolean)
            _CanManageMessageBoard = value
        End Set
    End Property
    Public Property CanEditSeller() As Boolean
        Get
            Return _CanEditSeller
        End Get
        Set(ByVal value As Boolean)
            _CanEditSeller = value
        End Set
    End Property
    Public Property CanEditAuctionier() As Boolean
        Get
            Return _CanEditAuctionier
        End Get
        Set(ByVal value As Boolean)
            _CanEditAuctionier = value
        End Set
    End Property
    Public Property CanEditBidder() As Boolean
        Get
            Return _CanEditBidder
        End Get
        Set(ByVal value As Boolean)
            _CanEditBidder = value
        End Set
    End Property
    Public Property CanAssignAuctioneer() As Boolean
        Get
            Return _CanAssignAuctioneer
        End Get
        Set(ByVal value As Boolean)
            _CanAssignAuctioneer = value
        End Set
    End Property
    Public Property CanManageBuyerTab() As Boolean
        Get
            Return _CanManageBuyerTab
        End Get
        Set(ByVal value As Boolean)
            _CanManageBuyerTab = value
        End Set
    End Property
    Public Property CanViewEditAssignBidders() As Boolean
        Get
            Return _CanViewEditAssignBidders
        End Get
        Set(ByVal value As Boolean)
            _CanViewEditAssignBidders = value
        End Set
    End Property

End Class
