﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI.WebControls
Imports System.Web.UI
Imports System.Text

''' <summary>
''' Summary description for StringCompareValidator
''' </summary>
Namespace RampValidators
    Public Class StringCompareValidator
        Inherits BaseValidator
        Public Enum OperatorType
            Equal = 1
            NotEqual = 2
        End Enum

        Public Sub New()

            IgnoreCase = False
            TrimText = False
            [Operator] = OperatorType.Equal
        End Sub

        Public Property ControlToCompare() As String
            Get
                Return m_ControlToCompare
            End Get
            Set(ByVal value As String)
                m_ControlToCompare = Value
            End Set
        End Property
        Private m_ControlToCompare As String

        Public Property IgnoreCase() As Boolean
            Get
                Return m_IgnoreCase
            End Get
            Set(ByVal value As Boolean)
                m_IgnoreCase = Value
            End Set
        End Property
        Private m_IgnoreCase As Boolean

        Public Property TrimText() As Boolean
            Get
                Return m_TrimText
            End Get
            Set(ByVal value As Boolean)
                m_TrimText = Value
            End Set
        End Property
        Private m_TrimText As Boolean

        Public Property [Operator]() As OperatorType
            Get
                Return m_Operator
            End Get
            Set(ByVal value As OperatorType)
                m_Operator = Value
            End Set
        End Property
        Private m_Operator As OperatorType

        Protected Overrides Function ControlPropertiesValid() As Boolean
            Dim ctrlToValidate As Control = FindControl(ControlToValidate)
            Dim ctrlToCompare As Control = FindControl(ControlToCompare)

            If ctrlToValidate IsNot Nothing AndAlso ctrlToCompare IsNot Nothing Then
                Dim tbCtrlToValidate As TextBox = TryCast(ctrlToValidate, TextBox)
                Dim tbCtrlToCompare As TextBox = TryCast(ctrlToCompare, TextBox)

                Return (tbCtrlToCompare IsNot Nothing AndAlso tbCtrlToValidate IsNot Nothing)
            Else
                Return False
            End If
        End Function

        Protected Overrides Function EvaluateIsValid() As Boolean
            Dim ctrlToValidateText As String = String.Empty
            Dim ctrlToCompareText As String = String.Empty

            Dim tbCtrlToValidate As TextBox = TryCast(FindControl(ControlToValidate), TextBox)
            Dim tbCtrlToCompare As TextBox = TryCast(FindControl(ControlToCompare), TextBox)

            If tbCtrlToValidate Is Nothing OrElse tbCtrlToCompare Is Nothing Then
                Return False
            End If

            If Not IgnoreCase AndAlso Not TrimText Then
                ctrlToValidateText = tbCtrlToValidate.Text
                ctrlToCompareText = tbCtrlToCompare.Text
            Else
                If IgnoreCase Then
                    ctrlToValidateText = tbCtrlToValidate.Text.ToLower()
                    ctrlToCompareText = tbCtrlToCompare.Text.ToLower()
                End If

                If TrimText Then
                    ctrlToValidateText = ctrlToValidateText.Trim()
                    ctrlToCompareText = ctrlToCompareText.Trim()
                End If
            End If

            If [Operator] = OperatorType.Equal Then
                Return (ctrlToValidateText = ctrlToCompareText)
            Else
                Return (ctrlToValidateText <> ctrlToCompareText)
            End If
        End Function

        Protected Overrides Sub OnPreRender(ByVal e As EventArgs)
            MyBase.OnPreRender(e)
            If EnableClientScript Then
                ClientScript()
            End If
        End Sub

        Private Sub ClientScript()
            Dim ctrlToValidate As Control = FindControl(ControlToValidate)
            Dim ctrlToCompare As Control = FindControl(ControlToCompare)
            If ctrlToValidate Is Nothing OrElse ctrlToCompare Is Nothing Then
                Return
            End If

            Dim sb_Script As New StringBuilder()
            sb_Script.Append("<script language=""javascript"">")
            sb_Script.Append("function StringCompareValidator(sender) {")
            sb_Script.Append("ctrlToValidate = document.getElementById('" & ctrlToValidate.ClientID & "');")
            sb_Script.Append("ctrlToCompare = document.getElementById('" & ctrlToCompare.ClientID & "');")

            sb_Script.Append("if(ctrlToValidate == null || ctrlToCompare == null) return false;")

            sb_Script.Append("var ctrlToValidateText = ctrlToValidate.value;")
            sb_Script.Append("var ctrlToCompare = ctrlToCompare.value;")

            If IgnoreCase Then
                sb_Script.Append("ctrlToValidateText = ctrlToValidateText.toLowerCase();")
                sb_Script.Append("ctrlToCompare = ctrlToCompare.toLowerCase();")
            End If

            If TrimText Then
                sb_Script.Append(" rtrim = /^(\s| )+|(\s| )+$/g;")

                sb_Script.Append("ctrlToValidateText = (ctrlToValidateText || '').replace( rtrim, '' );")
                sb_Script.Append("ctrlToCompare = (ctrlToCompare || '').replace( rtrim, '' );")
            End If

            If [Operator] = OperatorType.Equal Then
                sb_Script.Append("return (ctrlToValidateText == ctrlToCompare)")
            Else
                sb_Script.Append("return (ctrlToValidateText != ctrlToCompare)")
            End If

            sb_Script.Append("}")
            sb_Script.Append("</script>")

            'Inject the script into the page
            Page.ClientScript.RegisterClientScriptBlock([GetType](), "StringCompareValidatorScriptBlock", sb_Script.ToString())
            'Registering validator clientside javascript function
            Page.ClientScript.RegisterExpandoAttribute(ClientID, "evaluationfunction", "StringCompareValidator")
        End Sub
    End Class

End Namespace