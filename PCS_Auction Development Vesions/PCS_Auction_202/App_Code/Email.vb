﻿Imports Microsoft.VisualBasic

Public Class Email

    'Key use in Email_Html

    'User Name -- <<user_name>> eg. user_first_name + user_last_name
    'Server URL -- <<site_url>> eg: http://eplanet1-pc:150
    'Company Name -- <<company_name>> eg. PCS
    'Company Website --<<company_website>> eg. website
    'Bidder Name -- <<bidder_name>> eg: contact_first_name
    'Bidder First Name -- <<bidder_first_name>> eg. contact_first_name
    'Bidder Last Name -- <<bidder_last_name>> eg. contact_last_name
    'Bidder Tittle -- <<bidder_title>> eg. contact_title
    'Bidder Address1 --<<bidder_address1>> eg. address1
    'Bidder Address2 --<<bidder_address2>> eg. address2
    'Bidder City --<<bidder_city>>  eg. city
    'Bidder State --<<bidder_state>>    eg. state_text
    'Bidder Country --<<bidder_country>> eg. country
    'Bidder Zip --<<bidder_zip>>    eg. zip
    'Bidder Phone1 --<<bidder_mobile>>  eg. mobile
    'Bidder Phone2 --<<bidder_phone>>   eg. phone
    'Bidder Fax --<<bidder_fax>>    eg. fax
    'Bidder Comment --<<bidder_comment>>    eg. comment
    'Bidder Status --<<bidder_status>>  eg. status
    'Bidder Business Type --<<bidder_business_type>>    eg. business_type
    'Bidder Industry Type --<<bidder_industry_type>>    eg. industry_type
    'Bidder Find us --<<bidder_find_us>>    eg. Friend, Website
    'Bidder Email --<<bidder_email>>    eg. email
    'Bidding Date -- <<bid_date>>   eg. bid_date
    'Bidding Start Price -- <<bidding_start_price>> eg. start_price
    'Max Bid Amount -- <<max_bid_amount>>   eg. max_bid_amount
    'Auction Title -- <<auction_title>> eg. auction_title
    'Buyer ID -- <<bidder_id>> eg. buyer_id
    'Auction Start Date -- <<auction_start_date>>   eg. start_date 
    'Auction Start Time -- <<auction_start_time>>   eg. start_date converted to ToShortTimeString
    'Auction End Time -- <<auction_end_time>>   eg. display_end_time converted to ToShortTimeString
    'Auction End Date -- <<auction_end_date>>   eg. display_end_time
    'Auction Status -- <<auction_status>>   eg. auction_status
    'Auction Price -- <<auction_price>> eg. price
    'Auction Quantity -- <<auction_quantity>>   eg. quantity
    'Buy Type -- <<buy_type>>   eg.Buy it Now  
    'Winning Bid Amount -- <<win_bid_amount>>   eg.bid_amount
    'Paypal Payment Link -- <<payment_link>> eg. PaypalRequest.aspx
    Public Function Header() As String
        Dim str As String = ""

        str = str & "<table cellpadding='0' cellspacing='0' border='0' width='700' style='border: solid 1px #d0d0d0;'>"
        str = str & "<tr><td style='padding:5px;'>"
        str = str & "<img src='" & SqlHelper.of_FetchKey("ServerHttp") & "/images/logo.gif'>"
        str = str & "<table cellpadding='0' cellspacing='0' border='0' width='700' style='padding: 3px 3px 3px 3px;'>"
        str = str & "<tr height='5'><td></td></tr>"
        str = str & "<tr><td align='left' style='font-size:12px; color:black;font-family:arial;'>"

        Return str
    End Function
    Public Function Footer() As String
        Dim str As String = ""

        str = str & "<table cellpadding='0' cellspacing='0' border='0' width='692' style='font-family:arial;'>"

        str = str & "<tr><td style='font-size:12px; color:black; padding:5px 0px;' align='left'>"
        str = str & "<br><br><b>Moderator</b><br><b>PCS Auctions</b><br><a href='" & SqlHelper.of_FetchKey("ServerHttp") & "' target='_blank' style='color:#0082c4;text-decoration:none;'>www.overstockwireless.com</a><br><br>"
        str = str & "</td></tr>"
        str = str & "<tr><td style='font-size:11px; font-family:arial;' align='left'><br />If you have any questions, please Contact Customer Service by e-mail at <a href='mailto:" & SqlHelper.of_FetchKey("site_email_to") & "' style='color:#0082c4;text-decoration:none;'>" & SqlHelper.of_FetchKey("site_email_to") & "</a> or by phone at 973-805-7400 ext. 179."
        str = str & "</td></tr>"
        str = str & "<tr><td style='font-size:11px; color:gray; font-family:arial;' align='left'>"
        str = str & "<hr>"
        str = str & "NOTICE:This E-mail (including attachments) is covered by the Electronic Communications Privacy Act, 18 U.S.C. §§ 2510-2521, is confidential, and may be legally privileged. The information is solely for the use of the addressee named above. If you are not the intended recipient, any disclosure, copying, distribution or other use of the contents of this information is strictly prohibited. If you have received this e-mail in error, please forward it to <a href='mailto:" & SqlHelper.of_FetchKey("site_email_to") & "' style='color:#0082c4;text-decoration:none;'>" & SqlHelper.of_FetchKey("site_email_to") & "</a> and delete this message."
        str = str & "<hr>"
        str = str & "</td></tr>"
        str = str & "<tr><td style='font-size:11px; color:gray; font-family:arial;' align='center'>"
        str = str & "<br><br>"
        str = str & "Copyright &copy; PCS Auction"
        str = str & "<br>"
        str = str & "</td></tr>"

        str = str & "</table>"

        ' CLOSE HEADER TABLE
        str = str & "</td></tr>"
        str = str & "</table>"
        str = str & "</td></tr>"
        str = str & "</table>"

        Return str
    End Function
    Public Function Send_Registration_Mail(ByVal user_email As String, ByVal user_name As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim email_caption As String = "Bidder Registration Email"
        Dim body = "", strSubject As String = ""
        Dim comm As New CommonCode()
        Dim ds As New DataSet
        ds = comm.GetEmail_ByCode("em2")
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            body = ds.Tables(0).Rows(0)("html_body")
           ' HttpContext.Current.Response.Write(body)
            body = body.Replace("<<user_name>>", user_name)
            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))

            strSubject = ds.Tables(0).Rows(0)("email_subject")
            email_caption = ds.Tables(0).Rows(0)("email_name")
            If for_mail_content = False Then
                comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), strSubject, body, user_email, "", "", "", "", "")
            End If
        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
    Public Function Bidderd_Admin_Active(ByVal buyer_id As Int32, Optional ByVal for_mail_content As Boolean = False) As String
        Dim email_caption As String = "Activate New Bidder"
        Dim body As String = ""
        Dim strSubject As String = ""
        Dim comm As New CommonCode()
        Dim ds As DataSet = New DataSet()
        Dim strQuery As String = ""
        strQuery = "SELECT A.buyer_id, " & _
                      "ISNULL(U.username, '') AS username, " & _
                      "ISNULL(A.company_name, '') AS company_name, " & _
                      "ISNULL(A.contact_title, '') AS contact_title, " & _
                      "ISNULL(A.contact_first_name, '') AS contact_first_name, " & _
                      "ISNULL(A.contact_last_name, '') AS contact_last_name, " & _
                      "ISNULL(A.email, '') AS email, " & _
                      "ISNULL(A.website, '') AS website, " & _
                      "ISNULL(A.phone, '') AS phone, " & _
                      "ISNULL(A.mobile, '') AS mobile, " & _
                      "ISNULL(A.fax, '') AS fax," & _
                      "ISNULL(A.address1, '') AS address1, " & _
                      "ISNULL(A.address2, '') AS address2, " & _
                      "ISNULL(A.city, '') AS city, " & _
                      "ISNULL(state_text, '') AS state," & _
                      "ISNULL(A.zip, '') AS zip, " & _
                     "ISNULL(C.name, '') AS country, " & _
                     "ISNULL(A.comment, '') AS comment, " & _
                     "ISNULL(S.status, '') AS status, " & _
        "isnull((Stuff((Select ', ' + name From tbl_master_how_find_us Where  how_find_us_id in (select how_find_us_id from tbl_reg_buyer_find_us_assignment where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as find_us," & _
         "isnull((Stuff((Select ', ' + company_name From tbl_reg_sellers Where  seller_id in (select seller_id from tbl_reg_buyer_seller_mapping where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as company_name " & _
                      " FROM tbl_reg_buyers A Left JOIN tbl_master_countries C ON A.country_id=C.country_id Left join tbl_reg_buyer_users U on A.buyer_id=U.buyer_id Left join tbl_reg_buyser_statuses S on A.status_id=S.status_id WHERE A.buyer_id =" & buyer_id & " and U.is_admin=1"

        '"isnull((Stuff((Select ', ' + name From tbl_master_business_types Where  business_type_id in (select business_type_id from tbl_reg_buyer_business_type_mapping where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as business_type," & _
        '"isnull((Stuff((Select ', ' + name From tbl_master_industry_types Where  industry_type_id in (select industry_type_id from tbl_reg_buyer_industry_type_mapping where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as industry_type," & _

        Dim ds_email As New DataSet
        ds_email = comm.GetEmail_ByCode("em10", strQuery)

        If ds_email.Tables.Count = 2 AndAlso ds_email.Tables(1).Rows.Count > 0 AndAlso ds_email.Tables(0).Rows.Count > 0 Then
            'ds.Tables.Add(ds_email.Tables(1)) 'assign ds_email second table to ds
            body = ds_email.Tables(0).Rows(0)("html_body")
            body = body.Replace("<<user_name>>", ds_email.Tables(1).Rows(0)("username"))
            body = body.Replace("<<company_name>>", ds_email.Tables(1).Rows(0)("company_name"))
            body = body.Replace("<<company_website>>", ds_email.Tables(1).Rows(0)("website"))
            body = body.Replace("<<bidder_first_name>>", ds_email.Tables(1).Rows(0)("contact_first_name"))
            body = body.Replace("<<bidder_last_name>>", ds_email.Tables(1).Rows(0)("contact_last_name"))
            body = body.Replace("<<bidder_title>>", ds_email.Tables(1).Rows(0)("contact_title"))
            body = body.Replace("<<bidder_address1>>", ds_email.Tables(1).Rows(0)("address1"))
            body = body.Replace("<<bidder_address2>>", ds_email.Tables(1).Rows(0)("address2"))
            body = body.Replace("<<bidder_city>>", ds_email.Tables(1).Rows(0)("city"))
            body = body.Replace("<<bidder_country>>", ds_email.Tables(1).Rows(0)("country"))
            body = body.Replace("<<bidder_state>>", ds_email.Tables(1).Rows(0)("state"))
            body = body.Replace("<<bidder_zip>>", ds_email.Tables(1).Rows(0)("zip"))
            body = body.Replace("<<bidder_mobile>>", ds_email.Tables(1).Rows(0)("mobile"))
            body = body.Replace("<<bidder_phone>>", ds_email.Tables(1).Rows(0)("phone"))
            body = body.Replace("<<bidder_email>>", ds_email.Tables(1).Rows(0)("email"))
            body = body.Replace("<<bidder_fax>>", ds_email.Tables(1).Rows(0)("fax"))
            body = body.Replace("<<bidder_comment>>", ds_email.Tables(1).Rows(0)("comment"))
            body = body.Replace("<<bidder_status>>", ds_email.Tables(1).Rows(0)("status"))
            'body = body.Replace("<<bidder_business_type>>", ds_email.Tables(1).Rows(0)("business_type"))
            'body = body.Replace("<<bidder_industry_type>>", ds_email.Tables(1).Rows(0)("industry_type"))
            body = body.Replace("<<bidder_find_us>>", ds_email.Tables(1).Rows(0)("find_us"))
            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
            strSubject = ds_email.Tables(0).Rows(0)("email_subject")
            email_caption = ds_email.Tables(0).Rows(0)("email_name")
            If for_mail_content = False Then
                Dim email As String = CommonCode.fetch_profile_user_emails("AU")
                If email <> "" Then
                    comm.sendEmail("", "", strSubject, body, email, "", "", "", "", "")
                End If
            End If
        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function

    Public Function Send_Registration_Approve_Mail(ByVal buyer_user_id As Integer, ByVal user_email As String, ByVal user_name As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim strSubject As String = ""
        Dim str As String = "select first_name As name,email,username,password,ISNULL(is_active,0) As is_active  from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id
        Dim comm As New CommonCode()
        Dim username As String = ""
        Dim password As String = ""
        Dim email_caption = "Email for Bidder Registration Approval", email As String = ""
        Dim name As String = ""
        Dim is_active As Boolean = False

        Dim body As String = ""
        Dim ds As New DataSet
        ds = comm.GetEmail_ByCode("em11", str)
        If ds.Tables.Count = 2 AndAlso (ds.Tables(0).Rows.Count > 0 And ds.Tables(1).Rows.Count > 0) Then
            username = ds.Tables(1).Rows(0)("username")
            password = Security.EncryptionDecryption.DecryptValue(ds.Tables(1).Rows(0)("password"))
            email = ds.Tables(1).Rows(0)("email")
            name = ds.Tables(1).Rows(0)("name")

            body = ds.Tables(0).Rows(0)("html_body")
            email_caption = ds.Tables(0).Rows(0)("email_name")
            strSubject = ds.Tables(0).Rows(0)("email_subject")

            body = body.Replace("<<bidder_name>>", name)
            body = body.Replace("<<user_name>>", user_name)
            body = body.Replace("<<password>>", password)
            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))

            If for_mail_content = False Then
                comm.sendEmail("", "", strSubject, body, user_email, "", "", "", "", "")
            End If
        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
    Public Function Send_ChangePassword_Mail(ByVal user_email As String, ByVal user_name As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim email_caption As String = "Password Change Confirmation Email"
        Dim body As String = ""
        Dim strSubject As String = ""
        Dim comm As New CommonCode()
        Dim ds As DataSet = New DataSet()
        ds = comm.GetEmail_ByCode("em12")
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            body = ds.Tables(0).Rows(0)("html_body")
            body = body.Replace("<<user_name>>", user_name)
            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
            strSubject = ds.Tables(0).Rows(0)("email_subject")
            email_caption = ds.Tables(0).Rows(0)("email_name")
            If for_mail_content = False Then
                comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), strSubject, body, user_email, "", "", "", "", "")
            End If
        End If
        ds.Dispose()
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
    Public Function Send_Forgot_Pwd(ByVal pwd As String, ByVal user_email As String, ByVal usr As String, ByVal user_name As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim email_caption As String = "Forgotten Password"
        Dim body As String = ""
        Dim strSubject As String = ""
        Dim comm As New CommonCode()
        Dim ds As DataSet = New DataSet()
        ds = comm.GetEmail_ByCode("em13")
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            body = ds.Tables(0).Rows(0)("html_body")
            body = body.Replace("<<user_name>>", user_name)
            body = body.Replace("<<password>>", pwd)
            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
            strSubject = ds.Tables(0).Rows(0)("email_subject")
            email_caption = ds.Tables(0).Rows(0)("email_name")
            If for_mail_content = False Then
                comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), strSubject, body, user_email, "", "", "", "", "")
            End If
        End If
        ds.Dispose()
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
    Public Function send_buyer_approval_email(ByVal buyer_id As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim strSubject As String = ""

        Dim comm As New CommonCode()
        Dim body = "", email_caption As String = "Send buyer approval Email"
        Dim user_name As String = " "
        Dim password As String = " "
        Dim buyer_email As String = " "
        Dim buyer_name As String = " "
        If IsNumeric(buyer_id) Then
            Dim str1 As String = "select username,password from tbl_reg_buyer_users where ISNULL(is_admin,0)=1 and buyer_id=" & buyer_id
            ' Dim dt2 As DataTable = SqlHelper.ExecuteDatatable(str1)
            Dim str As String = "select buyer_id,ISNULL(contact_first_name,'') AS name,ISNULL(email,'') As email from tbl_reg_buyers where buyer_id=" & buyer_id
            ' Dim dt As DataTable = New DataTable()
            'dt = SqlHelper.ExecuteDatatable(str)

            Dim ds As New DataSet
            ds = comm.GetEmail_ByCode("em14", str1 & ";" & str)
            If ds.Tables.Count = 3 AndAlso (ds.Tables(0).Rows.Count > 0 And ds.Tables(2).Rows.Count > 0) Then
                buyer_email = ds.Tables(2).Rows(0)("email")
                buyer_name = ds.Tables(2).Rows(0)("name").ToString().Trim()

                If ds.Tables(1).Rows.Count > 0 Then
                    user_name = ds.Tables(1).Rows(0)("username")
                    password = Security.EncryptionDecryption.DecryptValue(ds.Tables(1).Rows(0)("password"))
                End If

                body = ds.Tables(0).Rows(0)("html_body")
                email_caption = ds.Tables(0).Rows(0)("email_name")
                strSubject = ds.Tables(0).Rows(0)("email_subject")

                body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                body = body.Replace("<<bidder_name>>", buyer_name)
                body = body.Replace("<<user_name>>", user_name)
                body = body.Replace("<<password>>", password)

                If for_mail_content = False Then
                    comm.sendEmail(SqlHelper.of_FetchKey("email_from"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, buyer_email, buyer_name, "", "", "", "")
                End If
            End If
        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
    Public Function send_buyer_status_changed_email(ByVal buyer_id As String, ByVal status As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim email_caption As String = "Access restricted on PCS Auction"
        Dim body = "", strSubject As String = ""
        Dim buyer_email As String = ""
        Dim buyer_name As String = ""
        Dim comm As New CommonCode()
        If IsNumeric(buyer_id) Then
            Dim ds As New DataSet
            ds = comm.GetEmail_ByCode("em15", "select buyer_id,ISNULL(contact_first_name,'') AS name,ISNULL(email,'') As email from tbl_reg_buyers where buyer_id=" & buyer_id)
            If ds.Tables.Count = 2 AndAlso (ds.Tables(0).Rows.Count > 0 And ds.Tables(1).Rows.Count > 0) Then
                body = ds.Tables(0).Rows(0)("html_body")
                email_caption = ds.Tables(0).Rows(0)("email_name")
                strSubject = ds.Tables(0).Rows(0)("email_subject")


                buyer_email = ds.Tables(1).Rows(0)("email")
                buyer_name = ds.Tables(1).Rows(0)("name")
                status = IIf(status.ToLower() = "on hold", "put on hold", IIf(status = "Reject", "deactivated", status))

                body = body.Replace("<<bidder_name>>", buyer_name)
                body = body.Replace("<<bidder_status>>", status)
                body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))

                If for_mail_content = False Then
                    comm.sendEmail(SqlHelper.of_FetchKey("email_from"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, buyer_email, buyer_name, "", "", "", "")
                End If
            End If
        End If

        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
    Public Function send_bidder_sub_login_creation_email(ByVal buyer_user_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String
        Dim str As String = "select first_name As name,email,username,password,ISNULL(is_active,0) As is_active  from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id
        Dim dt As DataTable
        dt = SqlHelper.ExecuteDatatable(str)
        Dim strSubject As String = ""
        Dim user_name As String = ""
        Dim password As String = ""
        Dim email As String = ""
        Dim name As String = ""
        Dim email_caption As String = ""
        Dim is_active As Boolean = False

        Dim comm As New CommonCode()

        Dim ds As New DataSet
        Dim body As String = ""
        If dt.Rows.Count > 0 Then
            user_name = dt.Rows(0)("username")
            password = Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("password"))
            email = dt.Rows(0)("email")
            name = dt.Rows(0)("name")

            is_active = dt.Rows(0)("is_active")


            If is_active Then
                ds = comm.GetEmail_ByCode("em16")
                email_caption = "Bidder Sub-Login Details created and Now it is Active"
            Else
                ds = comm.GetEmail_ByCode("em17")
                email_caption = "Bidder Sub-Login Created but in-Active"
            End If
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                body = ds.Tables(0).Rows(0)("html_body")
                email_caption = ds.Tables(0).Rows(0)("email_name")
                strSubject = ds.Tables(0).Rows(0)("email_subject")

                body = body.Replace("<<bidder_name>>", dt.Rows(0)("name"))
                body = body.Replace("<<user_name>>", user_name)
                body = body.Replace("<<password>>", password)
                body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
            End If
            If for_mail_content = False Then
                comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), strSubject, body, email, "", "", "", "", "")
            End If

        End If

        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body

    End Function
    Public Function send_bidder_sub_login_active_status_changed_email(ByVal buyer_user_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String
        Dim str As String = "select first_name As name,email,username,password,ISNULL(is_active,0) As is_active  from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id
        Dim dt As DataTable
        dt = SqlHelper.ExecuteDatatable(str)
        Dim strSubject As String = "Bidder Sub-Login Details"
        Dim user_name As String = ""
        Dim password As String = ""
        Dim email As String = ""
        Dim name As String = ""
        Dim email_caption As String = ""
        Dim is_active As Boolean = False

        Dim comm As New CommonCode()

        Dim ds As New DataSet
        Dim body As String = ""
        If dt.Rows.Count > 0 Then
            user_name = dt.Rows(0)("username")
            password = Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("password"))
            email = dt.Rows(0)("email")
            name = dt.Rows(0)("name")

            is_active = dt.Rows(0)("is_active")

            If is_active Then
                ds = comm.GetEmail_ByCode("em18")
                email_caption = "Bidder Sub-Login Creation Approval"
            Else
                ds = comm.GetEmail_ByCode("em19")
                email_caption = "Bidder Sub-Login Creation De-activated"
            End If
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                body = ds.Tables(0).Rows(0)("html_body")
                email_caption = ds.Tables(0).Rows(0)("email_name")
                strSubject = ds.Tables(0).Rows(0)("email_subject")

                body = body.Replace("<<bidder_name>>", dt.Rows(0)("name"))
                body = body.Replace("<<user_name>>", user_name)
                body = body.Replace("<<password>>", password)
                body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
            End If
            If for_mail_content = False Then
                comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), strSubject, body, email, "", "", "", "", "")
            End If

        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body

    End Function
    Public Function send_employee_creation_email(ByVal user_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String


        Dim str As String = "select user_id,first_name As name,email,username,password,ISNULL(is_active,0) As is_active from tbl_sec_users where user_id=" & user_id
        Dim dt As DataTable
        dt = SqlHelper.ExecuteDatatable(str)
        Dim email_caption As String = ""
        Dim body As String = ""
        Dim strSubject As String = ""
        Dim user_name As String = ""
        Dim password As String = ""
        Dim email As String = ""
        Dim name As String = ""
        Dim is_active As Boolean = False
        Dim comm As New CommonCode()
        If dt.Rows.Count > 0 Then
            user_name = dt.Rows(0)("username")
            password = Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("password"))
            email = dt.Rows(0)("email")
            name = dt.Rows(0)("name")
            is_active = dt.Rows(0)("is_active")

            If is_active Then
                email_caption = "Employee Login Detail Creation Confirmation (Active mode)"
                Dim ds As New DataSet
                ds = comm.GetEmail_ByCode("em20")
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    body = ds.Tables(0).Rows(0)("html_body")
                    body = body.Replace("<<employee_name>>", name)
                    body = body.Replace("<<user_name>>", user_name)
                    body = body.Replace("<<password>>", password)
                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                    strSubject = ds.Tables(0).Rows(0)("email_subject")
                    email_caption = ds.Tables(0).Rows(0)("email_name")
                    If for_mail_content = False Then
                        comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), strSubject, body, email, "", "", "", "", "")
                    End If
                End If
            Else
                email_caption = "Employee Login Creation Email (Inactive)"
                Dim ds As New DataSet
                ds = comm.GetEmail_ByCode("em21")
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    body = ds.Tables(0).Rows(0)("html_body")
                    body = body.Replace("<<user_name>>", user_name)
                    body = body.Replace("<<password>>", password)
                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                    strSubject = ds.Tables(0).Rows(0)("email_subject")
                    email_caption = ds.Tables(0).Rows(0)("email_name")
                    If for_mail_content = False Then
                        comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), strSubject, body, email, "", "", "", "", "")
                    End If
                End If
            End If
        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body

    End Function
    Public Function send_employee_active_status_changed_email(ByVal user_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String

        Dim email_caption As String = ""
        Dim body As String = ""
        Dim strSubject As String = ""
        Dim comm As New CommonCode()
        Dim ds As DataSet = New DataSet()
        Dim str As String = "select user_id,first_name As name,email,username,password,ISNULL(is_active,0) As is_active from tbl_sec_users where user_id=" & user_id
        ds = SqlHelper.ExecuteDataset(str)
        If ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim user_name As String = ""
            Dim password As String = ""
            Dim email As String = ""
            Dim name As String = ""
            Dim is_active As Boolean = False
            user_name = ds.Tables(0).Rows(0)("username")
            password = Security.EncryptionDecryption.DecryptValue(ds.Tables(0).Rows(0)("password"))
            email = ds.Tables(0).Rows(0)("email")
            name = ds.Tables(0).Rows(0)("name")
            is_active = ds.Tables(0).Rows(0)("is_active")
            If is_active Then
                email_caption = "Employee Login Status (Active)"
                ds.Tables.Clear()
                ds = comm.GetEmail_ByCode("em22", str)
                If ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    body = ds.Tables(0).Rows(0)("html_body")
                    body = body.Replace("<<user_name>>", ds.Tables(1).Rows(0)("username"))
                    body = body.Replace("<<password>>", ds.Tables(1).Rows(0)("password"))
                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                    strSubject = ds.Tables(0).Rows(0)("email_subject")
                    email_caption = ds.Tables(0).Rows(0)("email_name")
                    If for_mail_content = False Then
                        comm.sendEmail("", "", strSubject, body, email, "", "", "", "", "")
                    End If
                End If

            Else
                email_caption = "Employee Login Status (Inactive)"
                ds.Tables.Clear()
                ds = comm.GetEmail_ByCode("em25", str)
                If ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    body = ds.Tables(0).Rows(0)("html_body")
                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                    strSubject = ds.Tables(0).Rows(0)("email_subject")
                    email_caption = ds.Tables(0).Rows(0)("email_name")
                    If for_mail_content = False Then
                        comm.sendEmail("", "", strSubject, body, email, "", "", "", "", "")
                    End If
                End If

            End If

        End If
        ds.Dispose()
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body

    End Function
    Public Function sendOutOfBidMail(ByVal strMailTo As String, ByVal strMailToName As String, ByVal aucName As String, ByVal aucCode As String, ByVal bidamount As String, ByVal bid_id As Integer, ByVal buyer_rank As String, ByVal is_show_rank As Boolean, Optional ByVal for_mail_content As Boolean = False) As String
        Dim email_caption As String = "Out of Bid Email"
        Dim body As String = ""
        Dim strSubject As String = ""
        Dim comm As New CommonCode()
        Dim ds As DataSet = New DataSet()
        ds = comm.GetEmail_ByCode("em23")
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            body = ds.Tables(0).Rows(0)("html_body")
            body = body.Replace("<<bidder_first_name>>", strMailToName)
            body = body.Replace("<<bid_amount>>", bidamount)
            body = body.Replace("<<auction_name>>", aucName)
            body = body.Replace("<<auction_code>>", aucCode)
            If is_show_rank Then
                body = body.Replace("<<buyer_rank>>", "<br><br>Your Current rank is now " & buyer_rank)
            Else
                body = body.Replace("<<buyer_rank>>", "")
            End If

            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
            strSubject = ds.Tables(0).Rows(0)("email_subject").ToString()
            strSubject = strSubject.Replace("<<auction_code>>", aucCode)
            strSubject = strSubject.Replace("<<auction_name>>", aucName)
            email_caption = ds.Tables(0).Rows(0)("email_name")
            If bid_id > 0 And for_mail_content = False Then
                comm.sendEmail_transaction("", "", strSubject, body, strMailTo, strMailToName, "", "", "", "")
                body = body.Replace("'", "''")
                strSubject = strSubject.Replace("'", "''")
                Dim sqlstr As String = "INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & strMailTo & "','" & strSubject.Replace("'", "''") & "','" & body.Replace("'", "''") & "',''); select SCOPE_IDENTITY()"
                Dim mailid As Integer = SqlHelper.ExecuteScalar(sqlstr)
                SqlHelper.ExecuteNonQuery("update tbl_auction_bids set send_email=1,email_id=" & mailid & " where bid_id=" & bid_id)
            End If

        End If
        ds.Dispose()
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
    Public Function set_rep_contact_request_to_salesrep(ByVal sales_rep_id As Integer, ByVal subject As String, ByVal question As String, Optional ByVal for_mail_content As Boolean = False, Optional ByVal query_id As Integer = 1) As String
        Dim subj As String = ""
        Dim strSubject = "", email_caption As String = ""
        subj = question.Replace(Char.ConvertFromUtf32(10), " ").Trim
        subj = subj.Replace(Char.ConvertFromUtf32(13), " ")
        subj = IIf(subj = "", "Customer Ask Question", subj)
        If subj.Length > 30 Then
            subj = subj.Substring(0, 30)
        End If

        Dim user_name As String = ""
        Dim user_email As String = ""
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDatatable("SELECT ISNULL(A.first_name, '') AS NAME, ISNULL(A.email, '') AS email FROM tbl_sec_users A WHERE A.user_id=" & sales_rep_id)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                user_name = .Item("NAME")
                user_email = .Item("email")
            End With
        End If
        dt.Dispose()
        Dim comm As New CommonCode()
        Dim body As String = ""
        If user_email <> "" Then
            Dim ds As New DataSet
            If subject.Trim <> "" And question.Trim <> "" Then
                email_caption = "Contact Request To Sales Rep (Quick Contact)"
                ds = comm.GetEmail_ByCode("em24")
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    body = ds.Tables(0).Rows(0)("html_body")
                    body = body.Replace("<<user_name>>", user_name)
                    body = body.Replace("<<customer_subject>>", subject)
                    body = body.Replace("<<customer_question>>", question.Replace(Char.ConvertFromUtf32(10), "<br/>"))
                    body = body.Replace("<<query_id>>", Security.EncryptionDecryption.EncryptValueFormatted(query_id))
                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                    strSubject = ds.Tables(0).Rows(0)("email_subject")
                    email_caption = ds.Tables(0).Rows(0)("email_name")
                    strSubject = strSubject.Replace("<<customer_question>>", subj)
                    If for_mail_content = False Then
                        Dim email As String = CommonCode.fetch_profile_user_emails("BAQ")
                        comm.sendEmail("", "", strSubject, body, user_email, "", "", "", email, "")

                    End If
                End If
            Else
                email_caption = "Contact Request To Sales Rep (Customer Ask Question)"
                ds.Tables.Clear()
                ds = comm.GetEmail_ByCode("em28")
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    body = ds.Tables(0).Rows(0)("html_body")
                    body = body.Replace("<<user_name>>", user_name)
                    body = body.Replace("<<customer_question>>", question.Replace(Char.ConvertFromUtf32(10), "<br/>"))
                    body = body.Replace("<<query_id>>", Security.EncryptionDecryption.EncryptValueFormatted(query_id))
                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                    strSubject = ds.Tables(0).Rows(0)("email_subject")
                    email_caption = ds.Tables(0).Rows(0)("email_name")
                    strSubject = strSubject.Replace("<<customer_question>>", subj)
                    If for_mail_content = False Then
                        Dim email As String = CommonCode.fetch_profile_user_emails("BAQ")
                        comm.sendEmail("", "", strSubject, body, user_email, "", "", "", email, "")

                    End If
                End If
            End If

        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
    Public Function set_rep_contact_request_to_user(ByVal user_id As Integer, ByVal subject As String, ByVal question As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim subj As String = "Your Question to sales rep"
        Dim user_name As String = ""
        Dim user_email As String = ""
        Dim email_caption As String = ""
        Dim comm As New CommonCode()
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDatatable("SELECT ISNULL(A.first_name, '') AS NAME, ISNULL(A.email, '') AS email FROM tbl_reg_buyer_users A WHERE A.buyer_user_id=" & user_id)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                user_name = .Item("NAME")
                user_email = .Item("email")
            End With
        End If
        dt.Dispose()

        Dim body As String = ""
        If user_email <> "" Then
            Dim ds As New DataSet
            If subject.Trim <> "" And question.Trim <> "" Then
                email_caption = "Conatct Request To User(Customer Question)"
                ds = comm.GetEmail_ByCode("em29")
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    body = ds.Tables(0).Rows(0)("html_body")
                    body = body.Replace("<<user_name>>", user_name)
                    body = body.Replace("<<customer_question>>", question.Replace(Char.ConvertFromUtf32(10), "<br/>"))
                    body = body.Replace("<<customer_subject>>", subject)
                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                    subj = ds.Tables(0).Rows(0)("email_subject")
                    email_caption = ds.Tables(0).Rows(0)("email_name")
                    If for_mail_content = False Then
                        comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), subj, body, user_email, "", "", "", "", "")
                    End If
                End If
            Else
                email_caption = "Conatct Request To User (No Customer Question)"
                ds.Tables.Clear()
                ds = comm.GetEmail_ByCode("em30")
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    body = ds.Tables(0).Rows(0)("html_body")
                    body = body.Replace("<<user_name>>", user_name)
                    body = body.Replace("<<customer_question>>", question.Replace(Char.ConvertFromUtf32(10), "<br/>"))
                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                    subj = ds.Tables(0).Rows(0)("email_subject")
                    email_caption = ds.Tables(0).Rows(0)("email_name")
                    If for_mail_content = False Then
                        comm.sendEmail("", SqlHelper.of_FetchKey("email_from_name"), subj, body, user_email, "", "", "", "", "")
                    End If
                End If
            End If
            ds.Dispose()

        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & subj & "<br><br>" & body
    End Function
    Public Function Send_Bidder_Reply(ByVal login_user_id As Int32, ByVal parent_query_id As Int32, ByVal msg As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim body As String = ""
        Dim strSubject As String = ""
        Dim dt As DataTable = New DataTable()
        Dim strQuery As String = ""
        Dim email_caption As String = ""
        Dim comm As New CommonCode()
        strQuery = "select ISNULL(U.first_name,'') as first_name,ISNULL(U.last_name,'') as last_name,U.email,ISNULL(Q.title,'') as title,ISNULL(Q.message,'') as message from tbl_reg_buyer_users U inner join tbl_auction_queries Q on U.buyer_user_id=Q.buyer_user_id WHERE Q.query_id =" & parent_query_id & " union all select ISNULL(S.first_name,'') as first_name,ISNULL(S.last_name,'') as last_name,isnull(S.email,'') as email,ISNULL(S.title,'') as title,'' as message from tbl_sec_users S where S.user_id=" & login_user_id
        'Return strQuery
        'Exit Function
        dt = SqlHelper.ExecuteDatatable(strQuery)
        If dt.Rows.Count = 2 Then
            Dim ds As New DataSet
            If dt.Rows(0)("title") <> "" And dt.Rows(0)("message") <> "" Then
                email_caption = "Response To Bidder Inquiry (Title and Message Exists)"
                ds = comm.GetEmail_ByCode("em31")
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    body = ds.Tables(0).Rows(0)("html_body")
                    body = body.Replace("<<bidder_first_name>>", dt.Rows(0)("first_name").ToString().Trim())
                    body = body.Replace("<<message_title>>", dt.Rows(0)("title").ToString().Trim())
                    body = body.Replace("<<query_message>>", dt.Rows(0)("message").ToString().Trim())
                    body = body.Replace("<<reply_message>>", msg)
                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                    strSubject = ds.Tables(0).Rows(0)("email_subject")
                    email_caption = ds.Tables(0).Rows(0)("email_name")
                    If for_mail_content = False Then
                        comm.sendEmail(dt.Rows(1)("email"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, dt.Rows(0)("email"), "", "", "", "", "")
                    End If
                End If
            Else
                email_caption = "Response To Bidder Inquiry (Title or Message Not Exists)"
                ds = comm.GetEmail_ByCode("em32")
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    body = ds.Tables(0).Rows(0)("html_body")
                    body = body.Replace("<<bidder_first_name>>", dt.Rows(0)("first_name").ToString().Trim())
                    body = body.Replace("<<reply_message>>", msg)
                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                    strSubject = ds.Tables(0).Rows(0)("email_subject")
                    email_caption = ds.Tables(0).Rows(0)("email_name")
                End If
                If dt.Rows(0)("title") <> "" And dt.Rows(0)("message") = "" Then
                    If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        body = body.Replace("<<message_title>>", dt.Rows(0)("title").ToString().Trim())
                        If for_mail_content = False Then
                            comm.sendEmail(dt.Rows(1)("email"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, dt.Rows(0)("email"), "", "", "", "", "")
                        End If
                    End If
                Else
                    If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        body = body.Replace("<<message_title>>", dt.Rows(0)("message").ToString().Trim())
                        If for_mail_content = False Then
                            comm.sendEmail(dt.Rows(1)("email"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, dt.Rows(0)("email"), "", "", "", "", "")
                        End If
                    End If
                End If
            End If
        End If
        dt = Nothing
        comm = Nothing
        'HttpContext.Current.Response.Write("<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body)
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body


    End Function
    Public Function Send_Auction_Order_Mail(ByVal auction_id As Integer, ByVal click_option As Integer, ByVal buyer_user_id As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim user_name As String = ""
        Dim body As String = ""
        Dim strSubject As String = "Order Confirmation"
        Dim email_caption As String = ""
        Dim comm As New CommonCode()
        Dim user_email As String = ""
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDatatable("select first_name as name ,email from tbl_reg_buyer_users where buyer_user_id=" & buyer_user_id)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                user_name = .Item("name").ToString().Trim()
                user_email = .Item("email").ToString().Trim()
            End With
        End If
        dt.Dispose()

        dt = SqlHelper.ExecuteDatatable("select title , auction_type_id from tbl_auctions where auction_id=" & auction_id)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Dim ds As New DataSet

                Select Case click_option

                    Case 0 'main button click
                        Select Case .Item("auction_type_id")
                            Case 1
                                email_caption = "Auction Order Confirmation Mail(Buy Now, Private , Partial, Bidding)"
                                ds = comm.GetEmail_ByCode("em33")
                                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                    body = ds.Tables(0).Rows(0)("html_body")
                                    strSubject = ds.Tables(0).Rows(0)("email_subject")
                                    email_caption = ds.Tables(0).Rows(0)("email_name")
                                    body = body.Replace("<<user_name>>", user_name)
                                    body = body.Replace("<<auction_title>>", .Item("title"))
                                    body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                                    body = body.Replace("<<order_type>>", "buy now order")
                                End If
                            Case 2, 3
                                email_caption = "Auction Order Confirmation Mail(Buy Now, Private , Partial, Bidding)"
                                ds = comm.GetEmail_ByCode("em33")
                                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                    body = ds.Tables(0).Rows(0)("html_body")
                                    strSubject = ds.Tables(0).Rows(0)("email_subject")
                                    email_caption = ds.Tables(0).Rows(0)("email_name")
                                    body = body.Replace("<<user_name>>", user_name)
                                    body = body.Replace("<<auction_title>>", .Item("title"))
                                    body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                                    body = body.Replace("<<order_type>>", "bidding")
                                End If
                            Case 4
                                email_caption = "Auction Order Confirmation Mail(No Type Defined )"
                                ds = comm.GetEmail_ByCode("em34")
                                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                    body = ds.Tables(0).Rows(0)("html_body")
                                    strSubject = ds.Tables(0).Rows(0)("email_subject")
                                    email_caption = ds.Tables(0).Rows(0)("email_name")
                                    body = body.Replace("<<user_name>>", user_name)
                                    body = body.Replace("<<auction_title>>", .Item("title"))
                                    body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                                End If
                        End Select
                    Case 1
                        email_caption = "Auction Order Confirmation Mail(Buy Now, Private , Partial, Bidding)"
                        ds = comm.GetEmail_ByCode("em33")
                        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            body = ds.Tables(0).Rows(0)("html_body")
                            strSubject = ds.Tables(0).Rows(0)("email_subject")
                            email_caption = ds.Tables(0).Rows(0)("email_name")
                            body = body.Replace("<<user_name>>", user_name)
                            body = body.Replace("<<auction_title>>", .Item("title"))
                            body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                            body = body.Replace("<<order_type>>", "buy now order")
                        End If
                    Case 2
                        email_caption = "Auction Order Confirmation Mail(Buy Now, Private , Partial, Bidding)"
                        ds = comm.GetEmail_ByCode("em33")
                        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            body = ds.Tables(0).Rows(0)("html_body")
                            strSubject = ds.Tables(0).Rows(0)("email_subject")
                            email_caption = ds.Tables(0).Rows(0)("email_name")
                            body = body.Replace("<<user_name>>", user_name)
                            body = body.Replace("<<auction_title>>", .Item("title"))
                            body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                            body = body.Replace("<<order_type>>", "private order")
                        End If
                    Case 3
                        email_caption = "Auction Order Confirmation Mail(Buy Now, Private , Partial, Bidding)"
                        ds = comm.GetEmail_ByCode("em33")
                        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            body = ds.Tables(0).Rows(0)("html_body")
                            strSubject = ds.Tables(0).Rows(0)("email_subject")
                            email_caption = ds.Tables(0).Rows(0)("email_name")
                            body = body.Replace("<<user_name>>", user_name)
                            body = body.Replace("<<auction_title>>", .Item("title"))
                            body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                            body = body.Replace("<<order_type>>", "partial order")
                        End If
                End Select
                dt = Nothing
                If (for_mail_content = False And ds.Tables.Count > 0) AndAlso ds.Tables(0).Rows.Count > 0 Then
                    comm.sendEmail("", "", strSubject, body, user_email, "", "", "", "", "")

                    SqlHelper.ExecuteNonQuery("INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & user_email & "','" & strSubject.Replace("'", "''") & "','" & body.Replace("'", "''") & "','')")

                End If

            End With
        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
    Public Function Send_Mail_Confirm_Receipt(ByVal _id As String, ByVal type As String, ByVal strBody As String, Optional ByVal for_mail_content As Boolean = False) As String
        Dim user_name As String = ""
        Dim user_email As String = ""
        Dim buy_type As String = ""
        Dim auction_name As String = ""
        Dim email_caption As String = ""
        Dim str_subject As String = ""
        Dim auction_id As Int32 = 0
        Dim comm As New CommonCode()
        Dim body As String = ""
        Dim dt As New DataTable()
        If _id > 0 Then
            If type.ToLower = "b" Then
                dt = SqlHelper.ExecuteDatatable("select A.auction_id,U.first_name as name ,U.email,B.buy_type,A.title as auction_name from tbl_reg_buyer_users U inner join tbl_auction_buy B on U.buyer_user_id=B.buyer_user_id inner join tbl_auctions A on B.auction_id=A.auction_id where B.buy_id=" & _id & "")
            Else
                dt = SqlHelper.ExecuteDatatable("select A.auction_id,U.first_name as name ,U.email,'' as buy_type,A.title as auction_name from tbl_reg_buyer_users U inner join tbl_auction_bids B on U.buyer_user_id=B.buyer_user_id inner join tbl_auctions A on B.auction_id=A.auction_id where B.bid_id=" & _id & "")
            End If
            If dt.Rows.Count > 0 Then
                With dt.Rows(0)
                    user_name = .Item("name").ToString().Trim()
                    user_email = .Item("email").ToString().Trim()
                    buy_type = .Item("buy_type").ToString().Trim()
                    auction_name = .Item("auction_name").ToString().Trim()
                    auction_id = .Item("auction_id")
                End With

                Dim ds As New DataSet
                Select Case buy_type.ToLower()
                    Case "buy now"
                        email_caption = "Order Confirmation Receipt(Buy it Now)"
                        ds = comm.GetEmail_ByCode_AddBody("em35", concatenate_body:="<br /><br />" & strBody)
                        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            body = ds.Tables(0).Rows(0)("html_body")
                            str_subject = ds.Tables(0).Rows(0)("email_subject")
                            email_caption = ds.Tables(0).Rows(0)("email_name")
                            body = body.Replace("<<user_name>>", user_name)
                            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                            'If strBody.Trim <> "" Then
                            '    body = body & "<br /><br />" & strBody
                            'End If
                        End If
                    Case "private"
                        email_caption = "Order Confirmation Receipt(Private, Partial)"
                        ds = comm.GetEmail_ByCode_AddBody("em36", concatenate_body:="<br /><br />" & strBody)
                        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            body = ds.Tables(0).Rows(0)("html_body")
                            str_subject = ds.Tables(0).Rows(0)("email_subject")
                            email_caption = ds.Tables(0).Rows(0)("email_name")
                            body = body.Replace("<<user_name>>", user_name)
                            body = body.Replace("<<order_type_receipt>>", "private")
                            body = body.Replace("<<auction_name>>", auction_name)
                            body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                            'If strBody.Trim <> "" Then
                            '    body = body & "<br /><br />" & strBody
                            'End If
                        End If

                    Case "partial"
                        email_caption = "Order Confirmation Receipt(Private, Partial)"
                        ds = comm.GetEmail_ByCode_AddBody("em36", concatenate_body:="<br /><br />" & strBody)
                        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            body = ds.Tables(0).Rows(0)("html_body")
                            str_subject = ds.Tables(0).Rows(0)("email_subject")
                            email_caption = ds.Tables(0).Rows(0)("email_name")
                            body = body.Replace("<<user_name>>", user_name)
                            body = body.Replace("<<order_type_receipt>>", "partial")
                            body = body.Replace("<<auction_name>>", auction_name)
                            body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                            'If strBody.Trim <> "" Then
                            '    body = body & "<br /><br />" & strBody
                            'End If
                        End If
                    Case Else
                        email_caption = "Order Confirmation Receipt(No Type Defined)"
                        ds = comm.GetEmail_ByCode_AddBody("em37", concatenate_body:="<br /><br />" & strBody)
                        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            body = ds.Tables(0).Rows(0)("html_body")
                            str_subject = ds.Tables(0).Rows(0)("email_subject")
                            email_caption = ds.Tables(0).Rows(0)("email_name")
                            body = body.Replace("<<user_name>>", user_name)
                            body = body.Replace("<<auction_name>>", auction_name)
                            body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                            'If strBody.Trim <> "" Then
                            '    body = body & "<br /><br />" & strBody
                            'End If
                        End If
                End Select
                If (for_mail_content = False And ds.Tables.Count > 0) AndAlso ds.Tables(0).Rows.Count > 0 Then
                    comm.sendEmail("", "", str_subject, body, user_email, "", "", "", "", "")

                    SqlHelper.ExecuteNonQuery("INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & user_email & "','" & str_subject.Replace("'", "''") & "','" & body.Replace("'", "''") & "','')")
                End If
            End If
            dt.Dispose()
        End If
        comm = Nothing
        'HttpContext.Current.Response.Write("ID-" & _id & " <b>Email Caption:</b> " & email_caption & "<br>" & str_subject & "<br><br>" & body)
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & str_subject & "<br><br>" & body
    End Function
    Public Function SendWinningBidEmail(ByVal bid_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String
        'Done
        Dim email_caption As String = ""
        Dim body As String = ""
        Dim strSubject As String = ""
        Dim comm As New CommonCode()
        Dim str As String = ""
        If bid_id > 0 Then
            Dim dt As New DataTable()
            Dim buyer_email As String = ""
            Dim buyer_name As String = ""
            Dim bidder_email As String = ""
            Dim bidder_name As String = ""
            Dim auction_title As String = ""
            Dim auction_id As Int32 = 0
            Dim bid_date As DateTime = DateTime.Now
            Dim strBid As String = "select A.auction_id,A.bid_date,ISNULL(A.max_bid_amount,0) AS max_bid_amount,ISNULL(A.bid_amount,0) AS bid_amount,ISNULL(A.action,'') AS action," & _
                "ISNULL(B.contact_first_name,'') As buyer_name,ISNULL(B.email,'') As email, " & _
                "ISNULL(C.title,'') AS auction_title,C.auction_type_id, rtrim(ISNULL(U.first_name,'') + ' ' + ISNULL(U.last_name,'')) as bidder_name,ISNULL(U.email,'') as bidder_email" & _
                " from tbl_auction_bids A INNER JOIN tbl_reg_buyers B ON A.buyer_id=B.buyer_id INNER JOIN tbl_reg_buyer_users U on A.buyer_user_id=U.buyer_user_id INNER JOIN tbl_auctions C ON A.auction_id=C.auction_id where bid_id=" & bid_id
            dt = SqlHelper.ExecuteDatatable(strBid)
            If dt.Rows.Count > 0 Then
                buyer_email = dt.Rows(0)("email")
                buyer_name = dt.Rows(0)("buyer_name").ToString().Trim()
                bidder_email = dt.Rows(0)("bidder_email")
                bidder_name = dt.Rows(0)("bidder_name").ToString().Trim()
                auction_title = dt.Rows(0)("auction_title")
                auction_id = dt.Rows(0)("auction_id")
                bid_date = Convert.ToDateTime(dt.Rows(0)("bid_date")).ToShortDateString()
                Dim max_bid_amount As String = FormatNumber(dt.Rows(0)("max_bid_amount"))
                Dim bid_amount As String = FormatNumber(dt.Rows(0)("bid_amount"))

                Dim ds As DataSet = New DataSet()
                If dt.Rows(0)("action").ToString().ToUpper() = "Awarded".ToUpper() Then

                    'In Case of Congrats mail and auction type is proxy
                    If dt.Rows(0)("auction_type_id") = 3 Then
                        ds = comm.GetEmail_ByCode("em26")
                        email_caption = "Winning Bidder Mail (Proxy Auction)"
                        If ds.Tables(0).Rows.Count > 0 Then
                            strSubject = ds.Tables(0).Rows(0)("email_subject")
                            email_caption = ds.Tables(0).Rows(0)("email_name")
                            body = ds.Tables(0).Rows(0)("html_body")
                            body = body.Replace("<<bidder_name>>", buyer_name)
                            body = body.Replace("<<auction_title>>", auction_title)
                            body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                            body = body.Replace("<<max_bid_amount>>", max_bid_amount)
                            body = body.Replace("<<win_bid_amount>>", bid_amount)
                            body = body.Replace("<<bid_date>>", Format(Convert.ToDateTime(bid_date), "M/dd/yyyy"))
                            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))

                        End If

                    Else
                        'In Case of Congrats mail and auction type is not proxy
                        ds = comm.GetEmail_ByCode("em38")
                        email_caption = "Winning Bid Mail (Proxy Auction)"
                        If ds.Tables(0).Rows.Count > 0 Then
                            strSubject = ds.Tables(0).Rows(0)("email_subject")
                            email_caption = ds.Tables(0).Rows(0)("email_name")
                            body = ds.Tables(0).Rows(0)("html_body")
                            body = body.Replace("<<bidder_name>>", buyer_name)
                            body = body.Replace("<<auction_title>>", auction_title)
                            body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                            body = body.Replace("<<win_bid_amount>>", bid_amount)
                            body = body.Replace("<<bid_date>>", Format(Convert.ToDateTime(bid_date), "M/dd/yyyy"))
                            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))

                        End If

                    End If
                Else
                    If dt.Rows(0)("auction_type_id") = 3 Then
                        email_caption = "Email For Not Highest Bidder (Proxy Auction)"
                        ds = comm.GetEmail_ByCode("em27")
                        If ds.Tables(0).Rows.Count > 0 Then
                            strSubject = ds.Tables(0).Rows(0)("email_subject")
                            email_caption = ds.Tables(0).Rows(0)("email_name")
                            body = ds.Tables(0).Rows(0)("html_body")
                            body = body.Replace("<<bidder_name>>", buyer_name)
                            body = body.Replace("<<auction_title>>", auction_title)
                            body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                            body = body.Replace("<<max_bid_amount>>", bid_amount)
                            body = body.Replace("<<bid_date>>", Format(Convert.ToDateTime(bid_date), "M/dd/yyyy"))
                            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))

                        End If

                    Else
                        ds = comm.GetEmail_ByCode("em41")
                        email_caption = "Email For Not Highest Bidder"
                        If ds.Tables(0).Rows.Count > 0 Then
                            strSubject = ds.Tables(0).Rows(0)("email_subject")
                            email_caption = ds.Tables(0).Rows(0)("email_name")
                            body = ds.Tables(0).Rows(0)("html_body")
                            body = body.Replace("<<bidder_name>>", buyer_name)
                            body = body.Replace("<<auction_title>>", auction_title)
                            body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                            body = body.Replace("<<bid_date>>", Format(Convert.ToDateTime(bid_date), "M/dd/yyyy"))
                            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                        End If
                    End If
                End If

                If (for_mail_content = False And ds.Tables.Count > 0) AndAlso ds.Tables(0).Rows.Count > 0 Then
                    If dt.Rows(0)("action").ToString().ToUpper() = "Awarded".ToUpper() Then
                        comm.sendEmail_transaction("", "", strSubject, body, bidder_email, bidder_name, "", "", buyer_email, buyer_name)
                    Else
                        comm.sendEmail("", "", strSubject, body, bidder_email, bidder_name, "", "", buyer_email, buyer_name)
                    End If

                    str = "INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & buyer_email & "','" & strSubject.Replace("'", "''") & "','" & body.Replace("'", "''") & "','') select scope_identity()"
                    Dim email_id As Integer = SqlHelper.ExecuteScalar(str.Replace("'", "'''"))
                    If email_id > 0 Then
                        SqlHelper.ExecuteNonQuery("update tbl_auction_bids set send_email=1,email_id=" & email_id & " where bid_id=" & bid_id)
                    End If

                End If

            End If
        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body

    End Function
    Public Function SendBuyNowAcceptEmail(ByVal buy_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String
        'Done
        Dim comm As New CommonCode()
        Dim email_caption As String = ""
        Dim body As String = ""
        Dim strSubject As String = ""

        If buy_id > 0 Then
            Dim dt As DataTable = New DataTable()
            Dim buyer_email As String = ""
            Dim buyer_name As String = ""
            Dim auction_id As Int32 = 0
            Dim bid_date As DateTime = DateTime.Now
            Dim buy_type As String = ""
            Dim strBuy As String = "select A.auction_id,A.bid_date,ISNULL(A.price,0) AS price,ISNULL(A.quantity,0) AS quantity,ISNULL(A.buy_type,'') As buy_type, C.auction_type_id," & _
                                    "ISNULL(B.contact_first_name,'') As buyer_name,ISNULL(B.email,'') As email," & _
                                    "ISNULL(C.title,'') AS auction_title from tbl_auction_buy A INNER JOIN tbl_reg_buyers B ON A.buyer_id=B.buyer_id " & _
                                    "INNER JOIN tbl_auctions C ON A.auction_id=C.auction_id where buy_id=" & buy_id
            dt = SqlHelper.ExecuteDatatable(strBuy)
            If dt.Rows.Count > 0 Then
                buyer_email = dt.Rows(0)("email")
                buyer_name = dt.Rows(0)("buyer_name").ToString().Trim()
                bid_date = dt.Rows(0)("bid_date")
                auction_id = dt.Rows(0)("auction_id")
                'HttpContext.Current.Response.Write(dt.Rows(0)("buy_type").ToString())
                buy_type = IIf(dt.Rows(0)("buy_type").ToString() = "BUY NOW", "Buy it Now", dt.Rows(0)("buy_type").ToString())
                Dim qty As String = FormatNumber(dt.Rows(0)("quantity"), 2)
                Dim price As String = FormatNumber(dt.Rows(0)("price"))
                Dim title As String = dt.Rows(0)("auction_title")
                Dim ds As DataSet = New DataSet()
                buy_type = buy_type.Trim
                If buy_type.ToUpper() = "PRIVATE" Then
                    email_caption = "Order Confirmation (Private)"
                    ds = comm.GetEmail_ByCode("em39")
                    If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        body = ds.Tables(0).Rows(0)("html_body")
                        body = body.Replace("<<bidder_name>>", buyer_name)
                        body = body.Replace("<<buy_type>>", buy_type)
                        body = body.Replace("<<auction_title>>", title)
                        body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                        body = body.Replace("<<auction_price>>", price)
                        body = body.Replace("<<bid_date>>", bid_date)
                        body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))

                        strSubject = ds.Tables(0).Rows(0)("email_subject")
                        email_caption = ds.Tables(0).Rows(0)("email_name")
                        If for_mail_content = False Then
                            comm.sendEmail(SqlHelper.of_FetchKey("email_from"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, buyer_email, buyer_name, "", "", "", "")
                            SqlHelper.ExecuteNonQuery("INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & buyer_email & "','" & strSubject.Replace("'", "''") & "','" & body.Replace("'", "''") & "','')")
                        End If

                    End If
                Else
                        email_caption = "Order Confirmation"
                        ds = comm.GetEmail_ByCode("em4")
                        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            body = ds.Tables(0).Rows(0)("html_body")
                            body = body.Replace("<<bidder_name>>", buyer_name)
                            body = body.Replace("<<buy_type>>", buy_type)
                        body = body.Replace("<<auction_title>>", title)
                        body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                            body = body.Replace("<<auction_price>>", price)
                            body = body.Replace("<<auction_quantity>>", qty)
                            body = body.Replace("<<bid_date>>", bid_date)
                            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))

                            strSubject = ds.Tables(0).Rows(0)("email_subject")
                            email_caption = ds.Tables(0).Rows(0)("email_name")
                            If for_mail_content = False Then
                            comm.sendEmail(SqlHelper.of_FetchKey("email_from"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, buyer_email, buyer_name, "", "", "", "")
                            SqlHelper.ExecuteNonQuery("INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & buyer_email & "','" & strSubject.Replace("'", "''") & "','" & body.Replace("'", "''") & "','')")
                            End If

                        End If

                    End If
            End If
        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body

    End Function
    Public Function SendQuotationAcceptEmail(ByVal quotation_id As Integer, Optional ByVal for_mail_content As Boolean = False) As String
        'Done
        Dim body As String = ""
        Dim email_caption As String = ""
        Dim strSubject As String = "Thank you for submitting your offer!"
        Dim comm As New CommonCode()
        If quotation_id > 0 Then
            Dim dt As DataTable = New DataTable()
            Dim buyer_email As String = ""
            Dim buyer_name As String = ""
            Dim auction_title As String = ""
            Dim auction_id As Int32 = 0
            Dim bid_date As DateTime = DateTime.Now

            Dim strBuy As String = "select A.auction_id,A.bid_date, C.auction_type_id,ISNULL(B.contact_first_name,'') As buyer_name,ISNULL(B.email,'') As email, " & _
                                    "ISNULL(C.title,'') AS auction_title from tbl_auction_quotations A INNER JOIN tbl_reg_buyers B ON A.buyer_id=B.buyer_id " & _
                                    "INNER JOIN tbl_auctions C ON A.auction_id=C.auction_id where quotation_id =" & quotation_id
            dt = SqlHelper.ExecuteDatatable(strBuy)
            If dt.Rows.Count > 0 Then

                email_caption = "Thank Mail On Offer Submission"
                buyer_email = dt.Rows(0)("email")
                buyer_name = dt.Rows(0)("buyer_name").ToString().Trim()
                bid_date = dt.Rows(0)("bid_date")
                auction_title = dt.Rows(0)("auction_title").ToString().Trim()
                auction_id = dt.Rows(0)("auction_id")
                Dim ds As New DataSet
                ds = comm.GetEmail_ByCode("em3")
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    body = ds.Tables(0).Rows(0)("html_body")
                    body = body.Replace("<<bidder_name>>", buyer_name)
                    body = body.Replace("<<auction_title>>", auction_title)
                    body = body.Replace("<<auction_url>>", SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id.ToString()))
                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                    strSubject = ds.Tables(0).Rows(0)("email_subject")
                    email_caption = ds.Tables(0).Rows(0)("email_name")
                    If for_mail_content = False Then
                        comm.sendEmail(SqlHelper.of_FetchKey("email_from"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, buyer_email, buyer_name, "", "", "", "")
                        SqlHelper.ExecuteNonQuery("INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & buyer_email & "','" & strSubject.Replace("'", "''") & "','" & body.Replace("'", "''") & "','')")
                    End If
                End If
            End If
        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body

    End Function
    Public Sub sendInvitationBeforeMail(ByVal invitation_ids As String)
        'Done
        'bimal- change here you have to change the exe also
        Dim strQuery As String = "select E.schedule_type,E.email_schedule_id,A.auction_id,A.auction_type_id,A.title,A.code as auction_code,A.start_date,A.display_end_time,A.start_price,B.first_name as name,B.email,B.buyer_id from dbo.Fn_get_email_schedule_list_queue('" & invitation_ids & "') F inner join tbl_auction_email_schedules E on E.email_schedule_id=F.email_schedule_id inner join tbl_auctions A on A.auction_id=F.auction_id inner join tbl_reg_buyer_users B on B.buyer_id=F.buyer_id and B.is_admin=1"
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDatatable(strQuery)

        Dim dtBidders As New DataTable()
        Dim strBidders As String = "select distinct buyer_id from dbo.Fn_get_email_schedule_list_queue('" & invitation_ids & "')"
        dtBidders = SqlHelper.ExecuteDatatable(strBidders)

        Dim buyer_email As String = ""
        Dim buyer_name As String = ""
        Dim auction_title As String = ""
        Dim auc_id As Integer = 0
        Dim sce_id As Integer = 0
        Dim em_id As String = ""
        Dim email_caption As String = "Participation Email For Upcoming Auction (Dynamic)"
        Dim body As String = ""
        Dim email_body As String = ""
        Dim str_subject As String = ""
        Dim rept_body As String = ""
        Dim comm As New CommonCode()
        Dim ds As New DataSet
        ds = comm.GetEmail_ByCode("em5")
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            email_body = ds.Tables(0).Rows(0)("html_body")
            str_subject = ds.Tables(0).Rows(0)("email_subject")
            email_caption = ds.Tables(0).Rows(0)("email_name")
            If dtBidders.Rows.Count > 0 Then
                For j As Integer = 0 To dtBidders.Rows.Count - 1
                    body = email_body
                    buyer_name = ""
                    buyer_email = ""
                    rept_body = ""
                    auction_title = ""
                    Dim buyer_id As Integer = 0
                    buyer_id = dtBidders.Rows(j)("buyer_id")
                    Dim dv As DataView = dt.DefaultView
                    dv.RowFilter = "buyer_id=" & dtBidders.Rows(j)("buyer_id")


                    For i As Integer = 0 To dv.Count - 1
                        With dv(i)
                            If auc_id <> .Item("auction_id") Or sce_id <> .Item("email_schedule_id") Or em_id <> .Item("email") Then
                                SqlHelper.ExecuteNonQuery("INSERT INTO tbl_auction_email_schedule_statuses(email_schedule_id, auction_id,email_id,buyer_id) VALUES (" & .Item("email_schedule_id") & ", " & .Item("auction_id") & ",'" & .Item("email") & "'," & dtBidders.Rows(j)("buyer_id") & ")")
                            End If

                            auc_id = .Item("auction_id")
                            sce_id = .Item("email_schedule_id")
                            em_id = .Item("email")
                            
                            If auction_title = "" Then
                                auction_title = .Item("title")
                            Else
                                auction_title = auction_title & ", " & .Item("title")
                            End If
                            
                            If buyer_name.Trim() = "" Then
                                buyer_name = .Item("name").ToString().Trim()
                            End If
                            If buyer_email.Trim() = "" Then
                                buyer_email = .Item("email")
                            End If

                            rept_body = rept_body & "<br /><br /><b>[" & .Item("auction_code") & "]</b> " & .Item("title") & "."
                       
                        End With

                    Next

                    body = body.Replace("<<repeated_body_content>>", rept_body)
                    body = body.Replace("<<bidder_name>>", buyer_name)
                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                    body = body.Replace("<<bidder_id>>", Security.EncryptionDecryption.EncryptValueFormatted(buyer_id))

                    comm.sendEmail("", "", str_subject, body, buyer_email, buyer_name, "", "", "", "")

                Next
            End If
        End If
        comm = Nothing
    End Sub
    Public Function sendInvitationBeforeMail_static(ByVal buyer_id As Integer) As String
        'not call from function call from EmailContent.aspx only
        'Done

        Dim email_caption As String = "Participation Email For Upcoming Auction (Static)"
        Dim body As String = ""
        Dim strSubject As String = ""
        Dim comm As New CommonCode()
        Dim ds As New DataSet
        ds = comm.GetEmail_ByCode("em6")
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            body = ds.Tables(0).Rows(0)("html_body")
            body = body.Replace("<<bidder_id>>", Security.EncryptionDecryption.EncryptValueFormatted(buyer_id))
            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
            strSubject = ds.Tables(0).Rows(0)("email_subject")
            email_caption = ds.Tables(0).Rows(0)("email_name")
        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body

    End Function
Public Function sendInvitationBeforeEndMail(ByVal reminder_ids As String) As String
        'Done
        'bimal- change here you have to change the exe also
        Dim strQuery As String = "select E.schedule_type,E.email_schedule_id,A.auction_id,A.code as auction_code,A.auction_type_id,A.title,A.start_date,A.display_end_time,A.start_price,B.first_name as name,B.email,B.buyer_id from dbo.Fn_get_email_schedule_list_queue('" & reminder_ids & "') F inner join tbl_auction_email_schedules E on E.email_schedule_id=F.email_schedule_id inner join tbl_auctions A on A.auction_id=F.auction_id inner join tbl_reg_buyer_users B on B.buyer_id=F.buyer_id and B.is_admin=1 "
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDatatable(strQuery)

        Dim dtBidders As New DataTable()
        Dim ds As New DataSet()
        Dim strBidders As String = "select distinct buyer_id from dbo.Fn_get_email_schedule_list_queue('" & reminder_ids & "')"
        dtBidders = SqlHelper.ExecuteDatatable(strBidders)

        Dim buyer_email As String = ""
        Dim buyer_name As String = ""
        Dim auction_title As String = ""
        Dim end_date As DateTime = "1/1/1999"

        Dim auc_id As Integer = 0
        Dim sce_id As Integer = 0
        Dim em_id As String = ""
        Dim email_caption As String = "Send Invitation To Bidder For Bidding (Dynamic)"
        Dim body As String = ""
        Dim strSubject As String = ""
        Dim body_html As String = ""
        Dim rept_body As String = ""
        Dim comm As New CommonCode()
        ds = comm.GetEmail_ByCode("em8")
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            body_html = ds.Tables(0).Rows(0)("html_body")
            strSubject = ds.Tables(0).Rows(0)("email_subject")
        End If

        If dtBidders.Rows.Count > 0 And body_html <> "" Then
            For j As Integer = 0 To dtBidders.Rows.Count - 1
                body = body_html
                buyer_name = ""
                buyer_email = ""
                rept_body = ""
                auction_title = ""
                Dim buyer_id As Integer = 0
                buyer_id = dtBidders.Rows(j)("buyer_id")
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "buyer_id=" & dtBidders.Rows(j)("buyer_id")
                For i As Integer = 0 To dv.Count - 1
                    With dv(i)
                         If auc_id <> .Item("auction_id") Or sce_id <> .Item("email_schedule_id") Or em_id <> .Item("email") Then
                            SqlHelper.ExecuteNonQuery("INSERT INTO tbl_auction_email_schedule_statuses(email_schedule_id, auction_id,email_id,buyer_id) VALUES (" & .Item("email_schedule_id") & ", " & .Item("auction_id") & ",'" & .Item("email") & "'," & dtBidders.Rows(j)("buyer_id") & ")")
                        End If
                        auc_id = .Item("auction_id")
                        sce_id = .Item("email_schedule_id")
                        em_id = .Item("email")
                        end_date = .Item("display_end_time")
                        If buyer_name.Trim() = "" Then
                            buyer_name = .Item("name").ToString().Trim()
                        End If
                        If buyer_email.Trim() = "" Then
                            buyer_email = .Item("email")
                        End If

                        rept_body = rept_body & "<br /><br /><b>[" & .Item("auction_code") & "]</b> " & .Item("title") & "."
                    End With
                  Next
                body = body.Replace("<<repeated_body_content>>", rept_body)
                body = body.Replace("<<bidder_name>>", buyer_name)
                body = body.Replace("<<end_date>>", end_date)
                body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))

                body = body.Replace("<<bidder_id>>", Security.EncryptionDecryption.EncryptValueFormatted(buyer_id))

                comm.sendEmail("", "", strSubject, body, buyer_email, buyer_name, "", "", "", "")
            Next
        End If
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
    Public Function sendInvitationBeforeEndMail_static(ByVal buyer_id As Integer) As String
        Dim email_caption As String = "Send Invitation To Bidder For Bidding (Static)"
        Dim body As String = ""
        Dim strSubject As String = ""
        Dim start_date As DateTime = Now.AddDays(-1).AddHours(5).AddMinutes(15)
        Dim end_date As DateTime = Now.AddDays(3).AddHours(3).AddMinutes(5)
        Dim comm As New CommonCode()
        Dim ds As DataSet = New DataSet()
        ds = comm.GetEmail_ByCode("em7")
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            body = ds.Tables(0).Rows(0)("html_body")
            body = body.Replace("<<bidder_id>>", Security.EncryptionDecryption.EncryptValueFormatted(buyer_id))
            body = body.Replace("<<end_date>>", end_date)
            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
            email_caption = ds.Tables(0).Rows(0)("email_name")
        End If
        ds.Dispose()
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function

    Public Sub sendInvitationAfterMail(ByVal thanks_ids As String)
        'Done
        'bimal- change here you have to change the exe also
        Dim strQuery As String = "select E.schedule_type,E.email_schedule_id,A.auction_id,A.auction_type_id,A.title,A.start_date,A.display_end_time,A.start_price,B.first_name as name,B.email,B.buyer_id from dbo.Fn_get_email_schedule_list_queue('" & thanks_ids & "') F inner join tbl_auction_email_schedules E on E.email_schedule_id=F.email_schedule_id inner join tbl_auctions A on A.auction_id=F.auction_id inner join tbl_reg_buyer_users B on B.buyer_id=F.buyer_id and B.is_admin=1"
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Dim email_caption As String = "Thank You Mail For Bidder After Bid (Dynamic)"
        Dim dtBidders As New DataTable()
        Dim comm As New CommonCode()
        Dim strBidders As String = "select distinct buyer_id from dbo.Fn_get_email_schedule_list_queue('" & thanks_ids & "') "
        dtBidders = SqlHelper.ExecuteDatatable(strBidders)

        Dim buyer_email As String = ""
        Dim buyer_name As String = ""
        Dim auction_title As String = ""

        Dim span As TimeSpan
        Dim time_left As String = ""
        Dim no_of_days As Integer = 0
        Dim auc_id As Integer = 0
        Dim sce_id As Integer = 0
        Dim em_id As String = ""
        Dim body As String = ""
        Dim email_body As String = ""
        Dim str_subject As String = ""
        Dim ds As New DataSet
        ds = comm.GetEmail_ByCode("em1")
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            email_body = ds.Tables(0).Rows(0)("html_body")
            str_subject = ds.Tables(0).Rows(0)("email_subject")
            email_caption = ds.Tables(0).Rows(0)("email_name")
            If dtBidders.Rows.Count > 0 Then
                For j As Integer = 0 To dtBidders.Rows.Count - 1
                    body = email_body
                    buyer_name = ""
                    buyer_email = ""
                    Dim buyer_id As Integer = 0
                    buyer_id = dtBidders.Rows(j)("buyer_id")
                    Dim dv As DataView = dt.DefaultView
                    dv.RowFilter = "buyer_id=" & dtBidders.Rows(j)("buyer_id")
                    Dim auction_title_temp As String = ""
                    Dim auction_url_temp As String = ""
                    For i As Integer = 0 To dv.Count - 1
                        With dv(i)
                            If auc_id <> .Item("auction_id") Or sce_id <> .Item("email_schedule_id") Or buyer_email <> .Item("email") Then
                                SqlHelper.ExecuteNonQuery("INSERT INTO tbl_auction_email_schedule_statuses(email_schedule_id, auction_id,email_id,buyer_id) VALUES (" & .Item("email_schedule_id") & ", " & .Item("auction_id") & ",'" & .Item("email") & "'," & dtBidders.Rows(j)("buyer_id") & ")")
                            End If

                            auc_id = .Item("auction_id")
                            sce_id = .Item("email_schedule_id")
                            span = .Item("start_date").Subtract(DateTime.Now)

                            auction_title = .Item("title")
                            no_of_days = span.Days

                            If buyer_name.Trim() = "" Then
                                buyer_name = .Item("name").ToString().Trim()
                            End If
                            If buyer_email.Trim() = "" Then
                                buyer_email = .Item("email")
                            End If
                        End With
                        auction_title_temp = auction_title_temp & "<br />" & "<a target=""_blank"" href=""" & SqlHelper.of_FetchKey("ServerHttp") & "/login.html?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auc_id.ToString()) & """>" & auction_title & "</a>"
                    Next
                    body = body.Replace("<<auction_title>>", auction_title_temp)
                    body = body.Replace("<<bidder_name>>", buyer_name)
                    body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                    body = body.Replace("<<bidder_id>>", Security.EncryptionDecryption.EncryptValueFormatted(buyer_id))
                    comm.sendEmail("", "", str_subject, body, buyer_email, buyer_name, "", "", "", "")
                Next
            End If
        End If

        comm = Nothing
    End Sub
    Public Function sendInvitationAfterMail_static(ByVal buyer_id As Integer) As String
        Dim email_caption As String = "Thank You Mail For Bidder After Bid (Static)"
        Dim body As String = ""
        Dim strSubject As String = ""
        Dim comm As New CommonCode()
        Dim ds As DataSet = New DataSet()
        ds = comm.GetEmail_ByCode("em9")
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            body = ds.Tables(0).Rows(0)("html_body")
            body = body.Replace("<<bidder_first_name>>", "[first name]")
            body = body.Replace("<<auction_title>>", "[Title of the Auction]")
            body = body.Replace("<<bidder_id>>", Security.EncryptionDecryption.EncryptValueFormatted(buyer_id))
            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
            strSubject = ds.Tables(0).Rows(0)("email_subject")
            email_caption = ds.Tables(0).Rows(0)("email_name")
        End If
        ds.Dispose()
        comm = Nothing
        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
    Public Function send_Bidderd_SemiAproval_Admin_Active(ByVal buyer_id As Int32, Optional ByVal for_mail_content As Boolean = False) As String
        Dim email_caption As String = "Bidder Semi Approval Email"
        Dim body As String = ""
        Dim strSubject As String = ""
        Dim comm As New CommonCode()
        Dim ds As DataSet = New DataSet()
        Dim strQuery As String = ""
        strQuery = "SELECT A.buyer_id, " & _
                      "ISNULL(U.username, '') AS username, " & _
                      "ISNULL(A.company_name, '') AS company_name, " & _
                      "ISNULL(A.contact_title, '') AS contact_title, " & _
                      "ISNULL(A.contact_first_name, '') AS contact_first_name, " & _
                      "ISNULL(A.contact_last_name, '') AS contact_last_name, " & _
                      "ISNULL(A.email, '') AS email, " & _
                      "ISNULL(A.website, '') AS website, " & _
                      "ISNULL(A.phone, '') AS phone, " & _
                      "ISNULL(A.mobile, '') AS mobile, " & _
                      "ISNULL(A.fax, '') AS fax," & _
                      "ISNULL(A.address1, '') AS address1, " & _
                      "ISNULL(A.address2, '') AS address2, " & _
                      "ISNULL(A.city, '') AS city, " & _
                      "ISNULL(state_text, '') AS state," & _
                      "ISNULL(A.zip, '') AS zip, " & _
                     "ISNULL(C.name, '') AS country, " & _
                     "ISNULL(A.comment, '') AS comment, " & _
                     "ISNULL(S.status, '') AS status, " & _
        "isnull((Stuff((Select ', ' + name From tbl_master_how_find_us Where  how_find_us_id in (select how_find_us_id from tbl_reg_buyer_find_us_assignment where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as find_us," & _
        "isnull((Stuff((Select ', ' + company_name From tbl_reg_sellers Where  seller_id in (select seller_id from tbl_reg_buyer_seller_mapping where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as company_name " & _
                      " FROM tbl_reg_buyers A Left JOIN tbl_master_countries C ON A.country_id=C.country_id Left join tbl_reg_buyer_users U on A.buyer_id=U.buyer_id Left join tbl_reg_buyser_statuses S on A.status_id=S.status_id WHERE A.buyer_id =" & buyer_id & " and U.is_admin=1"
        '"isnull((Stuff((Select ', ' + name From tbl_master_business_types Where  business_type_id in (select business_type_id from tbl_reg_buyer_business_type_mapping where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as business_type," & _
        '"isnull((Stuff((Select ', ' + name From tbl_master_industry_types Where  industry_type_id in (select industry_type_id from tbl_reg_buyer_industry_type_mapping where buyer_id=A.buyer_id )  FOR XML PATH('')),1,2,'')),'') as industry_type," & _
        Dim ds_email As New DataSet
        ds_email = comm.GetEmail_ByCode("em40", strQuery)

        If ds_email.Tables.Count = 2 AndAlso (ds_email.Tables(1).Rows.Count > 0 And ds_email.Tables(0).Rows.Count > 0) Then
            'ds.Tables.Add(ds_email.Tables(1)) 'assign ds_email second table to ds
            body = ds_email.Tables(0).Rows(0)("html_body")
            body = body.Replace("<<user_name>>", ds_email.Tables(1).Rows(0)("username"))
            body = body.Replace("<<company_name>>", ds_email.Tables(1).Rows(0)("company_name"))
            body = body.Replace("<<company_website>>", ds_email.Tables(1).Rows(0)("website"))
            body = body.Replace("<<bidder_first_name>>", ds_email.Tables(1).Rows(0)("contact_first_name"))
            body = body.Replace("<<bidder_last_name>>", ds_email.Tables(1).Rows(0)("contact_last_name"))
            body = body.Replace("<<bidder_title>>", ds_email.Tables(1).Rows(0)("contact_title"))
            body = body.Replace("<<bidder_address1>>", ds_email.Tables(1).Rows(0)("address1"))
            body = body.Replace("<<bidder_address2>>", ds_email.Tables(1).Rows(0)("address2"))
            body = body.Replace("<<bidder_city>>", ds_email.Tables(1).Rows(0)("city"))
            body = body.Replace("<<bidder_country>>", ds_email.Tables(1).Rows(0)("country"))
            body = body.Replace("<<bidder_state>>", ds_email.Tables(1).Rows(0)("state"))
            body = body.Replace("<<bidder_zip>>", ds_email.Tables(1).Rows(0)("zip"))
            body = body.Replace("<<bidder_mobile>>", ds_email.Tables(1).Rows(0)("mobile"))
            body = body.Replace("<<bidder_phone>>", ds_email.Tables(1).Rows(0)("phone"))
            body = body.Replace("<<bidder_email>>", ds_email.Tables(1).Rows(0)("email"))
            body = body.Replace("<<bidder_fax>>", ds_email.Tables(1).Rows(0)("fax"))
            body = body.Replace("<<bidder_comment>>", ds_email.Tables(1).Rows(0)("comment"))
            body = body.Replace("<<bidder_status>>", ds_email.Tables(1).Rows(0)("status"))
            'body = body.Replace("<<bidder_business_type>>", ds_email.Tables(1).Rows(0)("business_type"))
            'body = body.Replace("<<bidder_industry_type>>", ds_email.Tables(1).Rows(0)("industry_type"))
            body = body.Replace("<<bidder_find_us>>", ds_email.Tables(1).Rows(0)("find_us"))
            body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
            strSubject = ds_email.Tables(0).Rows(0)("email_subject")
            email_caption = ds_email.Tables(0).Rows(0)("email_name")
            If for_mail_content = False Then

                Dim email As String = CommonCode.fetch_profile_user_emails("BAA")
                If email <> "" Then
                    comm.sendEmail("", "", strSubject, body, email, "", "", "", "", "")
                End If

            End If
        End If
        comm = Nothing
        'HttpContext.Current.Response.Write("<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body)
            Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
    Public Function SendPayment_RequestEmail(ByVal _id As Integer, ByVal _type As String, Optional ByVal for_mail_content As Boolean = False) As String
        'Done
        Dim comm As New CommonCode()
        Dim email_caption As String = "Order Payment Request Mail"
        Dim body As String = ""
        Dim strSubject As String = ""

        If _id > 0 Then
            Dim ds_email As DataSet = New DataSet()
            Dim buyer_email As String = ""
            Dim buyer_name As String = ""
            Dim bid_date As DateTime = DateTime.Now
            Dim buy_type = "", receipt_body As String = ""
            Dim title = "", price = "", strqry As String = ""
            If _type = "bid" Then
                strqry = "select A.auction_id, C.Code AS Auction_Code,ISNULL(C.title,'') AS auction_title,ISNULL(A.bid_amount,0) AS bid_amount, D.code," & _
                "ISNULL(B.contact_first_name,'') As buyer_name,ISNULL(B.email,'') As buyer_email " & _
                " from tbl_auction_bids A WITH(NOLOCK) INNER JOIN tbl_reg_buyers B WITH(NOLOCK) ON A.buyer_id=B.buyer_id INNER JOIN tbl_auctions C WITH(NOLOCK) ON A.auction_id=C.auction_id INNER JOIN tbl_master_countries D WITH(NOLOCK) ON B.country_id = D.country_id where bid_id=" & _id
            'Else
            '    strqry = "select A.auction_id,A.bid_date,ISNULL(A.price,0) AS price,ISNULL(A.quantity,0) AS quantity,ISNULL(A.buy_type,'') As buy_type, C.auction_type_id," & _
            '                                        "ISNULL(B.contact_first_name,'') As buyer_name,ISNULL(B.email,'') As email," & _
            '                                        "ISNULL(C.title,'') AS auction_title from tbl_auction_buy A INNER JOIN tbl_reg_buyers B ON A.buyer_id=B.buyer_id " & _
            '                                        "INNER JOIN tbl_auctions C ON A.auction_id=C.auction_id where buy_id=" & _id
            End If

            ds_email = comm.GetEmail_ByCode("em43", strqry)
            If ds_email.Tables.Count = 2 AndAlso ds_email.Tables(1).Rows.Count > 0 AndAlso ds_email.Tables(0).Rows.Count > 0 Then

                buyer_email = ds_email.Tables(1).Rows(0)("buyer_email")
                buyer_name = ds_email.Tables(1).Rows(0)("buyer_name").ToString().Trim()
                title = ds_email.Tables(1).Rows(0)("auction_title")
                body = ds_email.Tables(0).Rows(0)("html_body")
                body = body.Replace("<<bidder_name>>", buyer_name)
                body = body.Replace("<<auction_code>>", ds_email.Tables(1).Rows(0)("Auction_Code").ToString().Trim())
                body = body.Replace("<<auction_title>>", title)
                body = body.Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
                If ds_email.Tables(1).Rows(0)("bid_amount") <= 10000.0 And ds_email.Tables(1).Rows(0)("code") = "US" Then
                    body = body.Replace("<<payment_link>>", "<tr><td colspan='2'><br/><br/>Please <a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/PaypalRequest.aspx?bd=" & Security.EncryptionDecryption.EncryptValueFormatted(_id) & "' target='_blank'>click here</a> to make a payment via paypal.</td></tr><tr><td colspan='2'><br /><span style='font-size: 16px; font-weight: bold;'>OR</span></td></tr>")
                    Else
                        body = body.Replace("<<payment_link>>", "")
                End If
                strSubject = ds_email.Tables(0).Rows(0)("email_subject")
                email_caption = ds_email.Tables(0).Rows(0)("email_name")

                If for_mail_content = False Then
                    If _type.ToLower = "bid" Then
                        comm.sendEmail(SqlHelper.of_FetchKey("payment_email_from"), SqlHelper.of_FetchKey("email_from_name"), strSubject, body, buyer_email, buyer_name, "", "", "", "")
                        SqlHelper.ExecuteNonQuery("update tbl_auction_bids set is_payment_link=1 where bid_id=" & _id & "; INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & buyer_email & "','" & strSubject.Replace("'", "''") & "','" & body.Replace("'", "''") & "','')")
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_buy set is_payment_link=1 where buy_id=" & _id & "; INSERT INTO tbl_sec_emails(submit_date, from_email, to_email, subject, body, attachment)  VALUES (getdate(), '" & SqlHelper.of_FetchKey("email_from") & "','" & buyer_email & "','" & strSubject.Replace("'", "''") & "','" & body.Replace("'", "''") & "','')")
                    End If
                End If


            End If
            ds_email = Nothing
        End If
        comm = Nothing

        Return "<b>Email Caption:</b> " & email_caption & "<br>" & strSubject & "<br><br>" & body
    End Function
End Class
