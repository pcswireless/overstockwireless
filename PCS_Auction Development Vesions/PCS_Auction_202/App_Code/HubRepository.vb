﻿Imports Microsoft.VisualBasic

Public Class HubRepository
    '---------------------------------------------------------
    '-- Method Name : fe_upd_auction_buyer_groups
    '-- Store PROCEDURE Called : fe_upd_auction_buyer_groups
    '-- Description : 
    '-- Creation Date : 11/13/2013
    '---------------------------------------------------------
    Public Function fe_upd_auction_buyer_groups(ByVal action As String, ByVal buyer_user_id As Integer, ByVal buyer_id As Integer, ByVal connection_id As String) As DataTable
        Dim dtDatatable As DataTable
        Dim SqlParameter() As SqlParameter = New SqlParameter(3) {}

        SqlParameter(0) = New SqlParameter("@action", SqlDbType.VarChar)
        SqlParameter(0).Size = 1
        SqlParameter(0).Direction = ParameterDirection.Input
        SqlParameter(0).Value = action

        SqlParameter(1) = New SqlParameter("@buyer_user_id", SqlDbType.Int)
        SqlParameter(1).Direction = ParameterDirection.Input
        SqlParameter(1).Value = buyer_user_id

        SqlParameter(2) = New SqlParameter("@buyer_id", SqlDbType.Int)
        SqlParameter(2).Direction = ParameterDirection.Input
        SqlParameter(2).Value = buyer_id

        SqlParameter(3) = New SqlParameter("@connection_id", SqlDbType.VarChar)
        SqlParameter(3).Size = 50
        SqlParameter(3).Direction = ParameterDirection.Input
        SqlParameter(3).Value = connection_id


        dtDatatable = SqlHelper.ExecuteDatatable(SqlHelper.of_getConnectString(), CommandType.StoredProcedure, "fe_upd_auction_buyer_groups", SqlParameter)

        Return dtDatatable
        dtDatatable.Dispose()
        dtDatatable = Nothing
        SqlParameter = Nothing
    End Function


End Class
