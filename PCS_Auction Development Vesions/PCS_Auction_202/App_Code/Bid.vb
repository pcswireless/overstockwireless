﻿Imports Microsoft.VisualBasic

Public Class Bid



    '---------------------------------------------------------
    '-- Method Name : insert_tbl_auction_bids
    '-- Store PROCEDURE Called : usp_insert_tbl_auction_bids
    '-- Description : 
    '-- Creation Date : 11/16/2011
    '---------------------------------------------------------
    Public Function insert_tbl_auction_bids(ByVal auction_id As Integer, ByVal buyer_user_id As Integer, ByVal max_bid_amount As Double, ByVal shipping_value As String, ByVal shipping_amount As Double, ByVal is_proxy_bid As Boolean) As Integer
        Dim out_id As Integer = 0
        Dim SqlParameter() As SqlParameter = New SqlParameter(6) {}

        SqlParameter(0) = New SqlParameter("@auction_id", SqlDbType.Int)
        SqlParameter(0).Direction = ParameterDirection.Input
        SqlParameter(0).Value = auction_id

        SqlParameter(1) = New SqlParameter("@buyer_user_id", SqlDbType.Int)
        SqlParameter(1).Direction = ParameterDirection.Input
        SqlParameter(1).Value = buyer_user_id

        SqlParameter(2) = New SqlParameter("@max_bid_amount", SqlDbType.Money)
        SqlParameter(2).Direction = ParameterDirection.Input
        SqlParameter(2).Value = max_bid_amount

        SqlParameter(3) = New SqlParameter("@shipping_value", SqlDbType.VarChar)
        SqlParameter(3).Size = 500
        SqlParameter(3).Direction = ParameterDirection.Input
        SqlParameter(3).Value = shipping_value

        SqlParameter(4) = New SqlParameter("@shipping_amount", SqlDbType.Money)
        SqlParameter(4).Direction = ParameterDirection.Input
        SqlParameter(4).Value = shipping_amount

        SqlParameter(5) = New SqlParameter("@id_generated", SqlDbType.Int)
        SqlParameter(5).Direction = ParameterDirection.Output

        SqlParameter(6) = New SqlParameter("@is_proxy_bid", SqlDbType.Bit)
        SqlParameter(6).Direction = ParameterDirection.Input
        SqlParameter(6).Value = is_proxy_bid

        SqlHelper.ExecuteNonQuery(SqlHelper.of_getConnectString(), CommandType.StoredProcedure, "usp_insert_tbl_auction_bids", SqlParameter)
        out_id = SqlParameter(5).Value
        SqlParameter = Nothing
        If out_id <> 0 Then
            sendmailtobidder(out_id)
        End If
        Return out_id
    End Function
    Private Sub sendmailtobidder(ByVal bid_id As Integer)
        Dim tbl As New DataTable()

        tbl = SqlHelper.ExecuteDatatable("select ISNULL(A.max_bid_amount, 0) AS max_bid_amount,ISNULL(B.first_name, '') AS first_name,ISNULL(B.last_name, '') AS last_name,ISNULL(B.email, '') AS email,ISNULL(C.title, '') AS title,ISNULL(C.code, '') AS code,dbo.buyer_bid_rank(A.auction_id,B.buyer_id) as buyer_rank,isnull(is_show_rank,0) as is_show_rank from tbl_auction_bids A inner join tbl_reg_buyer_users B on A.buyer_user_id=B.buyer_user_id inner join tbl_auctions C on A.auction_id=C.auction_id where A.bid_id=" & bid_id)
        If tbl.Rows.Count > 0 Then
            Dim strMailTo As String = tbl.Rows(0).Item("email").ToString()
            Dim strMailToName As String = tbl.Rows(0).Item("first_name").ToString()
            Dim aucName As String = tbl.Rows(0).Item("title").ToString()
            Dim aucCode As String = tbl.Rows(0).Item("code").ToString()
            Dim bidamount As String = "$" & FormatNumber(tbl.Rows(0).Item("max_bid_amount"), 2)
            Dim buyer_rank As String = tbl.Rows(0).Item("buyer_rank").ToString()
            Dim is_show_rank As Boolean = tbl.Rows(0).Item("is_show_rank")
            Dim Comm As New Email()
            Dim mailbdy As String = Comm.sendOutOfBidMail(strMailTo, strMailToName, aucName, aucCode, bidamount, bid_id, buyer_rank, is_show_rank)
            Comm = Nothing

        End If
        tbl = Nothing
    End Sub
    Public Function get_buyer_bid_rank(ByVal auction_id As Integer) As Integer
        Dim rank As Integer = 0
        Dim str As String = ""
        Dim buyer_id As String = CommonCode.Fetch_Cookie_Shared("buyer_id")
        If buyer_id <> "" Then
            str = "select RANK() over(order by MAX(bid_amount)) As rank from tbl_auction_bids where auction_id =" & auction_id & " Group By buyer_id having buyer_id=" & buyer_id & " order by MAX(bid_amount) desc"
            rank = SqlHelper.ExecuteScalar(str)
        End If
        Return rank
    End Function
    Public Function get_auction_bid_number(ByVal auction_id As Integer) As Integer
        Dim bids_num As Integer = 0
        Dim str As String = ""
        str = "select count(distinct buyer_id) from tbl_auction_bids where auction_id=" & auction_id
        bids_num = SqlHelper.ExecuteScalar(str)
        Return bids_num
    End Function
    Public Function get_bid_detail(ByVal bid_id As Integer) As DataTable
        Dim str As String = ""
        Dim dt As DataTable = New DataTable()
        str = "SELECT " & _
      "ISNULL(A.auction_id, 0) AS auction_id," & _      "ISNULL(A.buyer_id, 0) AS buyer_id," & _      "ISNULL(A.buyer_user_id, 0) AS buyer_user_id," & _      "ISNULL(A.bid_date, '1/1/1900') AS bid_date," & _      "ISNULL(A.max_bid_amount, 0) AS max_bid_amount," & _      "ISNULL(A.bid_amount, 0) AS bid_amount," & _      "ISNULL(A.bid_type, '') AS bid_type," & _      "ISNULL(A.send_email, 0) AS send_email," & _      "ISNULL(A.email_id, 0) AS email_id," & _      "ISNULL(A.action, '') AS action" & _
        " From tbl_auction_bids A WHERE A.bid_id = " & bid_id
        dt = SqlHelper.ExecuteDataTable(str)
        Return dt
        dt = Nothing
    End Function

    'comment by ajay:- old function i.e before confirm functionality 
    Public Function save_buy_now_bid(ByVal auction_id As Integer, ByVal price As Double, ByVal qty As Integer, ByVal buy_type As String, ByVal buyer_user_id As Integer, ByVal buyer_id As Integer, Optional ByVal shipping_value As String = "", Optional ByVal shipping_amount As Double = 0) As Integer
        Dim str As String = ""
        Dim dt As DataTable = New DataTable()
        Dim auto_acceptance_price_private As Double = 0
        Dim auto_acceptance_price_partial As Double = 0
        Dim auto_acceptance_quantity As Integer = 0
        Dim buy_now_price As Double = 0
        Dim buy_id As Integer = 0
        Dim auction_type_id As Integer = 0
        str = "SELECT " & _
             "ISNULL(A.auction_type_id, 0) AS auction_type_id," & _             "ISNULL(A.auto_acceptance_price_private, 0) AS auto_acceptance_price_private," & _             "ISNULL(A.auto_acceptance_price_partial, 0) AS auto_acceptance_price_partial," & _             "ISNULL(A.auto_acceptance_quantity, 0) AS auto_acceptance_quantity," & _             "ISNULL(increament_amount,0) AS increament_amount," & _             "ISNULL(A.buy_now_price, 0) AS buy_now_price, " & _             "ISNULL(A.show_price, 0) AS show_price, " & _             "ISNULL(A.is_accept_pre_bid, 0) AS is_accept_pre_bid " & _             "FROM tbl_auctions A WHERE A.auction_id =" & auction_id
        dt = SqlHelper.ExecuteDatatable(str)
        If dt.Rows.Count > 0 Then
            auto_acceptance_price_private = dt.Rows(0)("auto_acceptance_price_private")
            auto_acceptance_price_partial = dt.Rows(0)("auto_acceptance_price_partial")
            auto_acceptance_quantity = dt.Rows(0)("auto_acceptance_quantity")
            auction_type_id = dt.Rows(0)("auction_type_id")
            Dim status As String = ""
            If buy_type.ToLower() = "partial" Then
                If qty >= auto_acceptance_quantity And price >= auto_acceptance_price_partial Then
                    status = "Accept"
                Else
                    status = "Pending"
                End If
            ElseIf buy_type.ToLower() = "private" Then
                If price >= auto_acceptance_price_private Then
                    'status = "Auto Accept"
                    status = "Accept"
                Else
                    status = "Pending"
                End If
            ElseIf buy_type.ToLower() = "buy now" Then
                If auction_type_id = 1 Then
                    status = "Accept"
                    price = dt.Rows(0)("show_price")
                Else
                    price = dt.Rows(0)("buy_now_price")
                    status = "Accept"
                    'If price >= dt.Rows(0)("buy_now_price") Then
                    '    status = "Accept"
                    'Else
                    '    status = "Pending"
                    'End If
                End If

            End If

            Dim strError As String = ""
            Dim strQuery As String = ""

            strQuery = "INSERT into tbl_auction_buy(auction_id,buyer_user_id,buyer_id,bid_date,buy_type,price,quantity,status,shipping_value,shipping_amount)Values(" & auction_id & "," & buyer_user_id & "," & buyer_id & ",getdate(),'" & buy_type & "','" & price & "','" & qty & "','" & status & "','" & shipping_value & "','" & shipping_amount & "') select scope_identity()"

            buy_id = SqlHelper.ExecuteScalar(strQuery)
            dt = Nothing
        End If
        Return buy_id
    End Function

    ' add by ajay "function call from auctionconfirmation.aspx"
    Public Function save_buy_now_with_confirm(ByVal auction_id As Integer, ByVal buy_type As String, ByVal price As Double, ByVal qty As Integer, ByVal buyer_user_id As Integer, ByVal buyer_id As Integer, ByVal shipping_value As String, ByVal shipping_option As String, ByVal shipping_amount As Double, ByVal tax As Double, ByVal confirmation_no As String) As String
        Dim str As String = ""
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable()
        Dim auto_acceptance_price_private As Double = 0
        Dim auto_acceptance_price_partial As Double = 0
        Dim auto_acceptance_quantity As Integer = 0
        Dim buy_now_price As Double = 0
        Dim buy_id As Integer = 0
        Dim auction_type_id As Integer = 0
        Dim amount As Double = 0
        Dim grand_total_amount As Double = 0
        Dim status As String = ""
        Dim auto_reject_price_private As Double = 0
        Dim auto_reject_price_partial As Double = 0
        Dim auto_reject_quantity As Integer = 0
        str = "SELECT " & _
             "ISNULL(A.auction_type_id, 0) AS auction_type_id," & _             "ISNULL(A.auto_acceptance_price_private, 0) AS auto_acceptance_price_private," & _             "ISNULL(A.auto_acceptance_price_partial, 0) AS auto_acceptance_price_partial," & _             "ISNULL(A.auto_acceptance_quantity, 0) AS auto_acceptance_quantity," & _             "ISNULL(increament_amount,0) AS increament_amount," & _             "ISNULL(A.buy_now_price, 0) AS buy_now_price, " & _             "ISNULL(A.show_price, 0) AS show_price, " & _             "ISNULL(A.is_accept_pre_bid, 0) AS is_accept_pre_bid, " & _             "ISNULL(A.private_lower_amt, 0) AS private_lower_amt," & _             "ISNULL(A.partial_lower_amt, 0) AS partial_lower_amt, " & _             "ISNULL(A.partial_lower_qty, 0) AS partial_lower_qty " & _             "FROM tbl_auctions A WHERE A.auction_id =" & auction_id
        dt = SqlHelper.ExecuteDatatable(str)
        'HttpContext.Current.Response.Write(str)
        If dt.Rows.Count > 0 Then
            amount = qty * price
            grand_total_amount = amount + shipping_amount + tax
            auto_acceptance_price_private = dt.Rows(0)("auto_acceptance_price_private")
            auto_acceptance_price_partial = dt.Rows(0)("auto_acceptance_price_partial")
            auto_acceptance_quantity = dt.Rows(0)("auto_acceptance_quantity")
            auto_reject_price_private = dt.Rows(0)("private_lower_amt")
            auto_reject_price_partial = dt.Rows(0)("partial_lower_amt")
            auto_reject_quantity = dt.Rows(0)("partial_lower_qty")
            auction_type_id = dt.Rows(0)("auction_type_id")
            'buy_type = dt.Rows(0)("buy_type").ToString.ToLower
            If buy_type.ToLower() = "partial" Then
                If qty >= auto_acceptance_quantity And amount >= auto_acceptance_price_partial Then
                    status = "Accept"
                ElseIf qty <= auto_reject_quantity And amount <= auto_reject_price_partial Then
                    status = "Reject"
                Else
                    status = "Pending"
                End If
            ElseIf buy_type.ToLower() = "private" Then
                If price >= auto_acceptance_price_private Then
                    status = "Accept"
                ElseIf price <= auto_reject_price_private Then
                    status = "Reject"
                Else
                    status = "Pending"
                End If
            ElseIf buy_type.ToLower() = "buy now" Then
                If auction_type_id = 1 Then
                    status = "Accept"
                    price = dt.Rows(0)("show_price")
                Else
                    price = dt.Rows(0)("buy_now_price")
                    status = "Accept"
                    'If price >= dt.Rows(0)("buy_now_price") Then
                    '    status = "Accept"
                    'Else
                    '    status = "Pending"
                    'End If
                End If

            End If

            Dim strError As String = ""

            strQuery = "INSERT into tbl_auction_buy (auction_id,buyer_user_id,buyer_id,bid_date,buy_type,price,quantity,status,shipping_value,shipping_option,shipping_amount,amount,sub_total_amount,tax,grand_total_amount,confirmation_no) Values (" & _
                auction_id & "," & buyer_user_id & "," & buyer_id & ",getdate(),'" & buy_type & "','" & price & "','" & qty & "','" & status & "','" & shipping_value & "','" & shipping_option & "'," & shipping_amount & "," & amount & "," & amount & "," & tax & "," & grand_total_amount & ",'" & confirmation_no & "') select isnull((select buy_type+'#'+cast(buy_id as varchar(10)) from tbl_auction_buy where buy_id=(scope_identity())),'')"
            'HttpContext.Current.Response.Write(strQuery)
            buy_type = SqlHelper.ExecuteScalar(strQuery)
            dt = Nothing
        End If

        Return buy_type
    End Function
    Public Function send_quotation(ByVal auction_id As Integer, ByVal quote As String, ByVal buyer_user_id As Integer, ByVal buyer_id As Integer) As Integer
        Dim Quotation_id As Integer = 0
        Try
            Dim str As String = ""
            Dim details As String = quote
            str = "Insert into tbl_auction_quotations(auction_id,buyer_user_id,buyer_id,bid_date,details,filename) Values (" & auction_id & "," & buyer_user_id & "," & buyer_id & ",getdate(),'" & details & "','') select scope_identity()"
            Quotation_id = SqlHelper.ExecuteScalar(str)
        Catch ex As Exception
        End Try
        Return Quotation_id
    End Function
    Public Sub upload_quotation_file(ByVal auction_id As Integer, ByVal quotation_id As Integer, ByVal fl_quote_file As System.Web.UI.WebControls.FileUpload, ByVal buyer_user_id As Integer)
        Dim filename As String = ""
        If fl_quote_file.HasFile Then
            filename = fl_quote_file.FileName
            filename = buyer_user_id.ToString() & "_" & filename
            If quotation_id > 0 Then
                Dim pathToCreate As String = "/Upload/Quotation/" & auction_id.ToString() '& "/" & Quotation_id.ToString()
                If Not System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(pathToCreate)) Then
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(pathToCreate))
                End If
                Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(HttpContext.Current.Server.MapPath(pathToCreate) & "/" & filename)
                If infoFile.Exists Then
                    System.IO.File.Delete(HttpContext.Current.Server.MapPath(pathToCreate) & "/" & filename)
                End If
                fl_quote_file.PostedFile.SaveAs(HttpContext.Current.Server.MapPath(pathToCreate) & "/" & filename)
                Dim str As String = ""
                str = "update tbl_auction_quotations set filename='" & filename & "' where quotation_id=" & quotation_id
                SqlHelper.ExecuteNonQuery(str)
            End If
        End If

    End Sub
End Class
