﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AddEditSubLogin.aspx.vb"
    Inherits="AddEditSubLogin" MasterPageFile="~/SitePopUp.master" Title="PCS Bidding" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">

        /***********************************************
        * Different CSS depending on OS (mac/pc)- © Dynamic Drive (www.dynamicdrive.com)
        * This notice must stay intact for use
        * Visit http://www.dynamicdrive.com/ for full source code
        ***********************************************/
        ///////No need to edit beyond here////////////

        var mactest = navigator.userAgent.indexOf("Mac") != -1


        if (mactest) {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet_mac.css"/>');
        }
        else {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet.css"/>');
        }
  </script>
    <script>
        function closeWin() {
            myWindow.close();                                                // Closes the new window
        }
    </script>
    <div id="bid-popup" style="width: 100%;">
        <div>
            <h2>
                <span class="blue">
                    <%=IIf(IsNumeric(Request.QueryString.Get("i")), "Edit Sub-Login", "Add Sub-Login")%></span></h2>
        </div>
        <br />
        <br />
        <!-- <table cellpadding="5" cellspacing="0" width="100%" border="0">-->
        <div class="inputboxes" style="margin: 0;">
            <%--<tr id="tr_price" runat="server"> --%>
            <!-- <td>-->
            <div id="tr_price" class="" runat="server" style="padding-bottom: 10px; text-align: left;">
                <table id="tbl_grd_item_edit" cellpadding="0" cellspacing="2" border="0">
                    <tr>
                        <td  style="width: 135px;">
                            First Name&nbsp;<span class="req_star">*</span>
                        </td>
                        <td style="width: 260px;" class="details">
                            <asp:TextBox ID="txt_sub_first_name" runat="server" CssClass="" Style="margin-top: 5px;" Width="150"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfv_first_name" runat="server" ErrorMessage="*" ForeColor="red"
                                ControlToValidate="txt_sub_first_name" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                        </td>
                        <td  style="width: 135px;">
                            Last Name
                        </td>
                        <td style="width: 260px;" class="details">
                            <asp:TextBox ID="txt_sub_last_name" runat="server" CssClass="" Style="margin-top: 5px;" Width="150"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            Title
                        </td>
                        <td class="details" style ="padding-left:18px;">
                            <asp:DropDownList ID="dd_sub_title" DataTextField="name" DataValueField="title_id" DataSourceID="sub_login_title"
                                runat="server" CssClass="typtxt" Style="margin-top: 5px;" Width="150">
                            </asp:DropDownList>
                        </td>
                        <td >
                            Email&nbsp;<span class="req_star">*</span>
                        </td>
                        <td class="details">
                            <asp:TextBox ID="txt_sub_email" runat="server" CssClass="" Style="margin-top: 5px;" Width="150"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfv_email" runat="server" ErrorMessage="*" ForeColor="red"
                                ControlToValidate="txt_sub_email" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ValidationGroup="val1_sub" ID="rev_email" runat="server"
                                ControlToValidate="txt_sub_email" Display="Dynamic" ErrorMessage="*" ValidationExpression="^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$"
                                CssClass="error"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            User Name&nbsp;<span class="req_star">*</span>
                        </td>
                        <td class="details">
                            <asp:TextBox ID="txt_sub_username"  runat="server" CssClass="" Style="margin-top: 5px;" Width="150"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfv_username" runat="server" ErrorMessage="*" ForeColor="red"
                                ControlToValidate="txt_sub_username" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                        </td>
                        <td  style="width: 145px;">
                            Password&nbsp;<span class="req_star">*</span>
                        </td>
                        <td class="details">
                            <asp:TextBox ID="txt_sub_password" runat="server" TextMode="Password" CssClass="" Style="margin-top: 5px;" Width="150"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfv_password" runat="server" ErrorMessage="*" ForeColor="red"
                                ControlToValidate="txt_sub_password" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rev_password" runat="server" Display="Dynamic"
                                ControlToValidate="txt_sub_password" ValidationGroup="val1_sub" CssClass="error"
                                ErrorMessage="<br>Alphanumeric of atleast 6 chars" ValidationExpression="(?=.*\d)(?=.*[a-zA-Z]).{6,}"></asp:RegularExpressionValidator>
                            <asp:HiddenField ID="hid_sub_password" runat="server" Value='<%# Security.EncryptionDecryption.DecryptValue(IIF(Eval("password") IS DBNULL.Value,"",Eval("password"))) %>' />
                        </td>
                    </tr>
                    <tr>
                        <td >
                            Bidding&nbsp;Limit
                        </td>
                        <td class="details">
                            <asp:TextBox ID="txt_sub_bidding_limit" runat="server" MaxLength="8" CssClass="" Style="margin-top: 5px;" Width="150">
                            </asp:TextBox><asp:RegularExpressionValidator ID="rev_bidding_limit" runat="server" Display="Dynamic"
                                ControlToValidate="txt_sub_bidding_limit" ValidationGroup="val1_sub" CssClass="error"
                                ErrorMessage="<br />Invalid" ValidationExpression="^\d*\.?\d*$"></asp:RegularExpressionValidator>
                            
                        </td>
                        <td >
                            Is Active
                        </td>
                        <td ><p class="checkbox" style=" margin-top:-10px;">
                            <div style="margin-left:-28px;"><asp:CheckBox ID="chk_sub_is_active" runat="server" CssClass="" /></div></p>
                            <asp:HiddenField ID="hid_old_active_status" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <asp:Literal ID="lbl_grid_error" runat="server"></asp:Literal>
                        </td>
                        <td align="right" colspan="2">
                        </td>
                    </tr>
                </table>
                <div class="clear">
                    <br />
                </div>
            </div>
        </div>
    </div>
    <!--inputboxes class ends-->
    <div id="change_pwd" style="width: 100%;">
        <div style="float: left;">
            <asp:Button ID="imgbtn_update" ValidationGroup="val1_sub" runat="server" Text="Update"
                CssClass="" /> <asp:SqlDataSource ID="sub_login_title" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                    runat="server" ProviderName="System.Data.SqlClient" SelectCommand="select '--Select--' as name,'' as title_id union all select isnull(name,'') as name,isnull(name,'') as title_id from tbl_master_titles order by name"></asp:SqlDataSource>
                           
        </div>
        <div>
            <input type="button" name="" id="popupClose" onclick="javascript: window.close();"
                class="fleft" value="Cancel" />
        </div>
    </div>
    <br />
    <div>
        <asp:Literal ID="lit_message" runat="server" EnableViewState="false"></asp:Literal>
        <asp:Label ID="lbl_error" runat="server"></asp:Label>
    </div>
    <script type ="text/javascript" >
        window.focus();
    </script>
</asp:Content>
