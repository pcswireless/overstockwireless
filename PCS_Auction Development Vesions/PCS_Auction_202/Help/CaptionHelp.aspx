﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false" CodeFile="CaptionHelp.aspx.vb" Inherits="Help_CaptionHelp" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" Runat="Server">
<telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
          function Cancel_Ajax(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                 args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
              args.set_enableAjax(false);
            }
            else {
              args.set_enableAjax(true);
            }
          }
        </script>
    </telerik:RadScriptBlock>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                  Frontend Help List
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; width: 40%; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    Please select the title to edit from below
                </div>
                
            </td>
        </tr>
           <tr>
            <td>
                <div style="height: 12px; display: block;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>

                <telerik:RadGrid ID="rad_helps" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="True"
                    AllowSorting="True"  ShowFooter="False" AllowPaging="false" Skin="Vista" ShowGroupPanel="false"
                    DataSourceID="SqlDataSource1" Width="750px" >
                         <ClientSettings AllowDragToGroup="true">
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                 <GroupingSettings ShowUnGroupButton="false" />
                    <MasterTableView DataKeyNames="help_id" AutoGenerateColumns="false" AllowFilteringByColumn="True"
                        ItemStyle-Height="40" AlternatingItemStyle-Height="40" CommandItemDisplay="Top">
                        <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false" ShowExportToPdfButton="false"
                            ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false"/>
                        <NoRecordsTemplate>
                            No help to display
                        </NoRecordsTemplate>
                         <SortExpressions>
                            <telerik:GridSortExpression FieldName="caption" SortOrder="Ascending" />
                        </SortExpressions>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Caption" SortExpression="caption" UniqueName="caption"
                                DataField="caption" FilterControlWidth="180" HeaderStyle-Width="200" ItemStyle-Width="190">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Help/AddEditCaptionHelp.aspx?i=<%# Eval("help_id") %>')" 
                                                            onmouseout="hide_tip_new();" onmouseover="tip_new('<%#Eval("description") %>','250','white');">
                                        <%# Eval("caption")%></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false" ItemStyle-Width="50">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Help/AddEditCaptionHelp.aspx?i=<%# Eval("help_id") %>')">
                                        <img src="/images/edit.png" style="border: none;" alt="Edit" /></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="select help_id, caption, [description] from tbl_caption_help order by help_id">
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

