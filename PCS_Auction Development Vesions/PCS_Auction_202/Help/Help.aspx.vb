﻿Imports Telerik.Web.UI

Partial Class Help_Help
    Inherits System.Web.UI.Page
    Private Sub setPermission()

        If CommonCode.is_super_admin() Then
            rad_helps.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
            rad_helps.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
            rad_helps.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
        Else

            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 35
                        rad_helps.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
                        rad_helps.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
                        rad_helps.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
                End Select
            Next
            dt = Nothing
        End If

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Protected Sub rad_helps_PreRender(sender As Object, e As System.EventArgs) Handles rad_helps.PreRender
        Dim menu As GridFilterMenu = rad_helps.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub
End Class
