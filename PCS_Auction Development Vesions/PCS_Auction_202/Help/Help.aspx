﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="Help.aspx.vb" Inherits="Help_Help" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
<telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                     args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
                else {
                    args.set_enableAjax(true);
                }
            }


           

        </script>
    </telerik:RadScriptBlock>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Backend Help List
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; width: 40%; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    Please select the title to edit from below
                </div>
                
            </td>
        </tr>
           <tr>
            <td>
                <div style="height: 12px; display: block;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                <ClientEvents OnRequestStart="Cancel_Ajax" />
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="rad_helps">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="rad_helps" LoadingPanelID="RadAjaxLoadingPanel1" />
                                <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="RadAjaxPanel1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Simple" />
                <telerik:RadGrid ID="rad_helps" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="True"
                    AllowSorting="True" PageSize="10" ShowFooter="False" AllowPaging="True" Skin="Vista" ShowGroupPanel="true"
                    DataSourceID="SqlDataSource1">
                    <ExportSettings IgnorePaging="true" FileName="Help_Export" 
                        OpenInNewWindow="true" ExportOnlyData="true" />
                         <ClientSettings AllowDragToGroup="true">
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                 <GroupingSettings ShowUnGroupButton="true" />
                    <MasterTableView DataKeyNames="help_id" AutoGenerateColumns="false" AllowFilteringByColumn="True"
                        ItemStyle-Height="40" AlternatingItemStyle-Height="40" CommandItemDisplay="Top">
                        <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false" ShowExportToPdfButton="false"
                            ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                        <NoRecordsTemplate>
                            No help to display
                        </NoRecordsTemplate>
                         <SortExpressions>
                            <telerik:GridSortExpression FieldName="title" SortOrder="Ascending" />
                        </SortExpressions>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Title" SortExpression="title" UniqueName="title"
                                DataField="title" FilterControlWidth="180" HeaderStyle-Width="200" ItemStyle-Width="190" GroupByExpression="title [Title] group by title">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Help/AddEditHelp.aspx?i=<%# Eval("help_id") %>')">
                                        <%# Eval("title")%></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Name" SortExpression="name" UniqueName="name"
                                DataField="name" FilterControlWidth="150" GroupByExpression="name [Name] group by name">
                                <ItemTemplate>
                                    <%# Eval("name")%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false" ItemStyle-Width="50">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Help/AddEditHelp.aspx?i=<%# Eval("help_id") %>')">
                                        <img src="/images/edit.png" style="border: none;" alt="Edit" /></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="select [help_id],[name],[title],[description] from [tbl_help] where is_active=1 order by help_id">
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
