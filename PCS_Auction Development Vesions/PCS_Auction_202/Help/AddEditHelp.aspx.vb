﻿Imports Telerik.Web.UI
Imports System.Drawing
Partial Class Help_AddEditHelp
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Private Sub setPermission()
        If Not CommonCode.is_super_admin() Then
            Dim Vew_Help As Boolean = False
            Dim Edit_Help As Boolean = False

            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 26
                        Vew_Help = True
                    Case 27
                        Edit_Help = True
                End Select
            Next
            dt = Nothing

            If Vew_Help = False Then Response.Redirect("/NoPermission.aspx")
            If Not Edit_Help Then
                pnl_help_buttons.Visible = False
            End If
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                bind_help()
                set_form_mode(False)
                set_button_visibility("View")

            Else
                set_form_mode(True)
                set_button_visibility("Add")
                page_heading.Text = "New Help"
            End If
        End If
    End Sub
    Private Sub set_button_visibility(ByVal mode As String)
        If mode.ToUpper() = "VIEW" Then
            img_button_edit.Visible = True
            img_button_save.Visible = False
            img_button_update.Visible = False
            div_basic_cancel.Visible = False
        ElseIf mode.ToUpper() = "EDIT" Then
            img_button_edit.Visible = False
            img_button_save.Visible = False
            img_button_update.Visible = True
            div_basic_cancel.Visible = True
        Else
            img_button_edit.Visible = False
            img_button_save.Visible = True
            img_button_update.Visible = False
            div_basic_cancel.Visible = False
        End If
    End Sub
    Private Sub set_form_mode(ByVal flag As Boolean)
        txt_name.Visible = flag
        txt_title.Visible = flag
        RadEditor1.Visible = flag
        tbl.Visible = flag
        span_desc.Visible = flag
        span_title.Visible = flag
        span_name.Visible = flag

        lbl_title.Visible = Not (flag)
        lbl_name.Visible = Not (flag)
        lbl_description.Visible = Not (flag)
        
    End Sub
    Private Sub bind_help()
        Dim strQuery As String = ""
        Dim dt As DataTable
        strQuery = "select help_id,ISNULL(title,'') AS title,ISNULL(name,'') AS name,ISNULL(description,'') AS description from tbl_help where help_id=" & Request.QueryString("i")
        dt = SqlHelper.ExecuteDataTable(strQuery)
        If dt.Rows.Count > 0 Then
            page_heading.Text = dt.Rows(0)("title")
            txt_name.Text = dt.Rows(0)("name")
            lbl_name.Text = dt.Rows(0)("name")
            txt_title.Text = dt.Rows(0)("title")
            lbl_title.Text = dt.Rows(0)("title")
            RadEditor1.Content = dt.Rows(0)("description")
            lbl_description.Text = dt.Rows(0)("description")
        End If
    End Sub

    Protected Sub img_button_save_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles img_button_save.Click
        If Page.IsValid Then
            Dim strQuery As String = ""
            strQuery = "Insert into tbl_help(name,title,description)Values('" & CommonCode.encodeSingleQuote(txt_name.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_title.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(RadEditor1.Content) & "') select scope_identity()"
            Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)
            If i > 0 Then
                Response.Redirect("/Help/Help.aspx")
            End If
        End If
        
    End Sub

    Protected Sub img_button_update_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles img_button_update.Click
        If Page.IsValid Then
            Dim strQuery As String = ""
            strQuery = "Update tbl_help set name='" & CommonCode.encodeSingleQuote(txt_name.Text.Trim()) & "',title='" & CommonCode.encodeSingleQuote(txt_title.Text.Trim()) & "',description='" & CommonCode.encodeSingleQuote(RadEditor1.Content) & "' where help_id=" & Request.QueryString("i")
            SqlHelper.ExecuteScalar(strQuery)
            set_form_mode(False)
            set_button_visibility("View")
            lbl_msg.Text = "Help updated successfully."
            bind_help()
        End If
    End Sub
    Protected Sub img_button_edit_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles img_button_edit.Click
        set_form_mode(True)
        set_button_visibility("Edit")
    End Sub
    Protected Sub img_button_cancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles img_button_cancel.Click
        set_form_mode(False)
        set_button_visibility("View")
    End Sub
End Class
