﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false" CodeFile="flag_master.aspx.vb" Inherits="Master_flag_master" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:radwindowmanager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Skin="Simple" />
    <input type="hidden" id="hid_stockloc_id" name="hid_stockloc_id"
        value="0" />
    <asp:TextBox ID="txt_test" runat="server" Visible="false"></asp:TextBox>
    <telerik:radajaxloadingpanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" 
        BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" alt="" />
    </telerik:radajaxloadingpanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Flag Listing</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:radajaxpanel ID="pnl1" runat="server" 
                    LoadingPanelID="RadAjaxLoadingPanel1">
                    <telerik:RadGrid ID="RadGrid_Titles" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                        AllowAutomaticInserts="True" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True"
                        AllowSorting="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                        AllowMultiRowSelection="false" AllowMultiRowEdit="false" Skin="Vista" ShowGroupPanel="False">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <HeaderStyle BackColor="#BEBEBE" />
                        <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="flag_id" DataSourceID="SqlDataSource1"
                            HorizontalAlign="NotSet" AutoGenerateColumns="False" SkinID="Vista" ItemStyle-Height="40" EditMode="EditForms"
                            AlternatingItemStyle-Height="40">
                            <CommandItemStyle BackColor="#E1DDDD" />
                            <NoRecordsTemplate>
                                No flag listing available
                            </NoRecordsTemplate>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="flag" SortOrder="Ascending" />
                            </SortExpressions>
                            <CommandItemSettings AddNewRecordText="Add New Flag" />
                        
                            <Columns>
                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                    EditImageUrl="/Images/edit_grid.gif">
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                </telerik:GridEditCommandColumn>
                                <telerik:GridTemplateColumn HeaderText="Name" SortExpression="name" UniqueName="name"
                                    DataField="name" EditFormHeaderTextFormat="Title Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_flag" runat="server" Text='<%# Eval("flag")%>' ></asp:Label>
                                        <asp:HiddenField ID="hd_count" runat="server" Value='<%# Eval("count")%>' />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Flag ID" SortExpression="flag_id" UniqueName="flag_id"
                                    DataField="flag_id" EditFormHeaderTextFormat="Flag ID">
                                    <ItemTemplate>
                                        <%# Eval("flag_id")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn HeaderText="Flag Name" SortExpression="flag" UniqueName="flag"
                                    DataField="flag" EditFormHeaderTextFormat="flag">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("flag")%>' ></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>                           
                              
                                <telerik:GridTemplateColumn HeaderText="Is Active" SortExpression="is_active" UniqueName="is_active"
                                    DataField="is_active" EditFormHeaderTextFormat="Is Active">
                                    <ItemTemplate>
                                        <%# Eval("is_active")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                    CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="50" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <EditFormSettings InsertCaption="New Items" EditFormType="Template">
                                <FormTemplate>
                                    <table id="tbl_grd_item_edit" cellpadding="0" cellspacing="2" border="0" width="800px">
                                        <tr>
                                            <td style="font-weight: bold; padding-top: 10px;" colspan="4">
                                                Flag Listing Details
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 135px;">
                                                Flag Name&nbsp;<span class="req_star">*</span>
                                            </td>
                                            <td style="width: 260px;" class="details">
                                                <asp:TextBox ID="txt_grd_name" TabIndex="1" runat="server" Text='<%#Bind("flag") %>' CssClass="inputtype" />
                                                <asp:RequiredFieldValidator ID="Req_txt_grd_name" runat="server" Font-Size="10px"
                                                    ValidationGroup="grd_items" ControlToValidate="txt_grd_name" Display="Dynamic"
                                                    ErrorMessage="<br>Name Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="CustomValidator1" ControlToValidate="txt_grd_name" Font-Size="10px" ForeColor="red" OnServerValidate="Checkname"
                                                    ErrorMessage="</br>Name Already Exists" runat="server" Display="Dynamic" ValidationGroup="grd_items"/>
                                                    <asp:HiddenField ID="hd_id" runat="server" Value='<%#Bind("flag_id") %>' />
                                            </td>
                                            <td align="right" colspan="2" rowspan="3">
                                                <asp:ImageButton ID="but_grd_submit" TabIndex="4" CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem),"PerformInsert","Update") %>'
                                                    ValidationGroup="grd_items" OnClientClick="return ValidationGroupEnable('grd_items', true);"
                                                    AlternateText='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "Save" , "Update") %>'
                                                    runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif" , "/images/update.gif") %>' />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:ImageButton ID="but_grd_cancel" TabIndex="5" CausesValidation="false" CommandName="Cancel"
                                                    runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                            </td>
                                        </tr>                                                                            

                                        <tr>
                                        <td class="caption" style="width: 135px;">
                                                Is Active 
                                            </td>
                                            <td style="width: 260px;" class="details">
                                                
                                            <asp:Label ID="lbl_basic_active" runat="server" />

                                               <asp:CheckBox ID="chk_basic_active" runat="server" Checked='<%#Bind("is_active") %>'  CssClass="inputtype" /> 
                                                </td>                                    
                                        </tr> 
                                       
                                        
                                    </table>
                                </FormTemplate>
                            </EditFormSettings>

                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                    </telerik:RadGrid>
                </telerik:radajaxpanel>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT ISNULL(A.flag_id, 0) AS flag_id, ISNULL(A.flag, '') AS flag, ISNULL(A.is_active, 0) AS is_active, case when exists (select flag_id from tbl_auction_flags where flag_id=A.flag_id) then 1 else 0 end 'count' FROM tbl_master_flags A order by A.flag"
                    DeleteCommand="DELETE FROM [tbl_master_flags] WHERE [flag_id] = @flag_id"
                    InsertCommand="INSERT INTO tbl_master_flags(flag,is_active) 
    VALUES (@flag,@is_active)" UpdateCommand="UPDATE tbl_master_flags SET 
       flag= @flag,is_active=@is_active WHERE flag_id= @flag_id">
                    <DeleteParameters>
                        <asp:Parameter Name="flag_id" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="flag_id" Type="Int32" />
                        <asp:Parameter Name="flag" Type="String" />
                        <asp:Parameter Name="is_active" Type="Boolean"  />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="flag" Type="String" />
                        <asp:Parameter Name="is_active" Type="Boolean"  />
                    </InsertParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

