﻿Imports Telerik.Web.UI
Partial Class Master_header_master
    Inherits System.Web.UI.Page
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        SetPermission()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      
    End Sub
    Private Sub SetPermission()
        If CommonCode.is_super_admin() Then
            div_new_auction.Visible = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
        Else
            div_new_auction.Visible = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 7
                        div_new_auction.Visible = True
                    Case 35
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
                End Select
            Next
            dt = Nothing

        End If

    End Sub

    Protected Sub RadGrid1_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid1.NeedDataSource
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            Dim strQuery As String = ""
            strQuery = "SELECT A.header_id, ISNULL(A.header_name, '') AS header_name, ISNULL(A.header_html, '') AS header_html, ISNULL(A.row_create_date, '1/1/1900') AS row_create_date, isnull(A.is_active,'False') as is_active FROM tbl_master_email_headers A"

            RadGrid1.DataSource = SqlHelper.ExecuteDatatable(strQuery)

        End If

    End Sub
    Protected Sub RadGrid1_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid1.PreRender
        Dim menu As GridFilterMenu = RadGrid1.FilterMenu
        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While

        If (Not Page.IsPostBack) Then
            RadGrid1.MasterTableView.FilterExpression = "([is_active] = True) "
            Dim column As GridColumn = RadGrid1.MasterTableView.GetColumnSafe("is_active")
            column.CurrentFilterFunction = GridKnownFunction.EqualTo
            column.CurrentFilterValue = "True"
            RadGrid1.MasterTableView.Rebind()
        End If
    End Sub
End Class
