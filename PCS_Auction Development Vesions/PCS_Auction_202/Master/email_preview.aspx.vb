﻿
Partial Class Master_email_preview
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If IsNumeric(Request.QueryString.Get("i")) AndAlso Request.QueryString.Get("i") > 0 Then
                hid_id.Value = Request.QueryString.Get("i")
                If Not String.IsNullOrEmpty(Request.QueryString.Get("t")) Then
                    If Request.QueryString.Get("t") = "h" Then
                        fill_header_data()
                    ElseIf Request.QueryString.Get("t") = "e" Then
                        fill_email_data()
                    ElseIf Request.QueryString.Get("t") = "f" Then
                        fill_footer_data()
                    End If
                Else
                    If (IsNumeric(Request.QueryString.Get("h")) And IsNumeric(Request.QueryString.Get("f"))) AndAlso (Request.QueryString.Get("f") > 0 And Request.QueryString.Get("h") > 0) Then

                        fill_emailedit_data()
                    End If
                End If
            Else
                hid_id.Value = 0
            End If



            'If IsNumeric(Request.QueryString.Get("i")) AndAlso Request.QueryString.Get("i") > 0 Then
            '    hid_id.Value = Request.QueryString.Get("i")

            '    If Not String.IsNullOrEmpty(Request.QueryString.Get("t")) Then
            '        If Request.QueryString.Get("t") = "h" Then
            '            fill_header_data()
            '        ElseIf Request.QueryString.Get("t") = "e" Then
            '            fill_email_data()
            '        ElseIf Request.QueryString.Get("t") = "f" Then
            '            fill_footer_data()
            '        End If
            '    End If
            'Else
            '    hid_id.Value = 0

            'End If

        End If

    End Sub

    Private Sub fill_header_data()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset("SELECT ISNULL(A.header_html, '') AS header_html FROM tbl_master_email_headers A WHERE A.header_id = " & hid_id.Value & "")
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)
                page_heading1.Text = .Item("header_html").Replace("<<site_url>>", SqlHelper.of_FetchKey("serverhttp"))
            End With
        End If

    End Sub
    Private Sub fill_email_data()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset("SELECT ISNULL(a.header_html, '') as header_html,isnull(b.email_html,'') as email_html,isnull(c.footer_html,'') as footer_html FROM tbl_master_emails b join tbl_master_email_headers a on a.header_id=b.header_id join tbl_master_email_footers c on b.footer_id=c.footer_id WHERE b.master_email_id= " & hid_id.Value & "")
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)
                page_heading1.Text = .Item("header_html")
                page_heading1.Text = page_heading1.Text & "<br>" & .Item("email_html")
                page_heading1.Text = page_heading1.Text & "<br>" & .Item("footer_html")
                page_heading1.Text = page_heading1.Text.Replace("<<site_url>>", SqlHelper.of_FetchKey("serverhttp"))

            End With
        End If

    End Sub
    Private Sub fill_footer_data()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset("SELECT ISNULL(A.footer_html, '') AS footer_html FROM tbl_master_email_footers A WHERE A.footer_id = " & hid_id.Value & "")
        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)
                page_heading1.Text = .Item("footer_html")
            End With
        End If

    End Sub

    Private Sub fill_emailedit_data()
        Dim ds As New DataSet
        page_heading1.Text = ""
        'ds = SqlHelper.ExecuteDataset("SELECT ISNULL(B.header_html, '') AS header_html, ISNULL(A.email_html, '') AS email_html, ISNULL(C.footer_html, '') AS footer_html FROM tbl_master_emails A INNER JOIN tbl_master_email_headers B ON	a.header_id = B.header_id INNER JOIN tbl_master_email_footers C ON A.footer_id = C.footer_id WHERE A.master_email_id= " & hid_id.Value & " and B.header_id=" & Request.QueryString.Get("h") & " and C.footer_id=" & Request.QueryString.Get("f") & "")
        ds = SqlHelper.ExecuteDataset("SELECT ISNULL(B.header_html, '') AS header_html from tbl_master_email_headers B where header_id = " & Request.QueryString.Get("h") & ";SELECT ISNULL(A.email_html, '') AS email_html from tbl_master_emails A where master_email_id = " & hid_id.Value & ";SELECT ISNULL(C.footer_html, '') AS footer_html from tbl_master_email_footers C where footer_id =" & Request.QueryString.Get("f") & "")
        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)
                page_heading1.Text = .Item("header_html")

            End With
        End If
        If ds.Tables(1).Rows.Count > 0 Then
            With ds.Tables(1).Rows(0)
                page_heading1.Text = page_heading1.Text & .Item("email_html")
            End With
        End If
        If ds.Tables(2).Rows.Count > 0 Then
            With ds.Tables(2).Rows(0)
                page_heading1.Text = page_heading1.Text & .Item("footer_html")
            End With
        End If
        page_heading1.Text = page_heading1.Text.Replace("<<site_url>>", SqlHelper.of_FetchKey("serverhttp"))



        'page_heading1.Text = page_heading1.Text & "<br>" & .Item("email_html")


    End Sub
End Class
