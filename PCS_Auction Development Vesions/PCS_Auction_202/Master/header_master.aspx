﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false" 
CodeFile="header_master.aspx.vb" Inherits="Master_header_master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                     args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
                else {
                    args.set_enableAjax(true);
                }
            }

        </script>
    </telerik:RadScriptBlock>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Email Header Template</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; width: 40%; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    Please select the email header name to edit from below
                </div>
                <div style="float: right; width: 40%; text-align: right; margin-bottom: 3px;" id="div_new_auction"
                    runat="server">
                    <a style="text-decoration: none" href="/Master/header_details.aspx">
                        <img src="/images/addnewheader.png" style="border: none" alt="Add New Header" /></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                    <ClientEvents OnRequestStart="Cancel_Ajax" />
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="RadGrid1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
                    left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
                    <img id="Image8" src="/images/img_loading.gif" />
                </telerik:RadAjaxLoadingPanel>
                    <telerik:RadGrid ID="RadGrid1" AllowFilteringByColumn="True" AutoGenerateColumns="false"
                    AllowSorting="True" PageSize="10" ShowFooter="False" ShowGroupPanel="false" AllowPaging="True"
                    runat="server" Skin="Vista" EnableLinqExpressions="false">
                    <PagerStyle Mode="NextPrevAndNumeric" />
                    <ExportSettings IgnorePaging="true" FileName="Email_Header_Export" OpenInNewWindow="true"
                        ExportOnlyData="true" />
                    <MasterTableView DataKeyNames="header_id" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                        AllowFilteringByColumn="True" ItemStyle-Height="40" AlternatingItemStyle-Height="40"
                        CommandItemDisplay="Top">
                        <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false" ShowExportToPdfButton="false"
                            ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            <NoRecordsTemplate>
                                No Email Header to display
                            </NoRecordsTemplate>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="header_name" SortOrder="Ascending" />
                            </SortExpressions>
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="Header Name" SortExpression="header_name"
                                    AutoPostBackOnFilter="false" FilterListOptions="VaryByDataType" ShowFilterIcon="true" UniqueName="header_name" DataField="header_name" FilterControlWidth="150" >
                                    <ItemTemplate>
                                               <%# Eval("header_name")%></a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>  
                                <telerik:GridTemplateColumn UniqueName="is_active" DataType="System.Boolean" DataField="is_active"
                                    SortExpression="is_active" HeaderText="Is Active">
                                    <FilterTemplate>
                                        <telerik:RadComboBox ID="RadComboBo_Chk_is_active" runat="server" OnClientSelectedIndexChanged="ActiveIndexChanged"
                                            Width="120px" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("is_active").CurrentFilterValue %>'>
                                            <Items>
                                                <telerik:RadComboBoxItem Text="All" Value="" />
                                                <telerik:RadComboBoxItem Text="Active" Value="True" />
                                                <telerik:RadComboBoxItem Text="In-Active" Value="False" />
                                            </Items>
                                        </telerik:RadComboBox>
                                        <telerik:RadScriptBlock ID="RadScriptBlock_is_active" runat="server">
                                            <script type="text/javascript">
                                                function ActiveIndexChanged(sender, args) {
                                                    var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                    if (args.get_item().get_value() == "") {
                                                        tableView.filter("is_active", args.get_item().get_value(), "NoFilter");
                                                    }
                                                    else {
                                                        tableView.filter("is_active", args.get_item().get_value(), "EqualTo");
                                                    }
                                                }
                                            </script>
                                        </telerik:RadScriptBlock>
                                    </FilterTemplate>
                                    <ItemTemplate>
                                        <img src='<%#IIf(Eval("is_active"),"/Images/true.gif","/Images/false.gif") %>' style="border: none;"
                                        alt="" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false"
                                    ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <a href="javascript:void(0);" onclick="redirectIframe('','','/master/header_details.aspx?i=<%# Eval("header_id")%> & t=h   ')">
                                                  <img src="/images/edit.png" style="border: none;" alt="Edit" /></a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings AllowDragToGroup="false">
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                        <GroupingSettings ShowUnGroupButton="false" />
                    </telerik:RadGrid>
                
                <%-- <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
        ProviderName="System.Data.SqlClient" SelectCommand="select [seller_id],[company_name] ,(contact_first_name+' '+contact_last_name) as contact_name,email ,name as state,is_active from [tbl_reg_sellers] inner join tbl_master_states on tbl_master_states.state_id=tbl_reg_sellers.state_id">
        
    </asp:SqlDataSource>--%>
            </td>
        </tr>
    </table>
</asp:Content>


