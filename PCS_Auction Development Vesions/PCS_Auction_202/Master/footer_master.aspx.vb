﻿Imports Telerik.Web.UI
Partial Class Master_footer_master
    Inherits System.Web.UI.Page
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        SetPermission()
    End Sub
    Private Sub SetPermission()
        If CommonCode.is_super_admin() Then
            div_new_auction.Visible = True
            RadGrid_Email_Footer.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
            RadGrid_Email_Footer.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
            RadGrid_Email_Footer.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
        Else
            div_new_auction.Visible = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 7
                        div_new_auction.Visible = True
                    Case 35
                        RadGrid_Email_Footer.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
                        RadGrid_Email_Footer.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
                        RadGrid_Email_Footer.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
                End Select
            Next
            dt = Nothing

        End If

    End Sub

    Protected Sub RadGrid_Email_Footer_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid_Email_Footer.NeedDataSource
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            Dim strQuery As String = ""
            strQuery = "SELECT A.footer_id, ISNULL(A.footer_name, '') AS footer_name, ISNULL(A.footer_html, '') AS footer_html, ISNULL(A.row_create_date, '1/1/1900') AS row_create_date, isnull(A.is_active,'False') as is_active FROM tbl_master_email_footers A"

            RadGrid_Email_Footer.DataSource = SqlHelper.ExecuteDatatable(strQuery)

        End If

    End Sub


    Protected Sub RadGrid_Email_Footer_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid_Email_Footer.PreRender

        Dim menu As GridFilterMenu = RadGrid_Email_Footer.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While

        If (Not Page.IsPostBack) Then
            RadGrid_Email_Footer.MasterTableView.FilterExpression = "([is_active] = True) "
            Dim column As GridColumn = RadGrid_Email_Footer.MasterTableView.GetColumnSafe("is_active")
            column.CurrentFilterFunction = GridKnownFunction.EqualTo
            column.CurrentFilterValue = "True"
            RadGrid_Email_Footer.MasterTableView.Rebind()
        End If
    End Sub
End Class
