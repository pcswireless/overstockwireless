﻿Imports Telerik.Web.UI
Partial Class Master_BucketFilterListing
    Inherits System.Web.UI.Page
    Protected Sub RadGrid1_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid1.NeedDataSource
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            Dim strQuery As String = ""
            strQuery = "SELECT A.bucket_filter_id, ISNULL(A.caption, '') AS caption, isnull(A.is_active,'False') as is_active FROM tbl_bucket_filter A"

            RadGrid1.DataSource = SqlHelper.ExecuteDatatable(strQuery)

        End If

    End Sub
    Protected Sub RadGrid1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadGrid1.PreRender
        If (Not Page.IsPostBack) Then
            RadGrid1.MasterTableView.FilterExpression = "([is_active] = True) "
            Dim column As GridColumn = RadGrid1.MasterTableView.GetColumnSafe("is_active")
            column.CurrentFilterFunction = GridKnownFunction.EqualTo
            column.CurrentFilterValue = "True"
            RadGrid1.MasterTableView.Rebind()
        End If
    End Sub
End Class
