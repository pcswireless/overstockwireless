﻿Imports System.IO
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Partial Class Master_footer_details
    Inherits System.Web.UI.Page
    Private New_Footer As Boolean = False
    Private View_Footer As Boolean = False
    Private Edit_Footer As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Request.QueryString.Get("e") = "1" Then
                lbl_error.Text = "New Footer created successfully"
            End If

            If IsNumeric(Request.QueryString.Get("i")) Then
                hd_seller_id.Value = Request.QueryString.Get("i")
                BasicTabEdit(True)

            Else

                hd_seller_id.Value = 0
                BasicTabEdit(False)


            End If


        End If
        If IsNumeric(Request.QueryString.Get("i")) Then
            Me.UC_System_log_link.Unique_ID = Request.QueryString("i")
            Me.UC_System_log_link.Module_Name = "Email-Footer"
        End If

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub

    Private Sub SetPermission()
        If Not CommonCode.is_super_admin() Then
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 9
                        New_Footer = True
                    Case 10
                        View_Footer = True
                    Case 11
                        Edit_Footer = True
                End Select
            Next
            dt = Nothing
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                If View_Footer = False Then Response.Redirect("/NoPermission.aspx")
                If Not Edit_Footer Then
                    'Set basic info tab 
                    Panel1_Content1button_per.Visible = False
                    'Set profile tab

                End If

            Else
                If New_Footer = False Then Response.Redirect("/NoPermission.aspx")
            End If
        Else
            New_Footer = True
            View_Footer = True
            Edit_Footer = True

        End If

    End Sub

#Region "Basic_Tab"


    Private Sub BasicTabEdit(ByVal readMode As Boolean)

        If hd_seller_id.Value > 0 Then
            fillBasicTab(hd_seller_id.Value)
        Else
            EmptyBasicData()

        End If
        visibleBasicData(readMode)
    End Sub
    Private Sub fillBasicTab(ByVal footer_id As Integer)
        Dim qry As String = "SELECT A.footer_id as footer_id,ISNULL(A.footer_name, '') AS footer_name, ISNULL(A.footer_html, '') AS footer_html, ISNULL(A.is_active, 0) AS is_active FROM tbl_master_email_footers A where A.footer_id=" & footer_id & ""
        Dim dt As DataTable = New DataTable()
        dt = SqlHelper.ExecuteDatatable(qry)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                page_heading.Text = CommonCode.decodeSingleQuote(.Item("footer_name"))
                'txt_basic_contact.Text = CommonCode.decodeSingleQuote(.Item("contact_title"))

                'txt_basic_confirm_email.Text = .Item("email")

                txt_basic_footer_1.Text = CommonCode.decodeSingleQuote(.Item("footer_html"))

                txt_basic_footer_name.Text = CommonCode.decodeSingleQuote(.Item("footer_name"))


                lbl_basic_footer_1.Text = HttpUtility.HtmlEncode(.Item("footer_html"))

                lbl_basic_footer_name.Text = CommonCode.decodeSingleQuote(.Item("footer_name"))

                chk_basic_active.Checked = .Item("is_active")

                If .Item("is_active") Then
                    lbl_basic_active.Text = "<img src='/Images/true.gif' alt=''>"
                Else
                    lbl_basic_active.Text = "<img src='/Images/false.gif' alt=''>"
                End If


            End With
        Else
            EmptyBasicData()
            ' RadWindowManager1.RadAlert("No records to display.", 280, 100, "Data Alert", "")
        End If
        dt = Nothing

    End Sub

    Private Sub EmptyBasicData()
        txt_basic_footer_1.Text = ""
        txt_basic_footer_name.Text = ""
        lbl_basic_footer_1.Text = ""
        lbl_error.Text = ""
        lbl_basic_footer_name.Text = ""
        chk_basic_active.Checked = True
        lbl_basic_active.Text = ""

    End Sub

    Private Sub visibleBasicData(ByVal readMode As Boolean)

        If readMode Then
            but_basic_edit.Visible = True
            but_preview.Visible = True
            but_basic_save.Visible = False
            but_basic_update.Visible = False
            div_basic_cancel.Visible = False
        Else
            but_basic_edit.Visible = False
            but_preview.Visible = False
            If hd_seller_id.Value = 0 Then
                but_basic_save.Visible = True
                but_basic_update.Visible = False
                div_basic_cancel.Visible = False
            Else
                but_basic_save.Visible = False
                but_basic_update.Visible = True
                ' but_basic_cancel.Visible = True
                div_basic_cancel.Visible = True
            End If
        End If



        'txt_basic_confirm_email.Visible = Not readMode

        txt_basic_footer_1.Visible = Not readMode
        span_basic_footer_name.Visible = Not readMode
        span_basic_footer_1.Visible = Not readMode
        'lbl_error.Visible = Not readMode
        txt_basic_footer_name.Visible = Not readMode
        chk_basic_active.Visible = Not readMode
        'lbl_confirm_email_basic.Visible = readMode
        lbl_basic_footer_1.Visible = readMode
        lbl_basic_footer_name.Visible = readMode
        lbl_basic_active.Visible = readMode

    End Sub

    Protected Sub btn_basic_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_edit.Click
        visibleBasicData(False)
    End Sub
    Protected Sub but_preview_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles but_preview.Click
        'Dim t As String = ""
        'Response.Redirect(String.Format("email_preview.aspx?i=" & hd_seller_id.Value & "&t=h"))


        ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "newWindow", "Opendetail('../../master/email_preview.aspx?i=" & hd_seller_id.Value & "&t=f','_blank','status=1,toolbar=0,menubar=0,location=1,scrollbars=1,resizable=1,width=600,height=10');", True)
    End Sub

    Protected Sub btn_basic_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_cancel.Click
        visibleBasicData(True)
    End Sub
    Public Function check_footer() As Boolean
        Dim str As String = ""
        Dim is_exists As Boolean = False
        Dim i As Integer = 0
        If hd_seller_id.Value > 0 Then
            str = "if exists(select footer_id from tbl_master_email_footers where footer_name='" & CommonCode.encodeSingleQuote(txt_basic_footer_name.Text.Trim()) & "' and footer_id<>" & hd_seller_id.Value & ") select 1 else select 0"
            ' lbl_error.Text = str
            ' Response.End()
        Else
            str = "if exists(select footer_id from tbl_master_email_footers where footer_name='" & CommonCode.encodeSingleQuote(txt_basic_footer_name.Text.Trim()) & "')select 1 else select 0 "

        End If
        i = SqlHelper.ExecuteScalar(str)
        If i = 1 Then
            is_exists = True
        Else
            is_exists = False
        End If
        Return is_exists
    End Function
    Protected Sub btn_basic_save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_save.Click
        If Page.IsValid Then

            If check_footer() Then
                lbl_error.Text = "Footer Name already exists."
                Exit Sub
            End If

            Dim obj As New CommonCode

            Dim insParameter As String = "footer_name"
            Dim insValue As String = "'" & CommonCode.encodeSingleQuote(txt_basic_footer_name.Text.Trim()) & "'"

            If txt_basic_footer_1.Text.Trim() <> "" Then
                insParameter = insParameter & ",footer_html"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_footer_1.Text.Trim()) & "'"
            End If
            'If txt_basic_first_name.Text.Trim() <> "" Then
            '    insParameter = insParameter & ",header_name"
            '    insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_first_name.Text.Trim()) & "'"
            'End If

            insParameter = insParameter & ",is_active"
            insValue = insValue & ",'" & IIf(chk_basic_active.Checked, 1, 0) & "'"

            insParameter = insParameter & ",row_create_date"
            Dim qry As String = "INSERT INTO tbl_master_email_footers (" & insParameter & ") values(" & insValue & ",'" & Now & "')  select scope_identity()"
            '  Response.Write(SqlHelper.ExecuteScalar(qry))

            Dim comp_id As Int32 = SqlHelper.ExecuteScalar(qry)

            If comp_id > 0 Then
                ' lbl_error.Text = "New Header Created."
                CommonCode.insert_system_log("New footer created.", "btn_basic_save_Click", comp_id, "", "Email-Footer")
                'hd_seller_id.Value = comp_id
                'BasicTabEdit(True)
                Response.Redirect("/master/footer_details.aspx?e=1&i=" & comp_id)
            End If
        End If


    End Sub



    Protected Sub btn_basic_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_update.Click

        ' Dim radalertscript As String = "<script language='javascript'>function f(){callConfirm(); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>"
        ' Page.ClientScript.RegisterStartupScript(Me.[GetType](), "radalert", radalertscript)


        lbl_error.Text = ""
        If hd_seller_id.Value > 0 And Page.IsValid Then
            If check_footer() Then
                lbl_error.Text = "Footer already exists."
                Exit Sub
                'ElseIf hid_seller_active_modify.Value = "0" Then
                'If chk_basic_active.Checked = False Then
                '    lbl_error.Text = "Header can not be deactivated because its assign to one or more locations."
                '    Exit Sub
                'End If
            End If
            Dim is_update_required As Boolean = False
            Dim updpara As String = ""


            If lbl_basic_footer_name.Text.Trim() <> txt_basic_footer_name.Text Then
                If is_update_required Then
                    updpara = updpara & ",footer_name='" & CommonCode.encodeSingleQuote(txt_basic_footer_name.Text.Trim()) & "'"
                Else
                    updpara = updpara & "footer_name='" & CommonCode.encodeSingleQuote(txt_basic_footer_name.Text.Trim()) & "'"
                End If

                is_update_required = True
            End If



            If lbl_basic_footer_1.Text.Trim() <> txt_basic_footer_1.Text.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") Then
                If is_update_required Then
                    updpara = updpara & ",footer_html='" & CommonCode.encodeSingleQuote(txt_basic_footer_1.Text.Trim()).Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'"
                Else
                    updpara = updpara & "footer_html='" & CommonCode.encodeSingleQuote(txt_basic_footer_1.Text.Trim()).Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'"
                End If

                is_update_required = True
            End If


            If is_update_required Then
                updpara = updpara & ",is_active='" & IIf(chk_basic_active.Checked, 1, 0) & "'"
            Else
                updpara = updpara & "is_active='" & IIf(chk_basic_active.Checked, 1, 0) & "'"
                is_update_required = True
            End If

            If is_update_required Then
                updpara = updpara & ",row_create_date='" & Now & "'"
            Else
                updpara = updpara & "row_create_date='" & Now & "'"
                is_update_required = True
            End If



            If is_update_required Then
                SqlHelper.ExecuteNonQuery("update tbl_master_email_footers set " & updpara & " where footer_id=" & hd_seller_id.Value)
                lbl_error.Text = "Footer Email Updated."
                CommonCode.insert_system_log("Footer Email basic information updated", "btn_basic_update_Click", Request.QueryString.Get("i"), "", "Email-Footer")
            End If

            BasicTabEdit(True)

        End If

    End Sub



#End Region
End Class
