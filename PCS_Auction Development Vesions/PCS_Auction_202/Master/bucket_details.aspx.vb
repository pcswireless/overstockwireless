﻿Imports System.IO
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Partial Class Master_bucket_details
    Inherits System.Web.UI.Page
    Private New_Bucket As Boolean = False
    Private View_Bucket As Boolean = False
    Private Edit_Bucket As Boolean = False
    Public m_iBucketId As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Request.QueryString.Get("e") = "1" Then
                lbl_error.Text = "New Bucket created successfully"
            End If
            If IsNumeric(Request.QueryString.Get("i")) Then
                hd_bucket_id.Value = Request.QueryString.Get("i")
                m_iBucketId = Request.QueryString.Get("i")
                BasicTabEdit(True)
                'Panel3_Header.Visible = True
                  Panel2_Header.Visible = True
            Else

                hd_bucket_id.Value = 0
                BasicTabEdit(False)


            End If


        End If
        If IsNumeric(Request.QueryString.Get("i")) Then
            Me.UC_System_log_link.Unique_ID = Request.QueryString("i")
            Me.UC_System_log_link.Module_Name = "Bucket"
        End If

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub

    Private Sub SetPermission()
        If Not CommonCode.is_super_admin() Then
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 9
                        New_Bucket = True
                    Case 10
                        View_Bucket = True
                    Case 11
                        Edit_Bucket = True
                End Select
            Next
            dt = Nothing
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                If View_Bucket = False Then Response.Redirect("/NoPermission.aspx")
                If Not Edit_Bucket Then
                    'Set basic info tab 
                    Panel1_Content1button_per.Visible = False
                    'Set profile tab

                End If

            Else
                If New_Bucket = False Then Response.Redirect("/NoPermission.aspx")
            End If
        Else
            New_Bucket = True
            View_Bucket = True
            Edit_Bucket = True

        End If

    End Sub

    Private Sub BasicTabEdit(ByVal readMode As Boolean)

        If hd_bucket_id.Value > 0 Then
            fillBasicTab(hd_bucket_id.Value)
        Else
            EmptyBasicData()

        End If
        visibleBasicData(readMode)
    End Sub

    Private Sub fillBasicTab(ByVal bucket_id As Integer)
        Dim qry As String = "SELECT A.bucket_id as bucket_id,ISNULL(A.bucket_name, '') AS bucket_name, ISNULL(A.description, '') AS description, ISNULL(A.sno, 0) AS sno, ISNULL(A.is_active, 0) AS is_active, ISNULL(A.show_in_registration, 0) AS show_in_registration FROM tbl_master_buckets A where A.bucket_id=" & bucket_id & ""
        Dim dt As DataTable = New DataTable()
        dt = SqlHelper.ExecuteDatatable(qry)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                page_heading.Text = "Edit Bucket: " & CommonCode.decodeSingleQuote(.Item("bucket_name"))
                'txt_basic_contact.Text = CommonCode.decodeSingleQuote(.Item("contact_title"))

                'txt_basic_confirm_email.Text = .Item("email")

                txt_basic_bucket_1.Text = CommonCode.decodeSingleQuote(.Item("description")).Replace("<br/>", Char.ConvertFromUtf32(10)).Replace("<br/>", Char.ConvertFromUtf32(13))

                txt_basic_bucket_name.Text = CommonCode.decodeSingleQuote(.Item("bucket_name"))
                txt_bckt_sno.Text = (.Item("sno"))


                lbl_basic_bucket_1.Text = .Item("description")

                lbl_basic_bucket_name.Text = CommonCode.decodeSingleQuote(.Item("bucket_name"))
                lbl_bckt_sno.Text = (.Item("sno"))

                chk_basic_active.Checked = .Item("is_active")

                chkShowInRegistration.Checked = .Item("show_in_registration")

                If .Item("is_active") Then
                    lbl_basic_active.Text = "<img src='/Images/true.gif' alt=''>"
                Else
                    lbl_basic_active.Text = "<img src='/Images/false.gif' alt=''>"
                End If

                If .Item("show_in_registration") Then
                    lblShowInRegistration.Text = "<img src='/Images/true.gif' alt=''>"
                Else
                    lblShowInRegistration.Text = "<img src='/Images/false.gif' alt=''>"
                End If


            End With
            'a_bid_assign.Attributes.Add("onclick", "return open_buyer_assign('" & hd_bucket_id.Value & "')")
        Else
            EmptyBasicData()
            ' RadWindowManager1.RadAlert("No records to display.", 280, 100, "Data Alert", "")
        End If
        dt = Nothing

    End Sub

    Private Sub EmptyBasicData()
        txt_basic_bucket_1.Text = ""
        txt_bckt_sno.Text = ""
        txt_basic_bucket_name.Text = ""
        lbl_basic_bucket_1.Text = ""
        lbl_bckt_sno.Text = ""
        lbl_error.Text = ""
        lbl_basic_bucket_name.Text = ""
        chk_basic_active.Checked = True
        chkShowInRegistration.Checked = True
        lblShowInRegistration.Text = ""
        lbl_basic_active.Text = ""

    End Sub

    Private Sub visibleBasicData(ByVal readMode As Boolean)

        If readMode Then
            but_basic_edit.Visible = True
            'but_preview.Visible = True
            but_basic_save.Visible = False
            but_basic_update.Visible = False
            div_basic_cancel.Visible = False
        Else
            but_basic_edit.Visible = False
            'but_preview.Visible = False
            If hd_bucket_id.Value = 0 Then
                but_basic_save.Visible = True
                but_basic_update.Visible = False
                div_basic_cancel.Visible = False
            Else
                but_basic_save.Visible = False
                but_basic_update.Visible = True
                ' but_basic_cancel.Visible = True
                div_basic_cancel.Visible = True
            End If
        End If


        'txt_basic_confirm_email.Visible = Not readMode

        txt_basic_bucket_1.Visible = Not readMode
        txt_bckt_sno.Visible = Not readMode
        span_basic_bucket_name.Visible = Not readMode
        span_basic_bucket_1.Visible = Not readMode
        'lbl_error.Visible = Not readMode
        span1.Visible = Not readMode
        txt_basic_bucket_name.Visible = Not readMode
        chk_basic_active.Visible = Not readMode
        chkShowInRegistration.Visible = Not readMode
        'lbl_confirm_email_basic.Visible = readMode
        lbl_basic_bucket_1.Visible = readMode
        lbl_basic_bucket_name.Visible = readMode
        lbl_bckt_sno.Visible = readMode
        lbl_basic_active.Visible = readMode
        lblShowInRegistration.Visible = readMode

    End Sub

    Protected Sub btn_basic_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_edit.Click
        visibleBasicData(False)
    End Sub

    'Protected Sub but_preview_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles but_preview.Click
    '    'Dim t As String = ""
    '    'Response.Redirect(String.Format("email_preview.aspx?i=" & hd_bucket_id.Value & "&t=h"))


    '    ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "newWindow", "Opendetail('../../master/email_preview.aspx?i=" & hd_bucket_id.Value & "&t=b','_blank','status=1,toolbar=0,menubar=0,location=1,scrollbars=1,resizable=1,width=600,height=10');", True)
    'End Sub

    Protected Sub btn_basic_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_cancel.Click
        visibleBasicData(True)
    End Sub

    Public Function check_bucket() As Boolean
        Dim str As String = ""
        Dim is_exists As Boolean = False
        Dim i As Integer = 0
        If hd_bucket_id.Value > 0 Then
            str = "if exists(select bucket_id from tbl_master_buckets where bucket_name='" & CommonCode.encodeSingleQuote(txt_basic_bucket_name.Text.Trim()) & "' and bucket_id<>" & hd_bucket_id.Value & ") select 1 else select 0"
            ' lbl_error.Text = str
            ' Response.End()
        Else
            str = "if exists(select bucket_id from tbl_master_buckets where bucket_name='" & CommonCode.encodeSingleQuote(txt_basic_bucket_name.Text.Trim()) & "')select 1 else select 0 "

        End If
        i = SqlHelper.ExecuteScalar(str)
        If i = 1 Then
            is_exists = True
        Else
            is_exists = False
        End If
        Return is_exists
    End Function

    Protected Sub btn_basic_save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_save.Click
        If Page.IsValid Then

            If check_bucket() Then
                lbl_error.Text = "Bucket Name already exists."
                Exit Sub
            End If

            Dim obj As New CommonCode

            Dim insParameter As String = "bucket_name"
            Dim insValue As String = "'" & CommonCode.encodeSingleQuote(txt_basic_bucket_name.Text.Trim()) & "'"

            If txt_basic_bucket_1.Text.Trim() <> "" Then
                insParameter = insParameter & ",description"
                insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_bucket_1.Text.Trim()) & "'"
                'insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_bucket_1.Text.Trim()).Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'" 'CommonCode.encodeSingleQuote(txt_basic_bucket_1.Text.Trim()) & "'"

            End If
            'If txt_basic_first_name.Text.Trim() <> "" Then
            '    insParameter = insParameter & ",header_name"
            '    insValue = insValue & ",'" & CommonCode.encodeSingleQuote(txt_basic_first_name.Text.Trim()) & "'"
            'End If

            If txt_bckt_sno.Text.Trim() <> "" Then
                insParameter = insParameter & ",sno"
                insValue = insValue & ",'" & (txt_bckt_sno.Text.Trim()) & "'"
            End If

            insParameter = insParameter & ",is_active"
            insValue = insValue & ",'" & IIf(chk_basic_active.Checked, 1, 0) & "'"

            insParameter = insParameter & ",show_in_registration"
            insValue = insValue & ",'" & IIf(chkShowInRegistration.Checked, 1, 0) & "'"

            Dim qry As String = "INSERT INTO tbl_master_buckets (" & insParameter & ") values(" & insValue & ")  select scope_identity()"
            '  Response.Write(SqlHelper.ExecuteScalar(qry))

            Dim comp_id As Int32 = SqlHelper.ExecuteScalar(qry)

            If comp_id > 0 Then
                ' lbl_error.Text = "New Header Created."
                CommonCode.insert_system_log("New bucket created.", "btn_basic_save_Click", comp_id, "", "Bucket")
                'hd_bucket_id.Value = comp_id
                'BasicTabEdit(True)
                Response.Redirect("/master/bucket_details.aspx?e=1&i=" & comp_id)
            End If
        End If


    End Sub

    Protected Sub btn_basic_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_update.Click

        ' Dim radalertscript As String = "<script language='javascript'>function f(){callConfirm(); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>"
        ' Page.ClientScript.RegisterStartupScript(Me.[GetType](), "radalert", radalertscript)


        lbl_error.Text = ""
        If hd_bucket_id.Value > 0 And Page.IsValid Then
            If check_bucket() Then
                lbl_error.Text = "Bucket already exists."
                Exit Sub
                'ElseIf hid_seller_active_modify.Value = "0" Then
                'If chk_basic_active.Checked = False Then
                '    lbl_error.Text = "Header can not be deactivated because its assign to one or more locations."
                '    Exit Sub
                'End If
            End If
            Dim is_update_required As Boolean = False
            Dim updpara As String = ""


            If lbl_basic_bucket_name.Text.Trim() <> txt_basic_bucket_name.Text Then
                If is_update_required Then
                    updpara = updpara & ",bucket_name='" & CommonCode.encodeSingleQuote(txt_basic_bucket_name.Text.Trim()) & "'"
                Else
                    updpara = updpara & "bucket_name='" & CommonCode.encodeSingleQuote(txt_basic_bucket_name.Text.Trim()) & "'"
                End If

                is_update_required = True
            End If



            If lbl_basic_bucket_1.Text.Trim() <> txt_basic_bucket_1.Text.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") Then
                If is_update_required Then
                    updpara = updpara & ",description='" & CommonCode.encodeSingleQuote(txt_basic_bucket_1.Text.Trim()).Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'"
                Else
                    updpara = updpara & "description='" & CommonCode.encodeSingleQuote(txt_basic_bucket_1.Text.Trim()).Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'"
                End If

                is_update_required = True
            End If

            If lbl_bckt_sno.Text.Trim() <> txt_bckt_sno.Text Then
                If is_update_required Then
                    updpara = updpara & ",sno='" & txt_bckt_sno.Text.Trim() & "'"
                Else
                    updpara = updpara & "sno='" & txt_bckt_sno.Text.Trim() & "'"
                End If

                is_update_required = True
            End If


            If is_update_required Then
                updpara = updpara & ",is_active='" & IIf(chk_basic_active.Checked, 1, 0) & "'"
            Else
                updpara = updpara & "is_active='" & IIf(chk_basic_active.Checked, 1, 0) & "'"
                is_update_required = True
            End If

            If is_update_required Then
                updpara = updpara & ",show_in_registration='" & IIf(chkShowInRegistration.Checked, 1, 0) & "'"
            Else
                updpara = updpara & "show_in_registration='" & IIf(chkShowInRegistration.Checked, 1, 0) & "'"
                is_update_required = True
            End If




            If is_update_required Then
                SqlHelper.ExecuteNonQuery("update tbl_master_buckets set " & updpara & " where bucket_id=" & hd_bucket_id.Value)
                'Response.write("update tbl_master_buckets set " & updpara & " where bucket_id=" & hd_bucket_id.Value)

                lbl_error.Text = "Bucket Updated Successfully."
                CommonCode.insert_system_log("Bucket  basic information updated", "btn_basic_update_Click", Request.QueryString.Get("i"), "", "Bucket")
            End If

            BasicTabEdit(True)

        End If

    End Sub

    
    Protected Sub but_bind_user_bidder_Click(sender As Object, e As System.EventArgs) Handles but_bind_user_bidder.Click
        'RadGrid_Bidder.Rebind()
        'If RadGrid_Bidder.PageCount > 0 Then
        '    RadGrid_Bidder.CurrentPageIndex = 0
        'End If
    End Sub

    Protected Sub RadGrid_Bucket_Items_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles RadGrid_Bucket_Items.ItemCommand
        If (e.CommandName = Telerik.Web.UI.RadGrid.InitInsertCommandName) Then
            'cancel the default operation
            e.Canceled = True
            'Prepare an IDictionary with the predefined values
            Dim newValues As System.Collections.Specialized.ListDictionary = New System.Collections.Specialized.ListDictionary()
            'set default checked state for checkbox inside the EditItemTemplate
            newValues("is_active") = False
            'newValues("Is_Disabled") = False
            'Insert the item and rebind
            e.Item.OwnerTableView.InsertItem(newValues)
        End If
    End Sub

      Protected Sub RadGrid_Bucket_Items_PreRender(sender As Object, e As EventArgs) Handles RadGrid_Bucket_Items.PreRender
        If (Not Page.IsPostBack) Then
            RadGrid_Bucket_Items.MasterTableView.FilterExpression = "([is_active] = True) "
            Dim column As GridColumn = RadGrid_Bucket_Items.MasterTableView.GetColumnSafe("is_active")
            column.CurrentFilterFunction = GridKnownFunction.EqualTo
            column.CurrentFilterValue = "True"
            RadGrid_Bucket_Items.MasterTableView.Rebind()
        End If
      End Sub
End Class
