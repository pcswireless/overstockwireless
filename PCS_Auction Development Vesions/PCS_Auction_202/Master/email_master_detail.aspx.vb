﻿
Partial Class Master_email_master_detail
    Inherits System.Web.UI.Page
    'Key use in Email_Html

    'User Name -- <<user_name>> eg. user_first_name + user_last_name
    'Server URL -- <<site_url>> eg: http://eplanet1-pc:150
    'Company Name -- <<company_name>> eg. PCS
    'Company Website --<<company_website>> eg. website, company_website
    'Bidder Name -- <<bidder_name>> eg: contact_first_name
    'Bidder First Name -- <<bidder_first_name>> eg. contact_first_name
    'Bidder Last Name -- <<bidder_last_name>> eg. contact_last_name
    'Bidder Title -- <<bidder_title>> eg. contact_title
    'Bidder Address1 --<<bidder_address1>> eg. address1
    'Bidder Address2 --<<bidder_address2>> eg. address2
    'Bidder City --<<bidder_city>>  eg. city
    'Bidder State --<<bidder_state>>    eg. state_text
    'Bidder Country --<<bidder_country>> eg. country
    'Bidder Zip --<<bidder_zip>>    eg. zip
    'Bidder Phone1 --<<bidder_mobile>>  eg. mobile
    'Bidder Phone2 --<<bidder_phone>>   eg. phone
    'Bidder Fax --<<bidder_fax>>    eg. fax
    'Bidder Comment --<<bidder_comment>>    eg. comment
    'Bidder Status --<<bidder_status>>  eg. status
    'Bidder Business Type --<<bidder_business_type>>    eg. business_type
    'Bidder Industry Type --<<bidder_industry_type>>    eg. industry_type
    'Bidder Find Us --<<bidder_find_us>>    eg. Friend, Website
    'Bidder Email --<<bidder_email>>    eg. email
    'Bidding Date -- <<bid_date>>   eg. bid_date
    'Bidding Start Price -- <<bidding_start_price>> eg. start_price
    'Buyer ID -- <<bidder_id>> eg. buyer_id
    'Employee Name -- <<employee_name>> eg. first_name
    'Max Bid Amount -- <<max_bid_amount>>   eg. max_bid_amount
    'Bid Amount -- <<bid_amount>>   eg. bidamount
    'Password -- <<password>>   eg. password
    'Auction Title -- <<auction_title>> eg. auction_title
    'Auction Name -- <<auction_name>>    eg. auction_name
    'Auction Code -- <<auction_code>>    eg. auction_code
    'Auction Start Date -- <<auction_start_date>>   eg. start_date 
    'Auction Start Time -- <<auction_start_time>>   eg. start_date converted to ToShortTimeString
    'Auction End Time -- <<auction_end_time>>   eg. display_end_time converted to ToShortTimeString
    'Auction End Date -- <<auction_end_date>>   eg. display_end_time
    'Auction Status -- <<auction_status>>   eg. auction_status
    'Auction Price -- <<auction_price>> eg. price
    'Auction Quantity -- <<auction_quantity>>   eg. quantity
    'Buy Type -- <<buy_type>>   eg.Buy it Now  
    'Customer Question -- <<customer_question>> eg. Dynamic Subject, _key, question
    'Question Title/Message  --<<message_title>> eg. title, message
    'Query/Ask Message -- <<query_message>> eg. message
    'Dynamic/Reply Message -- <<reply_message>> eg.msg
    'Query Id -- <<query_code>> eg.Dynamic query_id
    'Customer Subject -- <<customer_subject>> eg. subject
    'Order Type --<<order_type>>    eg. buy now order/private order/partial order/bidding
    'Order Type Receipt -- <<order_type_receipt>>    eg. private order/partial order
    'Winning Bid Amount -- <<win_bid_amount>>   eg. bid_amount

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            fillDropdown()
            If Request.QueryString.Get("e") = "1" Then
                lbl_msg.Text = "New Email created successfully"
            Else
                lbl_msg.Text = ""
            End If

            If Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
                hid_master_email_id.Value = Request.QueryString.Get("i")
                filldata()
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset("Select footer_html from tbl_master_email_footers where footer_id = " & hid_foot.Value)
                If ds.Tables(0).Rows.Count > 0 Then
                    With ds.Tables(0).Rows(0)
                        ' lbl_prvw_footer.Text = .Item("footer_html")
                    End With
                End If
                ' but_preview.Attributes.Add("onclick", "return Opendetail('/master/email_preview.aspx?i=" & hid_master_email_id.Value & "&t=e');")
                visibleData(True)
            Else
                hid_master_email_id.Value = 0
                Clear()
                visibleData(False)
            End If
        Else
            'hid_head.Value = rdolist_header.SelectedValue
            'hid_foot.Value = rdolist_footer.SelectedValue


            '   but_preview.Attributes.Add("onclick", "return Opendetail('/master/email_preview.aspx?i=" & hid_master_email_id.Value & "&h=" & hid_head.Value & "&f=" & hid_foot.Value & "');")
        End If

    End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Private Sub setPermission()

        If Not CommonCode.is_super_admin() Then

            Dim Vew_User As Boolean = False
            Dim Edit_User As Boolean = False
            Dim New_User As Boolean = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 14
                        New_User = True
                    Case 15
                        Vew_User = True
                    Case 16
                        Edit_User = True
                End Select
            Next
            dt = Nothing
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                If Vew_User = False Then Response.Redirect("/NoPermission.aspx")
                If Not Edit_User Then
                    Panel1_Content1button_per.Visible = False
                End If
            Else
                If New_User = False Then Response.Redirect("/NoPermission.aspx")
            End If
        End If
    End Sub
    Private Sub fillDropdown()

        rdolist_header.DataSource = SqlHelper.ExecuteDatatable("select header_id,isnull(header_name,'') as header_name from tbl_master_email_headers where ISNULL(is_active,0)=1 order by header_name")
        rdolist_header.DataTextField = "header_name"
        rdolist_header.DataValueField = "header_id"
        rdolist_header.DataBind()

        rdolist_footer.DataSource = SqlHelper.ExecuteDatatable("select footer_id,isnull(footer_name,'') as footer_name from tbl_master_email_footers where ISNULL(is_active,0)=1 order by footer_name")
        rdolist_footer.DataTextField = "footer_name"
        rdolist_footer.DataValueField = "footer_id"
        rdolist_footer.DataBind()



    End Sub

    Private Sub visibleData(ByVal readMode As Boolean)


        ' txt_email.Visible = Not readMode
        txt_email_html.Visible = Not readMode
        ' lbl_prvw_footer.Visible = Not readMode
        txt_email_subject.Visible = Not readMode
        rdolist_footer.Visible = Not readMode
        rdolist_header.Visible = Not readMode


        'span1.Visible = Not readMode
        span2.Visible = Not readMode
        span3.Visible = Not readMode
        span4.Visible = Not readMode
        span5.Visible = Not readMode
        'span6.Visible = Not readMode

        lbl_email.Visible = readMode
        'lbl_prvw_footer.Visible = readMode
        lbl_email_subject.Visible = readMode
        lbl_email_html.Visible = readMode
        lbl_header.Visible = readMode
        lbl_footer.Visible = readMode

        If readMode Then
            img_button_edit.Visible = True
            div_basic_cancel.Visible = False
            img_button_save.Visible = False
            img_button_update.Visible = False
            but_preview.Visible = True
            ' lbl_prvw_footer.Visible = False
        Else
            img_button_edit.Visible = False
            ' lbl_prvw_footer.Visible = True
            If String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
                img_button_save.Visible = True
            Else
                img_button_save.Visible = False
                img_button_update.Visible = True
                but_preview.Visible = False
                div_basic_cancel.Visible = True
            End If
        End If
        txt_email.Visible = False
        lbl_email.Visible = True
    End Sub
    Private Sub filldata()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset("select a.email_name,a.email_code,a.email_subject,a.email_html,a.header_id,a.email_field_description,a.footer_id,b.header_name,c.footer_name,b.header_html,c.footer_html from tbl_master_emails a join tbl_master_email_headers b on a.header_id=b.header_id inner join tbl_master_email_footers c on a.footer_id=c.footer_id where a.master_email_id=" & hid_master_email_id.Value)
        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)
                page_heading.Text = .Item("email_name")
                txt_email.Text = .Item("email_name")
                txt_email_html.Text = .Item("email_html")
                txt_email_subject.Text = .Item("email_subject")
                rdolist_footer.SelectedValue = .Item("footer_id")
                rdolist_header.SelectedValue = .Item("header_id")
                DIV_IMG_Default_Logo3_Img.Attributes.Add("onmouseover", "tip_new('" & .Item("email_field_description") & "','250','white');")
                lbl_email.Text = .Item("email_name")
                lbl_email_code.Text = .Item("email_code")
                lbl_email_subject.Text = .Item("email_subject")
                lbl_email_html.Text = HttpUtility.HtmlEncode(.Item("email_html"))
                lbl_header.Text = .Item("header_name")
                lbl_footer.Text = .Item("footer_name")
                hid_head.Value = .Item("header_id")
                hid_foot.Value = .Item("footer_id")
            End With
        End If

    End Sub
    
    Protected Sub Clear()
        txt_email.Text = ""
        txt_email_html.Text = ""
        txt_email_subject.Text = ""
        
        rdolist_footer.SelectedIndex = 0
        rdolist_header.SelectedIndex = 0

        lbl_email.Text = ""
        lbl_email_html.Text = ""
        lbl_email_subject.Text = ""
        lbl_header.Text = ""
        lbl_footer.Text = ""
        
    End Sub
    Protected Sub img_button_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_button_cancel.Click
        visibleData(True)
        rdolist_header.SelectedValue = hid_head.Value
        rdolist_footer.SelectedValue = hid_foot.Value
        'Dim ds As New DataSet
        'ds = SqlHelper.ExecuteDataset("Select footer_html from tbl_master_email_footers where footer_id = " & hid_foot.Value)
        'If ds.Tables(0).Rows.Count > 0 Then
        '    With ds.Tables(0).Rows(0)
        '        lbl_prvw_footer.Text = .Item("footer_html")
        '    End With
        'End If

    End Sub

    Protected Sub img_button_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_button_update.Click
        If Page.IsValid Then
            If SqlHelper.ExecuteDatatable("select master_email_id from tbl_master_emails where lower(email_name)='" & txt_email.Text.Trim().ToLower() & "' and master_email_id<>" & hid_master_email_id.Value).Rows.Count > 0 Then
                lbl_msg.Text = "Email Already Exists"
                Exit Sub
            End If
            Dim is_update_required As Boolean = False
            Dim updpara As String = ""

            If txt_email.Text.Trim() <> lbl_email.Text Then
                updpara = updpara & "email_name='" & CommonCode.encodeSingleQuote(txt_email.Text.Trim()) & "'"
                is_update_required = True
            End If
            If txt_email_subject.Text.Trim() <> lbl_email_subject.Text Then
                If is_update_required Then
                    updpara = updpara & ",email_subject='" & CommonCode.encodeSingleQuote(txt_email_subject.Text.Trim()) & "'"
                Else
                    updpara = updpara & "email_subject='" & CommonCode.encodeSingleQuote(txt_email_subject.Text.Trim()) & "'"
                End If

                is_update_required = True
            End If

            If txt_email_html.Text <> lbl_email_html.Text Then
                If is_update_required Then
                    updpara = updpara & ",email_html='" & CommonCode.encodeSingleQuote(txt_email_html.Text) & "'"
                Else
                    updpara = updpara & "email_html='" & CommonCode.encodeSingleQuote(txt_email_html.Text) & "'"
                End If
                is_update_required = True
            End If
            
            If is_update_required Then
                updpara = updpara & ",header_id=" & rdolist_header.SelectedValue
                is_update_required = True
            Else
                updpara = updpara & "header_id=" & rdolist_header.SelectedValue
                is_update_required = True
            End If


            If is_update_required Then
                updpara = updpara & ",footer_id=" & rdolist_footer.SelectedValue
                is_update_required = True
            Else
                updpara = updpara & "footer_id=" & rdolist_footer.SelectedValue
                is_update_required = True
            End If


            If is_update_required Then
                SqlHelper.ExecuteNonQuery("update tbl_master_emails set " & updpara & " where master_email_id=" & hid_master_email_id.Value)
            End If

            
            filldata()
            visibleData(True)
            lbl_msg.Text = "Email details Successfully Updated"
        End If
    End Sub

    Protected Sub img_button_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_button_edit.Click
        visibleData(False)

        '  but_preview.Attributes.Add("onclick", "return Opendetail('/master/email_preview.aspx?i=" & hid_master_email_id.Value & "&h=" & rdolist_header.SelectedValue & "&f=" & hid_foot.Value & "');")
    End Sub
    Protected Sub rdolist_footer_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rdolist_footer.SelectedIndexChanged
        '    Label1.Text = rdolist_footer.SelectedValue
        'hid_foot.Value = rdolist_footer.SelectedValue
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset("Select footer_html from tbl_master_email_footers where footer_id = " & rdolist_footer.SelectedValue)
        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)
                ' lbl_prvw_footer.Text = .Item("footer_html")
            End With
        End If
    End Sub
    Protected Sub rdolist_header_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rdolist_header.SelectedIndexChanged
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset("Select header_html from tbl_master_email_headers where header_id = " & rdolist_header.SelectedValue)
        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)
                '   lbl_prvw_header.Text = .Item("header_html")
            End With
        End If
    End Sub
End Class
