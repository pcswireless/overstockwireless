﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false" CodeFile="header_details.aspx.vb" Inherits="Master_header_details" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/Log/UserControls/SystemLogLink.ascx" TagName="SystemLogLink"
    TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <asp:HiddenField ID="hd_seller_id" runat="server" Value="0" />
    
    <telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
        <script type="text/javascript">
           
            function Cancel_Ajax(sender, args) {
                // alert(args.get_eventTarget().toString);
                 args.set_enableAjax(false);

             }

             function Opendetail(page_name) {

                 var width = "600";
                 var height = "600";

                 var left = (screen.width - width) / 2;
                 var top = (screen.height - height) / 2;

                 window.open(page_name, '_open_email_preview', 'left=' + left + ',top=' + top + ',width=' + width + ',height=' + height + ',status=1,scrollbars=1')

             }
        
        </script>
    </telerik:RadScriptBlock>
    <asp:HiddenField runat="server" ID="hid_popup_status" />
    <asp:Button ID="but_bind_user_bidder" runat="server" BorderStyle="None" BackColor="Transparent"
        ForeColor="Transparent" Width="0" Height="0" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" >
    <ClientEvents OnRequestStart="Cancel_Ajax" />
        <AjaxSettings>
          
            <telerik:AjaxSetting AjaxControlID="but_basic_edit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic_2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>

             <telerik:AjaxSetting AjaxControlID="but_preview">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic_2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="but_basic_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic_2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_basic_cancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic_2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_basic_save">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic_2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div style="float: left;">
                    <div class="pageheading">
                        <asp:Literal ID="page_heading" runat="server" Text="New Email Header"></asp:Literal></div>
                </div>
                <div style="float: right; padding-right: 20px; width: 180px;">
                    <div style="float: left;">
                        <uc4:SystemLogLink ID="UC_System_log_link" runat="server" />
                    </div>
                    <div style="float: left; padding-left: 10px;">
                        <a href="javascript:void(0);" onclick="javascript:open_help('3');">
                            <img src="/Images/help.gif" border="none" alt="" />
                        </a>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="PageTab">
                    <span>
                        <%= IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), "Add Header Details", "Edit Header")%></span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="PageTabContentEdit">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td class="tdTabItem">
                            <asp:Panel ID="Panel1_Header" runat="server" CssClass="pnlTabItemHeader">
                                <div class="tabItemHeader" style="background: url(/Images/basic_info.gif) no-repeat;
                                    background-position: 10px 0px;">
                                    Header Information
                                </div>
                              <%--  <asp:Image ID="Image1" runat="server" ImageAlign="left" ImageUrl="/Images/up_Arrow.gif"
                                    CssClass="panelimage" />--%>
                            </asp:Panel>
                           <%-- <ajax:CollapsiblePanelExtender ID="cpe1" BehaviorID="cpe1" runat="server" Enabled="True"
                                TargetControlID="Panel1_Content" CollapseControlID="Panel1_Header" ExpandControlID="Panel1_Header"
                                Collapsed="false" ImageControlID="Image1" CollapsedImage="/Images/down_Arrow.gif"
                                ExpandedImage="/Images/up_Arrow.gif">
                            </ajax:CollapsiblePanelExtender>--%>
                            <%--<ajax:CollapsiblePanelExtender ID="cpe11" BehaviorID="cpe1" runat="server" Enabled="True"
                                TargetControlID="Panel1_Content1button" CollapseControlID="Panel1_Header" ExpandControlID="Panel1_Header"
                                Collapsed="false" ImageControlID="Image1" CollapsedImage="/Images/down_Arrow.gif"
                                CollapsedSize="0" ExpandedImage="/Images/up_Arrow.gif">
                            </ajax:CollapsiblePanelExtender>--%>
                            <asp:Panel ID="Panel1_Content" runat="server" CssClass="collapsePanel">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel_Basic" runat="server" CssClass="TabGrid" LoadingPanelID="RadAjaxLoadingPanel1">
                                    <div style="padding-left: 5px;">
                                        <asp:Label ID="lbl_error" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                                        <asp:HiddenField ID="hid_seller_active_modify" runat="server" Value="1" />
                                    </div>
                                    <table cellpadding="0" cellspacing="2" border="0" width="838px">   
                                        <tr>
                                            <td class="caption" style="width: 145px;">
                                                Header Name&nbsp;<span id="span_basic_header_name" runat="server" class="req_star">*</span>
                                            </td>
                                            <td style="width: 500px;" class="details">
                                                <asp:TextBox ID="txt_basic_header_name"  Width="150" CssClass="inputtype" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Required_txt_basic_header_name" runat="server" Font-Size="10px"
                                                    ControlToValidate="txt_basic_header_name" ValidationGroup="_basic" Display="Dynamic"
                                                    ErrorMessage="<br>Header Name Required"></asp:RequiredFieldValidator>
                                                <asp:Label ID="lbl_basic_header_name" runat="server" />
                                            </td>

                                        </tr>
                                        
                                        <tr>
                                            <td class="caption">
                                                Header HTML&nbsp;<span id="span_basic_header_1" runat="server" class="req_star">*</span>
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_basic_header_1" TextMode="MultiLine" Height="150px" Width="550px" CssClass="inputtype"
                                                    runat="server"></asp:TextBox>
                                                <asp:Label ID="lbl_basic_header_1" runat="server" />
                                                <asp:RequiredFieldValidator ID="rfv_address1" runat="server" ControlToValidate="txt_basic_header_1"
                                                    ValidationGroup="_basic" Display="Dynamic" ErrorMessage="<br />Header HTML Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                            </td>                                           
                                            
                                        </tr>

                                        <tr>
                                        <td class="caption">
                                                Is Active
                                            </td>
                                            <td class="details">
                                                <asp:CheckBox ID="chk_basic_active" runat="server" />
                                                <asp:Label ID="lbl_basic_active" runat="server" />
                                            </td>         
                                        </tr>
                                 
                                    </table>
                                </telerik:RadAjaxPanel>
                            </asp:Panel>
                        </td>
                        <td class="fixedcolumn" valign="top">
                            <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                            <asp:Panel ID="Panel1_Content1button" runat="server" CssClass="collapsePanel">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel_Basic_2" runat="server">
                                    <asp:Panel ID="Panel1_Content1button_per" runat="server">
                                        <div class="addButton">
                                            <asp:ImageButton ID="but_basic_save" ValidationGroup="_basic" runat="server"  AlternateText="Save"
                                                ImageUrl="/images/save.gif" />
                                            <asp:ImageButton ID="but_basic_update" ValidationGroup="_basic" runat="server" AlternateText="Update"
                                                ImageUrl="/images/update.gif" />
                                            <asp:ImageButton ID="but_basic_edit" runat="server" AlternateText="Edit" ImageUrl="/images/edit.gif" />
                                           <br />
                                           <br />
                                            <asp:ImageButton ID="but_preview" runat="server"  AlternateText="Preview" ImageUrl="/images/preview.png" />
                                          </div>
                                        <div class="cancelButton" id="div_basic_cancel" runat="server">
                                            <asp:ImageButton ID="but_basic_cancel" CausesValidation="false" runat="server" AlternateText="Cancel"
                                                ImageUrl="/images/cancel.gif" /></div>
                                    </asp:Panel>
                                </telerik:RadAjaxPanel>
                            </asp:Panel>
                        </td>
                    </tr>
               
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

