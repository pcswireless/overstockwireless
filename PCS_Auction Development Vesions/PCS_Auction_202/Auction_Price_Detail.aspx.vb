﻿
Partial Class Auction_Price_Detail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            If CommonCode.Fetch_Cookie_Shared("is_backend") = "1" Then
                Response.Write("<script language='javascript'>top.location.href = '/Backend_Home.aspx?t=1';</script>")
            End If
        End If
        If Not IsPostBack Then
            If IsNumeric(Request.QueryString("i")) Then
                hid_auction_id.Value = Request.QueryString.Get("i")
                Dim strQuery As String = "SELECT A.auction_id,ISNULL(auction_type_id,0) AS auction_type_id from tbl_auctions A WITH (NOLOCK) where auction_id=" & hid_auction_id.Value
                Dim dt As DataTable = New DataTable
                dt = SqlHelper.ExecuteDataTable(strQuery)
                If dt.Rows.Count > 0 Then
                    hid_auction_type_id.Value = dt.Rows(0)("auction_type_id")
                End If
                set_form()

                If hid_auction_type_id.Value = "4" Then
                    pnl_detail.Visible = False
                Else
                    bind_bid_detail()
                End If

            End If
        End If
    End Sub
    Private Sub set_form()
        If hid_auction_type_id.Value = "1" Then
            pnl_buy_now.Visible = True
            pnl_trad_proxy.Visible = False
            tr_rank.Visible = False
        Else
            pnl_buy_now.Visible = False
            pnl_trad_proxy.Visible = True
            tr_rank.Visible = True
        End If
    End Sub
    Private Sub bind_bid_detail()
        Dim dt As DataTable = New DataTable()
        Dim strQuery As String = ""
        Dim auction_id As Integer = hid_auction_id.Value
        Dim is_show_actual_pricing_not_rank As Boolean = False
        Dim is_show_reserve_price As Boolean = False
        Dim is_show_bids_num As Boolean = False
        strQuery = "select A.auction_id,Convert(decimal(10,2),ISNULL(A.show_price,0)) As show_price,Convert(decimal(10,2),ISNULL(A.start_price,0)) As start_price,ISNULL(reserve_price,0) As reserve_price,ISNULL(increament_amount,0) AS increament_amount,isnull(is_show_reserve_price,0) AS is_show_reserve_price,ISNULL(is_show_no_of_bids,0) AS is_show_no_of_bids,ISNULL(is_show_actual_pricing,0) AS is_show_actual_pricing, "
        If CommonCode.Fetch_Cookie_Shared("buyer_id") = "" Then
            strQuery = strQuery & "0 AS current_rank,"
            strQuery = strQuery & "0 AS bidder_price"
        Else
            strQuery = strQuery & "dbo.buyer_bid_rank(A.auction_id," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ") AS current_rank,"
            strQuery = strQuery & "isnull((select max(bid_amount) from tbl_auction_bids WITH (NOLOCK) where auction_id=A.auction_id and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " ),0) AS bidder_price "
        End If
        strQuery = strQuery & " from tbl_auctions A WITH (NOLOCK) where A.auction_id=" & auction_id
        dt = SqlHelper.ExecuteDataTable(strQuery)
        If dt.Rows.Count > 0 Then
            is_show_actual_pricing_not_rank = CBool(dt.Rows(0)("is_show_actual_pricing"))
            is_show_reserve_price = CBool(dt.Rows(0)("is_show_reserve_price"))
            is_show_bids_num = CBool(dt.Rows(0)("is_show_no_of_bids"))
            lbl_list_price.Text = CommonCode.GetFormatedMoney(dt.Rows(0)("show_price"))
            lbl_start_price.Text = CommonCode.GetFormatedMoney(dt.Rows(0)("start_price"))
            If is_show_actual_pricing_not_rank And hid_auction_type_id.Value <> "1" Then
                trStartPrice.Visible = True
            Else
                trStartPrice.Visible = False
            End If
            If dt.Rows(0)("bidder_price") <> 0 Then
                lit_biddeer_price.Text = "My bidding price is $" & FormatNumber(dt.Rows(0)("bidder_price"), 2)
            Else
                lit_biddeer_price.Visible = False
            End If
            If is_show_reserve_price Then
                lbl_reserve_price.Text = CommonCode.GetFormatedMoney(dt.Rows(0)("reserve_price"))
            Else
                lbl_reserve_price_caption.Visible = False
            End If
            lbl_min_increament.Text = CommonCode.GetFormatedMoney(dt.Rows(0)("increament_amount"))




            Dim dt1 As DataTable = New DataTable()
            strQuery = ""
            strQuery = "select Top 1 B.auction_id, Convert(decimal(10,2),ISNULL(B.bid_amount,0)) AS max_bid_amount,U.first_name + ' ' + ISNULL(U.last_name,'') AS name  from tbl_auction_bids B WITH (NOLOCK) " & _
                        "INNER JOIN tbl_reg_buyer_users U ON B.buyer_user_id=U.buyer_user_id where(B.auction_id =" & auction_id & ") order by B.bid_amount Desc"
            dt1 = SqlHelper.ExecuteDataTable(strQuery)
            Dim rank As Integer = 0
            Dim obj As New Bid()
            If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                rank = dt.Rows(0)("current_rank") 'obj.get_buyer_bid_rank(auction_id)
            End If
            Dim bid_amount_rank As String = "--"
            If rank <> 0 Then
                'If is_show_actual_pricing_not_rank Then
                '    bid_amount_rank = "--"
                'Else

                'End If
                bid_amount_rank = rank.ToString()
            End If
            lbl_current_rank.Text = bid_amount_rank
            If dt1.Rows.Count > 0 Then
                lbl_bid_amt.Text = CommonCode.GetFormatedMoney(dt1.Rows(0)("max_bid_amount"))
            End If
            Dim bids_num As Integer = obj.get_auction_bid_number(auction_id)
            If is_show_bids_num Then
                lbl_no_of_bids.Text = bids_num.ToString()
            Else
                lbl_no_of_bids_caption.Visible = False
            End If
            obj = Nothing
            dt = Nothing
        End If

    End Sub
End Class
