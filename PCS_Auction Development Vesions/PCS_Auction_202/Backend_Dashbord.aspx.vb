﻿
Partial Class Backend_Dashbord
    Inherits System.Web.UI.Page
    Public Vew_Auction As Boolean = False
    Public Vew_Auction_in_dashboard As Boolean = False

    Private Sub setPermission()
        If Not CommonCode.is_super_admin() Then

            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))
            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 2
                        Vew_Auction = True
                    Case 38
                        Vew_Auction_in_dashboard = True
                End Select
            Next
            dt = Nothing
        Else
            Vew_Auction = True
            Vew_Auction_in_dashboard = True
        End If


    End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not CommonCode.Fetch_Cookie_Shared("user_id") = "" Then
                hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
            End If
            Show_links()
            showMessage()

            Bind_Auctions(0)

        End If
    End Sub
    Private Sub Show_links()

        Dim strQuery As String = ""

        If CommonCode.is_admin_user() Then
            strQuery = "select distinct A.auction_type_id from [tbl_auctions] A WITH (NOLOCK)  where isnull(A.discontinue,0)=0 and A.is_active=1  and dbo.get_auction_status(auction_id) in (1,2)"
        Else
            strQuery = "select distinct A.auction_type_id from [tbl_auctions] A WITH (NOLOCK) inner join [tbl_reg_seller_user_mapping] B on A.seller_id=B.seller_id where isnull(A.discontinue,0)=0 and A.is_active=1  and B.user_id=" & hid_user_id.Value & " and dbo.get_auction_status(auction_id) in (1,2)"
        End If

        Dim type_id As Integer = 0
        Dim count As Integer = 0
        Dim dtTable As New DataTable()

        dtTable = SqlHelper.ExecuteDataTable(strQuery)
        For count = 0 To dtTable.Rows.Count - 1
            Select Case dtTable.Rows(count).Item("auction_type_id")
                Case 1
                    type_id = type_id + 1
                    lnk_buy_now.Visible = True
                Case 2, 3
                    lnk_auctions.Visible = True
                Case 4
                    type_id = type_id + 1
                    lnk_quote.Visible = True
            End Select
        Next

        If lnk_auctions.Visible = True Then type_id = type_id + 1

        If type_id > 1 Then

            lnk_all.Visible = True

            If lnk_auctions.Visible = True Then
                span_bar_auctions.Visible = True
            End If
            If lnk_buy_now.Visible = True Then
                span_bar_buy_now.Visible = True
            End If
            If lnk_quote.Visible = True Then
                span_bar_quote.Visible = True
            End If
        Else
            lnk_buy_now.Visible = False
            lnk_auctions.Visible = False
            lnk_quote.Visible = False
        End If


    End Sub
    Private Sub showMessage()
        rep_message.DataSource = SqlHelper.ExecuteDataTable("select [message_board_id],[title],[description] from [tbl_message_boards]  M " & _
        "where dbo.message_board_status(ISNULL(display_from,'1/1/1900'),ISNULL(display_to,'1/1/1900'))='Running' and is_active=1 " & _
        "and " & CommonCode.Fetch_Cookie_Shared("user_id") & " in (select user_id from fn_get_message_invited_sellers((select top 1 invitation_id from tbl_message_invitations where message_board_id=M.message_board_id)))")
        rep_message.DataBind()

        If rep_message.Items.Count > 0 Then
            div_topmsg.Visible = True
            rep_message.Visible = True
        Else
            div_topmsg.Visible = False
            rep_message.Visible = False
        End If

    End Sub
    Private Sub Bind_Auctions(ByVal type_id As Integer)
        Dim str As String = ""
        lnk_auctions.CssClass = "filterBy"
        lnk_buy_now.CssClass = "filterBy"
        lnk_quote.CssClass = "filterBy"
        lnk_all.CssClass = "filterBy"
        Select Case type_id
            Case 1
                lnk_auctions.CssClass = "filterByBold"
                str = " and (A.auction_type_id = 2 or A.auction_type_id=3) "
            Case 2
                lnk_buy_now.CssClass = "filterByBold"
                str = " and A.auction_type_id = 1  "
            Case 3
                lnk_quote.CssClass = "filterByBold"
                str = " and A.auction_type_id = 4  "
            Case Else
                lnk_all.CssClass = "filterByBold"
        End Select
        Dim strQuery As String = ""
        If CommonCode.is_admin_user() Then
            strQuery = "select A.auction_id,A.code,A.title,A.auction_type_id,ISNULL(A.start_date, '1/1/1900') AS start_date,ISNULL(display_end_time,'1/1/1900') As display_end_time,ISNULL(A.no_of_clicks,0) AS no_of_clicks,ISNULL(A.start_price,0) As start_price,ISNULL(A.show_price,0) as show_price,ISNULL(A.increament_amount,0) AS increament_amount,dbo.fn_get_auction_image(A.auction_id) As image_path,dbo.get_auction_status(A.auction_id) As status,case dbo.get_auction_status(A.auction_id) when 1 then DATEDIFF(MINUTE,GETDATE(),display_end_time) when 2 then DATEDIFF(MINUTE,getdate(),start_date) end As time_left,ISNULL((select max(invitation_id) from tbl_auction_invitations where auction_id=A.auction_id),0) As invitation_id,(select COUNT(*) from dbo.fn_get_auction_buyer_bid_ids(A.auction_id)) As bidders_count from [tbl_auctions] A WITH (NOLOCK)  where isnull(A.discontinue,0)=0 and A.is_active=1 " & str & "  and dbo.get_auction_status(auction_id) in (1,2) order by time_left,status"
        Else
            strQuery = "select A.auction_id,A.code,A.title,A.auction_type_id,ISNULL(A.start_date, '1/1/1900') AS start_date,ISNULL(display_end_time,'1/1/1900') As display_end_time,ISNULL(A.no_of_clicks,0) AS no_of_clicks,ISNULL(A.start_price,0) As start_price,ISNULL(A.show_price,0) as show_price,ISNULL(A.increament_amount,0) AS increament_amount,dbo.fn_get_auction_image(A.auction_id) As image_path,dbo.get_auction_status(A.auction_id) As status,case dbo.get_auction_status(A.auction_id) when 1 then DATEDIFF(MINUTE,GETDATE(),display_end_time) when 2 then DATEDIFF(MINUTE,getdate(),start_date) end As time_left,ISNULL((select max(invitation_id) from tbl_auction_invitations where auction_id=A.auction_id),0) As invitation_id,(select COUNT(*) from dbo.fn_get_auction_buyer_bid_ids(A.auction_id)) As bidders_count from [tbl_auctions] A WITH (NOLOCK) inner join [tbl_reg_seller_user_mapping] B on A.seller_id=B.seller_id where isnull(A.discontinue,0)=0 and A.is_active=1  and B.user_id=" & hid_user_id.Value & " " & str & " and dbo.get_auction_status(auction_id) in (1,2)  order by time_left, status"
        End If

        If Vew_Auction_in_dashboard = True Then
            rpt_auctions.DataSource = SqlHelper.ExecuteDataTable(strQuery)
            rpt_auctions.DataBind()
        Else
            rpt_auctions.Visible = False
        End If


        If rpt_auctions.Items.Count > 0 Then
            rpt_auctions.Visible = True
            pnl_noItem.Visible = False
        Else
            rpt_auctions.Visible = False
            pnl_noItem.Visible = True
        End If

    End Sub
    Protected Sub lnk_all_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_all.Click
        Bind_Auctions(0)
    End Sub
    Protected Sub lnk_auctions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_auctions.Click
        Bind_Auctions(1)
    End Sub
    Protected Sub lnk_buy_now_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_buy_now.Click
        Bind_Auctions(2)
    End Sub
    Protected Sub lnk_quote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_quote.Click
        Bind_Auctions(3)
    End Sub
    Protected Sub rpt_auctions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles rpt_auctions.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim auction_id As Integer = 0
            Dim auction_type_id As Integer = 0
            Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)
            Dim start_price As Double = 0
            Dim show_price As Double = 0
            Dim incr_amount As Double = 0
            Dim num_of_clicks As Integer = 0
            Dim invitation_id As Integer = 0
            Dim bid_count As Integer = 0
            'Dim query_count As Integer = 0
            Dim status As Integer = 0
            Dim start_date As DateTime = "1/1/1900"

            Dim display_end_time As DateTime
            Dim cur_date As DateTime = DateTime.Now
            invitation_id = drv("invitation_id")
            auction_id = drv("auction_id")
            auction_type_id = drv("auction_type_id")
            start_price = drv("start_price")
            show_price = drv("show_price")
            incr_amount = drv("increament_amount")
            num_of_clicks = drv("no_of_clicks")
            bid_count = drv("bidders_count")
            display_end_time = drv("display_end_time")
            start_date = drv("start_date")
            'query_count = SqlHelper.ExecuteScalar("select count(A.query_id) As query_num from tbl_auction_queries A where auction_id=" & auction_id & " and ISNULL(parent_query_id,0)=0 and not exists(select query_id from tbl_auction_queries where parent_query_id=A.query_id) ")
            Dim span As TimeSpan = display_end_time.Subtract(cur_date)
            Dim time_left As String = ""
            Dim dt As DataTable = New DataTable()
            Dim no_of_invited_bidders As Integer = 0
            no_of_invited_bidders = CommonCode.GetAuctionInvitedBidders(auction_id, 0).Rows.Count
            Dim ltrl_current_bid_status As Literal = CType(e.Item.FindControl("ltrl_current_bid_status"), Literal)
            Dim ltrl_price As Literal = CType(e.Item.FindControl("ltrl_price"), Literal)
            Dim ddl_View_Page As DropDownList = CType(e.Item.FindControl("ddl_View_Page"), DropDownList)
            Dim lbl_bought As Label = CType(e.Item.FindControl("lbl_bought"), Label)
            Dim ltrl_time_left As Literal = CType(e.Item.FindControl("ltrl_time_left"), Literal)
            Dim ltrl_status As Literal = CType(e.Item.FindControl("ltrl_status"), Literal)
            status = drv("status")
            time_left = span.Days & "D " & span.Hours & "H " & span.Minutes & "M"
            Select Case status
                Case 1
                    ltrl_status.Text = "<span style='font-size:12px;color:#339933;'>Running</span>"
                Case 2
                    ltrl_status.Text = "<span style='font-size:12px;color:#FF7F00;'>Upcoming</span>"
                    span = start_date.Subtract(cur_date)
                    time_left = span.Days & "D " & span.Hours & "H " & span.Minutes & "M" & " left to start"
                Case 3
                    ltrl_status.Text = "<span style='font-size:12px;color:#FF2A2A;'>Bid Closed</span>"
            End Select

            Dim dtPage As DataTable = New DataTable()
            Dim dc1 As DataColumn = New DataColumn("id")
            Dim dc2 As DataColumn = New DataColumn("name")
            dtPage.Columns.Add(dc1)
            dtPage.Columns.Add(dc2)
            Dim dr1 As DataRow = dtPage.NewRow()
            dr1(0) = "1"
            dr1(1) = "View Invited Bidder (" & no_of_invited_bidders & ")"
            Dim dr2 As DataRow = dtPage.NewRow()
            dr2(0) = "2"
            dr2(1) = "Invite Bidder"
            'Dim dr3 As DataRow = dtPage.NewRow()
            'dr3(0) = "3"
            'dr3(1) = "Answer Query (" & query_count & ")"

            Dim dr4 As DataRow = dtPage.NewRow()
            dr4(0) = "4"
            dr4(1) = "After Launch Comments"

            dtPage.Rows.Add(dr1)
            dtPage.Rows.Add(dr2)
            'dtPage.Rows.Add(dr3)
            dtPage.Rows.Add(dr4)
            ddl_View_Page.DataSource = dtPage
            ddl_View_Page.DataTextField = "name"
            ddl_View_Page.DataValueField = "id"
            ddl_View_Page.DataBind()
            ddl_View_Page.Items.Insert(0, New ListItem("Please Select", "0"))
            ddl_View_Page.Attributes.Add("onchange", "javascript:redirect_to_auction(" & ddl_View_Page.ClientID & "," & auction_id & "," & CommonCode.getAuctionTabPosition(auction_id) & "," & invitation_id & ");")
            'ltrl_time_left.Text = time_left

            Dim str As String = ""

            If auction_type_id = 2 Or auction_type_id = 3 Then
                str = "select top 1 A.bid_id,A.buyer_id, A.bid_amount ,ISNULL(C.company_name,'')AS company_name from tbl_auction_bids A WITH (NOLOCK) INNER JOIN tbl_reg_buyers C WITH (NOLOCK) ON A.buyer_id=C.buyer_id where A.auction_id=" & auction_id & " order by  A.bid_amount desc,A.bid_date ASC"
                dt = SqlHelper.ExecuteDataTable(str)
                Dim strBid As String = "Highest bid : "
                Dim company_name As String = ""

                If dt.Rows.Count > 0 Then
                    company_name = dt.Rows(0)("company_name")
                    If dt.Rows(0)("bid_amount") = 0 Then
                        strBid = strBid & "$" & FormatNumber(start_price, 2) 'FormatNumber(start_price + incr_amount, 2)
                    Else
                        strBid = strBid & "$" & FormatNumber(dt.Rows(0)("bid_amount"), 2)
                    End If

                    strBid = strBid & "<br /><a  onmouseout=""hide_tip_new();"" onmouseover=""tip_new('/Bidders/bidder_mouse_over.aspx?i=" & dt.Rows(0)("buyer_id") & "','200','white','true');"">" & company_name & "</a>"
                Else
                    strBid = strBid & "No bid to display"
                End If
                ltrl_current_bid_status.Text = strBid
                ltrl_price.Text = "Start Price : $" & FormatNumber(start_price, 2) & " (+" & incr_amount & ")"
                lbl_bought.Text = bid_count & " bidder(s) for this item "
            ElseIf auction_type_id = 1 Then
                Dim strBuy As String = ""
                Dim sold As Integer = 0
                strBuy = "Price : " & "$" & FormatNumber(show_price, 2)
                ' str = "select count(*) As sold from tbl_auction_buy where status='Accept' and Upper(ISNULL(action,''))='DENIED' and auction_id=" & auction_id
                str = "select count(*) As sold from tbl_auction_buy WITH (NOLOCK) where status='Accept' and buy_type='buy now' and  Upper(ISNULL(action,''))<>'DENIED' and auction_id=" & auction_id
                sold = SqlHelper.ExecuteScalar(str)
                strBuy = strBuy & "<br />Sold : " & sold & "/" & num_of_clicks
                ltrl_current_bid_status.Text = strBuy
                lbl_bought.Text = sold & " bidder(s) bought this item"
            Else
                lbl_bought.Text = SqlHelper.ExecuteScalar("select count(distinct buyer_id) from tbl_auction_quotations where auction_id=" & auction_id) & " bidder(s) send the offer"
            End If
        End If
    End Sub

End Class
