﻿Imports System.Globalization
Partial Class frame_rank
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            'Literal1.Text = "<a onclick=""parent.hideArea('" & Request.QueryString.Get("c2") & "');"" href='#' >Call Me</a>"
            If Request.QueryString.Get("is_frontend") = "1" Then
                div_rank.Visible = True
                'div_label.Visible = True
                setAuction()
            Else
                setTimer()
                div_rank.Visible = False
                'div_label.Visible = False
            End If

        End If
    End Sub

    Private Sub setTimer()
        Me.litRefresh.Visible = False
        If Request.QueryString.Get("i") <> Nothing AndAlso IsNumeric(Request.QueryString.Get("i")) Then
            Dim dt As DataTable
            dt = SqlHelper.ExecuteDatatable("select is_active,auction_type_id from tbl_auctions WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i"))
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("is_active") Then
                    Dim qry = "select case when start_date >= getdate() then convert(varchar,start_date, 120) else convert(varchar, display_end_time, 120) end as show_time from tbl_auctions WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i") & " and display_end_time>=getdate()"
                    qry = SqlHelper.ExecuteScalar(qry)
                    If Not String.IsNullOrEmpty(qry) Then
                        Dim enddate As Date
                        enddate = CDate(qry)
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "_call", "var launchdate = new cdLocalTime(""cdcontainer"", ""server-asp"", 0, """ & Format(enddate, "MMMM dd, yyyy HH:mm:ss") & """); launchdate.displaycountdown(""days"", formatresults2)", True)
                        Me.litRefresh.Visible = True
                    Else
                        Select Case dt.Rows(0).Item("auction_type_id")
                            Case 2, 3
                                'lit_span.Text = "<span class='lcdstyle3'>Closed!</span>"
                            Case Else
                                'lit_span.Text = "<span class='lcdstyle3'>Closed</span>"
                        End Select
                    End If
                Else
                    'lit_span.Text = "<span class='lcdstyle3'>Inactive</span>"
                End If
            Else
                'lit_span.Text = "<span class='lcdstyle3'>No Auction!</span>"
            End If
        Else
            'lit_span.Text = "<span class='lcdstyle3'>No Auction!</span>"
        End If
    End Sub

    Private Sub setAuction()
        Me.litRefresh.Visible = True
        Dim str As String = "SELECT A.auction_id,"
        str = str & "ISNULL(A.product_catetory_id, 0) AS product_catetory_id,"
        str = str & "ISNULL(A.stock_condition_id, 0) AS stock_condition_id,"
        str = str & "ISNULL(A.auction_category_id, 0) AS auction_category_id,"
        str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"
        str = str & "ISNULL(A.start_price, 0) AS start_price,"
        str = str & "ISNULL(A.reserve_price, 0) AS reserve_price,"

        str = str & "ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing,"

        str = str & "ISNULL(A.increament_amount, 0) AS increament_amount,"
        str = str & "ISNULL(A.buy_now_price, 0) AS buy_now_price,"
        str = str & "ISNULL(A.show_price, 0) AS show_price,"
        str = str & "ISNULL(A.is_show_rank, 0) AS is_show_rank,"
        str = str & "dbo.get_auction_status(A.auction_id) as auction_status,"
        str = str & "ISNULL(IMG.stock_image_id, 0) AS stock_image_id,ISNULL(IMG.filename, '') AS filename"
        str = str & " FROM "
        str = str & "tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations L on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B on A.stock_condition_id=B.stock_condition_id "
        str = str & " left join tbl_auction_stock_images IMG on A.auction_id=IMG.auction_id and IMG.position=1 "
        str = str & " WHERE "
        str = str & "A.auction_id =" & Request.QueryString.Get("i")

        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDatatable(str)

        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)

                If .Item("auction_status") <> 3 Then 'running or upcoming

                    Dim bid_amount As Double = 0
                    Dim bidder_max_amount As Double = 0
                    'panel setting
                    If .Item("auction_type_id") = 1 Then

                        lit_rank.Visible = False
                        'lit_amount_caption.Text = "Buy now for $" & FormatNumber(.Item("show_price"), 2)

                    ElseIf .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then

                        Dim dtHBid As New DataTable()
                        dtHBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i") & " order by bid_amount desc")

                        Dim c_bid_amount As Double = 0
                        If dtHBid.Rows.Count > 0 Then
                            c_bid_amount = dtHBid.Rows(0).Item("bid_amount")
                        End If

                        Dim bidder_price As Double = 0
                        Dim bidder_max_price As Double = 0

                        If String.IsNullOrEmpty(CommonCode.Fetch_Cookie_Shared("buyer_id")) Then
                            lit_rank.Text = "Current Rank : --"
                        Else
                            Dim rak As Integer = SqlHelper.ExecuteScalar("select dbo.buyer_bid_rank(" & Request.QueryString.Get("i") & "," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
                            lit_rank.Text = "Current Rank : " & IIf(rak = 0, "--", rak)

                            Dim dtBid As New DataTable()
                            dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i") & " and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by bid_amount desc")

                            If dtBid.Rows.Count > 0 Then
                                If .Item("auction_type_id") = 3 Then
                                    bidder_max_amount = dtBid.Rows(0).Item("max_bid_amount")
                                Else
                                    bidder_max_amount = dtBid.Rows(0).Item("bid_amount")
                                End If
                            End If
                            dtBid = Nothing
                        End If

                        If dtHBid.Rows.Count > 0 Then
                            'running bid
                            bid_amount = dtHBid.Rows(0).Item("bid_amount")
                            If .Item("auction_type_id") = 2 Then
                                'lit_amount_caption.Text = "Acceptable Bid : $" & FormatNumber(dtHBid.Rows(0).Item("bid_amount") + .Item("increament_amount"), 2)
                            Else
                                If bidder_max_amount = 0 Then 'bidder bid first time
                                    bidder_max_amount = dtHBid.Rows(0).Item("bid_amount") + .Item("increament_amount")
                                    'lit_amount_caption.Text = "Acceptable Bid : $" & FormatNumber(bidder_max_amount, 2)

                                Else
                                    If bidder_max_amount <= dtHBid.Rows(0).Item("bid_amount") Then '2nd position
                                        bidder_max_amount = dtHBid.Rows(0).Item("bid_amount") + .Item("increament_amount")
                                    End If
                                    'lit_amount_caption.Text = "Acceptable Bid : $" & FormatNumber(bidder_max_amount, 2)
                                End If
                            End If
                        Else
                            'first bid for this auction
                            bidder_max_amount = .Item("start_price")
                            If .Item("auction_type_id") = 2 Then
                                'lit_amount_caption.Text = "Bid now for $" & FormatNumber(bidder_max_amount, 2)
                            Else
                                'lit_amount_caption.Text = "Acceptable Bid : $" & FormatNumber(bidder_max_amount, 2)
                            End If

                        End If

                        If Convert.ToBoolean(.Item("is_show_actual_pricing")) Then
                            ' lit_amount_caption.Visible = True
                        Else
                            'lit_amount_caption.Visible = False
                        End If

                        dtHBid = Nothing
                    Else
                        'lit_amount_caption.Visible = False
                        lit_rank.Visible = False
                    End If
                    setTimer()
                Else 'bid over
                    Me.litRefresh.Visible = False
                    If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then

                        Dim dtBid As New DataTable()
                        dtBid = SqlHelper.ExecuteDatatable("select top 1 a.bid_date,ISNULL(a.bid_amount, 0) AS bid_amount,a.action,b.auction_type_id from tbl_auction_bids a WITH (NOLOCK) inner join tbl_auctions b WITH (NOLOCK) on a.auction_id=b.auction_id where a.auction_id=" & Request.QueryString.Get("i") & " and a.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by a.bid_amount desc, a.bid_id desc")

                        If dtBid.Rows.Count > 0 Then
                            Dim rak As Integer = SqlHelper.ExecuteScalar("select dbo.buyer_bid_rank(" & Request.QueryString.Get("i") & "," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")

                            Dim lbdt As DateTime = dtBid.Rows(0).Item("bid_date")
                            If dtBid.Rows(0).Item("auction_type_id") = 1 Or dtBid.Rows(0).Item("auction_type_id") = 4 Then
                                'lit_span.Text = "<span class='lcdstyle3'></span>"
                                'lit_amount_caption.Text = ""
                            Else
                                If rak = 1 Then
                                    If dtBid.Rows(0).Item("bid_amount") > .Item("reserve_price") Then
                                        'lit_span.Text = "<span class='lcdstyle1'>You Won!</span>"
                                        'lit_amount_caption.Text = lbdt.ToString("dd MMMM yyyy HH:mm:ss") & "<br><b>Won at: $" & FormatNumber(dtBid.Rows(0).Item("bid_amount"), 2) & "</b>"
                                    Else
                                        ' lit_span.Text = "<span class='lcdstyle3'>You were the<br>highest bidder.</span>"
                                        'lit_amount_caption.Text = ""
                                    End If
                                Else
                                    'lit_span.Text = "<span class='lcdstyle3'>You were not the<br>highest bidder.</span>"
                                    'lit_amount_caption.Text = "" 'lbdt.ToString("dd MMMM yyyy HH:mm:ss")
                                End If
                            End If

                        Else
                            If .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then
                                'lit_span.Text = "<span class='lcdstyle3'>Closed</span>"

                            Else
                                'lit_span.Text = "<span class='lcdstyle3'>Closed</span>"

                            End If
                            'lit_amount_caption.Text = "" 'lbdt.ToString("dd MMMM yyyy HH:mm:ss")

                        End If
                        dtBid = Nothing

                        'lit_amount_caption.Visible = True

                    Else
                        'lit_amount_caption.Visible = False
                    End If

                    lit_rank.Visible = False
                End If

                If .Item("is_show_rank") Then
                    lit_rank.Visible = True
                Else
                    lit_rank.Visible = False
                End If

            End With
        End If
        dtTable = Nothing
    End Sub

End Class
