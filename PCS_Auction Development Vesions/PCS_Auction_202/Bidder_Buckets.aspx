﻿<%@ Page Title="" Language="VB" MasterPageFile="~/SitePopUp.master" AutoEventWireup="false"
    CodeFile="Bidder_Buckets.aspx.vb" Inherits="Bidder_Buckets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
           @media only screen and (max-width : 740px)
{
   
   .bktHeader
{
    background-color:#0190BC;
    color:White;
    font-weight:bold;
    font-size:12px;
    padding:5px;
    width:95px;
    }
        .bucket_width {
            width:100px;
        }
    }

    </style>
     <script type="text/javascript">
         
         function optoutmode(ctl) {

             var gridView = document.getElementById("tb_rpt_bucket");
             var checkBoxes = gridView.getElementsByTagName("input");
             for (var i = 0; i < checkBoxes.length; i++) {
                 if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                     checkBoxes[i].checked = false;
                 }
                 if (ctl.checked) {
                     checkBoxes[i].disabled = true;
                 }
                 else {
                     checkBoxes[i].disabled = false;
                 }
             }

             if (ctl.checked) {
                 document.getElementById("tb_rpt_bucket").style.color = "#ACAEB3";
             }
             else {
                 document.getElementById("tb_rpt_bucket").style.color = "#6f6f6f";
             }
         }



    </script>
  
<table border="0" cellpadding="0" cellspacing="0" width="620px;"><tr><td>
    <h1>
        <asp:Literal ID="ltr_title" Text="title" runat="server" /></h1>
    <asp:Panel ID="pnl_request" runat="server">
        <div style="text-align: right; color: Blue; padding-right: 20px;">
            <asp:LinkButton ID="lnk_all" runat="server" Text="Select All" />
            |
            <asp:LinkButton ID="lnk_none" runat="server" Text="Clear All" />
        </div>
        <div style="clear: both; padding-top: 10px;">
            <table cellpadding="0" cellspacing="0" border="0" width="400px;" style="padding-bottom:10px;">
                            <tr>
                                <td valign="top">
                                   Buckets :
                                </td>
                                <td valign="top" style="text-align:right;">
                                            <asp:CheckBox ID="chk_optout" onclick="javascript: optoutmode(this);"  runat="server" Text="Do not send me any emails" runat="server"  />
                                </td>
                            </tr>
            </table>
            <table id="tb_rpt_bucket" cellpadding="0" cellspacing="1" style="background-color: Gray;">
                <tr>
                    <asp:Repeater ID="rpt_bucket" runat="server">
                        <ItemTemplate>
                            <td valign="top" style="background-color: White;" colspan="2">
                                <div class="bktHeader">
                                    <%# Eval("bucket_name") %>
                                </div>
                                <asp:Literal ID="lit_bucket_id" runat="server" Text='<%# Eval("bucket_id") %>' Visible="false"></asp:Literal>
                                <asp:Repeater ID="rpt_bucket_value" runat="server" OnItemDataBound="rpt_bucket_value_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:Literal ID="lit_bucket_id" runat="server" Text='<%# Eval("bucket_id") %>' Visible="false"></asp:Literal>
                                        <asp:Literal ID="lit_bucket_value_id" runat="server" Text='<%# Eval("bucket_value_id") %>'
                                            Visible="false"></asp:Literal>
                                        <asp:Literal ID="ltr_mapping_id" runat="server" Text='<%# Eval("mapping_id") %>'
                                            Visible="false"></asp:Literal>
                                        <div class='<%# iif(container.itemindex mod 2 =0,"bucketStyle","bucketStyleAlternate") %>'>
                                            <asp:CheckBox ID="chk_select" runat="server" Text='<%# Eval("bucket_value") %>' />
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </ItemTemplate>
                    </asp:Repeater>
                </tr>
            </table>
        </div>
        <div style="clear: both; padding-top: 10px; text-align: center;">
            <asp:Button ID="imgbtn_Update" Text="Update" CssClass="sbmt_btn" runat="server" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnl_response" runat="server" Visible="false">
        <div style="height: 380px; width: 460px; padding: 20px;">
            <asp:Label ID="lbl_popmsg_msg" ViewStateMode="Disabled" runat="server" Font-Size="14"
                ForeColor="#e9650e" Style="margin: 2px 0px;" Font-Bold="true"></asp:Label>
        </div>
    </asp:Panel>
    <br />
    <br />
    <center>
        <div id='dv_askpop_ask'>
            <input type="button" name="" id="ImageButtonCancelMsg" onclick="javascript:window.close();"
                value="Close" />
        </div>
    </center>
</td></tr></table>
</asp:Content>
