﻿
Partial Class Backend_Dashbord_qry
    Inherits System.Web.UI.Page

    Protected Sub load_auction_bidder_queries()
        Dim qry As String = ""
        qry = "select A.query_id,ISNULL(A.auction_id,0) As auction_id,isnull(A.message,'') as message,A.title,ISNULL(A1.title,'') as auction_name,ISNULL(S.first_name,'') + ' ' + ISNULL(S.last_name,'') As ask_to,ISNULL(A.sales_rep_id,0) AS sales_rep_id,A.submit_date,A.buyer_id,B.company_name,isnull(case when B.state_id<>0 then (select name from tbl_master_states where state_id=B.state_id) else B.state_text end,'') as state,isnull(A.is_active,0) as is_active,ISNULL(admin_marked,0) As admin_marked,(select count(query_id) from tbl_auction_queries where parent_query_id=A.query_id) As answered_count from tbl_auction_queries A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id left join tbl_auctions A1 WITH (NOLOCK) on A.auction_id=A1.auction_id Left JOIN tbl_sec_users S ON A.sales_rep_id=S.user_id "
        If IsNumeric(Request.QueryString.Get("q")) Then
            qry = qry & IIf(Request.QueryString.Get("q") = "0", " inner join tbl_auctions Q WITH (NOLOCK) WITH (NOLOCK) on A.auction_id=Q.auction_id", "") & " where isnull(A1.discontinue,0)=0 and isnull(A.parent_query_id,0)=0 and isnull(A.auction_id,0)" & IIf(Request.QueryString.Get("q") = "0", ">0 and Q.is_active=1", "=0")
        Else
            qry = qry & " where isnull(A1.discontinue,0)=0 and  isnull(A.parent_query_id,0)=0 "
        End If
        If Request.QueryString.Get("a") <> Nothing AndAlso IsNumeric(Request.QueryString.Get("a")) Then
            qry = qry & " and A.auction_id=" & Request.QueryString.Get("a") & ""
        End If
        If Request.QueryString.Get("b") <> Nothing AndAlso IsNumeric(Request.QueryString.Get("b")) Then
            qry = qry & " and A.buyer_id=" & Request.QueryString.Get("b") & ""
        End If
        If CommonCode.is_admin_user() = False Then
            qry = qry & " and A.buyer_id in (select distinct buyer_id from tbl_reg_sale_rep_buyer_mapping where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ")"
        End If

        lnk_all.CssClass = "filterBy"
        lnk_pending.CssClass = "filterBy"
        lnk_ans.CssClass = "filterBy"


        If ViewState("qry_filter") = "all" Then
            lnk_all.CssClass = "filterByBold"
            lbl_no_record.Text = "Currently query is not available"
        End If
        If ViewState("qry_filter") = "answered" Then
            qry = qry & " and (ISNULL(admin_marked,0)=1 or A.query_id in (select isnull(parent_query_id,0) from tbl_auction_queries))"
            lnk_ans.CssClass = "filterByBold"
            lbl_no_record.Text = "Currently answered query is not available"
        End If
        If ViewState("qry_filter") = "pending" Then
            qry = qry & " and (ISNULL(A.admin_marked,0)=0 and A.query_id not in (select isnull(parent_query_id,0) from tbl_auction_queries))"
            lnk_pending.CssClass = "filterByBold"
            lbl_no_record.Text = "Currently pending query is not available"
        End If
        
        If ViewState("order_by") = "dateA" Then
            qry = qry & " order by submit_date"
        End If
        If ViewState("order_by") = "dateD" Then
            qry = qry & " order by submit_date desc"
        End If
        If ViewState("order_by") = "sales_rep" Then
            qry = qry & " order by S.first_name asc"
        End If
        If ViewState("order_by") = "customer" Then
            qry = qry & " order by B.company_name"
        End If
        'qry = qry & " order by submit_date desc"
        'Response.Write(qry)
        'Exit Sub
        rep_after_queries.DataSource = SqlHelper.ExecuteDataTable(qry)
        rep_after_queries.DataBind()
        If rep_after_queries.Items.Count > 0 Then
            rep_after_queries.Visible = True
            lbl_no_record.Visible = False
        Else
            rep_after_queries.Visible = False
            lbl_no_record.Visible = True
        End If

    End Sub

    Protected Sub lnk_all_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_all.Click
        ViewState("qry_filter") = "all"
        hid_query_type.Value = "all"
        load_auction_bidder_queries()
    End Sub
    Protected Sub lnk_pending_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_pending.Click
        ViewState("qry_filter") = "pending"
        hid_query_type.Value = "pending"
        load_auction_bidder_queries()
    End Sub
    Protected Sub lnk_ans_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_ans.Click
        ViewState("qry_filter") = "answered"
        hid_query_type.Value = "answered"
        load_auction_bidder_queries()
    End Sub
    
    Protected Sub rep_after_queries_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rep_after_queries.ItemCommand
        If e.CommandName = "send_query" Then
            Dim lbl_auction_id As Integer = DirectCast(e.Item.FindControl("lbl_auction_id"), Label).Text
            Dim lbl_sales_rep_id As Integer = DirectCast(e.Item.FindControl("lbl_sales_rep_id"), Label).Text

            Dim Rsp As String = DirectCast(e.Item.FindControl("rep_txt_message"), TextBox).Text.Trim.Replace("'", "''").Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")
            If Rsp <> "Response" Then
               
                SqlHelper.ExecuteNonQuery("insert into tbl_auction_queries (sales_rep_id,auction_id,message,buyer_id,user_id,parent_query_id,submit_date) values (" & lbl_sales_rep_id & "," & lbl_auction_id & ",'" & Rsp & "'," & DirectCast(e.Item.FindControl("rep_hid_buyer"), HiddenField).Value & "," & CommonCode.Fetch_Cookie_Shared("user_id") & "," & e.CommandArgument & ",getdate())")
                SqlHelper.ExecuteNonQuery("update tbl_auction_queries set admin_marked=1 where query_id=" & e.CommandArgument)
                Dim obj_email As New Email
                obj_email.Send_Bidder_Reply(CommonCode.Fetch_Cookie_Shared("user_id"), e.CommandArgument, Rsp)
                load_auction_bidder_queries()
                ' DirectCast(e.Item.FindControl("rep_txt_message"), TextBox).Text = "insert into tbl_auction_queries (sales_rep_id,auction_id,message,buyer_id,user_id,parent_query_id,submit_date) values (" & lbl_sales_rep_id & "," & lbl_auction_id & ",'" & Rsp & "'," & DirectCast(e.Item.FindControl("rep_hid_buyer"), HiddenField).Value & "," & CommonCode.Fetch_Cookie_Shared("user_id") & "," & e.CommandArgument & ",getdate())"
            End If
        ElseIf e.CommandName = "mark" Then

            SqlHelper.ExecuteNonQuery("update tbl_auction_queries set admin_marked=1 where query_id=" & e.CommandArgument)
            load_auction_bidder_queries()

        ElseIf e.CommandName = "send_note" Then

            Dim nts As String = DirectCast(e.Item.FindControl("txt_note"), TextBox).Text
            If nts <> "" Then
                SqlHelper.ExecuteNonQuery("INSERT INTO tbl_auction_query_note(query_id, message, created_date, created_by) VALUES (" & e.CommandArgument & ", '" & nts & "', getdate(), " & CommonCode.Fetch_Cookie_Shared("user_id") & ")")
                DirectCast(e.Item.FindControl("txt_note"), TextBox).Text = ""
                Dim rep_notes As Repeater = DirectCast(e.Item.FindControl("rptr_note"), Repeater)
                rep_notes.DataSource = SqlHelper.ExecuteDatatable("SELECT ISNULL(A.message, '') AS message,ISNULL(U.first_name, '') + ' ' + ISNULL(U.last_name, '') as full_name FROM tbl_auction_query_note A inner join tbl_sec_users U on A.created_by=U.user_id	WHERE A.query_id=" & e.CommandArgument & " order by A.qry_note_id ")
                rep_notes.DataBind()
            End If

        End If
    End Sub

    Protected Sub rep_after_queries_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rep_after_queries.ItemDataBound
        Dim rep_inner As Repeater = DirectCast(e.Item.FindControl("rep_inner_after_queries"), Repeater)
        If Request.QueryString.Get("b") <> Nothing AndAlso IsNumeric(Request.QueryString.Get("b")) Then
            rep_inner.DataSource = SqlHelper.ExecuteDatatable("select A.message, A.submit_date, ISNULL(C.first_name,'') AS first_name,ISNULL(C.last_name,'') AS last_name,ISNULL(D.first_name,'') AS buyer_first_name,ISNULL(D.last_name,'') AS buyer_last_name,isnull(C.title,'') as title,isnull(D.title,'') as buyer_title,isnull(C.image_path,'') as image_path,C.user_id from tbl_auction_queries A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id left join tbl_sec_users C on A.user_id=C.user_id LEFT JOIN tbl_reg_buyer_users D ON A.buyer_user_id=D.buyer_user_id where A.parent_query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & " and B.buyer_id=" & Request.QueryString.Get("b") & " order by submit_date DESC")
        Else
            rep_inner.DataSource = SqlHelper.ExecuteDatatable("select A.message, A.submit_date, ISNULL(C.first_name,'') AS first_name,ISNULL(C.last_name,'') AS last_name,ISNULL(D.first_name,'') AS buyer_first_name,ISNULL(D.last_name,'') AS buyer_last_name,isnull(C.title,'') as title,isnull(D.title,'') as buyer_title,isnull(C.image_path,'') as image_path,C.user_id from tbl_auction_queries A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id left join tbl_sec_users C on A.user_id=C.user_id LEFT JOIN tbl_reg_buyer_users D ON A.buyer_user_id=D.buyer_user_id where A.parent_query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & " order by submit_date DESC")
        End If

        rep_inner.DataBind()


        Dim rep_notes As Repeater = DirectCast(e.Item.FindControl("rptr_note"), Repeater)
        rep_notes.DataSource = SqlHelper.ExecuteDatatable("SELECT ISNULL(A.message, '') AS message,ISNULL(U.first_name, '') + ' ' + ISNULL(U.last_name, '') as full_name FROM tbl_auction_query_note A inner join tbl_sec_users U on A.created_by=U.user_id	WHERE A.query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & " order by A.qry_note_id ")
        rep_notes.DataBind()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            
            ViewState("qry_filter") = "pending"
            ViewState("order_by") = "dateA"
            load_auction_bidder_queries()
        End If
    End Sub
    Protected Sub ddlSortBy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSortBy.SelectedIndexChanged
        ViewState("order_by") = ddlSortBy.SelectedItem.Value
        load_auction_bidder_queries()
    End Sub

    Protected Sub but_bind_query_Click(sender As Object, e As System.EventArgs) Handles but_bind_query.Click
        load_auction_bidder_queries()
    End Sub
End Class
