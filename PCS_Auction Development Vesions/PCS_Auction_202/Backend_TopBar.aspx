﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Backend_TopBar.aspx.vb" Inherits="Backend_TopBar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="/Style/menu.css" />
    <script type="text/javascript" src="/pcs_js/dropdowntabs.js">

        /***********************************************
        * Drop Down Tabs Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
        * This notice MUST stay intact for legal use
        * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
        ***********************************************/

    </script>
    <script type="text/javascript" language="JavaScript">
        function redirectIframe(path1, path2, path3) {
            if (path1 != '')
                top.topFrame.location = path1;
            if (path2 != '')
                top.leftFrame.location = path2;
            if (path3 != '')
                top.mainFrame.location = path3;
        }
        function redirectPage(path) {
            if (path != '')
                top.location.href = path;
        }

        function lostFocus() {
            document.getElementById("txt_search").value = 'Search';
            return false;
        }


        function clearBox() {
            if (document.getElementById("txt_search").value == 'Search')
                document.getElementById("txt_search").value = '';
            return false;
        }
        function on_key_down(e) {
            if (e.keyCode == 13) {
                if (document.getElementById("txt_search").value != '')
                    redirectIframe('', '', '/Searchresult.aspx?sh=' + document.getElementById("txt_search").value);
                    //top.mainFrame.location = ;
                return false;
            }
            return true;
        }
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hid_sel_auc_id" runat="server" Value="0" />
    <div>
        <div id="mainBody">
            <div class="topR1">
                <div class="l1">
                    <a href="javascript:void(0);" onclick="top.location.href = '/Backend_Home.aspx'" style="text-decoration:none;"><img src="/Images/logo.png" border="0" alt="PCS Wireless Auctions" /></a>
                </div>
                <div class="l2">
                    <input type="text" value="Search" name="txt_search" id="txt_search" onfocus="return clearBox();"
                        onblur="return lostFocus()" onkeydown="return on_key_down(event)" class="searchtextbox" />
                </div>
                <div class="l3">
                    <div class="l2R1">
                        <div class="topmnu1">
                            <a href="/logout.aspx" target="_top">Logout</a>
                        </div>
                        <%--<div class="topmnusep">
                            |</div>
                        <div class="topmnu2">
                            <a href="javascript:void(0);">Help</a>
                        </div>--%>
                        <div class="topmnusep">
                            |</div>
                        <div class="topmnu3">
                            <a href="javascript:void(0);" onclick="redirectIframe('Backend_TopBar.aspx?t=0','/Backend_Leftbar.aspx?t=0','/users/my_account.aspx');">My Account</a>
                        </div>
                        <div class="topmnusep">
                            |</div>
                        <div class="topmnu4">
                             <asp:Literal ID="lit_username" runat="server"></asp:Literal>  
                        </div>
                    </div>
                    <div class="l2R2">
                        <asp:Literal ID="lit_systime" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
            <div class="topR2">
                <div id="glowmenu" class="glowingtabs">
                    <ul>
                        <li><a href="javascript:void(0);" onclick="redirectIframe('/Backend_TopBar.aspx?t=1','/Backend_Leftbar.aspx?t=1','<%=iif(CommonCode.is_sales_rep(),"/SalesHome.aspx",iif(commoncode.is_bidder_approval_agent(),"Bidders/BidderApproval.aspx","Backend_Dashbord.aspx")) %>');"
                            title="Home" class='<%=getCss(1,0) %>'><span class='<%=getCss(1,1) %>'>Home</span></a></li>
                            <% If (show_bidder) Then%>
                        <li><a href="javascript:void(0);" onclick="redirectIframe('/Backend_TopBar.aspx?t=2','/Backend_Leftbar.aspx?t=2','/Bidders/Buyers.aspx');"
                            title="Bidders" class='<%=getCss(2,0) %>'><span class='<%=getCss(2,1) %>'>Bidders</span></a></li>
                            <% End If%>
                             <% If (show_auction) Then%>
                        <li><a href="javascript:void(0);" onclick="redirectIframe('/Backend_TopBar.aspx?t=3','/Backend_Leftbar.aspx?t=3','/Auctions/AuctionListing.aspx');"
                            title="Auctions" class='<%=getCss(3,0) %>'><span class='<%=getCss(3,1) %>'>Auctions</span></a></li>
                             <% End If%>
                        <div class="auctiontabs">
                            <ul>
                                <asp:Repeater ID="rptItems" runat="server" Visible="false">
                                    <ItemTemplate>
                                        <li><a href="javascript:void(0);" id="auc_atag" runat="server" >
                                            <asp:Label CssClass="" ID="auc_span" runat="server"><small style="font-size: 12px;"
                                                onclick="redirectIframe('/Backend_TopBar.aspx?t=<%# Container.ItemIndex + 4 %>&a=<%# Eval("auction_id") %>','/Backend_Leftbar.aspx?t=3&a=<%# Eval("auction_id") %>','/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %>');">
                                                Auc-<%# IIf(Eval("title").ToString().Trim().Length > 8, Left(Eval("title").ToString, 6) & "..", Eval("title"))%></small>
                                                   <img id="auc_img" alt="" runat="server" src="/Images/menu/cross_white.gif" style="border: none;
                                                    margin-left: 10px;" /></asp:Label></a> </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
