﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SystemLogListing.ascx.vb"
    Inherits="Log_UserControls_SystemLogListing" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
    Skin="Simple" />
<asp:HiddenField ID="hid_unique_id" runat="server" Value="0" />
<asp:HiddenField ID="hid_mudule_name" runat="server" Value="" />
<div class="pageheading">
                    System Log</div> 
<br />
<telerik:RadGrid ID="RadGrid_System_Logs" GridLines="None" runat="server" AllowAutomaticDeletes="True"
    AllowAutomaticInserts="True" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True" 
    AutoGenerateColumns="False" DataSourceID="SqlDataSource1" AllowMultiRowSelection="false"
    AllowMultiRowEdit="false" Skin="Vista" AllowSorting="true" ShowGroupPanel="false">
    <PagerStyle Mode="NextPrevAndNumeric" />
    <HeaderStyle BackColor="#BEBEBE" />
    <ItemStyle Font-Size="11px" />
    <MasterTableView Width="100%" DataKeyNames="log_id" DataSourceID="SqlDataSource1"
        ItemStyle-Height="50" AlternatingItemStyle-Height="50" HorizontalAlign="NotSet"
        AutoGenerateColumns="False" SkinID="Vista" CommandItemDisplay="None">
        <NoRecordsTemplate>
            No log available
        </NoRecordsTemplate>
        <Columns>
            <telerik:GridBoundColumn DataField="module_name" HeaderText="Module" SortExpression="module_name"
                UniqueName="module">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="description" HeaderText="Description" SortExpression="description"
                UniqueName="description">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ip_address" HeaderText="IP Address" SortExpression="ip_address"
                UniqueName="ip_address">
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText="User" SortExpression="row_create_user" UniqueName="row_create_user" DataType="System.String"
                                AutoPostBackOnFilter="false"  DataField="row_create_user">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/users/user_mouse_over.aspx?i=<%# Eval("row_create_user_id") %>','270','white','true');">
                                        <%# Eval("row_create_user")%></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
         
             <telerik:GridBoundColumn DataField="row_create_date" HeaderText="Log Date" SortExpression="row_create_date"
                UniqueName="row_create_date">
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings AllowDragToGroup="false">
        <Selecting AllowRowSelect="True"></Selecting>
    </ClientSettings>
    <GroupingSettings ShowUnGroupButton="false" />
</telerik:RadGrid>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT log_id,ISNULL(A.description, '') AS description,	ISNULL(A.module_name, '') AS module_name,ISNULL(A.page_name, '') AS page_name,		ISNULL(A.function_name, '') AS function_name,		ISNULL(A.unique_id, 0) AS unique_id,		ISNULL(A.ip_address, '') AS ip_address,		ISNULL(A.row_create_date, '1/1/1900') AS row_create_date,		ISNULL(A.row_create_user_id, 0) AS row_create_user_id,
		ISNULL(U.first_name,'') + ' ' + ISNULL(U.last_name,'') as row_create_user
	    FROM tbl_sec_system_log A INNER JOIN tbl_sec_users U ON A.row_create_user_id=U.user_id	    WHERE A.module_name=@module_name and (A.unique_id=@unique_id or @unique_id=0) ORDER BY A.row_create_date DESC">
    <SelectParameters>
        <asp:ControlParameter Name="module_name" Type="String" ControlID="hid_mudule_name" PropertyName="Value" />
        <asp:ControlParameter Name="unique_id" Type="Int32" ControlID="hid_unique_id" PropertyName="Value" />
    </SelectParameters>
</asp:SqlDataSource>
