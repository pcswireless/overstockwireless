﻿Imports Telerik.Web.UI

Partial Class Users_profiledetail
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Request.QueryString.Get("e") = "1" Then
                lbl_msg.Text = "New profile created successfully"
            Else
                lbl_msg.Text = ""
            End If
            fill_permissions()
            If IsNumeric(Request.QueryString.Get("i")) Then
                ViewState("profile_id") = Request.QueryString.Get("i")
                'basic profile
                txt_profile_name.ReadOnly = True
                txt_profile_code.ReadOnly = False
                bind_profile_detail()
                FillAssignPermission()
                set_button_visibility("VIEW")
                RadTabStrip1.Tabs(0).Text = "Edit Auction"
                RadTabStrip1.SelectedIndex = 0
                RadMultiPage1.SelectedIndex = 0
                RadTabStrip1.Tabs(0).CssClass = "auctiontab"
                RadTabStrip1.Tabs(1).CssClass = "auctiontab"
                RadTabStrip1.Tabs(2).CssClass = "auctiontab"
                RadTabStrip1.Tabs(0).SelectedCssClass = "auctiontabSelected"
                RadTabStrip1.Tabs(1).SelectedCssClass = "auctiontabSelected"
                RadTabStrip1.Tabs(2).SelectedCssClass = "auctiontabSelected"
                dv_manage_cancel.Visible = False
                but_manage_update.Visible = False
                RadGrid_Manage_All.Visible = True
                profile_manage = SqlHelper.ExecuteDataTable("exec [sel_profile_manage]")
                FillManageAllProfile()
                RadGrid_Manage_Edit.Visible = False

            Else
                ViewState("profile_id") = 0
                ViewState("manage_profile") = Nothing
                set_button_visibility("Add")
                RadTabStrip1.Tabs(1).Visible = False
                RadTabStrip1.Tabs(2).Visible = False
                RadTabStrip1.Tabs(0).Text = "New Profile"
            End If
        End If
    End Sub

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Private Sub setPermission()

        If Not CommonCode.is_super_admin() Then
            Dim View_Profile As Boolean = False
            Dim Edit_Profile As Boolean = False
            Dim Assign_user As Boolean = False
            Dim Manage_Profile As Boolean = False
            Dim New_Profile As Boolean = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 18
                        New_Profile = True
                    Case 19
                        View_Profile = True
                    Case 20
                        Edit_Profile = True
                    Case 21
                        Assign_user = True
                    Case 22
                        Manage_Profile = True
                End Select
            Next
            dt = Nothing
            If Not Request.QueryString("i") = Nothing Then
                If Not View_Profile Then Response.Redirect("/NoPermission.aspx")
                If Not Edit_Profile Then
                    pnl_Edit_User_Buttons.Visible = False
                End If
                If Not Assign_user Then
                    but_2_update.Visible = False
                End If
                If Not Manage_Profile Then
                    pnl_manage_profile_buttons.Visible = False
                End If
            Else
                If Not New_Profile Then Response.Redirect("/NoPermission.aspx")
            End If

        End If
    End Sub

#Region "Profile Info"
    Private Sub bind_profile_detail()
        Dim str As String
        str = "select name,ISNULL(is_active,0) AS is_active,isnull(profile_code,'') as profile_code,iSNULL(description,'') as description,case when (select top 1 profile_id from tbl_sec_user_profile_mapping where profile_id=" & ViewState("profile_id") & ")>0 then 0 else 1 end AS is_active_modify from tbl_sec_profiles where profile_id=" & ViewState("profile_id")
        Dim dt As New DataTable
        dt = SqlHelper.ExecuteDataTable(str)
        If dt.Rows.Count > 0 Then
            page_heading.Text = dt.Rows(0)("name")
            txt_profile_name.Text = dt.Rows(0)("name")
            txt_profile_code.Text = dt.Rows(0)("profile_code")
            'lbl_profile.Text = dt.Rows(0)("name")
            lbl_profile_name.Text = dt.Rows(0)("name")
            lbl_profile_code.Text = dt.Rows(0)("profile_code")
            lbl_profile_description.Text = dt.Rows(0)("description")
            txt_profile_description.Text = dt.Rows(0)("description").Replace("<br/>", Char.ConvertFromUtf32(13)).Replace("<br/>", Char.ConvertFromUtf32(10))
            hid_profie_active_modify.Value = dt.Rows(0)("is_active_modify")
            If dt.Rows(0)("is_active") Then
                lbl_is_active.Text = "<img src='/Images/true.gif' alt=''>"
                chk_is_active.Checked = True
            Else
                lbl_is_active.Text = "<img src='/Images/false.gif' alt=''>"
                chk_is_active.Checked = False
            End If
            Checked_PermissionAssign()
        End If
        dt.Dispose()
        dt = Nothing
    End Sub
    Private Sub fill_permissions()
        Dim dt As New DataTable
        Dim str As String = ""
        str = "select perm.permission_id, perm.description,mm.module_id,mm.name from tbl_sec_permissions perm inner join tbl_master_modules mm on mm.module_id=perm.module_id where perm.is_active=1 order by perm.module_id"
        dt = SqlHelper.ExecuteDataTable(str)
        Dim module_id = -1
        Dim dr As DataRow = dt.NewRow()
        
        dg_permission.DataSource = dt
        dg_permission.DataBind()
    End Sub
    Private Sub set_button_visibility(ByVal mode As String)
        If mode.ToUpper() = "VIEW" Then
            'pnl_edit_mode.Visible = True
            but_1_edit.Visible = True
            but_1_save.Visible = False
            but_1_update.Visible = False
            'div_1_cancel.Visible = False
            div_1_cancel.Visible = False
            ltr_permission.Visible = True
            dg_permission.Columns(0).Display = True
            dg_permission.Columns(1).Display = False
            lbl_is_active.Visible = True
            chk_is_active.Visible = False
            txt_profile_name.Visible = False
            txt_profile_code.Visible = False
            lbl_profile_name.Visible = True
            lbl_profile_code.Visible = True
            txt_profile_description.Visible = False
            lbl_profile_description.Visible = True
            span_profile_name.Visible = False
            span_profile_code.Visible = False
        ElseIf mode.ToUpper() = "EDIT" Then
            but_1_edit.Visible = False
            but_1_save.Visible = False
            but_1_update.Visible = True
            'div_1_cancel.Visible = True
            div_1_cancel.Visible = True
            ltr_permission.Visible = False
            dg_permission.Columns(0).Display = False
            dg_permission.Columns(1).Display = True
            lbl_is_active.Visible = False
            chk_is_active.Visible = True
            txt_profile_name.Visible = True
            txt_profile_code.Visible = True
            lbl_profile_name.Visible = False
            lbl_profile_code.Visible = False
            txt_profile_description.Visible = True
            lbl_profile_description.Visible = False
            span_profile_name.Visible = True
            span_profile_code.Visible = True
            'pnl_edit_mode.Visible = True
        Else
            but_1_edit.Visible = False
            but_1_save.Visible = True
            but_1_update.Visible = False
            'div_1_cancel.Visible = False
            div_1_cancel.Visible = False
            ltr_permission.Visible = False
            'dg_permission.Columns(1).Visible = True
            lbl_is_active.Visible = False
            chk_is_active.Visible = True
            txt_profile_name.Visible = True
            txt_profile_code.Visible = True
            lbl_profile_name.Visible = False
            lbl_profile_code.Visible = False
            txt_profile_description.Visible = True
            lbl_profile_description.Visible = False
            dg_permission.Columns(0).Display = False
            dg_permission.Columns(1).Display = True
        End If
        If txt_profile_code.Text.Trim().ToUpper = "PR02" Then
            txt_profile_code.Visible = False
            lbl_profile_code.Visible = True
        End If
    End Sub
    Protected Sub but_1_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_1_edit.Click

        set_button_visibility("Edit")

        lbl_msg.Text = ""
    End Sub
    Protected Sub Checked_PermissionAssign()
        Dim str As String
        str = "select permission_id from tbl_sec_profile_permission_mapping where profile_id=" & ViewState("profile_id")
        Dim dt As New DataTable
        dt = SqlHelper.ExecuteDataTable(str)
        For i = 0 To dg_permission.Items.Count - 1
            'If dg_permission.Items(i).ItemType = ListItemType.Item Or dg_permission.Items(i).ItemType = ListItemType.AlternatingItem Then
            Dim chk_permission As New CheckBox
            Dim img_permission As New Image
            chk_permission = CType(dg_permission.Items(i).FindControl("chk_permission"), CheckBox)
            img_permission = CType(dg_permission.Items(i).FindControl("img_permission"), Image)
            Dim dr As DataRow()
            Dim item As GridDataItem = DirectCast(dg_permission.Items(i), GridDataItem)
            Dim id As String = item.GetDataKeyValue("permission_id")
            dr = dt.Select("permission_id=" & id)
            If dr.Length > 0 Then
                chk_permission.Checked = True
                img_permission.ImageUrl = "/Images/true.gif"
            Else
                chk_permission.Checked = False
                img_permission.ImageUrl = "/Images/false.gif"
            End If
            If id = 0 Then
                chk_permission.Visible = False
                img_permission.Visible = False
                dg_permission.Items(i).CssClass = Nothing
                dg_permission.Items(i).Style.Add("height", "30px")
                dg_permission.Items(i).Font.Bold = True
            End If
            ' End If
        Next
        dt.Dispose()
        dt = Nothing
    End Sub

    Protected Sub but_1_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_1_cancel.Click
        set_button_visibility("View")
        lbl_msg.Text = ""
    End Sub

    Protected Sub but_1_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_1_update.Click
        If Page.IsValid Then
            Dim str As String
            str = "select count(profile_id) from tbl_sec_profiles where profile_code='" & txt_profile_code.Text.Trim() & "' and profile_id<>" & ViewState("profile_id")
            If SqlHelper.ExecuteScalar(str) = "1" Then
                lbl_msg.Text = "Profile Code Already Exists."
                Return
            End If
            If hid_profie_active_modify.Value = "0" Then
                If chk_is_active.Checked = False Then
                    lbl_msg.Text = "Profile can not be deactivate because its assign to one or more user."
                    Exit Sub
                End If
            End If
            str = "Update tbl_sec_profiles set is_active=" & IIf(chk_is_active.Checked, 1, 0) & ",profile_code='" & txt_profile_code.Text.Trim() & "',description='" & CommonCode.encodeSingleQuote(txt_profile_description.Text.Trim.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "' where profile_id=" & ViewState("profile_id") & "; delete from tbl_sec_profile_permission_mapping where profile_id=" & ViewState("profile_id")
            SqlHelper.ExecuteNonQuery(str)
            str = ""

            For Each lsi As GridDataItem In dg_permission.Items
                Dim chk_permission As New CheckBox
                chk_permission = CType(lsi.FindControl("chk_permission"), CheckBox)
                If chk_permission.Checked = True Then
                    str = "insert into tbl_sec_profile_permission_mapping values (" & ViewState("profile_id") & "," & lsi.GetDataKeyValue("permission_id") & ") "
                    SqlHelper.ExecuteNonQuery(str)
                End If
            Next

            bind_profile_detail()
            FillAssignPermission()
            set_button_visibility("View")

            lbl_msg.Text = "Profile updated successfully"

        End If

    End Sub
    Protected Sub but_1_save_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles but_1_save.Click
        Dim str As String
        If txt_profile_name.Text.Trim() <> "" And txt_profile_code.Text.Trim() <> "" Then
            If check_profile_exists() = 0 Then
                str = "Insert into tbl_sec_profiles (name,profile_code,description,is_active) Values ('" & txt_profile_name.Text.Trim() & "','" & txt_profile_code.Text.Trim() & "','" & CommonCode.encodeSingleQuote(txt_profile_description.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "'," & IIf(chk_is_active.Checked, 1, 0) & ") select SCOPE_IDENTITY()"
                str = "select count(profile_id) from tbl_sec_profiles where name='" & txt_profile_name.Text.Trim() & "';" & "select count(profile_id) from tbl_sec_profiles where profile_code='" & txt_profile_code.Text.Trim() & "'"
                Dim temp As DataSet = SqlHelper.ExecuteDataset(str)

                If temp.Tables(0).Rows(0)(0) = 1 Then
                    lbl_msg.Text = "Profile Name Already Exists."
                    Return
                End If
                If temp.Tables(0).Rows(0)(1) = 1 Then
                    lbl_msg.Text = "Profile Code Already Exists."
                    Return
                End If
                temp.Dispose()
                Dim profile_id As Integer = SqlHelper.ExecuteScalar(str)
                If profile_id > 0 Then
                    ViewState("profile_id") = profile_id
                    For Each lsi As GridDataItem In dg_permission.Items
                        Dim chk_permission As New CheckBox
                        chk_permission = CType(lsi.FindControl("chk_permission"), CheckBox)
                        If chk_permission.Checked = True Then
                            str = "insert into tbl_sec_profile_permission_mapping (profile_id,permission_id) values (" & profile_id & "," & lsi.GetDataKeyValue("permission_id") & ") "
                            SqlHelper.ExecuteNonQuery(str)
                        End If
                    Next

                    ' pnl_edit_mode.Visible = True
                    bind_profile_detail()
                    FillAssignPermission()
                    set_button_visibility("View")

                    Response.Redirect("/Users/profiledetail.aspx?e=1&i=" & profile_id)
                End If
            Else
                lbl_msg.Text = "Profile already exists."
            End If
        End If

    End Sub
    Private Function check_profile_exists() As Integer
        Dim strQuery As String = "if exists(select profile_id from tbl_sec_profiles where name='" & txt_profile_name.Text.Trim() & "') select 1 else select 0"
        Dim status As Integer = SqlHelper.ExecuteScalar(strQuery)
        Return status
    End Function
    Protected Sub FillAssignPermission()

        Dim str As String
        str = "select ppm.profile_id,ppm.permission_id,perm.description from tbl_sec_profile_permission_mapping ppm inner join tbl_sec_permissions perm on ppm.permission_id=perm.permission_id  where ppm.profile_id=" & ViewState("profile_id") & ""
        Dim dt As New DataTable
        dt = SqlHelper.ExecuteDataTable(str)
        ltr_permission.Text = ""
        If dt.Rows.Count > 0 Then
            'For i As Integer = 0 To dg_permission.Items.Count - 1
            '    If dg_permission.Items(i).Selected = True Then
            '        ltr_permission.Text = ltr_permission.Text & "<img src='/Images/true.gif' alt=''>&nbsp;" & dg_permission.Items(i).Text & "<br/>"
            '    Else
            '        'If i = dg_permission.Items.Count - 1 Then
            '        ltr_permission.Text = ltr_permission.Text & "<img src='/Images/false.gif' alt=''>&nbsp;" & dg_permission.Items(i).Text & "<br/>"
            '        'End If
            '    End If
            'Next
        Else
            ltr_permission.Text = "<font color='red'>No any Permission Assign to " & txt_profile_name.Text.Trim() & "</font>"
        End If
        dt.Dispose()
        dt = Nothing
        'dg_permission.Visible = False
    End Sub
#End Region

   
    Protected Sub RadGrid_UserAssign_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        If IsNumeric(Request.QueryString.Get("i")) Then
            Dim Str As String
            '        Str = "select A.USER_ID,A.first_name+' '+A.last_name as name,A.email,A.phone,A.title,(select count(m.USER_ID) from tbl_sec_user_profile_mapping m where m.USER_ID=USER_ID and m.profile_id=" & ViewState("profile_id") & ") as is_map from tbl_sec_users A " & _
            '"INNER JOIN tbl_sec_user_profile_mapping M ON A.user_id=M.user_id INNER JOIN tbl_sec_profiles P ON M.profile_id=P.profile_id where P.profile_id=" & Request.QueryString("i")

            Str = "select A.USER_ID,A.first_name+' '+ A.last_name as name,A.email,A.phone,A.title,Case ISNULL(M.profile_id,0) when 0 then 0 else 1 end as is_map  from tbl_sec_users A " & _
                    "Left JOIN tbl_sec_user_profile_mapping M ON A.user_id=M.user_id and ISNULL(M.profile_id,0)=" & Request.QueryString("i")

            RadGrid_UserAssign.DataSource = SqlHelper.ExecuteDataTable(Str)
            'If (Not Page.IsPostBack) Then
            '    RadGrid_UserAssign.MasterTableView.FilterExpression = "([is_map] = 'Assigned')"
            '    Dim column As GridColumn = RadGrid_UserAssign.MasterTableView.GetColumnSafe("is_map")
            '    column.CurrentFilterFunction = GridKnownFunction.EqualTo
            '    column.CurrentFilterValue = "Assigned"
            'End If
        End If
    End Sub
    Protected Function is_map(ByVal a As Integer) As Boolean
        If a = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Protected Sub FillManageAllProfile()
        Dim dt As New DataTable
        dt = profile_manage
        RadGrid_Manage_All.MasterTableView.Columns.Clear()
        RadGrid_Manage_Edit.MasterTableView.Columns.Clear()
        RadGrid_Manage_All.DataSourceID = "SqlData_Pro_Manage"
        RadGrid_Manage_Edit.DataSourceID = "SqlData_Pro_Manage"
        Dim boundColumn As GridBoundColumn
        boundColumn = New GridBoundColumn()
        RadGrid_Manage_Edit.MasterTableView.Columns.Add(boundColumn)
        RadGrid_Manage_All.MasterTableView.Columns.Add(boundColumn)
        boundColumn.DataField = "permission_id"
        boundColumn.HeaderText = "permission_id"
        boundColumn.UniqueName = "permission_id"
        boundColumn.Visible = False
        boundColumn = New GridBoundColumn()
        RadGrid_Manage_Edit.MasterTableView.Columns.Add(boundColumn)
        RadGrid_Manage_All.MasterTableView.Columns.Add(boundColumn)
        boundColumn.DataField = "name"
        boundColumn.HeaderText = "name"
        boundColumn.UniqueName = "name"
        boundColumn.Visible = False
        boundColumn = New GridBoundColumn()
        RadGrid_Manage_Edit.MasterTableView.Columns.Add(boundColumn)
        RadGrid_Manage_All.MasterTableView.Columns.Add(boundColumn)
        boundColumn.DataField = "description"
        boundColumn.HeaderText = "Permission"
        boundColumn.UniqueName = "description"
        boundColumn.SortExpression = "description"
        'boundColumn.ShowSortIcon = True
        boundColumn.ItemStyle.Width = 300
        Dim ChkColumn As GridCheckBoxColumn
        Dim ImgColumn As GridImageColumn
        For i = 3 To profile_manage.Columns.Count - 1
            ChkColumn = New GridCheckBoxColumn()
            ImgColumn = New GridImageColumn()
            RadGrid_Manage_Edit.MasterTableView.Columns.Add(ChkColumn)
            RadGrid_Manage_All.MasterTableView.Columns.Add(ImgColumn)
            ChkColumn.HeaderText = dt.Columns(i).ColumnName
            ChkColumn.UniqueName = dt.Columns(i).ColumnName.Trim.Replace(" ", "") & "_"
            ChkColumn.UniqueName = ChkColumn.UniqueName & dt.Rows(0)(i).ToString.Remove(dt.Rows(0)(i).ToString.Trim.LastIndexOf("#")).Replace(" ", "")
            ChkColumn.SortExpression = dt.Columns(i).ColumnName
            ChkColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            ImgColumn.HeaderText = dt.Columns(i).ColumnName
            ImgColumn.UniqueName = dt.Columns(i).ColumnName.Trim.Replace(" ", "") & "_"
            ImgColumn.UniqueName = ImgColumn.UniqueName & dt.Rows(0)(i).ToString.Remove(dt.Rows(0)(i).ToString.Trim.LastIndexOf("#")).Replace(" ", "")
            ImgColumn.UniqueName = dt.Columns(i).ColumnName.Replace(" ", "") & "_" & dt.Rows(0)(i).ToString.Remove(dt.Rows(0)(i).ToString.LastIndexOf("#")).Replace(" ", "")
            ImgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            ImgColumn.SortExpression = dt.Columns(i).ColumnName
        Next
       
        
    End Sub

    Protected Sub CheckAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Integer
        If sender.checked Then
            For i = 0 To Me.RadGrid_UserAssign.Items.Count - 1
                CType(RadGrid_UserAssign.Items(i).FindControl("chk1"), CheckBox).Checked = True
            Next
        Else
            For i = 0 To Me.RadGrid_UserAssign.Items.Count - 1
                CType(RadGrid_UserAssign.Items(i).FindControl("chk1"), CheckBox).Checked = False
            Next
        End If
    End Sub

    Protected Sub but_2_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_2_update.Click
        Dim Str As String = ""
        For i = 0 To RadGrid_UserAssign.Items.Count - 1
            Dim checkBox As CheckBox = CType(RadGrid_UserAssign.Items(i).FindControl("chk1"), CheckBox)
            If checkBox.Checked = True Then
                Str = "if exists(select user_id from tbl_sec_user_profile_mapping where user_id=" & RadGrid_UserAssign.Items(i).Cells(2).Text & " and profile_id=" & ViewState("profile_id") & ") begin return end else begin insert into tbl_sec_user_profile_mapping(profile_id,user_id) values(" & ViewState("profile_id") & "," & RadGrid_UserAssign.Items(i).Cells(2).Text & ") end"
                SqlHelper.ExecuteNonQuery(Str)
            Else
                Str = "delete from  tbl_sec_user_profile_mapping where profile_id=" & ViewState("profile_id") & " and user_id=" & RadGrid_UserAssign.Items(i).Cells(2).Text
                SqlHelper.ExecuteNonQuery(Str)

            End If
        Next
        lbl_profile.Text = "User assignment updated successfully."
        RadGrid_UserAssign.Rebind()
        'fillprofilemap()
    End Sub

    Protected Sub but_manage_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_manage_update.Click
        'Try
        Dim Str As String = ""
        Dim dt As New DataTable
        dt = profile_manage
        For k = 3 To dt.Columns.Count - 1
            For i = 0 To RadGrid_Manage_Edit.MasterTableView.Items.Count - 1
                Dim chk As New CheckBox
                chk = CType(RadGrid_Manage_Edit.MasterTableView.Items(i)(dt.Columns(k).ColumnName.Trim.Replace(" ", "") & "_" & dt.Rows(i)(k).ToString.Trim.Remove(dt.Rows(i)(k).ToString.Trim.LastIndexOf("#")).Replace(" ", "")).Controls(0), CheckBox)
                'Response.Write(dt.Columns(k).ColumnName)
                If chk.Checked Then
                    Str = "if exists(select permission_id from tbl_sec_profile_permission_mapping where permission_id=" & RadGrid_Manage_Edit.MasterTableView.Items(i)("permission_id").Text & " and profile_id=" & dt.Rows(i)(k).ToString.Remove(dt.Rows(i)(k).ToString.LastIndexOf("#")) & ") begin return end else begin insert into tbl_sec_profile_permission_mapping(permission_id,profile_id) values(" & RadGrid_Manage_Edit.Items(i)("permission_id").Text & "," & dt.Rows(i)(k).ToString.Remove(dt.Rows(i)(k).ToString.LastIndexOf("#")) & ") end"
                    'Response.Write(Str)
                    SqlHelper.ExecuteNonQuery(Str)
                Else
                    Str = "delete from  tbl_sec_profile_permission_mapping where permission_id=" & RadGrid_Manage_Edit.Items(i)("permission_id").Text.Trim.Replace(" ", "") & " and profile_id=" & dt.Rows(i)(k).ToString.Trim.Remove(dt.Rows(i)(k).ToString.LastIndexOf("#")).Replace(" ", "")
                    'Response.Write(Str)
                    SqlHelper.ExecuteNonQuery(Str)
                End If
            Next
        Next
        'Exit Sub
        profile_manage = SqlHelper.ExecuteDatatable("exec [sel_profile_manage]")
        RadGrid_Manage_All.Visible = True
        FillManageAllProfile()

        RadGrid_Manage_Edit.Visible = False
        but_manage_update.Visible = False
        dv_manage_cancel.Visible = False
        but_manage_edit.Visible = True
        
        lbl_msg.Text = "Manage profiles successfully updated"
        
    End Sub

    Protected Sub but_manage_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_manage_edit.Click
        'FillManageAllProfile("edit")
        RadGrid_Manage_Edit.Visible = True
        RadGrid_Manage_All.Visible = False
        but_manage_update.Visible = True
        dv_manage_cancel.Visible = True
        but_manage_edit.Visible = False
    End Sub

    Protected Sub but_manage_cancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles but_manage_cancel.Click
        but_manage_update.Visible = False
        but_manage_edit.Visible = True
        ' FillManageAllProfile("view")
        RadGrid_Manage_Edit.Visible = False
        RadGrid_Manage_All.Visible = True
        dv_manage_cancel.Visible = False
    End Sub


    Protected Property profile_manage() As DataTable
        Get
            Return DirectCast(ViewState("_manage_state"), DataTable)
        End Get
        Set(value As DataTable)
            ViewState("_manage_state") = value
        End Set
    End Property

    Protected Sub RadGrid_Manage_Edit_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid_Manage_Edit.ItemDataBound
        If e.Item.ItemType = GridItemType.AlternatingItem Or e.Item.ItemType = GridItemType.Item Then
            For k = 4 To e.Item.Cells.Count - 1
                If e.Item.Cells(k).Controls.Count > 0 Then
                    Dim c As New CheckBox
                    c = CType(e.Item.Cells(k).Controls(0), CheckBox)
                    c.Enabled = True
                    c.Checked = IIf(DirectCast(e.Item.DataItem, DataRowView).Item(k - 3).ToString.Remove(0, DirectCast(e.Item.DataItem, DataRowView).Item(k - 3).ToString.LastIndexOf("#") + 1) = "1", True, False)
                End If
            Next
        End If
    End Sub
    Protected Sub RadGrid_Manage_All_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid_Manage_All.ItemDataBound
        If e.Item.ItemType = GridItemType.AlternatingItem Or e.Item.ItemType = GridItemType.Item Then
            For k = 4 To e.Item.Cells.Count - 1
                If e.Item.Cells(k).Controls.Count > 0 Then
                    CType(e.Item.Cells(k).Controls(0), Image).ImageUrl = IIf(DirectCast(e.Item.DataItem, DataRowView).Item(k - 3).ToString.Remove(0, DirectCast(e.Item.DataItem, DataRowView).Item(k - 3).ToString.LastIndexOf("#") + 1) = "1", "/Images/true.gif", "/Images/false.gif")
                End If
            Next
        End If
    End Sub

End Class
