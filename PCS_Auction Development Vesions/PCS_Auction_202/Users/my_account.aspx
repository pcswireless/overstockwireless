﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="my_account.aspx.vb" Inherits="Users_my_account" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <script type="text/javascript">
        function open_pass_win(_path, _pwd) {
            w1 = window.open(_path + '?' + _pwd, '_paswd', 'top=' + ((screen.height - 350) / 2) + ', left=' + ((screen.width - 500) / 2) + ', height=350, width=500,scrollbars=yes,toolbars=no,resizable=1;');
            w1.focus();
            return false;

        }

       
        //  End -->
</script>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Simple">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_edit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_save">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_cancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    <asp:Literal ID="page_heading" runat="server" Text="New User"></asp:Literal></div>
            </td>
        </tr>
         <tr>
            <td>
                <div style="float: left; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="PageTab">
                    <span>Edit User </span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="PageTabContentEdit">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td class="tdTabItem">
                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                CssClass="TabGrid">
                                <div style="text-align: left;padding-left:5px;">
                                    <asp:Label ID="lbl_msg" runat="server" Text="" ForeColor="Red" EnableViewState="false"></asp:Label>
                                </div>
                                <table cellpadding="0" cellspacing="2" border="0" width="838">
                                    <tr>
                                        <td colspan="4" class="dv_md_sub_heading">
                                            Personal Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 145px;" class="caption">
                                            First Name&nbsp;<span id="span_first_name" runat="server" class="req_star">*</span>
                                        </td>
                                        <td style="width: 270px;" class="details">
                                            <asp:TextBox ID="txt_first_name" runat="server" CssClass="inputtype" MaxLength="20"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="req_first_name" runat="server" ControlToValidate="txt_first_name"
                                                ValidationGroup="a" Display="Dynamic" ErrorMessage="<br>First Name Required"></asp:RequiredFieldValidator>
                                            <asp:Label ID="lbl_first_name" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td style="width: 145px;" class="caption">
                                            Last Name
                                        </td>
                                        <td style="width: 270px;" class="details">
                                            <asp:TextBox ID="txt_last_name" runat="server" CssClass="inputtype" MaxLength="20"></asp:TextBox>
                                            <asp:Label ID="lbl_last_name" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="caption">
                                            Title&nbsp;
                                        </td>
                                        <td class="details">
                                            <asp:DropDownList ID="ddl_title" runat="server" DataTextField="name" DataValueField="name"
                                                CssClass="drop_down">
                                            </asp:DropDownList>
                                            <asp:Label ID="lbl_title" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="dv_md_sub_heading">
                                            Contact Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="caption">
                                            Street&nbsp;<span id="span_address1" runat="server" class="req_star">*</span>
                                        </td>
                                        <td class="details">
                                            <asp:TextBox ID="txt_address1" runat="server" TextMode="MultiLine" CssClass="inputtype"
                                                Height="35px"></asp:TextBox>
                                            <asp:Label ID="lbl_address1" runat="server" Text=""></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfv_address1" runat="server" ControlToValidate="txt_address1"
                                                ValidationGroup="a" Display="Dynamic" ErrorMessage="<br />Street Required" CssClass="error"></asp:RequiredFieldValidator>
                                        </td>
                                        <td class="caption">
                                            Street Address 2
                                        </td>
                                        <td class="details">
                                            <asp:TextBox ID="txt_address2" runat="server" TextMode="MultiLine" CssClass="inputtype"
                                                Height="35px"></asp:TextBox>
                                            <asp:Label ID="lbl_address2" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="caption">
                                            City&nbsp;<span id="span_city" runat="server" class="req_star">*</span>
                                        </td>
                                        <td class="details">
                                            <asp:TextBox ID="txt_city" runat="server" ValidationGroup="a" CssClass="inputtype"
                                                MaxLength="50"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Required_city" ControlToValidate="txt_city" runat="server"
                                                ErrorMessage="<br>City Required" Display="Dynamic" ValidationGroup="a"></asp:RequiredFieldValidator>
                                            <asp:Label ID="lbl_city" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="caption">
                                            State/Province&nbsp;<span id="span_state" runat="server" class="req_star">*</span>
                                        </td>
                                        <td class="details">
                                            <asp:DropDownList ID="ddl_state" runat="server" DataTextField="name" DataValueField="state_id"
                                                CssClass="drop_down">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Required_state" ValidationGroup="a" runat="server"
                                                ControlToValidate="ddl_state" InitialValue="0" Display="Dynamic" ErrorMessage="<br>State Required"></asp:RequiredFieldValidator>
                                            <asp:Label ID="lbl_state" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="caption">
                                            Postal Code&nbsp;<span id="span_zip" runat="server" class="req_star">*</span>
                                        </td>
                                        <td valign="top" class="details">
                                            <asp:TextBox ID="txt_zip" runat="server" ValidationGroup="a" CssClass="inputtype"
                                                MaxLength="15"></asp:TextBox>
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_zip" ValidationGroup="a"
                                                ErrorMessage="<br>Postal Code Required" ID="Required_zip" Display="Dynamic"> </asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="Regular_zip" runat="server" ControlToValidate="txt_zip"
                                                ValidationGroup="a" ErrorMessage="<br>Invalid Postal Code" ValidationExpression="^[a-zA-Z0-9 ]*$"
                                                Display="Dynamic"></asp:RegularExpressionValidator>
                                            <asp:Label ID="lbl_zip" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="caption">
                                            Phone 1&nbsp;<span id="span_mobile" runat="server" class="req_star">*</span>
                                        </td>
                                        <td class="details">
                                            <asp:Label ID="lbl_mobile" runat="server" Text=""></asp:Label>
                                            <asp:TextBox ID="txt_mobile" ValidationGroup="a" runat="server" CssClass="inputtype"
                                                MaxLength="10"></asp:TextBox>
                                            <br />
                                            <asp:RadioButton ID="rdo_ph_1_landline" runat="server" Text="Landline" GroupName="p1"
                                                Checked="true" /><asp:RadioButton ID="rdo_ph_1_mobile" runat="server" Text="Mobile"
                                                    GroupName="p1" />
                                           <%-- <asp:RegularExpressionValidator ID="Regular_mobile" runat="server" ValidationGroup="a"
                                                ValidationExpression="^[0-9]{10}" ControlToValidate="txt_mobile" ErrorMessage="<br>Invalid Phone 1"
                                                Display="Dynamic"></asp:RegularExpressionValidator>--%>
                                            <asp:RequiredFieldValidator ValidationGroup="a" ID="Required_mobile" runat="server"
                                                ControlToValidate="txt_mobile" Display="Dynamic" ErrorMessage="<br>Phone 1 Required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td class="caption">
                                                            Phone Ext
                                                        </td>
                                                        <td class="details">
                                                            <asp:Label ID="lbl_phone_ext" runat="server"></asp:Label>
                                                            <asp:TextBox ID="txt_phone_ext" runat="server" CssClass="inputtype" MaxLength="50"></asp:TextBox>
                                                         </td>
                                       
                                    </tr>
                                    <tr>
                                         <td class="caption">
                                            Phone 2
                                        </td>
                                        <td class="details">
                                            <asp:Label ID="lbl_phone" runat="server" Text=""></asp:Label>
                                            <asp:TextBox ID="txt_phone" runat="server" ValidationGroup="a" CssClass="inputtype"
                                                MaxLength="15"></asp:TextBox>
                                            <br />
                                            <asp:RadioButton ID="rdo_ph_2_landline" runat="server" Text="Landline" GroupName="p2"
                                                Checked="true" /><asp:RadioButton ID="rdo_ph_2_mobile" runat="server" Text="Mobile"
                                                    GroupName="p2" />
                                           <%-- <asp:RegularExpressionValidator ID="Regular_phone" runat="server" ValidationExpression="^\d+$"
                                                ValidationGroup="a" ControlToValidate="txt_phone" ErrorMessage="<br>Invalid Phone 2"
                                                Display="Dynamic"></asp:RegularExpressionValidator>--%>
                                        </td><td class="caption">
                                            Email
                                        </td>
                                        <td class="details">
                                            <asp:Label ID="lbl_email" runat="server" Text=""></asp:Label>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="dv_md_sub_heading">
                                            Account setup
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="caption">
                                            User name
                                        </td>
                                        <td class="details">
                                            <asp:Label ID="lbl_username" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="caption">
                                            Password&nbsp;<span id="span_password" runat="server" class="req_star">*</span>
                                        </td>
                                        <td class="details">
                                            <asp:Literal ID="lit_change_password" runat="server"></asp:Literal>
                                            <asp:Label ID="lbl_password" runat="server" Text="******"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="dv_md_sub_heading">
                                            Other Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="caption">
                                            Picture
                                        </td>
                                        <td valign="top" class="details">
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <a id="a_IMG_Image_1" runat="server" href="javascript:void(0);" target="_blank" style="font-family: Arial;
                                                            font-size: 10pt;">
                                                            <div runat="server" onmouseout="hide_tip_new();" style="cursor: hand;" id="DIV_IMG_Default_Logo1_Img">
                                                                <asp:Image BorderWidth="1" BorderColor="Black" ID="img_user"  Width="60" Height="72"
                                                                    ImageUrl="~/Images/buyer_icon.gif" runat="server"></asp:Image>
                                                            </div>
                                                        </a>
                                                    </td>
                                                   
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="caption">
                                            Is Active
                                        </td>
                                        <td class="details">
                                            <asp:Label ID="lbl_active" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </telerik:RadAjaxPanel>
                            <br />
                        </td>
                        <td class="fixedcolumn" valign="top">
                            <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                            <asp:Panel ID="Panel1_Content1button" runat="server" CssClass="collapsePanel">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel11" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                                    <div class="addButton">
                                        <asp:ImageButton ID="img_button_edit" ImageUrl="~/Images/edit.gif" runat="server" />
                                        <%--<asp:ImageButton ID="img_button_save" ValidationGroup="a" ImageUrl="~/Images/save.gif"
                                            runat="server" />--%>
                                        <asp:ImageButton ID="img_button_update" ValidationGroup="a" ImageUrl="~/Images/update.gif"
                                            runat="server" />
                                    </div>
                                    <div class="cancelButton" id="div_basic_cancel" runat="server">
                                        <asp:ImageButton ID="img_button_cancel" ImageUrl="~/Images/cancel.gif" runat="server" />
                                    </div>
                                </telerik:RadAjaxPanel>
                            </asp:Panel>
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
