﻿Imports Telerik.Web.UI

Partial Class Users_salerep_bidders
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            If (IsNumeric(Request.QueryString.Get("i")) And Not String.IsNullOrEmpty(Request.QueryString.Get("t"))) AndAlso Request.QueryString.Get("t") = "u" Then
                hid_user_id.Value = Request.QueryString.Get("i")
            ElseIf (IsNumeric(Request.QueryString.Get("i")) And Not String.IsNullOrEmpty(Request.QueryString.Get("t"))) AndAlso Request.QueryString.Get("t") = "b" Then
                hid_bucket_id.Value = Request.QueryString.Get("i")
            ElseIf Request.QueryString.Get("t") = "bi" Then
                hid_bucket_id.Value = Request.QueryString.Get("i")
                hid_bucket_item_id.Value = Request.QueryString.Get("ii")
            Else
                hid_bucket_id.Value = 0
                hid_user_id.Value = 0
                hid_bucket_item_id.Value = 0
            End If
        End If
    End Sub

    Protected Function is_map(ByVal a As Integer) As Boolean
        If a = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Protected Sub CheckAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Integer
        If sender.checked Then
            For i = 0 To Me.RadGrid_BidderAssign.Items.Count - 1
                CType(RadGrid_BidderAssign.Items(i).FindControl("chk1"), CheckBox).Checked = True
            Next
        Else
            For i = 0 To Me.RadGrid_BidderAssign.Items.Count - 1
                CType(RadGrid_BidderAssign.Items(i).FindControl("chk1"), CheckBox).Checked = False
            Next
        End If
    End Sub

    Protected Sub RadGrid_BidderAssign_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid_BidderAssign.NeedDataSource
        If (IsNumeric(Request.QueryString.Get("i")) And Not IsNumeric(Request.QueryString.Get("ii")) And Not String.IsNullOrEmpty(Request.QueryString.Get("t"))) AndAlso Request.QueryString.Get("t") = "u" Then
            RadGrid_BidderAssign.DataSource = SqlHelper.ExecuteDatatable("SELECT S.buyer_id, S.company_name,isnull((S.contact_first_name+' '+S.contact_last_name),'') as contact,isnull(S.contact_title,'') as contact_title,S.email,S.phone,is_select=0 FROM tbl_reg_buyers S WITH (NOLOCK) where S.status_id=2 and S.buyer_id not in (select buyer_id from tbl_reg_sale_rep_buyer_mapping where user_id=" & hid_user_id.Value & ") order by S.company_name")
        ElseIf (IsNumeric(Request.QueryString.Get("i")) And Not IsNumeric(Request.QueryString.Get("ii")) And Not String.IsNullOrEmpty(Request.QueryString.Get("t"))) AndAlso Request.QueryString.Get("t") = "b" Then
            RadGrid_BidderAssign.DataSource = SqlHelper.ExecuteDatatable("SELECT S.buyer_id, S.company_name,isnull((S.contact_first_name+' '+S.contact_last_name),'') as contact,isnull(S.contact_title,'') as contact_title,S.email,S.phone,is_select=0 FROM tbl_reg_buyers S WITH (NOLOCK) where S.status_id=2 and S.buyer_id not in (select buyer_id from tbl_reg_buyer_bucket_mapping where bucket_id=" & hid_bucket_id.Value & ") order by S.company_name")
        ElseIf (IsNumeric(Request.QueryString.Get("i")) AndAlso IsNumeric(Request.QueryString.Get("ii")) And Not String.IsNullOrEmpty(Request.QueryString.Get("t"))) AndAlso Request.QueryString.Get("t") = "bi" Then
            RadGrid_BidderAssign.DataSource = SqlHelper.ExecuteDatatable("SELECT S.buyer_id, S.company_name,isnull((S.contact_first_name+' '+S.contact_last_name),'') as contact,isnull(S.contact_title,'') as contact_title,S.email,S.phone,is_select=0 FROM tbl_reg_buyers S WITH (NOLOCK) where S.status_id=2 and S.buyer_id not in (select buyer_id from tbl_reg_buyer_bucket_mapping where bucket_id=" & hid_bucket_id.Value & " and bucket_value_id=" & hid_bucket_item_id.Value & ") order by S.company_name")
        Else
            RadGrid_BidderAssign.DataSource = Nothing
        End If

    End Sub

    Protected Sub but_save_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles but_save.Click
        Dim dt As New DataTable
        Dim obj As New CommonCode

        Dim ins_qry As String = ""
        Response.Write(hid_user_id.Value & "  s" & hid_bucket_id.Value & "  " & hid_bucket_item_id.Value)
        If hid_user_id.Value > 0 And hid_bucket_id.Value = 0 And hid_bucket_item_id.Value = 0 Then
            'Dim del_qry As String = "delete s from tbl_reg_sale_rep_buyer_mapping s inner join tbl_reg_buyers b on s.buyer_id=b.buyer_id where b.status_id=2 and s.user_id=" & hid_user_id.Value & ";"
            'dt = SqlHelper.ExecuteDataTable(del_qry)
            Try
                For i = 0 To Me.RadGrid_BidderAssign.Items.Count - 1
                    If CType(RadGrid_BidderAssign.Items(i).FindControl("chk1"), CheckBox).Checked = True Then
                        ins_qry = "if exists(select mapping_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & RadGrid_BidderAssign.Items(i).GetDataKeyValue("buyer_id") & ") begin update tbl_reg_sale_rep_buyer_mapping set user_id=" & IIf(hid_user_id.Value > 0, hid_user_id.Value, SqlHelper.of_FetchKey("default_sales_rep_id")) & " where buyer_id=" & RadGrid_BidderAssign.Items(i).GetDataKeyValue("buyer_id") & " end else begin insert into tbl_reg_sale_rep_buyer_mapping (user_id,buyer_id) values (" & IIf(hid_user_id.Value > 0, hid_user_id.Value, SqlHelper.of_FetchKey("default_sales_rep_id")) & "," & RadGrid_BidderAssign.Items(i).GetDataKeyValue("buyer_id") & ") end"
                        dt = New DataTable
                        dt = SqlHelper.ExecuteDatatable(ins_qry)
                    End If
                Next
            Catch ex As Exception
                'lbl_msg.Text="Bidder can assign to only one salerep."
            Finally
                obj = Nothing
                dt = Nothing
            End Try
            Response.Write("<script>window.opener.popup_PostBackFunction();window.close();</script>")
        ElseIf hid_bucket_id.Value > 0 And hid_user_id.Value = 0 And hid_bucket_item_id.Value = 0 Then
            Try
                For i = 0 To Me.RadGrid_BidderAssign.Items.Count - 1
                    If CType(RadGrid_BidderAssign.Items(i).FindControl("chk1"), CheckBox).Checked = True Then
                        'ins_qry = "if exists(select mapping_id from tbl_reg_buyer_bucket_mapping where buyer_id=" & RadGrid_BidderAssign.Items(i).GetDataKeyValue("buyer_id") & ") begin update tbl_reg_buyer_bucket_mapping set bucket_id=" & hid_bucket_id.Value & " where buyer_id=" & RadGrid_BidderAssign.Items(i).GetDataKeyValue("buyer_id") & " end else begin insert into tbl_reg_buyer_bucket_mapping (bucket_id,buyer_id) values (" & hid_bucket_id.Value & "," & RadGrid_BidderAssign.Items(i).GetDataKeyValue("buyer_id") & ") end"

                        'only Assign new buyers to this bucket change logic
                        ins_qry = "insert into tbl_reg_buyer_bucket_mapping (bucket_id,buyer_id) values (" & hid_bucket_id.Value & "," & RadGrid_BidderAssign.Items(i).GetDataKeyValue("buyer_id") & ") "
                        dt = New DataTable
                        dt = SqlHelper.ExecuteDatatable(ins_qry)
                    End If
                Next
            Catch ex As Exception
                'lbl_msg.Text="Bidder can assign to only one salerep."
            Finally
                obj = Nothing
                dt = Nothing
            End Try
            '  Response.Write("<script>window.opener.location.href=window.opener.location.href;window.close();</script>")
            Response.Write("<script>window.opener.popup_PostBackFunction();window.close();</script>")
        ElseIf Request.QueryString.Get("t") = "bi" Then
            Try
                For i = 0 To Me.RadGrid_BidderAssign.Items.Count - 1
                    If CType(RadGrid_BidderAssign.Items(i).FindControl("chk1"), CheckBox).Checked = True Then
                        ins_qry = "insert into tbl_reg_buyer_bucket_mapping (bucket_id, buyer_id, bucket_value_id) values (" & Request.QueryString.Get("i") & "," & RadGrid_BidderAssign.Items(i).GetDataKeyValue("buyer_id") & ", " & Request.QueryString.Get("ii") & ")"
                        dt = New DataTable
                        dt = SqlHelper.ExecuteDatatable(ins_qry)
                    End If
                Next
            Catch ex As Exception
                'lbl_msg.Text="Bidder can assign to only one salerep."
            Finally
                obj = Nothing
                dt = Nothing
            End Try
            '  Response.Write("<script>window.opener.location.href=window.opener.location.href;window.close();</script>")
            Response.Write("<script>window.opener.popup_PostBackFunction();window.close();</script>")
        End If
    End Sub

    Protected Sub RadGrid_BidderAssign_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid_BidderAssign.PreRender
        Dim menu As GridFilterMenu = RadGrid_BidderAssign.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub

End Class
