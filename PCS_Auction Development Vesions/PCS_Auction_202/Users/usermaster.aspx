﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="usermaster.aspx.vb" Inherits="Users_usermaster" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">

    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                     args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
                else {
                    args.set_enableAjax(true);
                }
            }

        </script>
    </telerik:RadScriptBlock>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    User Listing
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; width: 40%; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    Please select the name to edit from below
                </div>
                <div style="float: right; width: 40%; text-align: right; margin-bottom: 3px;" id="div_new_user"
                    runat="server">
                    <a style="text-decoration: none" href="/Users/Userdetail.aspx">
                        <img src="/images/add_new_user.gif" style="border: none" alt="Add New User" /></a></div>
            </td>
        </tr>
          <tr>
            <td>
                <div style="height: 8px; display: block;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                    <ClientEvents OnRequestStart="Cancel_Ajax" />
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="RadGrid1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                                <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="RadAjaxPanel1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
                    left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
                    <img id="Image8" src="/images/img_loading.gif" />
                </telerik:RadAjaxLoadingPanel>
                <telerik:RadGrid ID="RadGrid1" AllowFilteringByColumn="True" AutoGenerateColumns="false"
                    AllowSorting="True" PageSize="10" ShowFooter="False" ShowGroupPanel="True" AllowPaging="True"
                    runat="server" Skin="Vista" EnableLinqExpressions="false">
                    <PagerStyle Mode="NextPrevAndNumeric" />
                    <ExportSettings IgnorePaging="true" FileName="User_Export" OpenInNewWindow="true"
                        ExportOnlyData="true" />
                    <MasterTableView DataKeyNames="user_id" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                        AllowFilteringByColumn="True" ItemStyle-Height="40" AlternatingItemStyle-Height="40"
                        CommandItemDisplay="Top">
                        <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false" ShowExportToPdfButton="false"
                            ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                        <NoRecordsTemplate>
                            No users to display
                        </NoRecordsTemplate>
                        <SortExpressions>
                            <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                        </SortExpressions>
                        
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Name" SortExpression="name" UniqueName="name" DataType="System.String"
                                AutoPostBackOnFilter="false" FilterListOptions="VaryByDataType" ShowFilterIcon="true" DataField="name" HeaderStyle-Width="150"
                                FilterControlWidth="140" GroupByExpression="name [name] Group By name">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('user_mouse_over.aspx?i=<%# Eval("user_id") %>','270','white','true');" onclick="redirectIframe('','','/Users/Userdetail.aspx?i=<%# Eval("user_id") %>')">
                                        <%# Eval("name")%></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="username" HeaderText="User Name" AutoPostBackOnFilter="false" DataType="System.String"
                                ShowFilterIcon="true" SortExpression="username" FilterListOptions="VaryByDataType" ItemStyle-Width="120" UniqueName="username"
                                FilterControlWidth="100">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="mobile" HeaderText="Phone"
                                AutoPostBackOnFilter="false" FilterListOptions="VaryByDataType" ShowFilterIcon="true" SortExpression="mobile" ItemStyle-Width="120"
                                UniqueName="mobile" FilterControlWidth="100">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="is_active" DataType="System.Boolean" SortExpression="is_active"
                                DataField="is_active" HeaderText="Is Active" ItemStyle-Width="65" GroupByExpression="is_active [Is Active] Group By is_active">
                                <FilterTemplate>
                                    <telerik:RadComboBox ID="RadComboBo_Chk_Active" runat="server" OnClientSelectedIndexChanged="StatusActiveIndexChanged"
                                        Width="120px" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("is_active").CurrentFilterValue %>'>
                                        <Items>
                                            <telerik:RadComboBoxItem Text="All" Value="" />
                                            <telerik:RadComboBoxItem Text="Active" Value="True" />
                                            <telerik:RadComboBoxItem Text="Inactive" Value="False" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    <telerik:RadScriptBlock ID="RadScriptBlock_combo_active" runat="server">
                                        <script type="text/javascript">
                                            function StatusActiveIndexChanged(sender, args) {
                                                var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                if (args.get_item().get_value() == "") {
                                                    tableView.filter("is_active", args.get_item().get_value(), "NoFilter");
                                                }
                                                else {
                                                    tableView.filter("is_active", args.get_item().get_value(), "EqualTo");
                                                }
                                            }
                                        </script>
                                    </telerik:RadScriptBlock>
                                </FilterTemplate>
                                <ItemTemplate>
                                    <img src='<%#IIf(Eval("is_active"),"/Images/true.gif","/Images/false.gif") %>' style="border: none;"
                                        alt="" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn UniqueName="is_super_admin" DataType="System.Boolean" SortExpression="is_super_admin"
                                DataField="is_super_admin" HeaderText="Is Super Admin" ItemStyle-Width="65" GroupByExpression="is_super_admin [Is Super Admin] Group By is_super_admin">
                                <FilterTemplate>
                                    <telerik:RadComboBox ID="RadComboBo_Chk_Admin" runat="server" OnClientSelectedIndexChanged="StatusAdminIndexChanged"
                                        Width="120px" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("is_super_admin").CurrentFilterValue %>'>
                                        <Items>
                                            <telerik:RadComboBoxItem Text="All" Value="" />
                                            <telerik:RadComboBoxItem Text="Yes" Value="True" />
                                            <telerik:RadComboBoxItem Text="No" Value="False" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    <telerik:RadScriptBlock ID="RadScriptBlock_combo_admin" runat="server">
                                        <script type="text/javascript">
                                            function StatusAdminIndexChanged(sender, args) {
                                                var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                if (args.get_item().get_value() == "") {
                                                    tableView.filter("is_super_admin", args.get_item().get_value(), "NoFilter");
                                                }
                                                else {
                                                    tableView.filter("is_super_admin", args.get_item().get_value(), "EqualTo");
                                                }
                                            }
                                        </script>
                                    </telerik:RadScriptBlock>
                                </FilterTemplate>
                                <ItemTemplate>
                                    <img src='<%#IIf(Eval("is_super_admin"),"/Images/true.gif","/Images/false.gif") %>' style="border: none;"
                                        alt="" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false"
                                ItemStyle-Width="50">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Users/Userdetail.aspx?i=<%# Eval("user_id") %>')">
                                        <img src="/images/edit.png" style="border: none;" alt="Edit" /></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowDragToGroup="true">
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="true" />
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</asp:Content>
