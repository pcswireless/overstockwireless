﻿<%@ Page Title="PCS Bidding" Language="VB" MasterPageFile="~/BackendPopUP.master" AutoEventWireup="false"
    CodeFile="Common_help.aspx.vb" Inherits="Common_help" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table cellpadding="0" cellspacing="0" border="0" style="padding-top: 4px;" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    <asp:Label ID="lbl_help_title" runat="server"></asp:Label>
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align:justify;"> 
                <asp:Literal ID="ltrl_help_desc" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td align="center">
                <div style="padding-top:10px;">
                <img src="/images/close.gif" border="none" onclick="javascript:window.close();" style="cursor: pointer;" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
