﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="Backend_Dashbord_qry.aspx.vb" Inherits="Backend_Dashbord_qry" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <script type="text/javascript">
        function openallquery(bidder_id) {
            window.open("/Auctions/BidderAllQuery.aspx?i=" + bidder_id, "_BidderAllQuery", "left=" + ((screen.width - 800) / 2) + ",top=" + ((screen.height - 400) / 2) + ",width=800,height=400,scrollbars=yes,toolbars=no,resizable=yes");
            window.focus();
            return false;
        }
    </script>
    <div class="pageheading">
        Bidder Queries</div>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" alt="" />
    </telerik:RadAjaxLoadingPanel>
   <%-- <asp:UpdatePanel ID="pnl_qry" runat="server">
        <ContentTemplate>--%>
         <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
         
    <asp:HiddenField ID="hid_query_type" runat="server" Value="Pending" />
    <asp:Button ID="but_bind_query" runat="server" BorderStyle="None" BackColor="Transparent"
        ForeColor="Transparent" Width="0" Height="0" />
            <div style="float: right; color: Blue; padding-right: 20px;"><a id="a_refresh_query" name="a_refresh_query" title="Refresh" onclick="refresh_query();" href="javascript:void(0);"><img alt="Refresh" src="/images/refresh.png" /></a></div>
            <div style="float: right; color: Blue; padding-right: 20px;">
                <span>Sort By:
                    <asp:DropDownList ID="ddlSortBy" runat="server" AutoPostBack="true">
                        <asp:ListItem Value="dateA">Date ASC</asp:ListItem>
                        <asp:ListItem Value="dateD">Date DESC</asp:ListItem>
                        <asp:ListItem Value="sales_rep">Sales Rep</asp:ListItem>
                        <asp:ListItem Value="customer">Customer</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;<span style="color: white;">------</span></span>
                <asp:LinkButton ID="lnk_all" runat="server" Text="Show All" />
                |
                <asp:LinkButton ID="lnk_pending" runat="server" Text="Pending" />
                |
                <asp:LinkButton ID="lnk_ans" runat="server" Text="Answered" />
            </div>
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="padding-top: 25px;">
                        <asp:Label ID="lbl_no_record" runat="server" Text="Currently bidder query is not available"></asp:Label>
                        <asp:Repeater ID="rep_after_queries" runat="server">
                            <ItemTemplate>
                                <div onclick="" style="color: #105386; font-weight: bold; font-size: 13px;">
                                    <%# Container.ItemIndex + 1 %>
                                    <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');"
                                        onclick="redirectIframe('/Backend_TopBar.aspx?t=<%# CommonCode.getAuctionTabPosition(Eval("auction_id")) %>&a=<%# Eval("auction_id") %>','/Backend_Leftbar.aspx?t=3&a=<%# Eval("auction_id") %>','/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %>')">
                                        <asp:Label ID="lbl_title" runat="server" Text='<%# Eval("auction_name")%>'></asp:Label></a>
                                    <%# IIf(Eval("auction_name").ToString().Trim() = "", "Asked to: " & Eval("ask_to"), "")%>
                                </div>
                                <div class="qryTitle" style="padding-left: 20px">
                                   <a name='<%# Eval("query_id")%>' href="javascript:void(0);" style="cursor:default;text-decoration:none;color:Black;"><%# Eval("title")%></a> 
                                    <%# IIf(Eval("is_active"), "", "<font color='red'>(Deleted)</font>")%>
                                </div>
                                <div style="padding-left: 22px; padding-top: 7px;">
                                    <%# Eval("message")%>
                                </div>
                                <div class="qryAlt" style="padding-left: 20px;">
                                    <br />
                                    From: <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');">
                                        <%# Eval("company_name").ToString().Trim()%></a><%# ", " & Eval("state") & ", " & Eval("submit_date")%>
                                </div>
                                <div style="padding: 20px;">
                                    <asp:Repeater ID="rep_inner_after_queries" runat="server">
                                        <ItemTemplate>
                                            <div class="qryDesc">
                                                <img src='<%# IIF(Eval("image_path")<>"","/upload/users/" & Eval("user_id") & "/" & Eval("image_path"),"/images/buyer_icon.gif") %>'
                                                    alt="" style="float: left; padding: 2px; height: 23px; width: 22px;" />
                                                <%#Eval("message") %>
                                            </div>
                                            <div class="qryAlt">
                                                <%# IIf(Eval("buyer_title") = "", Eval("title"), Eval("buyer_title"))%>
                                                <%# IIf(Eval("first_name") = "", Eval("buyer_first_name") & " " & Eval("buyer_last_name"), Eval("first_name") & " " & Eval("last_name"))%>,
                                                <%# Eval("submit_date")%>
                                            </div>
                                            <br />
                                            <br />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <br />
                                    <table width="100%" cellpadding="0" cellspacing="0" id="tbl_reply" runat="server"
                                        visible='<%# Eval("is_active")%>'>
                                        <tr>
                                            <td style="width: 525px;">
                                                <asp:TextBox runat="server" TextMode="MultiLine" onfocus="Focus(this.id,'Response');"
                                                    onblur="Blur(this.id,'Response');" CssClass="WaterMarkedTextBox" Text="Response"
                                                    ID="rep_txt_message" Width="500"></asp:TextBox>
                                                <asp:HiddenField ID="rep_hid_buyer" Value='<%#Eval("buyer_id") %>' runat="server" />
                                                <asp:Label ID="lbl_auction_id" runat="server" Text='<%#Eval("auction_id") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lbl_sales_rep_id" runat="server" Text='<%#Eval("sales_rep_id") %>'
                                                    Visible="false"></asp:Label>
                                                    
                                            </td>
                                            <td valign="bottom" style="padding-left: 15px;">
                                                <asp:ImageButton ID="rep_but_send" runat="server" CommandName="send_query" CommandArgument='<%#Eval("query_id") %>'
                                                    ImageUrl="/images/send.gif" AlternateText="Send" />
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <a href="javascript:void(0);" onclick="return openallquery(<%#Eval("buyer_id") %>)">
                                        <img border="0" src="/Images/view_all.gif" alt="View all query" /></a> &nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ImageUrl="/Images/markeasanswered.png"
                                        ID="imgBtnClose" runat="server" AlternateText="Mark as Answered" CommandArgument='<%# Eval("query_id")%>' OnClientClick="return confirm('Are you sure to mark as answered without reply?');"
                                        CommandName="mark" Visible='<%# IIf(CommonCode.is_admin_user() And CBool(Eval("admin_marked"))=False And Eval("answered_count") = 0, True, False)%>' />
                                    <br />
                                    </br>
                                    <div style="background-color: #FAFAFA; padding: 5px;" id="tbl_note" runat="server"
                                        visible='<%# Eval("is_active")%>'>
                                        <font color="black" size="2"><b>Internal Account Notes</b></font><br /><br />
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td valign="bottom" style="padding-left: 15px;">
                                                    <asp:Repeater ID="rptr_note" runat="server">
                                                        <ItemTemplate>
                                                            <div style='padding:5px 0px;<%#IIf(container.itemindex<>0,"border-top:2px solid white;","") %>'>
                                                                <%# Eval("full_name")%>-<%# Eval("message")%>
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 15px;">
                                                    <asp:Panel ID="pnl_note" runat="server" DefaultButton="rep_note_send">
                                                        <asp:TextBox runat="server" CssClass="noteBox" Text="Enter Note" ID="txt_note"></asp:TextBox>
                                                        <asp:TextBoxWatermarkExtender ID="wtm_order" runat="server" TargetControlID="txt_note"
                                                            WatermarkCssClass="noteMark" WatermarkText="Enter Note" />
                                                        <asp:ImageButton ID="rep_note_send" runat="server" CommandName="send_note" CommandArgument='<%#Eval("query_id") %>'
                                                            ImageUrl="/images/send.gif" AlternateText="Send" Style="display: none;" />
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <br />
                                    <br />
                                    <br />
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
            </telerik:RadAjaxPanel>
        <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
    <telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
    <script type="text/javascript">
        function Focus(objname, waterMarkText) {
            obj = document.getElementById(objname);
            if (obj.value == waterMarkText) {
                obj.value = "";
                obj.className = "WaterMarkText";
                if (obj.value == "Response" || obj.value == "" || obj.value == null) {
                    obj.style.color = "black";
                }
            }
        }
        function Blur(objname, waterMarkText) {

            obj = document.getElementById(objname);
            if (obj.value == "") {
                obj.value = waterMarkText;
                //                if (objname != "txtPwd") {
                //                    obj.className = "WaterMarkedTextBox";
                //                }
                //                else {
                obj.className = "WaterMarkedTextBox";
                //                }
            }
            else {
                obj.className = "WaterMarkText";
            }

            if (obj.value == "Response" || obj.value == "" || obj.value == null) {
                obj.style.color = "gray";
            }
        }
        var _refreshme, _refreshfun;
        function refresh_query() {
            // alert('currentstatus');
            document.getElementById('<%=but_bind_query.ClientID %>').disabled = false;
            document.getElementById('<%=but_bind_query.ClientID %>').click();
            _refreshfun = setTimeout('refresh_page()', 30000);

        }
        function refresh_page() {
            //alert(_option);
            _refreshme = setTimeout('refresh_query()', 30000);
        }
        window.onload = refresh_page;
    </script>
    </telerik:RadScriptBlock>
</asp:Content>
