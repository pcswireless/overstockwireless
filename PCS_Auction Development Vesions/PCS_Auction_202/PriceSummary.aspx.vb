﻿
Partial Class PriceSummary
    Inherits System.Web.UI.Page
    Dim meta As New HtmlMeta

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim str As String = "SELECT A.auction_id,"
            str = str & "ISNULL(A.product_catetory_id, 0) AS product_catetory_id,"
            str = str & "ISNULL(A.auction_category_id, 0) AS auction_category_id,"
            str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"
            str = str & "ISNULL(A.start_price, 0) AS start_price,"
            str = str & "ISNULL(A.reserve_price, 0) AS reserve_price,"
            str = str & "dbo.get_auction_status(A.auction_id) as auction_status,"
            str = str & "ISNULL(A.is_show_reserve_price, 0) AS is_show_reserve_price,"
            str = str & "ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing,"
            str = str & "ISNULL(A.is_show_no_of_bids, 0) AS is_show_no_of_bids,"
            str = str & "ISNULL(A.is_show_increment_price, 0) AS is_show_increment_price,"
            str = str & "ISNULL(A.increament_amount, 0) AS increament_amount"
            str = str & " FROM "
            str = str & "tbl_auctions WITH (NOLOCK) A WHERE "
            str = str & "A.auction_id =" & Request.QueryString.Get("i")

            Dim dtTable As New DataTable()
            dtTable = SqlHelper.ExecuteDatatable(str)

            If dtTable.Rows.Count > 0 Then
                With dtTable.Rows(0)
                    If .Item("auction_status") <> 3 Then
                        meta = New HtmlMeta
                        meta.Attributes.Add("http-equiv", "Refresh")
                        meta.Attributes.Add("content", "5")
                        Me.Page.Header.Controls.Add(meta)
                        meta = Nothing
                    End If
                    If .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then

                        Dim dtHBid As New DataTable()
                        dtHBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i") & " order by bid_amount desc")

                        Dim c_bid_amount As Double = 0
                        If dtHBid.Rows.Count > 0 Then
                            c_bid_amount = dtHBid.Rows(0).Item("bid_amount")
                        End If

                        Dim max_bid_amount As Double = 0
                        Dim bid_amount As Double = 0
                        Dim dtBid As New DataTable()
                        dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i") & " and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by bid_amount desc")

                        If dtBid.Rows.Count > 0 Then
                            With dtBid.Rows(0)
                                max_bid_amount = .Item("max_bid_amount")
                                bid_amount = .Item("bid_amount")
                            End With
                        End If
                        dtBid.Dispose()



                        Dim count As Integer = 0
                        Dim strTable As String = "<table cellpadding='0' cellspacing='1' width='100%'>"


                        If bid_amount > 0 Then
                            If .Item("auction_type_id") = 3 Then

                                If Request.QueryString.Get("j") = "g" Then
                                    strTable = strTable & "<tr><td style='color:green;'>My Current bid : $" & FormatNumber(bid_amount, 2) & "</td><tr>"
                                    strTable = strTable & "<tr><td>Auto bidder maximum bid : $" & FormatNumber(max_bid_amount, 2) & "</td></tr>"
                                Else
                                    strTable = strTable & "<tr><td style='color:green;'>My Current bid : $" & FormatNumber(bid_amount, 2) & "</td><td>Auto bidder maximum bid : $" & FormatNumber(max_bid_amount, 2) & "</td><tr>"
                                End If


                            Else
                                strTable = strTable & "<tr><td colspan='2' style='color:green;'>My Current bid : $" & FormatNumber(bid_amount, 2) & "</td><tr>"
                            End If
                        End If

                        strTable = strTable & "<tr>"

                        If Convert.ToBoolean(.Item("is_show_actual_pricing")) Then
                            If c_bid_amount > .Item("start_price") Then
                                strTable = strTable & "<td style='color:red;'>Highest Bid : $" & FormatNumber(c_bid_amount, 2) & "</td>"
                                count = count + 1
                            Else
                                strTable = strTable & "<td>Start Price : $" & FormatNumber(.Item("start_price"), 2) & "</td>"
                                count = count + 1
                            End If
                            If Request.QueryString.Get("j") = "g" Then strTable = strTable & "</tr><tr>"
                        End If


                        If .Item("is_show_increment_price") Then
                            strTable = strTable & "<td> Increments : $" & FormatNumber(.Item("increament_amount"), 2) & "</td>"
                            count = count + 1
                        End If

                        If Convert.ToBoolean(.Item("is_show_reserve_price")) Then
                            If count = 2 Or Request.QueryString.Get("j") = "g" Then
                                strTable = strTable & "</tr><tr>"
                                count = 0
                            End If

                            strTable = strTable & "<td>Reserve Price : $" & FormatNumber(.Item("reserve_price"), 2) & "</td>"
                            count = count + 1
                        End If




                        If Convert.ToBoolean(.Item("is_show_no_of_bids")) Then
                            Dim bid_total As Integer = SqlHelper.ExecuteScalar("select count(distinct buyer_id) from tbl_auction_bids WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i"))

                            If count = 2 Or Request.QueryString.Get("j") = "g" Then
                                strTable = strTable & "</tr><tr>"
                                count = 0
                            End If

                            strTable = strTable & "<td>No. of bidders : " & bid_total & "</td>"

                            count = count + 1
                        End If

                        If max_bid_amount > 0 Then

                            If .Item("auction_status") = 3 Then
                                If c_bid_amount >= .Item("reserve_price") Then
                                    Dim rak As Integer = SqlHelper.ExecuteScalar("select dbo.buyer_bid_rank(" & Request.QueryString.Get("i") & "," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
                                    If rak = 1 Then
                                        If Request.QueryString.Get("j") = "g" Then
                                            strTable = strTable & "</tr><tr><td style='color:green;'>Thank you for participating!</td>"
                                        Else
                                            strTable = strTable & "</tr><tr><td colspan='2' style='color:green;'>Thank you for participating!</td>"
                                        End If
                                    Else
                                        If Request.QueryString.Get("j") = "g" Then
                                            strTable = strTable & "</tr><tr><td style='color:green;'>Thank you for participating!</td>"
                                        Else
                                            strTable = strTable & "</tr><tr><td colspan='2' style='color:green;'>Thank you for participating!</td>"
                                        End If
                                    End If
                                Else
                                    If Request.QueryString.Get("j") = "g" Then
                                        strTable = strTable & "</tr><tr><td style='color:red;font-size:16px;'>The Reserve Price has not been met for this auction.<br>Thank you for participating!</td>"
                                    Else
                                        strTable = strTable & "</tr><tr><td colspan='2' style='color:red;font-size:16px;'>The Reserve Price has not been met for this auction.<br>Thank you for participating!</td>"
                                    End If
                                End If
                            End If
                    End If

                    strTable = strTable & "</tr></table>"
                    lit_price_setting.Text = strTable
                    End If
                End With
            End If

        End If
    End Sub
End Class
