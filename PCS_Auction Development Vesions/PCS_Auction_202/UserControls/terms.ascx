﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="terms.ascx.vb" Inherits="UserControls_terms" %>
 
<%--<div class="detail">--%>
       <%-- <div class="innerwraper">--%>
           <%-- <UC1:SalesRep runat="server" ID="UC_SalesRep"></UC1:SalesRep>--%>
            <%--<div class="paging">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li>Terms & Conditions</li>
                </ul>
            </div>--%>
            <%-- <div class="printbx">
                <ul>
                    <li class="print"><a href="javascript:window.print();">Print</a></li>
                    <li>
                        <asp:Literal ID="lit_pdf" runat="server" /></li>
                </ul>
            </div>--%>
            <div class="clear">
            </div>
           <%-- <div class="products">--%>
                    <%--<h1 class="title">
                        <asp:Literal ID="tab_sel" runat="server" /></h1>--%>
                <!--seprator -->
                <div class="clear">
                </div>
                <!--detailbxwrap -->
               
                <div class="summrybx dtltxtbx_abt" style="padding-top:2px; padding-left:10px; padding-bottom:10px!important;">
                    <div id="maincontent" style=" padding-left:0px;">
                        <%--<h1 class="pgtitle" style="margin-top: 2px;">
                            Terms and Conditions</h1>--%>
                        <p style =" text-align:justify;">
                            These Terms and Conditions (the "Agreement") applies to companies desiring to place
                            a bid on PCS Wireless, LLC’s (“PCS”) website <a href="http://www.overstockwireless.com/">www.overstockwireless.com</a>
                            (hereinafter, such merchant shall be referred to as "you"). The products listed
                            on the website <a href="http://www.overstockwireless.com/">www.overstockwireless.com</a>
                            (the "Site") may include product(s) from PCS, its affiliates and subsidiaries ("PCS
                            Product") as well as product(s) from third party sellers ("Third Party Product").
                            PCS Product and Third Party Product will be collectively referred to as "Listed
                            Products". With respect to Third Party Product, it is the objective of PCS to provide
                            access to services, including an online marketplace, so that third party sellers
                            that use the Site ("Third Party Sellers") can locate interested buyers. If you qualify
                            to place bids on this website as described below and have a bid accepted, you become
                            a buyer (“Buyer”).</p>
                        <p style =" text-align:justify;">
                            You must accept and agree to this Agreement before placing a bid. This Agreement
                            applies to the bidding process and to any purchase of Listed Products if you are
                            awarded those Listed Products. This Agreement applies to you on the date you accept
                            this Agreement and on a going-forward basis every time you use the Site or purchase
                            Listed Products on the Site. From time to time, PCS may add, delete or modify various
                            terms contained in this Agreement, and you agree to be bound by such additions,
                            deletions and modifications once they are made available on this website. Your continued
                            use of this website including but not limited to the placing of bids constitutes
                            your acceptance of such additions, deletions and modifications. <span style="text-transform: uppercase;
                                font-weight: bold;">THE SUBMISSION OF A BID CONSTITUTES AN IRREVOCABLE OFFER TO
                                PURCHASE THE LISTED PRODUCT YOU BID ON, WHICH WE MAY ACCEPT OR REJECT IN OUR SOLE
                                DISCRETION, ON THE TERMS AND CONDITIONS OF THE LISTING AND IN ACCORDANCE WITH THE
                                TERMS AND CONDITIONS OF THIS AGREEMENT. YOU AGREE THAT THIS AGREEMENT AND ITS TERMS
                                AND CONDITIONS APPLY EACH TIME YOU PLACE A BID OR PURCHASE LISTED PRODUCTS FROM
                                THE SITE.</span><span style="text-transform: uppercase;">SHOULD THE TERMS OF THIS AGREEMENT
                                    CONFLICT WITH THE TERMS OF THE LISTING THEN THE TERMS OF THIS AGREEMENT SHALL PREVAIL.</span>
                        </p>
                        <p style =" text-align:justify;">
                            <span style="text-transform: uppercase;">BY SIGNING OR OTHERWISE ACCEPTING THIS AGREEMENT,
                                YOU REPRESENT AND WARRANT TO PCS THAT YOU ARE A COMMERCIAL BUSINESS THAT IS EXPERIENCED
                                IN THE PURCHASE AND SALE OF PRODUCTS SUCH AS THE LISTED PRODUCTS AND THAT YOU HAVE
                                THE SKILL AND EXPERIENCE TO DETERMINE AN APPROPRIATE PRICE FOR THE LISTED PRODUCTS
                                PRIOR TO BIDDING ON THEM. YOU FURTHER AGREE THAT YOUR PLACING OF A BID ON A PRODUCT
                                CONFIRMS THIS REPRESENTATION AND WARRANTY. </span>
                        </p>
                        <p style =" text-align:justify;">
                            This Agreement will continue until terminated as provided in this Agreement. Notwithstanding
                            anything to the contrary herein, PCS will have the right, in its sole discretion,
                            to terminate this Agreement with or without cause upon notice to the other party.
                            PCS reserves the right, at its sole discretion, to suspend or terminate your use
                            of the Site without notice. You will be bound by this Agreement as soon as you accept
                            this Agreement by checking the "Accept" box below and you understand and agree that
                            you will be bound by this Agreement even though you will not receive a signed copy
                            from PCS. By using the Site you agree to be bound by its terms of use. If you do
                            not agree to the terms of use listed on the Site, please do not use the Site.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">1.&nbsp Required Documents.&nbsp</span>Prior to bidding,
                            you may be required to place the following on file: <strong>(1)</strong> a Signed
                            PCS Application; <strong>(2)</strong> a Resale Certificate or Country specific equivalent
                            documentation; <strong>(3)</strong>a credit card; and <strong>(4)</strong> a Photo
                            ID.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">2.&nbsp Purchase of Listed Products.&nbsp</span>"Listed
                            Products" as used herein means the Listed Products set forth in the final electronic
                            bid made by Buyer and accepted by PCS (the "Bid"). You acknowledge and agree that
                            by placing a bid for Listed Products, you agree to pay to PCS the price you bid
                            (the "Purchase Price") for the Listed Products, plus any applicable sales taxesif
                            you are awarded the Listed Products (“Total Purchase Price”). If you are awarded
                            the Listed Products, you will receive a notification email from us (the "Notification
                            Email") notifying you of your winning bid.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">3.&nbsp Payment.</span>You acknowledge that within twenty
                            four (24) hoursof your award of bid, you shall pay by wire transfer or by PayPal.
                        </p>
                        <p style =" text-align:justify;">
                            You acknowledge that if paying by credit card, immediately upon your award of bid,
                            we will automatically charge the entire amount of the Total Purchase Price to the
                            credit card on file if the Total Purchase Price is up to and including Ten Thousand
                            dollars ($10,000.00). If the Purchase Price exceeds Ten Thousand dollars ($10,000.00),
                            you will be required to wire transfer the entire Total Purchase Price within twenty
                            four (24) hours of your receipt of the Notification Email.
                        </p>
                        <p style =" text-align:justify;">
                            You agree thatthe PayPal chargeand/orwire transfer paymentand/or credit card chargeare
                            non-refundable. In the event that we do not receive the required PayPal charge or
                            wire payment or credit card charge from you within the allotted time, PCS, at its
                            sole option and discretion, may terminate the sale of the Listed Products and/or
                            this Agreement by giving written notice to you, which may be by email. Upon such
                            notice, you shall not have any right, title or interest in or to the Listed Products,
                            and the Listed Products may be marketed and sold to someone else without any liability
                            on PCS’s part and without PCS having to refund to you any prior payments made by
                            you.
                        </p>
                        <p style =" text-align:justify;">
                            If the Listed Products are not made available to you pursuant to Section 3 of this
                            Agreement within Fourteen (14) days following PCS’s receipt of your payment in full
                            for the Listed Products, then you may, at your sole discretion, cancel this Agreement
                            and your obligation to purchase the Listed Products by written notice to PCS. Such
                            termination shall be effective upon PCS’s receipt of written notice to cancel. PCS
                            shall return any payment made by you for the unavailable Listed Products under this
                            Agreement within Five (5) business days following receipt by PCS of the written
                            notice to cancel without any further liability on PCS’s part.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">4.&nbsp Receipt of Listed Products.</span><br />
                            <span class="tc_sub_head">a.&nbsp</span><span class="tc_underline">Options.&nbsp</span>The
                            following options may be available for a listing, as determined by PCS in its sole
                            discretion: (a) to have PCS ship the Listed Products; (b) for you to pick up the
                            Listed Product at the seller's facility if you paid by wire transfer; or (c) to
                            use your own shipping account. Option (a) includes rates based on the origin and
                            your destination as is listed in our records and you agree that you are responsible
                            to provide accurate information and to pay any applicable shipping rates when using
                            this option. Options (b) and (c) require you to make arrangements for any shipment
                            or pick up of the Listed Product based on the options available for a particular
                            listing on the Site. Payment for the Listed Products is required as set forth above
                            prior to any shipment, delivery or pick-up.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="tc_sub_head">b.&nbsp</span><span class="tc_underline">Appointment for Pick
                                Up.&nbsp</span>Assuming the option to pick up the Listed Product is available,
                            you will receive an email that provides instructions on how to schedule an appointment
                            to pick up the Listed Product at the seller's facility ("Scheduling Email"). You
                            will be required to pay PCS the pick up fee as set forth on the Site, if one is
                            listed, and to schedule an appointment with PCS for pick up within seventy-two (72)
                            hours of receiving the Scheduling Email. If you do not schedule an appointment within
                            such time period or fail to pick up your Listed Product at the scheduled appointment,
                            PCS may decide, in its sole discretion, to (1) arrange to ship the Listed Product
                            to you, assuming the Listed Product meets PCS’ shipping criteria, using a carrier
                            selected by PCS in its sole discretion, at the physical address you provided in
                            the User Registration and charge you for the shipping, handling, and related charges
                            ("Shipping Charges"); or (2) charge you a fee for PCS having to process and re-stock
                            the Listed Product ("Non-performance Fee"). The Non-performance Fee will be an amount
                            equal to the greater of (i) twenty-five dollars ($25.00); or (ii) ten percent (10%)
                            of the Purchase Price. You agree that we may charge you such Shipping Charges or
                            Non-Performance Fee without prior notification to you, without waiving any other
                            rights or remedies of PCS, and that the Shipping Charges or Non-Performance Fee
                            are non-refundable. You also acknowledge that if you are charged a Non-Performance
                            Fee, PCS will issue you a refund equal to the amount of the Purchase Price less
                            the Non-Performance Fee (Refund = Purchase Price - Non Performance Fee), and PCS
                            will terminate the sale of such Listed Product and may market and sell such Listed
                            Product to someone else without any liability on PCS's part.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="tc_sub_head">c.&nbsp</span><span class="tc_underline">Title and Risk of
                                Loss.&nbsp</span>Title and risk of loss of the Listed Products shall pass from
                            PCS (or, if Third Party Product, from the Third Party Seller) to you upon tender
                            of such Listed Products to you or to the carrier at the seller's facility ("Point
                            of Purchase"). PCS (or the Third Party Seller, if applicable) will load the Listed
                            Product. You shall pay all freight charges. Upon pick up of the Listed Product,
                            you shall release PCS of any fault, and shall forfeit any rights to claims against
                            PCS associated with the Listed Products or the delivery of the Listed Products purchased
                            by you.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">5.&nbsp Waiver of Detail Receipt.&nbsp</span>You acknowledge
                            that the number of units of the Listed Products set forth in the listing on the
                            Site is merely an estimate and that the final inventory levels of Listed Product
                            from PCS, as applicable, may vary from the number of units set forth in the listing.<span
                                style="text-transform: uppercase; font-weight: bold;">YOU HEREBY WAIVE ANY OPPORTUNITY
                                TO DETAIL THE RECEIPT OF THE LISTED PRODUCTS DUE TO THE TIME, COST AND OTHER CONSIDERATIONS
                                INVOLVED AS WELL AS THE DEEPLY DISCOUNTED PRICING AT WHICH YOU ARE ACQUIRING THE
                                LISTED PRODUCTS.</span>You agree to the following: (a) you assume the risk that
                            you may not receive all of the Listed Products set forth in the listing; (b) you
                            assume the risk that you may receive more Listed Products than set forth in the
                            listing; and (c) you release PCS from any and all claims, demands, actions and causes
                            of action you may have against PCS as a result of the discrepancy between the items
                            set forth in the listing and the items you actually receive from PCS.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">6.&nbsp Limitation of Warranties.&nbsp</span><span style="text-transform: uppercase;
                                font-weight: bold;">PCS MAKES NO WARRANTIES, EXPRESS OR IMPLIED, WITH RESPECT TO
                                THE LISTED PRODUCTS AND OTHER PRODUCTS UNDER THIS AGREEMENT, INCLUDING, WITHOUT
                                LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
                                PURPOSE.</span>
                        </p>
                        <p style =" text-align:justify;">
                            Buyer hereby acknowledges that Buyer used its own standards to determine the quality
                            and value of the Listed Products and, further, that the Listed Products and other
                            products under this Agreement are sold by PCS and purchased by Buyer "AS IS" and
                            "WITH ALL FAULTS". Buyer hereby acknowledges that Products are not covered by any
                            PCS or manufacturer’s warranty. Buyer acknowledges that PCS is not the manufacturer
                            of the Products, unless otherwise stated in writing by PCS.
                        </p>
                        <p style =" text-align:justify;">
                            Buyer may not return any of the Listed Products to PCS or Third Party Sellers for
                            refund, exchange or otherwise. In addition, you hereby acknowledge that Third Party
                            Sellers may fail to perform. PCS does not provide any representations or guarantees
                            that Third Party Sellers will complete a transaction. With respect to Third Party
                            Product, it is expressly understood and agreed that PCS has no duty to you with
                            regard to transactions through the Site other than those duties expressly outlined
                            in this Agreement. PCS does not guarantee any individual Third Party Seller's ability
                            to complete transactions using the Site and makes no representation regarding the
                            creditworthiness or performance of any Third Party Seller. You agree not to hold
                            PCS, its affiliates, officers, directors, employees or agents liable for any loss
                            or damage of any sort incurred as the result of your dealing with any Third Party
                            Seller.
                        </p>
                        <p style =" text-align:justify;">
                            7.&nbsp Intellectual Property and <strong>Privacy.</strong>
                            <br />
                            <span class="tc_sub_head">a.&nbsp</span><strong>Copyright.</strong> This website
                            and all its contents, including but not limited to, text, graphics (collectively
                            the “content”) are all copyrighted. The copyrights for this website are owned by
                            PCS.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="tc_sub_head">b.&nbsp</span><strong>Trademarks.</strong>The PCS name
                            and logo are trademarks of PCS. Users may not use any trademarks belonging to PCS,
                            subsidiaries or affiliated companies without the prior written permission of PCS.
                            Other trademarks or service marks included on PCS’ website belong to the indicated
                            owners, who should be contacted with any questions about the use of those trademarks.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="tc_sub_head">c.&nbsp</span><strong>No License.</strong>Except as otherwise
                            provided above, nothing contained on this website shall be construed to confer by
                            implication, estoppel, or otherwise, any license or right under any patent, trademark
                            or copyright.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="tc_sub_head">d.&nbsp</span><strong>Links.</strong>Although this website
                            may provide links to third party sites, PCS is not responsible for the content of
                            any such site. Links are provided as a convenience to viewers, and they shall not
                            be interpreted as an endorsement, sponsorship or association of any kind.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="tc_sub_head">e.&nbsp</span><strong>Privacy Policy.</strong>PCS is committed
                            to protecting the privacy of the users of its website. All data collected is protected
                            against unauthorized access. By using PCS’ website and providing data, you consent
                            to the collection and use of the data in accordance with this Privacy Policy. In
                            the event PCS changes its Privacy Policy, the changes will be posted on this page.
                            For the public portion of this website, PCS utilizes software which may collect
                            electronic data, including, but not limited to, “cookies” which are small pieces
                            of information that a website stores on a visitor’s web browser to remind the website
                            about the visitor the next time the visitor visits the website, a web beacon, a
                            device ID or unique identifier, a device type, geo-location information, computer
                            and connection information, statistics on page views, traffic to and from the sites,
                            referral URL, ad data, IP address, e-mail address, visitor type (admin/user)and
                            standard web log information. This information is used to help PCS improve, maintain
                            and protect its services and offerings, to develop new services, to inform you about
                            our services, to tailor our offerings to you and to understand what issues our customers
                            may have with the website. For the private portion of the website which is only
                            available to those who establish an account to bid (“Bidders”) and provide (1) a
                            credit card; (2) a Photo ID; and (3) a Reseller’s Certificate, the website collects
                            contact information including name, address, phone number and email address, as
                            well as credit card information,a copy of a photo id and a copy of the person’s
                            Reseller’s Certificate. Such information shall be kept on file and used to process
                            transactions with such Bidders, communicate with such Bidders and to help PCS improve
                            its services and offerings, and to understand what issues its customers may have
                            with the website. PCS may share the information that it collects with affiliates,
                            business partners and businesses that assist PCS in providing this online marketplace
                            oranalyzing the use of the online marketplace and other PCS websites, provided that
                            PCS will not share credit card information and the photo id with anyone outside
                            of PCS except for the limited purpose of processing payments charged to such credit
                            card and will only share the Reseller’s Certificate and information contained therein
                            with governmental organizations who have a right to request such information (e.g.
                            taxing authorities) or if compelled to do so by court or regulatory order. PCS expressly
                            disclaims responsibility for the privacy policies and customer information practices
                            of third party Internet sites linked to our website.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">8.&nbsp Indemnification.&nbsp</span>Buyer will indemnify,
                            defend and hold PCS harmless from and against any and all claims, demands, causes
                            of action, actions, fines, costs, liabilities, and expenses, of any nature, (including,
                            but not limited to court costs, costs of investigation, and attorney’s fees) arising
                            out of or in connection with the following: (a) any sale or resale of the Listed
                            Products, (b) any misrepresentation or breach of this Agreement, (c) damage or injury
                            to persons or property, including but not limited to the Listed Products and other
                            items delivered to the freight carrier or in the possession of Buyer or at storage
                            facilities following Buyer's failure to remove under Paragraph 3, (d) the loss,
                            theft or destruction, whether partial or whole, of the Listed Products and other
                            items, whether delivered to the freight carrier or in the possession of Buyer or
                            at storage facilities following Buyer's failure to remove under Paragraph 3; (e)
                            the reconditioning, refurbishing, resale, sale, or repackaging of the Listed Products
                            and other items under this Agreement; (f) the labeling, advertising or notices placed
                            on the packaging or elsewhere or the lack of notices which should have been placed
                            on the packaging; (g) inaccurate descriptions of the contents of the Listed Products,
                            other items, and the package; (h) the performance or nonperformance of Buyer's obligations
                            under this Agreement, including but not limited to the obligations set forth within
                            Paragraph 3; (i) your dispute with a Third Party Seller or another user of the Site;
                            (j) the failure to delete and remove all information (including Customer Information)
                            or items contained in or on the Listed Products and other items delivered to the
                            freight carrier or sold under this Agreement to Buyer; and (k) any actual or alleged
                            act of commission or omission by Buyer and/or Buyer's successors, assigns, affiliates,
                            parents, subsidiaries and/or divisions, and/or any of their respective affiliates,
                            successors, assigns, officers, directors, employees, agents, representatives and
                            independent contractors. This Paragraph will survive the termination or expiration
                            of this Agreement.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">9.&nbsp Inspection/Audit.&nbsp</span>In the event goods
                            are purchased with a territorial restriction, Buyer agrees that PCS’ authorized
                            representatives reserve the rights to visit Buyer's warehouse or place of business
                            any time in order to monitor compliance with the terms of this agreement.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">10.&nbsp Limitation of Liability.&nbsp</span><span style="text-transform: uppercase;">THE
                                ENTIRE RISK ARISING OUT OF ANY PRODUCTS OR SERVICES OFFERED ON OR IN CONNECTION
                                WITH THE SITE, AND ANY CONTENT, USER CONTENT OR DIGITAL DOWNLOADS REMAINS WITH YOU.
                                IN NO EVENT SHALL PCS OR ITS AFFILIATES BE LIABLE FOR ANY CONSEQUENTIAL, INCIDENTAL,
                                DIRECT, INDIRECT, SPECIAL, PUNITIVE, OR OTHER DAMAGES WHATSOEVER (INCLUDING, WITHOUT
                                LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF
                                BUSINESS INFORMATION, OR OTHER PECUNIARY LOSS) ARISING OUT OF THE AGREEMENT OR THE
                                USE OF OR INABILITY TO USE ANY PRODUCTS, SERVICES, CONTENT, USER CONTENT AND/OR
                                DIGITAL DOWNLOADS, THE PROVISION OF OR FAILURE TO PROVIDE PRODUCTS OR SERVICES,
                                OR FOR ANY INFORMATION, SOFTWARE, PRODUCTS, SERVICES, USER CONTENT AND CONTENT OBTAINED
                                THROUGH THE SITE WHETHER BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR
                                OTHERWISE EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. SOME JURISDICTIONS
                                DO NOT ALLOW THE EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES
                                SO SOME OF THE ABOVE EXCLUSIONS MAY NOT APPLY TO YOU. NOTWITHSTANDING ANYTHING ELSE
                                IN THE AGREEMENT OR POSTED ON THE SITE, THE MAXIMUM LIABILITY THAT PCS SHALL HAVE
                                IS LIMITED TO ANY AMOUNTS ACTUALLY PAID TO PCS BY YOU IN THE IMMEDIATELY PRECEDING
                                THREE (3) MONTHS. YOU ASSUME ALL RISK OF LOSS FOR SHIPPED LISTED PRODUCTS. USER
                                INFORMATION SUCH AS ADDRESS THAT IS INACCURATE OR INCOMPLETE MAY RESULT IN DELAYS
                                THAT SHALL NOT BE THE RESPONSIBILITY OF PCS. YOU ACKNOWLEDGE AND AGREE THAT THE
                                LIMITATIONS OF LIABILITY, DISCLAIMERS OF WARRANTIES AND LIMITED REMEDIES SET FORTH
                                HEREIN REPRESENT AN INSEPARABLE ALLOCATION OF RISK (INCLUDING, WITHOUT LIMITATION,
                                IN THE EVENT OF A TOTAL AND FUNDAMENTAL BREACH OF THIS AGREEMENT) THAT IS AN ESSENTIAL
                                BASIS OF THE BARGAIN BETWEEN THE PARTIES, INCLUDING BUT NOT LIMITED TO THE PRICE
                                THAT YOU PAY TO PURCHASE LISTED PRODUCTS.</span><strong>This provision shall survive
                                    the termination or expiration of this Agreement.</strong>
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">11.&nbsp Confidentiality.&nbsp</span>Buyer will not use
                            or disclose to any individual or entity, including but not limited to any transferee
                            or subsequent purchaser of the goods, the following: (a) that Buyer purchased or
                            received the goods from PCS; (b) the terms and conditions of this Agreement nor
                            the nature or result of any business relationship with PCS; (c) any of PCS's identification
                            marks;and (d) any PCS customer information as provided. Buyer will not use or disclose
                            to any individual or entity, including but not limited to any transferee or subsequent
                            purchaser of the goods:any files or information contained within the Listed Products
                            except that if the Listed Products include, as part of their description, a statement
                            that certain software and data that is found on the device (e.g. manufacturer’s
                            or original equipment reseller’s) may be resold with the device, then Buyer may
                            include such software and data when reselling the device, provided that all other
                            software and data has been removed.This Paragraph will survive the termination or
                            expiration of this Agreement.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">12.&nbsp Relationship of the Parties.&nbsp</span>PCS and
                            Buyer understand and acknowledge that each will perform its duties and obligations
                            under this Agreement as an independent contractor and that this Agreement does not
                            create a joint venture, partnership, employment or agency relationship between them.
                            Buyer understands and acknowledges that it will be solely responsible for all taxes
                            (whether imposed now or at a later date), its and its employees' wages, benefits,
                            unemployment compensation and workers' compensation and all other costs and expenses
                            relating to its employees and applicable to the performance of this Agreement. Buyer
                            represents and warrants to PCS that it has not paid any consideration or made any
                            payments of any kind to any person or entity, including but not limited to PCS employees,
                            officers, and directors, to obtain this Agreement or for the purchase of the items
                            being sold under this Agreement. This provision will survive the termination or
                            expiration of this Agreement.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">13.&nbsp Severability.&nbsp</span>If any provision herein
                            will be deemed or declared unenforceable, invalid or void by a court of competent
                            jurisdiction, the same will not impair any of the other provisions contained herein
                            which will be enforced in accordance with their respective terms.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">14.&nbsp Non-Assignment.&nbsp</span>Prior to the removal
                            of the Listed Products from PCS' facility or facilities, Buyer will not delegate,
                            subcontract, assign or transfer any of its rights, duties, or obligations under
                            this Agreement without the prior express written consent of PCS. In the event that
                            PCS grants any such consent, Buyer will be solely responsible for the conduct of
                            all agents and assignees of Buyer, and the granting of such consent will in no way
                            modify or affect the duties of Buyer to PCS under this Agreement. Subject to the
                            foregoing, this Agreement will be binding upon and inure to the benefits of the
                            parties, and their respective successors and assigns.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">15.&nbsp Governing Law.&nbsp</span>This Agreement shall
                            be governed by and construed in accordance with the laws of the United States of
                            America, which shall be deemed to be the proper law of this Agreement, without regard
                            to its conflicts of law principles. Any dispute arising from, connected with, or
                            relating to this Agreement or any related matters must be resolved before the Superior
                            Court of New Jersey in Morris County, and the parties hereby irrevocably submit
                            to the original and exclusive jurisdiction of those Courts in respect of any such
                            dispute or matter. This Section shall not be construed to limit a party's access
                            to injunction or other equitable or mandatory injunctive relief in any other jurisdiction
                            or affect the rights of a party to enforce a judgment or award outside of New Jersey,
                            including the right to record and enforce a judgment or award in any other jurisdiction.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">16.&nbsp External Network Connectivity Standards & Guidelines.&nbsp</span>In
                            conducting transactions or issuing related correspondence via the Site, Buyer will
                            comply with any other applicable PCS policies or procedures, all of which may be
                            modified from time to time at PCS’s sole discretion.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">17.&nbsp Compliance with Law.&nbsp</span>In the performance
                            of its obligations under this Agreement, Buyer will comply with all applicable laws,
                            regulations, rules, orders, and other requirements, including all applicable laws
                            and regulations relating to the disposal of electronic waste, now or hereafter in
                            effect, of governmental authorities having jurisdiction and any international laws
                            or conventions.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">18.&nbsp Warranty of Authority.&nbsp</span>Each party represents
                            and warrants to the other that it is duly organized, validly existing and in good
                            standing under the laws of the jurisdiction of its organization, and has the requisite
                            power and authority to execute and deliver, and to perform its obligations under,
                            this Agreement. Each party represents and warrants to the other that this Agreement
                            has been duly authorized, executed and delivered by such party and constitutes a
                            valid and binding obligation of such party enforceable against such party according
                            to its terms.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">19.&nbsp Notices. &nbsp</span>All notices, demands and other
                            communications that are required or may be given under this Agreement will be in
                            writing and will be deemed to have been duly given if emailed by PCS or if mailed
                            by either party certified mail, return receipt requested, or by a nationally recognized
                            overnight courier service, receipt confirmed. Notices to you via email will be deemed
                            effective at the time the email is sent. In the case of notices via certified mail
                            or courier service, notices will be deemed effective upon the date of receipt. Notices
                            to you will be addressed to the email or physical address you provided in the User
                            Registration and notices to PCS must be mailed and addressed to PCS Wireless, LLC,
                            Attn: Legal Department, 11 Vreeland Road, Florham Park, NJ 07932, unless either
                            party notifies the other of a change of address or email address, in which case
                            the latest noticed address or email address will be used. If PCS receives a message
                            that your email address is no longer valid or that the email communication could
                            not be delivered, notice shall be deemed to have been provided to you at the time
                            PCS received notice of non-delivery.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">20.&nbsp No Price Collusion. &nbsp</span>You represent and
                            warrant that (1) the overall purchase price offered by you for the Listed Products
                            was arrived at independently without consultation, communication or agreement with
                            any competitor or other potential purchaser or seller of the Listed Products; and
                            (2) no attempt has been made to induce any other person to submit or not to submit
                            a proposal to purchase the Listed Products.
                        </p>
                        <p style =" text-align:justify;">
                            <span class="priv_span">21.&nbsp General. &nbsp</span>The headings contained herein
                            are for the convenience of reference only and are not of substantive effect. This
                            Agreement may be executed in one or more counterparts, each of which will be deemed
                            an original but all of which together will constitute one and the same instrument.
                            This Agreement may be executed by facsimile or other "electronic signature" in a
                            manner agreed upon by the parties hereto. The placing of bids and the submission
                            of the required information (credit card, photo id and Reseller Certificate) confirm
                            your intention to be bound by your electronic signature on this Agreement. Any of
                            the provisions of this Agreement may be waived by the party entitled to the benefit
                            thereof. Neither party will be deemed, by any act or omission, to have waived any
                            of its right or remedies hereunder unless such waiver is in writing and signed by
                            the waiving party, and then only to the extent specifically set forth in such writing.
                            A waiver with reference to one event will not be construed as continuing or as a
                            bar to or waiver of any other right or remedy, or as to a subsequent event. You
                            understand and agree that the sale of Listed Product by PCS or through the Site
                            is not exclusive. PCS may sell the same type of inventory to other bidders. You
                            further understand and agree that PCS is not guaranteeing any minimum quantity,
                            quality or a certain type of product. This Agreement constitutes the entire agreement
                            and understanding between the parties with respect to the subject matter hereof
                            and supersedes all prior agreements and understandings relating to such subject
                            matter, terms of which can be modified only in writing. No amendment to this Agreement
                            will be effected by the acknowledgement or acceptance of a purchase order, invoice,
                            or other forms stipulating additional or different terms. In the event of any conflict
                            between the terms of use on the Site and this Agreement, the terms and conditions
                            of this Agreement shall govern. The expiration or termination of this Agreement
                            will not terminate vested rights of either party from any liabilities or obligations
                            incurred under this Agreement prior to or which by its express terms or by their
                            nature are intended to survive expiration or termination, including but not limited
                            to provisions relating to confidentiality, payment, sanitization obligations, and
                            indemnification. Every auction listed on the Site contains a time extension feature
                            that automatically extends the end time of an auction by an amount that is specific
                            to the auction if a bid is placed in the last few minutes of the auction. These
                            details will be posted on one of the tabs outlining the details of the auction.
                        </p>
                    </div>
                </div>
               
                <div class="clear">
                </div>
           <%-- </div>--%>
       <%-- </div>--%>
   <%-- </div>
--%>