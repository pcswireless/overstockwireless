﻿
Partial Class UserControls_login_ctrl
    Inherits System.Web.UI.UserControl
    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_submit.Click

        If txt_username.Text.Trim() <> "" And txt_password.Text.Trim() <> "" Then

            Dim str As String = "select * from vw_login_users where is_active=1 and username='" & txt_username.Text.Trim() & "' and password='" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "'"
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDataTable(str)

            If dt.Rows.Count > 0 Then
                If dt.Rows(0)("all_ip_allow").ToString = "0" Then
                    Dim al_ip As Integer = SqlHelper.ExecuteScalar("if exists(select ip_id from tbl_ip_addresses where ip_address='" & Request.ServerVariables("REMOTE_ADDR") & "') select 1 else select 0")
                    If al_ip = 0 Then
                        lbl_msg.Text = "Your account is restricted."
                        Exit Sub
                    End If
                End If

                If CBool(dt.Rows(0)("tc_is_accepted")) = False AndAlso CBool(dt.Rows(0)("is_backend")) = False Then
                    Response.Redirect("/default_terms_conditions.aspx?" & "u=" & Security.EncryptionDecryption.EncryptValueFormatted(dt.Rows(0)("user_id")))

                End If

                If chk_remember.Checked Then
                    CommonCode.Create_Cookie_shared("usr", Security.EncryptionDecryption.EncryptValue(txt_username.Text.Trim()))
                    CommonCode.Create_Cookie_shared("pwd", Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()))
                Else
                    CommonCode.Remove_Cookie_shared("usr")
                    CommonCode.Remove_Cookie_shared("pwd")
                    CommonCode.Create_Cookie_shared("usr", "")
                    CommonCode.Create_Cookie_shared("pwd", "")

                End If


                CommonCode.Create_Cookie_shared("user_id", dt.Rows(0)("user_id").ToString)
                CommonCode.Create_Cookie_shared("displayname", IIf(dt.Rows(0)("first_name").ToString <> "", dt.Rows(0)("first_name").ToString & IIf(dt.Rows(0)("last_name").ToString <> "", " " & dt.Rows(0)("last_name").ToString, ""), dt.Rows(0)("last_name").ToString))
                CommonCode.Create_Cookie_shared("username", dt.Rows(0)("username").ToString)
                CommonCode.Create_Cookie_shared("buyer_id", dt.Rows(0)("buyer_id").ToString)
                CommonCode.Create_Cookie_shared("is_backend", dt.Rows(0)("is_backend").ToString)
                CommonCode.Create_Cookie_shared("is_buyer", dt.Rows(0)("is_buyer").ToString)
                CommonCode.Create_Cookie_shared("is_admin", dt.Rows(0)("is_admin").ToString)
                CommonCode.Create_Cookie_shared("is_super_admin", dt.Rows(0)("super_admin").ToString)
                CommonCode.Create_Cookie_shared("profile_code", dt.Rows(0)("profile_code").ToString)
                str = "insert into tbl_sec_login_log (user_id,buyer_id,buyer_user_id,login_date,ip_address,browser_info,log_type) values (" & dt.Rows(0)("user_id") & "," & dt.Rows(0)("buyer_id").ToString & "," & dt.Rows(0)("user_id").ToString & ",getdate(),'" & HttpContext.Current.Request.UserHostAddress & "','" & Request.Browser.Browser.ToString & " - " & Request.Browser.Version.ToString & " - " & Request.Browser.Platform.ToString & "','Login')"
                SqlHelper.ExecuteNonQuery(str)

                Try
                    If Request.QueryString.Get("bi") <> Nothing Then
                        Dim buyer_id As Integer = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("bi"))

                        Dim y_mail As Int16 = SqlHelper.ExecuteScalar("if exists(select buyer_id from tbl_reg_buyers where status_id<>2 and buyer_id=" & buyer_id & ") begin update tbl_reg_buyers set status_id=2,approval_status='Approved' where buyer_id=" & buyer_id & "; update tbl_reg_buyer_users set is_active=1 where is_admin=1 and buyer_id=" & buyer_id & "; select 1 end else select 0")
                        If y_mail = 1 Then
                            Dim objEmail As New Email()
                            objEmail.send_buyer_approval_email(buyer_id)
                            objEmail = Nothing
                            Response.Redirect("/register_confirm.aspx?rt=A")

                        Else
                            Response.Redirect("/register_confirm.aspx?rt=E")

                        End If

                    End If
                Catch ex As Exception

                End Try

                Dim redirect As String = ""
                If Request.QueryString.Get("q") <> Nothing Then
                    redirect = "&q=" & Request.QueryString.Get("q")
                End If

                If dt.Rows(0)("is_backend").ToString = "1" Then
                    Response.Redirect("/Backend_Home.aspx?t=1" & redirect)
                Else
                    If Request.QueryString.Get("a") <> Nothing Then
                        Response.Redirect("/Auction_Details.aspx?t=1&a=" & Request.QueryString.Get("a"))
                    Else
                        If Request.Url.ToString().ToLower().IndexOf("sign-up.html") > 0 Or Request.Url.ToString().ToLower().IndexOf("signup.aspx") > 0 Then
                            Response.Redirect("/Auction_Listing.aspx?t=1")
                        Else
                            Response.Redirect(Request.Url.ToString())
                        End If

                    End If
                End If
            Else
                lbl_msg.Text = "In-Active/Invalid User Name or Password."
            End If
        Else
            lbl_msg.Text = "Please Enter User Name and Password."
        End If

    End Sub
    Private Function is_active_user() As Boolean
        Dim i As Integer = 0
        Dim is_active As Boolean = False
        Dim str As String = "if exists(select user_id from tbl_sec_users where is_active=1 and username='" & txt_username.Text.Trim() & "')Begin select 1 End Else Begin select 0 End"
        If Not txt_username.Text.Trim().ToUpper() = "ADMIN" Then
            i = SqlHelper.ExecuteScalar(str)
            If i = 1 Then
                is_active = True
            Else
                is_active = False
            End If
        Else
            is_active = True
        End If
        Return is_active
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Response.Write(Security.EncryptionDecryption.DecryptValue(SqlHelper.ExecuteScalar("Select password from tbl_sec_users")))
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            If Request.QueryString.Get("bi") <> Nothing Then
                Dim buyer_id As Integer = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("bi"))
                Dim y_mail As Int16 = SqlHelper.ExecuteScalar("if exists(select buyer_id from tbl_reg_buyers where status_id<>2 and buyer_id=" & buyer_id & ") begin update tbl_reg_buyers set status_id=2,approval_status='Approved' where buyer_id=" & buyer_id & "; update tbl_reg_buyer_users set is_active=1 where is_admin=1 and buyer_id=" & buyer_id & "; select 1 end else select 0")
                If y_mail = 1 Then
                    Dim objEmail As New Email()
                    objEmail.send_buyer_approval_email(buyer_id)
                    Response.Redirect("register_confirm.aspx?rt=A")
                    'Response.Write("<script language='javascript'>top.location.href = '/register_confirm.aspx?rt=A';</script>")
                Else
                    Response.Redirect("/register_confirm.aspx?rt=E")
                    'Response.Write("<script language='javascript'>top.location.href = '/register_confirm.aspx?rt=E';</script>")
                End If
            End If
        End If
        If Page.IsPostBack = False Then
            lbl_msg.Text = ""
            If Not String.IsNullOrEmpty(CommonCode.Fetch_Cookie_Shared("usr")) And Not String.IsNullOrEmpty(CommonCode.Fetch_Cookie_Shared("pwd")) Then
                txt_username.Text = Security.EncryptionDecryption.DecryptValue(CommonCode.Fetch_Cookie_Shared("usr"))
                txt_password.Text = Security.EncryptionDecryption.DecryptValue(CommonCode.Fetch_Cookie_Shared("pwd"))
                chk_remember.Checked = True
                btn_submit.Focus()
            Else
                txt_username.Text = ""
                txt_password.Text = ""
                chk_remember.Checked = False
                txt_username.Focus()
            End If
            If Not String.IsNullOrEmpty(Request.QueryString.Get("login")) Then
                ModalPopup_Credit.Show()
                txt_username.Focus()
            End If
        End If


        'Response.Write(HttpContext.Current.Request.Url.ToString.Remove(HttpContext.Current.Request.Url.ToString.IndexOf(HttpContext.Current.Request.Url.Port.ToString) + HttpContext.Current.Request.Url.Port.ToString.Length))
    End Sub

    Protected Sub but_send_Click(sender As Object, e As System.EventArgs) Handles but_send.Click
        lbl_msg.Text = ""
        If Page.IsValid Then
            Dim pwd As String = Security.EncryptionDecryption.EncryptValue(Guid.NewGuid().ToString.Remove(8))
           
            Dim _right As String = SqlHelper.ExecuteScalar("if exists (select username from tbl_reg_buyer_users where is_active=1 and (email='" & txt_email.Text & "' or username='" & txt_email.Text & "') ) BEGIN update tbl_reg_buyer_users set password='" & pwd & "' where is_active=1 and (email='" & txt_email.Text & "' or username='" & txt_email.Text & "'); select (email+convert(varchar,char(241))+username+convert(varchar,char(241))+'buyer') as email from tbl_reg_buyer_users where is_active=1 and (email='" & txt_email.Text & "' or username='" & txt_email.Text & "') END else if exists (select username from tbl_sec_users where is_active=1 and (email='" & txt_email.Text & "' or username='" & txt_email.Text & "')) BEGIN update tbl_sec_users set password='" & pwd & "' where is_active=1 and (email='" & txt_email.Text & "' or username='" & txt_email.Text & "'); select (email+convert(varchar,char(241))+username+convert(varchar,char(241))+'admin') as email from tbl_sec_users where is_active=1 and (email='" & txt_email.Text & "' or username='" & txt_email.Text & "') END else select 'break'") ' SqlHelper.ExecuteScalar("declare @eml_usr varchar(250) if exists (select @eml_usr=(email+'br~eak'+username) from tbl_reg_buyer_users where is_active=1 and (email='" & txt_email.Text & "' or username='" & txt_email.Text & "') ) BEGIN update tbl_reg_buyer_users set password='" & pwd & "' where is_active=1 and (email='" & txt_email.Text & "' or username='" & txt_email.Text & "') select @eml_usr END else if exists (select @eml_usr=(email+'br~eak'+username) from tbl_sec_users where is_active=1 and (email='" & txt_email.Text & "' or username='" & txt_email.Text & "') ) BEGIN update tbl_sec_users set password='" & pwd & "' where is_active=1 and (email='" & txt_email.Text & "' or username='" & txt_email.Text & "') select @eml_usr END else select 'break'"
            ' Exit Sub
            If _right <> "break" Then
                Dim break As String() = _right.Split("ñ")
                Dim obj As New Email
                Dim name As String
                If break(2) = "buyer" Then
                    name = SqlHelper.ExecuteScalar("select first_name from tbl_reg_buyer_users where is_active=1 and (email='" & txt_email.Text & "' or username='" & txt_email.Text & "')")
                Else
                    name = SqlHelper.ExecuteScalar("select first_name from tbl_sec_users where is_active=1 and (email='" & txt_email.Text & "' or username='" & txt_email.Text & "')")
                End If
                obj.Send_Forgot_Pwd(Security.EncryptionDecryption.DecryptValue(pwd), break(0), name, break(1))
                obj = Nothing
                Response.Redirect("/register_confirm.aspx?rt=F")
                'Response.Write("<script language='javascript'>top.location.href = '/register_confirm.aspx?rt=F';</script>")
            Else
                lbl_msg.Text = "In-Active/Invalid User Name or Email."
            End If
        Else
            lbl_msg.Text = "Please Enter User Name or Email."
        End If
    End Sub

    Protected Sub lnk_forget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_forget.Click
        If pnl_forget.Visible = True Then
            pnl_forget.Visible = False
        Else
            pnl_forget.Visible = True
        End If
    End Sub
End Class
