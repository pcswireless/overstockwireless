﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bidhistory_control.ascx.vb"
    Inherits="bidhistory_control" %>
<asp:Repeater ID="rep_bid_history" runat="server">
    <ItemTemplate>
        <div style="clear:both;">
            <div style="float: left; width: 180px; padding: 5px 0px 5px 10px;">
                <b>
                    <%# Eval("bidder")%>
                </b>
                <br />
                <span class="auctionSubTitle">
                    <%# Eval("email")%></span>
            </div>
            <div style="float: left; width: 100px; padding: 5px 0px 5px 10px; color: #006FD5;">
                <b>
                    <%# "$" & FormatNumber(Eval("bid_amount"), 2)%></b>
            </div>
        </div>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <div style="background-color: #F5F5F5; overflow: hidden;clear:both;">
            <div style="float: left; width: 180px; padding: 5px 0px 5px 10px;">
                <b>
                    <%# Eval("bidder")%>
                </b>
                <br />
                <span class="auctionSubTitle">
                    <%# Eval("email")%></span>
            </div>
            <div style="float: left; width: 100px; padding: 5px 0px 5px 10px; color: #006FD5;">
                <b>
                    <%# "$" & FormatNumber(Eval("bid_amount"), 2)%></b>
            </div>
        </div>
    </AlternatingItemTemplate>
</asp:Repeater>
<asp:Literal ID="empty_data" runat="server" Text="No bid submitted for this auction."></asp:Literal>
