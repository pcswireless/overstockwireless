<%@ Control Language="VB" AutoEventWireup="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<style type="text/css">
    .modalBackground
    {
        background-color: #FFFFFF;
        filter: alpha(opacity=0);
        opacity: 0.00;
    }
    .updateProgress
    {
        border-width: 1px;
        border-style: solid;
        background-color: #FFFFFF;
        position: absolute;
        width: 150px;
        height: 50px;
    }
    .updateProgressMessage
    {
        margin: 3px;
        font-family: Trebuchet MS;
        font-size: small;
        vertical-align: middle;
    }
</style>
<script type="text/javascript" language="javascript">

    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

    function beginRequest(sender, args) {
        // show the popup
        $find('<%=mdlPopup.ClientID %>').show();
    }

    function endRequest(sender, args) {
        //  hide the popup
        $find('<%=mdlPopup.ClientID %>').hide();
    }

</script>
<ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" TargetControlID="pnlPopup"
    PopupControlID="pnlPopup" BackgroundCssClass="modalBackground" />
<asp:Panel ID="pnlPopup" runat="server" CssClass="updateProgress" Style="display: none">
    <div align="center" style="margin-top: 13px;">
        <img src="/images/loading.gif" border="0" alt="Wait" />
    </div>
</asp:Panel>
