﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Invoice.ascx.vb" Inherits="UserControls_Invoice" %>
<div class="confirmHeading">
    ORDER RECEIPT
</div>
<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="invoiceAddress">
            <span class="invoiceCompanyName">PCS Wireless LLC</span><br />
            11 Vreeland Road<br />
            Florham Park NJ, 07932<br />
            Phone: (973) 850-7400 Fax: (973) 301-0975<br />
            www.pccwireless.com<br />
            sales@pccwireless.com
        </td>
        <td align="right" valign="top">
            <img src="/Images/logo_listing.jpg" alt="PCS B2B Logo" border="0" />
            <br />
            <br />
            <table>
                <tr>
                    <td class="invNumber">
                        Confirmation No:
                    </td>
                    <td class="invNumber">
                        <asp:Literal ID="lit_order_no" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td class="invNumber">
                        Date:
                    </td>
                    <td class="invNumber">
                        <asp:Literal ID="lit_date" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div class="invoiceBillTo">
    <strong>Bill To : </strong>
    <br />
    <asp:Literal ID="lit_bill_to" runat="server"></asp:Literal>
</div>
<table cellpadding="10" cellspacing="1" width="100%" border="0" style="background-color: #E3E3E3;">
    <tr>
        <td align="left" class="invCell" style="padding-left: 20px; width: 55%;">
            <b>PRODUCT DESCRIPTION</b>
        </td>
        <td class="invCell" align="center" style="width: 15%;">
            <b>QTY.</b>
        </td>
        <td class="invCell" align="center" style="width: 15%;">
            <b>PRICE</b>
        </td>
        <td class="invCell" align="center" style="width: 15%;">
            <b>TOTAL</b>
        </td>
    </tr>
    <asp:Repeater ID="rpt_device" runat="server">
        <ItemTemplate>
            <tr>
                <td align="left" class="invCell" valign="top" style="padding-left: 20px;">
                    <%# Eval("name")%>
                </td>
                <td class="invCell" align="center">
                    <%# Eval("quantity")%>
                </td>
                <td class="invCell" align="center">
                    <%# FormatCurrency(Eval("unit_price"), 2)%>
                </td>
                <td class="invCell" align="center">
                    <%# FormatCurrency(Eval("total_price"), 2)%>
                </td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
    <tr>
        <td class="invCell" colspan="2" rowspan="3" align="center"><b>Payment Method</b><br />
                            <asp:Label ID="lbl_payment_method" runat="server" CssClass="paymentMethod"></asp:Label>
        </td>
        <td class="invCell"  align="center"><b>Sub Total</b>
        </td>
        <td class="invCell" align="center"><b><asp:Literal ID="lit_total" runat="server"></asp:Literal></b>
        </td>
    </tr>
    <tr>
        
        <td class="invCell" align="center"><b>Estimated Shipping</b>
        </td>
        <td class="invCell" align="center"><b><asp:Literal ID="lit_shipping" runat="server"></asp:Literal></b>
        </td>
    </tr>
    <tr>
        <td class="invCell" align="center" style="font-size:14px;color:Black;"><b>Total</b>
        </td>
        <td class="invCell" align="center" style="font-size:14px;color:Black;"><b><asp:Literal ID="lit_payble" runat="server"></asp:Literal></b>
        </td>
    </tr>
    
</table>
