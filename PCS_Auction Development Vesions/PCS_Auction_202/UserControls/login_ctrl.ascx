﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="login_ctrl.ascx.vb" Inherits="UserControls_login_ctrl" %>
<%@ Register TagPrefix="ajax_load" TagName="Loader" Src="~/UserControls/ajaxLoader.ascx" %>
<style type="text/css">
    h1{
	font-family: 'UniSansRegular',"Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 22px;
	line-height: 22px; 
	color: #008db8;
	margin: 0;
	padding: 0 0 2px 0;
	font-weight: normal;
}
p 
{
  padding:7px 0px;
  margin-top:3px;
  font-size:13px;  }
h4 
{
  padding:10px 0px;
  font-size:13px;  }  
    
    .txt_smallest
    {
        width: 45px;
        border: 1px solid #CBCBCB;
        padding: 2px;
    }
    .txt_small
    {
        width: 44.14pt;
        border: 1px solid #CBCBCB;
        padding: 2px;
    }
    .txt_mid
    {
        width: 120px;
        border: 1px solid #CBCBCB;
        padding: 2px;
    }
    .txt_big
    {
        width: 210px;
        border: 1px solid #CBCBCB;
        padding: 2px;
    }
    .dd_small
    {
        width: 55px;
        border: 1px solid #CBCBCB;
        padding: 2px;
    }
    .dd_mid
    {
        width: 70px;
        border: 1px solid #CBCBCB;
        padding: 2px;
    }
    .dd_big
    {
        width: 216px;
        border: 1px solid #CBCBCB;
        padding: 2px;
    }
    .sp_err
    {
        color: #E30D7A;
    }
    .valid_err
    {
        color: Red;
        font-size: 10px;
    }
    .err_msg
    {
        color: Red;
        font-weight: bold;
        padding: 5px 0 5px 0;
        font-size: 11PX;
    }
    .modalBackground_Main
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    
    .modalPopup_Main
    {
        background-color: #AFAFAF;
        border-width: 3px;
        border-style: solid;
        border-color: Gray;
        padding: 3px;
        color: #626262; /*width:685px;*/
    }
    
    .controll_table
    {
        width: 685px;
        font-weight: bold;
        font-size: 11px;
        border-collapse: collapse;
        border-spacing: 0;
    }
    .controll_table th
    {
        line-height: normal;
        border-collapse: collapse;
        border-spacing: 0;
    }
    
    .controll_td
    {
        padding: 2px;
        line-height: normal;
        border-collapse: collapse;
        border-spacing: 0;
    }

    @media only screen and (max-width : 320px)
{
    .smartphones_login {
        width:95%;

    }
}

</style>
<script type="text/javascript">

    function Modal_Dialog() {
        var modalDialog = $find('ModalExtender_Credit');
        if (modalDialog != null) {
            modalDialog.show();
            document.getElementById('<%=txt_username.ClientID %>').focus();
        }
        return false;
    }

</script>
<asp:Button ID="but_null" runat="server" Text="" Style='display: none' />
<ajaxToolkit:ModalPopupExtender ID="ModalPopup_Credit" runat="server" BackgroundCssClass="modalBackground_Main"
    CancelControlID="img_close" DropShadow="true" BehaviorID="ModalExtender_Credit"
    TargetControlID="but_null" PopupControlID="pnl_credit_main">
</ajaxToolkit:ModalPopupExtender>
<div id="pnl_credit_main" runat="server" class="modalPopup_Main" style="padding: 10px;
    text-align: left; display: none; width:100%">
    <div class ="smartphones_login" style="background-color: White; padding: 0 0 0 7px;">
        <div style="text-align: right; padding: 0 0 0 5px;">
            <a href="#" id="img_close" runat="server" title="Close" style="text-decoration: none;">
                <img alt="Close" style="border: none;" src="/images/close_icon.jpg" /></a></div>
        <asp:UpdatePanel runat="server" ID="upd_pnl_credit">
            <ContentTemplate>
                <ajax_load:Loader ID="UC_AjaxLoader" runat="server"></ajax_load:Loader>
                <div class="products" style="max-width: 600px; padding: 20px;">
                    <div class="seprator">
                    </div>
                    <h1>
                        Member login</h1>
                    <p>
                        Only companies who have joined the PCS Wireless online auction and become members
                        may bid on auctions or purchase products on the site.
                    </p>
                    <p>
                        If you are already a member, please sign in with your user name and password below.
                    </p>
                    <h4 style="display: inline;">
                        <span style="color: black;">Want to become a member?</span> It's FREE. <a href="/sign-up.html"
                            style="color: #10AAEA; font-weight: bold;">Join now</a></h4>
                    <div style="padding-top: 10px;">
                        <table style="font-size: 13px; text-align: left;" cellpadding="2"
                            cellspacing="2" width="300" border="0">
                           
                                <tr style="vertical-align: middle;">
                                    <td align="left">
                                        <asp:Panel ID="pnl_login" runat="server" DefaultButton="btn_submit">
                                            <table >
                                                
                                                    <tr>
                                                        <td valign="top" style="padding-top: 10px;">
                                                            <label class="EditingFormLabel">
                                                                Member ID:</label>
                                                        </td>
                                                        <td style="padding-top: 10px;padding-left: 10px;">
                                                            <div class="EditingFormControlNestedControl">
                                                                <asp:TextBox ID="txt_username" runat="server" Width="145" CssClass="TextBoxField"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="req_name" ControlToValidate="txt_username" runat="server"
                                                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" style="padding-top: 10px;">
                                                            <label class="EditingFormLabel">
                                                                Password:</label>
                                                        </td>
                                                        <td style="padding-top: 10px;padding-left: 10px;">
                                                            <div class="EditingFormControlNestedControl">
                                                                <asp:TextBox ID="txt_password" runat="server" Width="145" TextMode="Password" CssClass="TextBoxField"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txt_password"
                                                                    runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            &nbsp;
                                                        </td>
                                                        <td style="padding-top: 10px;">
                                                            <div class="EditingFormControlNestedControl">
                                                                <asp:CheckBox ID="chk_remember" runat="server" Text=" Remember me" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            &nbsp;
                                                        </td>
                                                        <td style="padding-top: 10px;">
                                                            <asp:Button ID="btn_submit" runat="server" Text="Log on" />
                                                        </td>
                                                    </tr>
                                               
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 10px;">
                                        <asp:Label ID="lbl_msg" runat="server" Text="" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 10px;">
                                        <asp:LinkButton ID="lnk_forget" runat="server" Text="Forgot user name/password?"
                                            CausesValidation="false"></asp:LinkButton>
                                    </td>
                                </tr>
                                <asp:Panel ID="pnl_forget" runat="server" Visible="false">
                                    <tr>
                                        <td style="padding-top: 10px;">
                                            Your user name or e-mail:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 10px;">
                                            <asp:TextBox ID="txt_email" runat="server" MaxLength="100"></asp:TextBox>&nbsp;&nbsp;
                                            <asp:Button ID="but_send" runat="server" ValidationGroup="val_login_info" Text="Send password" />
                                            <asp:RequiredFieldValidator ID="rfv_email" runat="server" ControlToValidate="txt_email"
                                                ValidationGroup="val_login_info" Display="Dynamic" ErrorMessage="<br />Email or User name required"
                                                CssClass="error"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <tr>
                                    <td style="padding-top: 10px;">
                                        <a href="/sign-up.html" style="color: #10AAEA; font-weight: bold;">Join now</a>
                                    </td>
                                </tr>
                            
                        </table>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
