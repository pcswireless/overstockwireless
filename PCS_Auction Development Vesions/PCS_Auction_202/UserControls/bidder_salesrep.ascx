﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bidder_salesrep.ascx.vb"
    Inherits="UserControls_bidder_salesrep" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:ScriptManager runat="server" ID="sc_1">
</asp:ScriptManager>
<div class="slesbx">
    <a href="javascript:void(0);" id="contact-sales-rep">Contact Your Sales Rep</a>
    <div id="sales-rep-profile" style="margin-bottom: 0px; margin-right:0px;">
        <h2 style="color:#014782; font-size:19px!important;">
            "Please feel free to contact me with your questions and concerns."</h2>
        <asp:UpdatePanel ID="upd_contact_rp" runat="server">
            <ContentTemplate>
                <div class="profile">
                    <div class="fleft">
                        <h2>
                            <asp:Literal runat="server" ID="ltr_name1" Text="Sales Rep"></asp:Literal>:</h2>
                        <div class="fleft margin5" style=" margin-left:0px;">
                            <asp:Image ID="img_user" Width="69" Height="63" ImageUrl="~/Images/buyer_icon.gif"
                                runat="server"></asp:Image>
                        </div>
                        <div class="fright margin5" style="margin-top: 0px;">
                            <p>
                                Phone 1:
                                <asp:Literal runat="server" ID="ltr_phone1" Text="000-000-0000"></asp:Literal></p>
                            <p>
                                Phone 2:
                                <asp:Literal runat="server" ID="ltr_phone2" Text="000-000-0000"></asp:Literal></p>
                            <p>
                                Email:
                                <asp:Literal runat="server" ID="ltr_email" Text="info@companyname.com"></asp:Literal></p>
                            <div id='dv_qukcont_confirm' style="width: 185px; font-size: 0.8em; padding-top: 50px;
                                line-height: 11px; clear: both; overflow: hidden; height:auto;">
                                <asp:Label ID="rp_message" runat="server" Height="30" Text="Thanks for your question. Someone will get back to you within 24 hours."
                                    ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="fright quickContact">
                        <h2>
                            Quick Contact</h2>
                        <asp:HiddenField ID="hid_sales_rep_id" runat="server" Value="0" />
                        <p>
                            To:
                            <asp:Literal runat="server" Text="Sales Rep" ID="ltr_name2"></asp:Literal>
                        </p>
                        <p>
                            <asp:TextBox ID="txt_subject" runat="server" CssClass="RPTextBox_S" Style="border: 1px solid #E3E3E3;
                                box-shadow: 0 2px 3px 1px #F7F7F7 inset; color: #5F5F5F; width: 185px;"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_subject"
                                ValidationGroup="_sales_contact" Display="Static" ErrorMessage="*" CssClass="error"></asp:RequiredFieldValidator>
                            <ajax:TextBoxWatermarkExtender ID="TBWE1" runat="server" TargetControlID="txt_subject"
                                WatermarkText="Subject" WatermarkCssClass="WaterMarkedTextBox" />
                        </p>
                        <p style="display: block; padding-bottom: 0px;">
                            <asp:TextBox ID="question" runat="server" Style="border: 1px solid #E3E3E3; box-shadow: 0 2px 3px 1px #F7F7F7 inset;
                                color: #5F5F5F; width: 185px; padding-left: 7px; resize: vertical;" CssClass="RPTextBox_S"
                                TextMode="MultiLine" Height="50" Rows="3" Columns="22"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="question"
                                ValidationGroup="_sales_contact" Display="Static" ErrorMessage="*" CssClass="error"></asp:RequiredFieldValidator>
                            <ajax:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="question"
                                WatermarkText="Your Question" WatermarkCssClass="WaterMarkedTextBox" />
                        </p>
                        <p style="display: block; padding-top: 0px;" id='dv_qukcont_ask'>
                            <asp:Button ID="btnSubmit" Width="80" Height="30" runat="server" OnClientClick="question_wait('_sales_contact','qukcont')"
                                ValidationGroup="_sales_contact" CssClass="profile" Text="Submit" />
                        </p>
                        <p style="display: none; font-size: 11px;" id='dv_qukcont_msg'>
                            <span style="color: Red; width: 80; height: 30;">Please Wait...</span>
                        </p>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="clear">
        </div>
        <h2 class="fright txta2" style="padding: 0!important;">
            Contact Your Sales Rep
        </h2>
        <div class="clear">
        </div>
        
        <div class="close" id="sales-popup-close">
        </div>
        
    </div>
    <!-- 3sales-rep-profile -->
</div>
<!--salesbx -->
<div class="clear">
</div>
<script type="text/javascript">
    $('#contact-sales-rep').click(function () { $('#sales-rep-profile').toggle('slow', 'easeInQuad'); });
    $('#sales-popup-close').click(function () { $('#sales-rep-profile').toggle('slow', 'easeInQuad'); });
</script>
