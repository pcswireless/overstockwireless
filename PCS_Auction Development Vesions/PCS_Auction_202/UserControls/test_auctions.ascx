﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="test_auctions.ascx.vb"
    Inherits="UserControls_test_auctions" %>
    <script type="text/javascript">
        function calculate_rank(_ranks) {
            //alert(_ranks);
            var auction_ranks = new Array();
            auction_ranks = _ranks.split("~");
            if (auction_ranks.length > 0) {
                //alert(auction_ranks.length);
                var auction_ids = new Array();
                for (var i = 0; auction_ranks.length; i++) {
                    if (auction_ranks[i]) {
                        auction_ids = auction_ranks[i].split("#");
                        if (auction_ids.length > 0) {
                            if (document.getElementById('auc_' + auction_ids[0]))
                                document.getElementById('auc_' + auction_ids[0]).innerHTML = auction_ids[1];
                        }
                    }
                }


            }
            //var auction_ranks = _ranks.split("~")
            //alert(_ranks);
        }
    </script>
<asp:Panel ID="pn_auction_content" runat="server">
                            <iframe id="iframe_auctions" runat="server" style="width: 100%; overflow: hidden;display:none;" height="50;" scrolling="no"
                                frameborder="0" >
                            </iframe>
    <asp:Repeater ID="rpt_auctions" runat="server">
        <ItemTemplate>
            <asp:HiddenField ID="hid_auction_id" runat="server" Value='<%# Eval("auction_id")%>' />
            <div class='box <%# IIf(((Container.ItemIndex + 2) Mod 3) = 0, "box2", "box1")%>'>
                <div class="proimg">
                    <a id="a_img_auction" style="text-decoration: none; cursor: pointer" runat="server">
                       <asp:Image ID="img_auction_image" runat="server" CssClass="aucImage" />
                    </a>
                </div>
                <!--proimg -->
                <div class="txtbx">
                    <h1>
                        <asp:Literal runat="server" ID="ltr_title"></asp:Literal></h1>
                    <div class="clear">
                    </div>
                    <div class="MyBidstatus">
                        <span id='auc_<%# Eval("auction_id")%>'></span>
                    </div>
                    <div class="clear">
                    </div>
                    <asp:Literal ID="ltr_button_auction" runat="server" Text="More Info"></asp:Literal><!--btn -->
                    <div class="timetxt">
                        <span class="txt1">
                            <asp:Literal ID="ltr_auction_status" runat="server" Text="Starts in"></asp:Literal>
                        </span>
                        <div class="clear">
                        </div>
                        <span class="txt2">
                            <iframe id="iframe_time" style="width: 100%; overflow: hidden;" height="50;" scrolling="no"
                                frameborder="0" src='/frame_auction_time.aspx?is_frontend=1&i=<%#Eval("auction_id")%>&refresh=0'>
                            </iframe>
                        </span>
                        <div class="clear">
                        </div>
                    </div>
                    <!--timetext -->
                    <div class="clear">
                    </div>
                </div>
                <!--txtbx -->
            </div>
            <%# IIf(((Container.ItemIndex + 1) Mod 3) = 0, "<div class='clr'></div>", "")%>
        </ItemTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Panel ID="pnl_noItem" runat="server" Visible="false">
    <div style="border: 1px solid #10AAEA; background: url('/Images/fend/grdnt_template.jpg') repeat-y 100%;
        height: 800px; padding-top: 25px; text-align: center;">
        <div style="font-family: dinbold; font-size: 18;">
            <asp:Literal ID="lit_no_item" runat="server"></asp:Literal></div>
    </div>
    <br />
</asp:Panel>
<div class="clear">
</div>

