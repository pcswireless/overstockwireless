﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MyBids.ascx.vb" Inherits="UserControls_MyBids" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
    left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
    <img id="Image8" src="/images/img_loading.gif" alt="" />
</telerik:RadAjaxLoadingPanel>
<table cellpadding="0" cellspacing="0" width="100%" border="0">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
        CssClass="TabGrid">
        <tr>
            <td valign="top" align="center" style="padding: 5px; width: 150px;">
                <div class="Imgborder">
                    <asp:Image ID="img_image" runat="server" CssClass="aucImage" />
                </div>
                <br />
                <%--<asp:ImageButton ID="img_hide_now" runat="server" Visible="false" CausesValidation="false" />--%>
                <div style="padding-top: 8px; padding-left: 16px;">
                    <asp:Panel ID="Panel1_Header1" runat="server" CssClass="pnlTabItemHeader">
                        <asp:Image ID="pnl_img1" runat="server" ImageAlign="left" ImageUrl="/Images/fend/plus.png"
                            CssClass="panelimage" />
                    </asp:Panel>
                    <ajax:CollapsiblePanelExtender ID="cpe1" BehaviorID="cpe1" runat="server" Enabled="True"
                        TargetControlID="Panel1_Content1" CollapseControlID="Panel1_Header1" ExpandControlID="Panel1_Header1"
                        Collapsed="true" ImageControlID="pnl_img1" CollapsedImage="/Images/fend/plus.png"
                        ExpandedImage="/Images/fend/minus.png" CollapsedText="More Details" ExpandedText="Hide Details">
                    </ajax:CollapsiblePanelExtender>
                </div>
            </td>
            <td valign="top" style="padding: 5px;">
                <div style="height: 140px; width: 100%; display: block;">
                    <div class="auctionTitle">
                        <asp:Literal ID="lbl_title" runat="server" />
                    </div>
                    <asp:Label ID="lbl_sub_title" runat="server" CssClass="auctionSubTitle" />
                    <div class="auctionSubTitle">
                        <asp:Label Font-Size="11px" runat="server" ID="lit_sub_auction"></asp:Label>
                        # :
                        <asp:Label Font-Size="11px" ID="lbl_auction_code" runat="server" />
                    </div>
                    <div class="ShortDesc">
                        <asp:Literal ID="lbl_short_desc" runat="server" />
                    </div>
                    <asp:Panel ID="pnl_proxy" runat="server">
                        <table cellpadding="0" cellspacing="1" style="color: #525252;">
                            <tr>
                                <td>
                                    <b>
                                        <asp:Literal runat="server" ID="lit_sub_bid_start"></asp:Literal>
                                        :
                                        <asp:Literal ID="lit_start_price" runat="server" />&nbsp;(<asp:Literal ID="lit_increment"
                                            runat="server" />)</b>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="pnl_now" ForeColor="#525252" runat="server">
                        <b>
                            <asp:Literal runat="server" ID="lit_sub_buy_now"></asp:Literal>
                            :
                            <asp:Literal ID="lit_buy_now_price" runat="server" /></b>
                    </asp:Panel>
                    <asp:Panel ID="pnl_quote" runat="server">
                        <asp:Literal ID="lit_quote_msg" runat="server" />
                    </asp:Panel>
                </div>
                <%--<div style="float: right; text-align: right;">
                    <table cellpadding="2" style="text-align: right;" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <asp:ImageButton ID="btn_buy_now" runat="server" ImageUrl="/Images/fend/4.png" CausesValidation="false"
                                    Visible="false" />
                            </td>
                            <td>
                                <asp:ImageButton ID="btn_send_private_offer" runat="server" ImageUrl="/Images/fend/4.png"
                                    CausesValidation="false" Visible="false" />
                            </td>
                            <td>
                                <asp:ImageButton ID="btn_send_partial_offer" runat="server" ImageUrl="/Images/fend/4.png"
                                    CausesValidation="false" Visible="false" />
                            </td>
                        </tr>
                    </table>
                </div>--%>
            </td>
            <td valign="top" style="padding: 5px 5px 0 5px; width: 210px;">
                <asp:Literal ID="lit_price" Visible="false" runat="server" />
                <div style="text-align: center; height: 215px; display: block; overflow: hidden;">
                    <table cellpadding="4" cellspacing="0" width="100%" style="background-color: #F4F3F0;
                        padding-top: 3px; height: 150px;">
                        <tr>
                            <td valign="top">
                                <div class="Auc_status">
                                    <asp:Literal ID="ltrl_status" runat="server" />
                                </div>
                                <div>
                                    <asp:Panel ID="pnl_trad_proxy" runat="server">
                                        <div style="text-align: center; display: block;">
                                            <asp:Literal ID="ltrl_max_bid_amount" runat="server" />
                                        </div>
                                        <div class="bidderprice" style="padding-top: 10px;">
                                            <asp:Literal ID="lit_biddeer_price" runat="server" />
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnl_buyer_quote" runat="server">
                                        <br />
                                        <div class="bid_date">
                                            <asp:Literal ID="lit_bid_date_quote" runat="server"></asp:Literal>
                                        </div>
                                        <div style="text-align: left; height: 75px; padding: 3px; overflow-y: auto;">
                                            <asp:Literal ID="lit_buyer_quote_msg" runat="server" />
                                        </div>
                                        <div class="bidderprice" style="text-align: left; padding: 5px;">
                                            <asp:Literal ID="lit_quote_approval_status" runat="server"></asp:Literal>
                                        </div>
                                        <div style="text-align: right; padding-bottom: 5px; padding-right: 8px;">
                                            <asp:Literal ID="lit_quote_attach_file" runat="server" />
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnl_private_partial" runat="server">
                                        <br />
                                        <div class="bid_date">
                                            <asp:Literal ID="lit_bid_date_buy" runat="server"></asp:Literal>
                                        </div>
                                        <div style="text-align:left;padding: 3px;">
                                            <asp:Literal ID="lit_buy_price" runat="server" />
                                        </div>
                                        <div style="text-align:left;padding: 3px;">
                                            <asp:Literal ID="lit_buy_qty" runat="server" />
                                        </div>
                                        <div class="bidderprice" style="text-align:left;">
                                            <asp:Literal ID="lit_approval_status" runat="server" />
                                        </div>
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div style="text-align: center; height: 18px; display: block; padding-top: 5px;">
                        <asp:Label ID="lbl_bidder_history" runat="server" CssClass="bidHistory" Visible="false"></asp:Label>
                    </div>
                    <div style="text-align: center; height: 18px; display: block; padding-top: 5px;">
                        <asp:Label ID="lbl_print_confirmation" runat="server" CssClass="bidHistory" Visible="false"></asp:Label>
                    </div>
                </div>
            </td>
        </tr>
    </telerik:RadAjaxPanel>
    <tr>
        <td align="left" colspan="2" valign="top" style="padding: 0px 5px;">
            <asp:Panel ID="Panel1_Content1" runat="server" CssClass="collapsePanel">
                <telerik:RadTabStrip runat="server" ID="TabStip1" MultiPageID="Multipage1" SelectedIndex="0">
                    <Tabs>
                        <telerik:RadTab runat="server" Text="Summary" PageViewID="PageView1">
                        </telerik:RadTab>
                        <telerik:RadTab runat="server" Text="Items" PageViewID="PageView2">
                        </telerik:RadTab>
                        <telerik:RadTab runat="server" Text="Details" PageViewID="PageView3">
                        </telerik:RadTab>
                        <telerik:RadTab runat="server" Text="Terms &amp; Condition" PageViewID="PageView4">
                        </telerik:RadTab>
                        <telerik:RadTab runat="server" Text="Ask Question" PageViewID="PageView5">
                        </telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage runat="server" ID="Multipage1" SelectedIndex="0" RenderSelectedPageOnly="false">
                    <telerik:RadPageView runat="server" ID="PageView1">
                        <div class="Tabarea">
                            <div style="padding: 10px;">
                                <asp:Literal runat="server" ID="lbl_auction_afterlaunch" />
                            </div>
                            <div style="padding: 10px 10px 0 10px;">
                                <asp:Literal runat="server" ID="lit_auction_summary"></asp:Literal>
                            </div>
                        </div>
                    </telerik:RadPageView>
                    <telerik:RadPageView runat="server" ID="PageView2">
                        <div class="Tabarea">
                            <table width="100%">
                                <asp:Repeater ID="Grid_Items" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <div class="grBackDescription">
                                                    <b>
                                                        <%# Eval("manufacturer")%>
                                                    </b>&nbsp;<span class="auctionPartNo">
                                                        <%# "Part No. " & Eval("part_no")%></span> &nbsp;<span class="auctionPartNo">
                                                            <%# "Quantity " & Eval("quantity")%></span>
                                                    <br />
                                                    <%# Eval("description")%>
                                                </div>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </div>
                    </telerik:RadPageView>
                    <telerik:RadPageView runat="server" ID="PageView3">
                        <div class="Tabarea">
                            <div style="padding: 10px;">
                                <asp:Label runat="server" ID="lbl_auction_description"></asp:Label>
                            </div>
                            <div style="padding: 10px 10px 0 10px;">
                                <asp:Label runat="server" ID="lbl_auction_prodetail"></asp:Label>
                            </div>
                        </div>
                    </telerik:RadPageView>
                    <telerik:RadPageView runat="server" ID="PageView4">
                        <div class="Tabarea">
                            <div style="padding: 10px;">
                                <asp:Label runat="server" ID="lbl_auction_payment"></asp:Label>
                            </div>
                            <div style="padding: 10px 10px 0 10px;">
                                <asp:Label runat="server" ID="lbl_auction_shipping"></asp:Label>
                            </div>
                            <div style="padding: 10px 10px 0 10px;">
                                <asp:Label runat="server" ID="lbl_auction_other"></asp:Label>
                            </div>
                            <div style="padding: 10px 10px 0 10px;">
                                <asp:Label runat="server" ID="lbl_auction_special"></asp:Label>
                            </div>
                            <div style="padding: 10px;">
                                <asp:Label runat="server" ID="lbl_auction_tax"></asp:Label>
                            </div>
                        </div>
                    </telerik:RadPageView>
                    <telerik:RadPageView runat="server" ID="PageView5">
                        <div class="Tabarea">
                            <div style="padding: 10px;">
                                <div class="detailsSubHeading">
                                    <asp:Literal ID="lit_ask_question" runat="server"></asp:Literal>
                                </div>
                                <div class="grBackDescription" style="padding-left: 2px; margin-top: 10px; margin-bottom: 10px;
                                    overflow: hidden;">
                                    <div style="overflow: hidden; padding: 10px; float: left;">
                                        <asp:TextBox runat="server" ID="txt_thread_title" Height="50" Width="500" TextMode="MultiLine"
                                            ></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfv_create_thread" runat="server" ErrorMessage="*"
                                            ForeColor="red" Display="Static" ControlToValidate="txt_thread_title" ValidationGroup="create_thread"></asp:RequiredFieldValidator>
                                    </div>
                                    <div style="float: left; padding-left: 10px; margin-top: 42px;">
                                        <asp:ImageButton ID="img_btn_create_thread" OnClick="img_btn_create_thread_Click"
                                            runat="server" AlternateText="Create" ImageUrl="/Images/fend/send.png" ValidationGroup="create_thread" />
                                    </div>
                                </div>
                                <div class="detailsSubHeading">
                                    <asp:Literal ID="lit_your_query" runat="server"></asp:Literal>
                                </div>
                                <asp:Repeater ID="rep_after_queries" runat="server">
                                    <ItemTemplate>
                                        <div class="qryTitle">
                                            <%# Container.ItemIndex + 1 & ". " & Eval("title")%>
                                        </div>
                                        <div class="qryAlt" style="padding-left: 10px;">
                                            <%--Eval("company_name") & ", " & Eval("state") & --%>
                                            <%# "on " & Eval("submit_date")%>
                                        </div>
                                        <div style="padding: 10px;">
                                            <asp:Repeater ID="rep_inner_after_queries" runat="server">
                                                <ItemTemplate>
                                                    <div class="qryDesc">
                                                        <img src='<%# IIF(Eval("image_path")<>"","/upload/users/" & Eval("user_id") & "/" & Eval("image_path"),"/images/buyer_icon.gif") %>'
                                                            alt="" style="float: left; padding: 2px; height: 23px; width: 22px;" />
                                                        <%#Eval("message") %>
                                                    </div>
                                                    <div class="qryAlt">
                                                        <%# IIf(Eval("buyer_title") = "", Eval("title"), Eval("buyer_title"))%>
                                                        <%# IIf(Eval("first_name") = "", Eval("buyer_first_name") & " " & Eval("buyer_last_name"), Eval("first_name") & " " & Eval("last_name"))%>,
                                                        <%# Eval("submit_date")%>
                                                    </div>
                                                    <br />
                                                    <br />
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </asp:Panel>
        </td>
        <td>
        
        </td>
    </tr>
</table>
<%--<asp:Button runat="server" ID="btnModalPopUp" Style="display: none" />
<ajax:ModalPopupExtender ID="modalPopUpExtender1" runat="server" TargetControlID="btnModalPopUp"
    PopupControlID="pnlPopUpLogin" BackgroundCssClass="modalBackground" CancelControlID="img_btnCancel">
</ajax:ModalPopupExtender>
<asp:Panel runat="Server" ID="pnlPopUpLogin" Height="200px" Width="700px" BackColor="#ffffff"
    CssClass="confirm-dialog" Style="display: none; border: solid 1px #000000;">
    <div style="text-align: center;">
        <asp:Panel runat="server" ID="pnl_login_content" Visible="true" DefaultButton="img_btnsend">
            <div style="background-color: #F2F2F2; height: 30px;">
                <div style="font-size: 14px; font-weight: bold; padding-top: 6px;">
                    <asp:Label ID="lbl_login_caption" runat="server" ForeColor="#10C0F5" Text="Buy Now : "></asp:Label>
                    <asp:Label ID="lbl_login_auction" runat="server"></asp:Label>
                </div>
            </div>
            <center>
                <div style="text-align: center; width: 65%; margin-top: 20px;">
                    <div style="margin: 0px 10px; overflow: hidden;">
                        <div style="float: left; font-weight: bold; width: 150px; padding-top: 3px; text-align: center;">
                            User Name:</div>
                        <div style="float: left; text-align: center;">
                            <asp:TextBox runat="server" ID="txt_login_name" Width="164" Height="22" BackColor="#D9D9D9"
                                BorderStyle="None"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFUserName" runat="server" ControlToValidate="txt_login_name"
                                ErrorMessage=" *" Display="Static">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="margin: 10px; overflow: hidden; text-align: center;">
                        <div style="float: left; font-weight: bold; width: 150px; padding-top: 3px; text-align: center;">
                            Password:</div>
                        <div style="float: left; text-align: center;">
                            <asp:TextBox runat="server" ID="txt_login_pwd" Width="164" Height="22" BackColor="#D9D9D9"
                                BorderStyle="None" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFPassword" runat="server" ControlToValidate="txt_login_pwd"
                                ErrorMessage=" *" Display="Dynamic">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="margin: 10px; overflow: hidden; text-align: center;">
                        <asp:Label ID="lbl_error" ForeColor="Red" runat="server" Text="Invalid User Name/Password"></asp:Label>
                    </div>
                    <div style="padding: 0 10px; overflow: hidden; text-align: center;">
                        <asp:ImageButton runat="server" AlternateText="Login" ID="img_btnsend" ImageUrl="/Images/login.png" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:ImageButton ID="img_btnCancel" runat="server" AlternateText="Cancel" ImageUrl="/Images/cancel.png" />
                    </div>
                </div>
                <div style="margin: 0 10px; overflow: hidden; text-align: center;">
                    <asp:Label ID="Label1" Text="Please note that PCSWireless reserve the rights to cancel any order."
                        runat="server" ForeColor="#838383" Font-Bold="true"></asp:Label>
                </div>
            </center>
        </asp:Panel>
    </div>
</asp:Panel>--%>
<%--<asp:Button runat="server" ID="Button1" Style="display: none" />
<ajax:ModalPopupExtender ID="modalPopUpExtender2" runat="server" TargetControlID="Button1"
    PopupControlID="pnlPopUpOffer" BackgroundCssClass="modalBackground" CancelControlID="ImageButton2">
</ajax:ModalPopupExtender>
<asp:Panel runat="Server" ID="pnlPopUpOffer" Height="200px" Width="700px" BackColor="#ffffff"
    CssClass="confirm-dialog" Style="display: none; border: solid 1px #000000;">
    <div style="text-align: center;">
        <asp:Panel runat="server" ID="Panel2" Visible="true" DefaultButton="ImageButtonOffer">
            <div style="background-color: #F2F2F2; height: 30px;">
                <div style="font-size: 14px; font-weight: bold; padding-top: 6px;">
                    <asp:Label ID="lit_offer_heading" runat="server" ForeColor="#10C0F5"></asp:Label>
                    <asp:Label ID="lbl_bid_auction" runat="server"></asp:Label>
                </div>
            </div>
            <center>
                <div style="text-align: center; width: 66%;">
                    <div style="margin: 30px 10px 10px 10px; overflow: hidden;" id="div_amount" runat="server">
                        <div style="float: left; font-weight: bold; width: 170px; padding-top: 3px;">
                            I want to buy this item for :
                        </div>
                        <div style="float: left; text-align: center; width: 210px;">
                            <telerik:RadNumericTextBox ID="txt_amount" Width="160px" Height="22px" BackColor="#D9D9D9"
                                BorderStyle="None" runat="server" MaxLength="6" Type="Currency">
                            </telerik:RadNumericTextBox>
                            <asp:RequiredFieldValidator ID="RFOffer1" runat="server" ErrorMessage=" * " ForeColor="red"
                                ControlToValidate="txt_amount" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="CValidator1" runat="server" Display="Dynamic"
                                ControlToValidate="txt_amount" ErrorMessage=" * " ValidationExpression="^\d*\.?\d*$"></asp:RegularExpressionValidator>
                            <asp:CompareValidator ID="CValidator_a" runat="server" Display="Dynamic" ControlToValidate="txt_amount"
                                ErrorMessage=" * " Type="Double" Operator="GreaterThanEqual"></asp:CompareValidator><b>dollars.</b>
                        </div>
                    </div>
                    <div style="margin: 30px 10px 10px 10px; text-align: center; overflow: hidden;" id="div_buy_now_price_label"
                        runat="server" visible="false">
                        <div style="font-weight: bold; padding-top: 3px;">
                            <asp:Literal ID="lit_trad_proxy_buy_now_price" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div style="margin: 20px 10px 10px 10px; overflow: hidden;" id="div_qty" runat="server">
                        <div style="float: left; font-weight: bold; width: 180px; padding-top: 3px;">
                            I want to buy this item :
                        </div>
                        <div style="float: left;">
                            <asp:TextBox runat="server" ID="txt_qty" Width="160" Height="22" BackColor="#D9D9D9"
                                BorderStyle="None"></asp:TextBox>
                            <asp:CompareValidator ID="CValidator2" runat="server" Display="Dynamic" ControlToValidate="txt_qty"
                                ErrorMessage=" * " Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>
                            <asp:RequiredFieldValidator ID="RFOffer2" runat="server" ControlToValidate="txt_qty"
                                ErrorMessage=" * " Display="Dynamic">
                            </asp:RequiredFieldValidator><b>quantities.</b>
                        </div>
                    </div>
                    <div style="margin: 20px 10px 10px 0; overflow: hidden; text-align: center;">
                        <center>
                            <asp:ImageButton runat="server" AlternateText="Send" ID="ImageButtonOffer" ImageUrl="/Images/fend/send.png" />&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton
                                ID="ImageButton2" runat="server" AlternateText="Cancel" ImageUrl="/Images/cancel.png" />
                        </center>
                    </div>
                </div>
            </center>
            <div style="margin: 10px; overflow: hidden; text-align: center;">
                <asp:Label ID="lbl_note" Text="Please note that PCSWireless reserve the rights to cancel any order."
                    runat="server" ForeColor="#838383" Font-Bold="true"></asp:Label>
            </div>
        </asp:Panel>
    </div>
</asp:Panel>--%>
<%--<asp:Button runat="server" ID="Button3" Style="display: none" />
<ajax:ModalPopupExtender ID="modalPopUp_msg" runat="server" TargetControlID="Button3"
    PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" CancelControlID="ImageButton_msg_cancel">
</ajax:ModalPopupExtender>
<asp:Panel runat="server" ID="pnlPopUpMsg" Height="150px" Width="700px" BackColor="#ffffff"
    CssClass="confirm-dialog" Style="display: none; border: solid 1px #000000;">
    <div style="text-align: center;">
        <asp:Panel runat="server" ID="Panel4" Visible="true" DefaultButton="ImageButton_msg_cancel">
            <div style="background-color: #F2F2F2; height: 30px;">
                <div style="font-size: 14px; font-weight: bold; padding-top: 6px; text-align: center;">
                    <asp:Label ID="lbl_popmsg_caption" runat="server" ForeColor="#10C0F5" Text="Buy Now : "></asp:Label>
                    <asp:Label ID="lbl_popmsg_auction" runat="server"></asp:Label>
                </div>
            </div>
            <center>
                <div style="text-align: center; width: 75%; text-align: center; margin-top: 20px">
                    <asp:ImageButton ID="ImageButton_msg_cancel" CausesValidation="false" runat="server"
                        AlternateText="Close" ImageUrl="/Images/close.gif" />
                </div>
            </center>
            <div style="margin: 10px; overflow: hidden; text-align: center;">
                <asp:Label ID="lbl_popmsg_msg" ViewStateMode="Disabled" runat="server" ForeColor="#B18B4F"
                    Font-Bold="true"></asp:Label>
            </div>
            <div style="margin: 10px; overflow: hidden; text-align: center;">
                <asp:Label ID="Label5" Text="Please note that PCSWireless reserve the rights to cancel any order."
                    runat="server" ForeColor="#838383" Font-Bold="true"></asp:Label>
            </div>
        </asp:Panel>
    </div>
</asp:Panel>--%>
<asp:HiddenField runat="server" ID="hid_buyer_id" Value="0" />
<asp:HiddenField runat="server" ID="hid_auction_id" Value="0" />
<%--<asp:Button runat="server" ID="Button2" Style="display: none" />
<ajax:ModalPopupExtender ID="modalPopUpExtender3" runat="server" TargetControlID="Button2"
    PopupControlID="pnlPopUpQuote" BackgroundCssClass="modalBackground" CancelControlID="ImageButton_quote_cancel">
</ajax:ModalPopupExtender>
<asp:Panel runat="Server" ID="pnlPopUpQuote" Height="230px" Width="700px" BackColor="#ffffff"
    CssClass="confirm-dialog" Style="display: none; border: solid 1px #000000;">
    <div style="text-align: center;">
        <asp:Panel runat="server" ID="Panel3" Visible="true" DefaultButton="ImageButton_quote_send">
            <div style="background-color: #F2F2F2; height: 30px;">
                <div style="font-size: 14px; font-weight: bold; padding-top: 6px;">
                    <asp:Label ID="lbl_quote_caption" runat="server" ForeColor="#10C0F5" Text="Offer : "></asp:Label>
                    <asp:Label ID="lbl_quote_auction" runat="server"></asp:Label>
                </div>
            </div>
            <center>
                <div style="text-align: center; width: 75%;">
                    <div style="margin: 20px 10px 10px 10px; overflow: hidden;">
                        <div style="float: left; font-weight: bold; width: 120px; text-align: right;">
                            Description :
                        </div>
                        <div style="float: left; width: 170px; text-align: right;">
                            <asp:TextBox runat="server" ID="txt_quote_desc" Width="160" TextMode="MultiLine"
                                Height="40" BackColor="#D9D9D9" BorderStyle="None"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="Required_txt_quote_desc" runat="server" Font-Size="10px"
                                ValidationGroup="_quote_fl" ControlToValidate="txt_quote_desc" ErrorMessage="<br>Description Required"
                                Display="Dynamic">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="margin: 20px 10px 10px 0px; overflow: hidden;">
                        <div style="float: left; font-weight: bold; width: 120px; text-align: right;">
                            File :
                        </div>
                        <div style="float: left; width: 170px; text-align: right;">
                            <asp:FileUpload ID="fle_quote_upload" Width="160" size="15" runat="server" />
                        </div>
                    </div>
                    <div style="margin: 20px 10px 10px 0; overflow: hidden; text-align: center;">
                        <asp:ImageButton runat="server" CausesValidation="false" ValidationGroup="_hgsvasv"
                            OnClientClick="return set_fl_id(this.id,'txt_quote_desc');" AlternateText="Send"
                            ID="ImageButton_quote_send" ImageUrl="/Images/fend/8.png" />&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton
                                ID="ImageButton_quote_cancel" CausesValidation="false" runat="server" AlternateText="Cancel"
                                ImageUrl="/Images/cancel.png" />
                    </div>
                </div>
            </center>
            <div style="margin: 10px; overflow: hidden; text-align: center;">
                <asp:Label ID="Label4" Text="Please note that PCSWireless reserve the rights to cancel any order."
                    runat="server" ForeColor="#838383" Font-Bold="true"></asp:Label>
            </div>
            <div style="margin: 10px; overflow: hidden; text-align: center;">
                <asp:Label ID="lbl_quotep_msg" runat="server" ForeColor="#B18B4F" Font-Bold="true"></asp:Label>
            </div>
        </asp:Panel>
    </div>
</asp:Panel>
--%>