﻿
Partial Class signup
    Inherits System.Web.UI.Page
    Private rptVal As Repeater
    Private bucket_id As Integer
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
          If Page.IsPostBack = False Then
            fill_countries()
            fill_how_find_us()
            'fill_business_types()
            'fill_industry_types()
            fill_bucket()
            fill_companies()
            'fill_titles()
            txt_company.Focus()


        End If

    End Sub
    Protected Sub check_term(ByVal source As Object, ByVal e As ServerValidateEventArgs)
        If chk_accept_term.Checked = False Then
            e.IsValid = False
        Else
            e.IsValid = True
        End If
    End Sub

    Sub CheckUserID(ByVal source As Object, ByVal e As ServerValidateEventArgs)
        If validate_user_id() = 1 Then
            e.IsValid = False
        Else
            e.IsValid = True
        End If
    End Sub
    Sub CheckEmailID(ByVal source As Object, ByVal e As ServerValidateEventArgs)
        If Validate_email() = 1 Then
            e.IsValid = False
        Else
            e.IsValid = True
        End If
    End Sub

    Sub check_resale_validate(ByVal source As Object, ByVal e As ServerValidateEventArgs)

        If file_upload1.HasFile And file_upload1.PostedFile.ContentLength < 4096000 Then
            e.IsValid = True
        Else
            e.IsValid = False
        End If

        'If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
        '    If lbl_file.text = "" Then
        '        e.IsValid = True
        '    Else
        '        If Not file_upload1.HasFile Then
        '            e.IsValid = False
        '        Else
        '            e.IsValid = True
        '        End If
        '    End If
        'Else

        'End If
    End Sub

    Private Sub fill_countries()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select country_id,name,ISNULL(code,'') As code from tbl_master_countries"
        ds = SqlHelper.ExecuteDataset(strQuery)
        ddl_country.DataSource = ds
        ddl_country.DataTextField = "name"
        ddl_country.DataValueField = "country_id"
        ddl_country.DataBind()
        ddl_country.Items.Insert(0, New ListItem("--Select--", "0"))
        ddl_country.ClearSelection()
        ddl_country.Items.FindByText("USA").Selected = True
        ds.Dispose()
    End Sub

    Private Sub fill_titles()
        'Dim ds As DataSet
        'Dim strQuery As String = ""
        'strQuery = "select isnull(name,'') as name,title_id from tbl_master_titles order by name"
        'ds = SqlHelper.ExecuteDataset(strQuery)
        'ddl_title.DataSource = ds
        'ddl_title.DataTextField = "name"
        'ddl_title.DataValueField = "name"
        'ddl_title.DataBind()
        'ddl_title.Items.Insert(0, New ListItem("--Select--", ""))
        'ds.Dispose()
    End Sub
    Private Sub fill_how_find_us()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select how_find_us_id,name,ISNULL(description,'') as description,ISNULL(sno,99)as sno from tbl_master_how_find_us order by sno"
        ds = SqlHelper.ExecuteDataset(strQuery)
        ddl_how_find_us.DataSource = ds
        ddl_how_find_us.DataTextField = "name"
        ddl_how_find_us.DataValueField = "how_find_us_id"
        ddl_how_find_us.DataBind()
        ddl_how_find_us.Items.Insert(0, New ListItem("--Select--", ""))
        ds.Dispose()
    End Sub

    Private Sub fill_bucket()
       
        rpt_bucket.DataSource = SqlHelper.ExecuteDataset("select bucket_id,bucket_name from tbl_master_buckets where is_active=1 and bucket_id in (select bucket_id from tbl_master_bucket_values where is_active=1) order by sno")
        rpt_bucket.DataBind()
       

    End Sub
     Protected Sub rpt_bucket_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_bucket.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            rptVal = CType(e.Item.FindControl("rpt_bucket_value"), Repeater)
            bucket_id = CType(e.Item.FindControl("lit_bucket_id"), Literal).Text
             
            rptVal.DataSource = SqlHelper.ExecuteDataset("select V.bucket_id,V.bucket_value_id,V.bucket_value from tbl_master_bucket_values V where V.is_active=1 and V.bucket_id=" & bucket_id & " order by V.bucket_value_id")
            rptVal.DataBind()

            'Pre select all checkboxes on load  --IG 2014
            For Each RepeatorItem In rptVal.Items
                If RepeatorItem.ItemType = ListItemType.Item Or RepeatorItem.ItemType = ListItemType.AlternatingItem Then
                    Dim cb As CheckBox = CType(RepeatorItem.FindControl("chk_select"), CheckBox)
                    cb.Checked = True

                End If
            Next
            ''''''
        End If
    End Sub
    Protected Sub chk_optout_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_optout.CheckedChanged
        rpt_bucket.Visible = Not chk_optout.Checked
    End Sub
    'Private Sub fill_business_types()
    '    Dim ds As DataSet
    '    Dim strQuery As String = ""
    '    strQuery = "select business_type_id,name,ISNULL(description,'') as description,ISNULL(sno,99)as sno from tbl_master_business_types order by sno"
    '    ds = SqlHelper.ExecuteDataset(strQuery)
    '    chklst_business_types.DataSource = ds
    '    chklst_business_types.DataTextField = "name"
    '    chklst_business_types.DataValueField = "business_type_id"
    '    chklst_business_types.DataBind()
    '    ds.Dispose()
    'End Sub
    'Private Sub fill_industry_types()
    '    Dim ds As DataSet
    '    Dim strQuery As String = ""
    '    strQuery = "select industry_type_id,name,ISNULL(description,'') AS description,ISNULL(sno,999) as sno from tbl_master_industry_types order by sno"
    '    ds = SqlHelper.ExecuteDataset(strQuery)
    '    chklst_industry_types.DataSource = ds
    '    chklst_industry_types.DataTextField = "name"
    '    chklst_industry_types.DataValueField = "industry_type_id"
    '    chklst_industry_types.DataBind()
    '    ds.Dispose()
    'End Sub
    Private Sub fill_companies()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select seller_id,isnull(company_name,'') as company_name,isnull(auto_approve_bidders,0) as auto_approve_bidders from tbl_reg_sellers where is_active=1 order by company_name"
        ds = SqlHelper.ExecuteDataset(strQuery)
        If ds.Tables(0).Rows.Count > 1 Then
            chklst_companies_linked.DataSource = ds
            chklst_companies_linked.DataTextField = "company_name"
            chklst_companies_linked.DataValueField = "seller_id"
            chklst_companies_linked.DataBind()
            ds.Dispose()
            hid_comp_id.Value = 0
        ElseIf ds.Tables(0).Rows.Count = 1 Then
            hid_comp_id.Value = ds.Tables(0).Rows(0)("seller_id") & "#" & ds.Tables(0).Rows(0)("auto_approve_bidders")
            'Response.Write(hid_comp_id.Value.ToString.Remove(0, hid_comp_id.Value.ToString.LastIndexOf("#") + 1))
            tr_company.Visible = False
        Else
            hid_comp_id.Value = 0
            tr_company.Visible = False
        End If

    End Sub

    Private Function validate_user_id() As Integer
        Dim strQuery As String = ""
        'strQuery = "if exists(select user_id from vw_login_users where upper(username)=upper('" & txt_user_id.Text.Trim() & "')) select 1 else select 0"
        strQuery = "if exists(select buyer_user_id from tbl_reg_buyer_users where upper(username)=upper('" & txt_user_id.Text.Trim() & "')) or exists(select user_id from tbl_sec_users where upper(username)=upper('" & txt_user_id.Text.Trim() & "')) select 1 else select 0"
        Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)
        Return i
    End Function

    Private Function Validate_email() As Integer
        Dim strQuery As String = ""
        'strQuery = "if exists(select email from vw_login_users where upper(email)=upper('" & txt_email.Text.Trim() & "')) select 1 else select 0"
        strQuery = "if exists(select buyer_user_id from tbl_reg_buyer_users where upper(email)=upper('" & txt_email.Text.Trim() & "')) or exists(select user_id from tbl_sec_users where upper(email)=upper('" & txt_email.Text.Trim() & "')) select 1 else select 0"
        Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)
        Return i
    End Function
    Private Sub Update_buyer_buckets(ByVal buyer_id As Integer)
        Dim cnt As Integer
        Dim bucket_id As Integer
        Dim bucket_value_id As Integer
        Dim bucket_group(rpt_bucket.Items.Count - 1) As Boolean
        Dim bucket_group_anysel As Boolean = False

        '---------optimize - build string of values and Insert once into Table--IG

        If chk_optout.Checked = False Then

            For cnt = 0 To rpt_bucket.Items.Count - 1
                bucket_group(cnt) = False
                rptVal = CType(rpt_bucket.Items(cnt).FindControl("rpt_bucket_value"), Repeater)
                For j As Integer = 0 To rptVal.Items.Count - 1
                    If CType(rptVal.Items(j).FindControl("chk_select"), CheckBox).Checked = True Then
                        bucket_group(cnt) = True
                        bucket_group_anysel = True
                        Exit For
                    End If
                Next
            Next
            If bucket_group_anysel Then
                For cnt = 0 To rpt_bucket.Items.Count - 1
                    rptVal = CType(rpt_bucket.Items(cnt).FindControl("rpt_bucket_value"), Repeater)
                    If bucket_group(cnt) Then
                        For j As Integer = 0 To rptVal.Items.Count - 1
                            If CType(rptVal.Items(j).FindControl("chk_select"), CheckBox).Checked = True Then

                                bucket_id = CType(rptVal.Items(j).FindControl("lit_bucket_id"), Literal).Text
                                bucket_value_id = CType(rptVal.Items(j).FindControl("lit_bucket_value_id"), Literal).Text
                                SqlHelper.ExecuteNonQuery("INSERT INTO tbl_reg_buyer_bucket_mapping(buyer_id, bucket_id, bucket_value_id) VALUES (" & buyer_id & ", " & bucket_id & ", " & bucket_value_id & ")")

                            End If
                        Next
                    Else   'select all in this bucket 
                        For j As Integer = 0 To rptVal.Items.Count - 1

                            bucket_id = CType(rptVal.Items(j).FindControl("lit_bucket_id"), Literal).Text
                            bucket_value_id = CType(rptVal.Items(j).FindControl("lit_bucket_value_id"), Literal).Text
                            SqlHelper.ExecuteNonQuery("INSERT INTO tbl_reg_buyer_bucket_mapping(buyer_id, bucket_id, bucket_value_id) VALUES (" & buyer_id & ", " & bucket_id & ", " & bucket_value_id & ")")

                        Next
                    End If
                Next
            End If
        End If

    End Sub
    'Private Sub Update_buyer_business_types(ByVal buyer_id As Integer)
    '    For Each lst As ListItem In chklst_business_types.Items
    '        Dim strQuery As String = ""
    '        Dim i As String = lst.Value
    '        If lst.Selected Then
    '            strQuery = "if not exists(select mapping_id from tbl_reg_buyer_business_type_mapping where buyer_id=" & buyer_id.ToString() & " and business_type_id=" & i & ") insert into tbl_reg_buyer_business_type_mapping(buyer_id,business_type_id) Values(" & buyer_id.ToString() & "," & i & ")"
    '            ' Else
    '            '    strQuery = "delete from tbl_reg_buyer_business_type_mapping where buyer_id=" & buyer_id.ToString() & " and business_type_id=" & i
    '            SqlHelper.ExecuteNonQuery(strQuery)
    '        End If


    '    Next
    'End Sub
    'Private Sub Update_buyer_industry_types(ByVal buyer_id As Integer)
    '    For Each lst As ListItem In chklst_industry_types.Items
    '        Dim strQuery As String = ""
    '        Dim i As String = lst.Value
    '        If lst.Selected Then
    '            strQuery = "if not exists(select mapping_id from tbl_reg_buyer_industry_type_mapping where buyer_id=" & buyer_id.ToString() & " and industry_type_id=" & i & ") insert into tbl_reg_buyer_industry_type_mapping(buyer_id,industry_type_id) Values(" & buyer_id.ToString() & "," & i & ")"
    '            'Else
    '            'strQuery = "delete from tbl_reg_buyer_industry_type_mapping where buyer_id=" & buyer_id.ToString() & " and industry_type_id=" & i
    '            SqlHelper.ExecuteNonQuery(strQuery)
    '        End If

    '    Next
    'End Sub
    Private Sub Update_buyer_companies_linked(ByVal buyer_id As Integer)
        Dim strQuery As String = ""
        Dim i As String = ""
        If chklst_companies_linked.Items.Count > 1 Then

            For Each lst As ListItem In chklst_companies_linked.Items
                i = lst.Value
                If lst.Selected Then
                    strQuery = "if not exists(select mapping_id from tbl_reg_buyer_seller_mapping where buyer_id=" & buyer_id.ToString() & " and seller_id=" & i & ") insert into tbl_reg_buyer_seller_mapping(buyer_id,seller_id) Values(" & buyer_id.ToString() & "," & i & ")"
                    ' Else
                    'strQuery = "delete from tbl_reg_buyer_seller_mapping where buyer_id=" & buyer_id.ToString() & " and seller_id=" & i
                    SqlHelper.ExecuteNonQuery(strQuery)
                End If

            Next
        Else
            i = hid_comp_id.Value.ToString.Remove(hid_comp_id.Value.ToString.LastIndexOf("#"))
            strQuery = "if not exists(select mapping_id from tbl_reg_buyer_seller_mapping where buyer_id=" & buyer_id.ToString() & " and seller_id=" & i & ") insert into tbl_reg_buyer_seller_mapping(buyer_id,seller_id) Values(" & buyer_id.ToString() & "," & i & ")"

            SqlHelper.ExecuteNonQuery(strQuery)
        End If
    End Sub
    Private Sub Update_buyer_how_find_us(ByVal buyer_id As Integer)
        If ddl_how_find_us.SelectedValue <> "" Then
            Dim strQuery As String = ""

            '  If lst.Selected Then
            strQuery = "if not exists(select assignment_id from tbl_reg_buyer_find_us_assignment where buyer_id=" & buyer_id.ToString() & " and how_find_us_id=" & ddl_how_find_us.SelectedValue & ") insert into tbl_reg_buyer_find_us_assignment(buyer_id,how_find_us_id) Values(" & buyer_id.ToString() & "," & ddl_how_find_us.SelectedValue & ")"
            ' Else
            '     strQuery = "delete from tbl_reg_buyer_find_us_assignment where buyer_id=" & buyer_id.ToString() & " and how_find_us_id=" & i
            ' End If

            SqlHelper.ExecuteNonQuery(strQuery)
        End If
    End Sub

    Protected Sub imgbtn_basic_save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgbtn_basic_save.Click

        'txt_comment.Text = txt_comment.Text.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "<br><br> Normal: "
        'txt_comment.Text = txt_comment.Text.Replace("<br/>", Char.ConvertFromUtf32(13)).Replace("<br/>", Char.ConvertFromUtf32(10))
        ''Response.Write("<br><br> Normal: " & txt_comment.Text)
        'Exit Sub
        hid_password.Value = txt_password.Text.Trim()

        If Page.IsValid Then
            Dim i As Integer = validate_user_id()
            If i = 1 Then
                lblMessage.Text = "User exists. Please choose other user id."
                Exit Sub
            End If
            i = Validate_email()
            If i = 1 Then
                lblMessage.Text = "Email is already in our record."
                Exit Sub
            End If
            Dim str As String() = hid_comp_id.Value.Split("#")
            Dim buyer_id As Integer = 0
            Dim buyer_user_id As Integer = 0
            Dim strupload = ""
            If file_upload1.HasFile Then
                strupload = IO.Path.GetFileName(file_upload1.PostedFile.FileName)
            End If
            Dim strQuery As String = ""
            ' strQuery = "Insert into tbl_reg_buyers(company_name,contact_title,contact_first_name,contact_last_name,email,website,mobile,phone,fax,address1,address2,city,state_id,zip,country_id,status_id,state_text,comment,is_phone1_mobile,is_phone2_mobile,approval_status,submit_date,max_amt_bid,resale_certificate,microtelecom_account_no) Values ('" & CommonCode.encodeSingleQuote(txt_company.Text.Trim()) & "','" & String.Empty & "','" & CommonCode.encodeSingleQuote(txt_first_name.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_last_name.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_email.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_website.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_mobile.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_phone.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_fax.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_address1.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "','" & CommonCode.encodeSingleQuote(txt_address2.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "','" & CommonCode.encodeSingleQuote(txt_city.Text.Trim()) & "',0,'" & CommonCode.encodeSingleQuote(txt_zip.Text.Trim()) & "'," & ddl_country.SelectedItem.Value & "," & IIf(str(1) = "1", 2, 1) & ",'" & CommonCode.encodeSingleQuote(txt_other_state.Text.Trim) & "','" & CommonCode.encodeSingleQuote(txt_comment.Text.Trim.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "'," & IIf(rdo_ph_1_landline.Checked, 0, 1) & "," & IIf(rdo_ph_2_landline.Checked, 0, 1) & ",'" & IIf(str(1) = "1", "Approved", "Pending") & "',GETDATE()," & IIf(str(1) = "1", "9999999", "0") & ",'" & strupload & "','') select SCOPE_IDENTITY()"

            Dim sb_Script As New StringBuilder()
            sb_Script.Append("Insert into tbl_reg_buyers(company_name,contact_title,contact_first_name,")
            sb_Script.Append("contact_last_name,email,website,mobile,phone,fax,address1,address2,city,state_id,zip,country_id,status_id,state_text,")
            sb_Script.Append("comment,is_phone1_mobile,is_phone2_mobile,approval_status,submit_date,max_amt_bid,resale_certificate,microtelecom_account_no) Values (")
            sb_Script.Append("@company, @title, @firstname, @lastname, @email, @website, @mobil, @phone, @fax, @address1, @address2, @city, @state_id, @zipcode, @country, @status,  ")
            sb_Script.Append("@state_text, @comment,  @is_phone1_mobile,  @is_phone2_mobile, @approval,  @submitdate, @maxbid, @certificate, @microtelecom_account_no")
            sb_Script.Append(") select SCOPE_IDENTITY()")


            Using conn As New SqlConnection(SqlHelper.of_getConnectString())
                Dim SqlCmd1 As New SqlCommand(sb_Script.ToString, conn)


                SqlCmd1.Parameters.AddWithValue("@company", CommonCode.encodeSingleQuote(txt_company.Text.Trim()))
                SqlCmd1.Parameters.AddWithValue("@title", String.Empty)
                SqlCmd1.Parameters.AddWithValue("@firstname", CommonCode.encodeSingleQuote(txt_first_name.Text.Trim()))
                SqlCmd1.Parameters.AddWithValue("@lastname", CommonCode.encodeSingleQuote(txt_last_name.Text.Trim()))
                SqlCmd1.Parameters.AddWithValue("@email", CommonCode.encodeSingleQuote(txt_email.Text.Trim()))
                SqlCmd1.Parameters.AddWithValue("@website", CommonCode.encodeSingleQuote(txt_website.Text.Trim()))
                SqlCmd1.Parameters.AddWithValue("@mobil", CommonCode.encodeSingleQuote(txt_mobile.Text.Trim()))
                SqlCmd1.Parameters.AddWithValue("@phone", CommonCode.encodeSingleQuote(txt_phone.Text.Trim()))
                SqlCmd1.Parameters.AddWithValue("@fax", CommonCode.encodeSingleQuote(txt_fax.Text.Trim()))
                SqlCmd1.Parameters.AddWithValue("@address1", CommonCode.encodeSingleQuote(txt_address1.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")))
                SqlCmd1.Parameters.AddWithValue("@address2", CommonCode.encodeSingleQuote(txt_address2.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")))
                SqlCmd1.Parameters.AddWithValue("@city", CommonCode.encodeSingleQuote(txt_city.Text.Trim()))
                SqlCmd1.Parameters.AddWithValue("@state_id", "0")
                SqlCmd1.Parameters.AddWithValue("@zipcode", CommonCode.encodeSingleQuote(txt_zip.Text.Trim()))
                SqlCmd1.Parameters.AddWithValue("@country", ddl_country.SelectedItem.Value)
                SqlCmd1.Parameters.AddWithValue("@status", IIf(str(1) = "1", 2, 1))
                SqlCmd1.Parameters.AddWithValue("@state_text", CommonCode.encodeSingleQuote(txt_other_state.Text.Trim))
                SqlCmd1.Parameters.AddWithValue("@comment", CommonCode.encodeSingleQuote(txt_comment.Text.Trim.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")))
                SqlCmd1.Parameters.AddWithValue("@is_phone1_mobile", IIf(rdo_ph_1_landline.Checked, 0, 1))
                SqlCmd1.Parameters.AddWithValue("@is_phone2_mobile", IIf(rdo_ph_2_landline.Checked, 0, 1))
                SqlCmd1.Parameters.AddWithValue("@approval", IIf(str(1) = "1", "Approved", "Pending"))
                SqlCmd1.Parameters.AddWithValue("@submitdate", Now())
                SqlCmd1.Parameters.AddWithValue("@maxbid", IIf(str(1) = "1", "9999999", "0"))
                SqlCmd1.Parameters.AddWithValue("@certificate", strupload)
                SqlCmd1.Parameters.AddWithValue("@microtelecom_account_no", String.Empty)
         
                Try
                    conn.Open()
                    buyer_id = SqlCmd1.ExecuteScalar()
                    conn.Close()
                    ' buyer_id = SqlHelper.ExecuteScalar(strQuery)
                    If file_upload1.HasFile Then
                        IO.Directory.CreateDirectory(Server.MapPath("/upload/bidders/resale_certificate/" & buyer_id))
                        file_upload1.PostedFile.SaveAs(Server.MapPath("/upload/bidders/resale_certificate/" & buyer_id & "/" & IO.Path.GetFileName(file_upload1.PostedFile.FileName)))
                    End If
                Finally
                    If conn.State = ConnectionState.Open Then conn.Close()
                End Try
               
            End Using

           
            If buyer_id > 0 Then
                Dim is_admin_buyer As Integer = 1

                ' strQuery = "Insert into tbl_reg_buyer_users(buyer_id,first_name,last_name,title,username,password,email,is_active,submit_date,submit_by_user_id,is_admin,bidding_limit) Values(" & buyer_id.ToString & ",'" & CommonCode.encodeSingleQuote(txt_first_name.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_last_name.Text.Trim()) & "','" & String.Empty & "','" & txt_user_id.Text.Trim().Replace("'", "''") & "','" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_email.Text.Trim()) & "'," & IIf(hid_comp_id.Value.ToString.Contains("#"), 1, 0) & ",GETDATE(),0," & is_admin_buyer & "," & IIf(str(1) = "1", "9999999", "0") & ") select SCOPE_IDENTITY()"

                Dim sb2_Script As New StringBuilder()
                sb2_Script.Append("Insert into tbl_reg_buyer_users(buyer_id,first_name,last_name,title,username,password,email,is_active,submit_date,submit_by_user_id,is_admin,bidding_limit) Values(")
                sb2_Script.Append("@buyerid, @firstname, @lastname, @title, @username, @password, @email, @is_active, @submit_date, @submit_by_user_id, @is_admin, @bidding_limit ")
                sb2_Script.Append(") select SCOPE_IDENTITY()")

            
                Using conn As New SqlConnection(SqlHelper.of_getConnectString())
                    Dim SqlCmd1 As New SqlCommand(sb2_Script.ToString, conn)


                    SqlCmd1.Parameters.AddWithValue("@buyerid", buyer_id.ToString())
                    SqlCmd1.Parameters.AddWithValue("@firstname", CommonCode.encodeSingleQuote(txt_first_name.Text.Trim()))
                    SqlCmd1.Parameters.AddWithValue("@lastname", CommonCode.encodeSingleQuote(txt_last_name.Text.Trim()))
                    SqlCmd1.Parameters.AddWithValue("@title", String.Empty)
                    SqlCmd1.Parameters.AddWithValue("@username", txt_user_id.Text.Trim().Replace("'", "''"))
                    SqlCmd1.Parameters.AddWithValue("@password", Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()))
                    SqlCmd1.Parameters.AddWithValue("@email", CommonCode.encodeSingleQuote(txt_email.Text.Trim()))
                    SqlCmd1.Parameters.AddWithValue("@is_active", IIf(hid_comp_id.Value.ToString.Contains("#"), 1, 0))
                    SqlCmd1.Parameters.AddWithValue("@submit_date", Now())
                    SqlCmd1.Parameters.AddWithValue("@submit_by_user_id", "0")
                    SqlCmd1.Parameters.AddWithValue("@is_admin", is_admin_buyer)
                    SqlCmd1.Parameters.AddWithValue("@bidding_limit", IIf(str(1) = "1", "9999999", "0"))
                    

                    Try
                        conn.Open()
                        buyer_user_id = SqlCmd1.ExecuteScalar()
                        conn.Close()

                        'buyer_user_id = SqlHelper.ExecuteScalar(strQuery)
                        
                    Finally
                        If conn.State = ConnectionState.Open Then conn.Close()
                    End Try

                End Using
                        ' ASSIGN DEFAULT SALES REP TO THE BUYER - sandeep
                        SqlHelper.ExecuteNonQuery("insert into tbl_reg_sale_rep_buyer_mapping(buyer_id,user_id) values(" & buyer_id & "," & SqlHelper.of_FetchKey("default_sales_rep_id") & ")") 'sales_rep_id is going 0

                        Update_buyer_how_find_us(buyer_id)
                        'Update_buyer_business_types(buyer_id)
                        'Update_buyer_industry_types(buyer_id)
                        Update_buyer_companies_linked(buyer_id)
                        If rpt_bucket.Visible Then
                            Update_buyer_buckets(buyer_id)
                        End If

                        lblMessage.Text = "New bidder created successfully."


                        Dim reg_mail As New Email
                        If Not hid_comp_id.Value.ToString.Contains("#") Then
                            reg_mail.Send_Registration_Mail(txt_email.Text.Trim, CommonCode.encodeSingleQuote(txt_first_name.Text))
                            reg_mail.Bidderd_Admin_Active(buyer_id)
                            'Dim obj As New Email_NEW
                            'obj.send_Bidderd_SemiAproval_Admin_Active(buyer_id)
                            'Dim str1 As String = "select emails from from tbl_master_email_addresses where email_address_id=" & SqlHelper.of_FetchKey("bidder_email_address_id")
                            'Dim ds As New DataSet
                            'ds = SqlHelper.ExecuteDataset(str1)
                            'If ds.Tables(0).Rows.Count > 0 Then
                            '    Dim email_address() As String = ds.Tables(0).Rows(0).ToString().Split(",")
                            '    For i = 0 To email_address.Length

                            '    Next
                            'End If
                            Response.Redirect("/register_confirm.aspx?rt=N")
                        Else
                            If str(1) = "1" Then
                                reg_mail.Send_Registration_Approve_Mail(buyer_user_id, txt_email.Text.Trim, CommonCode.encodeSingleQuote(txt_first_name.Text))
                                Response.Redirect("/register_confirm.aspx?rt=C")
                            Else
                                reg_mail.Send_Registration_Mail(txt_email.Text.Trim, CommonCode.encodeSingleQuote(txt_first_name.Text))
                                reg_mail.Bidderd_Admin_Active(buyer_id)
                                reg_mail.send_Bidderd_SemiAproval_Admin_Active(buyer_id)
                                Response.Redirect("/register_confirm.aspx?rt=N")
                            End If
                        End If
                        reg_mail = Nothing

            Else
                        txt_password.Attributes.Add("value", hid_password.Value)
                        txt_retype_password.Attributes.Add("value", hid_password.Value)
                        lblMessage.Text = "Error in creating new buyer. For assistance, please call 973.805.7400 or email <a href='mailto:info@pcsww.com'>info@pcsww.com</a>"
            End If
        Else
            txt_password.Attributes.Add("value", hid_password.Value)
            txt_retype_password.Attributes.Add("value", hid_password.Value)
        End If
    End Sub

   
    
End Class
