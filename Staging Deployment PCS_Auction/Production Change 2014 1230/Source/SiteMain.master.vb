﻿
Partial Class SiteMain
    Inherits System.Web.UI.MasterPage
   
   
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If (Request.RawUrl <> "/terms_conditions_bottom.aspx") Then

            If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                If CommonCode.Fetch_Cookie_Shared("is_backend") = "1" Then
                    Response.Redirect("/Backend_Home.aspx?t=1")
                Else
                    Response.Redirect("/Auction_Listing.aspx?t=1")
                End If
            End If
        End If

    End Sub
End Class

