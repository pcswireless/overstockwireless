﻿Imports System.IO

Partial Class UserControls_order_confirm
    Inherits System.Web.UI.UserControl
    Public _ID As Int32
    Public _AUCTION_ID As Int32
    Public _QUANTITY As Int32
    Public _PRICE As Double
    Public _TYPE As String
    Public _BUY_TYPE As String

    ''''''''''''''''''Most Important:-
    ''''''''''1st Priority (if Request.QueryString.Get("t")="a" then code run for "Auction Receipt" elseif equal to "b" then code run for "Order Confirmation")
    'Request.QueryString.Get("b") is unique_id means for Auction-Receipt its "bid_id" from tbl_auction_bids else for Order-Confirmation its "buy_id" from tbl_auction_buy
    'Request.QueryString.Get("q") is quantity (code run for "Order Confirmation")
    'Request.QueryString.Get("a") is auction_id (code run for "Order Confirmation")
    'Request.QueryString.Get("p") is price (code run for "Order Confirmation")
    'Request.QueryString.Get("m") is total_amount only in case of "partial" (code run for "Order Confirmation")
    'Request.QueryString.Get("y") is buy_type(buy now/partial/private) only when code run for "Order Confirmation"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Me.LNK_CSS.Attributes.Add("href", SqlHelper.of_FetchKey("ServerHttp") & "/Style/AuctionBody.css")
            If Not Request.QueryString("b") Is Nothing Then
                Dim bid_id As Integer = 0
                Try
                    bid_id = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("b"))
                Catch ex As Exception
                    bid_id = Request.QueryString.Get("b")
                End Try
                hid_bid_id.value = bid_id
            End If

            If Not Request.QueryString("pay") Is Nothing Then
                tr_payment.Visible = True
                Dim is_bid As Boolean = False
                If Not Request.QueryString("t") Is Nothing Then
                    If Request.QueryString("t") = "a" Then
                        is_bid = True
                    Else
                        is_bid = False
                    End If
                End If
                lit_payment_link.Text = "<a href='/PaypalRequest.aspx?" & IIf(is_bid, "bd", "by") & "=" & Security.EncryptionDecryption.EncryptValueFormatted(hid_bid_id.value) & "'><img src='/Images/paymentbypaypal.png' style='border:none' alt=''></img></a>"
            End If
        End If


        If IsNumeric(Request.QueryString.Get("e")) AndAlso Request.QueryString.Get("e") = 1 Then
            If (IsNumeric(hid_bid_id.value) And Request.QueryString.Get("t") <> Nothing) AndAlso hid_bid_id.value > 0 Then
                _ID = hid_bid_id.value
                _TYPE = Request.QueryString.Get("t")
                Load_by_ID(_ID, _TYPE)

                img_but_print.Visible = False
            End If
        ElseIf IsNumeric(Request.QueryString.Get("pay")) AndAlso Request.QueryString.Get("pay") = 1 Then
            If (IsNumeric(hid_bid_id.value) And Request.QueryString.Get("t") <> Nothing) AndAlso hid_bid_id.value > 0 Then
                _ID = hid_bid_id.value
                _TYPE = Request.QueryString.Get("t")
                Load_by_ID(_ID, _TYPE)

                img_but_print.Visible = False
                img_but_email.Visible = False
            End If
        Else
            If CommonCode.Fetch_Cookie_Shared("user_id") <> Nothing AndAlso CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                If (IsNumeric(hid_bid_id.value) And Request.QueryString.Get("t") <> Nothing) AndAlso hid_bid_id.value > 0 Then
                    _ID = hid_bid_id.value
                    _TYPE = Request.QueryString.Get("t")
                    _TYPE = _TYPE.ToLower
                ElseIf IsNumeric(Request.QueryString.Get("a")) And Request.QueryString.Get("t") <> Nothing And Request.QueryString.Get("y") <> Nothing Then
                    Dim dtBid As New DataTable
                    dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(B.bid_amount, 0) AS bid_amount,ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value from tbl_auction_bids B WITH (NOLOCK) inner join tbl_auctions A WITH (NOLOCK) on A.auction_id=B.auction_id  where A.auction_id=" & Request.QueryString.Get("a") & " order by bid_amount desc")
                    If dtBid.Rows.Count > 0 Then
                        With dtBid.Rows(0)
                            If Not (.Item("bid_amount") < .Item("thresh_hold_value")) And .Item("thresh_hold_value") > 0 Then
                                pnl_ord_cnfm.Visible = False
                                dv_cnfm_msg.Visible = False
                                lbl_popmsg_msg.Text = "Sorry, This offer is currently not available. Please bid to win this product."
                                Exit Sub
                            End If
                        End With
                    End If
                    If IsNumeric(Request.QueryString.Get("q")) Then
                        If IsNumeric(Request.QueryString.Get("m")) AndAlso Request.QueryString.Get("m") > 0 Then
                            'Response.Write(_AUCTION_ID)
                            _AUCTION_ID = Request.QueryString.Get("a")
                            _QUANTITY = Request.QueryString.Get("q")
                            _PRICE = CDbl(Request.QueryString.Get("m")) / CDbl(Request.QueryString.Get("q"))
                            _BUY_TYPE = Request.QueryString.Get("y")
                            _TYPE = Request.QueryString.Get("t")
                            _TYPE = _TYPE.ToLower
                        ElseIf IsNumeric(Request.QueryString.Get("p")) Then
                            _AUCTION_ID = Request.QueryString.Get("a")
                            _QUANTITY = Request.QueryString.Get("q")
                            _PRICE = Request.QueryString.Get("p")
                            _BUY_TYPE = Request.QueryString.Get("y")
                            _TYPE = Request.QueryString.Get("t")
                            _TYPE = _TYPE.ToLower
                        End If
                    End If
                End If
                If Not Page.IsPostBack Then
                    If _ID > 0 And _TYPE <> "" Then
                        Load_by_ID(_ID, _TYPE)

                    ElseIf _TYPE = "b" Then
                        Load_by_Auction(_AUCTION_ID, _QUANTITY, _PRICE)

                    End If

                End If
            Else
                Response.Write("<script language='javascript'>top.location = '/';</script>")
            End If
        End If
    End Sub

    Protected Sub Load_by_ID(ByVal _id As Int32, ByVal _type As String)
        'Response.Write(_buyid)
        Dim sel_qry As String = ""
        If _id > 0 Then
            If CommonCode.Fetch_Cookie_Shared("is_backend") = "1" Then
                img_but_email.Visible = True
            End If
            img_but_print.Visible = True
            If _type = "b" Then
                lbl_page_heading.Text = "Order Confirmation"
                sel_qry = "SELECT A.auction_id " & _
                               ",ISNULL(A.code, '') AS code" & _                               ",ISNULL(A.title, '') AS title" & _                               ",ISNULL(A.sub_title, '') AS sub_title" & _                               ",ISNULL(B.name,'') as stock_condition" & _                               ",ISNULL(C.name,'') as package_condition" & _                               ",ISNULL(D.quantity, 0) AS quantity" & _                               ",'$'+convert(varchar(100),round(ISNULL(D.price, 0),2)) AS price" & _                               ",'$'+convert(varchar(100),round(ISNULL(D.amount, 0),2)) AS amount" & _                               ",'$'+convert(varchar(100),round(ISNULL(D.sub_total_amount, 0),2)) AS sub_total_amount" & _                               ",'$'+convert(varchar(100),round(ISNULL(D.tax, 0),2)) AS tax" & _                               ",'$'+convert(varchar(100),round(ISNULL(D.grand_total_amount, 0),2)) AS grand_total_amount" & _                               ",convert(varchar(100),ISNULL(D.bid_date, getdate()),101) AS bid_date" & _                               ",'$'+convert(varchar(100),round(ISNULL(D.shipping_amount, 0),2)) AS shipping_amount" & _                               ",ISNULL(D.confirmation_no,'') AS confirmation_no" & _                               ",ISNULL(D.shipping_value, '') AS shipping_value" & _                               " FROM tbl_auctions A WITH (NOLOCK) LEFT JOIN tbl_master_stock_conditions B ON A.stock_condition_id=B.stock_condition_id LEFT JOIN tbl_master_packaging C ON A.packaging_id=C.packaging_id " & _
                               "inner join tbl_auction_buy D WITH (NOLOCK) on A.auction_id=D.auction_id where D.buy_id=" & _id & ""
            Else
                lbl_page_heading.Text = "Auction Receipt"
                sel_qry = "SELECT A.auction_id " & _
                                               ",ISNULL(A.code, '') AS code" & _                                               ",ISNULL(A.title, '') AS title" & _                                               ",ISNULL(A.sub_title, '') AS sub_title" & _                                               ",ISNULL(B.name,'') as stock_condition" & _                                               ",ISNULL(C.name,'') as package_condition" & _                                               ",1 AS quantity" & _                                               ",'$'+convert(varchar(100),round(ISNULL(D.bid_amount, 0),2)) AS price" & _                                               ",'$'+convert(varchar(100),round(ISNULL(D.bid_amount, 0),2)) AS amount" & _                                               ",'$'+convert(varchar(100),round(ISNULL(D.bid_amount, 0),2)) AS sub_total_amount" & _                                               ",'$0.00' AS tax" & _                                               ",'$'+convert(varchar(100),round(ISNULL(D.shipping_amount,0)+ISNULL(D.bid_amount, 0),2)) AS grand_total_amount" & _                                               ",convert(varchar(100),ISNULL(D.bid_date, getdate()),101) AS bid_date" & _                                               ",'$'+convert(varchar(100),round(ISNULL(D.shipping_amount, 0),2)) AS shipping_amount" & _                                               ",ISNULL(D.shipping_value, '') AS shipping_value" & _                                               ",ISNULL(D.bid_id, '') AS confirmation_no" & _                                               " FROM tbl_auctions A WITH (NOLOCK) LEFT JOIN tbl_master_stock_conditions B ON A.stock_condition_id=B.stock_condition_id LEFT JOIN tbl_master_packaging C ON A.packaging_id=C.packaging_id " & _
                                               "inner join tbl_auction_bids D WITH (NOLOCK) on A.auction_id=D.auction_id where D.bid_id=" & _id & ""
            End If
            sel_qry = sel_qry & bindToAddress(_id, _type)
            'Response.Write(sel_qry)
            Dim ds As New DataSet()
            ds = SqlHelper.ExecuteDataset(sel_qry)
            If ds.Tables(0).Rows.Count > 0 Then
                With ds.Tables(0).Rows(0)
                    lit_auc_code.Text = .Item("code")
                    lit_auc_name.Text = .Item("title") & "<br/>(" & .Item("sub_title") & ")"
                    lit_condition.Text = .Item("stock_condition")
                    lit_auc_packaging.Text = .Item("package_condition")
                    lit_price.Text = .Item("price")
                    lit_qty.Text = .Item("quantity") & " Lot"
                    lit_tax.Text = .Item("tax")
                    lit_amount.Text = .Item("amount")
                    lit_sub_total.Text = .Item("amount")
                    lit_date.Text = .Item("bid_date") '.ToString("MM/dd/yyyy")
                    lit_confirm_no.Text = .Item("confirmation_no")
                    If .Item("confirmation_no").ToString.Trim = "" Then
                        tr_confirm_no.Visible = False
                    End If
                    lit_grand_total.Text = .Item("grand_total_amount")
                    lit_ship_amount.Text = .Item("shipping_amount")
                    lbl_shipping_option.Text = .Item("shipping_value") & "<br/><br/><span style='font-size:12px; font-weight:bold'>All sales are final and sold as is</span>"
                End With

            End If

            If ds.Tables.Count > 0 AndAlso ds.Tables(1).Rows.Count > 0 Then
                With ds.Tables(1).Rows(0)
                    sel_qry = .Item("contact_first_name") & IIf(.Item("contact_last_name").ToString() <> "", " " & .Item("contact_last_name"), "")
                    sel_qry = sel_qry & "<br />" & .Item("company_name")
                    sel_qry = sel_qry & "<br />" & .Item("address1") & " " & .Item("address2")
                    sel_qry = sel_qry & "<br />" & .Item("city") & ", " & .Item("state_text") & " " & .Item("zip")
                    If .Item("phone") <> "" Then sel_qry = sel_qry & "<br />Phone: " & .Item("phone")
                    If .Item("fax") <> "" Then sel_qry = sel_qry & " Fax: " & .Item("fax")
                    lit_bill_to.Text = sel_qry

                End With
            End If
            select_shipping_option(0, 0)
        End If
        tr_confirm_but.Visible = False
        dv_cnfm_msg.Visible = False
    End Sub

    Protected Sub Load_by_Auction(ByVal _auid As Int32, ByVal _quantity As Int32, ByVal _price As Double)
        'Response.Write(_quantity)
        If CommonCode.Fetch_Cookie_Shared("is_backend") = "0" Then
            Dim sel_qry As String = ""
            If _auid > 0 Then
                lbl_page_heading.Text = "Order Confirmation"
                sel_qry = "SELECT A.auction_id " & _
                               ",ISNULL(A.code, '') AS code" & _                               ",ISNULL(A.title, '') AS title" & _                               ",ISNULL(A.sub_title, '') AS sub_title" & _                               ",ISNULL(B.name,'') as stock_condition" & _                               ",ISNULL(C.name,'') as package_condition" & _                               ",0 as tax" & _                               "," & _quantity & " as quantity" & _                               "," & _price & " as price" & _                               "," & _quantity * _price & " as amount" & _                               " FROM tbl_auctions A WITH (NOLOCK) LEFT JOIN tbl_master_stock_conditions B ON A.stock_condition_id=B.stock_condition_id LEFT JOIN tbl_master_packaging C ON A.packaging_id=C.packaging_id WHERE A.auction_id = " & _auid & ";"
                sel_qry = sel_qry & bindToAddress(0, "b")
                Dim ds As New DataSet()
                ds = SqlHelper.ExecuteDataset(sel_qry)
                If ds.Tables(0).Rows.Count > 0 Then
                    With ds.Tables(0).Rows(0)
                        lit_auc_code.Text = .Item("code")
                        lit_auc_name.Text = .Item("title") & "<br/>(" & .Item("sub_title") & ")"
                        lit_condition.Text = .Item("stock_condition")
                        lit_auc_packaging.Text = .Item("package_condition")
                        lit_price.Text = FormatCurrency(.Item("price"), 2)
                        lit_qty.Text = .Item("quantity") & " Lot"
                        lit_tax.Text = FormatCurrency(.Item("tax"), 2)
                        lit_amount.Text = FormatCurrency(.Item("amount"), 2)
                        lit_sub_total.Text = FormatCurrency(.Item("amount"), 2)
                        lit_date.Text = DateTime.Now.ToString("MM/dd/yyyy")
                    End With
                End If

                If ds.Tables.Count > 0 AndAlso ds.Tables(1).Rows.Count > 0 Then
                    With ds.Tables(1).Rows(0)
                        sel_qry = .Item("contact_first_name") & IIf(.Item("contact_last_name").ToString() <> "", " " & .Item("contact_last_name"), "")
                        sel_qry = sel_qry & "<br />" & .Item("company_name")
                        sel_qry = sel_qry & "<br />" & .Item("address1") & " " & .Item("address2")
                        sel_qry = sel_qry & "<br />" & .Item("city") & ", " & .Item("state_text") & " " & .Item("zip")
                        If .Item("phone") <> "" Then sel_qry = sel_qry & "<br />Phone: " & .Item("phone")
                        If .Item("fax") <> "" Then sel_qry = sel_qry & " Fax: " & .Item("fax")
                        lit_bill_to.Text = sel_qry
                        If .Item("company_name").ToString.Length > 3 Then
                            lit_confirm_no.Text = .Item("company_name").ToString.Remove(2).ToUpper & .Item("buyer_id").ToString & Guid.NewGuid().ToString.Remove(6).ToString.ToUpper
                        ElseIf .Item("company_name").ToString.Length = 3 Then
                            lit_confirm_no.Text = .Item("company_name").ToString.ToUpper & .Item("buyer_id").ToString & Guid.NewGuid().ToString.Remove(6).ToString.ToUpper
                        Else
                            lit_confirm_no.Text = "COM" & .Item("buyer_id").ToString & Guid.NewGuid().ToString.Remove(6).ToString.ToUpper
                        End If

                    End With
                End If
                select_shipping_option(_auid, _quantity)
                'Response.Write(lit_ship_amount.Text)
                'Exit Sub
                lit_grand_total.Text = FormatCurrency(CDbl(lit_sub_total.Text.Replace("$", "").Replace(",", "")) + CDbl(lit_tax.Text.Replace("$", "").Replace(",", "")) + CDbl(lit_ship_amount.Text.Replace("$", "").Replace(",", "")), 2)
            End If
        Else
            img_but_confirm.Visible = False
        End If

    End Sub

    'Private Sub bindAuction()
    '    Dim qry As String = "SELECT A.auction_id, " & _
    '            "ISNULL(A.code, '') AS code," & _    '            "ISNULL(A.title, '') AS title," & _    '            "ISNULL(B.name,'') as stock_condition," & _    '            "ISNULL(C.name,'') as package_condition" & _    '            " FROM tbl_auctions A LEFT JOIN tbl_master_stock_conditions B ON A.stock_condition_id=B.stock_condition_id LEFT JOIN tbl_master_packaging C ON A.packaging_id=C.packaging_id WHERE A.auction_id = " & Request.QueryString.Get("i")

    '    Dim dt As DataTable = New DataTable()
    '    dt = SqlHelper.ExecuteDatatable(qry)
    '    If dt.Rows.Count > 0 Then
    '        With dt.Rows(0)
    '            lit_auc_code.Text = .Item("code")
    '            lit_auc_name.Text = .Item("title")
    '            lit_condition.Text = .Item("stock_condition")
    '            lit_auc_packaging.Text = .Item("package_condition")
    '        End With
    '    End If
    'End Sub

    Private Function bindToAddress(ByVal _id As Int32, ByVal _type As String) As String
        Dim sqlStr As String = ""
        If _id > 0 Then
            If _type = "b" Then
                sqlStr = "SELECT "
                sqlStr = sqlStr & "ISNULL(A.company_name, '') AS company_name,"
                sqlStr = sqlStr & "ISNULL(A.buyer_id, '') AS buyer_id,"                sqlStr = sqlStr & "ISNULL(A.contact_title, '') AS contact_title,"                sqlStr = sqlStr & "ISNULL(A.contact_first_name, '') AS contact_first_name,"                sqlStr = sqlStr & "ISNULL(A.contact_last_name, '') AS contact_last_name,"                sqlStr = sqlStr & "ISNULL(A.email, '') AS email,"                sqlStr = sqlStr & "ISNULL(A.website, '') AS website,"                sqlStr = sqlStr & "ISNULL(A.phone, '') AS phone,"                sqlStr = sqlStr & "ISNULL(A.mobile, '') AS mobile,"                sqlStr = sqlStr & "ISNULL(A.fax, '') AS fax,"                sqlStr = sqlStr & "ISNULL(A.address1, '') AS address1,"                sqlStr = sqlStr & "ISNULL(A.address2, '') AS address2,"                sqlStr = sqlStr & "ISNULL(A.city, '') AS city,"                sqlStr = sqlStr & "ISNULL(A.state_id, 0) AS state_id,"                sqlStr = sqlStr & "ISNULL(A.zip, '') AS zip,"                sqlStr = sqlStr & "ISNULL(A.country_id, 0) AS country_id,"                sqlStr = sqlStr & "ISNULL(A.state_text, '') AS state_text,"                sqlStr = sqlStr & "rtrim(ISNULL(U.first_name,'') + ' ' + ISNULL(U.last_name,'')) as sales_rep"                sqlStr = sqlStr & " FROM"
                sqlStr = sqlStr & " tbl_reg_buyers A WITH (NOLOCK) left join tbl_reg_sale_rep_buyer_mapping M on A.buyer_id=M.buyer_id inner join tbl_sec_users U on M.user_id=U.user_id"                sqlStr = sqlStr & " WHERE"                sqlStr = sqlStr & " A.buyer_id =(select buyer_id from tbl_auction_buy WITH (NOLOCK) where buy_id=" & _id & ")"
            Else
                sqlStr = "SELECT "
                sqlStr = sqlStr & "ISNULL(A.company_name, '') AS company_name,"
                sqlStr = sqlStr & "ISNULL(A.buyer_id, '') AS buyer_id,"                sqlStr = sqlStr & "ISNULL(A.contact_title, '') AS contact_title,"                sqlStr = sqlStr & "ISNULL(A.contact_first_name, '') AS contact_first_name,"                sqlStr = sqlStr & "ISNULL(A.contact_last_name, '') AS contact_last_name,"                sqlStr = sqlStr & "ISNULL(A.email, '') AS email,"                sqlStr = sqlStr & "ISNULL(A.website, '') AS website,"                sqlStr = sqlStr & "ISNULL(A.phone, '') AS phone,"                sqlStr = sqlStr & "ISNULL(A.mobile, '') AS mobile,"                sqlStr = sqlStr & "ISNULL(A.fax, '') AS fax,"                sqlStr = sqlStr & "ISNULL(A.address1, '') AS address1,"                sqlStr = sqlStr & "ISNULL(A.address2, '') AS address2,"                sqlStr = sqlStr & "ISNULL(A.city, '') AS city,"                sqlStr = sqlStr & "ISNULL(A.state_id, 0) AS state_id,"                sqlStr = sqlStr & "ISNULL(A.zip, '') AS zip,"                sqlStr = sqlStr & "ISNULL(A.country_id, 0) AS country_id,"                sqlStr = sqlStr & "ISNULL(A.state_text, '') AS state_text,"                sqlStr = sqlStr & "rtrim(ISNULL(U.first_name,'') + ' ' + ISNULL(U.last_name,'')) as sales_rep"                sqlStr = sqlStr & " FROM"
                sqlStr = sqlStr & " tbl_reg_buyers A WITH (NOLOCK) left join tbl_reg_sale_rep_buyer_mapping M on A.buyer_id=M.buyer_id inner join tbl_sec_users U on M.user_id=U.user_id"                sqlStr = sqlStr & " WHERE"                sqlStr = sqlStr & " A.buyer_id =(select buyer_id from tbl_auction_bids WITH (NOLOCK) where bid_id=" & _id & ")"
            End If

        ElseIf _type = "b" Then
            sqlStr = "SELECT "
            sqlStr = sqlStr & "ISNULL(A.company_name, '') AS company_name,"
            sqlStr = sqlStr & "ISNULL(A.buyer_id, '') AS buyer_id,"            sqlStr = sqlStr & "ISNULL(A.contact_title, '') AS contact_title,"            sqlStr = sqlStr & "ISNULL(A.contact_first_name, '') AS contact_first_name,"            sqlStr = sqlStr & "ISNULL(A.contact_last_name, '') AS contact_last_name,"            sqlStr = sqlStr & "ISNULL(A.email, '') AS email,"            sqlStr = sqlStr & "ISNULL(A.website, '') AS website,"            sqlStr = sqlStr & "ISNULL(A.phone, '') AS phone,"            sqlStr = sqlStr & "ISNULL(A.mobile, '') AS mobile,"            sqlStr = sqlStr & "ISNULL(A.fax, '') AS fax,"            sqlStr = sqlStr & "ISNULL(A.address1, '') AS address1,"            sqlStr = sqlStr & "ISNULL(A.address2, '') AS address2,"            sqlStr = sqlStr & "ISNULL(A.city, '') AS city,"            sqlStr = sqlStr & "ISNULL(A.state_id, 0) AS state_id,"            sqlStr = sqlStr & "ISNULL(A.zip, '') AS zip,"            sqlStr = sqlStr & "ISNULL(A.country_id, 0) AS country_id,"            sqlStr = sqlStr & "ISNULL(A.state_text, '') AS state_text,"            sqlStr = sqlStr & "rtrim(ISNULL(U.first_name,'') + ' ' + ISNULL(U.last_name,'')) as sales_rep"            sqlStr = sqlStr & " FROM"
            sqlStr = sqlStr & " tbl_reg_buyers A WITH (NOLOCK) left join tbl_reg_sale_rep_buyer_mapping M on A.buyer_id=M.buyer_id inner join tbl_sec_users U on M.user_id=U.user_id"            sqlStr = sqlStr & " WHERE"            sqlStr = sqlStr & " A.buyer_id =" & CommonCode.Fetch_Cookie_Shared("buyer_id")
        End If

        Return sqlStr

    End Function

    'Private Sub bindItems()
    '    Dim strQry As String = "SELECT A.product_item_id,"
    '    strQry = strQry & "ISNULL(A.part_no, '') AS part_no,"    '    strQry = strQry & "ISNULL(A.packaging_id, 0) AS packaging_id,"    '    strQry = strQry & "ISNULL(A.name, '') AS name,"    '    strQry = strQry & "ISNULL(A.quantity, 0) * " & Request.QueryString.Get("q") & " AS quantity,"    '    strQry = strQry & "ISNULL(A.upc, '') AS upc,"    '    strQry = strQry & "ISNULL(A.sku, '') AS sku,"    '    strQry = strQry & "ISNULL(A.estimated_msrp, 0) AS estimated_msrp,"    '    strQry = strQry & "ISNULL(A.total_msrp, 0) * " & Request.QueryString.Get("q") & " AS total_msrp"
    '    strQry = strQry & " FROM "
    '    strQry = strQry & " tbl_auction_product_items A"
    '    strQry = strQry & " WHERE A.auction_id=" & Request.QueryString.Get("i")

    '    rpt_device.DataSource = SqlHelper.ExecuteDatatable(strQry)
    '    rpt_device.DataBind()


    'End Sub

    Public Sub select_shipping_option(ByVal _aucid As Int32, ByVal _qty As Int32)
        If _aucid > 0 Then

            lbl_shipping_option.Text = ""

            Dim shipping_amount As Double = SqlHelper.ExecuteScalar("select isnull(shipping_amount,0) from tbl_auctions where auction_id=" & _aucid)
            If shipping_amount > 0 Then
                tr_ship_option.Visible = False
                lbl_shipping_option.Text = "The shipping cost for this lot will be $" & FormatNumber(shipping_amount * _qty, 2)
                lit_ship_amount.Text = FormatCurrency(CDbl(shipping_amount * _qty), 2)
            Else
                Dim shipping_preference As String = ""
                Dim last_shipping_option As String = ""


                Dim Str As String
                Dim dt As New DataTable
                Str = "select ISNULL(shipping_type,'') AS shipping_type,ISNULL(shipping_account_number,'') AS shipping_account_number,ISNULL(shipping_preference,'') AS shipping_preference,ISNULL(last_shipping_option,'') AS last_shipping_option from tbl_reg_buyers WITH (NOLOCK) where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id")
                dt = SqlHelper.ExecuteDatatable(Str)
                If dt.Rows.Count > 0 Then
                    With dt.Rows(0)
                        txt_shipping_type.Text = .Item("shipping_type")
                        txt_shipping_account_number.Text = .Item("shipping_account_number")
                        shipping_preference = .Item("shipping_preference")
                        last_shipping_option = .Item("last_shipping_option")
                    End With
                End If
                dt.Dispose()


                If shipping_preference <> Nothing AndAlso shipping_preference <> "" Then
                Else
                    shipping_preference = "0"
                End If
                Str = "select isnull(A.use_pcs_shipping,0) as use_pcs_shipping, isnull(A.use_your_shipping,0) as use_your_shipping, isnull(S.name,'') as name, isnull(S.city,'') as city, isnull(S.state,'') as state from tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations S on S.stock_location_id=A.stock_location_id where A.auction_id=" & _aucid
                dt = SqlHelper.ExecuteDatatable(Str)
                Dim stock_location As String = ""
                If dt.Rows.Count > 0 Then
                    stock_location = IIf(dt.Rows(0)("name") = "", "", dt.Rows(0)("name")) & IIf(dt.Rows(0)("city") = "", "", "</br>" & dt.Rows(0)("city")) & IIf(dt.Rows(0)("state") = "", "", ", " & dt.Rows(0)("state"))

                    If CBool(dt.Rows(0)("use_pcs_shipping")) Then

                        getFedExRate(_aucid, _qty, shipping_preference)

                        If CBool(dt.Rows(0)("use_your_shipping")) Then
                            'Both shipping Available
                            rdo_my_ship.Visible = True
                            rdo_fedex_ship.Visible = True


                            If shipping_preference = "1" Then
                                'my shipping selected
                                tbl_user_own.Visible = True
                                tbl_user_ours.Visible = False
                                rdo_my_ship.Checked = True
                                lit_ship_amount.Text = "$0.00"

                            Else
                                rdo_fedex_ship.Checked = True
                                tbl_user_own.Visible = False

                                If rdo_fedex.Items.Count > 0 Then
                                    tbl_user_ours.Visible = True
                                    Dim asd As String
                                    For kount As Integer = 0 To rdo_fedex.Items.Count - 1
                                        asd = "bimal" & rdo_fedex.Items(kount).Text
                                        If asd.IndexOf(last_shipping_option) > 0 Then
                                            rdo_fedex.ClearSelection()
                                            rdo_fedex.Items(kount).Selected = True
                                        End If
                                    Next
                                    Dim temp As String = 0
                                    If Not (rdo_fedex.SelectedIndex >= 0) Then
                                        rdo_fedex.SelectedIndex = 0
                                    End If
                                    temp = rdo_fedex.SelectedItem.Text.Trim.Remove(0, rdo_fedex.SelectedItem.Text.Trim.IndexOf("$")).Replace("</b>", "")
                                    lit_ship_amount.Text = FormatCurrency(CDbl(temp), 2)
                                Else
                                    lit_ship_amount.Text = "$0.00"
                                    tbl_user_ours.Visible = False
                                End If


                            End If
                        Else
                            'Only Fedex available
                            rdo_my_ship.Visible = False
                            tbl_user_own.Visible = False

                            rdo_fedex_ship.Visible = True
                            tbl_user_ours.Visible = True
                            rdo_fedex_ship.Checked = True

                            If rdo_fedex.Items.Count > 0 Then
                                Dim asd As String
                                For kount As Integer = 0 To rdo_fedex.Items.Count - 1
                                    asd = "bimal" & rdo_fedex.Items(kount).Text
                                    If asd.IndexOf(last_shipping_option) > 0 Then
                                        rdo_fedex.ClearSelection()
                                        rdo_fedex.Items(kount).Selected = True
                                    End If
                                Next

                                Dim temp As String = 0
                                If Not (rdo_fedex.SelectedIndex >= 0) Then
                                    rdo_fedex.SelectedIndex = 0
                                End If
                                temp = rdo_fedex.SelectedItem.Text.Trim.Remove(0, rdo_fedex.SelectedItem.Text.Trim.IndexOf("$")).Replace("</b>", "")
                                lit_ship_amount.Text = FormatCurrency(CDbl(temp), 2)

                            Else
                                lit_ship_amount.Text = "$0.00"
                            End If
                        End If

                    Else
                        'use your only

                        rdo_fedex_ship.Visible = False
                        tbl_user_ours.Visible = False

                        tbl_user_own.Visible = True
                        rdo_my_ship.Visible = True
                        rdo_my_ship.Checked = True
                        lit_ship_amount.Text = "$0.00"

                    End If
                End If

                If stock_location <> "" Then
                    table_stock.Visible = True
                    lbl_stock_location.Text = stock_location
                Else
                    table_stock.Visible = False
                End If


            End If
        Else
            tr_ship_option.Visible = False
        End If

    End Sub
    Private Sub fetchShippingRate(ByVal auction_id As Integer, ByVal Destination_StreetLines As String, ByVal Destination_City As String, ByVal Destination_StateOrProvinceCode As String, ByVal destination_zip As String, ByVal destination_country_code As String, ByVal auction_weight As Double)
        'Try
        Dim obj As New FedAllRateServices
        Dim objreq As New FedexRateServices.RateRequest
        Dim objrep As New FedexRateServices.RateReply
        Dim dt As New DataTable()
        Dim i As Integer
        Dim Origin_StreetLines = "", Origin_City = "", Origin_StateOrProvinceCode = "", Origin_PostalCode = "", Origin_CountryCode As String = ""
        ''Origin_StreetLines = "3940 30th street" ' SqlHelper.of_FetchKey("fedex_source_street")
        ''Origin_City = "Long island city" 'SqlHelper.of_FetchKey("fedex_source_city")
        ''Origin_StateOrProvinceCode = "NY" ' SqlHelper.of_FetchKey("fedex_source_state")
        Origin_PostalCode = SqlHelper.of_FetchKey("fedex_source_zip") ' 11101
        Origin_CountryCode = SqlHelper.of_FetchKey("fedex_source_country_code") ' "US" 

        dt = obj.GetFedexShippingOptions(auction_id, Origin_StreetLines, Origin_City, Origin_StateOrProvinceCode, Origin_PostalCode, Origin_CountryCode, Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, destination_zip, destination_country_code, 1, 1, auction_weight).DefaultView.ToTable(True, "ServiceType", "Rate", "Error")
        'Response.Write(Destination_StreetLines & "-" & Destination_City & "-" & Destination_StateOrProvinceCode & "-" & destination_zip & "-" & destination_country_code & "-" & auction_weight & "-" & dt.Rows.Count)

        If dt.Rows.Count > 0 Then
            rdo_fedex.Items.Clear()
            For i = 0 To dt.Rows.Count - 1
                With dt.Rows(i)
                    If .Item("Error").ToString.Trim = "" Then
                        rdo_fedex.Items.Insert(i, New ListItem(.Item("ServiceType").ToString().Replace("_", " ") & " <b>" & FormatCurrency(.Item("Rate"), 2) & "</b>", i))
                    End If

                End With
            Next
        Else
            'SqlHelper.ExecuteNonQuery("if not exists(select fedex_rate_id from tbl_auction_fedex_rates where auction_id=" & auction_id & " and buyer_id =" & buyer_id & " and shipping_options='') begin INSERT INTO tbl_auction_fedex_rates(auction_id, buyer_id, shipping_amount,shipping_options,error_log,is_error) VALUES (" & auction_id & ", " & buyer_id & ",0,'',1) end")
        End If
        dt.Dispose()

        If rdo_fedex.Items.Count > 0 Then
            rdo_fedex.SelectedIndex = 0
        Else
            lit_ship_amount.Text = "$0.00"
            tbl_user_ours.Visible = False
            rdo_fedex_ship.Text = "Shipping rates will be calculated later"
        End If
        'Catch ex As Exception
        'Response.Write("p-" & ex.Message)
        'SqlHelper.ExecuteNonQuery("if not exists(select fedex_rate_id from tbl_auction_fedex_rates where auction_id=" & auction_id & " and buyer_id =" & buyer_id & " and shipping_options='') begin INSERT INTO tbl_auction_fedex_rates(auction_id, buyer_id, shipping_amount,shipping_options,error_log,is_error) VALUES (" & auction_id & ", " & buyer_id & ",0,'','p-" & ex.Message & "',1) end")
        'End Try
    End Sub
    Private Sub getFedExRate(ByVal _aucid As Int32, ByVal _qty As Int32, ByVal _ship_select As String)
        Try
            Dim dt As New DataTable()
            'If _aucid > 0 Then  (comment by ajay)
            'If _qty > 1 Then
            '--- double comment start by ajay
            ''Dim obj As New FedAllRateServices
            ''Dim objreq As New FedexRateServices.RateRequest
            ''Dim objrep As New FedexRateServices.RateReply
            ''Dim Origin_StreetLines = "", Origin_City = "", Origin_StateOrProvinceCode = "", Origin_PostalCode = "", Origin_CountryCode = "", Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, Destination_PostalCode, Destination_CountryCode, PackageLineItems_weight_value As String

            '' ''Origin_StreetLines = "3940 30th street" ' SqlHelper.of_FetchKey("fedex_source_street")
            '' ''Origin_City = "Long island city" 'SqlHelper.of_FetchKey("fedex_source_city")
            '' ''Origin_StateOrProvinceCode = "NY" ' SqlHelper.of_FetchKey("fedex_source_state")
            ''Origin_PostalCode = SqlHelper.of_FetchKey("fedex_source_zip") ' 11101
            ''Origin_CountryCode = SqlHelper.of_FetchKey("fedex_source_country_code") ' "US" 
            '--- double comment end by ajay
            Dim Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, Destination_PostalCode, Destination_CountryCode, PackageLineItems_weight_value As String ' add by ajay

            dt = SqlHelper.ExecuteDatatable("SELECT ISNULL(A.address1, '') + ' ' + ISNULL(A.address2, '')  AS address, ISNULL(A.city, '') AS city,ISNULL(A.state_text, 0) AS state_text,ISNULL(A.zip, '') AS zip,ISNULL(C.CODE, 0) AS country_code FROM tbl_reg_buyers A WITH (NOLOCK) INNER JOIN tbl_master_countries C ON A.country_id=C.country_id WHERE A.buyer_id = " & CommonCode.Fetch_Cookie_Shared("buyer_id"))
            If dt.Rows.Count > 0 Then
                With dt.Rows(0)
                    Destination_StreetLines = .Item("address").ToString().Trim()
                    Destination_City = .Item("city")
                    Destination_StateOrProvinceCode = .Item("state_text")
                    Destination_PostalCode = .Item("zip")
                    Destination_CountryCode = .Item("country_code")
                End With

            Else

                Destination_StreetLines = ""
                Destination_City = ""
                Destination_StateOrProvinceCode = ""
                Destination_PostalCode = ""
                Destination_CountryCode = ""

            End If
            dt.Dispose()

            dt = SqlHelper.ExecuteDatatable("SELECT ISNULL(A.weight, 0) AS weight FROM tbl_auctions A WITH (NOLOCK) WHERE A.auction_id =" & _aucid)
            If dt.Rows.Count > 0 Then
                With dt.Rows(0)
                    PackageLineItems_weight_value = .Item("weight") * _qty
                End With
            Else
                PackageLineItems_weight_value = "0"
            End If
            dt.Dispose()
            fetchShippingRate(_aucid, Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, Destination_PostalCode, Destination_CountryCode, CDbl(PackageLineItems_weight_value))
            '--- double comment start by ajay
            ''dt = obj.GetFedexShippingOptions(_aucid, Origin_StreetLines, Origin_City, Origin_StateOrProvinceCode, Origin_PostalCode, Origin_CountryCode, Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, Destination_PostalCode, Destination_CountryCode, 1, 1, PackageLineItems_weight_value).DefaultView.ToTable(True, "ServiceType", "Rate", "Error")
            ''Response.Write(PackageLineItems_weight_value.ToString & "_" & dt.Rows.Count)
            'Else
            '    dt = SqlHelper.ExecuteDatatable("SELECT ISNULL(A.shipping_amount, 0) AS Rate,ISNULL(A.shipping_options, '') AS ServiceType FROM tbl_auction_fedex_rates A WHERE A.auction_id=" & _aucid & " and A.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id"))
            'End If
            ''Dim error_occur As Boolean = False
            ''For i = 0 To dt.Rows.Count - 1
            ''    If dt.Rows(i).Item("Error") <> "" Then
            ''        error_occur = True
            ''    End If
            ''Next


            ''If dt.Rows.Count > 0 And error_occur = False Then
            ''    Dim dv As DataView
            ''    dv = dt.DefaultView
            ''    dv.Sort = "Rate asc"
            ''    rdo_fedex.Items.Clear()
            ''    For i As Integer = 0 To dv.Count - 1
            ''        If dv.Item(i)("Error") = "" Then
            ''                rdo_fedex.Items.Insert(i, New ListItem(dv.Item(i)("ServiceType").ToString().Replace("_", " ") & " <b>" & FormatCurrency(dv.Item(i)("Rate"), 2) & "</b>", i))

            ''        End If
            ''    Next
            ''    If rdo_fedex.Items.Count > 0 Then
            ''        rdo_fedex.SelectedIndex = 0
            ''    End If

            ''Else
            ''    lit_ship_amount.Text = "$0.00"
            ''    tbl_user_ours.Visible = False
            ''    rdo_fedex_ship.Text = "Shipping rates will be calculated later"
            ''End If
            '--- double comment end by ajay
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try

    End Sub
    Public Function check_shipping_option_added(ByVal ServiceType As String) As Boolean
        Dim is_added As Boolean = False
        For i As Integer = 0 To rdo_fedex.Items.Count - 1
            'Response.Write(rdo_fedex.Items(i).Text.ToLower().IndexOf(ServiceType.Replace("_", " ").ToString().ToLower()))
            If rdo_fedex.Items(i).Text.ToLower().IndexOf(ServiceType.Replace("_", " ").ToString().ToLower()) <> -1 Then
                is_added = True
                Exit For
            End If
        Next
        'For Each li As ListItem In rdo_fedex.Items
        '    If li.Text.ToLower.IndexOf(ServiceType.Replace("_", " ").ToString().ToLower()) <> -1 Then
        '        is_added = True
        '    End If
        'Next
        Return is_added
    End Function
    Protected Sub rdo_my_ship_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdo_my_ship.CheckedChanged
        If rdo_my_ship.Checked Then
            tbl_user_own.Visible = True
            tbl_user_ours.Visible = False
            lit_grand_total.Text = FormatCurrency(CDbl(lit_grand_total.Text.Trim.Replace("$", "").Replace(",", "")) - CDbl(lit_ship_amount.Text.Trim.Replace("$", "").Replace(",", "")), 2)
            lit_ship_amount.Text = "$0.00"
        End If
        'upd_items.Update()
    End Sub
    Protected Sub rdo_fedex_ship_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdo_fedex_ship.CheckedChanged, rdo_fedex.SelectedIndexChanged
        If rdo_fedex_ship.Checked Then
            If rdo_fedex.Items.Count > 0 Then
                tbl_user_ours.Visible = True
            Else
                tbl_user_ours.Visible = False
            End If

            tbl_user_own.Visible = False
            Dim strShipping_option = "", temp As String = "0"
            Dim strShippingAmount As Double = 0
            If rdo_fedex.Items.Count > 0 Then
                If rdo_fedex.SelectedIndex > -1 AndAlso rdo_fedex.SelectedItem.Text.Trim.Contains("$") Then
                    strShipping_option = rdo_fedex.SelectedItem.Text.Trim.Remove(rdo_fedex.SelectedItem.Text.Trim.IndexOf("$")).Trim
                    strShipping_option = strShipping_option.Replace("<b>", "")
                    temp = rdo_fedex.SelectedItem.Text.Trim.Remove(0, rdo_fedex.SelectedItem.Text.Trim.IndexOf("$")).Replace("</b>", "")
                Else
                    If rdo_fedex.SelectedItem IsNot Nothing Then
                        strShipping_option = rdo_fedex.SelectedItem.Text
                    End If
                End If
            End If

            strShippingAmount = CDbl(temp)
            lit_ship_amount.Text = FormatCurrency(strShippingAmount, 2)
            lit_grand_total.Text = FormatCurrency(CDbl(lit_sub_total.Text.Trim.Replace("$", "").Replace(",", "")) + CDbl(lit_ship_amount.Text.Trim.Replace("$", "").Replace(",", "")), 2)
        End If
        'upd_items.Update()
    End Sub
    'Protected Sub rdo_option_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdo_option.SelectedIndexChanged
    '    If rdo_option.SelectedItem.Value = "1" Then
    '        tbl_user_own.Visible = True
    '        tbl_user_ours.Visible = False
    '    Else
    '        tbl_user_ours.Visible = True
    '        tbl_user_own.Visible = False
    '    End If
    'End Sub
    Protected Sub img_but_confirm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_but_confirm.Click
        If _AUCTION_ID > 0 And (Not String.IsNullOrEmpty(_BUY_TYPE)) And (Not String.IsNullOrEmpty(_TYPE)) Then
            Dim dtBid As New DataTable
            dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(B.bid_amount, 0) AS bid_amount,ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value from tbl_auction_bids B WITH (NOLOCK) inner join tbl_auctions A WITH (NOLOCK) on A.auction_id=B.auction_id  where A.auction_id=" & Request.QueryString.Get("a") & " order by bid_amount desc")
            If dtBid.Rows.Count > 0 Then
                With dtBid.Rows(0)
                    If Not (.Item("bid_amount") < .Item("thresh_hold_value")) And .Item("thresh_hold_value") > 0 Then
                        pnl_ord_cnfm.Visible = False
                        dv_cnfm_msg.Visible = False
                        lbl_popmsg_msg.Text = "Sorry, This offer is currently not available. Please bid to win this product."
                        Exit Sub
                    End If
                End With
            End If
        End If
        Dim auc_stat As Integer = SqlHelper.ExecuteScalar("select dbo.get_auction_status(" & _AUCTION_ID & ")")
        Select Case auc_stat
            Case 2

                pnl_ord_cnfm.Visible = False
                dv_cnfm_msg.Visible = False
                lbl_popmsg_msg.Text = "Sorry, but we’ve sold out of this product. Please check back regularly for the opportunity to participate in future auctions."

            Case 3
                pnl_ord_cnfm.Visible = False
                dv_cnfm_msg.Visible = False
                lbl_popmsg_msg.Text = "Sorry, but we’ve sold out of this product. Please check back regularly for the opportunity to participate in future auctions."
            Case Else

                Dim avail_qty As Integer = SqlHelper.ExecuteScalar("select isnull(total_qty,0) -(select isnull(sum(isnull(quantity,0)),0) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and auction_id=A.auction_id) from tbl_auctions A WITH (NOLOCK) where auction_id=" & _AUCTION_ID)

                If avail_qty < _QUANTITY And _BUY_TYPE = "buy now" Then
                    pnl_ord_cnfm.Visible = False
                    dv_cnfm_msg.Visible = False
                    lbl_popmsg_msg.Text = "Sorry, only " & avail_qty & " quantity are available at this moment. Please try again."
                Else
                    Dim obj As New Bid
                    Dim buy_type As String = ""
                    Dim tax As Double = 0
                    Dim buy_id As Int32 = 0
                    Dim strShipping_value = "", temp = "0", strShipping_option As String = ""
                    Dim strShippingAmount As Double = 0
                    If tr_ship_option.Visible = False Then
                        tr_ship_option.Visible = False
                        strShipping_value = lbl_shipping_option.Text
                        strShippingAmount = CDbl(lit_ship_amount.Text.Replace("$", "").Replace(",", ""))
                    Else

                        If rdo_my_ship.Checked Then
                            strShipping_value = "My shipping carrier is: " & txt_shipping_type.Text.Trim() & " And My Account # is: " & txt_shipping_account_number.Text.Trim()
                            strShipping_option = "Used my own shipping carrier"
                        Else
                            If rdo_fedex.Items.Count > 0 Then
                                Dim dtTable As DataTable = SqlHelper.ExecuteDatatable("select ISNULL(A.shipping_amount, 0) AS shipping_amount,ISNULL(A.shipping_options, '') AS shipping_options from tbl_auction_fedex_rates A WITH (NOLOCK) where A.fedex_rate_id=" & rdo_fedex.SelectedValue)
                                If dtTable.Rows.Count > 0 Then
                                    strShipping_option = dtTable.Rows(0).Item("shipping_options")
                                    strShippingAmount = dtTable.Rows(0).Item("shipping_amount")
                                End If
                                If rdo_fedex.SelectedItem.Text.Trim.Contains("$") Then
                                    strShipping_option = rdo_fedex.SelectedItem.Text.Trim.Remove(rdo_fedex.SelectedItem.Text.Trim.IndexOf("$")).Trim
                                    strShipping_option = strShipping_option.Replace("<b>", "")
                                    temp = rdo_fedex.SelectedItem.Text.Trim.Remove(0, rdo_fedex.SelectedItem.Text.Trim.IndexOf("$")).Replace("</b>", "")
                                Else
                                    strShipping_option = rdo_fedex.SelectedItem.Text
                                End If
                                strShippingAmount = CDbl(temp)
                                strShipping_value = "My shipping option: " & rdo_fedex.SelectedItem.Text
                            End If

                        End If
                    End If
                    buy_type = obj.save_buy_now_with_confirm(_AUCTION_ID, _BUY_TYPE, _PRICE, _QUANTITY, CommonCode.Fetch_Cookie_Shared("user_id"), CommonCode.Fetch_Cookie_Shared("buyer_id"), strShipping_value, strShipping_option, strShippingAmount, tax, lit_confirm_no.Text.Trim)

                    If buy_type <> "" AndAlso buy_type.Contains("#") Then
                        Dim upd_qry As String = ""
                        Dim shipping_type As String = txt_shipping_type.Text.Trim()
                        Dim shipping_account_number As String = txt_shipping_account_number.Text.Trim()
                        Dim shipping_preference As String = IIf(rdo_my_ship.Checked, "1", "2")

                        Dim last_shipping_option As String = ""

                        If rdo_my_ship.Checked Then
                            last_shipping_option = "Use my own shipping carrier to ship this item to me"
                        Else
                            If rdo_fedex.Items.Count > 0 Then
                                If last_shipping_option.IndexOf(" <b>") > 0 Then
                                    last_shipping_option = last_shipping_option.Substring(0, last_shipping_option.IndexOf(" <b>"))
                                Else
                                    last_shipping_option = "Shipping rates will be calculated later"
                                End If
                            Else
                                last_shipping_option = "Shipping rates will be calculated later"
                            End If

                        End If

                        upd_qry = "update tbl_reg_buyers set shipping_type='" & shipping_type & "', shipping_account_number='" & shipping_account_number & "', shipping_preference='" & shipping_preference & "', last_shipping_option='" & last_shipping_option & "' where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id")
                        SqlHelper.ExecuteNonQuery(upd_qry)
                        pnl_ord_cnfm.Visible = False
                        dv_cnfm_msg.Visible = False
                        Dim str() As String = buy_type.Trim.Split("#")
                        Dim pg_name As String = ""
                        Dim mail_body As String = ""
                        Dim com As New CommonCode

                        If str(0) = "buy now" Then
                            buy_id = str(1)
                            lbl_popmsg_msg.Text = "Thank you for submitting your order."
                        ElseIf str(0) = "private" Then
                            buy_id = str(1)
                            lbl_popmsg_msg.Text = "Thank you for submitting private order."
                        ElseIf str(0) = "partial" Then
                            buy_id = str(1)
                            lbl_popmsg_msg.Text = "Thank you for submitting partial order."
                        End If


                        If Request.RawUrl.ToString.Contains("?") Then
                            pg_name = Request.RawUrl.ToString.Remove(Request.RawUrl.ToString.IndexOf("?")).Replace("/", "")
                            com.Send_Confirm_Receipt_Mail(buy_id, pg_name.Trim, "b")
                        End If
                        'Response.Write(pg_name.Trim)
                        com = Nothing
                    End If
                    'Dim ins_qry As String = ""
                    'ins_qry = "insert into tbl_auction_buy (auction_id,buyer_id,buyer_user_id,bid_date,buy_type,price,quantity,status,action,shipping_value,confirmation_no,shipping_amount,shipping_option,amount,sub_total_amount,tax,grand_total_amount) values (" & _
                    '    "select auction_id," & CommonCode.Fetch_Cookie_Shared("buyer_id") & "," & CommonCode.Fetch_Cookie_Shared("user_id") & ",getdate(),case when isnull(is_buy_it_now,0)=1 then 'buy now' else case when isnull(is_partial_offer,0)=1 then 'partial' else 'private' end end," & _
                    '    "p,q,"
                End If
        End Select





    End Sub

    Protected Sub img_but_email_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_but_email.Click
        If Request.RawUrl.ToString.Contains("?") Then
            Dim pg_name As String = ""
            Dim com As New CommonCode
            pg_name = Request.RawUrl.ToString.Remove(Request.RawUrl.ToString.IndexOf("?")).Replace("/", "")
            com.Send_Confirm_Receipt_Mail(_ID, pg_name, _TYPE)
            pnl_ord_cnfm.Visible = False
            lbl_popmsg_msg.Text = "Email sent successfully."
            com = Nothing
        End If
    End Sub

End Class
