﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master"  AutoEventWireup="false" CodeFile="AdminReports.aspx.vb" Inherits="Reports_AdminReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" Runat="Server"  >



    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                   Dovi Custom Reports</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; width: 40%; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    Please select the criteria to filter the report
                    <br />
                    <br />
                    <br />
                    <br />
                </div>
                
            </td>
        </tr>
        </table>

    <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
				
				<TR>
					<TD class="auto-style1" >
						<B>Select a Date or Date Range For Highest bid, per auction, per user Report:</B>
                    </TD></TR>
                <TR>
					<TD class="auto-style1" align="left">   <B>&nbsp; &nbsp;From Date:&nbsp;&nbsp;</B>
							<asp:textbox id="startdate" runat="server" Width="86px"></asp:textbox>&nbsp;
                        	<asp:CompareValidator     id="dateValidator" runat="server"     Type="Date"    Operator="DataTypeCheck"    ControlToValidate="startdate"     ErrorMessage="Please enter a valid date."> </asp:CompareValidator>
							&nbsp;&nbsp;<asp:imagebutton id="ibselectFrom" runat="server" ImageUrl="/Images/calendar.gif" OnClick="ibselectFrom_Click1"></asp:imagebutton>&nbsp;
							<asp:calendar id="Calendar1" runat="server" BorderStyle="Solid" BorderColor="Blue" Visible="False"  OnSelectionChanged="Calendar1_SelectionChanged1"></asp:calendar>
                        
                    </TD></TR>
				<tr>
                    <td class="auto-style1"align="left" >		 <B>&nbsp;&nbsp;To Date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </B>
						<asp:textbox id="enddate" runat="server" Width="86px"></asp:textbox>&nbsp;       
                        <asp:CompareValidator     id="CompareValidator1" runat="server"     Type="Date"    Operator="DataTypeCheck"    ControlToValidate="enddate"     ErrorMessage="Please enter a valid date."> </asp:CompareValidator>	
                        &nbsp; &nbsp;<asp:imagebutton id="ibselectTo" runat="server" ImageUrl="/Images/calendar.gif" OnClick="ibselectTo_Click1" ></asp:imagebutton>&nbsp;
							<asp:calendar id="Calendar2" runat="server" Visible="False" BorderColor="Blue" BorderStyle="Solid" OnSelectionChanged="Calendar2_SelectionChanged1"></asp:calendar>
						
</td></TR>
					<TR>
					<TD class="auto-style1">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<P>  <asp:button id="btReport1" runat="server" Width="285px" align="center"  text="Get a Highest bid, per auction, per user" OnClick="btReport1_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</P>
							<P>
                            <asp:button id="btReport2" runat="server" Width="283px" align="center"  text="Get a List of approved bidders" OnClick="btReport2_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;</P>
                            <P><asp:button id="btReport3" runat="server" Width="281px"  align="center"  text="Get a List of pending bidders" OnClick="btReport3_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;</P>
						
					</TD>
				</TR>
				</table>
</asp:Content>

