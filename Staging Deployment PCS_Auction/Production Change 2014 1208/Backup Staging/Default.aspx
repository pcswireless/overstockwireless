﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<%@ Register Src="~/UserControls/login_ctrl.ascx" TagName="Login" TagPrefix="UC1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>:: Overstock Wireless :: The PCS Wireless Auction Site</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link href="/Common/newlook.css" rel="stylesheet" type="text/css" />
    <link href="/Common/responsive.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
  <script src="/js/html5.js"></script>
<![endif]-->
    <script type="text/javascript">
        function open_pop() {
            window.open('/terms-conditions.html', '_terms_conditions', 'left=' + ((screen.width - 600) / 2) + ',top=' + ((screen.height - 500) / 2) + ',width=600,height=500,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=no');
            window.focus();
            return false;
        }

    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-22465035-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sc_1">
    </asp:ScriptManager>
    <UC1:Login runat="server" ID="signin_ctrl" />
    <header>
  <div class="inner">
    <div class="logo"><img src="/Images/logo.png" width="317" height="64"></div>
    <nav>
      <ul>
        <% If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then%>
          <li><a><%= CommonCode.Fetch_Cookie_Shared("displayname")%></a></li> 
        <li><a href='/my-account.html'>My Account</a></li>
        <li><a class="selected" href="/logout.aspx">Logout</a></li>
           <% Else%>
        <li><a href="/">Home</a></li> 
        <li><a href='/sign-up.html'>Join Now</a></li>
        <li><a class="selected" onclick="return Modal_Dialog();" href="/login.html">Login</a></li> 
           <% End If%> 
      </ul>
    </nav>
  </div>
</header>
    <section id="welcome">
      <div class="inner">
        <h1>Welcome to Overstock Wireless</h1>
        <p>We are a private auction site exclusively for resellers and the only auction site that leverages the global open market distribution network of PCS Wireless. Sign up is FREE and there is no obligation to place a bid ever.</p>
        <div class="button"><a href="/sign-up.html">Join Now</a></div>
      </div>
    </section>
    <section id="auctions">
      <div class="inner">
        <h1><span>Our auction</span> features:</h1>
        <ul>
          <li>Mobile phones, tablets, aircards and accessories</li>
          <li>A wide variety of brands</li>
          <li>
            <ul>
              <li>GSM</li>
              <li>CDMA</li>
              <li>Unlocked</li>
            </ul>
          </li>
          <li>
            <ul>
              <li>3G</li>
              <li>4G</li>
              <li>WiFi</li>
            </ul>
          </li>
          <li>Brand new, certiﬁed manufacturer refurbished, A B and C stock, used and broken devices</li>
        </ul>
        <div class="button"><a href="/live-auctions.html">View current auctions</a></div>
      </div>
    </section>
    <section id="members">
      <div class="inner">
        <div class="text">
          <h1><span>Our members</span> enjoy:</h1>
          <ul>
            <li>No bidding fees</li>
            <li>Auctions without reserves</li>
            <li>Ability to pay by PayPal</li>
          </ul>
        </div>
        <div class="icons">
          <ul>
            <li><img src="/Images/newlook/icon-shop.png"></li>
            <li><img src="/Images/newlook/icon-paypal.png"></li>
            <li><img src="/Images/newlook/icon-wiretransfer.png"></li>
          </ul>
        </div>
      </div>
    </section>
    <section id="reasonsResellers">
      <div class="inner">
        <h1><span>Reasons resellers</span> choose us:</h1>
        <ul>
          <li>Objective and consistent product gradings</li>
          <li>Inventory owned and managed by PCS Wireless</li>
          <li>Hot models and hard to find products</li>
          <li>Unique and engaging auction platform</li>
        </ul>
      </div>
    </section>
    <section id="howWorks">
      <div class="inner">
        <h1>How does Overstock Wireless work?</h1>
        <ul>
          <li><span>1</span>Complete the quick and easy sign-up form</li>
          <li><span>2</span>Receive e-mail invites to auctions based on your preferences</li>
          <li><span>3</span>Join auctions that interest you to see product details and watch the bidding</li>
          <li><span>4</span>Bid when the products and pricing are right for you</li>
          <li><span>5</span>Pay when you win an auction</li>
	      <li><span>6</span>Receive your products direct from Overstock Wireless</li>
        </ul>
        <div class="button"><a href="/sign-up.html">Get started today</a></div>
      </div>
    </section>
    <footer>
  <div class="inner">
    <div class="leftCol">
      <p>Owned and operated by <a href="http://www.pcsww.com/" target="_blank"><img src="/Images/newlook/pcs-wireless.png"></a></p>
      <p><a href="/terms-conditions.html" onclick="return open_pop();">Terms & Conditions</a><br>Copyright 2013-2014</p>
    </div>
    <div class="rightCol">
      <h1>Contact Us</h1>
      <p>+1 973 805 7400 ext 179<br>
      <a href="mailto:support@overstockwireless.com">support@overstockwireless.com</a><br>
      11 Vreeland Road<br>
      Florham Park, NJ 07932</p>
    </div>
  </div>
</footer>
    </form>
</body>
</html>
