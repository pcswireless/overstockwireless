﻿Imports System
Imports System.Web.UI.WebControls
Imports System.Web.Services.Protocols
Imports ecommercemax
Partial Class FedExTest
    Inherits System.Web.UI.Page

    'fedex userid: recyclephone
    'Fedex password: pcc@account1
    'Required for All Web Services 
    'Developer Test Key:   WEQyExONhWPmdgbd  
    'Required for FedEx Web Services for Shipping  
    'Test Account Number:   510087186  
    'Test Meter Number:   118555715  
    'Required for FedEx Web Services for Office and Print  
    'Test FedEx Office Integrator ID:   123  
    'Test Client Product ID:   TEST  
    'Test Client Product Version:   9999  


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ' Step 1 of 3
        ' INITIALIZE COMPONENT
        ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        ' Instantiate the master component variable
        Dim fdx As New EMX_FDXRatesAvailable()

        fdx.EMX_RatesAvailableServerUrl = "https://gatewaybeta.fedex.com:443/web-services/rate"  ' LIVE server is  "https:'ws.fedex.com:443/web-services/rate"
        fdx.EMX_UserCredentialKey = "recyclephone"
        fdx.EMX_UserCredentialPassword = "56dk0CdKioRTy7TQxZWIEqmuE"
        fdx.EMX_AccountNumber = "510087186"
        fdx.EMX_MeterNumber = "118555715"

        fdx.EMX_RequestedShipmentDropoffType = DropoffType.REGULAR_PICKUP
        fdx.EMX_RequestedShipmentPackagingType = PackagingType.YOUR_PACKAGING
        fdx.EMX_ShipperAddressCity = "Beverly Hills"  ' <--- SET YOUR OWN CUSTOM VALUE
        fdx.EMX_ShipperAddressStateOrProvinceCode = "CA" ' <--- SET YOUR OWN CUSTOM VALUE
        fdx.EMX_ShipperAddressPostalCode = "90210" ' <--- SET YOUR OWN CUSTOM VALUE
        fdx.EMX_ShipperAddressCountryCode = "US" ' <---SET YOUR OWN CUSTOM VALUE
        fdx.EMX_RecipientAddressCity = "Windsor" ' <--- SET YOUR OWN CUSTOM VALUE
        fdx.EMX_RecipientAddressStateOrProvinceCode = "CT" ' <--- SET YOUR OWN CUSTOM VALUE
        fdx.EMX_RecipientAddressPostalCode = "06006" ' <--- SET YOUR OWN CUSTOM VALUE
        fdx.EMX_RecipientAddressCountryCode = "US" ' <--- SET YOUR OWN CUSTOM VALUE
        fdx.EMX_RecipientAddressResidential = False

        ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ' Step 2 of 3
        ' ADD PACKAGE(s)
        ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        Dim pkg1 As New EMX_Package()
        pkg1.WeightValue = 15.0

        fdx.EMX_AddPackage(pkg1)
        'EMX_Package pkg2 = new EMX_Package()
        'pkg2.WeightValue = 2.0M
        'fdx.EMX_AddPackage(pkg2)

        ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ' Any overrides can be created here by assigning it to "request" object.
        ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ' fdx.EMX_request.RequestedShipment.SpecialServicesRequested. etc etc...

        ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ' Step 3 of 3
        ' FINALIZE and EXECUTE
        ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        Try
            fdx.EMX_FinalizeRequest()
            TextBox1.Text = fdx.EMX_RequestXML ' --> If you want to display the actual XML request, useful for debugging purposes

            Dim reply As RateReply = fdx.EMX_Execute()
            TextBox2.Text = fdx.EMX_ReplyXML ' --> If you want to display the actual XML reply, useful for debugging purposes

            If reply.HighestSeverity = NotificationSeverityType.SUCCESS Or reply.HighestSeverity = NotificationSeverityType.NOTE Or reply.HighestSeverity = NotificationSeverityType.WARNING Then

                ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                ' Now just DISPLAY THE RESULTS
                ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                ShowRateReply(reply)
            Else
                ListBox1.Items.Add(reply.Notifications(0).Message)
            End If
        Catch ee As SoapException
            ListBox1.Items.Add(ee.Detail.InnerText)
        Catch ee As Exception
            ListBox1.Items.Add(ee.Message)
        End Try


    End Sub

    Private Sub ShowRateReply(ByVal reply As RateReply)

        ' --------------------------------------------------------------------------------
        ' Show Simple results (List or Public rates)
        ' --------------------------------------------------------------------------------
        Dim i As Integer
        Dim j As Integer
        For i = 0 To reply.RateReplyDetails.Length - 1
            Dim listrate_item As New ListItem()

            listrate_item.Text = "Service Type: " & reply.RateReplyDetails(i).ServiceType

            If reply.RateReplyDetails(i).DeliveryTimestampSpecified Then
                listrate_item.Text = listrate_item.Text & " | Delivery timestamp " & reply.RateReplyDetails(i).DeliveryTimestamp
            End If
            If reply.RateReplyDetails(i).TransitTimeSpecified Then
                listrate_item.Text = listrate_item.Text & " | Transit Time: " & reply.RateReplyDetails(i).TransitTime
            End If

            ' Extract value of PAYOR_ACCOUNT_PACKAGE
            Dim rateReplyDetail As RateReplyDetail = reply.RateReplyDetails(i)

            For j = 0 To rateReplyDetail.RatedShipmentDetails.Length - 1
                Dim RatedShipmentDetail As RatedShipmentDetail = rateReplyDetail.RatedShipmentDetails(j)
                If RatedShipmentDetail.ShipmentRateDetail.RateType = ReturnedRateType.PAYOR_LIST_PACKAGE Then
                    If Not RatedShipmentDetail.ShipmentRateDetail Is Nothing Then
                        listrate_item.Value = RatedShipmentDetail.ShipmentRateDetail.TotalNetCharge.Amount.ToString()
                        listrate_item.Text = "$" & listrate_item.Value.ToString() & " | " & listrate_item.Text
                    End If
                End If
            Next
            DropDown_ListRates.Items.Add(listrate_item)
        Next

        ' --------------------------------------------------------------------------------
        ' Show Simple results (Account, Discounted or Negotiated rates) 
        ' These rates may be different, than list or public rates depending on your particular account
        ' --------------------------------------------------------------------------------
        For i = 0 To reply.RateReplyDetails.Length - 1
            Dim payor_item As New ListItem()

            payor_item.Text = "Service Type: " & reply.RateReplyDetails(i).ServiceType
            If reply.RateReplyDetails(i).DeliveryTimestampSpecified Then
                payor_item.Text = payor_item.Text & " | Delivery timestamp " & reply.RateReplyDetails(i).DeliveryTimestamp
            End If
            If reply.RateReplyDetails(i).TransitTimeSpecified Then
                payor_item.Text = payor_item.Text & " | Transit Time: " & reply.RateReplyDetails(i).TransitTime
            End If

            ' Extract value of PAYOR_ACCOUNT_PACKAGE
            Dim rateReplyDetail As RateReplyDetail = reply.RateReplyDetails(i)
            For j = 0 To rateReplyDetail.RatedShipmentDetails.Length - 1
                Dim ratedShipmentDetail As RatedShipmentDetail = rateReplyDetail.RatedShipmentDetails(j)
                If ratedShipmentDetail.ShipmentRateDetail.RateType = ReturnedRateType.PAYOR_ACCOUNT_PACKAGE Then
                    If Not ratedShipmentDetail.ShipmentRateDetail Is Nothing Then
                        payor_item.Value = ratedShipmentDetail.ShipmentRateDetail.TotalNetCharge.Amount.ToString()
                        payor_item.Text = "$" & payor_item.Value.ToString() & " | " & payor_item.Text
                    End If
                End If
            Next
            DropDown_NegotiatedRates.Items.Add(payor_item)
        Next


        ' *************************************************************************************************
        ' Show Other Detailed Results:
        ' Usually, just the Shipping Cost, Service Type and Shipping Transit Time are enough for
        ' most shipping rates interface. However, if there are other values that you need to access
        ' then this block below shows many more complex ways of getting to those other results.
        ' *************************************************************************************************
        ListBox1.Items.Add("RateReply details:")
        ListBox1.Items.Add("**********************************************************")
        For i = 0 To reply.RateReplyDetails.Length - 1
            Dim rateReplyDetail As RateReplyDetail = reply.RateReplyDetails(i)
            ListBox1.Items.Add(String.Format("Rate Reply Detail for Service Type {0} ", i + 1))
            ListBox1.Items.Add("Service Type: " & rateReplyDetail.ServiceType)
            ListBox1.Items.Add("Packaging Type: " & rateReplyDetail.PackagingType)

            For j = 0 To rateReplyDetail.RatedShipmentDetails.Length - 1
                Dim ratedShipmentDetail As RatedShipmentDetail = rateReplyDetail.RatedShipmentDetails(j)
                ListBox1.Items.Add(String.Format("---Rated Shipment Detail for Rate Type {0}---", j + 1))
                ListBox1.Items.Add("Rate Type : " + ratedShipmentDetail.ShipmentRateDetail.RateType)
                If Not ratedShipmentDetail.ShipmentRateDetail Is Nothing Then
                    ShowShipmentRateDetails(ratedShipmentDetail.ShipmentRateDetail)
                End If
                If Not ratedShipmentDetail.RatedPackages Is Nothing Then
                    ShowPackageRateDetails(ratedShipmentDetail.RatedPackages)
                End If
            Next
            ShowDeliveryDetails(rateReplyDetail)
            ListBox1.Items.Add("**********************************************************")
        Next
    End Sub

    Private Sub ShowShipmentRateDetails(ByVal shipmentRateDetail As ShipmentRateDetail)
        ListBox1.Items.Add("--- Shipment Rate Detail ---")
        ListBox1.Items.Add(String.Format("Total billing weight {0} {1}", shipmentRateDetail.TotalBillingWeight.Value, shipmentRateDetail.TotalBillingWeight.Units))
        ListBox1.Items.Add(String.Format("    Total surcharges {0} {1}", shipmentRateDetail.TotalSurcharges.Amount, shipmentRateDetail.TotalSurcharges.Currency))
        ListBox1.Items.Add(String.Format("    Total net charge {0} {1}", shipmentRateDetail.TotalNetCharge.Amount, shipmentRateDetail.TotalNetCharge.Currency))
    End Sub

    Private Sub ShowPackageRateDetails(ByVal ratedPackages As RatedPackageDetail())
        ListBox1.Items.Add("--- Rated Package Detail ---")
        Dim k As Integer
        For k = 0 To ratedPackages.Length - 1
            Dim ratedPackage As RatedPackageDetail = ratedPackages(k)
            ListBox1.Items.Add(String.Format("Package {0}", k + 1))
            If Not ratedPackage.PackageRateDetail Is Nothing Then
                ListBox1.Items.Add(String.Format("Billing weight {0} {1}", ratedPackage.PackageRateDetail.BillingWeight.Value, ratedPackage.PackageRateDetail.BillingWeight.Units))
                ListBox1.Items.Add(String.Format("   Base charge {0} {1}", ratedPackage.PackageRateDetail.BaseCharge.Amount, ratedPackage.PackageRateDetail.BaseCharge.Currency))
                If Not ratedPackage.PackageRateDetail.Surcharges Is Nothing Then
                    Dim surcharge As Surcharge
                    For Each surcharge In ratedPackage.PackageRateDetail.Surcharges
                        ListBox1.Items.Add(String.Format("{0} surcharge {1} {2}", surcharge.SurchargeType, surcharge.Amount.Amount, surcharge.Amount.Currency))
                    Next
                End If
                ListBox1.Items.Add(String.Format("    Net charge {0} {1}", ratedPackage.PackageRateDetail.NetCharge.Amount, ratedPackage.PackageRateDetail.NetCharge.Currency))
            End If
        Next
    End Sub

    Private Sub ShowDeliveryDetails(ByVal rateReplyDetail As RateReplyDetail)
        If rateReplyDetail.DeliveryTimestampSpecified Then
            ListBox1.Items.Add("Delivery timestamp " + rateReplyDetail.DeliveryTimestamp)
        End If
        If rateReplyDetail.TransitTimeSpecified Then
            ListBox1.Items.Add("Transit Time: " + rateReplyDetail.TransitTime)
        End If
    End Sub
End Class
