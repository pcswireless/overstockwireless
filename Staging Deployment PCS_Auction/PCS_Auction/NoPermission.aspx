﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="NoPermission.aspx.vb" Inherits="NoPermission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Sorry</div>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; padding-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                You do not have permission to view this page.
            </td>
        </tr>
    </table>
</asp:Content>
