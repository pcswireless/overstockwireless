﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FedExTest.aspx.vb" Inherits="FedExTest" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <title></title>
  <style type="text/css">
    #form1 { width:800px; margin-left:auto; margin-right:auto;}
    ul { list-style-type:none;}
    label { clear:both; display:block; margin-top:20px; font-weight:bold;}
  </style>
</head>
<body>
  <form id="form1" runat="server">
  <div>
  <h1><a href="http://ecommercemax.com">Ecommercemax Solutions</a></h1>
    <fieldset>
      <legend>Fedex "Rates Available Service" - demo</legend>
      <ul>
        <li>
          <label>
          Make sure you have modified "Default.aspx.cs" to specify your own user credentials and shipping values, then click Execute button.
          </label>
          <asp:Button ID="Button1" runat="server"  Text="Execute" />
        </li>
        <li>
          <label>
            Simple results (List or Public rates):</label>
          <asp:DropDownList ID="DropDown_ListRates" runat="server">
          </asp:DropDownList>
        </li>        
        <li>
          <label>
            Simple results (Account, Discounted or Negotiated rates):</label>
          <asp:DropDownList ID="DropDown_NegotiatedRates" runat="server">
          </asp:DropDownList>
        </li>
        <li>
          <label>
            Detailed results:</label>
          <asp:ListBox ID="ListBox1" runat="server" Height="196px" Width="439px"></asp:ListBox>
        </li>
        <li>
          <label>
            XML Request:</label>
          <asp:TextBox ID="TextBox1" runat="server" Height="89px" TextMode="MultiLine" Width="491px"></asp:TextBox>
        </li>
        <li>
          <label>
            XML Reply</label>
          <asp:TextBox ID="TextBox2" runat="server" Height="89px" TextMode="MultiLine" Width="491px"></asp:TextBox>
        </li>
      </ul>
    </fieldset>
  </div>
  </form>
</body>
</html>