﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AuctionMain.master" AutoEventWireup="false"
    CodeFile="terms_conditions.aspx.vb" Inherits="terms_conditions" %>
<%@ Register Src="~/UserControls/bidder_salesrep.ascx" TagName="SalesRep" TagPrefix="UC2" %>
<%@ Register Src="~/UserControls/bidder_salesrep.ascx" TagName="SalesRep" TagPrefix="UC1" %>
<%@ Register Src="~/UserControls/terms.ascx" TagName="Terms" TagPrefix="UC3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <div class="detail">
        <div class="innerwraper">
            <UC1:SalesRep runat="server" ID="UC_SalesRep"></UC1:SalesRep>
            <UC3:Terms runat="server" ID="UC_Terms"></UC3:Terms>
        </div>
    </div>
   
</asp:Content>
