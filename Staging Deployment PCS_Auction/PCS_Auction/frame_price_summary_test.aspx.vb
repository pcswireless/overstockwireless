﻿
Partial Class frame_price_summary_test
    Inherits System.Web.UI.Page
    Dim meta As New HtmlMeta
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not CommonCode.Fetch_Cookie_Shared("buyer_id") Is Nothing And CommonCode.Fetch_Cookie_Shared("buyer_id") <> "" Then
            pnl_price.Visible = True
            pnl_session.Visible = False
            If Not Page.IsPostBack Then
                Dim str As String = "SELECT A.auction_id,"
                str = str & "ISNULL(A.product_catetory_id, 0) AS product_catetory_id,"
                str = str & "ISNULL(A.auction_category_id, 0) AS auction_category_id,"
                str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"
                str = str & "ISNULL(A.start_price, 0) AS start_price,"
                str = str & "ISNULL(A.reserve_price, 0) AS reserve_price,"
                str = str & "dbo.get_auction_status(A.auction_id) as auction_status,"
                str = str & "ISNULL(A.is_show_reserve_price, 0) AS is_show_reserve_price,"
                str = str & "ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing,"
                str = str & "ISNULL(A.is_show_no_of_bids, 0) AS is_show_no_of_bids,"
                str = str & "ISNULL(A.is_show_increment_price, 0) AS is_show_increment_price,"
                str = str & "ISNULL(A.increament_amount, 0) AS increament_amount"
                str = str & " FROM "
                str = str & "tbl_auctions A WITH (NOLOCK) WHERE "
                str = str & "A.auction_id =" & Request.QueryString.Get("i")

                Dim dtTable As New DataTable()
                dtTable = SqlHelper.ExecuteDatatable(str)

                If dtTable.Rows.Count > 0 Then
                    With dtTable.Rows(0)
                        If .Item("auction_status") <> 3 Then
                            Me.litRefresh.Visible = True
                            'meta = New HtmlMeta
                            'meta.Attributes.Add("http-equiv", "Refresh")
                            'meta.Attributes.Add("content", "5")
                            'Me.Page.Header.Controls.Add(meta)
                            'meta = Nothing
                        Else
                            Me.litRefresh.Visible = False
                        End If
                        If .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then

                            Dim dtHBid As New DataTable()
                            dtHBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i") & " order by bid_amount desc")

                            Dim c_bid_amount As Double = 0
                            If dtHBid.Rows.Count > 0 Then
                                c_bid_amount = dtHBid.Rows(0).Item("bid_amount")
                            End If

                            Dim max_bid_amount As Double = 0
                            Dim bid_amount As Double = 0
                            Dim dtBid As New DataTable()
                            dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i") & " and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by bid_amount desc")

                            If dtBid.Rows.Count > 0 Then
                                With dtBid.Rows(0)
                                    max_bid_amount = .Item("max_bid_amount")
                                    bid_amount = .Item("bid_amount")
                                End With
                            End If
                            dtBid.Dispose()


                            Dim div_count As Integer = 1
                            Dim count As Integer = 0
                            Dim strTable As String = "" '& "<div class= 'prodetail' >"


                            'If bid_amount > 0 Then
                            '    If .Item("auction_type_id") = 3 Then

                            '        If Request.QueryString.Get("j") = "g" Then

                            '            '   strTable = strTable & "<div class='flttxt flt1'>My Current bid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>: $" & FormatNumber(bid_amount, 2) & "</span></div>"
                            '            '    strTable = strTable & "<div class='flttxt flt2'>Auto maximum bid&nbsp;&nbsp;<span>: $" & FormatNumber(max_bid_amount, 2) & "</span></div>"
                            '        Else
                            '            '    strTable = strTable & "<div class='flttxt flt1'>My Current bid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>: $" & FormatNumber(bid_amount, 2) & "</span></div><div class='flttxt flt2'>Auto maximum bid&nbsp;&nbsp;<span>: $" & FormatNumber(max_bid_amount, 2) & "</span></div>"
                            '        End If
                            '        'div_count = div_count + 2


                            '    Else
                            '        strTable = strTable & "<div class='flttxt flt1'>My Current bid&nbsp;&nbsp;<span>: $" & FormatNumber(bid_amount, 2) & "</span></div>"
                            '    End If
                            'End If

                            strTable = strTable & ""

                            'If Convert.ToBoolean(.Item("is_show_actual_pricing")) Then
                            '    If c_bid_amount > .Item("start_price") Then
                            '        strTable = strTable & "<div class='flttxt flt" & div_count & "'>Highest Bid&nbsp;&nbsp;<span>: $" & FormatNumber(c_bid_amount, 2) & "</span></div>"
                            '        count = count + 1
                            '    Else
                            '        strTable = strTable & "<div class='flttxt flt" & div_count & "'>Start Price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>: $" & FormatNumber(.Item("start_price"), 2) & "</span></div>"
                            '        count = count + 1
                            '    End If
                            '    If Request.QueryString.Get("j") = "g" Then strTable = strTable & ""
                            '    div_count = div_count + 1
                            'End If


                            'If .Item("is_show_increment_price") Then
                            '    strTable = strTable & "<div class='flttxt flt" & div_count & "'>Increments&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>: $" & FormatNumber(.Item("increament_amount"), 2) & "</span></div>"
                            '    count = count + 1
                            '    div_count = div_count + 1
                            'End If

                            'If Convert.ToBoolean(.Item("is_show_reserve_price")) Then
                            '    If count = 2 Or Request.QueryString.Get("j") = "g" Then
                            '        strTable = strTable & ""
                            '        count = 0
                            '    End If

                            '    strTable = strTable & "<div class='flttxt flt" & div_count & "'>Reserve Price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>: $" & FormatNumber(.Item("reserve_price"), 2) & "</span></div>"
                            '    count = count + 1
                            '    div_count = div_count + 1
                            'End If




                            'If Convert.ToBoolean(.Item("is_show_no_of_bids")) Then
                            '    Dim bid_total As Integer = SqlHelper.ExecuteScalar("select count(distinct buyer_id) from tbl_auction_bids where auction_id=" & Request.QueryString.Get("i"))

                            '    If count = 2 Or Request.QueryString.Get("j") = "g" Then
                            '        strTable = strTable & ""
                            '        count = 0
                            '    End If

                            '    strTable = strTable & "<div class='flttxt flt" & div_count & "'>No. of bidders&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>: " & bid_total & "</span></div>"

                            '    count = count + 1
                            '    div_count = div_count + 1
                            'End If

                            'If max_bid_amount > 0 Then

                            '    If .Item("auction_status") = 3 Then
                            '        strTable = strTable & "<table border='0' width=100%>"
                            '        If c_bid_amount >= .Item("reserve_price") Then
                            '            Dim rak As Integer = SqlHelper.ExecuteScalar("select dbo.buyer_bid_rank(" & Request.QueryString.Get("i") & "," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
                            '            If rak = 1 Then
                            '                If Request.QueryString.Get("j") = "g" Then
                            '                    strTable = strTable & "<tr><td class='flttxtg'>Thank you for participating!</td></tr>"
                            '                Else
                            '                    strTable = strTable & "<tr><td class='flttxtg'>Thank you for participating!</td></tr>"
                            '                End If
                            '            Else
                            '                If Request.QueryString.Get("j") = "g" Then
                            '                    strTable = strTable & "<tr><td class='flttxtg'>Thank you for participating!</td></tr>"
                            '                Else
                            '                    strTable = strTable & "<tr><td class='flttxtg'>Thank you for participating!</td></tr>"
                            '                End If
                            '            End If
                            '        Else
                            '            If Request.QueryString.Get("j") = "g" Then
                            '                strTable = strTable & "<tr><td class='flttxtr'>The Reserve Price has not been met for this auction.<br>Thank you for participating!</td></tr>"
                            '            Else
                            '                strTable = strTable & "<tr><td class='flttxtr'>The Reserve Price has not been met for this auction.<br>Thank you for participating!</td></tr>"
                            '            End If
                            '        End If
                            '        strTable = strTable & "<table/>"
                            '    End If
                            'End If

                            strTable = strTable '& "</div>"
                            lit_price_setting.Text = strTable
                        End If
                    End With
                End If

            End If
        Else
            pnl_price.Visible = False
            pnl_session.Visible = True
        End If
    End Sub
End Class
