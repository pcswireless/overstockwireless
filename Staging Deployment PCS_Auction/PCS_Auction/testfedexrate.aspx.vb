﻿Imports FedexRateServices
Partial Class testfedexrate
    Inherits System.Web.UI.Page

    Protected Sub btn_rate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_rate.Click
        Dim obj As New FedAllRateServices
        Dim objreq As New FedexRateServices.RateRequest
        Dim objrep As New FedexRateServices.RateReply
        Dim Origin_StreetLines, Origin_City, Origin_StateOrProvinceCode, Origin_PostalCode, Origin_CountryCode, Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, Destination_PostalCode, Destination_CountryCode, PackageLineItems_SequenceNumber, PackageLineItems_GroupPackageCount, PackageLineItems_weight_value, PackageLineItems_Dimensions_length, PackageLineItems_Dimensions_width, PackageLineItems_Dimensions_height As String

        Origin_StreetLines = "39-40 30th street"
        Origin_City = "Long Island City"
        Origin_StateOrProvinceCode = "NY"
        Origin_PostalCode = "11101"
        Origin_CountryCode = "US"

        Destination_StreetLines = "170 S 3rd St"
        Destination_City = "Brooklyn"
        Destination_StateOrProvinceCode = "NY"
        Destination_PostalCode = "11211"
        Destination_CountryCode = "US"

        PackageLineItems_SequenceNumber = "1"
        PackageLineItems_GroupPackageCount = "1"
        PackageLineItems_weight_value = "18"
        PackageLineItems_Dimensions_length = "12"
        PackageLineItems_Dimensions_width = "13"
        PackageLineItems_Dimensions_height = "14"

        GridView1.DataSource = obj.GetFedexShippingOptions(Origin_StreetLines, Origin_City, Origin_StateOrProvinceCode, Origin_PostalCode, Origin_CountryCode, Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, Destination_PostalCode, Destination_CountryCode, PackageLineItems_SequenceNumber, PackageLineItems_GroupPackageCount, PackageLineItems_weight_value, PackageLineItems_Dimensions_length, PackageLineItems_Dimensions_width, PackageLineItems_Dimensions_height)
        GridView1.DataBind()

    End Sub
End Class
