﻿Imports System.IO
Imports System.Net.Mail

Public Class ThanksMail
    Private is_server As Integer
    Private DBConnection As String
    Private ServerHttp As String
    Private erroremail As String
    Private bccemail As String
    Private winning_bcc_email As String
    Private winning_bcc_email_test As String
    Private smtp_server As String
    Private smtp_user_name As String
    Private smtp_password As String
    Private smtp_server_port As String
    Private email_from As String
    Private winning_email_from As String
    Private email_from_name As String

    Private Sub WelComeMail_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Try
            ReadTextFile()
            ReadSMTP()
            Dim str As String
            Dim flag As Boolean = False
            Dim arr

            Dim myReader As StreamReader
            myReader = File.OpenText(Application.StartupPath() & "\source.txt")
            Do
                str = myReader.ReadLine
                If flag And str <> "" Then
                    arr = str.Split(",")
                    is_server = arr(0)
                    If arr(1) <> "" Then ServerHttp = arr(1)
                    If arr(2) <> "" Then erroremail = arr(2)
                    If arr(3) <> "" Then bccemail = arr(3)
                    If arr(4) <> "" Then winning_bcc_email = arr(4)
                    If arr(5) <> "" Then winning_bcc_email_test = arr(5)
                    If arr(6) <> "" Then winning_email_from = arr(6)
                End If
                flag = True
            Loop While str <> Nothing
            UpdateWinner()
            Me.Timer1.Enabled = True
        Catch ex As Exception
            sendEmail("Error in Auction end mail exe", "ThnaksMail_Load : " & ex.Message, erroremail, "Bimal Giri", False, False)
        End Try
    End Sub

    Protected Sub UpdateWinner()
        Dim all_success As Boolean = True
        Dim single_success As Boolean = True
        Dim sQuery As String = "SELECT auction_id, ISNULL(rank1_bid_id, 0) AS rank1_bid_id FROM tbl_auctions WITH(NOLOCK) WHERE auction_type_id IN (2,3) AND is_active = 1 AND ISNULL(is_email_sent, 0) = 0  AND (dbo.get_auction_status(auction_id) = 3)"
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDatatable(DBConnection, sQuery)
        Try

            For i = 0 To dt.Rows.Count - 1
                all_success = True
                single_success = True
                If dt.Rows(i).Item("rank1_bid_id") <> 0 Then
                    Dim str As String = "update tbl_auction_bids set action='Awarded' where bid_id=" & dt.Rows(i).Item("rank1_bid_id")
                    SqlHelper.ExecuteNonQuery(DBConnection, str)

                    Dim dtBidder As New DataTable
                    dtBidder = SqlHelper.ExecuteDatatable(DBConnection, "select max(bid_id) as bid_id, buyer_id from tbl_auction_bids WITH(NOLOCK) where auction_id=" & dt.Rows(i).Item("auction_id") & " group by buyer_id")

                    If dtBidder.Rows.Count > 0 Then

                        For j = 0 To dtBidder.Rows.Count - 1
                            single_success = SendWinningBidEmail(dtBidder.Rows(j)("bid_id"))
                            If single_success = False Then all_success = False
                        Next

                    End If
                    If all_success Then
                        SqlHelper.ExecuteNonQuery(DBConnection, "update tbl_auctions set is_email_sent = 1 where auction_id=" & dt.Rows(i).Item("auction_id"))
                    End If

                End If
            Next
        Catch ex As Exception
            sendEmail("Error in Auction thanks mail exe", "UpdateWinner : " & ex.Message, erroremail, "Bimal Giri", False, False)
        End Try

    End Sub

    Private Function SendWinningBidEmail(ByVal bid_id As Integer) As Boolean
        Dim IS_SUCCESS As Boolean = True
        'Done
        Try

            Dim email_caption As String = ""
            Dim body As String = ""
            Dim strSubject As String = ""
            Dim str As String = ""

            If bid_id > 0 Then
                Dim _type As String = "bid"
                Dim buyer_email As String = ""
                Dim buyer_name As String = ""
                Dim bidder_email As String = ""
                Dim bidder_name As String = ""

                Dim auction_title As String = ""
                Dim auction_code As String = ""
                Dim condition As String = ""
                Dim packaging As String = ""
                Dim qty As Integer = 0
                Dim price As Decimal = 0.0
                Dim amount As Decimal = 0.0
                Dim tax As Decimal = 0.0
                Dim subtotal As Decimal = 0.0
                Dim confirmationno As String = ""
                Dim grandtotal As Decimal = 0.0
                Dim shippingamount As Decimal = 0.0
                Dim shippingoption As String = ""

                Dim shipTo As String = ""
                Dim auction_id As Int32 = 0
                Dim wining_email As Boolean = False
                Dim test_email As Boolean = False
                Dim bid_date As DateTime = DateTime.Now
                Dim strBid As String = "select A.auction_id, C.Code AS Auction_Code, A.bid_date,ISNULL(A.max_bid_amount,0) AS max_bid_amount,ISNULL(A.bid_amount,0) AS bid_amount,ISNULL(A.action,'') AS action, D.code," & _
                    "ISNULL(B.contact_first_name,'') As buyer_name,ISNULL(B.email,'') As email, " & _
                    "ISNULL(C.title,'') AS auction_title,C.auction_type_id, rtrim(ISNULL(U.first_name,'') + ' ' + ISNULL(U.last_name,'')) as bidder_name,ISNULL(U.email,'') as bidder_email" & _
                    " from tbl_auction_bids A WITH(NOLOCK) INNER JOIN tbl_reg_buyers B WITH(NOLOCK) ON A.buyer_id=B.buyer_id INNER JOIN tbl_reg_buyer_users U WITH(NOLOCK) on A.buyer_user_id = U.buyer_user_id INNER JOIN tbl_auctions C WITH(NOLOCK) ON A.auction_id=C.auction_id INNER JOIN tbl_master_countries D WITH(NOLOCK) ON B.country_id = D.country_id where bid_id=" & bid_id

                strBid = strBid & bindToInvoice(bid_id)

                strBid = strBid & bindToAddress(bid_id)

                Dim ds As New DataSet()
                ds = SqlHelper.ExecuteDataset(DBConnection, strBid)

                If ds.Tables.Count > 0 AndAlso ds.Tables(2).Rows.Count > 0 Then
                    With ds.Tables(2).Rows(0)
                        shipTo = .Item("contact_first_name") & IIf(.Item("contact_last_name").ToString() <> "", " " & .Item("contact_last_name"), "")
                        shipTo = shipTo & "<br />" & .Item("company_name")
                        shipTo = shipTo & "<br />" & .Item("address1") & " " & .Item("address2")
                        shipTo = shipTo & "<br />" & .Item("city") & ", " & .Item("state_text") & " " & .Item("zip")
                        If .Item("phone") <> "" Then shipTo = shipTo & "<br />Phone: " & .Item("phone")
                        If .Item("fax") <> "" Then shipTo = shipTo & " Fax: " & .Item("fax")

                    End With
                End If

                If ds.Tables(1).Rows.Count > 0 Then
                    With ds.Tables(1).Rows(0)
                        auction_code = .Item("code")
                        condition = .Item("stock_condition")
                        packaging = .Item("package_condition")
                        price = .Item("price")
                        qty = .Item("quantity")
                        tax = .Item("tax")
                        amount = .Item("amount")
                        subtotal = .Item("amount")
                        bid_date = .Item("bid_date") '.ToString("MM/dd/yyyy")
                        confirmationno = .Item("confirmation_no")
                        grandtotal = .Item("grand_total_amount")
                        shippingamount = .Item("shipping_amount")
                        shippingoption = .Item("shipping_value")
                    End With

                End If

                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    With ds.Tables(0).Rows(0)
                        buyer_email = .Item("email")
                        buyer_name = .Item("buyer_name").ToString().Trim()
                        bidder_email = .Item("bidder_email").ToString().Trim()
                        bidder_name = .Item("bidder_name").ToString().Trim()
                        auction_title = .Item("auction_title")
                        auction_id = .Item("auction_id")
                        Dim max_bid_amount As String = FormatCurrency(.Item("max_bid_amount"), 2)
                        Dim bid_amount As String = FormatCurrency(.Item("bid_amount"), 2)
                       
                        Dim dsEmail As DataSet = New DataSet()
                        If .Item("action").ToString().ToUpper() = "Awarded".ToUpper() Then
                            wining_email = True
                            If auction_title.ToLower.Contains("test ") Then test_email = True

                            dsEmail = GetEmail_ByCode("em26")
                            email_caption = "Winning Bidder Mail (Proxy Auction)"
                            If dsEmail.Tables(0).Rows.Count > 0 Then
                                strSubject = dsEmail.Tables(0).Rows(0)("email_subject")
                                email_caption = dsEmail.Tables(0).Rows(0)("email_name")
                                body = dsEmail.Tables(0).Rows(0)("html_body")
                                body = body.Replace("<<bidder_name>>", buyer_name)
                                body = body.Replace("<<auction_title>>", auction_title)
                                body = body.Replace("<<auction_url>>", ServerHttp & "/login.html?a=" & EncryptDecrypt.EncryptValueFormatted(auction_id.ToString()))
                                body = body.Replace("<<site_url>>", ServerHttp)


                                body = body.Replace("<<Date>>", bid_date)
                                body = body.Replace("<<ConfirmationNo>>", confirmationno)
                                body = body.Replace("<<ShipTo>>", shipTo)
                                body = body.Replace("<<AuctionTitle>>", auction_title)
                                body = body.Replace("<<AuctionNo>>", auction_code)
                                body = body.Replace("<<Condition>>", condition)
                                body = body.Replace("<<Packaging>>", packaging)
                                body = body.Replace("<<Qty>>", qty.ToString() + " lot")
                                body = body.Replace("<<Price>>", FormatCurrency(price, 2))
                                body = body.Replace("<<Amount>>", FormatCurrency(amount, 2))

                                body = body.Replace("<<ShippingOption>>", shippingoption)
                                body = body.Replace("<<SubTotal>>", FormatCurrency(subtotal, 2))
                                body = body.Replace("<<Tax>>", FormatCurrency(tax, 2))

                                body = body.Replace("<<Freight>>", FormatCurrency(shippingamount, 2))
                                body = body.Replace("<<SubTotal>>", FormatCurrency(subtotal, 2))
                                body = body.Replace("<<Total>>", FormatCurrency(grandtotal, 2))

                            End If

                        Else
                            email_caption = "Email For Not Highest Bidder (Proxy Auction)"
                            dsEmail = GetEmail_ByCode("em27")
                            If dsEmail.Tables(0).Rows.Count > 0 Then
                                strSubject = dsEmail.Tables(0).Rows(0)("email_subject")
                                email_caption = dsEmail.Tables(0).Rows(0)("email_name")
                                body = dsEmail.Tables(0).Rows(0)("html_body")
                                body = body.Replace("<<bidder_name>>", buyer_name)
                                body = body.Replace("<<auction_title>>", auction_title)
                                body = body.Replace("<<auction_code>>", .Item("Auction_Code"))
                                body = body.Replace("<<auction_url>>", ServerHttp & "/login.html?a=" & EncryptDecrypt.EncryptValueFormatted(auction_id.ToString()))
                                body = body.Replace("<<max_bid_amount>>", FormatCurrency(bid_amount, 2))
                                body = body.Replace("<<bid_date>>", Format(Convert.ToDateTime(bid_date), "M/dd/yyyy"))
                                body = body.Replace("<<site_url>>", ServerHttp)

                            End If
                        End If

                        If (dsEmail.Tables.Count > 0 AndAlso dsEmail.Tables(0).Rows.Count > 0) Then
                            sendEmail(strSubject, body, bidder_email, bidder_name, test_email, wining_email)
                        End If

                        If .Item("action").ToString().ToUpper() = "Awarded".ToUpper() Then
                            SendWinningPaymentEmail(bid_id)
                            SqlHelper.ExecuteNonQuery(DBConnection, "update tbl_auction_bids set send_email=1 where bid_id=" & bid_id)

                        End If

                    End With
                End If
            End If

        Catch ex As Exception
            IS_SUCCESS = False
            sendEmail("Error in Auction thanks mail exe", "SendWinningBidEmail : " & ex.Message, erroremail, "Bimal Giri", False, False)
        End Try
        Return IS_SUCCESS
    End Function

    Private Sub SendWinningPaymentEmail(ByVal bid_id As Integer)
        'Done
        Try

            Dim body As String = ""
            Dim strSubject As String = ""
            
            Dim test_email As Boolean = False
            Dim strBid As String = "select A.auction_id, C.Code AS Auction_Code,ISNULL(C.title,'') AS auction_title,ISNULL(A.bid_amount,0) AS bid_amount, D.code," & _
                "ISNULL(B.contact_first_name,'') As buyer_name,ISNULL(B.email,'') As buyer_email " & _
                " from tbl_auction_bids A WITH(NOLOCK) INNER JOIN tbl_reg_buyers B WITH(NOLOCK) ON A.buyer_id=B.buyer_id INNER JOIN tbl_auctions C WITH(NOLOCK) ON A.auction_id=C.auction_id INNER JOIN tbl_master_countries D WITH(NOLOCK) ON B.country_id = D.country_id where bid_id=" & bid_id
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable(DBConnection, strBid)
            If dt.Rows.Count > 0 Then

                Dim ds As DataSet = New DataSet()
                ds = GetEmail_ByCode("em43")
                If ds.Tables(0).Rows.Count > 0 Then
                    strSubject = ds.Tables(0).Rows(0)("email_subject")
                    body = ds.Tables(0).Rows(0)("html_body")
                    body = body.Replace("<<bidder_name>>", dt.Rows(0)("buyer_name").ToString().Trim())
                    body = body.Replace("<<auction_code>>", dt.Rows(0)("Auction_Code").ToString().Trim())
                    body = body.Replace("<<auction_title>>", dt.Rows(0)("auction_title").ToString().Trim())
                    body = body.Replace("<<site_url>>", ServerHttp)
                    If dt.Rows(0)("bid_amount") <= 10000.0 And dt.Rows(0)("code") = "US" Then
                        body = body.Replace("<<payment_link>>", "<tr><td colspan='2'><br/><br/>Please <a href='" & ServerHttp & "/PaypalRequest.aspx?bd=" & EncryptDecrypt.EncryptValueFormatted(bid_id) & "' target='_blank'>click here</a> to make a payment via paypal.</td></tr><tr><td colspan='2'><br /><span style='font-size: 16px; font-weight: bold;'>OR</span></td></tr>")
                    Else
                        body = body.Replace("<<payment_link>>", "")
                    End If

                    sendEmail(strSubject, body, dt.Rows(0)("buyer_email"), dt.Rows(0)("buyer_name").ToString().Trim(), test_email, True)

                End If

            End If


        Catch ex As Exception
            sendEmail("Error in Auction thanks mail exe", "SendWinningPaymentEmail : " & ex.Message, erroremail, "Bimal Giri", False, False)
        End Try
    End Sub

    Private Function GetEmail_ByCode(ByVal email_code As String, Optional ByVal qry As String = "") As DataSet
        Dim ds As DataSet = New DataSet
        Try


            Dim strQuery As String = ""
            strQuery = "select isnull(A.email_name,'') as email_name,isnull(A.email_subject,'') as email_subject, cast(isnull(B.header_html,'') as nvarchar(max))+cast(isnull(A.email_html,'') as  nvarchar(max))+cast(isnull(C.footer_html,'')as nvarchar(max)) as html_body " & _
            "from tbl_master_emails A INNER JOIN tbl_master_email_headers B ON A.header_id=B.header_id INNER JOIN tbl_master_email_footers C ON A.footer_id=C.footer_id where A.is_active=1 and B.is_active=1 and C.is_active=1 and A.email_code='" & email_code & "'"
            If qry.Trim <> "" Then
                strQuery = strQuery & ";" & qry
            End If
            ds = SqlHelper.ExecuteDataset(DBConnection, strQuery)

        Catch ex As Exception
            sendEmail("Error in Auction end mail exe", "GetEmail_ByCode : " & ex.Message, erroremail, "Bimal Giri", False, False)
        End Try

        Return ds
    End Function

    Private Sub ReadTextFile()
        Try
            Dim oRead As System.IO.StreamReader
            Dim LineIn As String = ""
            oRead = File.OpenText(Application.StartupPath & "\DBConnection.ini")
            ' While oRead.Peek - 1
            LineIn &= oRead.ReadLine()
            'End While

            oRead.Close()

            Dim arr1 As String()
            Dim arr2 As String()

            arr1 = LineIn.Split(";")
            LineIn = ""

            For i As Integer = 0 To arr1.Length - 1
                arr2 = arr1(i).Split(",")
                If arr2.Length = 2 Then
                    LineIn = LineIn & arr2(0) & "=" & EncryptDecrypt.DecryptValue(arr2(1)) & ";"
                End If
            Next
            DBConnection = LineIn
        Catch ex As Exception
            sendEmail("Error in Auction end mail exe", "ReadTextFile : " & ex.Message, erroremail, "Bimal Giri", False, False)
        End Try

    End Sub

    Private Sub ReadSMTP()
        Try
            Dim oRead As System.IO.StreamReader
            Dim LineIn As String = ""
            oRead = File.OpenText(Application.StartupPath & "\AMsmtp.ini")
            ' While oRead.Peek - 1
            LineIn &= oRead.ReadLine()
            'End While

            oRead.Close()

            Dim arr1 As String()
            Dim arr2 As String()

            arr1 = LineIn.Split(";")

            For i As Integer = 0 To arr1.Length - 1
                arr2 = arr1(i).Split(",")
                If arr2.Length = 2 Then

                    If arr2(0) = "smtp_server" Then smtp_server = arr2(1)
                    If arr2(0) = "smtp_user_name" Then smtp_user_name = arr2(1)
                    If arr2(0) = "smtp_password" Then smtp_password = arr2(1)
                    If arr2(0) = "smtp_server_port" Then smtp_server_port = arr2(1)
                    If arr2(0) = "email_from" Then email_from = arr2(1)
                    If arr2(0) = "email_from_name" Then email_from_name = arr2(1)

                End If
            Next

        Catch ex As Exception
            sendEmail("Error in Auction automail exe", "ReadSMTP : " & ex.Message, erroremail, "Bimal Giri", False, False)
        End Try

    End Sub

    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Timer1.Enabled = False
        Close()
    End Sub

    Private Sub sendEmail(ByVal subject As String, ByVal body As String, ByVal to_email As String, ByVal buyer_name As String, ByVal test_auction As Boolean, ByVal winning_email As Boolean)
        Try
            Dim obj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient()

            Dim msgMail As New System.Net.Mail.MailMessage

            msgMail.IsBodyHtml = True
            msgMail.From = New System.Net.Mail.MailAddress(IIf(winning_email, winning_email_from, email_from), email_from_name)
            msgMail.To.Add(New System.Net.Mail.MailAddress(to_email, buyer_name))

            If winning_email And to_email <> erroremail Then
                msgMail.Bcc.Add(New System.Net.Mail.MailAddress(IIf(test_auction, winning_bcc_email, winning_bcc_email_test), email_from_name))
            End If

            If to_email <> erroremail Then
                msgMail.Bcc.Add(New System.Net.Mail.MailAddress(bccemail, email_from_name))
            End If
            obj.Host = smtp_server
            obj.UseDefaultCredentials = True
            obj.Credentials = New System.Net.NetworkCredential(smtp_user_name, smtp_password)
            obj.Port = smtp_server_port
            msgMail.Subject = subject
            msgMail.Body = body
            If is_server = 1 Then
                obj.Send(msgMail)
                SqlHelper.ExecuteNonQuery(DBConnection, "INSERT INTO tbl_sec_email_log(created_date, from_address, from_name, subject, body, to_address, to_name, bcc_address, bcc_name, cc_address, cc_name, is_send) VALUES (getdate(), '" & email_from & "','" & email_from_name & "','" & subject.Replace("'", "''") & "','" & body.Replace("'", "''") & "','" & to_email & "','" & buyer_name & "','" & bccemail & "','" & "PCS Wireless Auctions" & "','','',1)")
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Function bindToInvoice(ByVal _id As Int32) As String
        Dim sqlStr As String = ""
        If _id > 0 Then
            sqlStr = "SELECT A.auction_id " & _
                                                   ",ISNULL(A.code, '') AS code" & _
                                                   ",ISNULL(A.title, '') AS title" & _
                                                   ",ISNULL(B.name,'') as stock_condition" & _
                                                   ",ISNULL(C.name,'') as package_condition" & _
                                                   ",1 AS quantity" & _
                                                   ",'$'+convert(varchar(10),round(ISNULL(D.bid_amount, 0),2)) AS price" & _
                                                   ",'$'+convert(varchar(10),round(ISNULL(D.bid_amount, 0),2)) AS amount" & _
                                                   ",'$'+convert(varchar(10),round(ISNULL(D.bid_amount, 0),2)) AS sub_total_amount" & _
                                                   ",'$0.00' AS tax" & _
                                                   ",'$'+convert(varchar(10),round(ISNULL(D.shipping_amount,0)+ISNULL(D.bid_amount, 0),2)) AS grand_total_amount" & _
                                                   ",convert(varchar(10),ISNULL(D.bid_date, getdate()),101) AS bid_date" & _
                                                   ",'$'+convert(varchar(10),round(ISNULL(D.shipping_amount, 0),2)) AS shipping_amount" & _
                                                   ",ISNULL(D.shipping_value, '') AS shipping_value" & _
                                                   ",ISNULL(D.bid_id, '') AS confirmation_no" & _
                                                   " FROM tbl_auctions A WITH (NOLOCK) LEFT JOIN tbl_master_stock_conditions B ON A.stock_condition_id=B.stock_condition_id LEFT JOIN tbl_master_packaging C ON A.packaging_id=C.packaging_id " & _
                                                   "inner join tbl_auction_bids D WITH (NOLOCK) on A.auction_id=D.auction_id AND D.bid_id=" & _id & ""
        End If
        Return sqlStr
    End Function

    Private Function bindToAddress(ByVal _id As Int32) As String
        Dim sqlStr As String = ""
        If _id > 0 Then
            sqlStr = "SELECT "
            sqlStr = sqlStr & "ISNULL(A.company_name, '') AS company_name,"
            sqlStr = sqlStr & "ISNULL(A.buyer_id, '') AS buyer_id,"
            sqlStr = sqlStr & "ISNULL(A.contact_title, '') AS contact_title,"
            sqlStr = sqlStr & "ISNULL(A.contact_first_name, '') AS contact_first_name,"
            sqlStr = sqlStr & "ISNULL(A.contact_last_name, '') AS contact_last_name,"
            sqlStr = sqlStr & "ISNULL(A.email, '') AS email,"
            sqlStr = sqlStr & "ISNULL(A.website, '') AS website,"
            sqlStr = sqlStr & "ISNULL(A.phone, '') AS phone,"
            sqlStr = sqlStr & "ISNULL(A.mobile, '') AS mobile,"
            sqlStr = sqlStr & "ISNULL(A.fax, '') AS fax,"
            sqlStr = sqlStr & "ISNULL(A.address1, '') AS address1,"
            sqlStr = sqlStr & "ISNULL(A.address2, '') AS address2,"
            sqlStr = sqlStr & "ISNULL(A.city, '') AS city,"
            sqlStr = sqlStr & "ISNULL(A.state_id, 0) AS state_id,"
            sqlStr = sqlStr & "ISNULL(A.zip, '') AS zip,"
            sqlStr = sqlStr & "ISNULL(A.country_id, 0) AS country_id,"
            sqlStr = sqlStr & "ISNULL(A.state_text, '') AS state_text"
            sqlStr = sqlStr & " FROM"
            sqlStr = sqlStr & " tbl_reg_buyers A WITH (NOLOCK) INNER JOIN tbl_auction_bids B ON A.buyer_id=B.buyer_id AND B.bid_id=" & _id & ""

        End If

        Return sqlStr

    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Close()
    End Sub

End Class
