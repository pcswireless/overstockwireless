﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false" CodeFile="AddEditCaptionHelp.aspx.vb" Inherits="Help_AddEditCaptionHelp" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Simple">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_edit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_save">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                    <telerik:AjaxUpdatedControl ControlID="page_heading" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_cancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Simple" />
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="top">
                <div class="pageheading">
                    <asp:Literal ID="page_heading" runat="server" Text="New Help"></asp:Literal></div>
            </td>
        </tr>
        <tr>
            <td style="height:20px;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <div class="PageTab">
                    <span>
                        <telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
                            <%= IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), "New Help", "Edit Help")%></telerik:RadScriptBlock>
                    </span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="PageTabContentEdit">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td class="tdTabItem">
                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                CssClass="TabGrid">
                                <div style="padding-left: 10px; padding-bottom: 8px;">
                                    <asp:Label ID="lbl_msg" runat="server" Text="" ForeColor="Red" EnableViewState="false"></asp:Label>
                                </div>
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="2" border="0" width="838px">
                                                <tr>
                                                    <td style="width: 145px;" class="caption">
                                                        Caption&nbsp;<span id="span_title" runat="server" class="req_star">*</span>
                                                    </td>
                                                    <td style="width: 270px;" class="details">
                                                        <asp:TextBox ID="txtCaption" runat="server" CssClass="inputtype"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvCaption" runat="server" ControlToValidate="txtCaption"
                                                            ValidationGroup="help" Display="Dynamic" ErrorMessage="<br>Caption required"></asp:RequiredFieldValidator>
                                                        <asp:Label ID="lblCaption" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="caption">
                                                        Description
                                                    </td>
                                                    <td colspan="3" class="details">
                                                       <asp:TextBox ID="txtDescription" runat="server" CssClass="inputtype" TextMode="MultiLine" Width="650" Height="150"></asp:TextBox>
                                                        <asp:Label ID="lbl_description" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </telerik:RadAjaxPanel>
                        </td>
                        <td class="fixedcolumn" valign="top">
                            <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                            <asp:Panel ID="Panel1_Content1button" runat="server" CssClass="collapsePanel">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel11" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                                    <asp:Panel ID="pnl_help_buttons" runat="server">
                                        <div class="addButton">
                                            <asp:ImageButton ID="img_button_edit" ImageUrl="~/Images/edit.gif" runat="server" />
                                            <asp:ImageButton ID="img_button_save" ValidationGroup="help" ImageUrl="~/Images/save.gif"
                                                runat="server" />
                                            <asp:ImageButton ID="img_button_update" ValidationGroup="help" ImageUrl="~/Images/update.gif"
                                                runat="server" />
                                        </div>
                                        <div class="cancelButton" id="div_basic_cancel" runat="server">
                                            <asp:ImageButton ID="img_button_cancel" ImageUrl="~/Images/cancel.gif" runat="server" />
                                        </div>
                                    </asp:Panel>
                                </telerik:RadAjaxPanel>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

