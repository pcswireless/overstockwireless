﻿Imports Telerik.Web.UI
Imports System.Drawing
Partial Class Help_AddEditCaptionHelp
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Private Sub setPermission()
        If Not CommonCode.is_super_admin() Then
            Dim Vew_Help As Boolean = False
            Dim Edit_Help As Boolean = False

            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 26
                        Vew_Help = True
                    Case 27
                        Edit_Help = True
                End Select
            Next
            dt = Nothing

            If Vew_Help = False Then Response.Redirect("/NoPermission.aspx")
            If Not Edit_Help Then
                pnl_help_buttons.Visible = False
            End If
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                bind_help()
                set_form_mode(False)
                set_button_visibility("View")

            Else
                set_form_mode(True)
                set_button_visibility("Add")
                page_heading.Text = "New Help"
            End If
        End If
    End Sub
    Private Sub set_button_visibility(ByVal mode As String)
        If mode.ToUpper() = "VIEW" Then
            img_button_edit.Visible = True
            img_button_save.Visible = False
            img_button_update.Visible = False
            div_basic_cancel.Visible = False
        ElseIf mode.ToUpper() = "EDIT" Then
            img_button_edit.Visible = False
            img_button_save.Visible = False
            img_button_update.Visible = True
            div_basic_cancel.Visible = True
        Else
            img_button_edit.Visible = False
            img_button_save.Visible = True
            img_button_update.Visible = False
            div_basic_cancel.Visible = False
        End If
    End Sub
    Private Sub set_form_mode(ByVal flag As Boolean)
        txtCaption.Visible = False
        txtDescription.Visible = flag
        span_title.Visible = False

        lblCaption.Visible = True
        lbl_description.Visible = Not (flag)

    End Sub
    Private Sub bind_help()
        Dim strQuery As String = ""
        Dim dt As DataTable
        strQuery = "SELECT help_id, ISNULL(caption, '') AS caption, ISNULL([description], '') AS [description] FROM tbl_caption_help WHERE help_id = " & Request.QueryString("i")
        dt = SqlHelper.ExecuteDataTable(strQuery)
        If dt.Rows.Count > 0 Then
            page_heading.Text = dt.Rows(0)("caption")
            txtCaption.Text = dt.Rows(0)("caption")
            lblCaption.Text = dt.Rows(0)("caption")
            txtDescription.Text = dt.Rows(0)("description")
            lbl_description.Text = dt.Rows(0)("description")
        End If
    End Sub

    Protected Sub img_button_save_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles img_button_save.Click
        If Page.IsValid Then
            Dim strQuery As String = ""
            strQuery = "Insert into tbl_caption_help(caption, description)Values('" & CommonCode.encodeSingleQuote(txtCaption.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txtDescription.Text) & "') select scope_identity()"
            Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)
            If i > 0 Then
                Response.Redirect("/Help/CaptionHelp.aspx")
            End If
        End If

    End Sub

    Protected Sub img_button_update_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles img_button_update.Click
        If Page.IsValid Then
            Dim strQuery As String = ""
            strQuery = "Update tbl_caption_help set description='" & CommonCode.encodeSingleQuote(txtDescription.Text.Trim()) & "' where help_id=" & Request.QueryString("i")
            SqlHelper.ExecuteScalar(strQuery)
            set_form_mode(False)
            set_button_visibility("View")
            lbl_msg.Text = "Help updated successfully."
            bind_help()
        End If
    End Sub
    Protected Sub img_button_edit_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles img_button_edit.Click
        set_form_mode(True)
        set_button_visibility("Edit")
    End Sub
    Protected Sub img_button_cancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles img_button_cancel.Click
        set_form_mode(False)
        set_button_visibility("View")
    End Sub
End Class
