﻿
Partial Class PaymentTest
    Inherits System.Web.UI.Page

    Protected Sub btn_go_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_go.Click
        If Page.IsValid Then
            Dim sel_qry As String = ""
            Dim is_bid As Boolean = False
            Dim unq_id As Integer = 0



            If IsNumeric(txt_conf_no.Text.Trim) Then
                is_bid = True
                sel_qry = "SELECT A.auction_id " & _
                                               ",ISNULL(A.code, '') AS code" & _                                               ",ISNULL(A.title, '') AS title" & _                                              ",1 AS quantity" & _                                               ",'$'+convert(varchar(10),round(ISNULL(D.bid_amount, 0),2)) AS price" & _                                               ",'$'+convert(varchar(10),round(ISNULL(D.bid_amount, 0),2)) AS amount" & _                                               ",'$'+convert(varchar(10),round(ISNULL(D.bid_amount, 0),2)) AS sub_total_amount" & _                                               ",'$0.00' AS tax" & _                                               ",'$'+convert(varchar(10),round(ISNULL(D.shipping_amount,0)+ISNULL(D.bid_amount, 0),2)) AS grand_total_amount" & _                                               ",convert(varchar(10),ISNULL(D.bid_date, getdate()),101) AS bid_date" & _                                               ",'$'+convert(varchar(10),round(ISNULL(D.shipping_amount, 0),2)) AS shipping_amount" & _                                               ",ISNULL(D.shipping_value, '') AS shipping_value" & _                                               ",ISNULL(D.bid_id, '') AS confirmation_no" & _                                               ",ISNULL(D.buyer_id, 0) AS buyer_id" & _                                               ",ISNULL(D.bid_id, 0) AS id" & _                                               " FROM tbl_auctions A WITH (NOLOCK) inner join tbl_auction_bids D WITH (NOLOCK) on A.auction_id=D.auction_id where D.bid_id='" & txt_conf_no.Text.Trim & "'"
            Else
                sel_qry = "SELECT A.auction_id " & _
                               ",ISNULL(A.code, '') AS code" & _                               ",ISNULL(A.title, '') AS title" & _                                ",ISNULL(D.quantity, 0) AS quantity" & _                               ",'$'+convert(varchar(10),round(ISNULL(D.price, 0),2)) AS price" & _                               ",'$'+convert(varchar(10),round(ISNULL(D.amount, 0),2)) AS amount" & _                               ",'$'+convert(varchar(10),round(ISNULL(D.sub_total_amount, 0),2)) AS sub_total_amount" & _                               ",'$'+convert(varchar(10),round(ISNULL(D.tax, 0),2)) AS tax" & _                               ",'$'+convert(varchar(10),round(ISNULL(D.grand_total_amount, 0),2)) AS grand_total_amount" & _                               ",convert(varchar(10),ISNULL(D.bid_date, getdate()),101) AS bid_date" & _                               ",'$'+convert(varchar(10),round(ISNULL(D.shipping_amount, 0),2)) AS shipping_amount" & _                               ",ISNULL(D.confirmation_no,'') AS confirmation_no" & _                               ",ISNULL(D.shipping_value, '') AS shipping_value" & _                               ",ISNULL(D.buyer_id, 0) AS buyer_id" & _                               ",ISNULL(D.buy_id, 0) AS id" & _                               " FROM tbl_auctions A WITH (NOLOCK) inner join tbl_auction_buy D WITH (NOLOCK) on A.auction_id=D.auction_id where D.confirmation_no='" & txt_conf_no.Text.Trim & "'"
            End If
            Dim buyer_id As Integer = 0
            Dim dtTable As DataTable = SqlHelper.ExecuteDatatable(sel_qry)
            If dtTable.Rows.Count > 0 Then
                With dtTable.Rows(0)

                    buyer_id = .Item("buyer_id")
                    unq_id = .Item("id")
                    lit_auc_code.Text = .Item("code")
                    lit_auc_name.Text = .Item("title")
                   
                    lit_price.Text = .Item("price")
                    lit_qty.Text = .Item("quantity")
                    lit_tax.Text = .Item("tax")
                    lit_amount.Text = .Item("amount")
                    lit_sub_total.Text = .Item("amount")
                    lit_ship_amount.Text = .Item("shipping_amount")
                    lit_grand_total.Text = .Item("grand_total_amount")

                End With

                Dim sqlStr As String = ""

                sqlStr = "SELECT "
                sqlStr = sqlStr & "ISNULL(A.company_name, '') AS company_name,"
                sqlStr = sqlStr & "ISNULL(A.buyer_id, '') AS buyer_id,"                sqlStr = sqlStr & "ISNULL(A.contact_title, '') AS contact_title,"                sqlStr = sqlStr & "ISNULL(A.contact_first_name, '') AS contact_first_name,"                sqlStr = sqlStr & "ISNULL(A.contact_last_name, '') AS contact_last_name,"                sqlStr = sqlStr & "ISNULL(A.email, '') AS email,"                sqlStr = sqlStr & "ISNULL(A.website, '') AS website,"                sqlStr = sqlStr & "ISNULL(A.phone, '') AS phone,"                sqlStr = sqlStr & "ISNULL(A.mobile, '') AS mobile,"                sqlStr = sqlStr & "ISNULL(A.fax, '') AS fax,"                sqlStr = sqlStr & "ISNULL(A.address1, '') AS address1,"                sqlStr = sqlStr & "ISNULL(A.address2, '') AS address2,"                sqlStr = sqlStr & "ISNULL(A.city, '') AS city,"                sqlStr = sqlStr & "ISNULL(A.state_id, 0) AS state_id,"                sqlStr = sqlStr & "ISNULL(A.zip, '') AS zip,"                sqlStr = sqlStr & "ISNULL(A.country_id, 0) AS country_id,"                sqlStr = sqlStr & "ISNULL(A.state_text, '') AS state_text"                sqlStr = sqlStr & " FROM tbl_reg_buyers A WITH (NOLOCK) WHERE A.buyer_id =" & buyer_id

                Dim dtAdd As DataTable = SqlHelper.ExecuteDatatable(sqlStr)
                If dtAdd.Rows.Count > 0 Then
                    With dtAdd.Rows(0)
                        sel_qry = .Item("contact_first_name") & IIf(.Item("contact_last_name").ToString() <> "", " " & .Item("contact_last_name"), "")
                        sel_qry = sel_qry & "<br />" & .Item("company_name")
                        sel_qry = sel_qry & "<br />" & .Item("address1") & " " & .Item("address2")
                        sel_qry = sel_qry & "<br />" & .Item("city") & ", " & .Item("state_text") & " " & .Item("zip")
                        If .Item("phone") <> "" Then sel_qry = sel_qry & "<br />Phone: " & .Item("phone")
                        If .Item("fax") <> "" Then sel_qry = sel_qry & " Fax: " & .Item("fax")
                        lit_bill_to.Text = sel_qry
                    End With
                End If
                lit_payment_link.Text = "<a class='bid' href='/PaypalRequest.aspx?" & IIf(is_bid, "bd", "by") & "=" & Security.EncryptionDecryption.EncryptValueFormatted(unq_id) & "'>Pay with Paypal</a>"
                pnl_details.Visible = True
            Else
                lbl_error.Text = "Please check the confirmation no."
            End If


        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lit_payment_link.Text = ""
        pnl_details.Visible = False
        lbl_error.Text = ""
    End Sub
End Class
