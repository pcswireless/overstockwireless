﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AuctionList.aspx.vb" Inherits="AuctionList" %>

<%@ Register Src="~/UserControls/SalesRep.ascx" TagName="SalesRep" TagPrefix="UC1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Auctions</title><![if IE]>
    <link rel="stylesheet" type="text/css" href="/Style/font_ie.css" />
    <![endif]> <![if !IE]>
    <link rel="stylesheet" type="text/css" href="/Style/font_other.css" />
    <![endif]>
    <link type="text/css" rel="Stylesheet" href="/Style/AuctionBody.css" />
    <script type="text/javascript" src="/lightbox/prototype.js"></script>
    <script type="text/javascript" src="/lightbox/scriptaculous.js?load=effects"></script>
    <script type="text/javascript" src="/lightbox/lightbox.js"></script>
    <link rel="stylesheet" href="/lightbox/lightbox.css" type="text/css" media="screen" />
    <style type="text/css">
        .modal {
            display: none;
            position: absolute;
            top: 0px;
            left: 0px;
            background-color: black;
            z-index: 100;
            opacity: 0.8;
            filter: alpha(opacity=60);
            -moz-opacity: 0.8;
            min-height: 100%;
        }

        #divImage {
            display: none;
            z-index: 1000;
            position: fixed;
            top: 0;
            left: 0;
            background-color: White;
            height: 300px;
            width: 410px;
            padding: 3px;
            border: solid 1px black;
        }
        /* upper left arrow div */
        #upper_left_arrow_new {
            position: absolute;
            left: -300px;
            z-index: 101;
            visibility: hidden;
        }

        /* upper right arrow div */
        #upper_right_arrow_new {
            position: absolute;
            left: -300px;
            z-index: 101;
            direction: rtl;
            text-align: right;
            visibility: hidden;
        }

        /* lower left arrow div */
        #lower_left_arrow_new {
            position: absolute;
            left: -300px;
            z-index: 101;
            visibility: hidden;
        }

        /* lower right arrow div */
        #lower_right_arrow_new {
            position: absolute;
            left: -300px;
            z-index: 101;
            direction: rtl;
            text-align: right;
            visibility: hidden;
        }

        /* main class for pointer arrow */
        .arrow {
            border-color: black;
            border-right-style: solid;
            border-right-width: 2px;
            border-left-style: solid;
            border-left-width: 1px;
            overflow: hidden;
            height: 1px;
            font-size: 1px;
        }

        /* provides border or 'tip' for arrow */
        .arrow_tip {
            background-color: black;
            height: 1px;
            font-size: 1px;
        }

        /* main tip div */
        #dhtmltooltip_new {
            position: absolute;
            left: -300px;
            width: 150px;
            border: 1px solid black;
            padding: 2px;
            background-color: lightyellow;
            visibility: hidden;
            z-index: 100; /*Remove below line to remove shadow. Below line should always appear last within this CSS*/
            filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,direction=135); /* End ToolTip CSS */
        }
        .modalBackground {  
            background-color:Gray;  
            filter:alpha(opacity=70);  
            opacity:0.7;  
        }  
        #popup   
        {  
            background-color:White;  
        }  
        .target_display
        {
            display:none;
        }
    </style>
    <telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
        <script type="text/javascript" language="JavaScript">

            function parentHide(ivId) {
                document.getElementById(ivId).style.display = 'none';
            }


            function validBid(aucid, txtid, cmpVal, divid, buy_option, auc_type, refr_id, shipOption, shipValue,help_id) {

                var sOption;
                sOption = document.getElementById(shipOption).value;
                var sValue;
                sValue = document.getElementById(shipValue).value;



                document.getElementById(divid).innerHTML = '';

                if (sOption == '0')
                    document.getElementById(divid).innerHTML = 'Please select shipping';
                else {
                    var bidamt;
                    bidamt = document.getElementById(txtid).value;

                    if (bidamt == '' || bidamt == '0')
                        document.getElementById(divid).innerHTML = 'Amount Required';

                    else {

                        if (!isNumber(bidamt))
                            document.getElementById(divid).innerHTML = 'Invalid Amount';
                        else {
                            if (Number(bidamt) < Number(cmpVal)) {
                                document.getElementById(divid).innerHTML = 'Amount not Accepted';
                                document.getElementById(help_id).style.display = 'block';
                            }
                            else {
                                {
                                    document.getElementById(help_id).style.display = 'none';
                                    window.open('/AuctionBidPop.aspx?a=' + aucid + '&o=' + buy_option + '&t=' + auc_type + '&p=' + bidamt + '&r=' + refr_id + '&sOption=' + sOption + '&sValue=' + sValue, "_AuctionBidPop", "left=" + ((screen.width - 600) / 2) + ",top=" + ((screen.height - 420) / 2) + ",width=600,height=420,scrollbars=yes,toolbars=no,resizable=yes");
                                }
                            }
                        }
                    }
                }
                return false;
            }

            function openOffer(aucid, buy_option, auc_type, bidamt) {
                window.open('/AuctionBidPop.aspx?a=' + aucid + '&o=' + buy_option + '&t=' + auc_type + '&p=' + bidamt, "_AuctionBidPop", "left=" + ((screen.width - 600) / 2) + ",top=" + ((screen.height - 420) / 2) + ",width=600,height=420,scrollbars=yes,toolbars=no,resizable=yes");
                return false;
            }
            function openAskQuestion(aucid) {
                window.open('/AuctionQuestion.aspx?i=' + aucid, "_AuctionQuestion", "left=" + ((screen.width - 600) / 2) + ",top=" + ((screen.height - 420) / 2) + ",width=600,height=420,scrollbars=yes,toolbars=no,resizable=yes");
                return false;
            }

            function isNumber(n) { return !isNaN(parseFloat(n)) && isFinite(n); }

            function RefreshList(ctlId) { document.getElementById(ctlId).click(); }


            function redirectIframe(path1, path2, path3) {
                if (path1 != '')
                    top.topFrame.location = path1;
                if (path2 != '')
                    top.leftFrame.location = path2;
                if (path3 != '')
                    top.mainFrame.location = path3;
            }

            function redirectConfirm(_qid, _prc, _aid, _type) {
                var ddq = document.getElementById(_qid);
                var _qty = 0;
                if (ddq != null)
                    _qty = ddq.options[ddq.selectedIndex].value;
                else
                    _qty = _qid;

                //alert(_qid);
                //var _prc = document.getElementById(_pid).innerHTML;
                redirectIframe('', '', '/Auctionconfirmation.aspx?t=b&a=' + _aid + '&q=' + _qty + '&p=' + _prc + '&y=' + _type);
                return false;
            }

            function open_pop_win(_path, _id) {
                w = window.open(_path + '?i=' + _id, '_history', 'top=' + ((screen.height - 400) / 2) + ', left=' + ((screen.width - 324) / 2) + ', height=400, width=324,scrollbars=yes,toolbars=no,resizable=1;');
                w.focus();
                return false;
            }


            hu = window.location.search.substring(1);
            if (hu.indexOf('&a=') > 0) {
                redirectIframe(top.topFrame.location, '', '')
            }


            function setShipping(OptionId, OptionValue, shipAmt, bidAmt, shipAmt, hidCtl, shipCtl, totalCtl) {

            }


            function changeShipping(bidAmt, shipAmt, hidOCtl, hidFCtl, hidSCtl, shipCtl, totalCtl) {

                document.getElementById(hidOCtl).value = 2;
                document.getElementById(hidFCtl).value = shipAmt;
                document.getElementById(hidSCtl).value = shipAmt;
                document.getElementById(shipCtl).innerHTML = '$' + parseFloat(shipAmt).toFixed(2);

                var ttlAmt;
                ttlAmt = parseFloat(bidAmt) + parseFloat(shipAmt);

                document.getElementById(totalCtl).innerHTML = 'Total:&nbsp;&nbsp;&nbsp;&nbsp;$' + ttlAmt.toFixed(2);
                return true;
            }

            function changeShippingOption(shipOption, bidAmt, shipAmt, hidOCtl, hidFCtl, hidSCtl, shipCtl, totalCtl) {

                document.getElementById(hidOCtl).value = shipOption;
                if (shipOption == 2) {
                    document.getElementById(hidSCtl).value = document.getElementById(hidFCtl).value;
                    shipAmt = document.getElementById(hidFCtl).value;
                }


                document.getElementById(hidSCtl).value = shipAmt;
                document.getElementById(shipCtl).innerHTML = '$' + parseFloat(shipAmt).toFixed(2);
                var ttlAmt;
                ttlAmt = parseFloat(bidAmt) + parseFloat(shipAmt);

                document.getElementById(totalCtl).innerHTML = 'Total:&nbsp;&nbsp;&nbsp;&nbsp;$' + ttlAmt.toFixed(2);
                return true;
            }
            function question_wait(_valdt_grp, _aid) {
                if (Page_ClientValidate(_valdt_grp)) {
                    var dv_ask = document.getElementById('dv_' + _aid + '_ask');
                    var dv_msg = document.getElementById('dv_' + _aid + '_msg');
                    var dv_cnf = document.getElementById('dv_' + _aid + '_confirm');
                    if (dv_ask.style.display == 'block') {
                        dv_ask.style.display = 'none';
                        dv_cnf.style.display = 'none';
                        dv_msg.style.display = 'block';
                    }
                    else {
                        dv_ask.style.display = 'block';
                        dv_cnf.style.display = 'block';
                        dv_msg.style.display = 'none';
                    }
                }
            }

        </script>
    </telerik:RadScriptBlock>
    <script type='text/javascript'>        (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://www.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({ c: '16043132-05cd-4602-a161-b7c3e1339364', f: true }); done = true; } }; })();</script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="dhtmltooltip_new">
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <center>
            <UC1:SalesRep runat="server" ID="UC_SalesRep"></UC1:SalesRep>
        </center>
        <div id="div_topmsg" runat="server" style="background-color: #EDF9FF; padding: 5px; padding-left: 40px; background-image: url('/images/bulb.gif'); background-repeat: no-repeat; background-position: 10px 5px;">
            <span class="duplAuction">Message for you...</span>
            <asp:Repeater ID="rep_message" runat="server">
                <ItemTemplate>
                    <div style="border: 1px solid #FCAD35; padding: 5px; margin-top: 15px; text-align: justify;">
                        <b>
                            <%# Eval("title")%></b> -
                    <%# Eval("description")%>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div style="padding: 10px 5px 10px 5px;">
            <ajax:UpdatePanel ID="up1" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pn_auction_content" runat="server">
                        <%--<asp:TextBox ID="txt_test" runat="server"></asp:TextBox>--%>
                        <asp:Repeater ID="rpt_auctions" runat="server">
                            <ItemTemplate>
                                <table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin-top: 50px;">
                                    <tr>
                                        <td valign="top" align="center" style="padding: 5px; width: 150px;">
                                            <div class="Imgborder">
                                                <a onclick="return false;" id="a_img_product" style="text-decoration: none; cursor: pointer"
                                                    runat="server">
                                                    <asp:Image ID="img_image" runat="server" CssClass="aucImage" CausesValidation="false" />
                                                </a>
                                                <asp:Repeater ID="rpt_auction_stock_image" runat="server">
                                                    <ItemTemplate>
                                                        <%# IIf((Container.ItemIndex) <> -1, "<a rel='" & "lightbox[PerfumeSize" & Eval("auction_id") & "0]" & "' style='text-decoration: none; display: none;' href='" & "/upload/auctions/stock_images/" & Eval("path") & "' title='" & Eval("title") & "'></a>", "")%>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:HiddenField ID="hid_auction_type" runat="server" Value="0" />
                                                <asp:HiddenField ID="hid_auction_id" runat="server" Value='<%# Eval("auction_id")%>' />
                                            </div>
                                            <div style="padding: 5px 0px 5px 0px; height: 20px; display: block;">
                                                <asp:ImageButton ID="img_add_favourite" Visible="false" runat="server" CausesValidation="false"
                                                    CommandName="add_favourite" CommandArgument='<%# Eval("auction_id")%>' />
                                            </div>
                                            <asp:ImageButton ID="img_hide_now" runat="server" Visible="false" CausesValidation="false"
                                                CommandName="hide_now" CommandArgument='<%# Eval("auction_id")%>' />
                                            <div style="padding: 5px 0px 5px 0px; height: 20px; display: block;">
                                                <asp:ImageButton ID="img_ask_question" runat="server" CausesValidation="false" ImageUrl="/Images/fend/askaquestion.png" />
                                            </div>
                                        </td>
                                        <td valign="top" style="padding: 5px;">
                                            <div style="clear: both;">
                                                <div class="auctionTitle">
                                                    <asp:Literal ID="lbl_title" runat="server" />
                                                </div>
                                                <asp:Label ID="lbl_sub_title" runat="server" CssClass="auctionSubTitle" />
                                                <div class="auctionSubTitle">
                                                    Auction # :
                                                <asp:Label Font-Size="11px" ID="lbl_auction_code" runat="server" />
                                                </div>
                                                <div class="ShortDesc">
                                                    <asp:Literal ID="lbl_short_desc" runat="server" />
                                                </div>
                                                <asp:Panel ID="pnl_proxy" runat="server">
                                                    <iframe id="iframe_price_summary" runat="server" scrolling="no" frameborder="0" width="500px"
                                                        height="105px"></iframe>
                                                </asp:Panel>
                                                <asp:Panel ID="pnl_now" ForeColor="#525252" runat="server">
                                                    <b>Buy it now for :
                                                    <asp:Literal ID="lit_buy_now_price" runat="server" /></b>
                                                </asp:Panel>
                                                <asp:Panel ID="pnl_quote" runat="server">
                                                    <asp:Literal ID="lit_quote_msg" runat="server" />
                                                </asp:Panel>
                                                <asp:Literal ID="lit_item_attachment" runat="server"></asp:Literal>
                                            </div>
                                            <div style="float: right; text-align: right; clear: both;">
                                                <asp:Literal ID="lit_offer" runat="server"></asp:Literal>
                                            </div>
                                            <asp:Panel ID="pnl_your_bid" runat="server" Visible="false">
                                                <div style="text-align: left; clear: both;">
                                                    <table cellpadding="5" cellspacing="2" border="0">
                                                        <tr>
                                                            <td style="padding-left: 45px;">
                                                                <b>Your Current Bid</b>
                                                            </td>
                                                            <td class="bidPrice">
                                                                <asp:Label ID="your_current_bid" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td rowspan="2" class="shipingOptionBG" style="width: 355px; height: 148px;">
                                                                <asp:Literal ID="lit_own_shipping" runat="server" />
                                                                <asp:HiddenField ID="hid_shipping_value" runat="server" Value="0" />
                                                                <asp:HiddenField ID="hid_shipping_option" runat="server" Value="0" />
                                                                <asp:RadioButtonList ID="rdo_option" runat="server" CellPadding="0" CellSpacing="0">
                                                                    <asp:ListItem Text="Use my own shipping carrier to ship this item to me" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Estimated FedEx Shipping Rates" Value="2"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                                <center>
                                                                    <br />
                                                                </center>
                                                                <asp:Panel ID="pnl_fedex" runat="server" Visible="true" Style="padding: 5px 15px; background-color: White; border: 2px solid #E2E2E2;">
                                                                    <asp:RadioButtonList ID="rbt_fedex_options" runat="server" CellPadding="0" CellSpacing="0" />
                                                                    <asp:HiddenField ID="hid_fedex_selected" runat="server" Value="0" />
                                                                </asp:Panel>
                                                            </td>
                                                            <td class="bidPrice">
                                                                <div style="padding-top: 70px; height: 20px; display: block;">
                                                                    <asp:Label ID="your_current_shipping" runat="server" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bidPrice">
                                                                <asp:Label ID="your_total_bid" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:Panel>
                                            <div style="padding: 20px 0px; clear: both;">
                                                <asp:Panel ID="Panel1_Header1" runat="server" CssClass="pnlTabItemHeader">
                                                    <asp:Image ID="pnl_img1" runat="server" ImageAlign="left" ImageUrl="/Images/fend/minus.png"
                                                        CssClass="panelimage" />
                                                </asp:Panel>
                                                <ajax:CollapsiblePanelExtender ID="cpe1" runat="server" Enabled="True" TargetControlID="Panel1_Content1"
                                                    CollapseControlID="Panel1_Header1" ExpandControlID="Panel1_Header1" Collapsed="false"
                                                    ImageControlID="pnl_img1" CollapsedImage="/Images/fend/plus.png" ExpandedImage="/Images/fend/minus.png"
                                                    CollapsedText="More Details" ExpandedText="Hide Details">
                                                </ajax:CollapsiblePanelExtender>
                                            </div>
                                            <br clear="all" />
                                            <asp:Panel ID="Panel1_Content1" runat="server" CssClass="collapsePanel">
                                                <telerik:RadTabStrip runat="server" ID="TabStip1" MultiPageID="Multipage1" SelectedIndex="0"
                                                    CausesValidation="false">
                                                    <Tabs>
                                                        <telerik:RadTab runat="server" Text="Summary" PageViewID="PageView1">
                                                        </telerik:RadTab>
                                                        <telerik:RadTab runat="server" Text="Items" PageViewID="PageView2">
                                                        </telerik:RadTab>
                                                        <telerik:RadTab runat="server" Text="Details" PageViewID="PageView3">
                                                        </telerik:RadTab>
                                                        <telerik:RadTab runat="server" Text="Terms and Conditions" PageViewID="PageView4">
                                                        </telerik:RadTab>
                                                        <telerik:RadTab runat="server" Text="Ask a Question" PageViewID="PageView5">
                                                        </telerik:RadTab>
                                                    </Tabs>
                                                </telerik:RadTabStrip>
                                                <telerik:RadMultiPage runat="server" ID="Multipage1" SelectedIndex="0" RenderSelectedPageOnly="false" Width="100%">
                                                    <telerik:RadPageView runat="server" ID="PageView1">
                                                        <div class="Tabarea">
                                                            <asp:Literal ID="lit_Summary" runat="server" />
                                                        </div>
                                                    </telerik:RadPageView>
                                                    <telerik:RadPageView runat="server" ID="PageView2" >
                                                        <div class="Tabarea">
                                                            <%-- <asp:Literal ID="lit_Items" runat="server" />--%>
                                                            <div style="padding: 10px; width:100%;">
                                                                <table cellspacing="1" cellpadding="2" border="0" width="100%" bgcolor="#E3E3E3">
                                                                    <tr>
                                                                        <td class="itemGridDet">Manufacturer
                                                                        </td>
                                                                        <td class="itemGridDet">Title
                                                                        </td>
                                                                        <td class="itemGridDet">Part #
                                                                        </td>
                                                                        <td class="itemGridDet">Quantity
                                                                        </td>
                                                                        <td class="itemGridDet">UPC
                                                                        </td>
                                                                        <td class="itemGridDet">SKU
                                                                        </td>
                                                                        <td class="itemGridDet">Estimated MSRP
                                                                        </td>
                                                                        <td class="itemGridDet">Total MSRP
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width:100%;">
                                                                    <asp:Repeater ID="rptItems" runat="server" >
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td style="background-color: White;">
                                                                                    <%# Eval("manufacturer")%>
                                                                                </td>
                                                                                <td style="background-color: White;">
                                                                                    <%# Eval("title")%>
                                                                                </td>
                                                                                <td style="background-color: White;">
                                                                                    <%# Eval("part_no")%>
                                                                                </td>
                                                                                <td style="background-color: White;">
                                                                                    <%# Eval("quantity")%>
                                                                                </td>
                                                                                <td style="background-color: White;">
                                                                                    <%# Eval("UPC")%>
                                                                                </td>
                                                                                <td style="background-color: White;">
                                                                                    <%# Eval("SKU")%>
                                                                                </td>
                                                                                <td align="right" style="background-color: White;">
                                                                                    <%# CommonCode.GetFormatedMoney(Eval("estimated_msrp"))%>
                                                                                </td>
                                                                                <td align="right" style="background-color: White;">
                                                                                    <%# CommonCode.GetFormatedMoney(Eval("total_msrp"))%>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater></tr>
                                                                </table>
                                                                <%--<table cellspacing="1" cellpadding="2" border="0" width="100%" bgcolor="#E3E3E3">
                                                                    <tr>
                                                                        <td class="itemGridDet">Manufacturer
                                                                        </td>
                                                                        <td class="itemGridDet">Title
                                                                        </td>
                                                                        <td class="itemGridDet">Part #
                                                                        </td>
                                                                        <td class="itemGridDet">Quantity
                                                                        </td>
                                                                        <td class="itemGridDet">UPC
                                                                        </td>
                                                                        <td class="itemGridDet">SKU
                                                                        </td>
                                                                        <td class="itemGridDet">Estimated MSRP
                                                                        </td>
                                                                        <td class="itemGridDet">Total MSRP
                                                                        </td>
                                                                    </tr>
                                                                    <asp:Repeater ID="rptItems" runat="server">
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td style="background-color: White;">
                                                                                    <%# Eval("manufacturer")%>
                                                                                </td>
                                                                                <td style="background-color: White;">
                                                                                    <%# Eval("title")%>
                                                                                </td>
                                                                                <td style="background-color: White;">
                                                                                    <%# Eval("part_no")%>
                                                                                </td>
                                                                                <td style="background-color: White;">
                                                                                    <%# Eval("quantity")%>
                                                                                </td>
                                                                                <td style="background-color: White;">
                                                                                    <%# Eval("UPC")%>
                                                                                </td>
                                                                                <td style="background-color: White;">
                                                                                    <%# Eval("SKU")%>
                                                                                </td>
                                                                                <td align="right" style="background-color: White;">
                                                                                    <%# CommonCode.GetFormatedMoney(Eval("estimated_msrp"))%>
                                                                                </td>
                                                                                <td align="right" style="background-color: White;">
                                                                                    <%# CommonCode.GetFormatedMoney(Eval("total_msrp"))%>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </table>--%>
                                                            </div>
                                                        </div>
                                                    </telerik:RadPageView>
                                                    <telerik:RadPageView runat="server" ID="PageView3">
                                                        <div class="Tabarea">
                                                            <asp:Literal ID="lit_Details" runat="server" />
                                                        </div>
                                                    </telerik:RadPageView>
                                                    <telerik:RadPageView runat="server" ID="PageView4">
                                                        <div class="Tabarea">
                                                            <asp:Literal ID="lit_terms_condition" runat="server" />
                                                        </div>
                                                    </telerik:RadPageView>
                                                    <telerik:RadPageView runat="server" ID="PageView5">
                                                        <div class="Tabarea">
                                                            <div style="padding: 10px;">
                                                                <div class="detailsSubHeading">
                                                                    Ask a Question
                                                                </div>
                                                                <div class="grBackDescription" style="padding-left: 2px; margin-top: 5px; margin-bottom: 5px; overflow: hidden;">
                                                                    <div style="overflow: hidden; padding: 10px; float: left;">
                                                                        <asp:TextBox runat="server" ID="txt_thread_title" Height="50" Width="500" TextMode="MultiLine"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfv_create_thread" runat="server" ErrorMessage="*"
                                                                            ForeColor="red" Display="Static" ControlToValidate="txt_thread_title" ValidationGroup="create_thread"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                    <div style="float: left; padding-left: 10px; margin-top: 4px;font-size:11px;">
                                                                    <div style="width:75px;float:left">
                                                                    <div style="display:block;" id='<%# "dv_" & Eval("auction_id") & "_ask"%>'>
                                                                        <asp:ImageButton ID="img_btn_create_thread" CommandName="create_thread" CommandArgument='<%# Eval("auction_id")%>'
                                                                            runat="server" AlternateText="Create" ImageUrl="/Images/fend/send.png" ValidationGroup="create_thread" />
                                                                      </div>
                                                                      <div style="display:none;" id='<%# "dv_" & Eval("auction_id") & "_msg"%>'>
                                                                      <span style="color:Red">Please Wait...</span>
                                                                            </div>
                                                                    </div>
                                                                            <div id='<%# "dv_" & Eval("auction_id") & "_confirm"%>' style="width:360px;float:left;overflow:hidden;">
                                                                            <asp:Label ID="rp_message" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
                                                                             </div>
                                                                </div>
                                                                <div style="margin-top: 5px; color: Red;">
                                                                    <asp:Literal ID="lit_msg_confirmation" Visible="false" runat="server" />
                                                                </div>
                                                                <div class="detailsSubHeading">
                                                                    <asp:Literal ID="lit_your_query" runat="server" Text="Your Queries"></asp:Literal>
                                                                </div>
                                                                <asp:Repeater ID="rep_after_queries" runat="server" OnItemCommand="rep_after_queries_ItemCommand"
                                                                    OnItemDataBound="rep_after_queries_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <div class="qryTitle">
                                                                            <%# Container.ItemIndex + 1 & ". " & Eval("title")%>
                                                                        </div>
                                                                        <div style="padding: 0px 0px 0px 10px; float: left;">
                                                                            <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="/Images/close_modal.gif"
                                                                                CommandArgument='<%#Eval("query_id") %>' CommandName="deleteQry" AlternateText="Delete Questions"
                                                                                ToolTip="Delete Questions" />
                                                                        </div>
                                                                        <div class="qryAlt" style="padding-left: 10px; clear: both;">
                                                                            <%--Eval("company_name") & ", " & Eval("state") & --%>
                                                                            <%# "on " & Eval("submit_date")%>
                                                                        </div>
                                                                        <div style="padding: 10px;">
                                                                            <asp:Repeater ID="rep_inner_after_queries" runat="server">
                                                                                <ItemTemplate>
                                                                                    <div class="qryDesc">
                                                                                        <img src='<%# IIF(Eval("image_path")<>"","/upload/users/" & Eval("user_id") & "/" & Eval("image_path"),"/images/buyer_icon.gif") %>'
                                                                                            alt="" style="float: left; padding: 2px; height: 23px; width: 22px;" />
                                                                                        <%#Eval("message") %>
                                                                                    </div>
                                                                                    <div class="qryAlt">
                                                                                        <%# IIf(Eval("buyer_title") = "", Eval("title"), Eval("buyer_title"))%>
                                                                                        <%# IIf(Eval("first_name") = "", Eval("buyer_first_name") & " " & Eval("buyer_last_name"), Eval("first_name") & " " & Eval("last_name"))%>,
                                                                                    <%# Eval("submit_date")%>
                                                                                    </div>
                                                                                    <br />
                                                                                    <br />
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                    </telerik:RadPageView>
                                                </telerik:RadMultiPage>
                                            </asp:Panel>
                                        </td>
                                        <td valign="top" style="padding: 5px 5px 0 5px; width: 210px;">
                                            <div style='height: <%# IIf(Eval("auction_type_id") = 3, "400px", "230px")%>; display: block; overflow: hidden; line-height: normal;'>
                                                <table cellpadding="4" cellspacing="0" width="100%" style="background-color: #F4F3F0;"
                                                    border="0">
                                                    <tr>
                                                        <td align="center">
                                                            <iframe id="iframe_price" runat="server" scrolling="no" frameborder="0" width="200px"
                                                                height="100px"></iframe>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="tr_box_caption">
                                                        <td align="center">
                                                            <div style="text-align: center; height: 16px; display: block;">
                                                                <asp:Literal ID="lit_text_box_caption" runat="server"></asp:Literal>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="tr_bid_now">
                                                        <td align="center">
                                                            <div style="text-align: center; height: 19px; display: block;">
                                                                <asp:TextBox ID="txt_bid_now" Visible="false" runat="server" MaxLength="10" Type="Number"
                                                                    Height="18" CssClass="TextBox" Value="0">
                                                                </asp:TextBox>
                                                            </div>
                                                            <div style="text-align:center;padding-left:30px;">
                                                                <div style="float:left; padding-top: 5px; color: Red;" id="div_Validator" runat="server">
                                                                </div>
                                                                <div style="float:left; padding-left:3px;padding-top:4px;">
                                                                    <asp:Image ID="img_amt_help" runat="server" ImageUrl="/Images/fend/help.jpg" style="cursor:pointer;display:none;" />
                                                                </div>
                                                            </div>


                                                        </td>
                                                    </tr>
                                                    <asp:Panel ID="pnl_buy_now_confirm" runat="server" Visible="false">
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <div style="border: 2px solid #F2F2F2; text-align: center; font-weight: bold; padding-left: 3px;">
                                                                    <table cellpadding="5" cellspacing="0" border="0" width="100%">
                                                                        <tr>
                                                                            <td>Qty<br />
                                                                                <asp:DropDownList ID="ddl_qty_confirm" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_qty_confirm_SelectedIndexChanged" />
                                                                            </td>
                                                                            <td>Amount<br />
                                                                                <span style='color: red;'>
                                                                                    <asp:HiddenField ID="hid_buy_now_confirm_amount" runat="server" Value="0" />
                                                                                    <asp:Literal ID="lit_buy_now_amount_confirm" runat="server" /></span>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </asp:Panel>
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <div style="text-align: center; height: 24px; display: block; margin: 4px 5px;">
                                                                <asp:ImageButton ID="img_bid_now" runat="server" Visible="false" />
                                                                <asp:Button ID="btn_refresh" runat="server" Style="display: none;" CommandName="refresh"
                                                                    CommandArgument='<%# Eval("auction_id")%>' />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                    <asp:Literal ID="ltr_proxy_verbiage" runat="server"></asp:Literal>
                                                <div style="text-align: center; height: 18px; display: block; padding-top: 5px;">
                                                    <asp:Label ID="lbl_bidder_history" runat="server" CssClass="bidHistory" Visible="false"></asp:Label>
                                                </div>
                                            </div>
                                            <asp:Panel ID="pnl_buy_now" runat="server" Visible="false">
                                                <div style="border: 2px solid #F2F2F2; text-align: center; font-weight: bold;">
                                                    <table cellpadding="10" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td>Qty<br />
                                                                <asp:DropDownList ID="ddl_qty" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_qty_SelectedIndexChanged" />
                                                            </td>
                                                            <td>Amount<br />
                                                                <span style='color: red;'>
                                                                    <asp:HiddenField ID="hid_buy_now_amount" runat="server" Value="0" />
                                                                    <asp:Literal ID="lit_buy_now_amount" runat="server" /></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:ImageButton ID="btn_buy_now" CommandName="btn_buy_now" CommandArgument='<%# Eval("auction_id")%>'
                                                                    runat="server" ImageUrl="/Images/fend/4.png" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                        <%-- <div id="tbl_user_own" runat="server" visible="false">
                                <table border="0" style="border: 1px solid grey; width: 280px;">
                                    <tr>
                                        <td style="text-align: left;">
                                            My shipping carrier is<br />
                                            <asp:TextBox ID="txt_shipping_type" runat="server"></asp:TextBox>
                                        </td>
                                        <td rowspan="2">
                                            <table id="table_stock" runat="server" width="100%">
                                                <tr>
                                                    <td valign="top" style="border-left: 1px solid gray;">
                                                        <span style="color: #10C0F5;">Stock Location:</span><br />
                                                        <asp:Label ID="lbl_stock_location" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            My account # is<br />
                                            <asp:TextBox ID="txt_shipping_account_number" runat="server"></asp:TextBox><br />
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </div>--%>
                        <%--  <asp:Repeater ID="rpt_auctions_gridview" runat="server">
                        <ItemTemplate>
                            <div style='width: 328px; float: left; padding: 10px; text-align: left; <%# iif(((Container.ItemIndex + 1) mod 3) = 0,"border-bottom: 1px dotted LightGray;","border-bottom: 1px dotted LightGray;border-right: 1px dotted LightGray;")%>'>
                                <div style="margin-top: 20px; height: 80px;">
                                    <div class="auctionTitle">
                                        <asp:Literal ID="lbl_title" runat="server" />
                                    </div>
                                    <asp:Label ID="lbl_sub_title" runat="server" CssClass="auctionSubTitle" />
                                </div>
                                <div style="height: 280px;">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td valign="top" align="center" style="padding: 5px; width: 125px;">
                                                <div class="Imgborder">
                                                    <a onclick="return false;" id="a_img_product" style="text-decoration: none; cursor: pointer"
                                                        runat="server">
                                                        <asp:Image ID="img_image" runat="server" CssClass="aucImage" CausesValidation="false" />
                                                    </a>
                                                    <asp:Repeater ID="rpt_auction_stock_image" runat="server">
                                                        <ItemTemplate>
                                                            <%# IIf((Container.ItemIndex) <> -1, "<a rel='" & "lightbox[PerfumeSize" & Eval("auction_id") & "1]" & "' style='text-decoration: none; display: none;' href='" & "/upload/auctions/stock_images/" & Eval("path") & "' title='" & Eval("title") & "'></a>", "")%>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <asp:HiddenField ID="hid_auction_type" runat="server" Value="0" />
                                                    <asp:HiddenField ID="hid_auction_id" runat="server" Value='<%# Eval("auction_id")%>' />
                                                </div>
                                                <div style="padding: 5px 0px 5px 0px; height: 20px; display: block;">
                                                    <asp:ImageButton ID="img_add_favourite" Visible="false" runat="server" CausesValidation="false"
                                                        CommandName="add_favourite" CommandArgument='<%# Eval("auction_id")%>' />
                                                </div>
                                                 <div style="padding: 5px 0px 5px 0px; height: 20px; display: block;">
                                    <asp:ImageButton ID="img_ask_question" runat="server" CausesValidation="false" ImageUrl="/Images/fend/askaquestion.png" />
                                    </div>
                                                <div style="padding: 5px 0px 5px 0px; height: 20px; display: block;">
                                                    <asp:ImageButton ID="img_hide_now" runat="server" Visible="false" CausesValidation="false"
                                                        CommandName="hide_now" CommandArgument='<%# Eval("auction_id")%>' />
                                                </div>
                                            </td>
                                            <td valign="top" style="padding: 5px;">
                                                <div style="height: 240px; width: 200px; display: block;">
                                                    <div class="auctionSubTitle">
                                                        Auction # :
                                                        <asp:Label Font-Size="11px" ID="lbl_auction_code" runat="server" />
                                                    </div>
                                                    <asp:Panel ID="pnl_proxy" runat="server">
                                                        <iframe id="iframe_price_summary" runat="server" scrolling="no" frameborder="0" width="200px"
                                                            height="183px"></iframe>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnl_now" ForeColor="#525252" runat="server">
                                                        <b>Buy it now for :
                                                            <asp:Literal ID="lit_buy_now_price" runat="server" /></b>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnl_quote" runat="server">
                                                        <asp:Literal ID="lit_quote_msg" runat="server" />
                                                    </asp:Panel>
                                                    <asp:Literal ID="lit_item_attachment" runat="server"></asp:Literal>
                                                </div>
                                                <div style="float: right; text-align: right;">
                                                    <asp:Literal ID="lit_offer" runat="server"></asp:Literal>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="height: 280px; display: block; overflow: hidden; line-height: normal;
                                    clear: both;">
                                    <table cellpadding="4" cellspacing="0" width="100%" style="background-color: #F4F3F0;"
                                        border="0">
                                        <tr>
                                            <td align="center">
                                                <iframe id="iframe_price" runat="server" scrolling="no" frameborder="0" width="200px"
                                                    height="100px"></iframe>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <div style="text-align: center; height: 16px; display: block;">
                                                    <asp:Literal ID="lit_text_box_caption" runat="server"></asp:Literal>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <div style="text-align: center; height: 19px; display: block;">
                                                    <asp:TextBox ID="txt_bid_now" Visible="false" runat="server" MaxLength="10" Type="Number"
                                                        Height="18" CssClass="TextBox" Value="0">
                                                    </asp:TextBox>
                                                </div>
                                                <div style="padding-top: 5px; color: Red;" id="div_Validator" runat="server">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top">
                                                <div style="text-align: center; height: 24px; display: block; margin: 4px 5px;">
                                                    <asp:ImageButton ID="img_bid_now" runat="server" Visible="false" />
                                                    <asp:Button ID="btn_refresh" runat="server" Style="display: none;" CommandName="refresh"
                                                        CommandArgument='<%# Eval("auction_id")%>' />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="text-align: center; height: 18px; display: block; padding-top: 5px;">
                                        <asp:Label ID="lbl_bidder_history" runat="server" CssClass="bidHistory" Visible="false"></asp:Label>
                                    </div>
                                </div>
                                <div class="ShortDesc" style="height: 100px; display: block; overflow: hidden;">
                                    <asp:Literal ID="lbl_short_desc" runat="server" />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                   <div class="viewStyle" style="margin-top: 20px; clear: both;">
                        <asp:Literal ID="lnk_view1" runat="server"></asp:Literal>
                    </div>--%>
        <%--<asp:Panel ID="Panel_Msg" runat="server" Style="display: none;border:solid 1px Gray;width:250px;height:115px;color:Black;background-color:White;" CssClass="modalPopup">  
          <center><div style="padding:10px 5px;"> <asp:Label ID="rp_message" runat="server" ForeColor="Red"
           CssClass="RPMsg" Text="Thanks for your question. Someone will get back to you within 24 hours."></asp:Label></div></center>
           <center><div style="padding:10px 5px;"><asp:ImageButton runat="server" ID="img_lnk_close" ImageUrl="/Images/close.gif" AlternateText="Close" /></div></center>
        </asp:Panel>  
          <asp:LinkButton runat="server" ID="link_target" CssClass="target_display"  />  
    <ajax:ModalPopupExtender ID="ModalPopupExtender" runat="server"   
        TargetControlID="link_target"  
        PopupControlID="Panel_Msg"   
        BackgroundCssClass="modalBackground"  
        CancelControlID="img_lnk_close"   
        DropShadow="false"   />--%>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:UpdatePanel>
            <asp:Panel ID="pnl_noItem" runat="server">
                <div style="border: 1px solid #10AAEA; background: url('/Images/fend/grdnt_template.jpg') repeat; height: 800px; padding-top: 25px; text-align: center;">
                    <b>
                        <asp:Literal ID="lit_no_item" runat="server"></asp:Literal></b>
                </div>
            </asp:Panel>
        </div>
    </form>
    <script type="text/javascript" src="/pcs_js/divarrow_tooltip.js"></script>
</body>
</html>
