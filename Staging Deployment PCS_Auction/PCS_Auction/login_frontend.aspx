﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AuctionMain.master" AutoEventWireup="false"
    CodeFile="login_frontend.aspx.vb" Inherits="login_frontend" %>

<%@ Register Src="~/UserControls/login_ctrl.ascx" TagName="Login" TagPrefix="UC1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <br />
    <div class="innerwraper">
        <div class="paging">
            <ul>
                <li><a href="/">Home</a></li>
                <li>Login</li>
            </ul>
        </div>
        <div class="clear">
        </div>
        <br />
        <div id="secondarycontent" style="background-color: White;">
            <br />
            <div id="leftnav">
                <h4>
                    <a href="/login.html">LOGIN</a>
                </h4>
                <div class="leftcalloutbox">
                    <div class="leftcalloutboxcontainer">
                        Helping you grow your business is our number-one priority.
                    </div>
                </div>
                <div id="leftcontentboxfooter">
                    <div id="leftcontentboxfootercontainer">
                        <div style="font-size: 13px; font-weight: bold;">
                            Why become a member?</div>
                        <div style="padding-top: 10px;">
                            <ul style="list-style: url(/images/arrow_home.gif)">
                                <li>No bidding fees.</li>
                                <li>Auctions without minimum bids.</li>
                                <li>Purchase outright with buy it now options.</li>
                                <li>Phones, tablets, laptops and accessories.</li>
                                <li>New and pre-owned products.</li>
                                <li>Hard to find models.</li>
                                <li>Choose from multiple technologies, models and brands.</li>
                                <li>Pre-qualified buyers.</li>
                                <li>Market driven pricing.</li>
                                <li>Products in stock now.</li>
                            </ul>
                        </div>
                        <div>
                            <a href="/sign-up.html" style="color: #10AAEA; font-weight: bold;">Join now</a>
                        </div>
                        <div style="padding-top: 16px;">
                            <div style="font-weight: bold;">
                                Questions?</div>
                            <div style="padding-top: 10px;">
                                Email: <a href="mailto:auctions@pcsww.com">auctions@pcsww.com</a><br />
                                Phone: 973.805.7400 ext 179
                            </div>
                        </div>
                    </div>
                </div>
                <div class="leftcontentbox">
                </div>
            </div>
            <div id="maincontent">
                <div class="products">
                    <div class="seprator">
                    </div>
                    <h1>
                        Member login</h1>
                    <p>
                        Only companies who have joined the PCS Wireless online auction and become members
                        may bid on auctions or purchase products on the site.
                    </p>
                    <p>
                        If you are already a member, please sign in with your user name and password below.
                    </p>
                    <h4 style="display: inline;">
                        <span style="color: black;">Want to become a member?</span> It's FREE. <a href="/sign-up.html"
                            style="font-weight: bold;">Join now</a></h4>
                    <div style="padding-top: 4px;">
                        <UC1:Login runat="server" ID="signin_ctrl" />
                    </div>
                </div>
                <!--products
    -->
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
</asp:Content>
