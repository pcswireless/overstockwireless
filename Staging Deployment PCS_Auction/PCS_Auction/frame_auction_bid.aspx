﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frame_auction_bid.aspx.vb" Inherits="frame_auction_bid" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <link href="Style/stylesheet.css" rel="stylesheet" type="text/css" /> 
    <link href="Style/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
     <%--<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>--%>
    <script language="javascript" type="text/javascript">
        function closeNow(typeId, ctlId) {

            if (parseInt(typeId) == 2 || parseInt(typeId) == 3)
            { window.opener.RefreshList(ctlId); }


            window.close();
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptManager11" runat="server">
    </asp:ScriptManager>
      <div class="modalHeading">
        <asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
        <asp:HiddenField ID="hid_buyer_id" runat="server" Value="0" />
        <asp:HiddenField ID="hid_buyer_user_id" runat="server" Value="0" />
        <asp:HiddenField ID="hid_buy_now_price" runat="server" Value="0" />
        <asp:HiddenField ID="hid_bid_now_price" runat="server" Value="0" />
    </div>
    <div id="bid-popup" style="width:100%;">
       <h2><span class="blue"> <asp:Literal ID="lbl_modal_catg" runat="server"></asp:Literal></span> <asp:Literal ID="lbl_modal_caption" runat="server"></asp:Literal> </h2>
    <asp:Panel runat="server" ID="pnl_offer" Visible="true" DefaultButton="ImageButtonOffer">
      
           <!-- <table cellpadding="5" cellspacing="0" width="100%" border="0">-->
            
                  <div class="inputboxes">
              <%--<tr id="tr_price" runat="server"> --%>
                   <!-- <td>--><div id="tr_price"  class="fleft qnty" runat="server">
                        <%--<br />--%>
                       <%-- <div style="text-align: center;">--%>
                            <span class="text" id="tr_qty" runat="server">I want to buy</span> 
                                <asp:TextBox ID="txt_qty" 
                                 runat="server" MaxLength="10" Type="Number" onFocus="(this.value == 'Quantity') && (this.value = '')" onBlur="(this.value == '') && (this.value = 'Quantity')">
                                 <%--<NumberFormat DecimalDigits="0" />--%>
                            </asp:TextBox>
                                <asp:CompareValidator ID="CValidator2" runat="server" Display="Dynamic" ControlToValidate="txt_qty"
                                    ErrorMessage=" * " Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>
                                <asp:RequiredFieldValidator ID="RFOffer2" runat="server" ControlToValidate="txt_qty"
                                    ErrorMessage=" * " Display="Dynamic">
                                </asp:RequiredFieldValidator>
                                <span class="text">unit(s) for </span><asp:Literal runat="server"
                                    ID="lit_qty" Text="this lot for&nbsp;"></asp:Literal> 
                            <div class="fleft"><span class="text">$</span>
                             <asp:textbox ID="txt_amount" runat="server" MaxLength="10" Type="Number" onFocus="(this.value == 'Amount') && (this.value = '')" onBlur="(this.value == '') && (this.value = 'Amount')">
                           </asp:textbox>
                            <asp:RequiredFieldValidator ID="RFOffer1" runat="server" ErrorMessage=" * " ForeColor="red"
                                ControlToValidate="txt_amount" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="CValidator1" runat="server" Display="Dynamic"
                                ControlToValidate="txt_amount" ErrorMessage=" * " ValidationExpression="^\d*\.?\d*$"></asp:RegularExpressionValidator>
                            <asp:CompareValidator ID="CValidator_a" runat="server" Display="Dynamic" ControlToValidate="txt_amount"
                                ErrorMessage=" * " Type="Double" Operator="GreaterThanEqual"></asp:CompareValidator><span class="text">/unit.</span> <%--</div>--%>
                                </div></div>
                    <!--</td>-->
                    <div class="clear"></div>
                    </div><!--inputboxes class ends-->
                   
               <!--</tr>-->
                <%--<tr id="tr_message" runat="server">--%><div id="tr_message" runat="server">
                    <!--<td style="padding: 5px 0px 5px 0px; text-align: center;">--><div style="padding: 5px 0px 5px 0px; text-align: center;">
                        <asp:Literal ID="lit_offer_text" runat="server"></asp:Literal>
                    <!--</td>--></div>
                <!--</tr>--></div>
               <%-- <!--<tr>--><div>
                    <!--<td>--><div>
                      
                    <!--</td>--></div>
                <!--</tr>--></div>--%>
                <asp:Panel runat="server" ID="pnl_shipping">
                <!--<tr>--><div>
                    <!--<td style="text-align: left; font-size: 13px; padding-left: 80px;">--><div style="text-align: left; font-size: 13px; padding-left: 80px;">
                        <b>Shipping Options</b>
                    <!--</td>--></div>
                <!--</tr>--></div>
                <!--<tr>--><div>
                    <!--<td style="padding: 0px 0px 0px 0px; color: black;">--><div style="padding: 0px 0px 0px 0px; color: black;">
                        <center style="text-align: left; padding-left: 80px;">
                            <asp:RadioButtonList ID="rdo_option" runat="server" AutoPostBack="true">
                                <asp:ListItem Text="Use my own shipping carrier to ship this item to me" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Estimated FedEx Shipping Rates" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                        </center>
                        <center>
                           
                            <div id="tbl_user_own" runat="server" visible="false">
                                <!--<table border="0" style="border: 1px solid grey; width: 280px;">--><div style="border: 1px solid grey; width: 280px;" >
                                    <!--<tr>--><div>
                                        <!--<td style="text-align: left;">--><div style=" float:left; text-align: left; overflow:hidden;" >
                                            My shipping carrier is
                                            <asp:TextBox ID="txt_shipping_type" runat="server"></asp:TextBox>
                                        <!--</td>--></div>
                                        <!--<td rowspan="2">--><div>
                                           <%--<table id="table_stock" runat="server" width="100%">--%><div id="table_stock"  runat="server" style="width:100%;">
                                                <!--<tr>--><div>
                                                    <!--<td valign="top" style="border-left: 1px solid gray;">--><div style="border-left: 1px solid gray;">
                                                        <span style="color: #10C0F5;">Stock Location:</span>
                                                        <asp:Label ID="lbl_stock_location" runat="server" Text=""></asp:Label>
                                                    <!--</td>--></div>
                                                <!--</tr>--></div>
                                            <!--</table>--></div>
                                        <!--</td>--></div>
                                    <!--</tr>--></div>
                                    <!--<tr>--><div>
                                        <!--<td style="text-align: left">--><div style="text-align: left; float:left; overflow:hidden;">
                                            My account # is
                                            <asp:TextBox ID="txt_shipping_account_number" runat="server"></asp:TextBox>
                                        <!--</td>--></div>
                                        <!--<td>--><div>
                                        <!--</td>--></div>
                                    <!--</tr>--></div>
                                <!--</table>--></div>
                            </div>
                        </center>
                        <center>
                            <div id="tbl_user_ours" runat="server" visible="false" style="border: 1px solid grey;
                                width: 300px; text-align: left;">
                                <asp:RadioButtonList ID="rdo_fedex" runat="server">
                                    <asp:ListItem Text=" FEDEX EXPRESS SAVER" Value="0" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text=" FEDEX 2 DAY" Value="1"></asp:ListItem>
                                    <asp:ListItem Text=" FEDEX EXPRESS SAVER" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                            <%--<asp:Button ID="btn_save" runat="server" Text="Save" />--%><br />
                        </center>
                    <!--</td>--></div>
                <!--</tr>--></div>
                </asp:Panel>
                <!--<tr>-->
                    <!--<td style="padding: 0px 0px 0px 0px; text-align: center; color: Blue;">-->
                       <p class="orange">Please note that PCSWireless reserve the rights to cancel any
                            order.</p>
                       
                        <p class="orange">Rates are subject to change. Additional shipping and handling
                            fees may apply.</p>
                      <p class="checkbox">
                        <asp:CheckBox ID="chk_accept_term" runat="server" Text=" Accept our Terms & Conditions"
                            TextAlign="Right" />
                            </p>
                            <div class="clear"></div>
                    <!--</td>-->
                <!--</tr>--> <!--Message div-->
                <!--<tr>-->
                    <!--<td align="center">-->
                        
                        <asp:ImageButton runat="server" AlternateText="Send" ID="ImageButtonOffer" ImageUrl="/Images/fend/send.png" />&nbsp;&nbsp;<asp:ImageButton
                            ID="ImageButtonCancelOffer" CausesValidation="false" runat="server" AlternateText="Cancel"
                            ImageUrl="/Images/cancel.png" />
                       
                        <asp:Label ID="lbl_terms_error" runat="server" Text="Please check our terms & conditions."
                            ForeColor="Red" Visible="false" EnableViewState="false"></asp:Label>
                    <!--</td>-->
                <!--</tr>-->
            <!--</table>-->
       
        
    </asp:Panel>



    <asp:Panel runat="server" ID="pnl_confirm" Visible="true" DefaultButton="ImageButtonCancelMsg">
        <center>
            <!--<table cellpadding="5" cellspacing="0" border="0" style="height: 300px;">--><div style="height: 300px;">
                <!--<tr>--><div>
                    <!--<td align="center">--><div>
                        <asp:Label ID="lbl_popmsg_msg" ViewStateMode="Disabled" runat="server" ForeColor="#B18B4F"
                            Font-Bold="true"></asp:Label>
                    <!--</td>--></div>
                <!--</tr>--></div>
                <!--<tr>--><div>
                    <!--<td align="center">--><div>
                        <asp:ImageButton ID="ImageButtonCancelMsg" CausesValidation="false" runat="server"
                            AlternateText="Close" ImageUrl="/Images/close.gif" />
                    <!--</td>--></div>
                <!--</tr>--></div>
            <!--</table>--></div>
        </center>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnl_send_quote" Visible="false" DefaultButton="ImageButton_quote_send">
        <center>
           <!-- <table cellpadding="5" cellspacing="0" border="0" style="height: 300px;">--><div style="height: 300px;">
                <!--<tr>--><div>
                    <!--<td align="left" colspan="2">--><div>
                        <asp:TextBox runat="server" ID="txt_quote_desc" Width="300" TextMode="MultiLine"
                            Height="60" CssClass ="ask_txtbx" BorderStyle="None"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Required_txt_quote_desc" ValidationGroup="VG1" runat="server"
                            Font-Size="10px" ControlToValidate="txt_quote_desc" ErrorMessage="*" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    <!--</td>--></div>
                <!--</tr>--></div>
                <!--<tr>--><div>
                    <!--<td class="modalCaption">--><div style="float:left; overflow:hidden;">
                        Quote File
                    <!--</td>--></div>
                    <!--<td align="left">--><div>
                        <asp:FileUpload ID="fle_quote_upload" Width="160" size="15" runat="server" />
                    <!--</td>--></div>
                <!--</tr>--></div>
                <!--<tr>--><div>
                    <!--<td colspan="2">--><div>
                        <asp:ImageButton runat="server" AlternateText="Send Offer" ID="ImageButton_quote_send"
                            ValidationGroup="VG1" ImageUrl="/Images/fend/3.png" />&nbsp;&nbsp;<asp:ImageButton
                                ID="ImageButtonCancelQuote" CausesValidation="false" runat="server" AlternateText="Cancel"
                                ImageUrl="/Images/cancel.png" />
                   <!-- </td>--></div>
                <!--</tr>--></div>
                <!--<tr>--><div>
                    <!--<td class="modalCaption">--><div style="float:left; overflow:hidden;">
                    <!--</td>--></div>
                    <!--<td>--><div>
                        <asp:Label ID="lbl_error_quote" runat="server" ForeColor="Red"></asp:Label>
                   <!-- </td>--></div>
                <!--</tr>--></div>
            <!--</table>--></div>
        </center>
    </asp:Panel>
    </div>
    </form>
</body>
</html>
