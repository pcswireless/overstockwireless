﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Auction_Price_Detail.aspx.vb"
    Inherits="Auction_Price_Detail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" rel="Stylesheet" href="/Style/AuctionBody.css" />
    <meta id="Meta1" http-equiv="refresh" content="30" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnl_detail" runat="server" Width="100%">
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td colspan="2">
                    <asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
                    <asp:HiddenField ID="hid_auction_type_id" runat="server" Value="0" />
                </td>
            </tr>
            <tr>
                <td width="50%" valign="top">
                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr id="tr_rank" runat="server" visible="false">
                            <td valign="top">
                                <div class="currentRankD" style="padding-top:0px;">
                                    <asp:Label ID="lbl_current_rank_caption" runat="server" Text="Current Rank : "></asp:Label>
                                    &nbsp;<asp:Label ID="lbl_current_rank" runat="server"></asp:Label>
                                </div>
                                <div class="currentRankD" style="padding-top:0px;">
                                    <asp:Label ID="lbl_bid_amt_caption" runat="server" Text="Current Bid :"></asp:Label>
                                    &nbsp;<asp:Label ID="lbl_bid_amt" runat="server" CssClass="currentRankD"></asp:Label>
                                </div>
                                  </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="bidderprice_detail">
                                    <asp:Literal ID="lit_biddeer_price" runat="server" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                            <td valign="top" style="font-size: 13px;">
                                <asp:Panel ID="pnl_buy_now" runat="server" Visible="false">
                                    <b>List Price at : </b>
                                    <asp:Literal ID="lbl_list_price" runat="server"></asp:Literal>
                                </asp:Panel>
                                <asp:Panel ID="pnl_trad_proxy" runat="server" Visible="false">
                                    <table cellspacing="0" cellpadding="3" border="0" width="100%">
                                        <tr id="trStartPrice" runat="server">
                                            <td>
                                                <b>Start Price at : </b>
                                                <asp:Literal ID="lbl_start_price" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Minimum Increment : </b>
                                                <asp:Literal ID="lbl_min_increament" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>
                                                    <asp:Literal ID="lbl_reserve_price_caption" runat="server" Text="Reserve price : "></asp:Literal></b><asp:Literal
                                                        ID="lbl_reserve_price" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>
                                                    <asp:Literal ID="lbl_no_of_bids_caption" runat="server" Text="# of Bid : "></asp:Literal></b>
                                                <asp:Literal ID="lbl_no_of_bids" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
