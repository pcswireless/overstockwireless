﻿
Partial Class Reports_AdminReports
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            
        End If
    End Sub

    Protected Sub btReport1_Click(sender As Object, e As EventArgs) Handles btReport1.Click


        If (startdate.Text = String.Empty) Then
            startdate.Text = DateTime.Now.Date()
            enddate.Text = DateTime.Now.AddHours(24).ToShortDateString()
        ElseIf (enddate.Text = String.Empty) Then
            enddate.Text = Convert.ToDateTime(startdate.Text).AddHours(24).ToString
        Else
            enddate.Text = Convert.ToDateTime(enddate.Text).AddHours(24).ToString
        End If

        Dim dt As DataTable = New DataTable
        Dim queryStr As New StringBuilder()
        'populate Highest bid, per auction, per user
        queryStr.Append("SELECT a.*, bidDetail.bid_date, bidDetail.action ")
        queryStr.Append("FROM ( ")
        queryStr.Append("SELECT ")
        queryStr.Append("auction.auction_id ")
        queryStr.Append(",bid.buyer_id ")
        queryStr.Append(",auction.code ")
        queryStr.Append(",auction.title ")
        queryStr.Append(",auction.sub_title ")
        queryStr.Append(",auction.start_date ")
        queryStr.Append(",auction.end_date ")
        queryStr.Append(",max(bid.bid_id) bid_id ")
        queryStr.Append(",MAX(bid.[bid_amount]) maxBidAmount ")
        queryStr.Append(",buyer.email ")
        queryStr.Append(",buyer.company_name ")
        queryStr.Append(",buyerCountry =(Select  buyerCountry.name from [PCS_Auction].[dbo].[tbl_master_countries] buyerCountry where  buyerCountry.country_id=buyer.country_id)")
        queryStr.Append(",buyer.contact_first_name ")
        queryStr.Append(",buyer.contact_last_name ")
        queryStr.Append("FROM [PCS_Auction].[dbo].[tbl_auctions] auction ")
        queryStr.Append("JOIN [PCS_Auction].[dbo].[tbl_auction_bids] bid ")
        queryStr.Append("ON bid.auction_id = auction.auction_id ")
        queryStr.Append("JOIN [PCS_Auction].[dbo].[tbl_reg_buyers] buyer ")
        queryStr.Append("ON buyer.buyer_id=bid.buyer_id ")
        queryStr.Append("WHERE auction.start_date>='")
        queryStr.Append(startdate.Text)
        queryStr.Append("' and auction.start_date< '")
        queryStr.Append(enddate.Text)
        queryStr.Append("' GROUP BY ")
        queryStr.Append("auction.auction_id ")
        queryStr.Append(",auction.start_date ")
        queryStr.Append(",auction.end_date ")
        queryStr.Append(",auction.code ")
        queryStr.Append(",auction.title ")
        queryStr.Append(",auction.sub_title ")
        queryStr.Append(",bid.buyer_id ")
        queryStr.Append(",buyer.email ")
        queryStr.Append(",buyer.company_name ")
        queryStr.Append(",buyer.country_id ")
        queryStr.Append(",buyer.contact_first_name ")
        queryStr.Append(",buyer.contact_last_name ")
        queryStr.Append(") a ")
        queryStr.Append("JOIN [PCS_Auction].[dbo].[tbl_auction_bids] bidDetail ")
        queryStr.Append("ON bidDetail.bid_id=a.bid_id ")
        queryStr.Append("ORDER BY a.auction_id, a.buyer_id, bidDetail.bid_date, bidDetail.action ")


        dt = SqlHelper.ExecuteDatatable(queryStr.ToString())
       
        Dim fileName As String = String.Empty

        fileName = String.Format("Report-{0}-{1}.xls", "Highest bid_per auction_per user", startdate.Text)

        Dim attachment As String = "attachment; filename=" + fileName
        Response.ClearContent()
        Response.AddHeader("content-disposition", attachment)
        Response.ContentType = "application/excel"


        Dim tab As String = String.Empty

        For Each dc As DataColumn In dt.Columns
            Response.Write(tab + dc.ColumnName)
            tab = vbTab
        Next
        Response.Write(vbLf)

        Dim i As Integer
        For Each dr As DataRow In dt.Rows
            tab = String.Empty
            For i = 0 To dt.Columns.Count - 1
                Response.Write(tab & dr(i).ToString())
                tab = vbTab
            Next
            Response.Write(vbLf)
        Next
        Response.End()

    End Sub

    Protected Sub Calendar2_SelectionChanged1(sender As Object, e As EventArgs) Handles Calendar2.SelectionChanged

        enddate.Text = Calendar2.SelectedDate.ToShortDateString()
        Calendar2.Visible = False

    End Sub

    Protected Sub Calendar1_SelectionChanged1(sender As Object, e As EventArgs) Handles Calendar1.SelectionChanged

        startdate.Text = Calendar1.SelectedDate.ToShortDateString()
        Calendar1.Visible = False

    End Sub

    Protected Sub startdate_TextChanged(sender As Object, e As EventArgs) Handles startdate.TextChanged

    End Sub

    Protected Sub ibselectFrom_Click1(sender As Object, e As ImageClickEventArgs) Handles ibselectFrom.Click
        'If (Calendar1.Visible) Then
        Calendar1.Visible = False
        ' Else
        Calendar1.Visible = True
        ' End If
    End Sub

    Protected Sub ibselectTo_Click1(sender As Object, e As ImageClickEventArgs) Handles ibselectTo.Click
        ' If (Calendar2.Visible) Then
        'Calendar2.Visible = False
        '  Else
        Calendar2.Visible = True
        ' End If
    End Sub

    Protected Sub btReport2_Click(sender As Object, e As EventArgs) Handles btReport2.Click


        Dim dt As DataTable = New DataTable
        Dim queryStr As New StringBuilder()
        'populate List of approved bidders
        queryStr.Append("SELECT TOP 1000 * FROM [PCS_Auction].[dbo].[tbl_reg_buyers]  WHERE approval_status='Approved' ")
        
        dt = SqlHelper.ExecuteDatatable(queryStr.ToString())

        Dim fileName As String = String.Empty

        fileName = String.Format("Report-{0}-{1}.xls", "List of approved bidders", DateTime.Now.ToString)

        Dim attachment As String = "attachment; filename=" + fileName
        Response.ClearContent()
        Response.AddHeader("content-disposition", attachment)
        Response.ContentType = "application/excel"


        Dim tab As String = String.Empty

        For Each dc As DataColumn In dt.Columns
            Response.Write(tab + dc.ColumnName)
            tab = vbTab
        Next
        Response.Write(vbLf)

        Dim i As Integer
        For Each dr As DataRow In dt.Rows
            tab = String.Empty
            For i = 0 To dt.Columns.Count - 1
                Response.Write(tab & dr(i).ToString())
                tab = vbTab
            Next
            Response.Write(vbLf)
        Next
        Response.End()


    End Sub

    Protected Sub btReport3_Click(sender As Object, e As EventArgs) Handles btReport3.Click


        Dim dt As DataTable = New DataTable
        Dim queryStr As New StringBuilder()
        'populate List of pending bidders
        queryStr.Append("SELECT *   FROM [PCS_Auction].[dbo].[tbl_reg_buyers]  WHERE not (approval_status='Approved' or approval_status='Reject') ")

        dt = SqlHelper.ExecuteDatatable(queryStr.ToString())

        Dim fileName As String = String.Empty

        fileName = String.Format("Report-{0}-{1}.xls", "List of pending bidders", DateTime.Now.ToString)

        Dim attachment As String = "attachment; filename=" + fileName
        Response.ClearContent()
        Response.AddHeader("content-disposition", attachment)
        Response.ContentType = "application/excel"


        Dim tab As String = String.Empty

        For Each dc As DataColumn In dt.Columns
            Response.Write(tab + dc.ColumnName)
            tab = vbTab
        Next
        Response.Write(vbLf)

        Dim i As Integer
        For Each dr As DataRow In dt.Rows
            tab = String.Empty
            For i = 0 To dt.Columns.Count - 1
                Response.Write(tab & dr(i).ToString())
                tab = vbTab
            Next
            Response.Write(vbLf)
        Next
        Response.End()
    End Sub

    Protected Sub enddate_TextChanged(sender As Object, e As EventArgs) Handles enddate.TextChanged

    End Sub
End Class

