﻿
Partial Class Reports_SalesRepAuctionReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            populate_search_criteria()
        End If
    End Sub
    Private Sub populate_search_criteria()
        Dim str As String = ""
        Dim dt As DataTable = New DataTable

        'populate auction items
        str = "select A.product_item_id,(case ISnull(M.name,'') when '' then '' else M.name + '--' end) + ISNULL(A.name,'') As name from tbl_auction_product_items A INNER JOIN tbl_master_manufacturers M ON A.manufacturer_id=M.manufacturer_id"
        dt = SqlHelper.ExecuteDataTable(str)
        ddl_auction_items.DataSource = dt
        ddl_auction_items.DataTextField = "name"
        ddl_auction_items.DataValueField = "product_item_id"
        ddl_auction_items.DataBind()
        ddl_auction_items.Items.Insert(0, New ListItem("<--Please Select-->", "0"))

        'populate bidders

    End Sub
End Class
