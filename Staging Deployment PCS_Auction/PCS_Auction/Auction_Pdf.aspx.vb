﻿Imports System.IO
Imports iTextSharp.text.pdf
Imports iTextSharp.text
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.html

Partial Class Auction_Pdf
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            form1.Action = Request.RawUrl
            If Request.QueryString.Get("i") <> Nothing AndAlso IsNumeric(Request.QueryString.Get("i")) Then
                fill_auction_details(Request.QueryString.Get("i"))
                img_close.ImageUrl = SqlHelper.of_FetchKey("ServerHttp") & "/Images/cross.gif"
                If String.IsNullOrEmpty(Request.QueryString.Get("t")) Then
                    img_close.Attributes.Add("onclick", "window.close(); return false;")
                Else
                    Dim _path As String = ""
                     If Request.QueryString.Get("t") = 1 Then
                        _path = "/live-auctions.html"
                    ElseIf Request.QueryString.Get("t") = 2 Then
                        _path = "/auction-history.html"
                    ElseIf Request.QueryString.Get("t") = 3 Then
                        _path = "/my-live-auctions.html"
                    ElseIf Request.QueryString.Get("t") = 4 Then
                        _path = "/my-account.html"
                    End If
                    img_close.Attributes.Add("onclick", "location.href='" & _path & "'; return false;")
                End If


            Else
                Response.Redirect("/")
            End If
        End If
    End Sub

    Protected Sub fill_auction_details(ByVal auction_id As Int32)
        If auction_id > 0 Then
            Dim ds As New DataSet
            Dim str As String = "SELECT A.auction_id,"
            str = str & "ISNULL(A.code, '') AS code,"
            str = str & "ISNULL(A.title, '') AS title,"
            str = str & "ISNULL(A.sub_title, '') AS sub_title,"
            str = str & "ISNULL(A.tax_details, '') AS tax_details,"
            str = str & "ISNULL(A.product_catetory_id, 0) AS product_catetory_id,"
            str = str & "ISNULL(A.stock_condition_id, 0) AS stock_condition_id,"
            str = str & "ISNULL(A.packaging_id, 0) AS packaging_id,"
            str = str & "ISNULL(L.name, '') AS stock_location,"
            str = str & "ISNULL(A.description, '') AS description,"
            str = str & "ISNULL(A.special_conditions, '') AS special_conditions,"
            str = str & "ISNULL(A.payment_terms, '') AS payment_terms,"
            str = str & "ISNULL(A.product_details, '') AS product_details,"
            str = str & "ISNULL(A.shipping_terms, '') AS shipping_terms,"
            str = str & "ISNULL(A.other_terms, '') AS other_terms,"
            str = str & "ISNULL(A.auction_category_id, 0) AS auction_category_id,"
            str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"
            str = str & "ISNULL(A.request_for_quote_message, '') AS request_for_quote_message,"
            str = str & "ISNULL(A.after_launch_message, '') AS after_launch_message,"
            str = str & "ISNULL(A.seller_id, 0) AS seller_id,"
            str = str & "ISNULL(A.code_f, '') AS code_f,"
            str = str & "ISNULL(A.code_s, '') AS code_s,"
            str = str & "ISNULL(A.code_c, '') AS code_c,"
            str = str & "ISNULL(A.title_f, '') AS title_f,"
            str = str & "ISNULL(A.title_s, '') AS title_s,"
            str = str & "ISNULL(A.title_c, '') AS title_c,"
            str = str & "ISNULL(A.sub_title_f, '') AS sub_title_f,"
            str = str & "ISNULL(A.sub_title_s, '') AS sub_title_s,"
            str = str & "ISNULL(A.sub_title_c, '') AS sub_title_c,"
            str = str & "ISNULL(A.tax_details_f, '') AS tax_details_f,"
            str = str & "ISNULL(A.tax_details_s, '') AS tax_details_s,"
            str = str & "ISNULL(A.tax_details_c, '') AS tax_details_c,"
            str = str & "ISNULL(A.description_f, '') AS description_f,"
            str = str & "ISNULL(A.description_s, '') AS description_s,"
            str = str & "ISNULL(A.description_c, '') AS description_c,"
            str = str & "ISNULL(A.special_conditions_f, '') AS special_conditions_f,"
            str = str & "ISNULL(A.special_conditions_s, '') AS special_conditions_s,"
            str = str & "ISNULL(A.special_conditions_c, '') AS special_conditions_c,"
            str = str & "ISNULL(A.payment_terms_f, '') AS payment_terms_f,"
            str = str & "ISNULL(A.payment_terms_s, '') AS payment_terms_s,"
            str = str & "ISNULL(A.payment_terms_c, '') AS payment_terms_c,"
            str = str & "ISNULL(A.product_details_f, '') AS product_details_f,"
            str = str & "ISNULL(A.product_details_s, '') AS product_details_s,"
            str = str & "ISNULL(A.product_details_c, '') AS product_details_c,"
            str = str & "ISNULL(A.shipping_terms_f, '') AS shipping_terms_f,"
            str = str & "ISNULL(A.shipping_terms_s, '') AS shipping_terms_s,"
            str = str & "ISNULL(A.shipping_terms_c, '') AS shipping_terms_c,"
            str = str & "ISNULL(A.other_terms_f, '') AS other_terms_f,"
            str = str & "ISNULL(A.other_terms_s, '') AS other_terms_s,"
            str = str & "ISNULL(A.other_terms_c, '') AS other_terms_c,"
            str = str & "ISNULL(A.short_description, '') AS short_description,"
            str = str & "ISNULL(A.short_description_s, '') AS short_description_s,"
            str = str & "ISNULL(A.short_description_c, '') AS short_description_c,"
            str = str & "ISNULL(A.short_description_f, '') AS short_description_f,"
            str = str & "ISNULL(A.start_date, '1/1/1900') AS start_date,"
            str = str & "ISNULL(A.end_date, '1/1/1900') AS end_date,"
            str = str & "ISNULL(A.display_end_time, '1/1/1900') AS display_end_time,"
            str = str & "ISNULL(A.is_display_start_date, 0) AS is_display_start_date,"
            str = str & "ISNULL(A.is_display_end_date, 0) AS is_display_end_date,"
            str = str & "ISNULL(B.name, '') AS stock_condition,"
            str = str & "ISNULL(L.name, '') AS stock_location,"
            str = str & "ISNULL(C.name, '') AS packaging,"
            str = str & "ISNULL(IMG.stock_image_id, 0) AS stock_image_id,ISNULL(IMG.filename, '') AS filename,"
            str = str & "ISNULL(D.company_name, '') AS seller,"
            str = str & "ISNULL(L.name, '') AS stock_location,"
            str = str & "ISNULL(B.name, '') AS stock_condition,"
            str = str & "ISNULL(C.name, '') AS package"
            str = str & " FROM "
            str = str & "tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations L on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B on A.stock_condition_id=B.stock_condition_id "
            str = str & "Left Join tbl_master_packaging C on A.packaging_id=C.packaging_id left join tbl_reg_sellers D on A.seller_id=D.seller_id"
            str = str & " left join tbl_auction_stock_images IMG on A.auction_id=IMG.auction_id and IMG.stock_image_id=(SELECT top 1 stock_image_id from tbl_auction_stock_images where auction_id=a.auction_id order by position) "
            str = str & " WHERE "
            str = str & "A.auction_id =" & auction_id

            ds = SqlHelper.ExecuteDataset("select [product_item_id], isnull(m.[description],'') AS manufacturer, case '" & Application("_lang") & "' when 'fr-FR' then  [part_no_f] when 'es-ES' then  [part_no_s] when 'zh-CN' then  [part_no_c] else [part_no] End AS part_no, case '" & Application("_lang") & "' when 'fr-FR' then p.[description_f] when 'es-ES' then p.[description_s] when 'zh-CN' then p.[description_c] else p.[description] End AS description,isnull(quantity,0) as quantity  from [tbl_auction_product_items] p left join tbl_master_manufacturers m on p.manufacturer_id=m.manufacturer_id where auction_id= " & auction_id & "; " & str)
            Grid_Items.DataSource = ds.Tables(0)
            Grid_Items.DataBind()
            If ds.Tables(0).Rows.Count = 0 Then
                pnl_auc_item.Visible = False
            End If
            If ds.Tables(1).Rows.Count > 0 Then
                With ds.Tables(1).Rows(0)
                    Select Case CommonCode.Fetch_Cookie_Shared("_lang")
                        Case "es-ES"
                            lbl_auc_description.Text = .Item("description_s")
                            lbl_auc_payment.Text = .Item("payment_terms_s")
                            lbl_auc_shipping.Text = .Item("shipping_terms_s")
                            lbl_auc_other.Text = .Item("other_terms_s")
                            lbl_auc_special.Text = .Item("special_conditions_s")
                            lbl_auc_tax.Text = .Item("tax_details_s")
                            lbl_auc_prodetail.Text = .Item("product_details_s").ToString().Trim()
                            lbl_auc_code.Text = .Item("code_s")
                            lbl_auc_title.Text = .Item("title_s")
                            lbl_auc_subtitle.Text = .Item("sub_title_s")
                            ltrl_short_desc.Text = .Item("short_description_s")
                        Case "fr-FR"
                            lbl_auc_description.Text = .Item("description_f")
                            lbl_auc_payment.Text = .Item("payment_terms_f")
                            lbl_auc_shipping.Text = .Item("shipping_terms_f")
                            lbl_auc_other.Text = .Item("other_terms_f")
                            lbl_auc_special.Text = .Item("special_conditions_f")
                            lbl_auc_tax.Text = .Item("tax_details_f")
                            lbl_auc_prodetail.Text = .Item("product_details_f").ToString().Trim()
                            lbl_auc_code.Text = .Item("code_f")
                            lbl_auc_title.Text = .Item("title_f")
                            lbl_auc_subtitle.Text = .Item("sub_title_f")
                            ltrl_short_desc.Text = .Item("short_description_f")
                        Case "zh-CN"
                            lbl_auc_description.Text = .Item("description_c")
                            lbl_auc_payment.Text = .Item("payment_terms_c")
                            lbl_auc_shipping.Text = .Item("shipping_terms_c")
                            lbl_auc_other.Text = .Item("other_terms_c")
                            lbl_auc_special.Text = .Item("special_conditions_c")
                            lbl_auc_tax.Text = .Item("tax_details_c")
                            lbl_auc_prodetail.Text = .Item("product_details_c").ToString().Trim()
                            lbl_auc_code.Text = .Item("code_c")
                            lbl_auc_title.Text = .Item("title_c")
                            lbl_auc_subtitle.Text = .Item("sub_title_c")
                            ltrl_short_desc.Text = .Item("short_description_c")
                        Case Else
                            lbl_auc_description.Text = .Item("description")
                            lbl_auc_payment.Text = .Item("payment_terms")
                            lbl_auc_shipping.Text = .Item("shipping_terms")
                            lbl_auc_other.Text = .Item("other_terms")
                            lbl_auc_special.Text = .Item("special_conditions")
                            lbl_auc_tax.Text = .Item("tax_details")
                            lbl_auc_prodetail.Text = .Item("product_details").ToString().Trim()
                            lbl_auc_code.Text = .Item("code")
                            lbl_auc_title.Text = .Item("title")
                            lbl_auc_subtitle.Text = .Item("sub_title")
                            ltrl_short_desc.Text = .Item("short_description")
                    End Select

                    If .Item("filename").ToString <> "" Then
                        If .Item("filename").ToString.Contains(".") Then
                            'Response.Write(SqlHelper.of_FetchKey("ServerHttp") & "/upload/auctions/stock_images/" & Request.QueryString("i") & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString.Remove(.Item("filename").ToString.LastIndexOf(".")) & "_thumb1" & .Item("filename").ToString.Remove(0, .Item("filename").ToString.LastIndexOf(".")))
                            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath("~/upload/auctions/stock_images/" & Request.QueryString("i") & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString.Remove(.Item("filename").ToString.LastIndexOf(".")) & "_thumb1" & .Item("filename").ToString.Remove(0, .Item("filename").ToString.LastIndexOf("."))))
                            If Not infoFile.Exists Then
                                auc_image.Visible = False
                            Else
                                auc_image.ImageUrl = SqlHelper.of_FetchKey("ServerHttp") & "/upload/auctions/stock_images/" & Request.QueryString("i") & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString.Remove(.Item("filename").ToString.LastIndexOf(".")) & "_thumb1" & .Item("filename").ToString.Remove(0, .Item("filename").ToString.LastIndexOf("."))
                            End If

                        End If
                        'auc_image.ImageUrl = "/upload/auctions/stock_images/" & Request.QueryString("i") & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString '.Remove(.Item("filename").ToString.LastIndexOf(".")) & "_thumb1" & .Item("filename").ToString.Remove(0, .Item("filename").ToString.LastIndexOf("."))
                        'auc_image.ImageUrl = .Item("filename").ToString
                    Else
                        auc_image.ImageUrl = SqlHelper.of_FetchKey("ServerHttp") & "/images/imagenotavailable.gif"
                    End If
                    lbl_auc_condition.Text = .Item("stock_condition")
                    lbl_auc_location.Text = .Item("stock_location")
                    lbl_auc_packaging.Text = .Item("package")
                    lbl_auc_company.Text = .Item("seller")
                    'If lbl_auc_payment.Text.Trim <> "" Then
                    '    lbl_auc_payment.Text = "<div style='padding-bottom:10px;'><b>Payment Terms: </b>" & lbl_auc_payment.Text & "</div>"
                    '    'If .Item("payment_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("payment_terms_attach").ToString.Trim.Remove(0, .Item("payment_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                    '    '    lbl_auc_payment.Text = lbl_auc_payment.Text
                    '    'End If
                    'Else
                    '    lbl_auc_payment.Visible = False
                    '    'If .Item("payment_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("payment_terms_attach").ToString.Trim.Remove(0, .Item("payment_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                    '    '    lbl_auc_payment.Text = "<div style='padding-bottom:10px;'><b>Payment Terms</b></div><a target='_blank' href='/Upload/Auctions/payment_term_attachments/" & hid_auc_id.Value & "/" & .Item("payment_terms_attach").ToString.Remove(.Item("payment_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("payment_terms_attach").ToString.Remove(0, .Item("payment_terms_attach").ToString.LastIndexOf("#") + 1) & "'>" & .Item("payment_terms_attach").ToString.Remove(0, .Item("payment_terms_attach").ToString.LastIndexOf("#") + 1) & "</a>"
                    '    'End If
                    'End If

                    'If lbl_auc_shipping.Text.Trim <> "" Then
                    '    lbl_auc_shipping.Text = "<div style='padding-bottom:10px;'><b>Shipping Terms: </b>" & lbl_auc_shipping.Text & "</div>"
                    '    'If .Item("shipping_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("shipping_terms_attach").ToString.Trim.Remove(0, .Item("shipping_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                    '    '    lbl_auc_shipping.Text = lbl_auc_shipping.Text & "<br><a target='_blank' href='/Upload/Auctions/shipping_term_attachments/" & hid_auc_id.Value & "/" & .Item("shipping_terms_attach").ToString.Remove(.Item("shipping_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("shipping_terms_attach").ToString.Remove(0, .Item("shipping_terms_attach").ToString.LastIndexOf("#") + 1) & "'>" & .Item("shipping_terms_attach").ToString.Remove(0, .Item("shipping_terms_attach").ToString.LastIndexOf("#") + 1) & "</a>"
                    '    'End If
                    'Else
                    '    lbl_auc_shipping.Visible = False
                    '    'If .Item("shipping_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("shipping_terms_attach").ToString.Trim.Remove(0, .Item("shipping_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                    '    '    lbl_auc_shipping.Text = "<div style='padding-bottom:10px;'><b>Shipping Terms</b></div><a target='_blank' href='/Upload/Auctions/shipping_term_attachments/" & hid_auc_id.Value & "/" & .Item("shipping_terms_attach").ToString.Remove(.Item("shipping_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("shipping_terms_attach").ToString.Remove(0, .Item("shipping_terms_attach").ToString.LastIndexOf("#") + 1) & "'>" & .Item("shipping_terms_attach").ToString.Remove(0, .Item("shipping_terms_attach").ToString.LastIndexOf("#") + 1) & "</a>"
                    '    'End If
                    'End If

                    'If lbl_auc_other.Text.Trim <> "" Then
                    '    lbl_auc_other.Text = "<div style='padding-bottom:10px;'><b>Other Terms: </b>" & lbl_auc_other.Text & "</div>"
                    '    'If .Item("other_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("other_terms_attach").ToString.Trim.Remove(0, .Item("other_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                    '    '    lbl_auc_other.Text = lbl_auc_other.Text & "<br><a target='_blank' href='/Upload/Auctions/terms_cond_attachments/" & hid_auc_id.Value & "/" & .Item("other_terms_attach").ToString.Remove(.Item("other_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("other_terms_attach").ToString.Remove(0, .Item("other_terms_attach").ToString.LastIndexOf("#") + 1) & "'>" & .Item("other_terms_attach").ToString.Remove(0, .Item("other_terms_attach").ToString.LastIndexOf("#") + 1) & "</a>"
                    '    'End If
                    'Else
                    '    lbl_auc_other.Visible = False
                    '    'If .Item("other_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("other_terms_attach").ToString.Trim.Remove(0, .Item("other_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                    '    '    lbl_auc_other.Text = "<div style='padding-bottom:10px;'><b>Other Terms</b></div><a target='_blank' href='/Upload/Auctions/terms_cond_attachments/" & hid_auc_id.Value & "/" & .Item("other_terms_attach").ToString.Remove(.Item("other_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("other_terms_attach").ToString.Remove(0, .Item("other_terms_attach").ToString.LastIndexOf("#") + 1) & "'>" & .Item("other_terms_attach").ToString.Remove(0, .Item("other_terms_attach").ToString.LastIndexOf("#") + 1) & "</a>"
                    '    'End If
                    'End If

                    'If lbl_auc_special.Text.Trim <> "" Then
                    '    lbl_auc_special.Text = "<div style='padding-bottom:10px;'><b>Special Terms: </b>" & lbl_auc_special.Text & "</div>"
                    '    'If .Item("special_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("special_terms_attach").ToString.Trim.Remove(0, .Item("special_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                    '    '    lbl_auc_special.Text = lbl_auc_special.Text & "<br><a target='_blank' href='/Upload/Auctions/special_conditions/" & hid_auc_id.Value & "/" & .Item("special_terms_attach").ToString.Remove(.Item("special_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("special_terms_attach").ToString.Remove(0, .Item("special_terms_attach").ToString.LastIndexOf("#") + 1) & "'>" & .Item("special_terms_attach").ToString.Remove(0, .Item("special_terms_attach").ToString.LastIndexOf("#") + 1) & "</a>"
                    '    'End If
                    'Else
                    '    lbl_auc_special.Visible = False
                    '    'If .Item("special_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("special_terms_attach").ToString.Trim.Remove(0, .Item("special_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                    '    '    lbl_auc_special.Text = "<div style='padding-bottom:10px;'><b>Special Terms</b></div><a target='_blank' href='/Upload/Auctions/special_conditions/" & hid_auc_id.Value & "/" & .Item("special_terms_attach").ToString.Remove(.Item("special_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("special_terms_attach").ToString.Remove(0, .Item("special_terms_attach").ToString.LastIndexOf("#") + 1) & "'>" & .Item("special_terms_attach").ToString.Remove(0, .Item("special_terms_attach").ToString.LastIndexOf("#") + 1) & "</a>"
                    '    'End If
                    'End If

                    'If lbl_auc_tax.Text.Trim <> "" Then
                    '    lbl_auc_tax.Text = "<div style='padding-bottom:10px;'><b>Tax Details: </b>" & lbl_auc_tax.Text & "</div>"
                    '    'If .Item("tax_details_attach").ToString.Trim.Length > 1 AndAlso (.Item("tax_details_attach").ToString.Trim.Remove(0, .Item("tax_details_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                    '    '    lbl_auc_tax.Text = lbl_auc_tax.Text & "<br><a target='_blank' href='/Upload/Auctions/tax_attachments/" & hid_auc_id.Value & "/" & .Item("tax_details_attach").ToString.Remove(.Item("tax_details_attach").ToString.LastIndexOf("#")) & "/" & .Item("tax_details_attach").ToString.Remove(0, .Item("tax_details_attach").ToString.LastIndexOf("#") + 1) & "'>" & .Item("tax_details_attach").ToString.Remove(0, .Item("tax_details_attach").ToString.LastIndexOf("#") + 1) & "</a>"
                    '    'End If
                    'Else
                    '    lbl_auc_tax.Visible = False
                    '    'If .Item("tax_details_attach").ToString.Trim.Length > 1 AndAlso (.Item("tax_details_attach").ToString.Trim.Remove(0, .Item("tax_details_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                    '    '    lbl_auc_tax.Text = "<div style='padding-bottom:10px;'><b>Tax Details</b></div><a target='_blank' href='/Upload/Auctions/tax_attachments/" & hid_auc_id.Value & "/" & .Item("tax_details_attach").ToString.Remove(.Item("tax_details_attach").ToString.LastIndexOf("#")) & "/" & .Item("tax_details_attach").ToString.Remove(0, .Item("tax_details_attach").ToString.LastIndexOf("#") + 1) & "'>" & .Item("tax_details_attach").ToString.Remove(0, .Item("tax_details_attach").ToString.LastIndexOf("#") + 1) & "</a>"
                    '    'End If
                    'End If

                    'If lbl_auc_prodetail.Text.Trim <> "" Or lbl_auc_description.Text.Trim <> "" Then
                    '    lbl_auc_prodetail.Text = "<div style='padding-bottom:10px;'><h3>Product Details</h3></div>" & lbl_auc_prodetail.Text
                    '    'If .Item("product_details_attach").ToString.Trim.Length > 1 AndAlso (.Item("product_details_attach").ToString.Trim.Remove(0, .Item("product_details_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                    '    '    lbl_auc_prodetail.Text = lbl_auc_prodetail.Text & "<br><a target='_blank' href='/Upload/Auctions/product_attachments/" & hid_auc_id.Value & "/" & .Item("product_details_attach").ToString.Remove(.Item("product_details_attach").ToString.LastIndexOf("#")) & "/" & .Item("product_details_attach").ToString.Remove(0, .Item("product_details_attach").ToString.LastIndexOf("#") + 1) & "'>" & .Item("product_details_attach").ToString.Remove(0, .Item("product_details_attach").ToString.LastIndexOf("#") + 1) & "</a>"
                    '    'End If
                    'Else
                    '    lbl_auc_prodetail.Visible = False
                    '    'If .Item("product_details_attach").ToString.Trim.Length > 1 AndAlso (.Item("product_details_attach").ToString.Trim.Remove(0, .Item("product_details_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                    '    '    lbl_auc_prodetail.Text = "<div style='padding-bottom:10px;'><b>Product Details</b></div><a target='_blank' href='/Upload/Auctions/product_attachments/" & hid_auc_id.Value & "/" & .Item("product_details_attach").ToString.Remove(.Item("product_details_attach").ToString.LastIndexOf("#")) & "/" & .Item("product_details_attach").ToString.Remove(0, .Item("product_details_attach").ToString.LastIndexOf("#") + 1) & "'>" & .Item("product_details_attach").ToString.Remove(0, .Item("product_details_attach").ToString.LastIndexOf("#") + 1) & "</a>"
                    '    'End If
                    'End If

                    'If lbl_auc_description.Text.Trim <> "" Then
                    '    lbl_auc_description.Text = "<div style='padding-bottom:10px;'><b>Description: </b>" & lbl_auc_description.Text & "</div>"
                    '    'If .Item("payment_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("payment_terms_attach").ToString.Trim.Remove(0, .Item("payment_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                    '    '    lbl_auc_payment.Text = lbl_auc_payment.Text
                    '    'End If
                    'Else
                    '    lbl_auc_description.Visible = False
                    '    'If .Item("payment_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("payment_terms_attach").ToString.Trim.Remove(0, .Item("payment_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                    '    '    lbl_auc_payment.Text = "<div style='padding-bottom:10px;'><b>Payment Terms</b></div><a target='_blank' href='/Upload/Auctions/payment_term_attachments/" & hid_auc_id.Value & "/" & .Item("payment_terms_attach").ToString.Remove(.Item("payment_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("payment_terms_attach").ToString.Remove(0, .Item("payment_terms_attach").ToString.LastIndexOf("#") + 1) & "'>" & .Item("payment_terms_attach").ToString.Remove(0, .Item("payment_terms_attach").ToString.LastIndexOf("#") + 1) & "</a>"
                    '    'End If
                    'End If

                    'Dim tbl_build As New StringBuilder
                    'tbl_build.Append("<h2 style='color:#000000'>Summary</h2><table cellspacing='2' cellpadding='3'><tr><td style='width: 100px;background-color:#F5F5F5;'>" & IIf(.Item("stock_condition").ToString.Trim <> "", "<b>Stock Condition:</b></td><td width='200px' style='background-color:#F5F5F5;'>" & .Item("stock_condition").ToString & "</td>", "</td>"))
                    'tbl_build.Append("<td style='width: 100px;background-color:#F5F5F5;'>" & IIf(.Item("stock_location").ToString.Trim <> "", "<b>Stock Location:</b></td><td width='200px' style='background-color:#F5F5F5;'>" & .Item("stock_location").ToString & "</td></tr>", "</td></tr>"))
                    'tbl_build.Append("<tr><td style='background-color:#F5F5F5;'>" & IIf(.Item("packaging").ToString.Trim <> "", "<b>Packaging:</b></td><td style='background-color:#F5F5F5;'>" & .Item("packaging").ToString & "</td>", "</td>"))
                    'tbl_build.Append("<td style='background-color:#F5F5F5;'>" & IIf(.Item("seller").ToString.Trim <> "", "<b>Company:</b></td><td style='background-color:#F5F5F5;'>" & .Item("seller").ToString & "</td></tr></table>", "</td></tr></table>"))
                    'lit_auc_summary.Text = tbl_build.ToString

                    ''lit_auc_summary.Visible = IIf(.Item("stock_condition").ToString.Trim <> "" Or .Item("stock_location").ToString.Trim <> "" Or .Item("packaging").ToString.Trim <> "" Or .Item("seller").ToString.Trim <> "", True, False)

                    ''TabStip1.Tabs(0).Visible = IIf(lbl_auc_afterlaunch.Text.Trim <> "" Or lit_auc_summary.Visible, True, False)
                    ''TabStip1.Tabs(1).Visible = IIf(Grid_Items.Items.Count > 0, True, False)
                    ''TabStip1.Tabs(2).Visible = IIf(lbl_auc_description.Text.Trim <> "" Or lbl_auc_prodetail.Text.Trim <> "", True, False)
                    ''TabStip1.Tabs(3).Visible = IIf(lbl_auc_payment.Text.Trim <> "" Or lbl_auc_shipping.Text.Trim <> "" Or lbl_auc_other.Text.Trim <> "" Or lbl_auc_special.Text.Trim <> "", True, False)

                End With
                Dim dt3 As DataTable = New DataTable()
                dt3 = SqlHelper.ExecuteDatatable("select top 1 payment_term_attachment_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_payment_term_attachments where auction_id=" & auction_id & " order by payment_term_attachment_id desc")
                If dt3.Rows.Count > 0 Then
                    With dt3.Rows(0)
                        Dim file_name As String = ""

                        Select Case CommonCode.Fetch_Cookie_Shared("_lang")
                            Case "es-ES"
                                file_name = .Item("filename_s")
                            Case "fr-FR"
                                file_name = .Item("filename_f")
                            Case "zh-CN"
                                file_name = .Item("filename_c")
                            Case Else
                                file_name = .Item("filename")
                        End Select
                        If file_name <> "" Then
                            lbl_attach_payment_terms.Text = "<a style='color:blue;text-decoration:none;' href='" & SqlHelper.of_FetchKey("ServerHttp") & "/Upload/Auctions/payment_term_attachments/" & auction_id & "/" & .Item("payment_term_attachment_id") & "/" & file_name & "' target='_blank'>" & file_name & "</a>"
                        Else
                            lbl_attach_payment_terms.Text = ""
                        End If

                    End With

                End If
                dt3 = Nothing

                Dim dt4 As DataTable = New DataTable()
                dt4 = SqlHelper.ExecuteDatatable("select top 1 shipping_term_attachment_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_shipping_term_attachments where auction_id=" & auction_id & " order by shipping_term_attachment_id desc")
                If dt4.Rows.Count > 0 Then
                    With dt4.Rows(0)
                        Dim file_name As String = ""
                        Select Case CommonCode.Fetch_Cookie_Shared("_lang")
                            Case "es-ES"
                                file_name = .Item("filename_s")
                            Case "fr-FR"
                                file_name = .Item("filename_f")
                            Case "zh-CN"
                                file_name = .Item("filename_c")
                            Case Else
                                file_name = .Item("filename")
                        End Select
                        'HID_shipping_term_attachment_id.Value = .Item("shipping_term_attachment_id")
                        If file_name <> "" Then
                            lbl_attach_shipping_terms.Text = "<a style='color:blue;text-decoration:none;' href='" & SqlHelper.of_FetchKey("ServerHttp") & "/Upload/Auctions/shipping_term_attachments/" & auction_id & "/" & .Item("shipping_term_attachment_id") & "/" & file_name & "' target='_blank'>" & file_name & "</a>"
                        Else
                            lbl_attach_shipping_terms.Text = ""
                        End If
                    End With
                End If
                dt4 = Nothing

                Dim dt5 As DataTable = New DataTable()
                dt5 = SqlHelper.ExecuteDatatable("select top 1 term_cond_attachment_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_terms_cond_attachments where auction_id=" & auction_id & " order by term_cond_attachment_id desc")
                If dt5.Rows.Count > 0 Then
                    With dt5.Rows(0)
                        Dim file_name As String = ""
                        Select Case CommonCode.Fetch_Cookie_Shared("_lang")
                            Case "es-ES"
                                file_name = .Item("filename_s")
                            Case "fr-FR"
                                file_name = .Item("filename_f")
                            Case "zh-CN"
                                file_name = .Item("filename_c")
                            Case Else
                                file_name = .Item("filename")
                        End Select

                        If file_name <> "" Then
                            lbl_attach_other_terms_and_conditions.Text = "<a style='color:blue;text-decoration:none;' href='" & SqlHelper.of_FetchKey("ServerHttp") & "/Upload/Auctions/terms_cond_attachments/" & auction_id & "/" & .Item("term_cond_attachment_id") & "/" & file_name & "' target='_blank'>" & file_name & "</a>"
                        Else
                            lbl_attach_other_terms_and_conditions.Text = ""
                        End If
                    End With

                End If
                dt5 = Nothing

                Dim dt6 As DataTable = New DataTable()
                dt6 = SqlHelper.ExecuteDatatable("select top 1 product_attachment_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_product_attachments where auction_id=" & auction_id & " order by product_attachment_id desc")
                If dt6.Rows.Count > 0 Then
                    With dt6.Rows(0)
                        Dim file_name As String = ""
                        Select Case CommonCode.Fetch_Cookie_Shared("_lang")
                            Case "es-ES"
                                file_name = .Item("filename_s")
                            Case "fr-FR"
                                file_name = .Item("filename_f")
                            Case "zh-CN"
                                file_name = .Item("filename_c")
                            Case Else
                                file_name = .Item("filename")
                        End Select

                        If file_name <> "" Then
                            lbl_attach_product_details.Text = "<a style='color:blue;text-decoration:none;' href='" & SqlHelper.of_FetchKey("ServerHttp") & "/Upload/Auctions/product_attachments/" & auction_id & "/" & .Item("product_attachment_id") & "/" & file_name & "' target='_blank'>" & file_name & "</a>"
                        Else
                            lbl_attach_product_details.Text = ""
                        End If
                    End With

                End If
                dt6 = Nothing

                Dim dt7 As DataTable = New DataTable()
                dt7 = SqlHelper.ExecuteDatatable("select top 1 tax_attachment_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_tax_attachments where auction_id=" & auction_id & " order by tax_attachment_id desc")
                If dt7.Rows.Count > 0 Then
                    With dt7.Rows(0)
                        Dim file_name As String = ""
                        Select Case CommonCode.Fetch_Cookie_Shared("_lang")
                            Case "es-ES"
                                file_name = .Item("filename_s")
                            Case "fr-FR"
                                file_name = .Item("filename_f")
                            Case "zh-CN"
                                file_name = .Item("filename_c")
                            Case Else
                                file_name = .Item("filename")
                        End Select

                        If file_name <> "" Then
                            lbl_attach_tax_details.Text = "<a style='color:blue;text-decoration:none;' href='" & SqlHelper.of_FetchKey("ServerHttp") & "/Upload/Auctions/tax_attachments/" & auction_id & "/" & .Item("tax_attachment_id") & "/" & file_name & "' target='_blank'>" & file_name & "</a>"
                        Else
                            lbl_attach_tax_details.Text = ""
                        End If
                    End With

                End If
                dt7 = Nothing

                Dim dt8 As DataTable = New DataTable()
                dt8 = SqlHelper.ExecuteDatatable("select top 1 special_condition_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_special_conditions where auction_id=" & auction_id & " order by special_condition_id desc")
                If dt8.Rows.Count > 0 Then
                    With dt8.Rows(0)
                        Dim file_name As String = ""
                        Select Case CommonCode.Fetch_Cookie_Shared("_lang")
                            Case "es-ES"
                                file_name = .Item("filename_s")
                            Case "fr-FR"
                                file_name = .Item("filename_f")
                            Case "zh-CN"
                                file_name = .Item("filename_c")
                            Case Else
                                file_name = .Item("filename")
                        End Select

                        If file_name <> "" Then
                            lbl_attach_special_conditions.Text = "<a style='color:blue;text-decoration:none;' href=""" & SqlHelper.of_FetchKey("ServerHttp") & "/Upload/Auctions/special_conditions/" & auction_id & "/" & .Item("special_condition_id") & "/" & file_name & """>" & file_name & "</a>"
                        Else
                            lbl_attach_special_conditions.Text = ""
                        End If
                    End With
                End If
            Else
                Response.Write("Requested Auction details is not available.")
            End If
            ds = Nothing
        End If
    End Sub

    Protected Sub but_to_pdf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles but_to_pdf.Click
        'Dim bfTimes As BaseFont = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, True)
        'Dim times As New Font(bfTimes, 10, Font.ITALIC)
        'Response.ContentType = "application/pdf"
        'Response.AddHeader("content-disposition", "attachment;filename=TestPage.pdf")
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Dim sw As New StringWriter()
        'Dim hw As New HtmlTextWriter(sw)
        'Me.Page.RenderControl(hw)
        'Dim sr As New StringReader(sw.ToString())
        'Dim pdfDoc As New Document(PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        'Dim htmlparser As New HTMLWorker(pdfDoc)
        'PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        'pdfDoc.Open()
        'htmlparser.Parse(sr)
        'pdfDoc.Close()
        'Response.Write(pdfDoc)
        'Response.Flush()
        'Set content type in response stream
        ' ''Response.ContentType = "application/pdf"
        ' ''Dim attachment As String = "attachment; filename=" & lbl_auc_title.Text & ".pdf"
        '' ''Response.ClearContent()
        ' ''Response.AddHeader("content-disposition", attachment)
        ' ''Dim stw As New StringWriter()
        ' ''Dim str As New StringReader(stw.ToString())
        ' ''Dim htextw As New HtmlTextWriter(stw)
        ' ''htextw.AddStyleAttribute("font-size", "7pt")
        ' ''htextw.AddStyleAttribute("color", "Black")
        ' ''Me.Page.RenderControl(htextw)
        '' ''Panel_Name.RenderControl(htextw)
        '' ''Name of the Panel
        ' ''Dim document As New Document()
        ' ''document = New Document()
        '' ''FontFactory.GetFont("Arial", 50, iTextSharp.text.BaseColor.BLUE)
        ' ''PdfWriter.GetInstance(document, Response.OutputStream)

        ' ''document.Open()


        ' ''Dim htmlworker As New HTMLWorker(document)
        ' ''htmlworker.Parse(str)

        ' ''document.Close()
        ' ''Response.Write(document)
        Dim regexSearch As String = New String(Path.GetInvalidFileNameChars()) & New String(Path.GetInvalidPathChars())
        'Response.Output.Write(regexSearch)
        'Exit Sub
        Dim r As New Regex(String.Format("[{0}]", Regex.Escape(regexSearch)))
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=" & lbl_auc_title.Text.Trim.Replace(" ", "_") & ".pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        hw.AddStyleAttribute("font-size", "10px")
        Me.Page.RenderControl(hw)
        Dim sr As New StringReader(sw.ToString())
        Dim sb As New StringBuilder
        sb.Append(sw)
        Dim htmlText As String = sb.ToString
        'For Each c As String In
        '    htmlText = htmlText & c
        'Next

        'htmlText = r.Replace(htmlText, "")
        'htmlText = htmlText.Replace("<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">", "")
        'htmlText = htmlText.Replace("<%@ Page Language=""VB"" AutoEventWireup=""false"" CodeFile=""test.aspx.vb"" Inherits=""test"" EnableEventValidation=""false"" %>", "")
        'htmlText = htmlText.Replace("?", "")
        'htmlText = htmlText.Replace("*", "")
        'htmlText = htmlText.Replace("|", "")
        'htmlText = htmlText.Replace("""", "")
        htmlText = htmlText.Replace(Environment.NewLine, "")
        'htmlText = htmlText.Replace("<", "&lt;")
        Dim pdfDoc As New Document(PageSize.A4, 10.0F, 10.0F, 5.0F, 0.0F)

        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        Dim st As New html.simpleparser.StyleSheet()

        'Dim link As Font = FontFactory.GetFont("Arial", 7, Font.NORMAL)
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.End()

    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered

    End Sub

End Class
