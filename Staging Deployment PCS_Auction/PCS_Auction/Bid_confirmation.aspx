﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="Bid_confirmation.aspx.vb"
    Inherits="Bid_confirmation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Bidding Confirmation</title>
    <link type="text/css" rel="Stylesheet" href="/Style/AuctionBody.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding: 10px 15px 0px 20px;">
        <div class="pageHeading">
            <asp:Literal ID="lit_heading" runat="server" Text="THANKS"></asp:Literal>
        </div>
        <div style="border: 1px solid #10AAEA; background: url('/Images/fend/grdnt_template.jpg') repeat-x;
            height: 137px">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td style="padding-top: 25px;" align="center">
                        <b>
                            <asp:Literal ID="lbl_thank_you" runat="server" Text="Thank you for taking interest in us."></asp:Literal></b>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 15px;" align="center">
                        <asp:Literal ID="lbl_message" runat="server"></asp:Literal>
                        <asp:HiddenField ID="hid_bid_type" runat="server" />
                        <asp:HiddenField ID="hid_bid_id" runat="server" Value="0" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
