﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CurrentStatus.ascx.vb"
    Inherits="Auctions_UserControls_CurrentStatus" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Charting" Assembly="Telerik.Web.UI" %>
<script type="text/javascript" language="javascript">
        var _currentstatus;
    function open_bidder_history(bidder_id) {

        window.open("/Auctions/Buyer_Bidding_history.aspx?i=" + bidder_id, "_BidderAllQuery", "left=" + ((screen.width - 1000) / 2) + ",top=" + ((screen.height - 800) / 2) + ",width=1000,height=800,scrollbars=yes,toolbars=no,resizable=yes");
        window.focus();
        return false;
    }
    function open_pop_order_confirmation(_path, _id, au_mode) {
        w = window.open(_path + '?b=' + _id + '&t=' + au_mode, '_ord_Confirm', 'top=' + ((screen.height - 700) / 2) + ', left=' + ((screen.width - 890) / 2) + ', height=700, width=890,scrollbars=yes,toolbars=no,resizable=1;');
        w.focus();
        return false;
    }
    function refresh_currentstatus(_option) {
       // alert('currentstatus');
        document.getElementById('<%=but_bind_current_status.ClientID %>').disabled = false;
        document.getElementById('<%=but_bind_current_status.ClientID %>').click();
        if (_option > 0) {
            _currentstatus = setTimeout('refresh_afterlaunch_currentstatus()', 30000);
        }
        else if (_currentstatus) {
           // alert(_refreshme);
            clearTimeout(_currentstatus);
            tab_select_name()
        }

    }
</script>
<asp:HiddenField ID="HID_auction_type_id" runat="server" Value="0" />
<asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
<asp:HiddenField ID="hid_user_id" runat="server" Value="0" />
    <asp:Button ID="but_bind_current_status" runat="server" BorderStyle="None" BackColor="Transparent"
        ForeColor="Transparent" Width="0" Height="0" />
<asp:Panel ID="pnl_current_bidders" runat="server">
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
        <tr>
            <td colspan="2">
                <div class="statusSectionTitle">
                    <div style="float: left;">
                        Top Bidders 
                    </div>
                    <div style="float: left; padding-left: 20px; font-size: 10px;">
                        <asp:Literal ID="lbl_auction_time_display1" runat="server"></asp:Literal>
                    </div>
                    <asp:HiddenField ID="hid_auction_status" runat="server" Value="0" />
                    <div class="statusTimeBack">
                        <asp:Literal ID="lbl_auction_status_caption" runat="server"></asp:Literal>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="statusSecBack" style="width: 55%;">
                <iframe id="iframe_current_bidder" width="100%" height="300" scrolling="auto" runat="server"
                    frameborder="0"></iframe>
            </td>
            <td align="right" valign="top" style="padding: 20px 10px 10px 10px;">
                <%--<img src="/Images/barchart.jpg" alt="chart" />--%>
                <telerik:RadChart ID="RadChart1" Skin="Office2007" runat="server" ChartTitle-Visible="false"
                    AutoLayout="true">
                    <PlotArea Appearance-Border-Width="0">
                        <XAxis MaxValue="5" MinValue="1" Step="1" Appearance-MinorGridLines-Visible="false">
                        </XAxis>
                        <YAxis AxisMode="Extended" Appearance-MinorGridLines-Visible="false">
                        </YAxis>
                    </PlotArea>
                    <Legend Visible="true" Appearance-Border-Width="0"></Legend>
                </telerik:RadChart>
            </td>
        </tr>
    </table>
    <br />
    <br />
</asp:Panel>
<asp:Panel ID="pnl_proxy_auction" runat="server" Visible="false">
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
        <tr>
            <td>
                <div class="statusSectionTitle">
                    <div style="float: left;">
                        <asp:Literal ID="lbl_proxy_auction_status_label" runat="server"></asp:Literal>
                    </div>
                    <div style="float: left; padding-left: 20px; font-size: 10px;">
                        <asp:Literal ID="lbl_auction_time_display2" runat="server"></asp:Literal>
                    </div>
                    <div class="statusTimeBack">
                        <asp:Literal ID="lbl_proxy_auction_status_caption" runat="server"></asp:Literal>
                    </div>
                </div>
            </td>
            <td align="right" class="statusSectionTitle">
                <asp:ImageButton ID="img_btn_sendemail" runat="server" Visible="false" ImageUrl="/images/emailstowinnersandloosers.png" />
                <asp:Label ID="lbl_msg_mail" runat="server" ForeColor="Red" Visible="false" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="statusSecBack" style="width: 70%;" valign="top">
                <asp:Label ID="lbl_msg_bid_over_status_update" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="lbl_no_trad_proxy_winner" runat="server"></asp:Label>
                <div id="tr_trad_proxy_winners" runat="server">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <table cellspacing="10" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td align="left" style="background-color: #FFFFFF;">
                                            <div class="winnerArrow">
                                                <asp:HiddenField ID="hid_winner_bid_id" runat="server" Value="0" />
                                                <asp:HiddenField ID="hid_winner_buyer_id" runat="server" Value="0" />
                                            </div>
                                            <div class="winnerPrice">
                                                <asp:Literal ID="lbl_winner_bid_amount" runat="server"></asp:Literal>
                                                <br /><br />
                                                <asp:ImageButton runat="server" ID="lnk_winner_send_payment" Visible="false" ImageUrl="/images/send_payment.png" />
                                            </div>
                                            <div class="winnerCompany">
                                                <div class="winnerCompanyTitle" >
                                                    <a onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%= hid_winner_buyer_id.Value %>','200','white','true');">
                                                        <asp:Literal ID="lbl_winner_company" runat="server" ></asp:Literal></a>
                                                </div>
                                                <div class="winnerCompanyDetails">
                                                    <asp:Literal ID="ltrl_winner_address" runat="server"></asp:Literal><br />
                                                    <asp:Literal ID="lbl_winner_mobile" runat="server"></asp:Literal><br />
                                                    <asp:Literal ID="Ltl_fedex_option" runat="server"></asp:Literal>
                                                </div>
                                                <div style="clear: both;padding-top:5px;">
                                                    <asp:DropDownList ID="ddl_winner_closed" runat="server" AutoPostBack="true" Visible="false">
                                                        <asp:ListItem Value="">Pending Decision</asp:ListItem>
                                                        <asp:ListItem Value="Awarded">Awarded</asp:ListItem>
                                                        <asp:ListItem Value="Denied">Denied</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div style="clear: both;padding-bottom:0px;padding-top:5px;">
                                                    <asp:Label ID="lbl_print_receipt" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            
                                                    
                                        </td>
                                        <td valign="top" align="left" style="background-color: #FFFFFF; width: 150px; padding: 5px;">
                                            <div class="wonLot">
                                                Other won lots (top 5)
                                            </div>
                                            <div style="height: 80px;">
                                                <asp:Repeater ID="rpt_winner_won_lots" runat="server">
                                                    <ItemTemplate>
                                                        <div class="lotDetals">
                                                            <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');">
                                                                <%# Eval("code")%></a> :
                                                            <%# "$" & FormatNumber(Eval("bid_amount"), 2)%>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:Literal ID="lbl_no_winner_other_winning_history" runat="server"></asp:Literal>
                                            </div>
                                            <asp:Literal ID="ltrl_winner_view_all" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="td_first_runner" runat="server" visible="false">
                            <td>
                                <table cellspacing="10" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td align="left" style="background-color: #FFFFFF;">
                                            <div class="looserArrow">
                                                <asp:HiddenField ID="hid_first_runner_up_bid_id" runat="server" Value="0" />
                                                <asp:HiddenField ID="hid_first_runner_up_buyer_id" runat="server" Value="0" />
                                            </div>
                                            <div class="looserPrice">
                                                <asp:Literal ID="lbl_first_runner_bid_amount" runat="server"></asp:Literal>
                                            </div>
                                            <div class="winnerCompany">
                                                <div class="winnerCompanyTitle">
                                                    <a onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%= hid_first_runner_up_buyer_id.Value %>','200','white','true');">
                                                        <asp:Literal ID="lbl_first_runner_company" runat="server"></asp:Literal>
                                                    </a>
                                                </div>
                                                <div class="winnerCompanyDetails">
                                                    <asp:Literal ID="ltrl_first_runner_address" runat="server"></asp:Literal><br />
                                                    <asp:Literal ID="lbl_first_runner_mobile" runat="server"></asp:Literal><br />
                                                    <asp:Literal ID="ltrl_fedex_option_first_runner" runat="server"></asp:Literal>
                                                </div>
                                                <div style="clear: both;padding-top:5px;">
                                                    <asp:DropDownList ID="ddl_first_runner_closed" runat="server" AutoPostBack="true"
                                                        Visible="false">
                                                        <asp:ListItem Value="">Pending Decision</asp:ListItem>
                                                        <asp:ListItem Value="Awarded">Awarded</asp:ListItem>
                                                        <asp:ListItem Value="Denied">Denied</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div style="padding-left:305px;clear:both;padding-top:5px;">
                                                <asp:ImageButton runat="server" ID="lnk_first_send_payment" Visible="false" ImageUrl="/images/send_payment.png" />
                                            </div>
                                        </td>
                                        <td valign="top" align="left" style="background-color: #FFFFFF; width: 150px; padding: 5px;">
                                            <div class="wonLot">
                                                Other won lots (top 5)
                                            </div>
                                            <div style="height: 80px;">
                                                <asp:Repeater ID="rpt_first_runner_up_won_lots" runat="server">
                                                    <ItemTemplate>
                                                        <div class="lotDetals">
                                                            <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');">
                                                                <%# Eval("code")%></a> :
                                                            <%# "$" & FormatNumber(Eval("bid_amount"), 2)%>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:Literal ID="lbl_no_first_runner_up_other_winning_history" runat="server"></asp:Literal>
                                            </div>
                                            <asp:Literal ID="ltrl_first_runner_up_view_all" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="td_second_runner" runat="server" visible="false">
                            <td>
                                <table cellspacing="10" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td align="left" style="background-color: #FFFFFF;">
                                            <div class="looserMostArrow">
                                                <asp:HiddenField ID="hid_second_runner_up_bid_id" runat="server" Value="0" />
                                                <asp:HiddenField ID="hid_second_runner_up_buyer_id" runat="server" Value="0" />
                                            </div>
                                            <div class="looserPrice">
                                                <asp:Literal ID="lbl_second_runner_bid_amount" runat="server"></asp:Literal>
                                            </div>
                                            <div class="winnerCompany">
                                                <div class="winnerCompanyTitle">
                                                    <a onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%= hid_second_runner_up_buyer_id.Value %>','200','white','true');">
                                                        <asp:Literal ID="lbl_second_runner_company" runat="server"></asp:Literal></a>
                                                </div>
                                                <div class="winnerCompanyDetails">
                                                    <asp:Literal ID="ltrl_second_runner_address" runat="server"></asp:Literal><br />
                                                    <asp:Literal ID="lbl_second_runner_mobile" runat="server"></asp:Literal><br />
                                                    <asp:Literal ID="ltrl_fedex_option_second_runner" runat="server"></asp:Literal>
                                                </div>
                                                <div style="clear: both;padding-top:5px;">
                                                    <asp:DropDownList ID="ddl_second_runner_closed" runat="server" AutoPostBack="true"
                                                        Visible="false">
                                                        <asp:ListItem Value="">Pending Decision</asp:ListItem>
                                                        <asp:ListItem Value="Awarded">Awarded</asp:ListItem>
                                                        <asp:ListItem Value="Denied">Denied</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div style="padding-left:305px;clear:both;padding-top:5px;">
                                                <asp:ImageButton runat="server" ID="lnk_second_send_payment" Visible="false" ImageUrl="/images/send_payment.png" />
                                                </div>
                                        </td>
                                        <td valign="top" align="left" style="background-color: #FFFFFF; width: 150px; padding: 5px;">
                                            <div class="wonLot">
                                                Other won lots (top 5)
                                            </div>
                                            <div style="height: 80px;">
                                                <asp:Repeater ID="rpt_second_runner_up_won_lots" runat="server">
                                                    <ItemTemplate>
                                                        <div class="lotDetals">
                                                            <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');">
                                                                <%# Eval("code")%></a> :
                                                            <%# "$" & FormatNumber(Eval("bid_amount"), 2)%>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:Literal ID="lbl_no_second_runner_up_other_winning_history" runat="server"></asp:Literal>
                                            </div>
                                            <asp:Literal ID="ltrl_second_runner_up_view_all" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td class="statusSecBack" align="left" valign="top" style="width: 30%; padding-top: 25px;">
                <asp:Panel ID="pnl_other_runners_up" runat="server">
                    <div class="ORUP">
                        Other RUNNER Ups
                    </div>
                    <div style="padding-right: 2px;">
                        <telerik:RadAjaxPanel ID="radPanelOtherBidders" runat="server"><telerik:RadGrid ID="RadGrid_other_bidders" DataSourceID="RadGrid_BidHistory_SqlDataSource"
                                runat="server" Skin="Vista" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True"
                                PageSize="3" GridLines="None" Width="100%"><PagerStyle Mode="NextPrevAndNumeric" /><HeaderStyle BackColor="#BEBEBE" /><ItemStyle Font-Size="11px" /><MasterTableView DataSourceID="RadGrid_BidHistory_SqlDataSource" AllowMultiColumnSorting="false"
                                    GroupLoadMode="Server" ItemStyle-Height="40" AlternatingItemStyle-Height="40"><NoRecordsTemplate>No bidder to display</NoRecordsTemplate><Columns><telerik:GridTemplateColumn UniqueName="bidder" SortExpression="bidder" HeaderButtonType="TextButton"
                                            DataField="bidder" HeaderText="Bidder"><ItemTemplate><a onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');"></a></ItemTemplate></telerik:GridTemplateColumn><telerik:GridTemplateColumn UniqueName="bid_amount" SortExpression="bid_amount" HeaderButtonType="TextButton"
                                            DataField="bid_amount" HeaderText="Amount"><ItemTemplate></ItemTemplate></telerik:GridTemplateColumn></Columns></MasterTableView><ClientSettings AllowDragToGroup="false" /></telerik:RadGrid><asp:SqlDataSource ID="radgrid_bidhistory_sqldatasource" ConnectionString="<%$ connectionstrings:tconnectionstring %>"
                                ProviderName="system.data.sqlclient" SelectCommand="select A.buyer_id,A.bid_id,ISNULL(action,'') AS action,A.bid_amount,A.bid_type,
                                    ISNULL(C.company_name,'')AS bidder,ISNULL(C.email,'') As email
                                    from tbl_auction_bids A WITH (NOLOCK) INNER JOIN tbl_reg_buyers C WITH (NOLOCK) ON A.buyer_id=C.buyer_id
                                    LEFT JOIN tbl_master_states S ON C.state_id=S.state_id 
                                    where A.bid_id in (select bid_id from dbo.fn_get_auction_buyer_bid_ids(@auction_id))
                                    and A.bid_id not in
                                    (
	                                    select top 3 A.bid_id from tbl_auction_bids A WITH (NOLOCK) 
	                                    INNER JOIN tbl_reg_buyers C WITH (NOLOCK) ON A.buyer_id=C.buyer_id
	                                    LEFT JOIN tbl_master_states S ON C.state_id=S.state_id 
	                                    where A.bid_id in (select bid_id from dbo.fn_get_auction_buyer_bid_ids(@auction_id))
	                                    order by A.bid_amount DESC
                                    )  order by A.bid_amount desc"
                                runat="server"><SelectParameters><asp:QueryStringParameter Name="auction_id" QueryStringField="i" /></SelectParameters></asp:SqlDataSource></telerik:RadAjaxPanel>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <br />
    <br />
</asp:Panel>
<asp:Panel ID="pnl_partial_offer" runat="server" Visible="false">
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
        <tr>
            <td>
                <div class="statusSectionTitle">
                    <div style="float: left;">
                        <asp:Literal ID="lbl_buy_now_auction_status_label" runat="server"></asp:Literal>
                    </div>
                    <div style="float: left; padding-left: 20px; font-size: 10px;">
                        <asp:Literal ID="lbl_auction_time_display3" runat="server"></asp:Literal>
                    </div>
                    <div style="float: left; padding-left: 25px; font-size: 11px;color:red; padding-top: 3px;">
                        <asp:Label ID="lbl_email_sent_msg" runat="server" EnableViewState="false"></asp:Label>
                    </div>
                    <div class="statusTimeBack">
                        <asp:Literal ID="lbl_buy_now_auction_status_caption" runat="server"></asp:Literal>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="statusSecBack" valign="top" align="left">
                <asp:Label ID="lbl_no_buy_now_partial_offers" runat="server"></asp:Label>
                <asp:DataList ID="dl_buy_partial_offers" runat="server" RepeatColumns="4" RepeatDirection="Horizontal"
                    DataKeyField="buy_id" CellSpacing="10" ItemStyle-BorderColor="#000000" ItemStyle-Width="230"
                    ItemStyle-BorderWidth="1">
                    <ItemTemplate>
                        <div>
                            <div class="buyNowArea">
                                <div class="wonLot">
                                    <%# Eval("buy_type")%>
                                </div>
                                <div class="buyPrice" style="padding-bottom:3px;">
                                    <%# "$" & FormatNumber(Eval("grand_total_amount"), 2)%>
                                </div>
                                <div class="buyTime">
                                    <%# "$" & FormatNumber(Eval("price"), 2) & " * " & Eval("quantity")%>
                                </div>
                                <div class="buyQty">
                                    <%# IIf(Eval("buy_type") = "Partial Offer", "Qty : " & Eval("quantity"), "")%>
                                </div>
                                <div class="buyTime">
                                    <%# "Posted: " & Convert.ToDateTime(Eval("bid_date")).ToString("MM/dd/yyyy hh:mm tt") %>
                                </div>
                                <div class="buyShipping" style="width: 210px;">
                                    <%# Eval("shipping_value")%>
                                </div>
                                <div class="winnerCompany">
                                    <div class="winnerCompanyTitle">
                                        <a onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');">
                                            <%# Eval("company_name")%></a>
                                    </div>
                                    <div class="winnerCompanyDetails">
                                        <%# Eval("address1") & IIf(Eval("address2") = "", "", "<br>" & Eval("address2"))%><br />
                                        <%# Eval("city") & ", " & Eval("state_code") & " " & Eval("zip")%><br />
                                        <%# "(P) " & Eval("mobile")%>
                                    </div>
                                </div>
                            </div>
                            <div style="float: left; width: 250px; height: 165px; background-color: #BBB9B9; overflow: hidden;">
                                <div class="buyNowAreaAccept">
                                    <div class="buyNowAreaActionLabel">
                                        Offer Accepted by us?
                                    </div>
                                    <div style="height: 34px;">
                                        <asp:DropDownList ID="ddl_bid_status" runat="server" AutoPostBack="true" OnSelectedIndexChanged="update_bid_buy_status"
                                            Width="150">
                                            <asp:ListItem Value="Pending">Pending Decision</asp:ListItem>
                                            <asp:ListItem Value="Accept">Accepted</asp:ListItem>
                                            <asp:ListItem Value="Reject">Rejected</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Panel ID="pnl_label_closed_action" runat="server" Visible="false">
                                        <div class="buyNowAreaActionLabel">
                                            Updating in our Backend?
                                        </div>
                                    </asp:Panel>
                                    <div style="height: 26px;">
                                        <asp:DropDownList ID="ddl_closed" runat="server" Visible="false" AutoPostBack="true"
                                            OnSelectedIndexChanged="update_bid_buy_close_status" Width="150" Enabled="false">
                                            <asp:ListItem Value="Pending">Pending Decision</asp:ListItem>
                                            <asp:ListItem Value="Awarded">Awarded</asp:ListItem>
                                            <asp:ListItem Value="Denied">Denied</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div style="height: 20px;">
                                        <a href="#" style="text-decoration: none;" onclick="javascript:return  open_pop_order_confirmation('/PrintConfirmation.aspx','<%# Eval("buy_id")%>','b')">Print Order Confirmation</a>
                                    </div>
                                    <asp:ImageButton runat="server" ID="lnk_send_payment" Visible="false" ImageUrl='<%#iif(Cbool(Eval("is_payment_link")),"/images/resend_payment.png","/images/send_payment.png") %>' OnClick="lnk_send_payment_Click" CommandArgument='<%# Eval("buy_id")%>' />
                                </div>
                            </div>
                            <%--<div class="buyNowArea" style="height:30px;">
                                <div style="padding: 10px 0px 5px 0px; clear: both;">
                                    <a href='#' onclick='javascript:return open_bidder_history(<%# Eval("buyer_id") %>)'>
                                        <img src="/images/bidder-history.gif" alt="View History" style="border: none;" /></a>
                                </div>
                            </div>--%>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnl_quotation" runat="server" Visible="false">
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
        <tr>
            <td>
                <div class="statusSectionTitle">
                    <div style="float: left;">
                        Case of Request for Offer
                    </div>
                    <div style="float: left; padding-left: 20px; font-size: 10px;">
                        <asp:Literal ID="lbl_auction_time_display4" runat="server"></asp:Literal>
                    </div>
                    <div class="statusTimeBack">
                        <asp:Literal ID="lbl_quote_auction_status_caption" runat="server"></asp:Literal>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="statusSecBack" valign="top" align="left">
                <asp:Label ID="lbl_no_quotation" runat="server"></asp:Label>
                <asp:DataList ID="dl_quotation" runat="server" RepeatColumns="4" CellSpacing="10"
                    RepeatDirection="Horizontal" DataKeyField="quotation_id">
                    <ItemTemplate>
                        <div class="quoteNowArea">
                            <div class="winnerCompany">
                                <div class="winnerCompanyTitle">
                                    <a onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');">
                                        <%# Eval("company_name")%></a>
                                </div>
                                <div class="winnerCompanyDetails">
                                    <%# Eval("address1") & IIf(Eval("address2") = "", "", "<br>" & Eval("address2"))%><br />
                                    <%# Eval("city") & ", " & Eval("state_code") & " " & Eval("zip")%><br />
                                    <%# "(P) " & Eval("mobile")%>
                                </div>
                                <div style="height: 15px;">
                                    <b>Message:</b>
                                </div>
                                <div style="background-color: #d4d3d3; width: 200px; height: 100px; overflow-x: hidden; overflow-y: auto; padding: 2px;">
                                    <%# Eval("details") %>
                                </div>
                                <div style="padding: 10px 0px 5px 0px; clear: both;">
                                    <a href='<%# IIF(Eval("filename")<>"", "/Upload/Quotation/" & hid_auction_id.Value & "/" & Eval("filename"),"") %>'
                                        target="_blank" style="color: Black;">
                                        <%# Eval("filename")%></a>
                                </div>
                                <div class="buyTime">
                                    <%# "Posted: " & Convert.ToDateTime(Eval("bid_date")).ToString("MM/dd/yyyy hh:mm tt") %>
                                </div>
                                <div style="padding: 5px 0px 0px 0px; width: 250px; height: 30px;">
                                    <asp:DropDownList ID="ddl_closed" runat="server" AutoPostBack="true" Visible="false"
                                        OnSelectedIndexChanged="update_quotation_status">
                                        <asp:ListItem Value="Pending">Pending Decision</asp:ListItem>
                                        <asp:ListItem Value="Awarded">Awarded</asp:ListItem>
                                        <asp:ListItem Value="Backed Off">Denied</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <%-- <div style="padding: 5px 0px 0px 0px; clear: both;">
                                    <a href='#' onclick='javascript:return open_bidder_history(<%# Eval("buyer_id") %>)'>
                                        <img src="/images/bidder-history.gif" alt="View History" style="border: none;" /></a>
                                </div>--%>
                            </div>
                        </div>
                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
                            <tr>
                                <td class="details" style="background-color: White; padding-left: 30px;"></td>
                            </tr>
                            <tr>
                                <td class="details" style="background-color: White; padding-left: 30px;"></td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>
</asp:Panel>
