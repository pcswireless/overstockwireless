﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="auction_fedex.ascx.vb"
    Inherits="Auctions_UserControls_auction_fedex" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<telerik:RadAjaxPanel ID="pnl_fedex1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
        <tr>
            <td class="tdTabItem">
                <div class="pnlTabItemHeader">
                    <div class="tabItemHeader" style="background: url(/Images/fedex_icon.jpg) no-repeat;
                        background-position: 10px 0px;">
                        FedEx
                    </div>
                    <div style="float: right; width: 450px; font-weight: normal">
                        <asp:Label ID="lbl_fedex_error" runat="server" CssClass="error" EnableViewState="false"></asp:Label></div>
                </div>
                <div style="padding: 10px;">
                    <asp:HiddenField ID="hid_user_id" runat="server" Value="0" />
                    <asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
                    <asp:HiddenField ID="hid_package_type" runat="server" Value="" />
                    <table cellpadding="0" cellspacing="2" border="0" width="838">
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lbl_msg" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                Shipping Amount
                            </td>
                            <td class="details">
                                <asp:Label ID="lbl_shipping_amount" runat="server" />
                                <telerik:RadNumericTextBox ID="txt_shipping_amount" CssClass="inputtype" runat="server"
                                    Type="Currency" Width="80">
                                </telerik:RadNumericTextBox>&nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                &nbsp;
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td class="caption">
                                Use PCS Shipping
                            </td>
                            <td class="details">
                                <asp:CheckBox ID="chk_pcs_shipping" runat="server" Checked="true" />
                                <asp:Label ID="lbl_pcs_shipping" runat="server" />&nbsp;
                            </td>
                            <td class="caption">
                                Use Bidder Shipping
                            </td>
                            <td class="details">
                                <asp:CheckBox ID="chk_your_shipping" runat="server" />
                                <asp:Label ID="lbl_your_shipping" runat="server" />&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                Auction Weight<span id="span_auction_weight" runat="server" class="req_star"> *</span><br />
                                <font size="1">in lbs</font>
                            </td>
                            <td class="details">
                                <asp:TextBox CssClass="inputtype" ID="txt_auction_weight" runat="server" MaxLength="8"
                                    Width="50" />
                                <asp:Label ID="lbl_auction_weight" runat="server" />
                                <asp:RegularExpressionValidator ID="RegularExpression_txt_auction_weight" ControlToValidate="txt_auction_weight"
                                    CssClass="error" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*" ErrorMessage="<br>Invalid Auction Weight"
                                    runat="server" ValidationGroup="fedex_main" />
                                <asp:RequiredFieldValidator ID="rfv_auction_weight" runat="server" ControlToValidate="txt_auction_weight"
                                    ValidationGroup="fedex_main" Display="Dynamic" ErrorMessage="<br>Auction Weight Required"
                                    CssClass="error"></asp:RequiredFieldValidator>
                            </td>
                            <td class="caption">
                                Package Type
                            </td>
                            <td class="details">
                                <telerik:RadComboBox ID="ddl_package_type" runat="server" Height="200px" Width="200px"
                                    EmptyMessage="--Package Type--" AllowCustomText="true">
                                    <Items>
                                        <telerik:RadComboBoxItem Value="FEDEX BOX" Text="FEDEX BOX" />
                                        <telerik:RadComboBoxItem Value="FEDEX ENVELOPE" Text="FEDEX ENVELOPE" />
                                        <telerik:RadComboBoxItem Value="FEDEX PAK" Text="FEDEX PAK" />
                                        <telerik:RadComboBoxItem Value="FEDEX 10KG BOX" Text="FEDEX 10KG BOX" />
                                        <telerik:RadComboBoxItem Value="FEDEX 25KG BOX" Text="FEDEX 25KG BOX" />
                                        <telerik:RadComboBoxItem Value="FEDEX TUBE" Text="FEDEX TUBE" />
                                        <telerik:RadComboBoxItem Value="YOUR PACKAGING" Text="YOUR PACKAGING" />
                                    </Items>
                                </telerik:RadComboBox>
                                <asp:Label ID="lbl_package_type" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                Auction Weight<br />
                                <font size="1">in Kgs</font>
                            </td>
                            <td class="details">
                                <asp:Label ID="lbl_kgs" runat="server" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="details">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="dv_md_sub_heading" style="padding-left: 0px;" colspan="4">
                                FedEx Packages Details
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="padding-top: 5px;">
                                <telerik:RadGrid ID="rad_grid_fedex" runat="server" Skin="Vista" GridLines="None"
                                    AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                    OnNeedDataSource="rad_grid_fedex_NeedDataSource" OnDeleteCommand="rad_grid_fedex_DeleteCommand"
                                    OnInsertCommand="rad_grid_fedex_InsertCommand" OnUpdateCommand="rad_grid_fedex_UpdateCommand"
                                    enableajax="true" ClientSettings-Selecting-AllowRowSelect="false" PageSize="10"
                                    ShowGroupPanel="false">
                                    <PagerStyle Mode="NextPrevAndNumeric" />
                                    <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="fedex_pagkage_id"
                                        ItemStyle-Height="40" AlternatingItemStyle-Height="40" HorizontalAlign="NotSet"
                                        AutoGenerateColumns="False" EditMode="EditForms" ClientDataKeyNames="package_type">
                                        <CommandItemStyle BackColor="#E1DDDD" />
                                        <NoRecordsTemplate>
                                            Fedex package not available
                                        </NoRecordsTemplate>
                                        <CommandItemSettings AddNewRecordText="Add New Package" />
                                        <Columns>
                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                EditImageUrl="/Images/edit_grid.gif">
                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                            </telerik:GridEditCommandColumn>
                                            <telerik:GridTemplateColumn HeaderText="Package" UniqueName="package">
                                                <ItemTemplate>
                                                    <%# Container.ItemIndex + 1%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Package Type" UniqueName="package_type" DataField="package_type">
                                                <ItemTemplate>
                                                    <%# Eval("package_type")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Package Weight" UniqueName="package_weight"
                                                DataField="package_weight" EditFormHeaderTextFormat="Package Weight">
                                                <ItemTemplate>
                                                    <%# Eval("package_weight")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Insured Amount" UniqueName="insured_amount"
                                                DataField="insured_amount" EditFormHeaderTextFormat="Insured Amount">
                                                <ItemTemplate>
                                                    <%# Eval("insured_amount")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridButtonColumn ConfirmText="Delete this Package?" ConfirmDialogType="RadWindow"
                                                ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                            </telerik:GridButtonColumn>
                                        </Columns>
                                        <EditFormSettings InsertCaption="New Pagkage" EditFormType="Template">
                                            <FormTemplate>
                                                <table id="tbl_grd_fedex_edit" cellpadding="2" cellspacing="2" border="0" width="800px">
                                                    <tr>
                                                        <td style="font-weight: bold; padding-top: 10px;" colspan="2">
                                                            Pagkage Details
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption" style="width: 150px;">
                                                            Package Weight (in lbs)&nbsp;<span class="req_star">*</span>
                                                        </td>
                                                        <td style="width: 260px;" class="details">
                                                            <asp:TextBox ID="txt_grd_weight" runat="server" Width="65" MaxLength="10" Text='<%# Bind("package_weight") %>'
                                                                CssClass="inputtype" />
                                                            <asp:RequiredFieldValidator ID="Req_txt_grd_weight" runat="server" Font-Size="10px"
                                                                ValidationGroup="grd_fedex_items" ControlToValidate="txt_grd_weight" Display="Dynamic"
                                                                ErrorMessage="<br>Package Weight Required"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="Reg_txt_grd_weight" runat="server" Font-Size="10px"
                                                                ControlToValidate="txt_grd_weight" ValidationExpression="^\d{1,8}(?:\.\d{1,4})?$"
                                                                ValidationGroup="grd_fedex_items" Display="Dynamic" ErrorMessage="<br>Wrong Package Weight"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption" style="width: 150px;">
                                                            Insured Amount
                                                        </td>
                                                        <td style="width: 260px;" class="details">
                                                            $&nbsp;<asp:TextBox ID="txt_grd_insured" runat="server" MaxLength="10" Width="65"
                                                                Text='<%# Bind("insured_amount") %>' CssClass="inputtype" />
                                                            <asp:RegularExpressionValidator ID="Reg_txt_grd_insured" runat="server" Font-Size="10px"
                                                                ControlToValidate="txt_grd_insured" ValidationExpression="\d+(\.\d{1,2})?" ValidationGroup="grd_fedex_items"
                                                                Display="Dynamic" ErrorMessage="<br>Wrong Insured Amount"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_grd_dimension" runat="server">
                                                        <td class="caption" style="width: 150px;">
                                                            Dimensions (in inch)
                                                        </td>
                                                        <td style="width: 260px;" class="details">
                                                            L&nbsp;<asp:TextBox ID="txt_grd_length" MaxLength="4" runat="server" Text='<%# Bind("package_length") %>'
                                                                Width="30" CssClass="inputtype" />&nbsp;
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Font-Size="10px"
                                                                ControlToValidate="txt_grd_length" ValidationExpression="\d+(\.\d{1,2})?" ValidationGroup="grd_fedex_items"
                                                                Display="Dynamic" ErrorMessage="*"></asp:RegularExpressionValidator>&nbsp; W&nbsp;<asp:TextBox
                                                                    ID="txt_grd_width" MaxLength="4" runat="server" Text='<%# Bind("package_width") %>'
                                                                    CssClass="inputtype" Width="30" />&nbsp;
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Font-Size="10px"
                                                                ControlToValidate="txt_grd_width" ValidationExpression="\d+(\.\d{1,2})?" ValidationGroup="grd_fedex_items"
                                                                Display="Dynamic" ErrorMessage="*"></asp:RegularExpressionValidator>&nbsp; H&nbsp;<asp:TextBox
                                                                    ID="txt_grd_height" MaxLength="4" runat="server" Text='<%# Bind("package_height") %>'
                                                                    CssClass="inputtype" Width="30" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Font-Size="10px"
                                                                ControlToValidate="txt_grd_height" ValidationExpression="\d+(\.\d{1,2})?" ValidationGroup="grd_fedex_items"
                                                                Display="Dynamic" ErrorMessage="*"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Literal ID="lbl_grid_error" EnableViewState="false" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:ImageButton ID="but_grd_fedexsubmit" CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem),"PerformInsert","Update") %>'
                                                                ValidationGroup="grd_fedex_items" OnClientClick="return ValidationGroupEnable('grd_fedex_items', true);"
                                                                AlternateText='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "Save" , "Update") %>'
                                                                runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif" , "/images/update.gif") %>' />
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <asp:ImageButton ID="but_grd_fedexcancel" CausesValidation="false" CommandName="Cancel"
                                                                runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </FormTemplate>
                                            <%--       <FormTableItemStyle Wrap="False"></FormTableItemStyle>
            <FormCaptionStyle CssClass="EditFormHeader" Font-Bold="true"></FormCaptionStyle>
            <FormMainTableStyle GridLines="None" CellSpacing="0" CellPadding="3" BackColor="White"
                Width="100%" />
            <FormTableStyle CellSpacing="0" CellPadding="2" Height="110px" BackColor="White" />
            <FormTableAlternatingItemStyle Wrap="False"></FormTableAlternatingItemStyle>
            <EditColumn ButtonType="ImageButton" InsertText="Insert Order" UpdateText="Update record"
                UniqueName="EditCommandColumn1" CancelText="Cancel edit" CancelImageUrl="/images/cancel.gif"
                InsertImageUrl="/images/save.gif" UpdateImageUrl="/images/update.gif">
            </EditColumn>
            <FormTableButtonRowStyle HorizontalAlign="Right" CssClass="EditFormButtonRow"></FormTableButtonRowStyle>--%>
                                        </EditFormSettings>
                                    </MasterTableView>
                                    <ClientSettings AllowDragToGroup="false">
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <GroupingSettings ShowUnGroupButton="false" />
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:Button ID="btn_refresh_fedex" runat="server" Style="display: none;" Text="Refresh" />
                <%--</telerik:RadAjaxPanel>--%>
            </td>
            <td class="fixedcolumn" valign="top">
                <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                <br />
                <%--   <telerik:RadAjaxPanel ID="pnl_fedex11" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">--%>
                <asp:Panel ID="pnl_fedex_buttons" runat="server">
                    <div class="addButton">
                        <asp:ImageButton ID="btnEditFedex" runat="server" CausesValidation="false" AlternateText="Save"
                            ImageUrl="/images/edit.gif" />
                        <asp:ImageButton ID="btnUpdateFedex" runat="server" AlternateText="Update" ImageUrl="/images/update.gif"
                            ValidationGroup="fedex_main" />
                    </div>
                    <div class="cancelButton" id="divCancelFedex" runat="server">
                        <asp:ImageButton ID="btnCancelFedex" runat="server" AlternateText="Cancel" CausesValidation="false"
                            ImageUrl="/images/cancel.gif" />
                    </div>
                    <div style="padding: 10px 0px 5px 15px;" id="btn_auction_fedex" runat="server">
                        <a href="javascript:void(0);" onclick="openFedEx()">
                            <img src="/images/fedex.png" alt="" style="border: none;" /></a>
                        <br />
                        <br />
                        <asp:Label ID="lbl_count_remaining" ForeColor="Red" runat="server"></asp:Label>
                    </div>
                    <div style="padding: 10px 0px 5px 15px;" id="btn_fedex_report" runat="server">
                        <a href="javascript:void(0);" onclick="openFedExReport()">
                            <img src="/images/fedex_report.png" alt="" style="border: none;" /></a>
                        <br />
                        <br />
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>
<telerik:RadScriptBlock ID="RadScriptBlock_Package" runat="server">
    <script type="text/javascript">

        function RowClicked(sender, args) {
            var cellValues = args.getDataKeyValue("package_type");
            //alert(cellValues);


        }
        function RowShown(sender, args) {
            var cellValues = args.getDataKeyValue("package_type");
            //alert(cellValues);

        }
        function on_key_press() {

            var amt;

            if (document.getElementById('<%=txt_auction_weight.ClientID%>').value == '')
                amt = 0;
            else
                amt = document.getElementById('<%=txt_auction_weight.ClientID%>').value;

            var profits = (parseFloat(amt) * 10) / 22;
            document.getElementById('<%=lbl_kgs.ClientID%>').innerHTML = profits.toFixed(2);

        }

        function refresh_fedex() {
            document.getElementById('<%=btn_refresh_fedex.ClientID%>').click();
            return true;
        }
        function openFedEx() {
            w = window.open('/Auctions/UpdateFedExRate_New.aspx?i=<%=Request("i") %>', "_UpdateFedExRate", "left=" + ((screen.width - 400) / 2) + ",top=" + ((screen.height - 300) / 2) + ",width=450,height=300,scrollbars=yes,toolbars=no,resizable=yes");
            w.focus();
            return false;
        }
        function openFedExReport() {
            w = window.open('/Auctions/Auction_Fedex_Report.aspx?i=<%=Request("i") %>', "_FedExReport", "left=" + ((screen.width - 750) / 2) + ",top=" + ((screen.height - 600) / 2) + ",width=750,height=600,scrollbars=yes,toolbars=no,resizable=yes");
            w.focus();
            return false;
        }
        //        function validate_package(source, args) {
        //            args.IsValid = false;
        //            var combo = $find(document.getElementById('hid_package_type').value);
        //            var text = combo.get_text();
        //            if (text.length < 1) {
        //                args.IsValid = false;
        //            }
        //            else {
        //                var node = combo.findItemByText(text);
        //                if (node) {
        //                    var value = node.get_value();
        //                    if (value.length > 0) {
        //                        args.IsValid = true;
        //                    }
        //                }
        //                else {
        //                    args.IsValid = false;
        //                }
        //            }
        //        }
    </script>
</telerik:RadScriptBlock>
<telerik:RadScriptBlock ID="RadScriptBlock_Dimension" runat="server">
    <script type="text/javascript">
        function grd_package_dimension(sender, args) {
            var item = args.get_item();
            var dim;
            if (item.get_text() == 'YOUR PACKAGING')
                document.getElementById(dim).style.display = '';
            else
                document.getElementById(dim).style.display = 'none';
        }
    </script>
</telerik:RadScriptBlock>
<asp:SqlDataSource ID="SqlDataSource_Fedex" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT ROW_NUMBER() 
        OVER (ORDER BY fedex_pagkage_id) AS package_no, 
    fedex_pagkage_id, weight
FROM tbl_auction_fedex_packages where auction_id= @auction_id" DeleteCommand="DELETE FROM [tbl_auction_fedex_packages] WHERE [fedex_pagkage_id] = @fedex_pagkage_id"
    InsertCommand="INSERT INTO tbl_auction_fedex_packages (auction_id, weight,row_create_date,row_create_user_id) 
    VALUES (@auction_id,@weight,getdate(),@row_create_user_id)" UpdateCommand="UPDATE tbl_auction_fedex_packages SET weight=@weight       
     WHERE fedex_pagkage_id = @fedex_pagkage_id">
    <SelectParameters>
        <asp:ControlParameter Name="auction_id" Type="Int32" ControlID="hid_auction_id" PropertyName="Value" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="fedex_pagkage_id" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="fedex_pagkage_id" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Name="weight" Type="Double" />
        <asp:ControlParameter Name="auction_id" Type="Int32" ControlID="hid_auction_id" PropertyName="Value" />
        <asp:ControlParameter Name="row_create_user_id" Type="Int32" ControlID="hid_user_id"
            PropertyName="Value" />
    </InsertParameters>
</asp:SqlDataSource>
<script type="text/javascript">
    function load_package_dimension(_id) {
        alert(document.getElementById(_id).value);
        if (document.getElementById(_id).value == 'YOUR PACKAGING')
            document.getElementById('tr_grd_dimension').style.display = '';
        else
            document.getElementById('tr_grd_dimension').style.display = 'none';
    }
</script>
