﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LeftbarBucket.ascx.vb"
    Inherits="Auctions_UserControls_LeftbarBucket" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<telerik:RadAjaxPanel ID="RadAjaxPanel7" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
    CssClass="TabGrid">
    <table cellpadding="0" cellspacing="0" border="0" width="950">
        <tr>
            <td style="width: 450px;" class="invSubHeading">
                Available Options
            </td>
            <td style="width: 50px;">
                &nbsp;
            </td>
            <td style="width: 450px;" class="invSubHeading">
                Selected Options
            </td>
        </tr>
        <tr>
            <td style="padding: 5px; border: 1px solid #E4E4E5;" valign="top">
                <telerik:RadGrid ID="RadGrid_SelectedBucketlist_left" GridLines="None" runat="server"
                    AllowPaging="False" AutoGenerateColumns="False" AllowMultiRowSelection="false"
                    Skin="Vista" AllowFilteringByColumn="false" ShowHeader="false" ShowFooter="false"
                    DataSourceID="SqlDataSource4">
                    <MasterTableView DataKeyNames="bucket_filter_value_id" HorizontalAlign="NotSet" AutoGenerateColumns="False">
                        <NoRecordsTemplate>
                            Available Bucket has been selected
                        </NoRecordsTemplate>
                        <Columns>
                            <telerik:GridTemplateColumn>
                                <ItemTemplate>
                                    <asp:Literal ID="lit_bucket_filter_id" runat="server" Text='<%# Eval("bucket_filter_id") %>'
                                        Visible="false"></asp:Literal>
                                    <asp:Literal ID="lit_bucket_filter_value_id" runat="server" Text='<%# Eval("bucket_filter_value_id") %>'
                                        Visible="false"></asp:Literal>
                                    <asp:Literal ID="lit_bucket" runat="server" Text='<%# Eval("bucket_name") %>'></asp:Literal>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk" runat="server" Text='<%#Eval("bucket_value") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="select B.caption as bucket_name,V.bucket_filter_id,V.bucket_filter_value_id,V.caption as bucket_value from tbl_bucket_filter B inner join tbl_bucket_filter_value V on B.bucket_filter_id=V.bucket_filter_id where B.is_active=1 and V.is_active=1 and V.bucket_filter_value_id not in (select isnull(bucket_filter_value_id,0) from tbl_auction_bucket_filter where auction_id=@auction_id) order by B.caption,V.caption">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="auction_id" QueryStringField="i" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td valign="middle" align="center" style="padding: 5px;">
                <asp:ImageButton ID="btn_add_filter" runat="server" AlternateText="Save" ImageUrl="/images/add_filter.gif" />
            </td>
            <td style="padding: 5px; border: 1px solid #E4E4E5;" valign="top">
                <telerik:RadGrid ID="RadGrid_SelectedBucketlist" GridLines="None" runat="server"
                    AllowAutomaticDeletes="True" AllowPaging="False" AutoGenerateColumns="False"
                    AllowMultiRowSelection="false" DataSourceID="SqlDataSource2" Skin="Vista" AllowFilteringByColumn="false"
                    ShowHeader="false" ShowFooter="false">
                    <MasterTableView DataKeyNames="aucton_bucket_mapping_id" HorizontalAlign="NotSet"
                        AutoGenerateColumns="False" DataSourceID="SqlDataSource2">
                        <NoRecordsTemplate>
                            <div style="height: 19px; padding-top: 3px; padding-left: 5px;">
                                Bucket not selected</div>
                        </NoRecordsTemplate>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Bucket" UniqueName="bucket_name">
                                <ItemTemplate>
                                    <asp:Literal ID="lit_bucket_filter_id" runat="server" Text='<%# Eval("bucket_filter_id") %>'
                                        Visible="false"></asp:Literal>
                                    <asp:Literal ID="lit_bucket" runat="server" Text='<%# Eval("bucket_name") %>'></asp:Literal>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="bucket_value" HeaderText="Option" UniqueName="bucket_value">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn ConfirmText="Delete this bucket?" ConfirmDialogType="RadWindow"
                                ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="select F.aucton_bucket_mapping_id,B.caption as bucket_name,V.bucket_filter_id,V.bucket_filter_value_id,V.caption as bucket_value from tbl_bucket_filter B inner join tbl_bucket_filter_value V on B.bucket_filter_id=V.bucket_filter_id inner join tbl_auction_bucket_filter F on V.bucket_filter_value_id=F.bucket_filter_value_id and F.auction_id=@auction_id order by B.caption,V.caption"
                    DeleteCommand="DELETE FROM [tbl_auction_bucket_filter] WHERE [aucton_bucket_mapping_id] = @aucton_bucket_mapping_id">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="auction_id" QueryStringField="i" />
                    </SelectParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="aucton_bucket_mapping_id" Type="Int32" />
                    </DeleteParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>
