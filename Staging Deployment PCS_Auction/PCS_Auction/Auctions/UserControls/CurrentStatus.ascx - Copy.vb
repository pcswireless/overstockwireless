﻿Imports Telerik.Web.UI
Imports Telerik.Charting

Partial Class Auctions_UserControls_CurrentStatus
    Inherits System.Web.UI.UserControl
    Private show_partial_offer As Boolean = False

    Private Sub setPermission()
        If Not CommonCode.is_super_admin() Then
            Dim Vew_Auction As Boolean = False
            Dim Edit_Auction As Boolean = False
            Dim New_Auction As Boolean = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))
            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i).Item("permission_id")
                    Case 1
                        New_Auction = True
                    Case 2
                        Vew_Auction = True
                    Case 3
                        Edit_Auction = True
                End Select
            Next
            dt = Nothing
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                If Vew_Auction = False Then Response.Redirect("/NoPermission.aspx")
                If Not Edit_Auction Then
                    ddl_winner_closed.Enabled = False
                    ddl_first_runner_closed.Enabled = False
                    ddl_second_runner_closed.Enabled = False
                    For j As Integer = 0 To dl_buy_partial_offers.Items.Count - 1
                        CType(dl_buy_partial_offers.Items(j).FindControl("ddl_bid_status"), DropDownList).Enabled = False
                    Next
                    For j As Integer = 0 To dl_quotation.Items.Count - 1
                        CType(dl_buy_partial_offers.Items(j).FindControl("ddl_closed"), DropDownList).Enabled = False
                    Next

                End If
            Else
                If New_Auction = False Then Response.Redirect("/NoPermission.aspx")
            End If
        End If
    End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If IsNumeric(Request.QueryString.Get("i")) Then
                hid_auction_id.Value = Request.QueryString.Get("i")
                hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
                Dim is_mail_send As Boolean = False
                Dim is_accept_pre_bid As Boolean = False

                Dim qry As String = "SELECT A.auction_id, " & _
              "ISNULL(A.code, '') AS code," & _              "ISNULL(A.title, '') AS title," & _              "ISNULL(A.sub_title, '') AS sub_title," & _              "ISNULL(A.product_catetory_id, 0) AS product_catetory_id," & _              "ISNULL(A.stock_condition_id, 0) AS stock_condition_id," & _              "ISNULL(A.packaging_id, 0) AS packaging_id," & _              "ISNULL(A.seller_id, 0) AS seller_id," & _              "ISNULL(A.stock_location_id, '') AS stock_location_id," & _              "ISNULL(B.name, '') AS stock_location," & _              "ISNULL(A.is_active, 0) AS is_active," & _              "ISNULL(A.is_private_offer, 0) AS is_private_offer," & _              "ISNULL(A.is_partial_offer, 0) AS is_partial_offer," & _              "ISNULL(A.is_buy_it_now, 0) AS is_buy_it_now," & _                "ISNULL(A.is_accept_pre_bid, 0) AS is_accept_pre_bid," & _              "ISNULL(A.start_date, '1/1/1999') as start_date," & _              "ISNULL(A.end_date, '1/1/1999') as end_date," & _
               "ISNULL(A.is_email_sent, 0) as is_email_sent," & _
               "ISNULL(A.display_end_time, '1/1/1999') as display_end_time," & _
              "ISNULL(A.auction_type_id, 0) AS auction_type_id" & _               " FROM tbl_auctions A WITH (NOLOCK) LEFT JOIN tbl_master_stock_locations B ON A.stock_location_id=B.stock_location_id WHERE A.auction_id = " & hid_auction_id.Value
                Dim startDate As DateTime = Now
                Dim endDate As DateTime = Now()

                Dim dt As DataTable = New DataTable()
                dt = SqlHelper.ExecuteDataTable(qry)
                If dt.Rows.Count > 0 Then
                    With dt.Rows(0)
                        is_mail_send = .Item("is_email_sent")
                        is_accept_pre_bid = .Item("is_accept_pre_bid")
                        lbl_auction_time_display1.Text = "Start Date : " & String.Format("{0:M/d/yyyy hh:mm tt}", .Item("start_date")) & "<br>End Date : " & String.Format("{0:M/d/yyyy hh:mm tt}", .Item("display_end_time"))
                        lbl_auction_time_display2.Text = "Start Date : " & String.Format("{0:M/d/yyyy hh:mm tt}", .Item("start_date")) & "<br>End Date : " & String.Format("{0:M/d/yyyy hh:mm tt}", .Item("display_end_time"))
                        lbl_auction_time_display3.Text = "Start Date : " & String.Format("{0:M/d/yyyy hh:mm tt}", .Item("start_date")) & "<br>End Date : " & String.Format("{0:M/d/yyyy hh:mm tt}", .Item("display_end_time"))
                        lbl_auction_time_display4.Text = "Start Date : " & String.Format("{0:M/d/yyyy hh:mm tt}", .Item("start_date")) & "<br>End Date : " & String.Format("{0:M/d/yyyy hh:mm tt}", .Item("display_end_time"))
                        startDate = .Item("start_date")
                        endDate = .Item("display_end_time")
                        HID_auction_type_id.Value = .Item("auction_type_id")
                        Dim strCasePartial As String = ""
                        If Convert.ToBoolean(.Item("is_buy_it_now")) Then
                            strCasePartial = strCasePartial & "Buy Now"
                            show_partial_offer = True
                        End If
                        If Convert.ToBoolean(.Item("is_private_offer")) Then
                            If strCasePartial <> "" Then
                                strCasePartial = strCasePartial & " / Private"
                            Else
                                strCasePartial = "Private"
                            End If
                            show_partial_offer = True
                        End If
                        If Convert.ToBoolean(.Item("is_partial_offer")) Then
                            If strCasePartial <> "" Then
                                strCasePartial = strCasePartial & " / Partial"
                            Else
                                strCasePartial = "Partial"
                            End If
                            show_partial_offer = True
                        End If
                        If strCasePartial <> "" Then
                            strCasePartial = "Case of " & strCasePartial & " Offers"
                        End If
                        lbl_buy_now_auction_status_label.Text = strCasePartial
                    End With
                End If

                Dim aucStatus As Integer = Convert.ToInt32(SqlHelper.ExecuteScalar("select dbo.get_auction_status(" & Request.QueryString.Get("i") & ")"))

                Select Case aucStatus

                    Case 1
                        hid_auction_status.Value = "C"

                        lbl_auction_status_caption.Text = "Auction currently running"
                        lbl_proxy_auction_status_caption.Text = "Auction Running "
                        lbl_buy_now_auction_status_caption.Text = "Auction Running "
                        lbl_quote_auction_status_caption.Text = "Auction Running "
                    Case 2
                        hid_auction_status.Value = "U"

                        lbl_auction_status_caption.Text = "Upcoming auction"
                        lbl_proxy_auction_status_caption.Text = "Upcoming auction"
                        lbl_quote_auction_status_caption.Text = "Upcoming auction"
                        lbl_buy_now_auction_status_caption.Text = "Upcoming auction"
                    Case 3
                        hid_auction_status.Value = "P"

                        lbl_auction_status_caption.Text = "Auction over"
                        lbl_proxy_auction_status_caption.Text = "Auction over"
                        lbl_quote_auction_status_caption.Text = "Auction over"
                        lbl_buy_now_auction_status_caption.Text = "Auction over"
                    Case Else

                End Select

                Dim no_of_bidder As Integer = SqlHelper.ExecuteScalar("select count(bid_id) from tbl_auction_bids WITH (NOLOCK) where auction_id=" & hid_auction_id.Value)

                If no_of_bidder > 0 And (HID_auction_type_id.Value = "2" Or HID_auction_type_id.Value = "3") And (hid_auction_status.Value = "P" Or hid_auction_status.Value = "C" Or (hid_auction_status.Value = "U" And is_accept_pre_bid)) Then
                    setChart(HID_auction_type_id.Value)
                    iframe_current_bidder.Attributes.Add("src", "/Auctions/Current_Bidders.aspx?i=" & Request("i"))
                Else
                    pnl_current_bidders.Visible = False
                End If
                If HID_auction_type_id.Value = 1 Then
                    show_partial_offer = True
                End If

                If HID_auction_type_id.Value = "2" Or HID_auction_type_id.Value = "3" Then
                    pnl_proxy_auction.Visible = True
                    If HID_auction_type_id.Value = "3" And is_mail_send = False And aucStatus = 3 And no_of_bidder > 0 Then
                        img_btn_sendemail.Visible = True
                    End If
                    bind_proxy_bidders()
                End If
                If HID_auction_type_id.Value <> "4" Then
                    bind_buy_partial_bidding()
                    If show_partial_offer Then
                        pnl_partial_offer.Visible = True
                    End If

                Else
                    bind_quotation()
                    pnl_quotation.Visible = True
                End If
            End If
        End If
    End Sub

#Region "Current Status"

    Private Sub setChart(ByVal auc_type As Integer)
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDataTable("select A.bid_id,A.buyer_id,B.company_name,A.max_bid_amount,A.bid_amount from dbo.fn_get_current_status_chart_data(" & Request.QueryString.Get("i") & ") A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id")
        If dt.Rows.Count > 0 Then
            Dim chart_max As Integer = 0
            Dim chart_step As Integer = 0

            Dim max_bid_amount As Double = 0
            If auc_type = 2 Then
                max_bid_amount = dt.Rows(0).IsNull("max_bid_amount")
            Else
                max_bid_amount = dt.Rows(0).IsNull("bid_amount")
            End If


            If max_bid_amount > 1000 Then
                chart_max = (Convert.ToInt32(max_bid_amount / 1000) + 1) * 1000
                chart_step = 1000
            ElseIf max_bid_amount > 100 Then
                chart_max = (Convert.ToInt32(max_bid_amount / 100) + 1) * 100
                chart_step = 100
            ElseIf max_bid_amount > 10 Then
                chart_max = (Convert.ToInt32(max_bid_amount / 10) + 1) * 10
                If max_bid_amount > 50 Then
                    chart_step = 10
                Else
                    chart_step = 5
                End If
            Else
                chart_max = 10
                chart_step = 2
            End If

            RadChart1.PlotArea.YAxis.MaxValue = chart_max
            RadChart1.PlotArea.YAxis.Step = chart_step
            RadChart1.PlotArea.XAxis.AxisLabel.TextBlock.Text = "Rank -->"
            RadChart1.PlotArea.XAxis.AxisLabel.Visible = True
            RadChart1.PlotArea.YAxis.AxisLabel.TextBlock.Text = "Amount in $"
            RadChart1.PlotArea.YAxis.AxisLabel.Visible = True

            Dim ChrtSeries As New ChartSeries
            ChrtSeries.Name = "Bid"
            ChrtSeries.Appearance.FillStyle.MainColor = System.Drawing.Color.FromArgb(192, 80, 78)

            Dim ChrtSeries1 As New ChartSeries
            ChrtSeries1.Name = "Won"
            ChrtSeries1.Appearance.FillStyle.MainColor = System.Drawing.Color.FromArgb(79, 129, 190)

            Dim ChrtSeriesItem As ChartSeriesItem
            Dim ChrtSeriesItem1 As ChartSeriesItem
            Dim i As Integer
            For i = 0 To dt.Rows.Count - 1
                ChrtSeriesItem = New ChartSeriesItem
                ChrtSeriesItem.ActiveRegion.Tooltip = dt.Rows(i).Item("company_name")
                ChrtSeriesItem.YValue = dt.Rows(i).Item("max_bid_amount")
                ChrtSeriesItem.Label.Visible = False
                ChrtSeries.Items.Add(ChrtSeriesItem)

                ChrtSeriesItem1 = New ChartSeriesItem
                ChrtSeriesItem1.ActiveRegion.Tooltip = dt.Rows(i).Item("company_name")
                ChrtSeriesItem1.YValue = dt.Rows(i).Item("bid_amount")
                ChrtSeriesItem1.Label.Visible = False
                ChrtSeries1.Items.Add(ChrtSeriesItem1)

            Next
            dt = Nothing
            If auc_type = 3 Then RadChart1.Series.Add(ChrtSeries)
            RadChart1.Series.Add(ChrtSeries1)

        End If

    End Sub
    Private Sub bind_proxy_bidders()
        Dim auction_id As Integer = Request.QueryString.Get("i")
        Dim str As String = ""
        Dim dt As DataTable = New DataTable()
        str = "select A.buyer_id," & _
         "A.bid_id," & _
         "ISNULL(action,'') AS action," & _
         "A.bid_amount," & _
         "ISNULL(C.company_name,'')AS company_name," & _
         "ISNULL(C.shipping_type,'')AS shipping_type," & _
          "ISNULL(C.shipping_account_number,'')AS shipping_account_number," & _
           "ISNULL(C.shipping_preference,'')AS shipping_preference," & _
            "ISNULL(C.last_shipping_option,'')AS last_shipping_option," & _
          "ISNULL(A.shipping_value,'') AS shipping_value, " & _
         "ISNULL(C.address1,'') As address1," & _
         "ISNULL(C.address2,'') As address2," & _
         "ISNULL(C.city,'') AS city," & _
         "ISNULL(S.name,'') AS state," & _
         "ISNULL(S.code,'') AS state_code," & _
         "ISNULL(C.zip,'') AS zip," & _
         "ISNULL(C.mobile,'') AS mobile " & _
         "from tbl_auction_bids A WITH (NOLOCK) " & _
         "INNER JOIN tbl_reg_buyers C ON A.buyer_id=C.buyer_id " & _
         "LEFT JOIN tbl_master_states S ON C.state_id=S.state_id " & _
        "where A.bid_id in (select bid_id from dbo.fn_get_auction_buyer_bid_ids(" & hid_auction_id.Value & ")) order by A.bid_amount DESC"

        dt = SqlHelper.ExecuteDataTable(str)
        If HID_auction_type_id.Value = "2" Then
            lbl_proxy_auction_status_label.Text = "Case of Traditional Auction"
        ElseIf HID_auction_type_id.Value = "3" Then
            lbl_proxy_auction_status_label.Text = "Case of Proxy Auction"
        End If

        If dt.Rows.Count > 0 Then
            hid_winner_bid_id.Value = dt.Rows(0)("bid_id")
            hid_winner_buyer_id.Value = dt.Rows(0)("buyer_id")
            lbl_winner_bid_amount.Text = CommonCode.GetFormatedMoney(dt.Rows(0)("bid_amount"))
            lbl_winner_company.Text = dt.Rows(0)("company_name")
            lbl_winner_mobile.Text = "(P) " & dt.Rows(0)("mobile")

            If Not ddl_winner_closed.Items.FindByValue(dt.Rows(0)("action")) Is Nothing Then
                ddl_winner_closed.Items.FindByValue(dt.Rows(0)("action")).Selected = True
            End If
            ddl_winner_closed.Visible = True
            If hid_auction_status.Value = "C" Then
                ddl_winner_closed.Enabled = False
            ElseIf hid_auction_status.Value = "P" Then
                ddl_winner_closed.Enabled = True
                lbl_print_receipt.Text = "<a href=""javascript:void(0);"" onclick=""return open_pop_order_confirmation('/PrintConfirmation.aspx','" & hid_winner_bid_id.Value & "','a');"">Print Order Receipt</a>"
                lbl_print_receipt.Visible = True
            Else
                ddl_winner_closed.Enabled = False
               
            End If
           
            Dim strAddress As String = ""
            strAddress = strAddress & dt.Rows(0)("address1")
            If dt.Rows(0)("address2") <> "" Then
                strAddress = strAddress & "<br />" & dt.Rows(0)("address2")
            End If
            If dt.Rows(0)("city") <> "" Then
                strAddress = strAddress & "<br />" & dt.Rows(0)("city")
            End If
            If dt.Rows(0)("state_code") <> "" Then
                strAddress = strAddress & ", " & dt.Rows(0)("state_code")
            End If
            If dt.Rows(0)("zip") <> "" Then
                strAddress = strAddress & " " & dt.Rows(0)("zip")
            End If
            ltrl_winner_address.Text = strAddress

            Ltl_fedex_option.Text = dt.Rows(0)("shipping_value")
            If hid_winner_buyer_id.Value <> "0" Then
                str = "select top 5 A.auction_id,ISNULL(A.title,'') AS title,ISNULL(A.code,'') AS code, ISNULL(B.bid_amount,0) AS bid_amount from tbl_auctions A WITH (NOLOCK) INNER JOIN tbl_auction_bids B WITH (NOLOCK) ON A.rank1_bid_id=B.bid_id Where A.auction_id<>" & hid_auction_id.Value & " and B.buyer_id =" & hid_winner_buyer_id.Value & " and upper(B.action)=upper('Awarded')"
                Dim dtOther As DataTable = New DataTable()
                dtOther = SqlHelper.ExecuteDataTable(str)
                rpt_winner_won_lots.DataSource = dtOther
                rpt_winner_won_lots.DataBind()
                dtOther = Nothing

                If rpt_winner_won_lots.Items.Count > 0 Then
                    'ltrl_winner_view_all.Text = "<a href='#' onclick='javascript:return open_bidder_history(" & hid_winner_buyer_id.Value & ");' style='text-decoration:underline;'><img src='/Images/bidder-history.gif' alt='' border='0'></a>"
                Else
                    lbl_no_winner_other_winning_history.Text = "No other bid(s) won."
                End If
            End If
        Else
            Dim bid_total As Integer = SqlHelper.ExecuteScalar("select count(distinct buyer_id) from tbl_auction_bids WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i"))
            If bid_total = 0 Then
                lbl_no_trad_proxy_winner.Text = "No bid submitted for this auction."
            Else
                lbl_no_trad_proxy_winner.Text = "No winning bid submitted for this auction."
            End If

            tr_trad_proxy_winners.Visible = False

        End If

        If dt.Rows.Count > 1 Then
            td_first_runner.Visible = True
            hid_first_runner_up_bid_id.Value = dt.Rows(1)("bid_id")
            hid_first_runner_up_buyer_id.Value = dt.Rows(1)("buyer_id")

            lbl_first_runner_bid_amount.Text = CommonCode.GetFormatedMoney(dt.Rows(1)("bid_amount"))

            lbl_first_runner_company.Text = dt.Rows(1)("company_name")
            lbl_first_runner_mobile.Text = "(P) " & dt.Rows(1)("mobile")
            If hid_auction_status.Value = "C" Then
                ddl_first_runner_closed.Visible = True
            Else
                ddl_first_runner_closed.Enabled = False
            End If
            If Not ddl_first_runner_closed.Items.FindByValue(dt.Rows(1)("action")) Is Nothing Then
                ddl_first_runner_closed.Items.FindByValue(dt.Rows(1)("action")).Selected = True
            End If

            ddl_first_runner_closed.Visible = True
            If hid_auction_status.Value = "C" Then
                ddl_first_runner_closed.Enabled = False
            ElseIf hid_auction_status.Value = "P" Then
                ddl_first_runner_closed.Enabled = True
            Else
                ddl_first_runner_closed.Enabled = False
            End If
            Dim strAddress As String = ""
            strAddress = strAddress & dt.Rows(1)("address1")
            If dt.Rows(1)("address2") <> "" Then
                strAddress = strAddress & "<br />" & dt.Rows(1)("address2")
            End If
            If dt.Rows(1)("city") <> "" Then
                strAddress = strAddress & "<br />" & dt.Rows(1)("city")
            End If
            If dt.Rows(1)("state_code") <> "" Then
                strAddress = strAddress & ", " & dt.Rows(1)("state_code")
            End If
            If dt.Rows(1)("zip") <> "" Then
                strAddress = strAddress & " " & dt.Rows(1)("zip")
            End If

            ltrl_first_runner_address.Text = strAddress

            ltrl_fedex_option_first_runner.Text = dt.Rows(1)("shipping_value")

            If hid_first_runner_up_buyer_id.Value <> "0" Then
                str = "select top 5 A.auction_id, ISNULL(A.title,'') AS title,ISNULL(A.code,'') AS code, ISNULL(B.bid_amount,0) AS bid_amount from tbl_auctions A WITH (NOLOCK) INNER JOIN tbl_auction_bids B WITH (NOLOCK) ON A.rank1_bid_id=B.bid_id Where A.auction_id<>" & hid_auction_id.Value & " and B.buyer_id =" & hid_first_runner_up_buyer_id.Value & " and upper(B.action)=upper('Awarded')"
                Dim dtOther As DataTable = New DataTable()
                dtOther = SqlHelper.ExecuteDataTable(str)
                rpt_first_runner_up_won_lots.DataSource = dtOther
                rpt_first_runner_up_won_lots.DataBind()
                dtOther = Nothing
                If rpt_first_runner_up_won_lots.Items.Count > 0 Then
                    '    ltrl_first_runner_up_view_all.Text = "<a href='#' onclick='javascript:return open_bidder_history(" & hid_first_runner_up_buyer_id.Value & ");' style='text-decoration:underline;'><img src='/Images/bidder-history.gif' alt='' border='0'></a>"
                    '    'ltrl_first_runner_up_view_all.Text = "<a href='/Auctions/Buyer_winning_history.aspx?i=" & hid_first_runner_up_buyer_id.Value & "' target='_blank' style='text-decoration:underline;'><img src='/Images/bidder-history-2.gif' alt='' border='0'></a>"
                Else
                    lbl_no_first_runner_up_other_winning_history.Text = "No other bid(s) won."
                End If
            End If
        End If
        If dt.Rows.Count > 2 Then
            td_second_runner.Visible = True
            hid_second_runner_up_bid_id.Value = dt.Rows(2)("bid_id")
            hid_second_runner_up_buyer_id.Value = dt.Rows(2)("bid_id")
            lbl_second_runner_bid_amount.Text = CommonCode.GetFormatedMoney(dt.Rows(2)("bid_amount"))
            lbl_second_runner_company.Text = dt.Rows(2)("company_name")
            lbl_second_runner_mobile.Text = "(P) " & dt.Rows(2)("mobile")
            If Not ddl_second_runner_closed.Items.FindByValue(dt.Rows(2)("action")) Is Nothing Then
                ddl_second_runner_closed.Items.FindByValue(dt.Rows(2)("action")).Selected = True
            End If
            ddl_second_runner_closed.Visible = True
            If hid_auction_status.Value = "C" Then
                ddl_second_runner_closed.Enabled = False
            ElseIf hid_auction_status.Value = "P" Then
                ddl_second_runner_closed.Enabled = True
            Else
                ddl_second_runner_closed.Enabled = False
            End If

            Dim strAddress As String = ""
            strAddress = strAddress & dt.Rows(2)("address1")
            If dt.Rows(2)("address2") <> "" Then
                strAddress = strAddress & "<br />" & dt.Rows(2)("address2")
            End If
            If dt.Rows(2)("city") <> "" Then
                strAddress = strAddress & "<br />" & dt.Rows(2)("city")
            End If
            If dt.Rows(2)("state_code") <> "" Then
                strAddress = strAddress & ", " & dt.Rows(2)("state_code")
            End If
            If dt.Rows(2)("zip") <> "" Then
                strAddress = strAddress & " " & dt.Rows(2)("zip")
            End If

            ltrl_second_runner_address.Text = strAddress

            ltrl_fedex_option_second_runner.Text = dt.Rows(2)("shipping_value")

            If hid_second_runner_up_buyer_id.Value <> "0" Then
                str = "select top 5 A.auction_id,ISNULL(A.title,'') AS title,ISNULL(A.code,'') AS code, ISNULL(B.bid_amount,0) AS bid_amount from tbl_auctions A WITH (NOLOCK) INNER JOIN tbl_auction_bids B WITH (NOLOCK) ON A.rank1_bid_id=B.bid_id Where A.auction_id<>" & hid_auction_id.Value & " and B.buyer_id =" & hid_second_runner_up_buyer_id.Value & " and upper(B.action)=upper('Awarded')"
                Dim dtOther As DataTable = New DataTable()
                dtOther = SqlHelper.ExecuteDataTable(str)
                rpt_second_runner_up_won_lots.DataSource = dtOther
                rpt_second_runner_up_won_lots.DataBind()
                dtOther = Nothing
                If rpt_second_runner_up_won_lots.Items.Count > 0 Then
                    'ltrl_second_runner_up_view_all.Text = "<a href='/Auctions/Buyer_winning_history.aspx?i=" & hid_second_runner_up_buyer_id.Value & "' target='_blank' style='text-decoration:underline;'><img src='/Images/bidder-history-2.gif' alt='' border='0'></a>"
                    'ltrl_second_runner_up_view_all.Text = "<a href='#' onclick='javascript:return open_bidder_history(" & hid_second_runner_up_buyer_id.Value & ");' style='text-decoration:underline;'><img src='/Images/bidder-history.gif' alt='' border='0'></a>"
                Else
                    lbl_no_second_runner_up_other_winning_history.Text = "No other bid(s) won."
                End If
                'bind_other_bidders()
            End If
        End If
        If dt.Rows.Count > 0 Then
            pnl_other_runners_up.Visible = True
        Else
            pnl_other_runners_up.Visible = False
        End If
        dt = Nothing

    End Sub
    Protected Sub ddl_first_runner_closed_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_first_runner_closed.SelectedIndexChanged
        If ddl_first_runner_closed.SelectedItem.Value <> "" Then
            update_bid_status(ddl_first_runner_closed.SelectedItem.Value, hid_first_runner_up_bid_id.Value)
            'If ddl_first_runner_closed.SelectedItem.Value = "Awarded" Then
            '    Dim obj As New Email()
            '    obj.SendWinningBidEmail(hid_first_runner_up_bid_id.Value)
            '    obj = Nothing
            'End If
            CommonCode.insert_system_log("Auction second runner " & ddl_second_runner_closed.SelectedItem.Text, "ddl_first_runner_closed_SelectedIndexChanged", Request.QueryString.Get("i"), "", "Auction")
        End If
    End Sub
    Protected Sub ddl_winner_closed_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_winner_closed.SelectedIndexChanged
        If ddl_winner_closed.SelectedItem.Value <> "" Then
            update_bid_status(ddl_winner_closed.SelectedItem.Value, hid_winner_bid_id.Value)
           
            'If ddl_winner_closed.SelectedItem.Value = "Awarded" Then
            '    Dim obj As New Email()
            '    obj.SendWinningBidEmail(hid_winner_bid_id.Value)
            '    obj = Nothing
            'End If

            CommonCode.insert_system_log("Auction current winner " & ddl_first_runner_closed.SelectedItem.Text, "ddl_winner_closed_SelectedIndexChanged", Request.QueryString.Get("i"), "", "Auction")
        End If

    End Sub
    Protected Sub ddl_second_runner_closed_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_second_runner_closed.SelectedIndexChanged
        If ddl_second_runner_closed.SelectedItem.Value <> "" Then
            update_bid_status(ddl_second_runner_closed.SelectedItem.Value, hid_second_runner_up_bid_id.Value)
            'If ddl_second_runner_closed.SelectedItem.Value = "Awarded" Then
            '    Dim obj As New Email()
            '    obj.SendWinningBidEmail(hid_second_runner_up_bid_id.Value)
            '    obj = Nothing
            'End If

            CommonCode.insert_system_log("Auction second runner " & ddl_second_runner_closed.SelectedItem.Text, "ddl_second_runner_closed_SelectedIndexChanged", Request.QueryString.Get("i"), "", "Auction")
        End If
    End Sub
    Private Sub update_bid_status(ByVal status As String, ByVal bid_id As Integer)
        Dim str As String = ""
        str = "update tbl_auction_bids set action='" & status & "' where bid_id= " & bid_id
        SqlHelper.ExecuteNonQuery(str)
        CommonCode.insert_system_log("Auction bidding status updated", "update_bid_status", Request.QueryString.Get("i"), "", "Auction")
        lbl_msg_bid_over_status_update.Text = "Status updated successfully."
    End Sub
    Private Sub bind_buy_partial_bidding()
        Dim dt As DataTable = New DataTable()
        Dim str As String = "select A.buy_id,B.buyer_id,ISNULL(A.status,'') As status,ISNULL(A.action,'') As action,ISNULL(A.shipping_value,'') As shipping_value," & _
      "A.price As price," & _
      "A.bid_date," & _
      "isnull(A.grand_total_amount,0) as grand_total_amount," & _
      "ISNULL(A.quantity,0) AS quantity,case ISNULL(A.buy_type,'') when 'partial' then 'Partial Offer' when 'private' then 'Private Offer' when 'buy now' then 'Buy Now' end  AS buy_type," & _
      "ISNULL(C.company_name,'')AS company_name," & _
      "ISNULL(C.address1,'') As address1," & _
      "ISNULL(C.address2,'') As address2," & _
      "ISNULL(C.city,'') AS city," & _
      "ISNULL(S.name,'') AS state," & _
      "ISNULL(S.code,'') AS state_code," & _
      "ISNULL(C.zip,'') AS zip," & _
      "ISNULL(C.mobile,'') AS mobile " & _
      "from tbl_auction_buy A WITH (NOLOCK) " & _
      "LEFT JOIN tbl_reg_buyer_users B ON A.buyer_user_id=B.buyer_user_id " & _
      "LEFT JOIN tbl_reg_buyers C ON A.buyer_id=C.buyer_id " & _
      "LEFT JOIN tbl_master_states S ON C.state_id=S.state_id " & _
      "where A.auction_id=" & hid_auction_id.Value & " Order by A.buy_type, A.price desc"
        dt = SqlHelper.ExecuteDataTable(str)
        dl_buy_partial_offers.DataSource = dt
        dl_buy_partial_offers.DataBind()
        If dt.Rows.Count = 0 Then
            lbl_no_buy_now_partial_offers.Text = "No offer submitted for this auction."
        End If
    End Sub
    Protected Sub dl_buy_partial_offers_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dl_buy_partial_offers.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim ddl_closed As DropDownList = CType(e.Item.FindControl("ddl_closed"), DropDownList)
            Dim ddl_bid_status As DropDownList = CType(e.Item.FindControl("ddl_bid_status"), DropDownList)
            Dim pnl_label_closed_action As Panel = CType(e.Item.FindControl("pnl_label_closed_action"), Panel)
            ' = CType(e.Item.DataItem, DataListItem)
            ' Dim lbl_print_confirmation As Label = CType(e.Item.FindControl("lbl_print_confirmation"), Label)
            Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)
            If Not ddl_bid_status.Items.FindByValue(drv("status").ToString()) Is Nothing Then
                ddl_bid_status.Items.FindByValue(drv("status").ToString()).Selected = True
            End If
            If Not ddl_closed.Items.FindByValue(drv("action").ToString()) Is Nothing Then
                ddl_closed.Items.FindByValue(drv("action").ToString()).Selected = True
            End If
            If hid_auction_status.Value.ToUpper() = "P" Then
                ddl_closed.Visible = True
                pnl_label_closed_action.Visible = True
                ddl_bid_status.Enabled = True
            Else
                ddl_bid_status.Enabled = False
            End If
            If ddl_closed.SelectedItem.Value.ToUpper() <> "PENDING" Then
                ddl_bid_status.Enabled = False
            End If
            If ddl_bid_status.SelectedItem.Value.ToUpper() = "ACCEPT" Then
                ddl_closed.Enabled = True
            End If

        End If
    End Sub
    Protected Sub update_bid_buy_close_status(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList = CType(sender, DropDownList)
        Dim item As DataListItem = ddl.NamingContainer
        Dim buy_id As Integer = dl_buy_partial_offers.DataKeys(item.ItemIndex).ToString()
        Dim status As String = ddl.SelectedItem.Value
        Dim str As String = ""
        str = "update tbl_auction_buy set action='" & status & "' where buy_id=" & buy_id
        SqlHelper.ExecuteNonQuery(str)
        If ddl.SelectedItem.Value = "Awarded" Then
            Dim obj As New Email()
            obj.SendBuyNowAcceptEmail(buy_id)
            obj = Nothing
        End If
        CommonCode.insert_system_log("Auction buy action updated", "update_bid_buy_close_status", Request.QueryString.Get("i"), "", "Auction")
        bind_buy_partial_bidding()
    End Sub
    Protected Sub update_bid_buy_status(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList = CType(sender, DropDownList)
        Dim item As DataListItem = ddl.NamingContainer
        Dim buy_id As Integer = dl_buy_partial_offers.DataKeys(item.ItemIndex).ToString()
        Dim status As String = ddl.SelectedItem.Value
        Dim str As String = ""
        str = "update tbl_auction_buy set status='" & status & "' where buy_id=" & buy_id
        SqlHelper.ExecuteNonQuery(str)
        CommonCode.insert_system_log("Auction buy status updated", "update_bid_buy_status", Request.QueryString.Get("i"), "", "Auction")
        bind_buy_partial_bidding()
    End Sub
    Private Sub bind_quotation()
        Dim dt As DataTable = New DataTable()
        Dim str As String = ""
        str = "select B.buyer_id," & _
        "A.quotation_id," & _
        "A.bid_date," & _
        "ISNULL(A.details,'') AS details," & _
        "ISNULL(A.filename,'') AS filename," & _
        "ISNULL(C.company_name,'')AS company_name," & _
        "ISNULL(C.address1,'') As address1," & _
        "ISNULL(C.address2,'') As address2," & _
        "ISNULL(C.city,'') AS city," & _
        "ISNULL(S.name,'') AS state," & _
        "ISNULL(S.code,'') AS state_code," & _
        "ISNULL(C.zip,'') AS zip," & _
        "ISNULL(C.mobile,'') AS mobile " & _
        "from tbl_auction_quotations A " & _
        "INNER JOIN tbl_reg_buyer_users B ON A.buyer_user_id=B.buyer_user_id " & _
        "INNER JOIN tbl_reg_buyers C ON B.buyer_id=C.buyer_id " & _
        "LEFT JOIN tbl_master_states S ON C.state_id=S.state_id " & _
        "where A.auction_id =" & Request.QueryString.Get("i") 
        dt = SqlHelper.ExecuteDataTable(str)
        dl_quotation.DataSource = dt
        dl_quotation.DataBind()
        If dt.Rows.Count = 0 Then
            lbl_no_quotation.Text = "No offer submitted for this auction."
        End If
        dt = Nothing
    End Sub
    Protected Sub update_quotation_status(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList = CType(sender, DropDownList)
        Dim item As DataListItem = ddl.NamingContainer
        Dim quotation_id As Integer = dl_buy_partial_offers.DataKeys(item.ItemIndex).ToString()
        Dim status As String = ddl.SelectedItem.Text
        Dim str As String = ""
        str = "update tbl_auction_quotations set status='" & status & "' where buy_id=" & quotation_id
        SqlHelper.ExecuteNonQuery(str)
        If ddl.SelectedItem.Value = "Awarded" Then
            Dim obj As New Email()
            obj.SendQuotationAcceptEmail(quotation_id)
            obj = Nothing
        End If
        CommonCode.insert_system_log("Auction offer status updated", "update_quotation_status", Request.QueryString.Get("i"), "", "Auction")
    End Sub

#End Region


    Protected Sub img_btn_sendemail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_btn_sendemail.Click
        Dim dt As New DataTable
        dt = SqlHelper.ExecuteDatatable("select max(bid_id) as bid_id, buyer_id from tbl_auction_bids WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i") & " group by buyer_id ")
        If dt.Rows.Count > 0 Then
            Dim obj As New Email()

            For i = 0 To dt.Rows.Count - 1
                obj.SendWinningBidEmail(dt.Rows(i)("bid_id"))
            Next
            obj = Nothing
            SqlHelper.ExecuteNonQuery("update tbl_auctions WITH (NOLOCK) set is_email_sent=1 where auction_id=" & Request.QueryString.Get("i"))
            CommonCode.insert_system_log("Auction Email Sent for Winner and Looser", "img_btn_sendemail_Click", Request.QueryString.Get("i"), "", "Auction")
            lbl_msg_mail.Visible = True
            lbl_msg_mail.Text = "Mail sent Successfully"
            img_btn_sendemail.Visible = False

        End If
    End Sub
End Class
