﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Auction_Comments.ascx.vb"
    Inherits="Auctions_UserControls_Auction_Comments" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
    Skin="Simple" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" alt="" />
    </telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="RadAjaxPanel9" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
    CssClass="TabGrid">
    <asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
    <asp:HiddenField ID="hid_user_id" runat="server" Value="0" />
    <telerik:RadGrid ID="RadGrid_Comments" GridLines="None" runat="server" AllowAutomaticDeletes="True"
        AllowAutomaticInserts="True" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True"
        AutoGenerateColumns="False" DataSourceID="SqlDataSource1" AllowMultiRowSelection="false"
        AllowMultiRowEdit="false" Skin="Vista" AllowSorting="true" ShowGroupPanel="false">
        <PagerStyle Mode="NextPrevAndNumeric" />
        <MasterTableView Width="100%" DataKeyNames="comment_id" DataSourceID="SqlDataSource1"
            ItemStyle-Height="40" AlternatingItemStyle-Height="40" HorizontalAlign="NotSet"
            AutoGenerateColumns="False" SkinID="Vista" CommandItemDisplay="Top" EditMode="EditForms">
            <CommandItemStyle BackColor="#E1DDDD" />
            <NoRecordsTemplate>
                No comment is available
            </NoRecordsTemplate>
            <CommandItemSettings AddNewRecordText="Add New Comment" ShowRefreshButton="false" />
            <Columns>
                <%--<telerik:GridEditCommandColumn UniqueName="EditCommandColumn">
            </telerik:GridEditCommandColumn>--%>
                <telerik:GridTemplateColumn HeaderText="Posted By" SortExpression="submit_by_user"
                    UniqueName="submit_by_user" DataField="submit_by_user" GroupByExpression="submit_by_user [Posted By] Group By submit_by_user"
                    ItemStyle-Width="200px">
                    <ItemTemplate>
                        <div style="font-weight: bold;">
                            <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/users/user_mouse_over.aspx?i=<%# Eval("submit_by_user_id") %>','270','white','true');">
                                <%# Eval("submit_by_user")%></a>
                        </div>
                        <div style="color: Gray;">
                            <%# Eval("submit_date")%></div>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="comment" HeaderText="Comment" SortExpression="comment"
                    UniqueName="comment" EditFormHeaderTextFormat="Comment">
                </telerik:GridBoundColumn>
            </Columns>
            <EditFormSettings ColumnNumber="2" EditFormType="Template" EditColumn-ItemStyle-Width="100%">
                <FormTemplate>
                    <table cellpadding="0" cellspacing="2" border="0" width="838">
                        <tr>
                            <td style="font-weight: bold; padding-top: 10px;" colspan="2">
                                Add New Comment
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                Comment<span id="span_auction_title" runat="server" class="req_star"> *</span>
                            </td>
                            <td class="details">
                                <asp:TextBox ID="txt_comment" runat="server" Text='<%#Bind("comment") %>' Width="400"
                                    Height="100" TextMode="MultiLine" />
                                <asp:RequiredFieldValidator ID="rfv_comment" runat="server" Font-Size="10px" ControlToValidate="txt_comment"
                                    Display="Static" ErrorMessage=" Comment Required" ValidationGroup="_comment"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <asp:ImageButton ID="img_btn_save_comment" runat="server" ImageUrl="/images/save.gif"
                                    ValidationGroup="_comment" CommandName="PerformInsert" />
                                <asp:ImageButton ID="img_btn_cancel" runat="server" ImageUrl="/images/cancel.gif"
                                    CausesValidation="false" CommandName="cancel" />
                                <%-- <asp:Button ID="btnUpdate" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'>
                            </asp:Button>--%>&nbsp;
                            </td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>
        <ClientSettings AllowDragToGroup="false">
            <Selecting AllowRowSelect="True"></Selecting>
        </ClientSettings>
        <GroupingSettings ShowUnGroupButton="false" />
    </telerik:RadGrid>
    <%--<telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditor1" runat="server" TextBoxStyle-Width="400px"
    TextBoxMode="MultiLine" TextBoxStyle-Height="70px" />--%>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
        ProviderName="System.Data.SqlClient" SelectCommand="select A.comment_id,A.auction_id,ISNULL(A.comment,'') As comment,A.submit_date,ISNULL(A.submit_by_user_id,0) as submit_by_user_id,ISNULL(ISNULL(U.first_name,'') + ' ' + ISNULL(U.last_name,''),'System') AS submit_by_user 
from tbl_auction_comments A LEFT JOIN tbl_sec_users U ON A.submit_by_user_id=U.user_id where A.auction_id=@auction_id Order by submit_date DESC"
        InsertCommand="insert INTO tbl_auction_comments(auction_id,comment,submit_date,submit_by_user_id) Values (@auction_id,replace(replace(@comment,CHAR(13)+CHAR(10),'<br/>'),CHAR(10),'<br/>'),GETDATE(),@user_id)">
        <SelectParameters>
            <asp:ControlParameter Name="auction_id" Type="Int32" ControlID="hid_auction_id" PropertyName="Value" />
        </SelectParameters>
        <InsertParameters>
            <asp:ControlParameter Name="auction_id" Type="Int32" ControlID="hid_auction_id" PropertyName="Value" />
            <asp:Parameter Name="comment" Type="String" />
            <asp:ControlParameter Name="user_id" Type="Int32" ControlID="hid_user_id" PropertyName="Value" />
        </InsertParameters>
    </asp:SqlDataSource>
</telerik:RadAjaxPanel>
