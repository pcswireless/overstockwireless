﻿Imports Telerik.Web.UI
Partial Class Auctions_UserControls_LeftbarBucket
    Inherits System.Web.UI.UserControl
    Private bucket_filter_id As Integer = 0
    Private bucket_filter_id_v As Integer = 0
    Protected Sub RadGrid_SelectedBucketlist_left_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid_SelectedBucketlist_left.ItemDataBound
        If e.Item.ItemType = GridItemType.Item Or e.Item.ItemType = GridItemType.AlternatingItem Then
            If bucket_filter_id <> CType(e.Item.FindControl("lit_bucket_filter_id"), Literal).Text Then
                bucket_filter_id = CType(e.Item.FindControl("lit_bucket_filter_id"), Literal).Text
            Else
                CType(e.Item.FindControl("lit_bucket"), Literal).Text = ""
            End If
        End If

    End Sub
    Protected Sub RadGrid_SelectedBucketlist_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid_SelectedBucketlist.ItemDataBound
        If e.Item.ItemType = GridItemType.Item Or e.Item.ItemType = GridItemType.AlternatingItem Then
            If bucket_filter_id_v <> CType(e.Item.FindControl("lit_bucket_filter_id"), Literal).Text Then
                bucket_filter_id_v = CType(e.Item.FindControl("lit_bucket_filter_id"), Literal).Text
            Else
                CType(e.Item.FindControl("lit_bucket"), Literal).Text = ""
            End If
        End If

    End Sub
    Private Function validInvitationSelection() As Boolean
        Dim flg As Boolean = False
        Dim i As Integer = 0
        For i = 0 To RadGrid_SelectedBucketlist_left.Items.Count - 1
            Dim chk As New CheckBox
            chk = CType(RadGrid_SelectedBucketlist_left.Items(i).FindControl("chk"), CheckBox)
            If chk.Checked = True Then flg = True
        Next

        Return flg
    End Function
    Protected Sub btn_add_filter_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_add_filter.Click
        If validInvitationSelection() Then

            'save filter values
            Dim i As Integer = 0
            Dim qry As String = ""

            Dim chk As New CheckBox
            Dim lit_bucket_filter_id As Integer
            Dim bucket_filter_value_id As Integer
            For i = 0 To RadGrid_SelectedBucketlist_left.Items.Count - 1
                chk = CType(RadGrid_SelectedBucketlist_left.Items(i).FindControl("chk"), CheckBox)
                If chk.Checked = True Then
                    lit_bucket_filter_id = CType(RadGrid_SelectedBucketlist_left.Items(i).FindControl("lit_bucket_filter_id"), Literal).Text
                    bucket_filter_value_id = RadGrid_SelectedBucketlist_left.Items(i).GetDataKeyValue("bucket_filter_value_id")
                    qry = "INSERT INTO tbl_auction_bucket_filter(auction_id, bucket_filter_id, bucket_filter_value_id) VALUES (" & Request.QueryString.Get("i") & "," & lit_bucket_filter_id & ", " & bucket_filter_value_id & ")"
                    SqlHelper.ExecuteNonQuery(qry)
                End If
            Next

            bucket_filter_id = 0
            RadGrid_SelectedBucketlist_left.Rebind()

            bucket_filter_id_v = 0
            RadGrid_SelectedBucketlist.Rebind()


        End If

    End Sub
    Protected Sub RadGrid_SelectedBucketlist_ItemDeleted(ByVal source As Object, ByVal e As Telerik.Web.UI.GridDeletedEventArgs) Handles RadGrid_SelectedBucketlist.ItemDeleted
        If e.Exception Is Nothing Then
            bucket_filter_id = 0
            RadGrid_SelectedBucketlist_left.Rebind()
        End If
    End Sub
End Class
