﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ListInvitedBidders.ascx.vb"
    Inherits="Auctions_UserControls_ListInvitedBidders" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<div style="padding-bottom: 10px; padding-top: 8px;">
    <div class="pageheading">
        &nbsp;<asp:Literal ID="lit_heading" runat="server" Text="Invited Bidders"></asp:Literal></div>
</div>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <div style="padding: 0; margin: 0; padding-bottom: 10px; background-color: rgb(251,251,251);">
                <table width="90%" style="text-align: left;">
                    <tr>
                        <td style="padding-left: 10px;">
                            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                                <AjaxSettings>
                                    <telerik:AjaxSetting AjaxControlID="rad_grid_invited_bidders">
                                        <UpdatedControls>
                                            <telerik:AjaxUpdatedControl ControlID="rad_grid_invited_bidders" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        </UpdatedControls>
                                    </telerik:AjaxSetting>
                                </AjaxSettings>
                            </telerik:RadAjaxManager>
                            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
                                left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
                                <img id="Image8" src="/images/img_loading.gif" />
                            </telerik:RadAjaxLoadingPanel>
                            <telerik:RadGrid ID="rad_grid_invited_bidders" GridLines="None" runat="server" PageSize="10"
                                AllowPaging="True" AutoGenerateColumns="False" Skin="Vista" AllowFilteringByColumn="True"
                                ShowGroupPanel="True" AllowSorting="true">
                                <GroupingSettings CaseSensitive="false" />
                                <HeaderStyle BackColor="#BEBEBE" />
                                <ItemStyle Font-Size="11px" />
                                <PagerStyle Mode="NextPrevAndNumeric" />
                                <MasterTableView DataKeyNames="buyer_id" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                                    AllowFilteringByColumn="True" ItemStyle-Height="40" AlternatingItemStyle-Height="40">
                                    <SortExpressions>
                                            <telerik:GridSortExpression FieldName="company_name" SortOrder="Ascending" />
                                    </SortExpressions>
                                    <Columns>
                                        <telerik:GridTemplateColumn HeaderText="Company" SortExpression="company_name" ShowSortIcon="true"
                                            UniqueName="company_name" DataField="company_name" FilterControlWidth="70">
                                            <ItemTemplate>
                                                <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');">
                                                    <%# Eval("company_name")%></a>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="contact_title" HeaderText="Title" SortExpression="contact_title"
                                            UniqueName="contact_title" FilterControlWidth="70">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="name" HeaderText="Name" SortExpression="name"
                                            UniqueName="name" FilterControlWidth="70">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Email Address" SortExpression="email" UniqueName="email"
                                            DataField="email" FilterControlWidth="200" GroupByExpression="email [Email Address] Group By email">
                                            <ItemTemplate>
                                                <a href="mailto:<%# Eval("email")%>">
                                                    <%# Eval("email")%></a>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="mobile" HeaderText="Phone" SortExpression="mobile"
                                            UniqueName="mobile" FilterControlWidth="70">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings AllowDragToGroup="true">
                                    <Selecting AllowRowSelect="True"></Selecting>
                                </ClientSettings>
                                <GroupingSettings ShowUnGroupButton="true" />
                            </telerik:RadGrid>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
