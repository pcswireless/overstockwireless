﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Current_Bidders.aspx.vb"
    Inherits="Auctions_Current_Bidders" EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link type="text/css" rel="Stylesheet" href="/Style/master_menu.css" />
    <link type="text/css" rel="Stylesheet" href="/Style/pcs.css" />
</head>
<body style="background-color: #fafafa;">
    <form id="form1" runat="server">
        <div id="dhtmltooltip_new">
    </div>
    <table cellpadding="0" cellspacing="5" border="0" style="width: 100%;">
        <asp:Repeater ID="RadGrid_BidHistory" DataSourceID="RadGrid_BidHistory_SqlDataSource"
            runat="server">
            <ItemTemplate>
                <tr>
                    <td style="width: 40%; background-color: #FFFFFF;">
                        <div class="<%# IIf(Container.ItemIndex = 0, "winnerImg", "looserImg")%>">
                            <%# "$" & FormatNumber(Eval("bid_amount"), 2)%>
                        </div>
                        <div class="winnerSubString">
                            Last Bid : $<%# FormatNumber(IIf(Eval("max_bid_amount") > 0, Eval("max_bid_amount"), Eval("bid_amount")), 2)%>
                        </div>
                        <div class="winnerSubString">
                            Total Bid: <%# Eval("no_of_bid")%>
                        </div>
                        <div class="winnerSubString">
                            <%# Convert.ToDateTime(Eval("bid_date")).ToString("dd MMM yyyy hh:mm:ss")%>
                        </div>
                        <br />
                    </td>
                    <td style="width: 60%; background-color: #FFFFFF;">
                        <div class="statusCompany">
                            <a onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');">
                                <%# Eval("bidder")%></a>
                        </div>
                        <div class="statusCompanyDetails">
                           (E) <a href="mailto:<%# Eval("email")%>"><%# Eval("email")%></a>
                        </div>
                        <div class="statusCompanyDetails">
                           (P) <%# Eval("bidder_mobile")%>
                        </div>
                        <div class="statusCompanyDetails">
                            <br />
                        </div>
                        
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <asp:SqlDataSource ID="RadGrid_BidHistory_SqlDataSource" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT ISNULL(C.company_name,'') As bidder,ISNULL(C.mobile,'') As bidder_mobile,A1.buyer_id,A.bid_amount,A.max_bid_amount,ISNULL(bid_date,'1/1/1900') As bid_date, A.bid_type, (A1.first_name + ' ' +A1.last_name) as bidder,A1.email,isnull((select COUNT(bid_id) from tbl_auction_bids WITH (NOLOCK) where auction_id=@auction_id and upper(bid_type)='MANUAL' and buyer_id=A.buyer_id),0) as no_of_bid  FROM tbl_auction_bids A inner join tbl_reg_buyer_users A1 on A.buyer_user_id=A1.buyer_user_id INNER JOIN tbl_reg_buyers C ON A1.buyer_id=C.buyer_id where A.auction_id=@auction_id order by bid_amount desc,bid_date ASC"
        runat="server">
        <SelectParameters>
            <asp:QueryStringParameter Name="auction_id" QueryStringField="i" />
        </SelectParameters>
    </asp:SqlDataSource>
    </form>
    <script type="text/javascript" src="/pcs_js/divarrow_tooltip.js"></script>
</body>
</html>
