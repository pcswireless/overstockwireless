﻿
Partial Class Auctions_DownloadItemSheet
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            CreateCSV()
        End If
    End Sub

    Public Sub CreateCSV()
        Try
            Dim sql As String = ""
            Dim ds1 As New DataTable
            Dim table As csvFile
            Dim recNum As Int16
            Dim str As String = ""

            sql = "SELECT "
            sql = sql & "ISNULL(A.part_no, '') AS part_no,"            sql = sql & "ISNULL(A.description, '') AS description,"            sql = sql & "ISNULL(A.location, '') AS location,"            sql = sql & "ISNULL(S.NAME, '') AS stock_condition,"            sql = sql & "ISNULL(P.NAME, '') AS packaging,"            sql = sql & "ISNULL(A.name, '') AS name,"            sql = sql & "ISNULL(A.comment, '') AS comment,"            sql = sql & "ISNULL(A.quantity, 0) AS quantity,"            sql = sql & "ISNULL(M.NAME, '') AS manufacturer,"            sql = sql & "ISNULL(L.NAME, '') AS stock_location,"            sql = sql & "ISNULL(A.upc, '') AS upc,"            sql = sql & "ISNULL(A.sku, '') AS sku,"            sql = sql & "ISNULL(A.estimated_msrp, 0) AS estimated_msrp,"            sql = sql & "ISNULL(A.total_msrp, 0) AS total_msrp"
            sql = sql & " FROM"            sql = sql & " tbl_auction_product_items A LEFT JOIN tbl_master_stock_conditions S ON A.stock_condition_id=S.stock_condition_id LEFT JOIN tbl_master_stock_locations L ON A.stock_location_id=L.stock_location_id"            sql = sql & " LEFT JOIN tbl_master_packaging P ON A.packaging_id=P.packaging_id LEFT JOIN tbl_master_manufacturers M ON A.manufacturer_id=M.manufacturer_id"
            sql = sql & " WHERE	A.auction_id = " & Request.QueryString.Get("i")
            ds1 = SqlHelper.ExecuteDatatable(sql)



            '---- Create the table
            table = New csvFile()

            '---- Create the appropriate fields
            table.addField("MANUFACTURRER")
            table.addField("TITLE")
            table.addField("PART NO")
            table.addField("QUANTITY")
            table.addField("UPC")
            table.addField("SKU")
            table.addField("ESTIMATED MSRP")
            table.addField("TOTAL MSRP")
            table.addField("STOCK LOCATION")
            table.addField("PACKAGING")
            table.addField("STOCK CONDITION")
            table.addField("DESCRIPTION")
            table.addField("COMMENT")

            For j As Integer = 0 To ds1.Rows.Count - 1

                recNum = table.addRecord()
                With ds1.Rows(j)
                    table.data("MANUFACTURRER", recNum) = .Item("manufacturer")
                    table.data("TITLE", recNum) = .Item("name")
                    table.data("PART NO", recNum) = .Item("part_no")
                    table.data("QUANTITY", recNum) = .Item("quantity")
                    table.data("UPC", recNum) = .Item("upc")
                    table.data("SKU", recNum) = .Item("sku")
                    table.data("ESTIMATED MSRP", recNum) = .Item("estimated_msrp")
                    table.data("TOTAL MSRP", recNum) = .Item("total_msrp")
                    table.data("STOCK LOCATION", recNum) = .Item("stock_location")
                    table.data("PACKAGING", recNum) = .Item("packaging")
                    table.data("STOCK CONDITION", recNum) = .Item("stock_condition")
                    table.data("DESCRIPTION", recNum) = .Item("description")
                    table.data("COMMENT", recNum) = .Item("comment")
                End With

            Next

            ds1.Dispose()

            ds1 = Nothing
         
            '---- Save the table to disk
            table.save(Server.MapPath("/") & "\upload\Auctions\product_items\productitems.csv")

            Me.litMessage.Text = "Click <a href='/upload/Auctions/product_items/productitems.csv'>here</a> to download template file (.csv)"

        Catch ex As Exception
            Me.litMessage.Text = "<font color=red>ERROR: " & ex.Message.ToString & "</font>"
        End Try

    End Sub

End Class
