﻿
Partial Class Auctions_UploadImage
    Inherits System.Web.UI.Page
    Dim count As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select title,is_active,dbo.get_auction_status(auction_id) As status from tbl_auctions WITH (NOLOCK) where auction_id=" & Request.QueryString.Get("i"))
            If dt.Rows.Count > 0 Then
                With dt.Rows(0)
                    page_heading.Text = CommonCode.decodeSingleQuote(.Item("title"))
                    If Not .Item("is_active") Then
                        ltrl_current_status.Text = "<span style='color:red;'> (Inactive) </span>"
                    Else
                        Select Case .Item("status")
                            Case 1
                                ltrl_current_status.Text = "<span style='color:#339933;'> (Running) </span>"
                            Case 2
                                ltrl_current_status.Text = "<span style='color:#FF7F00;'> (Upcoming) </span>"
                            Case 3
                                ltrl_current_status.Text = "<span style='color:#FF2A2A;'> (Bid Closed) </span>"
                        End Select
                    End If
                End With

            End If
            dt.Dispose()


            If IsNumeric(Request.QueryString("d")) Then
                RemoveImage(Request.QueryString("d"))
            End If
            bindgrid()

        End If
    End Sub
    Private Sub RemoveImage(ByVal stock_image_id As Integer)
        Dim dt As New DataTable()
        dt = SqlHelper.ExecuteDataTable("select stock_image_id,auction_id,filename from tbl_auction_stock_images where stock_image_id=" & stock_image_id)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Dim filename As String = .Item("filename")
                Dim pathToCreate As String = "/Upload/Auctions/stock_images/" & .Item("auction_id")
                Try
                    SqlHelper.ExecuteNonQuery("Update tbl_auction_stock_images Set position=(select position from tbl_auction_stock_images where stock_image_id=" & stock_image_id & ") Where auction_id = " & .Item("auction_id") & " and position=(select max(position) from tbl_auction_stock_images where auction_id=" & .Item("auction_id") & ");delete tbl_auction_stock_images where stock_image_id=" & stock_image_id)
                    'Response.Write("Update tbl_auction_stock_images Set position=(select position from tbl_auction_stock_images where stock_image_id=" & stock_image_id & ") Where auction_id = " & .Item("auction_id") & " and position=(select max(position) from tbl_auction_stock_images where auction_id=" & .Item("auction_id") & ");delete tbl_auction_stock_images where stock_image_id=" & stock_image_id)

                    Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & stock_image_id & "/" & filename)
                    If infoFile.Exists Then
                        System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & stock_image_id & "/" & filename)
                        CommonCode.insert_system_log("Auction Image Deleted", "RemoveImage", Request.QueryString.Get("i"), "", "Auction")
                    End If
                Catch ex As Exception

                End Try

            End With
        End If
    End Sub
    Private Sub bindgrid()

        Dim ds As New DataTable()
        ds = SqlHelper.ExecuteDataTable("select stock_image_id,auction_id,filename,isnull(position,0) as position from tbl_auction_stock_images where auction_id=" & Request.QueryString.Get("i") & " order by position")
        count = ds.Rows.Count
        Me.grid.DataSource = ds
        Me.grid.DataBind()
        If grid.Items.Count > 0 Then
            lbl_no_image.Visible = False
            grid.Visible = True
        Else
            lbl_no_image.Visible = True
            grid.Visible = False
        End If
    End Sub
    Protected Sub grid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles grid.ItemCreated
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            AddHandler CType(e.Item.FindControl("ddl_sno"), DropDownList).SelectedIndexChanged, AddressOf ddl_sno_Change
        End If
    End Sub
    Protected Sub grid_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles grid.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim intcount As Integer
            Dim position As Integer = CType(e.Item.FindControl("lbl_position"), Label).Text
            Dim ddl As DropDownList
            ddl = CType(e.Item.FindControl("ddl_sno"), DropDownList)
            If Not CType(e.Item.FindControl("ddl_sno"), DropDownList) Is Nothing Then
                For intcount = 1 To count
                    CType(e.Item.FindControl("ddl_sno"), DropDownList).Items.Add(New ListItem(intcount, intcount))
                Next
                If Not CType(e.Item.FindControl("ddl_sno"), DropDownList).Items.FindByValue(position) Is Nothing Then
                    CType(e.Item.FindControl("ddl_sno"), DropDownList).Items.FindByValue(position).Selected = True
                End If
            End If
        End If
    End Sub
    Private Sub ddl_sno_Change(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim auction_id As Integer = Request.QueryString.Get("i")
        Dim current_position As Integer = CType(sender, DropDownList).ToolTip
        Dim desire_position As Integer = CType(sender, DropDownList).SelectedValue
        SqlHelper.ExecuteNonQuery("Update tbl_auction_stock_images Set position=99999 Where auction_id = " & auction_id & " and position=" & current_position & ";Update tbl_auction_stock_images Set position=" & current_position & " Where auction_id = " & auction_id & " and position=" & desire_position & ";Update tbl_auction_stock_images Set position=" & desire_position & " Where auction_id = " & auction_id & " and position=99999")
        'Response.Write("Update tbl_auction_stock_images Set position=99999	Where auction_id = " & auction_id & " and position=" & current_position & ";Update tbl_auction_stock_images Set position=" & current_position & " Where auction_id = " & auction_id & " and position=" & desire_position & ";Update tbl_auction_stock_images Set position=" & desire_position & " Where auction_id = " & auction_id & " and position=99999")
        bindgrid()
    End Sub
    Protected Sub btn_Image_Upload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_Image_Upload.Click
        lbl_msg.Text = ""
        If Page.IsValid Then
            If FileUpload1.HasFile Then
                Dim ext As String
                ext = IO.Path.GetExtension(FileUpload1.PostedFile.FileName).ToLower()
                If ext = ".jpg" Or ext = ".gif" Or ext = ".png" Then
                    Dim strFileName As String = Upload_Picture(Request.QueryString.Get("i"))
                    If strFileName <> "" Then
                        CommonCode.insert_system_log("Auction New Image Uploaded", "btn_Image_Upload_Click", Request.QueryString.Get("i"), "", "Auction")
                    End If
                    Response.Redirect(Request.Url.ToString)
                    'bindgrid()
                Else
                    lbl_msg.Text = "Only jpg, gif or png file will be upload"
                End If
            End If
        End If
    End Sub
    Private Function Upload_Picture(ByVal auction_id As Integer) As String
        Dim filename As String = ""
        Dim stock_image_id As Integer = 0
        Dim pathToCreate As String = "/Upload/Auctions/stock_images/" + auction_id.ToString()
        Dim obj As New CommonClass()
        filename = FileUpload1.FileName
        If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
            System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
        End If

        Dim qry As String = "insert into tbl_auction_stock_images(auction_id,filename,submit_date,submit_by_user_id,position) values(" & auction_id & ",'" & filename & "',getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & "," & grid.Items.Count + 1 & ")  select scope_identity()"
        stock_image_id = SqlHelper.ExecuteScalar(qry)

        If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate & "/" & stock_image_id)) Then
            System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate & "/" & stock_image_id))
        End If

        Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & stock_image_id & "/" & filename)
        If infoFile.Exists Then
            System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & stock_image_id & "/" & filename)
        End If

        'FLUN_Image_1.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & stock_image_id & "/" & filename)
        'lbl_file.Text = obj.GetMapPath(pathToCreate & "/" & stock_image_id)
        obj.UploadFile(FileUpload1, pathToCreate & "/" & stock_image_id, filename, 0)
        obj.GetThumbNailFixedPath(pathToCreate & "/" & stock_image_id & "/" & filename, 120, 100, "_thumb1", 1)
        'obj.GetThumbNailFixedPath(pathToCreate & "/" & stock_image_id & "/" & filename, 72, 60, "_thumb2", 1)
        Return filename

    End Function
End Class
