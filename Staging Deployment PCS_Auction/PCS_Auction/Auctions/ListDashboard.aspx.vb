﻿
Partial Class Auctions_ListDashboard
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            bind_dashboard()

        End If
    End Sub
    Private Sub bind_dashboard()
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable()
        strQuery = "SELECT " & _
        "A.auction_id," & _
      "ISNULL(A.code, '') AS code," & _
      "ISNULL(A.title, '') AS title," & _
      "ISNULL(A.is_private_offer, 0) AS is_private_offer," & _
      "ISNULL(A.is_partial_offer, 0) AS is_partial_offer," & _
      "ISNULL(A.auto_acceptance_price_private, 0) AS auto_acceptance_price_private," & _
      "ISNULL(A.auto_acceptance_price_partial, 0) AS auto_acceptance_price_partial," & _
      "ISNULL(A.auto_acceptance_quantity, 0) AS auto_acceptance_quantity," & _
      "ISNULL(A.start_price, 0) AS start_price," & _
      "ISNULL(A.reserve_price, 0) AS reserve_price," & _
       "ISNULL(A.is_show_reserve_price, 0) AS is_show_reserve_price," & _
      "ISNULL(A.increament_amount, 0) AS increament_amount," & _
      "ISNULL(A.bid_time_interval, 0) AS bid_time_interval," & _
      "ISNULL(A.start_date, '1/1/1900') AS start_date," & _
      "ISNULL(A.end_date, '1/1/1900') AS end_date," & _
      "ISNULL(A.is_display_start_date, 0) AS is_display_start_date," & _
      "ISNULL(A.is_display_end_date, 0) AS is_display_end_date," & _
      "ISNULL(A.show_price, 0) AS show_price," & _
      "ISNULL(A.auction_type_id, 0) AS auction_type_id," & _
      "ISNULL(T.name,'') as auction_type," & _
      "ISNULL(C.name,'') AS auction_category," & _
      "ISNULL(A.request_for_quote_message,'') AS request_for_quote_message " & _
      "FROM tbl_auctions A WITH (NOLOCK) LEFT JOIN tbl_auction_types T on A.auction_type_id=T.auction_type_id LEFT JOIN tbl_auction_categories C" & _
      " ON A.auction_category_id=C.auction_category_id INNER JOIN tbl_reg_sellers S ON A.seller_id=S.seller_id INNER JOIN tbl_reg_seller_user_mapping U ON S.seller_id=U.seller_id "
        Dim strWhere As String = ""
        If Not String.IsNullOrEmpty(Request.QueryString("st")) Then
            If Request.QueryString.Get("st") = "r" Then
                strWhere = " where ISNULL(A.start_date,'1/1/1900')<=getdate() and ISNULL(A.end_date,'1/1/1900') >= getdate() and ISNULL(A.is_active,0)=1 and U.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
            ElseIf Request.QueryString.Get("st") = "p" Then
                strWhere = " where ISNULL(A.end_date,'1/1/1900') <= getdate() and ISNULL(A.is_active,0)=1 and U.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
            Else
                strWhere = " where ISNULL(A.end_date,'1/1/1900') >= getdate() and ISNULL(A.is_active,0)=1 and U.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
            End If
        End If
        strQuery = strQuery & strWhere

        dt = SqlHelper.ExecuteDataTable(strQuery)
        rad_grid_dash_board.DataSource = dt
        rad_grid_dash_board.DataBind()

        If dt.Rows.Count = 0 Then
            If Not String.IsNullOrEmpty(Request.QueryString("st")) Then
                If Request.QueryString.Get("st") = "r" Then
                    lbl_no_record.Text = "There is no running auction."
                Else
                    lbl_no_record.Text = "There is no past auction."
                End If
            End If
        Else
            lbl_no_record.Text = ""
        End If
        dt.Dispose()
    End Sub
    Public Function GetTimeLeft(ByVal end_date As String) As String
        Dim strTimeLeft As String = ""
        Dim currDate As DateTime = DateTime.Now
        Dim diffDays As Integer = 0
        Dim diffHr As Integer = 0
        Dim diffMin As Integer = 0
        If end_date <> String.Empty Then
            Dim ts As TimeSpan = CDate(end_date) - currDate
            diffDays = ts.Days
            diffHr = ts.Hours
            diffMin = ts.Minutes
            strTimeLeft = "<span style='color: #D81B18; font-size: 16px;'>" & diffDays.ToString() & "D " & diffHr.ToString() & "Hrs" & "<br />" & diffMin.ToString() & "Mins</span>"
        End If
        Return strTimeLeft
    End Function
End Class
