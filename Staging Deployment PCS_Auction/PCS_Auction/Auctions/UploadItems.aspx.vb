﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb

Partial Class Auctions_UploadItems
    Inherits System.Web.UI.Page
    Public ext As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

        End If
    End Sub

    Protected Sub btn_Image_Upload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_Image_Upload.Click
        lbl_msg.Text = ""
        If Page.IsValid Then
            If FileUpload1.HasFile Then
                ext = IO.Path.GetExtension(FileUpload1.PostedFile.FileName).ToLower()
                If ext = ".csv" Then
                    Dim strFileName As String = Upload_excel()

                Else
                    lbl_msg.Text = "Only csv format file will be uploaded."
                End If
            End If
        End If
    End Sub

    Private Function Upload_excel() As String
        Dim file_path As String = ""

        Try
            Dim filename As String = ""
            Dim stock_image_id As Integer = Request.QueryString.Get("i")
            Dim pathToCreate As String = "/Upload/Auctions/product_items"
            Dim obj As New CommonClass()
            filename = FileUpload1.FileName
            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
            End If

            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate & "/" & stock_image_id)) Then
                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate & "/" & stock_image_id))
            End If

            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & stock_image_id & "/" & filename)

            If infoFile.Exists Then
                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & stock_image_id & "/" & filename)
            End If
            obj.UploadFile(FileUpload1, pathToCreate & "/" & stock_image_id, filename, 0)

            file_path = Server.MapPath(pathToCreate) & "/" & stock_image_id & "/" & filename

            UpdateProductItems(file_path)
            If Me.lbl_msg.Text = "" Then
                Me.lbl_msg.Text = "Data uploaded successfully."
                Response.Write("<script>window.opener.PostBackFunction_items();window.close();</script>")
            End If

        Catch ex As Exception
            Me.lbl_msg.Text = "ERROR: " & ex.Message.ToString
        End Try

        Return file_path

    End Function

    Public Sub UpdateProductItems(ByVal file_path As String)

        Dim auction_id As Integer = Request.QueryString.Get("i")


        Dim dsItem As DataSet
        Dim obj As New csvFile
        Dim i As Integer, j As Integer
        Dim part_no As String
        Dim description As String
        Dim location As String
        Dim stock_condition As String
        Dim stock_condition_id As Integer
        Dim packaging As String
        Dim packaging_id As Integer
        Dim name As String
        Dim comment As String
        Dim quantity As String
        Dim manufacturer As String
        Dim manufacturer_id As Integer
        Dim stock_location As String
        Dim stock_location_id As Integer
        Dim upc As String
        Dim sku As String
        Dim estimated_msrp As String
        Dim total_msrp As String
        Dim sqlstr As String = ""
        dsItem = obj.ConvertCSV2Dataset1(file_path, "productitems", ",")
        'dg.DataSource = dsItem
        'dg.DataBind()
        If validData(dsItem.Tables(0)) Then

            SqlHelper.ExecuteNonQuery("delete from tbl_auction_product_items where auction_id=" & auction_id)

            Dim dtmanufacturers As DataTable
            Dim dtlocations As DataTable
            Dim dtpackaging As DataTable
            Dim dtconditions As DataTable
            dtmanufacturers = SqlHelper.ExecuteDatatable("select manufacturer_id,name from tbl_master_manufacturers")
            dtlocations = SqlHelper.ExecuteDatatable("select stock_location_id,name from tbl_master_stock_locations")
            dtpackaging = SqlHelper.ExecuteDatatable("select packaging_id,name from tbl_master_packaging")
            dtconditions = SqlHelper.ExecuteDatatable("select stock_condition_id,name from tbl_master_stock_conditions")

            For i = 0 To dsItem.Tables(0).Rows.Count - 1
                With dsItem.Tables(0).Rows(i)
                    sqlstr = ""
                    stock_condition_id = 0
                    packaging_id = 0
                    manufacturer_id = 0
                    stock_location_id = 0
                    manufacturer = .Item("MANUFACTURRER").ToString().Trim()
                    stock_location = .Item("STOCK LOCATION").ToString().Trim()
                    packaging = .Item("PACKAGING").ToString().Trim()
                    stock_condition = .Item("STOCK CONDITION").ToString().Trim()
                    name = .Item("TITLE").ToString().Trim()
                  
                    If manufacturer <> "" And name <> "" Then

                        For j = 0 To dtmanufacturers.Rows.Count - 1
                            If dtmanufacturers.Rows(j).Item("name").ToString.Trim().ToUpper = manufacturer.ToUpper() Then
                                manufacturer_id = dtmanufacturers.Rows(j).Item("manufacturer_id")
                            End If
                        Next

                        If stock_location <> "" Then
                            For j = 0 To dtlocations.Rows.Count - 1
                                If dtlocations.Rows(j).Item("name").ToString.Trim().ToUpper = stock_location.ToUpper() Then
                                    stock_location_id = dtlocations.Rows(j).Item("stock_location_id")
                                End If
                            Next
                        End If

                        If packaging <> "" Then
                            For j = 0 To dtpackaging.Rows.Count - 1
                                If dtpackaging.Rows(j).Item("name").ToString.Trim().ToUpper = packaging.ToUpper() Then
                                    packaging_id = dtpackaging.Rows(j).Item("packaging_id")
                                End If
                            Next
                        End If

                        If stock_condition <> "" Then
                            For j = 0 To dtconditions.Rows.Count - 1
                                If dtconditions.Rows(j).Item("name").ToString.Trim().ToUpper = stock_condition.ToUpper() Then
                                    stock_condition_id = dtconditions.Rows(j).Item("stock_condition_id")
                                End If
                            Next
                        End If



                        part_no = .Item("PART NO").ToString().Trim()
                        quantity = .Item("QUANTITY").ToString().Trim()
                        upc = .Item("UPC").ToString().Trim()
                        sku = .Item("SKU").ToString().Trim()
                        estimated_msrp = .Item("ESTIMATED MSRP").ToString().Trim().Replace("$", "").Replace(",", "")
                        total_msrp = .Item("TOTAL MSRP").ToString().Trim().Replace("$", "").Replace(",", "")
                        location = .Item("STOCK LOCATION").ToString().Trim()

                        description = .Item("DESCRIPTION").ToString().Trim()
                        comment = .Item("COMMENT").ToString().Trim()

                        sqlstr = "INSERT INTO tbl_auction_product_items(auction_id, part_no, description, location, stock_condition_id, packaging_id, name, comment, quantity, submit_date, submit_by_user_id, manufacturer_id, stock_location_id, upc, sku, estimated_msrp, total_msrp)"
                        sqlstr = sqlstr & " VALUES ( " & auction_id & ", '" & part_no & "','" & description & "','" & location & "'," & IIf(stock_condition_id = 0, "null", stock_condition_id) & "," & IIf(packaging_id = 0, "null", packaging_id) & ",'" & name & "','" & comment & "','" & quantity & "',getdate() ,'" & CommonCode.Fetch_Cookie_Shared("user_id") & "'," & manufacturer_id & "," & IIf(stock_location_id = 0, "null", stock_location_id) & ",'" & upc & "','" & sku & "','" & estimated_msrp & "','" & total_msrp & "')"
                        Response.Write(sqlstr)
                        SqlHelper.ExecuteNonQuery(sqlstr)
                    End If
                End With
            Next
            dtmanufacturers.Dispose()
            dtlocations.Dispose()
            dtpackaging.Dispose()
            dtconditions.Dispose()
        End If

        dsItem.Dispose()
        dsItem = Nothing


    End Sub
    Private Function validData(ByVal dtTable As DataTable) As Boolean
        Dim strErr As String = ""



        Dim i As Integer, j As Integer
        
        Dim stock_condition As String
        Dim stock_condition_id As Integer

        Dim packaging As String
        Dim packaging_id As Integer
       
        Dim manufacturer As String
        Dim manufacturer_id As Integer

        Dim stock_location As String
        Dim stock_location_id As Integer

        Dim dtmanufacturers As DataTable
        Dim dtlocations As DataTable
        Dim dtpackaging As DataTable
        Dim dtconditions As DataTable
        dtmanufacturers = SqlHelper.ExecuteDatatable("select manufacturer_id,name from tbl_master_manufacturers")
        dtlocations = SqlHelper.ExecuteDatatable("select stock_location_id,name from tbl_master_stock_locations")
        dtpackaging = SqlHelper.ExecuteDatatable("select packaging_id,name from tbl_master_packaging")
        dtconditions = SqlHelper.ExecuteDatatable("select stock_condition_id,name from tbl_master_stock_conditions")
        For i = 0 To dtTable.Rows.Count - 1

            With dtTable.Rows(i)

                stock_condition_id = 0
                packaging_id = 0
                manufacturer_id = 0
                stock_location_id = 0

                manufacturer = .Item("MANUFACTURRER").ToString().Trim()
                stock_location = .Item("STOCK LOCATION").ToString().Trim()
                packaging = .Item("PACKAGING").ToString().Trim()
                stock_condition = .Item("STOCK CONDITION").ToString().Trim()

                If manufacturer <> "" And stock_location <> "" And packaging <> "" And stock_condition <> "" Then
                    For j = 0 To dtmanufacturers.Rows.Count - 1
                        If dtmanufacturers.Rows(j).Item("name").ToString.ToUpper = manufacturer.ToUpper() Then
                            manufacturer_id = dtmanufacturers.Rows(j).Item("manufacturer_id")
                        End If
                    Next
                    If manufacturer_id = 0 Then strErr = strErr & "<br>Invalid manufacturer at row " & i + 1



                    For j = 0 To dtlocations.Rows.Count - 1
                        If dtlocations.Rows(j).Item("name").ToString.ToUpper = stock_location.ToUpper() Then
                            stock_location_id = dtlocations.Rows(j).Item("stock_location_id")
                        End If
                    Next
                    If stock_location_id = 0 Then strErr = strErr & "<br>Invalid stock location at row " & i + 1


                    For j = 0 To dtpackaging.Rows.Count - 1
                        If dtpackaging.Rows(j).Item("name").ToString.ToUpper = packaging.ToUpper() Then
                            packaging_id = dtpackaging.Rows(j).Item("packaging_id")
                        End If
                    Next
                    If packaging_id = 0 Then strErr = strErr & "<br>Invalid packaging at row " & i + 1



                    For j = 0 To dtconditions.Rows.Count - 1
                        If dtconditions.Rows(j).Item("name").ToString.ToUpper = stock_condition.ToUpper() Then
                            stock_condition_id = dtconditions.Rows(j).Item("stock_condition_id")
                        End If
                    Next
                    If stock_condition_id = 0 Then strErr = strErr & "<br>Invalid stock condition at row " & i + 1

                End If

            End With
        Next

        dtmanufacturers.Dispose()
        dtlocations.Dispose()
        dtpackaging.Dispose()
        dtconditions.Dispose()


        If strErr = "" Then
            Return True
        Else
            Me.lbl_msg.Text = strErr
            Return False
        End If
    End Function
End Class
