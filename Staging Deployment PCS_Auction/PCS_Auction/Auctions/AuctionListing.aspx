﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="AuctionListing.aspx.vb" Inherits="Auctions_AuctionListing" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">

    <asp:HiddenField ID="hid_filter_val" Value="" runat="server" />
    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                     args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
                else {
                    args.set_enableAjax(true);
                }
            }

        </script>
    </telerik:RadScriptBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Vista">
        <ClientEvents OnRequestStart="Cancel_Ajax" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid_Auction_List">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid_Auction_List" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid_Auction_List" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rad_ajax_panel_search">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid_Auction_List" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="imgBtn_Search">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid_Auction_List" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed; left: 420px; top: 180px; z-index: 9999"
        runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Auction Listing
                </div>
            </td>
        </tr>
        <tr>
            <td>
               <telerik:RadAjaxPanel ID="rad_ajax_panel_search" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                    <table cellspacing="0" cellpadding="3" border="0">
                        <tr>
                            <td colspan="3">
                                <b>Search</b>
                            </td>
                        </tr>
                        <tr>
                            <td>Confirmation No</td>
                            <td>
                                <asp:TextBox ID="txt_confirmation_number" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="rfv_search" runat="server" Display="Static" CssClass="error" ErrorMessage="*" ValidationGroup="_search" ControlToValidate="txt_confirmation_number"></asp:RequiredFieldValidator>--%>
                            </td>
                            <td>
                                <asp:ImageButton ID="imgBtn_Search" runat="server" ImageUrl="/Images/search.png" AlternateText="Search" /></td>
                        </tr>
                    </table>
               </telerik:RadAjaxPanel>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; width: 40%; text-align: left; margin-bottom: 3px; padding-top: 10px; font-weight: bold;">
                    Please select the title to edit from below
                </div>
                <div style="float: right; width: 40%; text-align: right; margin-bottom: 3px;" id="new_auction" runat="server" visible="false">
                    <a style="text-decoration: none" href="/Auctions/AddEditAuction.aspx">
                        <img src="/images/add_new_auction.gif" style="border: none" alt="Add New Auction" /></a>

                </div>
            </td>
        </tr>
        <tr>
            <td>
                <%--<asp:TextBox ID="txt_test" runat="server"></asp:TextBox>--%>
                <telerik:RadGrid runat="server" ID="RadGrid_Auction_List" AllowFilteringByColumn="True"
                    AllowSorting="True" PageSize="10" ShowFooter="False" ShowGroupPanel="True" AllowPaging="True"
                    AutoGenerateColumns="false" Skin="Vista" EnableLinqExpressions="false">
                    <PagerStyle Mode="NextPrevAndNumeric" />
                    <ExportSettings IgnorePaging="true" FileName="Auction_Export"
                        OpenInNewWindow="true" ExportOnlyData="true" />
                    <ClientSettings AllowDragToGroup="true">
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="true" />
                    <MasterTableView DataKeyNames="auction_id" AutoGenerateColumns="false"
                        ItemStyle-Height="40" AlternatingItemStyle-Height="40" CommandItemDisplay="Top">
                        <CommandItemStyle BackColor="#d6d6d6" />
                        <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false" ShowExportToPdfButton="false"
                            ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                        <NoRecordsTemplate>
                            No Auction to display
                        </NoRecordsTemplate>
                        <SortExpressions>
                            <telerik:GridSortExpression FieldName="title" SortOrder="Ascending" />
                        </SortExpressions>
                        <Columns>
                            <telerik:GridBoundColumn DataField="code" HeaderText="Auction Code" SortExpression="code"
                                UniqueName="auction_code" AutoPostBackOnFilter="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn DataField="title" HeaderText="Title" SortExpression="title"
                                UniqueName="title" GroupByExpression="title [Title] group by title">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');" onclick="redirectIframe('/Backend_TopBar.aspx?t=<%# CommonCode.getAuctionTabPosition(Eval("auction_id")) %>&a=<%# Eval("auction_id") %>','/Backend_Leftbar.aspx?t=3&a=<%# Eval("auction_id") %>','/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %>')">
                                        <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("title") %>'></asp:Label></a>
                                        <a target="_blank" title='Open in New Window' href='/Auctions/AddEditAuction.aspx?t=9&i=<%# Eval("auction_id") %>' >
                                           <img src='/Images/newwin.gif' alt="Open in New Tab"></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn UniqueName="product_category" DataField="product_category"
                                SortExpression="product_category" HeaderText="Product Category" GroupByExpression="product_category [Product Category] group by product_category">
                                <FilterTemplate>
                                    <telerik:RadComboBox ID="RadComboBoxCategory" DataSourceID="SqlDataSource2" DataTextField="product_category"
                                        DataValueField="product_category" Height="200px" AppendDataBoundItems="true"
                                        SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("product_category").CurrentFilterValue %>'
                                        runat="server" OnClientSelectedIndexChanged="CategoryIndexChanged">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="All" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                        <script type="text/javascript">
                                            function CategoryIndexChanged(sender, args) {
                                                var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                tableView.filter("product_category", args.get_item().get_value(), "EqualTo");

                                            }
                                        </script>
                                    </telerik:RadScriptBlock>
                                </FilterTemplate>
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn DataField="start_date" SortExpression="start_date" UniqueName="start_date" AllowFiltering="false" HeaderText="Start Date" Groupable="false" GroupByExpression="start_date [Start Date] group by start_date" ItemStyle-Width="100">
                                <ItemTemplate>
                                    <%# Convert.ToDateTime(Eval("start_date")).ToString("MM-dd-yyyy hh:mm tt")%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="display_end_time" SortExpression="display_end_time" UniqueName="display_end_time" AllowFiltering="false" HeaderText="End Date" Groupable="false" GroupByExpression="display_end_time [End Date] group by display_end_time" ItemStyle-Width="100">
                                <ItemTemplate>
                                    <%# Convert.ToDateTime(Eval("display_end_time")).ToString("MM-dd-yyyy hh:mm tt")%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="status" HeaderText="Status" SortExpression="status" UniqueName="status" HeaderStyle-Width="120px"
                                DataType="System.String" ItemStyle-Width="120px" GroupByExpression="status [Status] group by status">
                                <FilterTemplate>

                                    <telerik:RadComboBox ID="RadComboBo_Chk_Status" runat="server" OnClientSelectedIndexChanged="StatusIndexChanged"
                                        Width="120px" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("status").CurrentFilterValue %>'>
                                        <Items>
                                            <telerik:RadComboBoxItem Text="All" />
                                            <telerik:RadComboBoxItem Text="Bid Closed" Value="Bid Closed" />
                                            <telerik:RadComboBoxItem Text="Running" Value="Running" />
                                            <telerik:RadComboBoxItem Text="Upcoming" Value="Upcoming" />
                                            <telerik:RadComboBoxItem Text="Inactive" Value="Inactive" />
                                        </Items>
                                    </telerik:RadComboBox>

                                    <telerik:RadScriptBlock ID="RadScriptBlock_combo" runat="server">
                                        <script type="text/javascript">
                                            function StatusIndexChanged(sender, args) {
                                                var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                tableView.filter("status", args.get_item().get_value(), "Contains");

                                            }
                                        </script>
                                    </telerik:RadScriptBlock>
                                </FilterTemplate>
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false" ItemStyle-Width="50"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onclick="redirectIframe('/Backend_TopBar.aspx?t=<%# CommonCode.getAuctionTabPosition(Eval("auction_id")) %>&a=<%# Eval("auction_id") %>','/Backend_Leftbar.aspx?t=3&a=<%# Eval("auction_id") %>','/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %>')">
                                        <img src="/images/edit.png" style="border: none;" alt="Edit" /></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>

                </telerik:RadGrid>
                <asp:HiddenField ID="hid_user_id" runat="server" Value="0" />

                <asp:SqlDataSource ID="SqlDataSource2" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="select DISTINCT [name] as product_category,product_catetory_id from [tbl_master_product_categories]"
                    runat="server"></asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
