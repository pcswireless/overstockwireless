﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="Buyer_winning_history.aspx.vb" Inherits="Auctions_Buyer_winning_history" %>
  <%@ register tagprefix="telerik" namespace="Telerik.Web.UI" assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
  
    <div style="font-size: 14px; margin: 10px;">
        <b>Bidding History Buyer:</b>&nbsp;<asp:Label ID="lbl_buyer" runat="server"></asp:Label>
    </div>
    <div style="font-weight: bold; font-size: 14px; margin: 10px;">
        <telerik:RadGrid ID="RadGrid_BidHistory" DataSourceID="RadGrid_BidHistory_SqlDataSource"
            runat="server" Skin="Simple" AutoGenerateColumns="False" AllowSorting="True"
            AllowPaging="True" PageSize="10" GridLines="Vertical">
            <PagerStyle Mode="NextPrevAndNumeric" />
            <HeaderStyle BackColor="#0190BC" ForeColor="#ffffff" />
            <ItemStyle Font-Size="11px" />
            <MasterTableView DataSourceID="RadGrid_BidHistory_SqlDataSource" AllowMultiColumnSorting="false"
                GroupLoadMode="Server">
                <NoRecordsTemplate>
                    Buyer has not won any auction.
                </NoRecordsTemplate>
                <Columns>
                    <telerik:GridTemplateColumn UniqueName="title" SortExpression="title" HeaderButtonType="TextButton"
                        HeaderText="Auction">
                        <ItemTemplate>
                            <div style="font-weight: bold; font-size: 12px;">
                                <a href='/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %>' target="_blank"
                                    style="text-decoration: underline; color: #006FD5;">
                                    <%# Eval("title")%></a>
                            </div>
                            <div>
                                <%# Eval("sub_title")%>
                            </div>
                            <div>
                                <%# Eval("code")%>
                            </div>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="bid_amount" SortExpression="bid_amount" HeaderButtonType="TextButton"
                        HeaderText="Bid Amount">
                        <ItemTemplate><%# "$" & FormatNumber(Eval("bid_amount"), 2)%>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                   
                </Columns>
            </MasterTableView>
            <ClientSettings AllowDragToGroup="false" />
        </telerik:RadGrid>
        <asp:SqlDataSource ID="RadGrid_BidHistory_SqlDataSource" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
            ProviderName="System.Data.SqlClient" SelectCommand="select A.auction_id,ISNULL(A.code,'') AS code,ISNULL(A.title,'') AS title,ISNULL(A.sub_title,'') AS sub_title,B.buyer_id,
B.bid_amount AS bid_amount from tbl_auctions A WITH (NOLOCK) INNER JOIN tbl_auction_bids B WITH (NOLOCK) ON A.rank1_bid_id=B.bid_id where B.buyer_id=1@buyer_id"
            runat="server">
            <SelectParameters>
                <asp:QueryStringParameter Name="buyer_id" QueryStringField="i" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>
