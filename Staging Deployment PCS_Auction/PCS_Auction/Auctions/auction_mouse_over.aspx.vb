﻿
Partial Class Auctions_auction_mouse_over
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.QueryString.Get("i") <> Nothing AndAlso Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDatatable("SELECT A.auction_id,ISNULL(A.code,'') As code,ISNULL(A.sub_title,'') As sub_title,dbo.get_auction_status(A.auction_id) As status,isnull(CONVERT(varchar(10), A.start_date, 101),'') as start_date,isnull(CONVERT(varchar(10), A.end_date, 101),'') as end_date,T.name as type,ISNULL(IMG.stock_image_id, 0) AS stock_image_id,ISNULL(IMG.filename, '') As image_path,isnull(S.company_name,'') as company_name,isnull(A.start_price,0) as start_price,isnull(A.show_price,0) as show_price,isnull(A.increament_amount,0) as increament_amount,isnull(A.auction_type_id,0) as auction_type_id,ISNULL(ST.name,'') As stock_condition,ISNULL(P.name,'') As packaging FROM tbl_auctions A WITH (NOLOCK) left join tbl_auction_types T on A.auction_type_id=T.auction_type_id left join tbl_reg_sellers S on A.seller_id=S.seller_id left join tbl_master_stock_conditions ST ON A.stock_condition_id=ST.stock_condition_id left join tbl_master_packaging P ON A.packaging_id=P.packaging_id left join tbl_auction_stock_images IMG on A.auction_id=IMG.auction_id and IMG.stock_image_id=(SELECT top 1 stock_image_id from tbl_auction_stock_images where auction_id=A.auction_id order by position)  where A.auction_id=" & Request.QueryString.Get("i") & "")
            If dt.Rows.Count > 0 Then
                Dim status As String = ""
                If dt.Rows(0)("status") = 1 Then
                    status = "Running"
                ElseIf dt.Rows(0)("status") = 2 Then
                    status = "Upcoming"
                ElseIf dt.Rows(0)("status") = 3 Then
                    status = "Closed"
                End If
                If dt.Rows(0)("image_path").ToString <> "" Then
                    img_image.ImageUrl = "/upload/auctions/stock_images/" & dt.Rows(0)("auction_id") & "/" & dt.Rows(0)("stock_image_id").ToString & "/" & dt.Rows(0)("image_path").ToString '.Remove(dt.Rows(0)("image_path").ToString.LastIndexOf(".")) & "_thumb1" & dt.Rows(0)("image_path").ToString.Remove(0, dt.Rows(0)("image_path").ToString.LastIndexOf("."))

                Else
                    img_image.ImageUrl = "/images/imagenotavailable.gif"
                End If

                'lbl_auction.Text = "<b>Company: </b>" & dt.Rows(0)("company_name") & "<br><b>From: </b>" & dt.Rows(0)("start_date") & "<br><b>To: </b>" & dt.Rows(0)("end_date") & IIf(dt.Rows(0)("auction_type_id") = 2 Or dt.Rows(0)("auction_type_id") = 3, "<br><b>Start Price: </b>$" & FormatNumber(dt.Rows(0)("start_price"), 2) & "<br><b>Increment Price: </b>$" & FormatNumber(dt.Rows(0)("increament_amount"), 2), IIf(dt.Rows(0)("auction_type_id") = 2, "<br><b>Buy Price: </b>$" & FormatNumber(dt.Rows(0)("show_price"), 2), "")) & "<br><b>Type: </b>" & dt.Rows(0)("type")
                lbl_auction.Text = "<b>" & dt.Rows(0)("code") & "</b><br>" & dt.Rows(0)("sub_title") & IIf(dt.Rows(0)("packaging") <> "", "<br>" & dt.Rows(0)("packaging"), "") & "<br><br>" & status & "<br>" & dt.Rows(0)("type")
            Else
                img_image.Visible = False
                lbl_auction.Text = "No record for this auction."
            End If
        Else
            img_image.Visible = False
            lbl_auction.Text = "No record for this auction."
        End If
    End Sub
End Class
