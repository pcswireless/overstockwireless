﻿<%@ Page Title="PCS Bidding" Language="VB" MasterPageFile="~/BackendPopUP.master" AutoEventWireup="false"
    CodeFile="BidderAllQuery.aspx.vb" Inherits="Auctions_BidderAllQuery" EnableViewState="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="padding-bottom: 10px; padding-top: 8px;">
        <div class="pageheading">
            &nbsp; All Bidder Queries</div>
    </div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div style="padding: 0; margin: 0; text-align: center; padding-bottom: 10px; background-color: rgb(251,251,251);">
                    <center>
                        <table width="90%" style="text-align: left;">
                            <tr>
                                <td style="padding-left: 10px;">
                                    <asp:Repeater ID="rep_after_queries" runat="server" EnableViewState="false">
                                        <ItemTemplate>
                                            <table width="100%" style="border: 1px solid black; padding: 5px; margin-top: 15px;
                                                text-align: left; border: 2px solid #fff; background-color: #fff">
                                                <tr>
                                                    <td colspan="2" class="qryTitle">
                                                        <%# Container.ItemIndex + 1 & ". "%>
                                                        <a onmouseout="hide_tip_new();" onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');">
                                                            <%# IIf(Eval("auction_name") = "", "", "Auction -" & Eval("auction_name"))%></a>
                                                        <%# IIf(Eval("auction_name").ToString().Trim() = "", "Asked to: " & Eval("ask_to"), "")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="color: #194467; font-weight: bold; padding-left: 20px;">
                                                        <%#Eval("title") %> <%# IIf(Eval("is_active"), "", " <font color='red'>(Deleted)</font>")%>
                                                    </td>
                                                </tr>
<tr>
                                                    <td colspan="2" style="padding-left: 20px;">
                                                        <%# Eval("message")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left: 20px;">
                                                        <a onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');">
                                                            <b>
                                                                <%#Eval("company_name") %></b></a> <span style="padding-left: 10px; color: Gray;">
                                                                    <%#Eval("submit_date") %></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 74%; padding-left: 10px;">
                                                        <asp:Repeater ID="rep_inner_after_queries" runat="server">
                                                            <ItemTemplate>
                                                                <table width="100%" style="border: 1px solid black; padding: 5px; margin-top: 15px;
                                                                    text-align: left; border: 1px solid #C1C1C1;">
                                                                    <tr>
                                                                        <td>
                                                                            <b>
                                                                                <%#Eval("message") %></b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span style="font-style: italic; font-size: 12px; color: gray;">by&nbsp;<%#Eval("first_name") %>&nbsp;on&nbsp;<%#Eval("submit_date") %></span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                        </table>
                    </center>
                    <asp:Label ID="lblNoRecord" runat="server" Text="Currently, there is no bidder query."
                        Visible="false" ForeColor="Red"></asp:Label>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
