﻿Imports Telerik.Web.UI
Partial Class Auctions_Auction_Fedex_Report
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        populate_bidders("")
        If Not Page.IsPostBack Then
            Dim no_of_error As Integer = SqlHelper.ExecuteScalar("select isnull(count(fedex_rate_id),0) from tbl_auction_fedex_rates WITH (NOLOCK) where is_error=1 and auction_id=" & Request.QueryString.Get("i"))
            If no_of_error > 0 Then ddl_erorr_status.Items.FindByValue("1").Selected = True

        End If

    End Sub

    Protected Sub ddl_bidder_ItemsRequested(sender As Object, e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles ddl_bidder.ItemsRequested
        populate_bidders(e.Text)
    End Sub

    Private Sub populate_bidders(ByVal search_text As String)
        Dim sqlSelectCommand As String = ""
        If CommonCode.is_admin_user() Then
            '    sqlSelectCommand = "select distinct A.buyer_id,A.contact_first_name +' ' + A.contact_last_name As buyer_name From tbl_reg_buyers A INNER JOIN tbl_auction_fedex_rates B ON A.buyer_id=B.buyer_id" & _
            '" where B.auction_id=" & Request.QueryString.Get("i") & " and A.contact_first_name +' ' + A.contact_last_name like '% " & search_text & "%' "
            sqlSelectCommand = "select A.buyer_id,ltrim(rtrim(A.contact_first_name +' ' + A.contact_last_name)) As buyer_name From tbl_reg_buyers A " & _
            " INNER JOIN dbo.fn_get_invited_buyers(" & Request.QueryString.Get("i") & ")B ON A.buyer_id=B.buyer_id " & _
            " where A.contact_first_name +' ' + A.contact_last_name like  '% " & search_text & "%'"

        Else
            'sqlSelectCommand = "select distinct A.buyer_id,A.contact_first_name +' ' + A.contact_last_name As buyer_name From tbl_reg_buyers A INNER JOIN tbl_auction_fedex_rates B ON A.buyer_id=B.buyer_id" & _
            '        " where B.auction_id=" & Request.QueryString.Get("i") & " and A.contact_first_name +' ' + A.contact_last_name like '% " & search_text & "%' and A.buyer_id in (select buyer_id from tbl_reg_buyer_seller_mapping where seller_id in " & _
            '        "(select seller_id from tbl_reg_seller_user_mapping where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & "))"
            sqlSelectCommand = "select A.buyer_id,ltrim(rtrim(A.contact_first_name +' ' + A.contact_last_name)) As buyer_name From tbl_reg_buyers A " & _
           " INNER JOIN dbo.fn_get_invited_buyers(" & Request.QueryString.Get("i") & ")B ON A.buyer_id=B.buyer_id " & _
           " where A.contact_first_name +' ' + A.contact_last_name like  '% " & search_text & "%' and A.buyer_id in (select buyer_id from tbl_reg_buyer_seller_mapping where seller_id in " & _
            "(select seller_id from tbl_reg_seller_user_mapping where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & "))"
        End If

        Dim dt As New DataTable()
        ddl_bidder.Items.Clear()
        dt = SqlHelper.ExecuteDatatable(sqlSelectCommand)
        ddl_bidder.DataSource = SqlHelper.ExecuteDataView(sqlSelectCommand)
        ddl_bidder.DataBind()
    End Sub

    Protected Sub rad_grid_buyers_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles rad_grid_buyers.ItemDataBound
        If e.Item.ItemType = GridItemType.Item Or e.Item.ItemType = GridItemType.AlternatingItem Then
            Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)
            Dim is_error As Boolean = CBool(drv("is_error"))
            If is_error Then
                e.Item.CssClass = "grid_sel_backcolor"
        
            End If
        End If
    End Sub

    Protected Sub rad_grid_buyers_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles rad_grid_buyers.NeedDataSource
        Dim str As String = ""
        Dim error_status As String = ddl_erorr_status.SelectedItem.Value
        Dim dt As DataTable
        If CommonCode.is_admin_user() Then
            str = "SELECT fedex_rate_id,ISNULL(A.auction_id, 0) AS auction_id,ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.shipping_amount, 0) AS shipping_amount,ISNULL(A.shipping_options, '') AS shipping_options,ISNULL(A.error_log, '') AS error_log,ISNULL(A.is_error, 0) AS is_error, B.contact_first_name +' ' + B.contact_last_name As buyer_name" & _
       " FROM tbl_auction_fedex_rates A WITH (NOLOCK) INNER JOIN tbl_reg_buyers B ON B.buyer_id=A.buyer_id where A.auction_id=" & Request.QueryString.Get("i") & " and Ltrim(rtrim(B.contact_first_name +' ' + B.contact_last_name)) like '%" & ddl_bidder.Text.Trim() & "%'"
        Else
            str = "SELECT fedex_rate_id,ISNULL(A.auction_id, 0) AS auction_id,ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.shipping_amount, 0) AS shipping_amount,ISNULL(A.shipping_options, '') AS shipping_options,ISNULL(A.error_log, '') AS error_log,ISNULL(A.is_error, 0) AS is_error, B.contact_first_name +' ' + B.contact_last_name As buyer_name" & _
       " FROM tbl_auction_fedex_rates A WITH (NOLOCK) INNER JOIN tbl_reg_buyers B ON B.buyer_id=A.buyer_id where A.auction_id=" & Request.QueryString.Get("i") & " and Ltrim(rtrim(B.contact_first_name +' ' + B.contact_last_name)) like '%" & ddl_bidder.Text.Trim() & "%' and A.buyer_id in (select buyer_id from tbl_reg_buyer_seller_mapping where seller_id in " & _
        "(select seller_id from tbl_reg_seller_user_mapping where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ")) "
        End If
        If error_status <> "2" Then
            str = str & " And ISNULL(A.is_error,0)=" & error_status
        End If
        str = str & " order by B.contact_first_name,ISNULL(A.is_error,0),A.shipping_amount"
        'Response.Write(str)
        dt = SqlHelper.ExecuteDatatable(str)
        rad_grid_buyers.DataSource = dt
    End Sub

    Protected Sub imgBtn_Search_Click(sender As Object, e As ImageClickEventArgs) Handles imgBtn_Search.Click
        rad_grid_buyers.Rebind()
    End Sub

    Protected Sub btn_delete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_delete.Click
        Dim i As Integer = 0
        Dim qry As String = ""
        Dim fedex_rate_id As Integer = 0
        Dim selected As Boolean = False
        For i = 0 To rad_grid_buyers.Items.Count - 1
            If rad_grid_buyers.Items(i).ItemType = GridItemType.Item Or rad_grid_buyers.Items(i).ItemType = GridItemType.AlternatingItem Then
                If CType(rad_grid_buyers.Items(i).FindControl("chk_select"), CheckBox).Checked Then
                    fedex_rate_id = rad_grid_buyers.Items(i).OwnerTableView.DataKeyValues(rad_grid_buyers.Items(i).ItemIndex)("fedex_rate_id").ToString()
                    qry = "delete from tbl_auction_fedex_rates where fedex_rate_id=" & fedex_rate_id
                    SqlHelper.ExecuteNonQuery(qry)
                    selected = True
                End If
            End If
        Next
        If selected Then
            rad_grid_buyers.Rebind()
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "ref_fedex", "<script>window.opener.refresh_fedex();</script>", False)
        End If

    End Sub

End Class
