﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Auction-Bidder-Query.aspx.vb"
    Inherits="Auctions_Auction_Bidder_Query" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta id="Meta1" http-equiv="refresh" content="60" runat="server" />
    <link type="text/css" rel="Stylesheet" href="/Style/pcs.css" />
    <script type="text/javascript">
        function Focus(objname, waterMarkText) {
            obj = document.getElementById(objname);
            if (obj.value == waterMarkText) {
                obj.value = "";
                obj.className = "WaterMarkText";
                obj.style.width = "100%"
                if (obj.value == "Response" || obj.value == "" || obj.value == null) {
                    obj.style.color = "black";
                    obj.style.width = "100%"
                }
            }
        }
        function Blur(objname, waterMarkText) {

            obj = document.getElementById(objname);
            if (obj.value == "") {
                obj.value = waterMarkText;
                //                if (objname != "txtPwd") {
                //                    obj.className = "WaterMarkedTextBox";
                //                }
                //                else {
                obj.className = "WaterMarkedTextBox";
                obj.style.width = "100%"
                //                }
            }
            else {
                obj.className = "WaterMarkText";
                obj.style.width = "100%"
            }

            if (obj.value == "Response" || obj.value == "" || obj.value == null) {
                obj.style.color = "gray";
                obj.style.width = "100%"
            }
        }
    </script>
</head>
<body style="margin: 0px;">
    <form id="form1" runat="server">
    <asp:HiddenField ID="hid_user_id" runat="server" />
    <telerik:RadScriptManager ID="ScriptManager" runat="server" />
    <%-- <table align="left" cellpadding="0" cellspacing="0" border="0" style="padding:0px; width: 370px">
        <tr>
            <td valign="top">--%>
    <telerik:RadGrid ID="RadGrid_Seller" runat="server" PageSize="10" Width="99%" AllowPaging="True"
        AutoGenerateColumns="False" Skin="Simple" ShowGroupPanel="false" AllowSorting="true">
        <PagerStyle Mode="NumericPages" />
       
        <HeaderStyle BackColor="#BEBEBE" />
        <ItemStyle Font-Size="11px" />
        <MasterTableView DataKeyNames="auction_id" AllowFilteringByColumn="false" SkinID="Simple"
            HorizontalAlign="NotSet" AutoGenerateColumns="False" Width="100%">
            <ItemStyle VerticalAlign="Top" Wrap="true" />
            <AlternatingItemStyle VerticalAlign="Top" Wrap="true" />
            <Columns>
                <%--<telerik:GridTemplateColumn HeaderText="Company Name" 
SortExpression="company_name" UniqueName="company_name" FilterControlWidth="50">
                   
                    <ItemTemplate>
                     <asp:LinkButton ID="grd_lnk_select" runat="server" Text='<%#Eval("company_name") 
%>' CommandName="detail" CommandArgument='<%#Eval("seller_id") %>'></asp:LinkButton>
                    </ItemTemplate>
                    </telerik:GridTemplateColumn>--%>
                <telerik:GridBoundColumn DataField="auction_id" HeaderText="Auction" SortExpression="auction_id"
                    UniqueName="auction_id">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Queries" SortExpression="title">
                    <ItemTemplate>
                        <table width="100%" style="border: none; padding: 0; margin: 0;">
                            <tr>
                                <td style="border: none;">
                                    <asp:Label runat="server" Font-Bold="true" ForeColor="#194467" Text='<%#Eval("title") %>'
                                        ID="rep_lbl_title"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border: none;">
                                    <asp:Repeater ID="rep_inner_after_queries" runat="server">
                                        <ItemTemplate>
                                            <table width="100%" style="border: none; padding: 5px; margin-top: 5px; text-align: left;
                                                border: none;">
                                                <tr>
                                                    <td style="border: none;">
                                                        <asp:Label runat="server" Text='<%#Eval("message") %>' ID="rep_inner_lbl_msg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="border: none;">
                                                        <span style="font-style: italic; font-size: 12px; color: #507300;">by&nbsp;<asp:Label
                                                            runat="server" Text='<%#Eval("first_name") %>' ID="rep_inner_lbl_user"></asp:Label></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                            <tr>
                                <td style="border: none;">
                                    <asp:TextBox runat="server" TextMode="MultiLine" onfocus="Focus(this.id,'Response');"
                                        onblur="Blur(this.id,'Response');" CssClass="WaterMarkedTextBox" Text="Response" ID="rep_txt_message"></asp:TextBox>
                                    <asp:HiddenField ID="rep_hid_buyer" Value='<%#Eval("buyer_id") %>' runat="server" />
                                    <asp:HiddenField ID="rep_hid_auction" Value='<%#Eval("auction_id") %>' runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="border: none;">
                                    <asp:ImageButton ID="rep_but_send" runat="server" CommandName="send_query" CommandArgument='<%#Eval("query_id") %>'
                                        ImageUrl="/images/send_reply.gif" AlternateText="Send" />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="submit_date" HeaderText="Date" SortExpression="submit_date"
                    UniqueName="submit_date">
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <%--    </td>
        </tr>
    </table>--%>
    <%--<div style="width:988px;">
      <table width="100%" cellpadding="0" cellspacing="0" >
                    <tr>
                        <td class="dv_md_sub_heading">
                        Bidder Queries
                        </td>
                    </tr>
                    <tr  >
                        <td >
                            <div style="height:500px;overflow:auto;padding:0;margin:0;text-
align:center;background-color:rgb(251,251,251);">
                            <center>
                            
                            <table width="90%" style="text-align:left;;margin-top:10px;">
                            <tr>
                            <td style="padding-left:10px;">
                            <asp:Repeater ID="rep_after_queries" runat="server">
<ItemTemplate>
<table width="100%" style="border:1px solid black;padding:5px;margin-top:15px;text-
align:left;border:2px solid #fff;background-color:#fff">
<tr>
<td colspan="2">
<asp:Label runat="server" Font-Bold="true" ForeColor="#194467" Text='<%#Eval("title") %>' 
ID="rep_lbl_title"></asp:Label>
</td>
</tr>
<tr>
<td >
<div style="float:left;width:20%;">
<asp:Label runat="server" Font-Bold="true" Text='<%#Eval("company_name") %>' 
ID="rep_lbl_name"></asp:Label>
</div>
<div style="float:left;width:80%;">
<asp:Label runat="server" Font-Size="13px" ForeColor="Gray" Text='<%#Eval("submit_date") %>' 
ID="rep_lbl_date"></asp:Label>
</div>
</td>
</tr>
<tr>
<td style="width:74%;">
<asp:Repeater ID="rep_inner_after_queries" runat="server">
<ItemTemplate>
<table width="100%" style="border:1px solid black;padding:5px;margin-top:15px;text-
align:left;border:1px solid #C1C1C1;">
<tr>
<td >
<asp:Label runat="server" Font-Bold="true" Text='<%#Eval("message") %>' 
ID="rep_inner_lbl_msg"></asp:Label>
</td>
</tr>
<tr>
<td >
<span style="font-style:italic;font-size:12px;color:gray;">
by&nbsp;<asp:Label runat="server" Text='<%#Eval("first_name") %>' 
ID="rep_inner_lbl_user"></asp:Label>&nbsp;on&nbsp;<asp:Label runat="server" Text='<%#Eval
("submit_date") %>' ID="rep_inner_lbl_date"></asp:Label></span>
</td>
</tr>
</table>
</ItemTemplate>

</asp:Repeater>
</td>
<td>&nbsp;
</td>
</tr>
<tr>
<td style="width:74%;">
<asp:TextBox runat="server" TextMode="MultiLine"  onfocus="Focus(this.id,'Response');" onblur="Blur
(this.id,'Response');"  CssClass="WaterMarkedTextBox" Text="Response" 
ID="rep_txt_message"></asp:TextBox>
    <asp:HiddenField ID="rep_hid_buyer" Value='<%#Eval("buyer_id") %>' runat="server" />
    <asp:HiddenField ID="rep_hid_auction" Value='<%#Eval("auction_id") %>' runat="server" />
</td>
<td valign="bottom" style="padding-left:15px;"><asp:ImageButton ID="rep_but_send" runat="server" 
CommandName="send_query" CommandArgument='<%#Eval("query_id") %>' ImageUrl="/images/send.gif" 
AlternateText="Send" />
</td>
</tr>
<tr>
<td colspan="2">
<asp:ImageButton ID="rep_but_viewall" CommandArgument='<%#Eval("buyer_id") %>' runat="server" 
ImageUrl="/images/view_all.gif" AlternateText="View all communication with this bidder" />
</td>
</tr>

</table>
</ItemTemplate>
</asp:Repeater>
                            </td>
                            </tr>
                            </table>
                            </center>
                            </div>
                        </td>
                    </tr>

                    <tr>
                    <td>
                    
                    </td>
                    </tr>
                    </table>
    </div>--%>
    </form>
</body>
</html>
