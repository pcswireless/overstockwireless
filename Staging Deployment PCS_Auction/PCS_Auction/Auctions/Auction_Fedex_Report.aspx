﻿<%@ Page Title="PCS Bidding" Language="VB" MasterPageFile="~/BackendPopUP.master" AutoEventWireup="false"
    CodeFile="Auction_Fedex_Report.aspx.vb" Inherits="Auctions_Auction_Fedex_Report" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="Auction_id" runat="server" Value="0" />
    <table cellpadding="10" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Auction Bidder FedEx Report
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="3" width="100%" border="0">
                    <tr>
                        <td width="10%">
                            Bidder
                        </td>
                        <td colspan="2">
                            <telerik:RadComboBox ID="ddl_bidder" runat="server" Height="200px" Width="200px"
                                EmptyMessage="Find Your Bidder" DataTextField="buyer_name" DataValueField="buyer_id"
                                AllowCustomText="true" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Show
                        </td>
                        <td width="25%">
                            <asp:DropDownList ID="ddl_erorr_status" runat="server" Width="200">
                                <asp:ListItem Value="2">All</asp:ListItem>
                                <asp:ListItem Value="1">With Error</asp:ListItem>
                                <asp:ListItem Value="0">Without Error</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:ImageButton ID="imgBtn_Search" runat="server" AlternateText="Search" ImageUrl="/Images/search.png" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_msg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxPanel ID="Rad_panel" runat="server">
                    <telerik:RadGrid ID="rad_grid_buyers" GridLines="None" runat="server" PageSize="10"
                        AllowPaging="True" AutoGenerateColumns="False" AllowMultiRowSelection="false"
                        Skin="Vista" AllowFilteringByColumn="True" AllowSorting="true" ShowGroupPanel="false"
                        EnableLinqExpressions="false">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <MasterTableView DataKeyNames="fedex_rate_id" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                            AllowFilteringByColumn="false" AllowSorting="true" ItemStyle-Height="25" AlternatingItemStyle-Height="25"
                            CommandItemDisplay="None" CellPadding="2">
                            <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false"
                                ShowExportToPdfButton="false" ShowExportToCsvButton="false" ShowAddNewRecordButton="false"
                                ShowRefreshButton="false" />
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="buyer_name" SortOrder="Ascending" />
                            </SortExpressions>
                            <NoRecordsTemplate>
                                FedEx rate not available
                            </NoRecordsTemplate>
                            <Columns>

                                <telerik:GridTemplateColumn HeaderText="Buyer" DataField="buyer_name" UniqueName="buyer_name"
                                    HeaderStyle-Width="150" ItemStyle-Width="140" SortExpression="buyer_name">
                                    <ItemTemplate>
                                        <%-- <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');" onclick="redirectIframe('','/Backend_Leftbar.aspx?t=2&b=<%# Eval("buyer_id")%>','/Bidders/AddEditBuyer.aspx?i=<%# Eval("buyer_id") %>')">
                                            <%# Eval("buyer_name")%>
                                        </a>--%>
                                        <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');">
                                            <%# Eval("buyer_name")%>
                                        </a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="shipping_options" HeaderText="Shipping Option"
                                    SortExpression="shipping_options" UniqueName="shipping_options" HeaderStyle-Width="150"
                                    ItemStyle-Width="140">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="shipping_amount" HeaderText="Amount" SortExpression="shipping_amount"
                                    UniqueName="shipping_amount" DataFormatString="{0:C}" ItemStyle-HorizontalAlign="Right"
                                    HeaderStyle-Width="100" ItemStyle-Width="90">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="error_log" HeaderText="Error" SortExpression="error_log"
                                    UniqueName="error_log">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="Select"  AllowFiltering="false" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chk_select" runat="server" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings AllowDragToGroup="true">
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                        <GroupingSettings ShowUnGroupButton="true" />
                    </telerik:RadGrid>
                    <div style="padding:10px 0px;text-align:right;"><asp:ImageButton ID="btn_delete" runat="server"  AlternateText="Delete" ImageUrl="/images/delete.png" /></div>
                </telerik:RadAjaxPanel>
            </td>
        </tr>
        <tr>
            <td valign="bottom" align="center">
                <br />
                <br />
                <asp:ImageButton ID="ImageButton1" OnClientClick="javascript:window.close();" runat="server"
                    AlternateText="Close" ImageUrl="/images/close.gif" />
            </td>
        </tr>
    </table>
</asp:Content>
