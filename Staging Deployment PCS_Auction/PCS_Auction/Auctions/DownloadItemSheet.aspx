﻿<%@ Page Title="PCS Bidding" Language="VB" MasterPageFile="~/BackendPopUP.master" AutoEventWireup="false" CodeFile="DownloadItemSheet.aspx.vb" Inherits="Auctions_DownloadItemSheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <table cellpadding="5" width="100%">
        <tr>
            <td align="center">
                <asp:Literal id="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
         <tr>
            <td bgcolor="#f7f7f7">
                <b>Steps to download the template</b>
                <br />
                <br />
                1. Click on above link
                <br />
                2. A Save popup will appear. Select "Save as" and save the file on your local machine
                <br />
                3. Add or edit the items in Excel.
                <br />
                <br />
            </td>
        </tr>
        <%--<tr>
            <td>
                <b>Points to Note:</b><br /> 1. MANUFACTURRER<br />
                2. STOCK LOCATION <br />
                3. PACKAGING<br />
                4. STOCK CONDITION<br />
                need to valid string within the list
            </td>
        </tr>--%>

    </table>
</asp:Content>

