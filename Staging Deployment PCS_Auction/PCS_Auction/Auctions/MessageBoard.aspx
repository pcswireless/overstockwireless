﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MessageBoard.aspx.vb" Inherits="Auctions_MessageBoard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PCS Bidding</title>
    <meta id="Meta1" http-equiv="refresh" content="60" runat="server" />
    <link type="text/css" rel="Stylesheet" href="/Style/master_menu.css" />
    <link type="text/css" rel="Stylesheet" href="/Style/pcs.css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlDashboard" runat="server">
        <table cellpadding="0" cellspacing="0" style="width: 370px">
            <tr>
                <td>
                    <asp:DataList ID="dl_message" runat="server" Width="100%" CellPadding="0" CellSpacing="0">
                        
                        <HeaderStyle BackColor="#BEBEBE" />
                        <ItemStyle Font-Size="11px" />
                        <ItemTemplate>
                            <span style="float: left; text-align: justify; padding-left: 3px; padding-right: 3px">
                                <%# Container.DataItem("description")%></span>
                            <br clear="all" />
                            <span style="float: right; font-style: italic; color: #6B849D; font-weight: bolder">
                                by&nbsp;<%# Container.DataItem("name")%>
                            </span>
                            <br clear="all" />
                            <span style="float: right; font-style: italic; color: #638303;">
                                <%# Format(Convert.ToDateTime(Container.DataItem("submit_date")), "MM/dd/yyyy hh:mm tt")%></span>
                            <br clear="all" />
                            <%-- <hr style="background-color: #cccccc" />--%>
                        </ItemTemplate>
                        <SeparatorTemplate>
                            <hr style="background-color: #D9D9D9" />
                        </SeparatorTemplate>
                        
                    </asp:DataList>
                    <asp:Label ID="lbl_no_record" runat="server" EnableViewState="false"> </asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
