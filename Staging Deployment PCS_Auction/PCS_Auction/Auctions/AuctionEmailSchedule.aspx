﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="AuctionEmailSchedule.aspx.vb" Inherits="Auctions_AuctionEmailSchedule"  %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                     args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
                else {
                    args.set_enableAjax(true);
                }
            }
            function ShowWait() {
              document.getElementById('wait').style.display = 'block';
              document.getElementById('<%=btn_send_email.ClientID%>').style.display = 'none';
        return true;

      }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Vista">
        <ClientEvents OnRequestStart="Cancel_Ajax" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rad_grid_sublogins">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rad_grid_sublogins" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rad_grid_sublogins" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Auctions Email Schedule</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="height: 28px; display: block;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
                    left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
                    <img id="Image8" src="/images/img_loading.gif" />
                </telerik:RadAjaxLoadingPanel>
                <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                    <asp:Label ID="lbl_no_permit" runat="server" Text="You do not have permission to view this page."></asp:Label>
                    <telerik:RadGrid ID="rad_grid_sublogins" runat="server" AllowSorting="True" AllowMultiRowSelection="false"
                        ShowGroupPanel="True" AllowPaging="True" PageSize="10" AutoGenerateColumns="false"
                        Skin="Vista" EnableLinqExpressions="false" OnNeedDataSource="rad_grid_sublogins_NeedDataSource"
                        OnDeleteCommand="rad_grid_sublogins_DeleteCommand" OnInsertCommand="rad_grid_sublogins_InsertCommand"
                        OnUpdateCommand="rad_grid_sublogins_UpdateCommand">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <ExportSettings IgnorePaging="true" FileName="Auction_EmailSchedule_Export" OpenInNewWindow="true"
                            ExportOnlyData="true" />
                        <ClientSettings AllowDragToGroup="true">
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                        <GroupingSettings ShowUnGroupButton="true" />
                        <MasterTableView CommandItemDisplay="Top" DataKeyNames="email_schedule_id" AllowFilteringByColumn="true"
                            AutoGenerateColumns="False" EditMode="EditForms">
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="title" SortOrder="Ascending" />
                            </SortExpressions>
                            <NoRecordsTemplate>
                                Schedule not available
                            </NoRecordsTemplate>
                            <CommandItemSettings AddNewRecordText="Add New Email Schedule" ShowExportToWordButton="false"
                                ShowExportToExcelButton="false" ShowExportToCsvButton="false" ShowExportToPdfButton="false"
                                ShowRefreshButton="false" />
                            <Columns>
                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                    CancelImageUrl="/images/cancel.gif" UpdateImageUrl="/images/update.gif" InsertImageUrl="/images/save.gif"
                                    EditImageUrl="/Images/edit_grid.gif">
                                    <ItemStyle HorizontalAlign="center" CssClass="MyImageButton" Width="90" />
                                </telerik:GridEditCommandColumn>
                                <telerik:GridTemplateColumn UniqueName="title" DataField="title" SortExpression="title"
                                    HeaderText="Title" GroupByExpression="title [Title] group by title">
                                    <ItemTemplate>
                                        <%# Eval("title")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn UniqueName="days" DataType="System.Int32" DataField="days"
                                    SortExpression="days" HeaderText="Days" GroupByExpression="days [Days] group by days">
                                    <ItemTemplate>
                                        <%# Eval("days")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn UniqueName="hours" DataType="System.Int32" DataField="hours"
                                    SortExpression="hours" HeaderText="Hours" GroupByExpression="hours [Hours] group by hours">
                                    <ItemTemplate>
                                        <%# Eval("hours")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Type (Before/After)" SortExpression="schedule_type"
                                    DataField="schedule_type" UniqueName="schedule_type" GroupByExpression="schedule_type [Type (Before/After)] group by schedule_type">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbl_schedule_type" Text='<%# Eval("schedule_type") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FilterTemplate>
                                        <telerik:RadComboBox ID="RadComboBo_Chk_schedule_type" runat="server" OnClientSelectedIndexChanged="ScheduleIndexChanged"
                                            Width="120px" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("schedule_type").CurrentFilterValue %>'>
                                            <Items>
                                                <telerik:RadComboBoxItem Text="All" />
                                                <telerik:RadComboBoxItem Text="Before Start" Value="Before Start" />
                                                <telerik:RadComboBoxItem Text="Before End" Value="Before End" />
                                                <telerik:RadComboBoxItem Text="After End" Value="After End" />
                                            </Items>
                                        </telerik:RadComboBox>
                                        <telerik:RadScriptBlock ID="RadScriptBlock_combo" runat="server">
                                            <script type="text/javascript">
                                                function ScheduleIndexChanged(sender, args) {
                                                    var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                    tableView.filter("schedule_type", args.get_item().get_value(), "Contains");

                                                }
                                            </script>
                                        </telerik:RadScriptBlock>
                                    </FilterTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn UniqueName="is_active" DataType="System.Int32" DataField="is_active"
                                    SortExpression="is_active" HeaderText="Is Active">
                                    <ItemTemplate>
                                        <img src='/Images/<%#IIf(Convert.ToBoolean(Eval("is_active")),"true.gif","false.gif")%>'
                                            alt="" border="none" />
                                    </ItemTemplate>
                                    <FilterTemplate>
                                        <telerik:RadComboBox ID="RadComboBo_Chk_is_active" runat="server" OnClientSelectedIndexChanged="ActiveIndexChanged"
                                            Width="120px" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("is_active").CurrentFilterValue %>'>
                                            <Items>
                                                <telerik:RadComboBoxItem Text="All" Value="" />
                                                <telerik:RadComboBoxItem Text="Active" Value="1" />
                                                <telerik:RadComboBoxItem Text="In-Active" Value="0" />
                                            </Items>
                                        </telerik:RadComboBox>
                                        <telerik:RadScriptBlock ID="RadScriptBlock_is_active" runat="server">
                                            <script type="text/javascript">
                                                function ActiveIndexChanged(sender, args) {
                                                    var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                    tableView.filter("is_active", args.get_item().get_value(), "EqualTo");

                                                }
                                            </script>
                                        </telerik:RadScriptBlock>
                                    </FilterTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"
                                    ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete"
                                    UniqueName="DeleteColumn">
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                </telerik:GridButtonColumn>
                            </Columns>
                            <EditFormSettings EditFormType="Template" EditColumn-ItemStyle-Width="100%" EditColumn-HeaderStyle-HorizontalAlign="Left"
                                EditColumn-ItemStyle-HorizontalAlign="Left">
                                <FormTemplate>
                                    <table cellpadding="0" cellspacing="2" border="0" width="100%">
                                        <tr>
                                            <td style="font-weight: bold; padding-top: 10px;" colspan="4">
                                                Auction Schedule Details
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 110px;">
                                                Title
                                            </td>
                                            <td class="details" style="width: 270px;">
                                                <asp:TextBox ID="txt_title" runat="server" Text='<%# Bind("title") %>' ValidationGroup="val1"
                                                    CssClass="inputtype"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfv_title" runat="server" ErrorMessage="*" CssClass="error"
                                                    ControlToValidate="txt_title" Display="Dynamic" ValidationGroup="val1"></asp:RequiredFieldValidator>
                                            </td>
                                            <td style="width: 145px;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 270px;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 110px;">
                                                Days
                                            </td>
                                            <td class="details" style="width: 270px;">
                                                <asp:TextBox ID="txt_days" runat="server" Text='<%# Bind("days") %>' CssClass="inputtype"
                                                    ValidationGroup="val1"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfv_days" runat="server" ErrorMessage="*" CssClass="error"
                                                    ControlToValidate="txt_days" Display="Dynamic" ValidationGroup="val1"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revdays" runat="server" ControlToValidate="txt_days"
                                                    Display="Dynamic" ValidationExpression="^\d+$" ErrorMessage="Invalid" ValidationGroup="val1"
                                                    CssClass="error"></asp:RegularExpressionValidator>
                                            </td>
                                            <td style="width: 145px;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 270px;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 110px;">
                                                Hours
                                            </td>
                                            <td class="details" style="width: 270px;">
                                                <asp:TextBox ID="txt_hours" runat="server" Text='<%# Bind("hours") %>' CssClass="inputtype"
                                                    ValidationGroup="val1"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfv_hours" runat="server" ErrorMessage="*" CssClass="error"
                                                    ControlToValidate="txt_hours" Display="Dynamic" ValidationGroup="val1"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revHours" runat="server" ControlToValidate="txt_hours"
                                                    Display="Dynamic" ValidationExpression="^\d+$" ErrorMessage="Invalid" ValidationGroup="val1"
                                                    CssClass="error"></asp:RegularExpressionValidator>
                                            </td>
                                            <td style="width: 145px;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 270px;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 110px; text-align: right;">
                                                Type (Before/After)
                                            </td>
                                            <td class="details" style="width: 270px;">
                                                <asp:DropDownList runat="server" ID="ddl_schedule_type" CssClass="inputtype" SelectedValue='<%# getcheckedValue( Eval("schedule_type")) %>'
                                                    Height="25px">
                                                    <asp:ListItem Text="Before Start" Value="Before Start"></asp:ListItem>
                                                    <asp:ListItem Text="Before End" Value="Before End"></asp:ListItem>
                                                    <asp:ListItem Text="After End" Value="After End"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 145px;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 270px;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 110px; text-align: right;">
                                                Is Active
                                            </td>
                                            <td class="details" style="width: 270px;">
                                                <asp:CheckBox ID="chk_is_active" runat="server" Checked='<%# IIF(checkedActive(Eval("is_active")),True,False) %>' />
                                            </td>
                                            <td style="width: 145px;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 270px;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="right">
                                                <asp:ImageButton ID="img_btn_save_comment" runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif", "/images/update.gif") %>'
                                                    CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "PerformInsert", "Update")%>'
                                                    ValidationGroup="val1" />
                                                <asp:ImageButton ID="img_btn_cancel" runat="server" ImageUrl="/images/cancel.gif"
                                                    CausesValidation="false" CommandName="cancel" />
                                            </td>
                                        </tr>
                                    </table>
                                </FormTemplate>
                            </EditFormSettings>
                        </MasterTableView>
                    </telerik:RadGrid>
                </telerik:RadAjaxPanel>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 25px;">
                <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                    EnableAJAX="false">
                    <asp:Panel runat="server" ID="pnl_email_schedule">
                        <div class="pageheading" style="padding-bottom: 10px">
                            Email Queue</div>
                        <telerik:RadGrid ID="RadGrid_email_schedule" runat="server" Skin="Vista" AutoGenerateColumns="False"
                            AllowSorting="True" AllowPaging="false" GridLines="None" Width="100%">
                            <HeaderStyle BackColor="#BEBEBE" />
                            <ItemStyle Font-Size="11px" />
                            <MasterTableView AllowMultiColumnSorting="false" ItemStyle-Height="40" AlternatingItemStyle-Height="40">
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Auction Title">
                                        <ItemTemplate>
                                            <%# Eval("auction_name")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Email Type">
                                        <ItemTemplate>
                                            <%# Eval("email_type")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="# of Emails">
                                        <ItemTemplate>
                                            <%# Eval("no_of_email")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Send">
                                        <ItemTemplate>
                                            <asp:Literal ID="lit_email_schedule_id" runat="server" Text='<%# Eval("email_schedule_id") %>'
                                                Visible="false"></asp:Literal>
                                            <asp:Literal ID="lit_auction_id" runat="server" Text='<%# Eval("auction_id") %>'
                                                Visible="false"></asp:Literal>
                                            <asp:Literal ID="lit_email_type" runat="server" Text='<%# Eval("email_type") %>'
                                                Visible="false"></asp:Literal>
                                            <asp:CheckBox ID="chk_select" runat="server" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                        <div style="float: right; padding: 10px 0px;">
                            <asp:ImageButton runat="server" ID="btn_send_email" AlternateText="Send Emails"
                            ImageUrl="/images/sendmail.png" OnClientClick="javascript:ShowWait();" CausesValidation="false" />
                            <img id="wait" src="/Images/waitmailsending.gif" border='none' alt="Wait" style="display: none;" />
                        </div>
                    </asp:Panel>
                    <asp:Label ID="lbl_msg" runat="server" ForeColor="Red" Text="Email send successfully"
                        Visible="false"></asp:Label>
                </telerik:RadAjaxPanel>
            </td>
        </tr>
    </table>
</asp:Content>
