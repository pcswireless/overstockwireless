﻿<%@ Page Title="PCS Bidding" Language="VB" MasterPageFile="~/BackendPopUP.master" AutoEventWireup="false" 
    CodeFile="UploadImage.aspx.vb" Inherits="Auctions_UploadImage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table cellpadding="10" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    <asp:Literal ID="page_heading" runat="server"></asp:Literal>
                    <asp:Literal ID="ltrl_current_status" runat="server"></asp:Literal>
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table cellpadding="5">
                    <tr>
                        <td class="caption">
                            Attach Image
                        </td>
                        <td>
                            <%--<asp:FileUpload ID="FileUpload1" runat="server" CssClass="TextBox" Width="180" size="15" />--%>
                            <input type="file" id="FLUN_Default_Small_lmage" runat="server" class="textbox" width="100"
                                        name="FLUN_Default_Small_lmage" onkeydown="return false;" />
                         </td>
                        <td>
                            <asp:ImageButton ID="btn_Image_Upload" runat="server" AlternateText="Update" ImageUrl="/images/save.gif"
                                ValidationGroup="DV2" />
                        </td>
                        <td>
                            <asp:ImageButton ID="ImageButton1" OnClientClick="javascript:window.close();" runat="server"
                                AlternateText="Close" ImageUrl="/images/close.gif" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                             <asp:RequiredFieldValidator ID="req_image" runat="server" InitialValue="" ValidationGroup="DV2"
                                ControlToValidate="FLUN_Default_Small_lmage" ErrorMessage="Image is Required" Display="Dynamic"></asp:RequiredFieldValidator>
                      </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_msg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lbl_no_image" runat="server" ForeColor="Red" Text="Image not uploaded"></asp:Label>
                <asp:DataList ID="grid" runat="server" ShowHeader="false" BackColor="#f7f7f7" DataKeyField="stock_image_id"
                    RepeatColumns="8" CellPadding="0" RepeatDirection="horizontal">
                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Justify" CssClass="rightBorder" />
                    <AlternatingItemStyle VerticalAlign="Top" HorizontalAlign="Justify" CssClass="noRightBorder" />
                    <ItemTemplate>
                        <table border="0">
                            <tr>
                                <td height="10" width="80" align="center">
                                    <a href="UploadImage.aspx?i=<%=Request.QueryString.Get("i") %>&d=<%#Container.Dataitem("stock_image_id")%>"
                                        onclick="return confirm('Are you sure you want to delete this Image?')">
                                        <img src="/images/delete_grid.gif" border="0" alt="" /></a>
                                    <asp:DropDownList ID="ddl_sno" runat="server" Font-Size="6" ToolTip='<%# Container.dataitem("position")%>'
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                    <asp:Label ID="lbl_position" runat="server" Text='<%# Container.dataitem("position")%>'
                                        Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td height="80" align="center">
                                    <a href="#" onclick="return openGallaryWindow();return false;">
                                        <img id="img_product" src="/Upload/Auctions/stock_images/<%#Container.Dataitem("auction_id") %>/<%#Container.Dataitem("stock_image_id") %>/<%#Container.Dataitem("filename") %>"
                                            alt="" width="100" height="120" style="border: 1px solid gray" /></a>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle BackColor="#FBFBFB" />
                    <AlternatingItemStyle BackColor="#FBFBFB" />
                </asp:DataList>
            </td>
        </tr>
    </table>
</asp:Content>

