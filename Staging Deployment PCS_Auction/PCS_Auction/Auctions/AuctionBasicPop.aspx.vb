﻿
Partial Class Auctions_AuctionBasicPop
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            fillOtherDetailsData(Request.QueryString.Get("i"))
            managemode()
            managetype()
        End If
    End Sub
    Private Sub managetype()
        If Request.QueryString.Get("type") = "d" Or Request.QueryString.Get("type") = "h" Then
            tr_attachment.Visible = False
            tr_attachment_detail.Visible = False
            tr_detail.Visible = True
        ElseIf Request.QueryString.Get("type") = "m" Or Request.QueryString.Get("type") = "l" Then
            tr_detail.Visible = False
            tr_attachment.Visible = True
            tr_attachment_detail.Visible = True
        Else
            tr_detail.Visible = True
            tr_attachment.Visible = True
            tr_attachment_detail.Visible = True
        End If
    End Sub
    Private Sub managemode()
        'If opt.ToLower = "v" Then
        '    txt_control1.Visible = False
        '    lbl_control1.Visible = True
        '    lbl_control2.Visible = True
        '    FileUpload1.Visible = False
        '    btn_other_update.Visible = False
        '    div_cancel.Visible = False
        '    remove_check.Visible = False
        '    btn_other_edit.Visible = True
        'Else
        ' btn_other_cancel.Visible = False

        txt_control1.Visible = True
        lbl_control1.Visible = False
        lbl_control2.Visible = True
        FileUpload1.Visible = True
        btn_other_update.Visible = True

        'btn_other_edit.Visible = False
        If lbl_control2.Text <> "" Then
            remove_check.Visible = True
        Else
            remove_check.Visible = False
        End If

        'End If
    End Sub


    Private Sub fillOtherDetailsData(ByVal auction_id As Integer)

        If auction_id > 0 Then

            Dim dt As DataTable = New DataTable()
            Dim qry As String = ""
            If Request.QueryString.Get("type") = "m" Then
                lbl_caption1.Text = "Product Item"
                lbl_caption2.Text = "Attach Product Item"
                qry = "select top 1 product_item_attachment_id,isnull(filename,'') as filename from tbl_auction_product_item_attachments where auction_id=" & auction_id & " order by product_item_attachment_id desc"
            ElseIf Request.QueryString.Get("type") = "l" Then
                lbl_caption1.Text = "After Launch"
                lbl_caption2.Text = "Attach After Launch"
                qry = "SELECT A.auction_id, " & _
               " isnull(A.after_launch_filename,'') as after_launch_filename FROM tbl_auctions A WITH (NOLOCK) WHERE A.auction_id = " & auction_id
            Else
                qry = "SELECT A.auction_id, " & _
                             "case " & Request.QueryString.Get("l") & " when 2 then ISNULL(A.description_f, '') when 3 then ISNULL(A.description_s, '') when 4 then ISNULL(A.description_c, '') else ISNULL(A.description, '') end AS description," & _                             "case " & Request.QueryString.Get("l") & " when 2 then ISNULL(A.special_conditions_f, '') when 3 then ISNULL(A.special_conditions_s, '') when 4 then ISNULL(A.special_conditions_c, '') else ISNULL(A.special_conditions, '') end AS special_conditions," & _                             "case " & Request.QueryString.Get("l") & " when 2 then ISNULL(A.payment_terms_f, '') when 3 then ISNULL(A.payment_terms_s, '') when 4 then ISNULL(A.payment_terms_c, '') else ISNULL(A.payment_terms, '') end AS payment_terms," & _                             "case " & Request.QueryString.Get("l") & " when 2 then ISNULL(A.shipping_terms_f, '') when 3 then ISNULL(A.shipping_terms_s, '') when 4 then ISNULL(A.shipping_terms_c, '') else ISNULL(A.shipping_terms, '') end AS shipping_terms," & _                             "case " & Request.QueryString.Get("l") & " when 2 then ISNULL(A.other_terms_f, '') when 3 then ISNULL(A.other_terms_s, '') when 4 then  ISNULL(A.other_terms_c, '') else ISNULL(A.other_terms, '') end AS other_terms," & _                             "case " & Request.QueryString.Get("l") & " when 2 then ISNULL(A.short_description_f, '') when 3 then ISNULL(A.short_description_s, '') when 4 then  ISNULL(A.short_description_c, '') else ISNULL(A.short_description, '') end AS short_description," & _                             "case " & Request.QueryString.Get("l") & " when 2 then ISNULL(A.tax_details_f, '') when 3 then ISNULL(A.tax_details_s, '') when 4 then  ISNULL(A.tax_details_c, '') else ISNULL(A.tax_details, '') end AS tax_details," & _                             "case " & Request.QueryString.Get("l") & " when 2 then ISNULL(A.product_details_f, '') when 3 then ISNULL(A.product_details_s, '') when 4 then  ISNULL(A.product_details_c, '') else ISNULL(A.product_details, '') end AS product_details," & _                             " isnull(A.after_launch_filename,'') as after_launch_filename FROM tbl_auctions A WITH (NOLOCK) WHERE A.auction_id = " & auction_id

            End If

            dt = SqlHelper.ExecuteDataTable(qry)


            If dt.Rows.Count > 0 Then
                With dt.Rows(0)

                    If Request.QueryString.Get("type") = "d" Then
                        lbl_caption1.Text = "Description"
                        lbl_control1.Text = CommonCode.decodeSingleQuote(.Item("description"))
                        txt_control1.Content = .Item("description")
                    ElseIf Request.QueryString.Get("type") = "c" Then
                        lbl_caption1.Text = "Special Conditions"
                        lbl_caption2.Text = "Attach Special Conditions"
                        lbl_control1.Text = CommonCode.decodeSingleQuote(.Item("special_conditions"))
                        txt_control1.Content = .Item("special_conditions")
                    ElseIf Request.QueryString.Get("type") = "p" Then
                        lbl_caption1.Text = "Payment Terms"
                        lbl_caption2.Text = "Attach Payment Terms"
                        lbl_control1.Text = CommonCode.decodeSingleQuote(.Item("payment_terms"))
                        txt_control1.Content = .Item("payment_terms")
                    ElseIf Request.QueryString.Get("type") = "s" Then
                        lbl_caption1.Text = "Shipping Terms"
                        lbl_caption2.Text = "Attach Shipping Terms"
                        lbl_control1.Text = CommonCode.decodeSingleQuote(.Item("shipping_terms"))
                        txt_control1.Content = .Item("shipping_terms")
                    ElseIf Request.QueryString.Get("type") = "o" Then
                        lbl_caption1.Text = "Other Terms"
                        lbl_caption2.Text = "Attach Other Terms"
                        lbl_control1.Text = CommonCode.decodeSingleQuote(.Item("other_terms"))
                        txt_control1.Content = .Item("other_terms")
                    ElseIf Request.QueryString.Get("type") = "h" Then
                        lbl_caption1.Text = "Short Description"
                        lbl_control1.Text = CommonCode.decodeSingleQuote(.Item("short_description"))
                        txt_control1.Content = .Item("short_description")
                    ElseIf Request.QueryString.Get("type") = "t" Then
                        lbl_caption1.Text = "Tax Details"
                        lbl_caption2.Text = "Attach Tax Details"
                        lbl_control1.Text = CommonCode.decodeSingleQuote(.Item("tax_details"))
                        txt_control1.Content = .Item("tax_details")
                    ElseIf Request.QueryString.Get("type") = "r" Then
                        lbl_caption1.Text = "Product Details"
                        lbl_caption2.Text = "Attach Product Details"
                        lbl_control1.Text = CommonCode.decodeSingleQuote(.Item("product_details"))
                        txt_control1.Content = .Item("product_details")

                    End If

                End With

                If Request.QueryString.Get("type") <> "d" And Request.QueryString.Get("type") <> "h" Then

                    Dim dt1 As DataTable = New DataTable()
                    Dim str As String = ""
                    If Request.QueryString.Get("type") = "p" Then
                        str = "select top 1 payment_term_attachment_id,case " & Request.QueryString.Get("l") & " when 2 then filename_f when 3 then filename_s when 4 then filename_c else filename end as filename from tbl_auction_payment_term_attachments where auction_id=" & auction_id & " order by payment_term_attachment_id desc"
                        dt1 = SqlHelper.ExecuteDataTable(str)
                        If dt1.Rows.Count > 0 Then
                            With dt1.Rows(0)
                                HID_Upload.Value = dt1.Rows(0)("payment_term_attachment_id")
                                lbl_control2.Text = IIf(String.IsNullOrEmpty(dt1.Rows(0)("filename")), "", "<a href='/Upload/Auctions/payment_term_attachments/" & auction_id & "/" & dt1.Rows(0)("payment_term_attachment_id") & "/" & dt1.Rows(0)("filename") & "' target='_blank'>" & dt1.Rows(0)("filename") & "</a>")
                                remove_check.Visible = IIf(String.IsNullOrEmpty(dt1.Rows(0)("filename")), False, True)
                            End With
                        Else
                            HID_Upload.Value = "0"
                        End If
                        dt1 = Nothing
                    ElseIf Request.QueryString.Get("type") = "s" Then
                        str = "select top 1 shipping_term_attachment_id,case " & Request.QueryString.Get("l") & " when 2 then filename_f when 3 then filename_s when 4 then filename_c else filename end as filename from tbl_auction_shipping_term_attachments where auction_id=" & auction_id & " order by shipping_term_attachment_id desc"
                        dt1 = SqlHelper.ExecuteDataTable(str)
                        If dt1.Rows.Count > 0 Then
                            With dt1.Rows(0)
                                HID_Upload.Value = dt1.Rows(0)("shipping_term_attachment_id")
                                lbl_control2.Text = IIf(String.IsNullOrEmpty(dt1.Rows(0)("filename")), "", "<a href='/Upload/Auctions/shipping_term_attachments/" & auction_id & "/" & dt1.Rows(0)("shipping_term_attachment_id") & "/" & dt1.Rows(0)("filename") & "' target='_blank'>" & dt1.Rows(0)("filename") & "</a>")
                                remove_check.Visible = IIf(String.IsNullOrEmpty(dt1.Rows(0)("filename")), False, True)
                            End With
                        Else
                            HID_Upload.Value = "0"
                        End If
                        dt1 = Nothing
                    ElseIf Request.QueryString.Get("type") = "o" Then
                        str = "select top 1 term_cond_attachment_id,case " & Request.QueryString.Get("l") & " when 2 then filename_f when 3 then filename_s when 4 then filename_c else filename end as filename from tbl_auction_terms_cond_attachments where auction_id=" & auction_id & " order by term_cond_attachment_id desc"
                        dt1 = SqlHelper.ExecuteDataTable(str)
                        If dt1.Rows.Count > 0 Then
                            With dt1.Rows(0)
                                HID_Upload.Value = dt1.Rows(0)("term_cond_attachment_id")
                                lbl_control2.Text = IIf(String.IsNullOrEmpty(dt1.Rows(0)("filename")), "", "<a href='/Upload/Auctions/terms_cond_attachments/" & auction_id & "/" & dt1.Rows(0)("term_cond_attachment_id") & "/" & dt1.Rows(0)("filename") & "' target='_blank'>" & dt1.Rows(0)("filename") & "</a>")
                                remove_check.Visible = IIf(String.IsNullOrEmpty(dt1.Rows(0)("filename")), False, True)
                            End With
                        Else
                            HID_Upload.Value = "0"
                        End If
                        dt1 = Nothing
                    ElseIf Request.QueryString.Get("type") = "t" Then
                        str = "select top 1 tax_attachment_id,isnull(case " & Request.QueryString.Get("l") & " when 2 then filename_f when 3 then filename_s when 4 then filename_c else filename end,'') as filename from tbl_auction_tax_attachments where auction_id=" & auction_id & " order by tax_attachment_id desc"
                        dt1 = SqlHelper.ExecuteDataTable(str)
                        If dt1.Rows.Count > 0 Then
                            With dt1.Rows(0)
                                HID_Upload.Value = dt1.Rows(0)("tax_attachment_id")
                                lbl_control2.Text = IIf(String.IsNullOrEmpty(dt1.Rows(0)("filename")), "", "<a href='/Upload/Auctions/tax_attachments/" & auction_id & "/" & dt1.Rows(0)("tax_attachment_id") & "/" & dt1.Rows(0)("filename") & "' target='_blank'>" & dt1.Rows(0)("filename") & "</a>")
                                remove_check.Visible = IIf(String.IsNullOrEmpty(dt1.Rows(0)("filename")), False, True)
                            End With
                        Else
                            HID_Upload.Value = "0"
                        End If
                        dt1 = Nothing
                    ElseIf Request.QueryString.Get("type") = "r" Then
                        str = "select top 1 product_attachment_id,isnull(case " & Request.QueryString.Get("l") & " when 2 then filename_f when 3 then filename_s when 4 then filename_c else filename end,'') as filename from tbl_auction_product_attachments where auction_id=" & auction_id & " order by product_attachment_id desc"
                        dt1 = SqlHelper.ExecuteDataTable(str)
                        If dt1.Rows.Count > 0 Then
                            With dt1.Rows(0)
                                HID_Upload.Value = dt1.Rows(0)("product_attachment_id")
                                lbl_control2.Text = IIf(String.IsNullOrEmpty(dt1.Rows(0)("filename")), "", "<a href='/Upload/Auctions/product_attachments/" & auction_id & "/" & dt1.Rows(0)("product_attachment_id") & "/" & dt1.Rows(0)("filename") & "' target='_blank'>" & dt1.Rows(0)("filename") & "</a>")
                                remove_check.Visible = IIf(String.IsNullOrEmpty(dt1.Rows(0)("filename")), False, True)
                            End With
                        Else
                            HID_Upload.Value = "0"
                        End If
                        dt1 = Nothing
                    ElseIf Request.QueryString.Get("type") = "c" Then
                        str = "select top 1 special_condition_id,isnull(case " & Request.QueryString.Get("l") & " when 2 then filename_f when 3 then filename_s when 4 then filename_c else filename end,'') as filename from tbl_auction_special_conditions where auction_id=" & auction_id & " order by special_condition_id desc"
                        dt1 = SqlHelper.ExecuteDataTable(str)
                        If dt1.Rows.Count > 0 Then
                            With dt1.Rows(0)
                                HID_Upload.Value = dt1.Rows(0)("special_condition_id")
                                lbl_control2.Text = IIf(String.IsNullOrEmpty(dt1.Rows(0)("filename")), "", "<a href='/Upload/Auctions/special_conditions/" & auction_id & "/" & dt1.Rows(0)("special_condition_id") & "/" & dt1.Rows(0)("filename") & "' target='_blank'>" & dt1.Rows(0)("filename") & "</a>")
                                remove_check.Visible = IIf(String.IsNullOrEmpty(dt1.Rows(0)("filename")), False, True)
                            End With
                        Else
                            HID_Upload.Value = "0"
                        End If
                        dt1 = Nothing
                    ElseIf Request.QueryString.Get("type") = "m" Then
                        HID_Upload.Value = dt.Rows(0)("product_item_attachment_id")
                        lbl_control2.Text = IIf(String.IsNullOrEmpty(dt.Rows(0)("filename")), "", "<a href='/Upload/Auctions/product_items/" & auction_id & "/" & dt.Rows(0)("product_item_attachment_id") & "/" & dt.Rows(0)("filename") & "' target='_blank'>" & dt.Rows(0)("filename") & "</a>")
                        lit_for_product_item.Text = "<br><br>"
                        remove_check.Visible = IIf(String.IsNullOrEmpty(dt.Rows(0)("filename")), False, True)
                    ElseIf Request.QueryString.Get("type") = "l" Then
                        HID_Upload.Value = "0"
                        lbl_control2.Text = IIf(String.IsNullOrEmpty(dt.Rows(0)("after_launch_filename")), "", "<a href='/Upload/Auctions/after_launch/" & auction_id & "/" & dt.Rows(0)("after_launch_filename") & "' target='_blank'>" & dt.Rows(0)("after_launch_filename") & "</a>")
                        lit_for_product_item.Text = "<br><br>"
                        remove_check.Visible = IIf(String.IsNullOrEmpty(dt.Rows(0)("after_launch_filename")), False, True)
                    End If

                    'Response.Write(str & "<br><br>")
                End If
            Else
                HID_Upload.Value = "0"
            End If
        End If
    End Sub

    Protected Sub btn_other_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_other_update.Click
        ' If Page.IsValid Then

        Dim updpara As String = ""
        If Request.QueryString.Get("type") = "d" Then
            updpara = "description=case " & Request.QueryString.Get("l") & " when 1 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else description end,description_f=case " & Request.QueryString.Get("l") & " when 2 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else description_f end,description_s=case " & Request.QueryString.Get("l") & " when 3 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else description_s end,description_c=case " & Request.QueryString.Get("l") & " when 4 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else description_c end"
            CommonCode.insert_system_log("Auction description has been updated.", "", Request.QueryString("i"), "", "Auction")
        ElseIf Request.QueryString.Get("type") = "c" Then
            updpara = "special_conditions=case " & Request.QueryString.Get("l") & " when 1 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' end,special_conditions_f=case " & Request.QueryString.Get("l") & " when 2 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else special_conditions_f end,special_conditions_s=case " & Request.QueryString.Get("l") & " when 3 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else special_conditions_s end,special_conditions_c=case " & Request.QueryString.Get("l") & " when 4 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else special_conditions_c end"
            UploadFile(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
            CommonCode.insert_system_log("Auction special condition has been updated.", "UploadFile", Request.QueryString("i"), "", "Auction")
        ElseIf Request.QueryString.Get("type") = "p" Then
            updpara = "payment_terms=case " & Request.QueryString.Get("l") & " when 1 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else payment_terms end,payment_terms_f=case " & Request.QueryString.Get("l") & " when 2 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else payment_terms_f end,payment_terms_s=case " & Request.QueryString.Get("l") & " when 3 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else payment_terms_s end,payment_terms_c=case " & Request.QueryString.Get("l") & " when 4 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else payment_terms_c end"
            UploadFile(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
            CommonCode.insert_system_log("Auction payment terms has been updated.", "UploadFile", Request.QueryString("i"), "", "Auction")
        ElseIf Request.QueryString.Get("type") = "s" Then
            updpara = "shipping_terms=case " & Request.QueryString.Get("l") & " when 1 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else shipping_terms end,shipping_terms_f=case " & Request.QueryString.Get("l") & " when 2 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else shipping_terms_f end,shipping_terms_s=case " & Request.QueryString.Get("l") & " when 3 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else shipping_terms_s end,shipping_terms_c=case " & Request.QueryString.Get("l") & " when 4 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else shipping_terms_c end"
            UploadFile(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
            CommonCode.insert_system_log("Auction shipping terms has been updated.", "UploadFile", Request.QueryString("i"), "", "Auction")
        ElseIf Request.QueryString.Get("type") = "o" Then
            updpara = "other_terms=case " & Request.QueryString.Get("l") & " when 1 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else other_terms end,other_terms_f=case " & Request.QueryString.Get("l") & " when 2 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else other_terms_f end,other_terms_s=case " & Request.QueryString.Get("l") & " when 3 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else other_terms_s end,other_terms_c=case " & Request.QueryString.Get("l") & " when 4 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else other_terms_c end"
            UploadFile(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
            CommonCode.insert_system_log("Auction other terms has been updated.", "UploadFile", Request.QueryString("i"), "", "Auction")
        ElseIf Request.QueryString.Get("type") = "h" Then
            'If txt_control1.Content.Trim.Length <= 300 Then
            updpara = "short_description=case " & Request.QueryString.Get("l") & " when 1 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else short_description end,short_description_f=case " & Request.QueryString.Get("l") & " when 2 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else short_description_f end,short_description_s=case " & Request.QueryString.Get("l") & " when 3 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else short_description_s end,short_description_c=case " & Request.QueryString.Get("l") & " when 4 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else short_description_c end"
            CommonCode.insert_system_log("Auction short description has been updated.", "", Request.QueryString("i"), "", "Auction")
            'End If

        ElseIf Request.QueryString.Get("type") = "t" Then
            updpara = "tax_details=case " & Request.QueryString.Get("l") & " when 1 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else tax_details end,tax_details_f=case " & Request.QueryString.Get("l") & " when 2 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else tax_details_f end,tax_details_s=case " & Request.QueryString.Get("l") & " when 3 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else tax_details_s end,tax_details_c=case " & Request.QueryString.Get("l") & " when 4 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else tax_details_c end"
            UploadFile(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
            CommonCode.insert_system_log("Auction tax details has been updated.", "UploadFile", Request.QueryString("i"), "", "Auction")
        ElseIf Request.QueryString.Get("type") = "r" Then
            updpara = "product_details=case " & Request.QueryString.Get("l") & " when 1 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else product_details end,product_details_f=case " & Request.QueryString.Get("l") & " when 2 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else product_details_f end,product_details_s=case " & Request.QueryString.Get("l") & " when 3 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else product_details_s end,product_details_c=case " & Request.QueryString.Get("l") & " when 4 then '" & txt_control1.Content.Trim.Replace("'", "''") & "' else product_details_c end"
            UploadFile(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
            CommonCode.insert_system_log("Auction product details has been updated.", "UploadFile", Request.QueryString("i"), "", "Auction")

        End If
        If Request.QueryString.Get("type") = "m" Then
            UploadFile(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
            CommonCode.insert_system_log("Auction product items has been updated.", "", Request.QueryString("i"), "", "Auction")
        ElseIf Request.QueryString.Get("type") = "l" Then
            UploadFile(IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")))
            CommonCode.insert_system_log("Auction attachment of after launch has been updated.", "", Request.QueryString("i"), "", "Auction")
        Else
            updpara = updpara & ",update_date=getdate(),update_by_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
            updpara = "update tbl_auctions set " & updpara & " where auction_id=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i"))
        End If



        'txt_control1.Content = updpara
        'Response.Write(updpara)
        SqlHelper.ExecuteNonQuery(updpara)
        Select Case Request.QueryString.Get("type").ToLower
            Case "d", "r"
                Response.Write("<script>window.opener.tab_PostBackFunction();window.close();</script>")
            Case "h"
                'Response.Write(txt_control1.Content.Trim.Length & "--" & txt_control1.Content)
                'If txt_control1.Content.Trim.Length <= 300 Then
                Response.Write("<script>window.opener.tab_PostBackFunction();window.close();</script>")
                'Else
                'lbl_msg.Text = "Short description is too long."
                'End If
            Case "m"
                Response.Write("<script>window.opener.redirectIframe('','','/Auctions/AddEditAuction.aspx?t=2&i=" & Request.QueryString.Get("i") & "');window.close();</script>")
            Case "l"
                Response.Write("<script>window.opener.redirectIframe('','','/Auctions/AddEditAuction.aspx?t=6&i=" & Request.QueryString.Get("i") & "');window.close();</script>")
            Case Else
                Response.Write("<script>window.opener.tab_PostBackFunction_terms();window.close();</script>")
        End Select

        'Response.Write("<script>window.close();opener.location.reload();</script>")  'redirectIframe('','','/Auctions/AddEditAuction.aspx?t=2&i=<%=Request.QueryString.Get("a") %>#2')
        'End If
    End Sub

    Private Sub UploadFile(ByVal auction_id As Integer)
        Try
            Dim filename As String = ""
            Dim pathToCreate As String = ""
            Dim str As String = ""
            Dim str1 As String = ""
            If remove_check.Checked And HID_Upload.Value.ToString <> "0" Then
                If Request.QueryString.Get("type") = "p" Then
                    str = "update tbl_auction_payment_term_attachments set filename=case " & Request.QueryString.Get("l") & " when 1 then '' else filename end,filename_f=case " & Request.QueryString.Get("l") & " when 2 then '' else filename_f end,filename_s=case " & Request.QueryString.Get("l") & " when 3 then '' else filename_s end,filename_c=case " & Request.QueryString.Get("l") & " when 4 then '' else filename_c end where payment_term_attachment_id=" & HID_Upload.Value
                    str1 = "select case " & Request.QueryString.Get("l") & " when 2 then filename_f when 3 then filename_s when 4 then filename_c else filename end as filename from tbl_auction_payment_term_attachments where payment_term_attachment_id=" & HID_Upload.Value & ""
                    pathToCreate = "../Upload/Auctions/payment_term_attachments"

                ElseIf Request.QueryString.Get("type") = "s" Then
                    str = "update tbl_auction_shipping_term_attachments set filename=case " & Request.QueryString.Get("l") & " when 1 then '' else filename end,filename_f=case " & Request.QueryString.Get("l") & " when 2 then '' else filename_f end,filename_s=case " & Request.QueryString.Get("l") & " when 3 then '' else filename_s end,filename_c=case " & Request.QueryString.Get("l") & " when 4 then '' else filename_c end where shipping_term_attachment_id=" & HID_Upload.Value
                    str1 = "select case " & Request.QueryString.Get("l") & " when 2 then filename_f when 3 then filename_s when 4 then filename_c else filename end as filename from tbl_auction_shipping_term_attachments where shipping_term_attachment_id=" & HID_Upload.Value & ""
                    pathToCreate = "../Upload/Auctions/shipping_term_attachments"
                ElseIf Request.QueryString.Get("type") = "o" Then
                    str = "update tbl_auction_terms_cond_attachments set filename=case " & Request.QueryString.Get("l") & " when 1 then '' else filename end,filename_f=case " & Request.QueryString.Get("l") & " when 2 then '' else filename_f end,filename_s=case " & Request.QueryString.Get("l") & " when 3 then '' else filename_s end,filename_c=case " & Request.QueryString.Get("l") & " when 4 then '' else filename_c end where term_cond_attachment_id=" & HID_Upload.Value
                    str1 = "select case " & Request.QueryString.Get("l") & " when 2 then filename_f when 3 then filename_s when 4 then filename_c else filename end as filename from tbl_auction_terms_cond_attachments where term_cond_attachment_id=" & HID_Upload.Value & ""
                    pathToCreate = "../Upload/Auctions/terms_cond_attachments"
                ElseIf Request.QueryString.Get("type") = "t" Then
                    str = "update tbl_auction_tax_attachments set filename=case " & Request.QueryString.Get("l") & " when 1 then '' else filename end,filename_f=case " & Request.QueryString.Get("l") & " when 2 then '' else filename_f end,filename_s=case " & Request.QueryString.Get("l") & " when 3 then '' else filename_s end,filename_c=case " & Request.QueryString.Get("l") & " when 4 then '' else filename_c end where tax_attachment_id=" & HID_Upload.Value
                    str1 = "select case " & Request.QueryString.Get("l") & " when 2 then filename_f when 3 then filename_s when 4 then filename_c else filename end as filename from tbl_auction_tax_attachments where tax_attachment_id=" & HID_Upload.Value & ""
                    pathToCreate = "../Upload/Auctions/tax_attachments"
                ElseIf Request.QueryString.Get("type") = "r" Then
                    str = "update tbl_auction_product_attachments set filename=case " & Request.QueryString.Get("l") & " when 1 then '' else filename end,filename_f=case " & Request.QueryString.Get("l") & " when 2 then '' else filename_f end,filename_s=case " & Request.QueryString.Get("l") & " when 3 then '' else filename_s end,filename_c=case " & Request.QueryString.Get("l") & " when 4 then '' else filename_c end where product_attachment_id=" & HID_Upload.Value
                    str1 = "select case " & Request.QueryString.Get("l") & " when 2 then filename_f when 3 then filename_s when 4 then filename_c else filename end as filename from tbl_auction_product_attachments where product_attachment_id=" & HID_Upload.Value & ""
                    pathToCreate = "../Upload/Auctions/product_attachments"
                ElseIf Request.QueryString.Get("type") = "c" Then
                    str = "update tbl_auction_special_conditions set filename=case " & Request.QueryString.Get("l") & " when 1 then '' else filename end,filename_f=case " & Request.QueryString.Get("l") & " when 2 then '' else filename_f end,filename_s=case " & Request.QueryString.Get("l") & " when 3 then '' else filename_s end,filename_c=case " & Request.QueryString.Get("l") & " when 4 then '' else filename_c end where special_condition_id=" & HID_Upload.Value
                    str1 = "select case " & Request.QueryString.Get("l") & " when 2 then filename_f when 3 then filename_s when 4 then filename_c else filename end as filename from tbl_auction_special_conditions where special_condition_id=" & HID_Upload.Value & ""
                    pathToCreate = "../Upload/Auctions/special_conditions"
                ElseIf Request.QueryString.Get("type") = "m" Then
                    str = "update tbl_auction_product_item_attachments set filename='' where product_item_attachment_id=" & HID_Upload.Value
                    str1 = "select isnull(filename,'') as filename from tbl_auction_product_item_attachments where product_item_attachment_id=" & HID_Upload.Value & ""
                    pathToCreate = "../Upload/Auctions/product_items"

                End If
                filename = SqlHelper.ExecuteScalar(str1)
                SqlHelper.ExecuteNonQuery(str)
                Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & auction_id.ToString() & "/" & HID_Upload.Value & "/" & filename)
                If infoFile.Exists Then
                    System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & auction_id.ToString() & "/" & HID_Upload.Value & "/" & filename)
                End If
            ElseIf remove_check.Checked And Request.QueryString.Get("type") = "l" Then
                str = "update tbl_auctions set after_launch_filename='' where auction_id=" & auction_id
                str1 = "select isnull(after_launch_filename,'') as filename from tbl_auctions WITH (NOLOCK) where auction_id=" & auction_id & ""
                pathToCreate = "../Upload/Auctions/after_launch"
                filename = SqlHelper.ExecuteScalar(str1)
                SqlHelper.ExecuteNonQuery(str)
                Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & auction_id.ToString() & "/" & filename)
                If infoFile.Exists Then
                    System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & auction_id.ToString() & "/" & filename)
                End If
            End If

            If FileUpload1.HasFile Then

                filename = System.IO.Path.GetFileName(FileUpload1.FileName)
                If HID_Upload.Value.ToString <> "0" Then

                    If Request.QueryString.Get("type") = "p" Then
                        'Response.Write(auction_id & filename)
                        str = "update tbl_auction_payment_term_attachments set filename=case " & Request.QueryString.Get("l") & " when 1 then '" & filename & "' else filename end,filename_f=case " & Request.QueryString.Get("l") & " when 2 then '" & filename & "' else filename_f end,filename_s=case " & Request.QueryString.Get("l") & " when 3 then '" & filename & "' else filename_s end,filename_c=case " & Request.QueryString.Get("l") & " when 4 then '" & filename & "' else filename_c end where payment_term_attachment_id=" & HID_Upload.Value
                        pathToCreate = "../Upload/Auctions/payment_term_attachments"
                    ElseIf Request.QueryString.Get("type") = "s" Then
                        str = "update tbl_auction_shipping_term_attachments set filename=case " & Request.QueryString.Get("l") & " when 1 then '" & filename & "' else filename end,filename_f=case " & Request.QueryString.Get("l") & " when 2 then '" & filename & "' else filename_f end,filename_s=case " & Request.QueryString.Get("l") & " when 3 then '" & filename & "' else filename_s end,filename_c=case " & Request.QueryString.Get("l") & " when 4 then '" & filename & "' else filename_c end where shipping_term_attachment_id=" & HID_Upload.Value
                        pathToCreate = "../Upload/Auctions/shipping_term_attachments"
                    ElseIf Request.QueryString.Get("type") = "o" Then
                        str = "update tbl_auction_terms_cond_attachments set filename=case " & Request.QueryString.Get("l") & " when 1 then '" & filename & "' else filename end,filename_f=case " & Request.QueryString.Get("l") & " when 2 then '" & filename & "' else filename_f end,filename_s=case " & Request.QueryString.Get("l") & " when 3 then '" & filename & "' else filename_s end,filename_c=case " & Request.QueryString.Get("l") & " when 4 then '" & filename & "' else filename_c end where term_cond_attachment_id=" & HID_Upload.Value
                        pathToCreate = "../Upload/Auctions/terms_cond_attachments"
                    ElseIf Request.QueryString.Get("type") = "t" Then
                        str = "update tbl_auction_tax_attachments set filename=case " & Request.QueryString.Get("l") & " when 1 then '" & filename & "' else filename end,filename_f=case " & Request.QueryString.Get("l") & " when 2 then '" & filename & "' else filename_f end,filename_s=case " & Request.QueryString.Get("l") & " when 3 then '" & filename & "' else filename_s end,filename_c=case " & Request.QueryString.Get("l") & " when 4 then '" & filename & "' else filename_c end where tax_attachment_id=" & HID_Upload.Value
                        pathToCreate = "../Upload/Auctions/tax_attachments"
                    ElseIf Request.QueryString.Get("type") = "r" Then
                        str = "update tbl_auction_product_attachments set filename=case " & Request.QueryString.Get("l") & " when 1 then '" & filename & "' else filename end,filename_f=case " & Request.QueryString.Get("l") & " when 2 then '" & filename & "' else filename_f end,filename_s=case " & Request.QueryString.Get("l") & " when 3 then '" & filename & "' else filename_s end,filename_c=case " & Request.QueryString.Get("l") & " when 4 then '" & filename & "' else filename_c end where product_attachment_id=" & HID_Upload.Value
                        pathToCreate = "../Upload/Auctions/product_attachments"
                    ElseIf Request.QueryString.Get("type") = "c" Then
                        str = "update tbl_auction_special_conditions set filename=case " & Request.QueryString.Get("l") & " when 1 then '" & filename & "' else filename end,filename_f=case " & Request.QueryString.Get("l") & " when 2 then '" & filename & "' else filename_f end,filename_s=case " & Request.QueryString.Get("l") & " when 3 then '" & filename & "' else filename_s end,filename_c=case " & Request.QueryString.Get("l") & " when 4 then '" & filename & "' else filename_c end where special_condition_id=" & HID_Upload.Value
                        pathToCreate = "../Upload/Auctions/special_conditions"
                    ElseIf Request.QueryString.Get("type") = "m" Then
                        str = "update tbl_auction_product_item_attachments set filename='" & filename & "' where product_item_attachment_id=" & HID_Upload.Value
                        pathToCreate = "../Upload/Auctions/product_items"
                    ElseIf Request.QueryString.Get("type") = "l" Then
                        str = "update tbl_auctions set after_launch_filename='" & filename & "' where auction_id=" & auction_id
                        pathToCreate = "../Upload/Auctions/after_launch"
                    End If
                    SqlHelper.ExecuteNonQuery(str)
                    If Request.QueryString.Get("type") = "l" Then

                        pathToCreate = pathToCreate & "/" & auction_id.ToString()
                    Else
                        ' HID_Upload.Value = SqlHelper.ExecuteScalar(str)
                        pathToCreate = pathToCreate & "/" & auction_id.ToString() & "/" & HID_Upload.Value.ToString

                    End If
                Else
                    If Request.QueryString.Get("type") = "p" Then
                        '
                        str = "insert into tbl_auction_payment_term_attachments (filename,filename_f,filename_s,filename_c,auction_id,submit_date,submit_by_user_id) values (case " & Request.QueryString.Get("l") & " when 1 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 2 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 3 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 4 then '" & filename & "' else '' end," & auction_id & ",getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ") select scope_identity()"
                        pathToCreate = "../Upload/Auctions/payment_term_attachments"
                    ElseIf Request.QueryString.Get("type") = "s" Then
                        str = "insert into tbl_auction_shipping_term_attachments (filename,filename_f,filename_s,filename_c,auction_id,submit_date,submit_by_user_id) values (case " & Request.QueryString.Get("l") & " when 1 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 2 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 3 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 4 then '" & filename & "' else '' end," & auction_id & ",getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ") select scope_identity()"
                        'Response.Write(str)
                        pathToCreate = "../Upload/Auctions/shipping_term_attachments"
                    ElseIf Request.QueryString.Get("type") = "o" Then
                        str = "insert into tbl_auction_terms_cond_attachments (filename,filename_f,filename_s,filename_c,auction_id,submit_date,submit_by_user_id) values (case " & Request.QueryString.Get("l") & " when 1 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 2 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 3 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 4 then '" & filename & "' else '' end," & auction_id & ",getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ") select scope_identity()"
                        pathToCreate = "../Upload/Auctions/terms_cond_attachments"
                    ElseIf Request.QueryString.Get("type") = "t" Then
                        str = "insert into tbl_auction_tax_attachments (filename,filename_f,filename_s,filename_c,auction_id,submit_date,submit_by_user_id) values (case " & Request.QueryString.Get("l") & " when 1 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 2 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 3 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 4 then '" & filename & "' else '' end," & auction_id & ",getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ") select scope_identity()"
                        pathToCreate = "../Upload/Auctions/tax_attachments"
                    ElseIf Request.QueryString.Get("type") = "r" Then
                        str = "insert into tbl_auction_product_attachments (filename,filename_f,filename_s,filename_c,auction_id,submit_date,submit_by_user_id) values (case " & Request.QueryString.Get("l") & " when 1 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 2 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 3 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 4 then '" & filename & "' else '' end," & auction_id & ",getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ") select scope_identity()"
                        pathToCreate = "../Upload/Auctions/product_attachments"
                    ElseIf Request.QueryString.Get("type") = "c" Then
                        str = "insert into tbl_auction_special_conditions (filename,filename_f,filename_s,filename_c,auction_id,submit_date,submit_by_user_id) values (case " & Request.QueryString.Get("l") & " when 1 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 2 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 3 then '" & filename & "' else '' end,case " & Request.QueryString.Get("l") & " when 4 then '" & filename & "' else '' end," & auction_id & ",getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ") select scope_identity()"
                        pathToCreate = "../Upload/Auctions/special_conditions"
                    ElseIf Request.QueryString.Get("type") = "m" Then
                        str = "insert into tbl_auction_product_item_attachments (filename,auction_id,submit_date,submit_by_user_id) values ('" & filename & "'," & auction_id & ",getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ") select scope_identity()"
                        pathToCreate = "../Upload/Auctions/product_items"
                    ElseIf Request.QueryString.Get("type") = "l" Then
                        str = "update tbl_auctions set after_launch_filename='" & filename & "' where auction_id=" & auction_id
                        pathToCreate = "../Upload/Auctions/after_launch"
                    End If

                    If Request.QueryString.Get("type") = "l" Then

                        SqlHelper.ExecuteScalar(str)
                        pathToCreate = pathToCreate & "/" & auction_id.ToString()
                    Else
                        HID_Upload.Value = SqlHelper.ExecuteScalar(str)
                        pathToCreate = pathToCreate & "/" & auction_id.ToString() & "/" & HID_Upload.Value.ToString

                    End If
                End If


                Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
                If infoFile.Exists Then
                    System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
                End If
                If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
                    System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
                End If
                'Threading.Thread.Sleep(1000)
                'Response.Write(pathToCreate)
                FileUpload1.PostedFile.SaveAs(Server.MapPath(pathToCreate & "/" & filename))
                '
            End If
            ' lbl_msg.Text = "Updated"

        Catch ex As Exception
            lbl_msg.Text = ex.Message
        End Try
    End Sub
    'Private Sub UploadFileShippingTerms(ByVal auction_id As Integer)
    '    Try

    '        Dim filename As String = ""
    '        Dim pathToCreate As String = "../Upload/Auctions/shipping_term_attachments/" + auction_id.ToString()

    '        If remove_attach_shipping_terms.Checked And HID_shipping_term_attachment_id.Value <> "0" Then
    '            lbl_attach_shipping_terms.Text = ""
    '            filename = SqlHelper.ExecuteScalar("select filename from tbl_auction_shipping_term_attachments where shipping_term_attachment_id=" & HID_shipping_term_attachment_id.Value & " order by shipping_term_attachment_id desc")
    '            SqlHelper.ExecuteNonQuery("delete tbl_auction_shipping_term_attachments where shipping_term_attachment_id=" & HID_shipping_term_attachment_id.Value)
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & HID_shipping_term_attachment_id.Value & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & HID_shipping_term_attachment_id.Value & "/" & filename)
    '            End If
    '            HID_shipping_term_attachment_id.Value = "0"
    '        End If
    '        remove_attach_shipping_terms.Checked = False
    '        filename = ""


    '        If File_attach_shipping_terms.HasFile Then
    '            filename = File_attach_shipping_terms.FileName
    '            lbl_attach_shipping_terms.Text = filename
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If

    '            Dim shipping_term_attachment_id As Integer = 0

    '            If HID_shipping_term_attachment_id.Value <> "0" Then
    '                shipping_term_attachment_id = HID_shipping_term_attachment_id.Value
    '                SqlHelper.ExecuteNonQuery("update tbl_auction_shipping_term_attachments set filename='" & filename & "' where shipping_term_attachment_id=" & shipping_term_attachment_id)
    '            Else
    '                Dim qry As String = "insert into tbl_auction_shipping_term_attachments(auction_id,filename,submit_date,submit_by_user_id) values(" & auction_id & ",'" & filename & "',getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ")  select scope_identity()"
    '                shipping_term_attachment_id = SqlHelper.ExecuteScalar(qry)
    '            End If

    '            pathToCreate = pathToCreate & "/" & shipping_term_attachment_id
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
    '            End If
    '            File_attach_shipping_terms.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

    '        End If
    '    Catch ex As Exception
    '        'RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))
    '    End Try
    'End Sub
    'Private Sub UploadFileTermsConditions(ByVal auction_id As Integer)
    '    Try

    '        Dim filename As String = ""
    '        Dim pathToCreate As String = "../Upload/Auctions/terms_cond_attachments/" + auction_id.ToString()

    '        If remove_attach_other_terms_and_conditions.Checked And HID_term_cond_attachment_id.Value <> "0" Then
    '            lbl_attach_other_terms_and_conditions.Text = ""
    '            filename = SqlHelper.ExecuteScalar("select filename from tbl_auction_terms_cond_attachments where term_cond_attachment_id=" & HID_term_cond_attachment_id.Value & " order by term_cond_attachment_id desc")
    '            SqlHelper.ExecuteNonQuery("delete tbl_auction_terms_cond_attachments where term_cond_attachment_id=" & HID_term_cond_attachment_id.Value)
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & HID_term_cond_attachment_id.Value & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & HID_term_cond_attachment_id.Value & "/" & filename)
    '            End If
    '            HID_term_cond_attachment_id.Value = "0"
    '        End If
    '        remove_attach_other_terms_and_conditions.Checked = False
    '        filename = ""

    '        If File_attach_other_terms_and_conditions.HasFile Then
    '            filename = File_attach_other_terms_and_conditions.FileName
    '            lbl_attach_other_terms_and_conditions.Text = filename
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If

    '            Dim term_cond_attachment_id As Integer = 0

    '            If HID_term_cond_attachment_id.Value <> "0" Then
    '                term_cond_attachment_id = HID_term_cond_attachment_id.Value
    '                SqlHelper.ExecuteNonQuery("update tbl_auction_terms_cond_attachments set filename='" & filename & "' where term_cond_attachment_id=" & term_cond_attachment_id)
    '            Else
    '                Dim qry As String = "insert into tbl_auction_terms_cond_attachments(auction_id,filename,submit_date,submit_by_user_id) values(" & auction_id & ",'" & filename & "',getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ")  select scope_identity()"
    '                term_cond_attachment_id = SqlHelper.ExecuteScalar(qry)
    '            End If

    '            pathToCreate = pathToCreate & "/" & term_cond_attachment_id
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
    '            End If
    '            File_attach_other_terms_and_conditions.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

    '        End If
    '    Catch ex As Exception
    '        'RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))
    '    End Try
    'End Sub

    'Protected Sub btn_other_edit_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btn_other_edit.Click
    '    ' managemode("e")
    'End Sub

    'Protected Sub btn_other_cancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btn_other_cancel.Click
    '    ' managemode("v")
    'End Sub
End Class
