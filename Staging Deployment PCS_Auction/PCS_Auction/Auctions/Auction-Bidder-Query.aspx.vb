﻿
Partial Class Auctions_Auction_Bidder_Query
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            'If IsNumeric(Request.QueryString.Get("i")) And Request.QueryString("i") <> "0" Then
            '    ViewState("auction_id") = Request.QueryString.Get("i")
            'Else
            '    ViewState("auction_id") = 0
            'End If
            'setHiddenValue()
            hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
            'load_auction_bidder_queries()
            load_all_auction_queries()
        End If
    End Sub

    'Private Sub setHiddenValue()
    '    hid_auction_id.Value = ViewState("auction_id")
    '    hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
    'End Sub
    Protected Sub load_all_auction_queries()
        If hid_user_id.Value > 0 Then
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDatatable("select A.query_id,A.auction_id,A.title,A.submit_date,A.buyer_id,B.company_name from tbl_auction_queries A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id inner join tbl_auctions A1 WITH (NOLOCK) on A.auction_id=A1.auction_id inner join tbl_reg_seller_user_mapping C on A1.seller_id=C.seller_id where A.parent_query_id=0 and C.user_id=" & hid_user_id.Value & "")
            RadGrid_Seller.DataSource = dt
            RadGrid_Seller.DataBind()
        End If
    End Sub
    'Protected Sub load_auction_bidder_queries()
    '    If hid_user_id.Value > 0 Then
    '        Dim dt As New DataTable
    '        dt = SqlHelper.ExecuteDataTable("select A.auction_id,A.query_id,A.title,A.submit_date,A.buyer_id,B.company_name from tbl_auction_queries A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id where A.parent_query_id=0")
    '        rep_after_queries.DataSource = dt
    '        rep_after_queries.DataBind()
    '    End If
    'End Sub

    'Protected Sub rep_after_queries_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rep_after_queries.ItemCommand
    '    If e.CommandName = "send_query" Then
    '        Dim dt As New DataTable
    '        dt = SqlHelper.ExecuteDataTable("insert into tbl_auction_queries (auction_id,message,buyer_id,user_id,parent_query_id,submit_date) values (" & DirectCast(e.Item.FindControl("rep_hid_auction"), HiddenField).Value & ",'" & DirectCast(e.Item.FindControl("rep_txt_message"), TextBox).Text.Trim.Replace("'", "''") & "'," & DirectCast(e.Item.FindControl("rep_hid_buyer"), HiddenField).Value & "," & hid_user_id.Value & "," & e.CommandArgument & ",getdate())")
    '        load_auction_bidder_queries()
    '    End If
    'End Sub

    'Protected Sub rep_after_queries_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rep_after_queries.ItemDataBound
    '    Dim rep_inner As Repeater = DirectCast(e.Item.FindControl("rep_inner_after_queries"), Repeater)
    '    rep_inner.DataSource = SqlHelper.ExecuteDataTable("select A.message,A.submit_date,C.first_name from tbl_auction_queries A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id left join tbl_sec_users C on A.user_id=C.user_id where A.parent_query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & "")
    '    rep_inner.DataBind()
    'End Sub

    Protected Sub RadGrid_Seller_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrid_Seller.ItemCommand
        If e.CommandName = "send_query" Then
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDataTable("insert into tbl_auction_queries (auction_id,message,buyer_id,user_id,parent_query_id,submit_date) values (" & DirectCast(e.Item.FindControl("rep_hid_auction"), HiddenField).Value & ",'" & DirectCast(e.Item.FindControl("rep_txt_message"), TextBox).Text.Trim.Replace("'", "''").Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'," & DirectCast(e.Item.FindControl("rep_hid_buyer"), HiddenField).Value & "," & hid_user_id.Value & "," & e.CommandArgument & ",getdate())")
            load_all_auction_queries()
        End If
    End Sub

    Protected Sub RadGrid_Seller_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid_Seller.ItemDataBound
        If TypeOf e.Item Is Telerik.Web.UI.GridDataItem Then
            Dim rep_inner As Repeater = DirectCast(e.Item.FindControl("rep_inner_after_queries"), Repeater)
            rep_inner.DataSource = SqlHelper.ExecuteDataTable("select A.message,A.submit_date,C.first_name from tbl_auction_queries A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id left join tbl_sec_users C on A.user_id=C.user_id where A.parent_query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & "")
            rep_inner.DataBind()
        End If

    End Sub

    Protected Sub RadGrid_Seller_SortCommand(sender As Object, e As Telerik.Web.UI.GridSortCommandEventArgs) Handles RadGrid_Seller.SortCommand
        load_all_auction_queries()
    End Sub
End Class
