﻿<%@ Page Language="VB" AutoEventWireup="false" MaintainScrollPositionOnPostback="true" CodeFile="AuctionConfirmation_Old.aspx.vb" Inherits="AuctionConfirmation_Old" %>

<%@ Register Src="~/UserControls/SalesRep.ascx" TagName="SalesRep" TagPrefix="UC1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Auctions</title><![if IE]>
    <link rel="stylesheet" type="text/css" href="/Style/font_ie.css" />
    <![endif]> <![if !IE]>
    <link rel="stylesheet" type="text/css" href="/Style/font_other.css" />
    <![endif]>
    <link type="text/css" rel="Stylesheet" href="/Style/AuctionBody.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="dhtmltooltip_new">
    </div>
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <center>
        <UC1:SalesRep runat="server" ID="UC_SalesRep"></UC1:SalesRep>
    </center>
    <div style="width: 800px; padding-left: 20px; padding-bottom: 20px;">
        <div class="confirmHeading">
            Order Confirmation
        </div>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="invoiceAddress">
                    <span class="invoiceCompanyName">PCS Wireless LLC</span><br />
                    11 Vreeland Road<br />
                    Florham Park NJ, 07932<br />
                    Phone: (973) 850-7400 Fax: (973) 301-0975<br />
                    www.pcsww.com<br />
                    sales@pcsww.com
                </td>
                <td align="right" valign="top">
                    <img src="/Images/logo.gif" alt="PCS Auction" border="0" />
                    <br />
                    <br />
                    <table>
                        <tr>
                            <td class="invNumber">
                                Date:
                            </td>
                            <td class="invNumber">
                                <asp:Literal ID="lit_date" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="invoiceBillTo">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <strong>Ship To : </strong>
                        <br />
                        <asp:Literal ID="lit_bill_to" runat="server" />
                    </td>
                    <td valign="top">
                        <strong>Rep :
                            <asp:Literal ID="lit_sales_rep" runat="server" /></strong>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <table cellpadding="10" cellspacing="1" width="100%" border="0" style="background-color: #E3E3E3;">
            <tr>
                <td align="left" class="invCell" style="padding-left: 20px; width: 55%;">
                    <b>AUCTION TITLE</b>
                </td>
                <td align="left" class="invCell" style="width: 15%;">
                    <b>AUCTION NO</b>
                </td>
                <td align="left" class="invCell" style="width: 15%;">
                    <b>CONDITION</b>
                </td>
                <td class="invCell" align="left" style="width: 15%;">
                    <b>PACKAGING</b>
                </td>
            </tr>
            <tr>
                <td align="left" class="invCell" style="padding-left: 20px;">
                    <asp:Literal ID="lit_auc_name" runat="server" />
                </td>
                <td class="invCell" align="left">
                    <asp:Literal ID="lit_auc_code" runat="server" />
                </td>
                <td class="invCell" align="left">
                    <asp:Literal ID="lit_condition" runat="server" />
                </td>
                <td class="invCell" align="left">
                    <asp:Literal ID="lit_auc_packaging" runat="server" />
                </td>
            </tr>
        </table>
        <br />
        <asp:UpdatePanel ID="upd_items" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table cellpadding="10" cellspacing="1" width="100%" border="0" style="background-color: #E3E3E3;">
                    <tr>
                        <td align="left" class="invCell" style="padding-left: 20px; width: 10%;">
                            <b>PART NO</b>
                        </td>
                        <td align="left" class="invCell" style="width: 50%;">
                            <b>DESCRIPTION</b>
                        </td>
                        <td align="left" class="invCell" style="width: 20%;">
                            <b>SKU</b>
                        </td>
                        <td class="invCell" align="center" style="width: 20%;">
                            <b>QTY.</b>
                        </td>
                    </tr>
                    <asp:Repeater ID="rpt_device" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td class="invCell" align="left" style="padding-left: 20px;">
                                    <%# Eval("part_no")%>
                                </td>
                                <td align="left" class="invCell">
                                    <%# Eval("name")%>
                                </td>
                                <td class="invCell" align="left">
                                    <%# Eval("sku")%>
                                </td>
                                <td class="invCell" align="center">
                                    <%# Eval("quantity")%>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td class="invCell" colspan="2" rowspan="3" align="left" style="padding-left: 20px;">
                            <table>
                                <tr>
                                    <td style="text-align: left; font-size: 13px;">
                                        <b>Shipping Options</b><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 0px 0px 0px; color: black;">
                                        <center style="text-align: left;">
                                            <asp:RadioButtonList ID="rdo_option" runat="server" AutoPostBack="true">
                                                <asp:ListItem Text="Use my own shipping carrier to ship this item to me" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Estimated FedEx Shipping Rates" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </center>
                                        <center>
                                            <br />
                                            <div id="tbl_user_own" runat="server" visible="false">
                                                <table border="0" style="border: 1px solid grey; width: 280px;">
                                                    <tr>
                                                        <td style="text-align: left;">
                                                            My shipping carrier is<br />
                                                            <asp:TextBox ID="txt_shipping_type" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td rowspan="2">
                                                            <table id="table_stock" runat="server" width="100%">
                                                                <tr>
                                                                    <td valign="top" style="border-left: 1px solid gray;">
                                                                        <span style="color: #10C0F5;">Stock Location:</span><br />
                                                                        <asp:Label ID="lbl_stock_location" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left">
                                                            My account # is<br />
                                                            <asp:TextBox ID="txt_shipping_account_number" runat="server"></asp:TextBox><br />
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </center>
                                        <center>
                                            <div id="tbl_user_ours" runat="server" visible="false" style="border: 1px solid grey;
                                                width: 300px; text-align: left;">
                                                <asp:RadioButtonList ID="rdo_fedex" runat="server">
                                                    <asp:ListItem Text=" FEDEX EXPRESS SAVER" Value="0" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text=" FEDEX 2 DAY" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text=" FEDEX EXPRESS SAVER" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                            <%--<asp:Button ID="btn_save" runat="server" Text="Save" />--%><br />
                                        </center>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="invCell" align="center">
                            <b>Sub Total</b>
                        </td>
                        <td class="invCell" align="center">
                            <b>
                                <asp:Literal ID="lit_total" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                    <tr>
                        <td class="invCell" align="center">
                            <b>Estimated Shipping</b>
                        </td>
                        <td class="invCell" align="center">
                            <b>
                                <asp:Literal ID="lit_shipping" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                    <tr>
                        <td class="invCell" align="center" style="font-size: 14px; color: Black;">
                            <b>Total</b>
                        </td>
                        <td class="invCell" align="center" style="font-size: 14px; color: Black;">
                            <b>
                                <asp:Literal ID="lit_payble" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
