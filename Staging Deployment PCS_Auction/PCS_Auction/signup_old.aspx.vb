﻿
Partial Class signup_old
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        DirectCast(Master.FindControl("lnk_content_current"), HyperLink).Text = "SIGNUP"
        DirectCast(Master.FindControl("lnk_content_current"), HyperLink).NavigateUrl = "/sign-up.html"
        If Page.IsPostBack = False Then
            fill_countries()
            fill_states()
            fill_how_find_us()
            fill_business_types()
            fill_industry_types()
            fill_companies()
            fill_titles()
            txt_company.Focus()

        End If

    End Sub
    Private Sub fill_countries()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select country_id,name,ISNULL(code,'') As code from tbl_master_countries"
        ds = SqlHelper.ExecuteDataset(strQuery)
        ddl_country.DataSource = ds
        ddl_country.DataTextField = "name"
        ddl_country.DataValueField = "country_id"
        ddl_country.DataBind()
        ddl_country.Items.Insert(0, New ListItem("--Select--", "0"))
        ddl_country.ClearSelection()
        ddl_country.Items.FindByText("USA").Selected = True
        ds.Dispose()
    End Sub
    Private Sub fill_states()
        Dim ds As New DataTable
        Dim strQuery As String = ""
        Dim country_id As Integer = 0
        If ddl_country.SelectedItem.Value <> "" Then
            country_id = ddl_country.SelectedItem.Value
            strQuery = "select state_id,name from tbl_master_states where country_id=" & country_id.ToString() & " order by name"

            ds = SqlHelper.ExecuteDataTable(strQuery)
            ddl_state.DataSource = ds
            ddl_state.DataTextField = "name"
            ddl_state.DataValueField = "state_id"
            ddl_state.DataBind()
        End If
        ddl_state.Items.Insert(0, New ListItem("--Select--", "0"))
        ds.Dispose()
        ddl_state.SelectedIndex = 1

    End Sub
    Private Sub fill_titles()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select isnull(name,'') as name,title_id from tbl_master_titles order by name"
        ds = SqlHelper.ExecuteDataset(strQuery)
        ddl_title.DataSource = ds
        ddl_title.DataTextField = "name"
        ddl_title.DataValueField = "name"
        ddl_title.DataBind()
        ddl_title.Items.Insert(0, New ListItem("--Select--", ""))
        ds.Dispose()
    End Sub
    Private Sub fill_how_find_us()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select how_find_us_id,name,ISNULL(description,'') as description,ISNULL(sno,99)as sno from tbl_master_how_find_us order by sno"
        ds = SqlHelper.ExecuteDataset(strQuery)
        ddl_how_find_us.DataSource = ds
        ddl_how_find_us.DataTextField = "name"
        ddl_how_find_us.DataValueField = "how_find_us_id"
        ddl_how_find_us.DataBind()
        ddl_how_find_us.Items.Insert(0, New ListItem("--Select--", ""))
        ds.Dispose()
    End Sub
    Private Sub fill_business_types()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select business_type_id,name,ISNULL(description,'') as description,ISNULL(sno,99)as sno from tbl_master_business_types order by sno"
        ds = SqlHelper.ExecuteDataset(strQuery)
        chklst_business_types.DataSource = ds
        chklst_business_types.DataTextField = "name"
        chklst_business_types.DataValueField = "business_type_id"
        chklst_business_types.DataBind()
        ds.Dispose()
    End Sub
    Private Sub fill_industry_types()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select industry_type_id,name,ISNULL(description,'') AS description,ISNULL(sno,999) as sno from tbl_master_industry_types order by sno"
        ds = SqlHelper.ExecuteDataset(strQuery)
        chklst_industry_types.DataSource = ds
        chklst_industry_types.DataTextField = "name"
        chklst_industry_types.DataValueField = "industry_type_id"
        chklst_industry_types.DataBind()
        ds.Dispose()
    End Sub
    Private Sub fill_companies()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select seller_id,company_name from tbl_reg_sellers where is_active=1 order by company_name"
        ds = SqlHelper.ExecuteDataset(strQuery)
        If ds.Tables(0).Rows.Count > 1 Then
            chklst_companies_linked.DataSource = ds
            chklst_companies_linked.DataTextField = "company_name"
            chklst_companies_linked.DataValueField = "seller_id"
            chklst_companies_linked.DataBind()
            ds.Dispose()
        ElseIf ds.Tables(0).Rows.Count = 1 Then
            hid_comp_id.Value = ds.Tables(0).Rows(0)("seller_id")
            tr_company.Visible = False
        Else
            tr_company.Visible = False
        End If

    End Sub

    Private Function validate_user_id() As Integer
        Dim strQuery As String = ""
        strQuery = "if exists(select user_id from vw_login_users where username='" & txt_user_id.Text.Trim() & "') select 1 else select 0"
        Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)
        Return i
    End Function

    Private Function Validate_email() As Integer
        Dim strQuery As String = ""

        strQuery = "if exists(select email from vw_login_users where email='" & txt_email.Text.Trim() & "') select 1 else select 0"

        Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)
        Return i
    End Function

    Private Sub Update_buyer_business_types(ByVal buyer_id As Integer)
        For Each lst As ListItem In chklst_business_types.Items
            Dim strQuery As String = ""
            Dim i As String = lst.Value
            If lst.Selected Then
                strQuery = "if not exists(select mapping_id from tbl_reg_buyer_business_type_mapping where buyer_id=" & buyer_id.ToString() & " and business_type_id=" & i & ") insert into tbl_reg_buyer_business_type_mapping(buyer_id,business_type_id) Values(" & buyer_id.ToString() & "," & i & ")"
                ' Else
                '    strQuery = "delete from tbl_reg_buyer_business_type_mapping where buyer_id=" & buyer_id.ToString() & " and business_type_id=" & i
                SqlHelper.ExecuteNonQuery(strQuery)
            End If


        Next
    End Sub
    Private Sub Update_buyer_industry_types(ByVal buyer_id As Integer)
        For Each lst As ListItem In chklst_industry_types.Items
            Dim strQuery As String = ""
            Dim i As String = lst.Value
            If lst.Selected Then
                strQuery = "if not exists(select mapping_id from tbl_reg_buyer_industry_type_mapping where buyer_id=" & buyer_id.ToString() & " and industry_type_id=" & i & ") insert into tbl_reg_buyer_industry_type_mapping(buyer_id,industry_type_id) Values(" & buyer_id.ToString() & "," & i & ")"
                'Else
                'strQuery = "delete from tbl_reg_buyer_industry_type_mapping where buyer_id=" & buyer_id.ToString() & " and industry_type_id=" & i
                SqlHelper.ExecuteNonQuery(strQuery)
            End If

        Next
    End Sub
    Private Sub Update_buyer_companies_linked(ByVal buyer_id As Integer)
        Dim strQuery As String = ""
        Dim i As String = ""
        If chklst_companies_linked.Items.Count > 1 Then

            For Each lst As ListItem In chklst_companies_linked.Items
                i = lst.Value
                If lst.Selected Then
                    strQuery = "if not exists(select mapping_id from tbl_reg_buyer_seller_mapping where buyer_id=" & buyer_id.ToString() & " and seller_id=" & i & ") insert into tbl_reg_buyer_seller_mapping(buyer_id,seller_id) Values(" & buyer_id.ToString() & "," & i & ")"
                    ' Else
                    'strQuery = "delete from tbl_reg_buyer_seller_mapping where buyer_id=" & buyer_id.ToString() & " and seller_id=" & i
                    SqlHelper.ExecuteNonQuery(strQuery)
                End If

            Next
        Else

            strQuery = "if not exists(select mapping_id from tbl_reg_buyer_seller_mapping where buyer_id=" & buyer_id.ToString() & " and seller_id=" & hid_comp_id.Value & ") insert into tbl_reg_buyer_seller_mapping(buyer_id,seller_id) Values(" & buyer_id.ToString() & "," & hid_comp_id.Value & ")"

            SqlHelper.ExecuteNonQuery(strQuery)
        End If
    End Sub
    Private Sub Update_buyer_how_find_us(ByVal buyer_id As Integer)
        If ddl_how_find_us.SelectedValue <> "" Then
            Dim strQuery As String = ""

            '  If lst.Selected Then
            strQuery = "if not exists(select assignment_id from tbl_reg_buyer_find_us_assignment where buyer_id=" & buyer_id.ToString() & " and how_find_us_id=" & ddl_how_find_us.SelectedValue & ") insert into tbl_reg_buyer_find_us_assignment(buyer_id,how_find_us_id) Values(" & buyer_id.ToString() & "," & ddl_how_find_us.SelectedValue & ")"
            ' Else
            '     strQuery = "delete from tbl_reg_buyer_find_us_assignment where buyer_id=" & buyer_id.ToString() & " and how_find_us_id=" & i
            ' End If

            SqlHelper.ExecuteNonQuery(strQuery)
        End If
    End Sub

    Protected Sub imgbtn_basic_save_Click(sender As Object, e As System.EventArgs) Handles imgbtn_basic_save.Click

        'txt_comment.Text = txt_comment.Text.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "<br><br> Normal: "
        'txt_comment.Text = txt_comment.Text.Replace("<br/>", Char.ConvertFromUtf32(13)).Replace("<br/>", Char.ConvertFromUtf32(10))
        ''Response.Write("<br><br> Normal: " & txt_comment.Text)
        'Exit Sub
        hid_password.Value = txt_password.Text.Trim()
        'txt_address1.Text = "Sanjay"
        If Page.IsValid Then
            Dim i As Integer = validate_user_id()
            If i = 1 Then
                lblMessage.Text = "User exists. Please choose other user id."
                Exit Sub
            End If
            i = Validate_email()
            If i = 1 Then
                lblMessage.Text = "Email is already in our record."
                Exit Sub
            End If
            Dim buyer_id As Integer = 0
            Dim strQuery As String = ""
            strQuery = "Insert into tbl_reg_buyers(company_name,contact_title,contact_first_name,contact_last_name,email,website,mobile,phone,fax,address1,address2,city,state_id,zip,country_id,status_id,state_text,comment,is_phone1_mobile,is_phone2_mobile)Values('" & txt_company.Text.Trim() & "','" & ddl_title.SelectedValue & "','" & txt_first_name.Text.Trim() & "','" & txt_last_name.Text.Trim() & "','" & txt_email.Text.Trim() & "','" & txt_website.Text.Trim() & "','" & txt_mobile.Text.Trim() & "','" & txt_phone.Text.Trim() & "','" & txt_fax.Text.Trim() & "','" & txt_address1.Text.Trim() & "','" & txt_address2.Text.Trim() & "','" & txt_city.Text.Trim() & "'," & IIf(ddl_country.SelectedItem.Text.ToLower.Contains("usa"), ddl_state.SelectedItem.Value, 0) & ",'" & txt_zip.Text.Trim() & "'," & ddl_country.SelectedItem.Value & ",1,'" & IIf(ddl_country.SelectedItem.Text.ToLower.Contains("usa"), "", txt_other_state.Text.Trim) & "','" & txt_comment.Text.Trim.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'," & IIf(rdo_ph_1_landline.Checked, 0, 1) & "," & IIf(rdo_ph_2_landline.Checked, 0, 1) & ") select SCOPE_IDENTITY()"

            'lblMessage.Text = strQuery
            Try
                buyer_id = SqlHelper.ExecuteScalar(strQuery)
            Catch ex As Exception

            End Try

            If buyer_id > 0 Then
                Dim is_admin_buyer As Integer = 1
                strQuery = "Insert into tbl_reg_buyer_users (buyer_id,first_name,last_name,title,username,password,email,is_active,submit_date,submit_by_user_id,is_admin) Values(" & buyer_id.ToString & ",'" & txt_first_name.Text.Trim() & "','" & txt_last_name.Text.Trim() & "','" & ddl_title.SelectedValue & "','" & txt_user_id.Text.Trim() & "','" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "','" & txt_email.Text.Trim() & "',0,GETDATE(),0," & is_admin_buyer & ")"
                'lblMessage.Text = lblMessage.Text & strQuery
                Try
                    SqlHelper.ExecuteNonQuery(strQuery)
                Catch ex As Exception

                End Try

                Update_buyer_how_find_us(buyer_id)
                Update_buyer_business_types(buyer_id)
                Update_buyer_industry_types(buyer_id)
                Update_buyer_companies_linked(buyer_id)

                Dim reg_mail As New Email
                reg_mail.Send_Registration_Mail(txt_email.Text.Trim, txt_first_name.Text)
                reg_mail.Bidderd_Admin_Active(buyer_id)
                reg_mail = Nothing

                Response.Redirect("/register_confirm.aspx?rt=N")
            Else
                txt_password.Attributes.Add("value", hid_password.Value)
                txt_retype_password.Attributes.Add("value", hid_password.Value)
                lblMessage.Text = "Error in creating new buyer. Please contact administrator."
            End If
        Else
            txt_password.Attributes.Add("value", hid_password.Value)
            txt_retype_password.Attributes.Add("value", hid_password.Value)
        End If
    End Sub
End Class
