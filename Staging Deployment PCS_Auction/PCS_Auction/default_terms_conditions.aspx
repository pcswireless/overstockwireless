﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AuctionMain.master" AutoEventWireup="false"
    CodeFile="default_terms_conditions.aspx.vb" Inherits="default_terms_conditions" %>

<%@ Register Src="~/UserControls/terms.ascx" TagName="Terms" TagPrefix="UC3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript">

        function PrintThisPage() {
            //alert(document.getElementById("termsframe"));
            // alert(window.frames["termsframe"]);
            window.frames["termsframe"].focus();
            window.frames["termsframe"].print();
            return false;
        } 

    </script>
    <br />
    <div class="innerwraper">
        <div class="paging">
            <ul>
                <li><a href="/">Home</a></li>
                <li>Login Accept</li>
            </ul>
        </div>
        <div class="clear">
        </div>
        <br />
        <div id="secondarycontent" style="background-color: White;">
            <br />
            <div id="leftnav">
                <h4>
                    <a href="/" onclick='return Modal_Dialog();'>LOGIN</a>
                </h4>
                <div class="leftcalloutbox">
                    <div class="leftcalloutboxcontainer">
                        Helping you grow your business is our number-one priority.
                    </div>
                </div>
                <div id="leftcontentboxfooter">
                    <div id="leftcontentboxfootercontainer">
                        <div style="font-size: 13px; font-weight: bold;">
                            Why become a member?</div>
                        <div style="padding-top: 10px;">
                            <ul style="list-style: url(/images/arrow_home.gif)">
                                <li>No bidding fees.</li>
                                <li>Auctions without minimum bids.</li>
                                <li>Purchase outright with buy it now options.</li>
                                <li>Phones, tablets, laptops and accessories.</li>
                                <li>New and pre-owned products.</li>
                                <li>Hard to find models.</li>
                                <li>Choose from multiple technologies, models and brands.</li>
                                <li>Pre-qualified buyers.</li>
                                <li>Market driven pricing.</li>
                                <li>Products in stock now.</li>
                            </ul>
                        </div>
                        <div>
                            <a href="/sign-up.html" style="color: #10AAEA; font-weight: bold;">Join now</a>
                        </div>
                        <div style="padding-top: 16px;">
                            <div style="font-weight: bold;">
                                Questions?</div>
                            <div style="padding-top: 10px;">
                                Email: <a href="mailto:<%= SqlHelper.of_FetchKey("site_email_to")  %>"><%= SqlHelper.of_FetchKey("site_email_to")  %></a><br />
                                Phone: 973.805.7400 ext 179
                            </div>
                        </div>
                    </div>
                </div>
                <div class="leftcontentbox">
                </div>
            </div>
            <div id="maincontent">
                <div class="products">
                    <div class="seprator">
                    </div>
                    <div style="margin-left: -30px;">
                        <div class="products2">
                            <div class="printbx_home" style="visibility: visible; width: 35px;">
                                <ul>
                                    <li class="printbx_12"><a href="javascript:void(0);" onclick="return PrintThisPage();">
                                        Print</a></li>
                                    <%--<li>
                            <asp:Literal ID="lit_pdf" runat="server" /></li>--%>
                                </ul>
                            </div>
                        </div>
                        <h1 class="pgtitle" style="margin-top: 2px; padding-left: 0px;">
                            Terms and Conditions</h1>
                        <br />
                        <iframe id="termsframe" src="/frame_src_term_condition.aspx" name="termsframe" scrolling="yes"
                            frameborder="1" style="border: 1px solid #CCCCCC; overflow-x: hidden;" width="100%"
                            height="300"></iframe>
                        <div id="buynow-popup" style="padding-top: 20px;">
                            <asp:Button runat="server" ID="accept" CssClass="fleft" Text="Accept" CausesValidation="false"/>
                            <asp:Button runat="server" ID="btnReject" CssClass="fleft" Text="Reject" CausesValidation="false"/>
                        </div>
                    </div>
                </div>
                <!--products
    -->
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
</asp:Content>
