﻿Imports System
Imports System.IO
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Security.Cryptography
Public Class EncryptDecrypt

    Private Shared Function Encrypt(ByVal plainText As String, _
                                      ByVal passPhrase As String, _
                                      ByVal saltValue As String, _
                                      ByVal hashAlgorithm As String, _
                                      ByVal passwordIterations As Integer, _
                                      ByVal initVector As String, _
                                      ByVal keySize As Integer) _
                              As String

        ' Convert strings into byte arrays.
        ' Let us assume that strings only contain ASCII codes.
        ' If strings include Unicode characters, use Unicode, UTF7, or UTF8 
        ' encoding.
        Dim initVectorBytes As Byte()
        initVectorBytes = Encoding.ASCII.GetBytes(initVector)

        Dim saltValueBytes As Byte()
        saltValueBytes = Encoding.ASCII.GetBytes(saltValue)

        ' Convert our plaintext into a byte array.
        ' Let us assume that plaintext contains UTF8-encoded characters.
        Dim plainTextBytes As Byte()
        plainTextBytes = Encoding.UTF8.GetBytes(plainText)

        ' First, we must create a password, from which the key will be derived.
        ' This password will be generated from the specified passphrase and 
        ' salt value. The password will be created using the specified hash 
        ' algorithm. Password creation can be done in several iterations.
        Dim password As PasswordDeriveBytes
        password = New PasswordDeriveBytes(passPhrase, _
                                           saltValueBytes, _
                                           hashAlgorithm, _
                                           passwordIterations)

        ' Use the password to generate pseudo-random bytes for the encryption
        ' key. Specify the size of the key in bytes (instead of bits).
        Dim keyBytes As Byte()
        keyBytes = password.GetBytes(keySize / 8)

        ' Create uninitialized Rijndael encryption object.
        Dim symmetricKey As RijndaelManaged
        symmetricKey = New RijndaelManaged()

        ' It is reasonable to set encryption mode to Cipher Block Chaining
        ' (CBC). Use default options for other symmetric key parameters.
        symmetricKey.Mode = CipherMode.CBC

        ' Generate encryptor from the existing key bytes and initialization 
        ' vector. Key size will be defined based on the number of the key 
        ' bytes.
        Dim encryptor As ICryptoTransform
        encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes)

        ' Define memory stream which will be used to hold encrypted data.
        Dim memoryStream As MemoryStream
        memoryStream = New MemoryStream()

        ' Define cryptographic stream (always use Write mode for encryption).
        Dim cryptoStream As CryptoStream
        cryptoStream = New CryptoStream(memoryStream, _
                                        encryptor, _
                                        CryptoStreamMode.Write)
        ' Start encrypting.
        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length)

        ' Finish encrypting.
        cryptoStream.FlushFinalBlock()

        ' Convert our encrypted data from a memory stream into a byte array.
        Dim cipherTextBytes As Byte()
        cipherTextBytes = memoryStream.ToArray()

        ' Close both streams.
        memoryStream.Close()
        cryptoStream.Close()

        ' Convert encrypted data into a base64-encoded string.
        Dim cipherText As String
        cipherText = System.Convert.ToBase64String(cipherTextBytes)

        ' Return encrypted string.
        Encrypt = cipherText
    End Function
    Private Shared Function Decrypt(ByVal cipherText As String, _
                                   ByVal passPhrase As String, _
                                   ByVal saltValue As String, _
                                   ByVal hashAlgorithm As String, _
                                   ByVal passwordIterations As Integer, _
                                   ByVal initVector As String, _
                                   ByVal keySize As Integer) _
                           As String

        ' Convert strings defining encryption key characteristics into byte
        ' arrays. Let us assume that strings only contain ASCII codes.
        ' If strings include Unicode characters, use Unicode, UTF7, or UTF8
        ' encoding.
        Dim initVectorBytes As Byte()
        initVectorBytes = Encoding.ASCII.GetBytes(initVector)

        Dim saltValueBytes As Byte()
        saltValueBytes = Encoding.ASCII.GetBytes(saltValue)

        ' Convert our ciphertext into a byte array.
        Dim cipherTextBytes As Byte()
        cipherTextBytes = System.Convert.FromBase64String(cipherText)

        ' First, we must create a password, from which the key will be 
        ' derived. This password will be generated from the specified 
        ' passphrase and salt value. The password will be created using
        ' the specified hash algorithm. Password creation can be done in
        ' several iterations.
        Dim password As PasswordDeriveBytes
        password = New PasswordDeriveBytes(passPhrase, _
                                           saltValueBytes, _
                                           hashAlgorithm, _
                                           passwordIterations)

        ' Use the password to generate pseudo-random bytes for the encryption
        ' key. Specify the size of the key in bytes (instead of bits).
        Dim keyBytes As Byte()
        keyBytes = password.GetBytes(keySize / 8)

        ' Create uninitialized Rijndael encryption object.
        Dim symmetricKey As RijndaelManaged
        symmetricKey = New RijndaelManaged()

        ' It is reasonable to set encryption mode to Cipher Block Chaining
        ' (CBC). Use default options for other symmetric key parameters.
        symmetricKey.Mode = CipherMode.CBC

        ' Generate decryptor from the existing key bytes and initialization 
        ' vector. Key size will be defined based on the number of the key 
        ' bytes.
        Dim decryptor As ICryptoTransform
        decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes)

        ' Define memory stream which will be used to hold encrypted data.
        Dim memoryStream As MemoryStream
        memoryStream = New MemoryStream(cipherTextBytes)

        ' Define memory stream which will be used to hold encrypted data.
        Dim cryptoStream As CryptoStream
        cryptoStream = New CryptoStream(memoryStream, _
                                        decryptor, _
                                        CryptoStreamMode.Read)

        ' Since at this point we don't know what the size of decrypted data
        ' will be, allocate the buffer long enough to hold ciphertext;
        ' plaintext is never longer than ciphertext.
        Dim plainTextBytes As Byte()
        ReDim plainTextBytes(cipherTextBytes.Length)

        ' Start decrypting.
        Dim decryptedByteCount As Integer
        decryptedByteCount = cryptoStream.Read(plainTextBytes, _
                                               0, _
                                               plainTextBytes.Length)

        ' Close both streams.
        memoryStream.Close()
        cryptoStream.Close()

        ' Convert decrypted data into a string. 
        ' Let us assume that the original plaintext string was UTF8-encoded.
        Dim plainText As String
        plainText = Encoding.UTF8.GetString(plainTextBytes, _
                                            0, _
                                            decryptedByteCount)

        ' Return decrypted string.
        Decrypt = plainText
    End Function
    Public Shared Function EncryptValue(ByVal strString As String) As String
        If strString.Trim = "" Then Return ""
        Dim strplainText As String
        Dim strcipherText As String

        Dim strpassPhrase As String
        Dim strsaltValue As String
        Dim strhashAlgorithm As String
        Dim intpasswordIterations As Integer
        Dim strinitVector As String
        Dim intkeySize As Integer

        strplainText = strString
        '"Hello, World! - abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890-=!@#$%^&*()_+[];,./<>?:'{}|\" ' original plaintext

        strpassPhrase = "Pas5pr@se"       ' can be any string
        strsaltValue = "s@1tValue"        ' can be any string
        strhashAlgorithm = "SHA1"         ' can be "MD5"
        intpasswordIterations = 2         ' can be any number
        strinitVector = "@1B2c3D4e5F6g7H8" ' must be 16 bytes
        intkeySize = 256                  ' can be 192 or 128

        'Response.Write(String.Format("Plaintext : {0}", plainText))
        'Response.Write("<br>")
        strcipherText = Encrypt(strplainText, _
                                            strpassPhrase, _
                                            strsaltValue, _
                                            strhashAlgorithm, _
                                            intpasswordIterations, _
                                            strinitVector, _
                                            intkeySize)

        'Response.Write(String.Format("Encrypted : {0}", cipherText))
        'Response.Write("<br>")

        Return strcipherText

    End Function
    Public Shared Function DecryptValue(ByVal strString As String) As String
        If strString.Trim = "" Then Return ""
        Dim strplainText As String
        Dim strcipherText As String

        Dim strpassPhrase As String
        Dim strsaltValue As String
        Dim strhashAlgorithm As String
        Dim intpasswordIterations As Integer
        Dim strinitVector As String
        Dim intkeySize As Integer

        strplainText = strString
        '"Hello, World! - abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890-=!@#$%^&*()_+[];,./<>?:'{}|\" ' original plaintext

        strpassPhrase = "Pas5pr@se"       ' can be any string
        strsaltValue = "s@1tValue"        ' can be any string
        strhashAlgorithm = "SHA1"         ' can be "MD5"
        intpasswordIterations = 2         ' can be any number
        strinitVector = "@1B2c3D4e5F6g7H8" ' must be 16 bytes
        intkeySize = 256                  ' can be 192 or 128

        'Response.Write(String.Format("Plaintext : {0}", plainText))
        'Response.Write("<br>")
        strcipherText = Decrypt(strplainText, _
                                            strpassPhrase, _
                                            strsaltValue, _
                                            strhashAlgorithm, _
                                            intpasswordIterations, _
                                            strinitVector, _
                                            intkeySize)

        'Response.Write(String.Format("Encrypted : {0}", cipherText))
        'Response.Write("<br>")

        Return strcipherText

    End Function
    Public Shared Function DecryptValueFormatted(ByVal strDecryptString As String) As String
        Return DecryptValue(strDecryptString.Replace("_", "+"))
    End Function
    Public Shared Function EncryptValueFormatted(ByVal strEncryptString As String) As String
        Return EncryptValue(strEncryptString).Replace("+", "_")
    End Function
End Class
