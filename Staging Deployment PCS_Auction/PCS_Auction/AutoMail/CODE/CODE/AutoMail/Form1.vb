﻿Imports System.IO
Public Class Form1
    Private is_server As Integer
    Private DBConnection As String
    Private ServerHttp As String
    Private erroremail As String
    Private bccemail As String

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            ReadTextFile()

            Dim str As String
            Dim flag As Boolean = False
            Dim arr

            Dim myReader As StreamReader
            myReader = File.OpenText(Application.StartupPath() & "\source.txt")
            Do
                str = myReader.ReadLine
                If flag And str <> "" Then
                    arr = str.Split(",")
                    is_server = arr(0)
                    If arr(1) <> "" Then ServerHttp = arr(1)
                    If arr(2) <> "" Then erroremail = arr(2)
                    If arr(3) <> "" Then bccemail = arr(3)
                End If
                flag = True
            Loop While str <> Nothing

            CheckforMail()

            Me.Timer1.Enabled = True
        Catch ex As Exception
            sendEmail("Error in Auction automail exe", "Form1_Load : " & ex.Message, erroremail, "Bimal Giri")
        End Try
    End Sub
    Private Function GetEmail_ByCode(ByVal email_code As String, Optional ByVal qry As String = "") As DataSet
        Dim ds As DataSet = New DataSet
        Try


            Dim strQuery As String = ""
            strQuery = "select isnull(A.email_name,'') as email_name,isnull(A.email_subject,'') as email_subject, cast(isnull(B.header_html,'') as nvarchar(max))+cast(isnull(A.email_html,'') as  nvarchar(max))+cast(isnull(C.footer_html,'')as nvarchar(max)) as html_body " & _
            "from tbl_master_emails A INNER JOIN tbl_master_email_headers B ON A.header_id=B.header_id INNER JOIN tbl_master_email_footers C ON A.footer_id=C.footer_id where A.is_active=1 and B.is_active=1 and C.is_active=1 and A.email_code='" & email_code & "'"
            If qry.Trim <> "" Then
                strQuery = strQuery & ";" & qry
            End If
            ds = SqlHelper.ExecuteDataset(DBConnection, strQuery)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                ds.Tables(0).Rows(0)("html_body").ToString().Replace("<<site_url>>", ServerHttp)
            End If

        Catch ex As Exception
            sendEmail("Error in Auction automail exe", "GetEmail_ByCode : " & ex.Message, erroremail, "Bimal Giri")
        End Try

        Return ds
    End Function
    Sub CheckforMail()
        Try

            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDatatable(DBConnection, "select distinct S.schedule_type from [dbo].[fn_get_email_schedule_list]() F inner join tbl_auction_email_schedules S on F.email_schedule_id=S.email_schedule_id")
            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    Select Case dt.Rows(i)("schedule_type").ToString().ToUpper()
                        Case "BEFORE START"
                            sendInvitationBeforeMail()
                        Case "BEFORE END"
                            sendInvitationBeforeEndMail()
                        Case "AFTER END"
                            sendInvitationAfterMail()
                    End Select
                Next
            End If
            dt = Nothing
        Catch ex As Exception
            sendEmail("Error in Auction automail exe", "CheckforMail : " & ex.Message, erroremail, "Bimal Giri")
        End Try
    End Sub
    Public Sub sendInvitationBeforeMail()
        Try
            Dim strQuery As String = "select E.email_schedule_id,A.auction_id,A.auction_type_id,A.title,A.start_date,A.display_end_time,A.start_price,B.first_name as name,B.email,B.buyer_id from dbo.fn_get_email_schedule_list() F inner join tbl_auction_email_schedules E on E.email_schedule_id=F.email_schedule_id inner join tbl_auctions A on A.auction_id=F.auction_id inner join tbl_reg_buyer_users B on B.buyer_id=F.buyer_id and B.is_admin=1 and upper(E.schedule_type)='BEFORE START'"
            'Label1.Text = Label1.Text & strQuery

            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable(DBConnection, strQuery)

            Dim dtBidders As New DataTable()
            Dim strBidders As String = "select distinct buyer_id from dbo.fn_get_email_schedule_list() F inner join tbl_auction_email_schedules S on F.email_schedule_id=S.email_schedule_id WHERE UPPER(S.schedule_type)='BEFORE START'"
            dtBidders = SqlHelper.ExecuteDatatable(DBConnection, strBidders)

            Dim buyer_email As String = ""
            Dim buyer_name As String = ""
            Dim auction_title As String = ""
            Dim start_date As DateTime = "1/1/1999"
            Dim end_date As DateTime = "1/1/1999"

            Dim span As TimeSpan
            Dim time_left As String = ""
            Dim no_of_days As Integer = 0
            Dim auc_id As Integer = 0
            Dim sce_id As Integer = 0
            Dim em_id As String = ""
            Dim email_caption As String = "Participation Email For Upcoming Auction (Dynamic)"
            Dim body As String = ""
            Dim email_body As String = ""
            Dim str_subject As String = ""
            Dim rept_body As String = ""
            Dim ds As New DataSet
            ds = GetEmail_ByCode("em5")
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                email_body = ds.Tables(0).Rows(0)("html_body")
                str_subject = ds.Tables(0).Rows(0)("email_subject")
                email_caption = ds.Tables(0).Rows(0)("email_name")
                If dtBidders.Rows.Count > 0 Then
                    For j As Integer = 0 To dtBidders.Rows.Count - 1
                        body = email_body
                        buyer_name = ""
                        buyer_email = ""
                        rept_body = ""
                        auction_title = ""
                        Dim buyer_id As Integer = 0
                        buyer_id = dtBidders.Rows(j)("buyer_id")
                        Dim dv As DataView = dt.DefaultView
                        dv.RowFilter = "buyer_id=" & dtBidders.Rows(j)("buyer_id")
                        For i As Integer = 0 To dv.Count - 1
                            With dv(i)
                                If auc_id <> .Item("auction_id") Or sce_id <> .Item("email_schedule_id") Or em_id <> .Item("email") Then
                                    SqlHelper.ExecuteNonQuery(DBConnection, "INSERT INTO tbl_auction_email_schedule_statuses(email_schedule_id, auction_id,email_id) VALUES (" & .Item("email_schedule_id") & ", " & .Item("auction_id") & ",'" & .Item("email") & "')")
                                End If

                                auc_id = .Item("auction_id")
                                sce_id = .Item("email_schedule_id")
                                em_id = .Item("email")
                                span = .Item("start_date").Subtract(DateTime.Now)

                                If auction_title = "" Then
                                    auction_title = .Item("title")
                                Else
                                    auction_title = auction_title & ", " & .Item("title")
                                End If
                                no_of_days = span.Days
                                start_date = .Item("start_date")
                                end_date = .Item("display_end_time")

                                If buyer_name.Trim() = "" Then
                                    buyer_name = .Item("name").ToString().Trim()
                                End If
                                If buyer_email.Trim() = "" Then
                                    buyer_email = .Item("email")
                                End If

                                rept_body = rept_body & "<br /><br />PCS Wireless Auctions would like to invite you to participate in the upcoming auction for " & .Item("title") & ".<br /><br /> Please remember to log in on " & Convert.ToDateTime(start_date.ToString()).ToShortDateString() & " at " & Convert.ToDateTime(start_date).ToShortTimeString() & " when bidding first opens. "
                                If .Item("auction_type_id") = 3 Then
                                    rept_body = rept_body & "The minimum bidding amount is $" & FormatNumber(.Item("start_price"), 2) & ". "
                                End If
                                rept_body = rept_body & "Bidding is scheduled to close on " & Convert.ToDateTime(end_date.ToString()).ToShortDateString() & " at " & Convert.ToDateTime(end_date).ToShortTimeString() & "."
                            End With

                        Next
                        body = body.Replace("<<repeated_body_content>>", rept_body)
                        body = body.Replace("<<bidder_name>>", buyer_name)
                        body = body.Replace("<<auction_title>>", auction_title)
                        'body = body.Replace("<<auction_start_date>>", Convert.ToDateTime(start_date.ToString()).ToShortDateString())
                        'body = body.Replace("<<auction_start_time>>", Convert.ToDateTime(start_date).ToShortTimeString())
                        'body = body.Replace("<<bidding_start_price>>", FormatNumber(start_price, 2))
                        'body = body.Replace("<<auction_end_date>>", Convert.ToDateTime(end_date.ToString()).ToShortDateString())
                        'body = body.Replace("<<auction_end_time>>", Convert.ToDateTime(end_date).ToShortTimeString())
                        body = body.Replace("<<site_url>>", ServerHttp)
                        body = body.Replace("<<bidder_id>>", EncryptDecrypt.EncryptValueFormatted(buyer_id))
                        'Label1.Text = Label1.Text & "Before Send Mail " & buyer_email
                        sendEmail(str_subject, body, buyer_email, buyer_name)
                    Next
                End If
            End If
        Catch ex As Exception
            sendEmail("Error in Auction automail exe", "sendInvitationBeforeMail : " & ex.Message, erroremail, "Bimal Giri")
        End Try
    End Sub
    Private Sub sendInvitationBeforeEndMail()
        Try


            Dim strQuery As String = "select E.email_schedule_id,A.auction_id,A.auction_type_id,A.title,A.start_date,A.display_end_time,A.start_price,B.first_name as name,B.email,B.buyer_id,ISNULL(IMG.stock_image_id, 0) AS stock_image_id,ISNULL(IMG.filename, '') AS filename from dbo.fn_get_email_schedule_list() F inner join tbl_auction_email_schedules E on E.email_schedule_id=F.email_schedule_id inner join tbl_auctions A on A.auction_id=F.auction_id  left join tbl_auction_stock_images IMG on A.auction_id=IMG.auction_id and IMG.stock_image_id=(SELECT top 1 stock_image_id from tbl_auction_stock_images where auction_id=a.auction_id order by position) inner join tbl_reg_buyer_users B on B.buyer_id=F.buyer_id and B.is_admin=1 AND upper(E.schedule_type)='BEFORE END'"
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable(DBConnection, strQuery)

            Dim dtBidders As New DataTable()
            Dim ds As New DataSet()
            Dim strBidders As String = "select distinct buyer_id from dbo.fn_get_email_schedule_list() F inner join tbl_auction_email_schedules S on F.email_schedule_id=S.email_schedule_id WHERE UPPER(S.schedule_type)='BEFORE END'"
            dtBidders = SqlHelper.ExecuteDatatable(DBConnection, strBidders)

            Dim buyer_email As String = ""
            Dim buyer_name As String = ""
            Dim auction_title As String = ""
            Dim start_price As Double = 0
            Dim start_date As DateTime = "1/1/1999"
            Dim end_date As DateTime = "1/1/1999"

            Dim span As TimeSpan
            Dim time_left As String = ""
            Dim no_of_days As Integer = 0
            Dim auc_id As Integer = 0
            Dim sce_id As Integer = 0
            Dim em_id As String = ""
            Dim stock_image_id As Integer
            Dim filename As String
            Dim email_caption As String = "Send Invitation To Bidder For Bidding (Dynamic)"
            Dim body As String = ""
            Dim strSubject As String = ""
            Dim body_html As String = ""
            Dim rept_body As String = ""
            Dim subj As String = ""
            Dim kount As Integer = 0
            ds = GetEmail_ByCode("em8")
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                body_html = ds.Tables(0).Rows(0)("html_body")
                strSubject = ds.Tables(0).Rows(0)("email_subject")
            End If

            If dtBidders.Rows.Count > 0 And body_html <> "" Then
                For j As Integer = 0 To dtBidders.Rows.Count - 1
                    body = body_html
                    subj = strSubject
                    buyer_name = ""
                    buyer_email = ""
                    rept_body = ""
                    auction_title = ""
                    kount = 0
                    Dim buyer_id As Integer = 0
                    buyer_id = dtBidders.Rows(j)("buyer_id")
                    Dim dv As DataView = dt.DefaultView
                    dv.RowFilter = "buyer_id=" & dtBidders.Rows(j)("buyer_id")
                    For i As Integer = 0 To dv.Count - 1
                        With dv(i)
                            If auc_id <> .Item("auction_id") Or sce_id <> .Item("email_schedule_id") Or em_id <> .Item("email") Then
                                SqlHelper.ExecuteNonQuery(DBConnection, "INSERT INTO tbl_auction_email_schedule_statuses(email_schedule_id, auction_id,email_id) VALUES (" & .Item("email_schedule_id") & ", " & .Item("auction_id") & ",'" & .Item("email") & "')")
                            End If
                            kount = kount + 1
                            auc_id = .Item("auction_id")
                            sce_id = .Item("email_schedule_id")
                            em_id = .Item("email")
                            span = .Item("start_date").Subtract(DateTime.Now)
                            auction_title = .Item("title")

                            no_of_days = span.Days
                            start_date = .Item("start_date")
                            end_date = .Item("display_end_time")
                            If buyer_name.Trim() = "" Then
                                buyer_name = .Item("name").ToString().Trim()
                            End If
                            If buyer_email.Trim() = "" Then
                                buyer_email = .Item("email")
                            End If


                            filename = .Item("filename")
                            stock_image_id = .Item("stock_image_id")

                            rept_body = rept_body & "<br /><br />Time is running out to bid on this auction " & .Item("title") & "! The auction is scheduled to close at " & end_date & "."
                        End With


                    Next

                    body = body.Replace("<<repeated_body_content>>", rept_body)
                    body = body.Replace("<<bidder_name>>", buyer_name)
                    body = body.Replace("<<site_url>>", ServerHttp)

                    body = body.Replace("<<bidder_id>>", EncryptDecrypt.EncryptValueFormatted(buyer_id))
                    If kount > 1 Then
                        subj = "Time is running out! Bid now on Auctions!"
                    Else
                        subj = subj.Replace("<<auction_title>>", auction_title)
                    End If
                    body = body.Replace("<<site_url>>", ServerHttp)
                    sendEmail(subj, body, buyer_email, buyer_name)
                Next
            End If
        Catch ex As Exception
            sendEmail("Error in Auction automail exe", "sendInvitationBeforeEndMail : " & ex.Message, erroremail, "Bimal Giri")
        End Try
    End Sub
    Private Sub sendInvitationAfterMail()
        Try


            Dim strQuery As String = "select E.email_schedule_id,A.auction_id,A.auction_type_id,A.title,A.start_date,A.display_end_time,A.start_price,B.first_name as name,B.email,B.buyer_id from dbo.fn_get_email_schedule_list() F inner join tbl_auction_email_schedules E on E.email_schedule_id=F.email_schedule_id inner join tbl_auctions A on A.auction_id=F.auction_id inner join tbl_reg_buyer_users B on B.buyer_id=F.buyer_id and B.is_admin=1 and upper(E.schedule_type)='AFTER END'"
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable(DBConnection, strQuery)
            Dim email_caption As String = "Thank You Mail For Bidder After Bid (Dynamic)"
            Dim dtBidders As New DataTable()

            Dim strBidders As String = "select distinct buyer_id from dbo.fn_get_email_schedule_list() F inner join tbl_auction_email_schedules S on F.email_schedule_id=S.email_schedule_id WHERE UPPER(S.schedule_type)='AFTER END'"
            dtBidders = SqlHelper.ExecuteDatatable(DBConnection, strBidders)

            Dim buyer_email As String = ""
            Dim buyer_name As String = ""
            Dim auction_title As String = ""

            Dim span As TimeSpan
            Dim time_left As String = ""
            Dim no_of_days As Integer = 0
            Dim auc_id As Integer = 0
            Dim sce_id As Integer = 0
            Dim em_id As String = ""
            Dim body As String = ""
            Dim email_body As String = ""
            Dim str_subject As String = ""

            Dim ds As New DataSet
            ds = GetEmail_ByCode("em1")
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                email_body = ds.Tables(0).Rows(0)("html_body")
                str_subject = ds.Tables(0).Rows(0)("email_subject")
                email_caption = ds.Tables(0).Rows(0)("email_name")
                If dtBidders.Rows.Count > 0 Then
                    For j As Integer = 0 To dtBidders.Rows.Count - 1
                        body = email_body
                        buyer_name = ""
                        buyer_email = ""

                        Dim buyer_id As Integer = 0
                        buyer_id = dtBidders.Rows(j)("buyer_id")
                        Dim dv As DataView = dt.DefaultView
                        dv.RowFilter = "buyer_id=" & dtBidders.Rows(j)("buyer_id")
                        Dim auction_title_temp As String = ""
                        For i As Integer = 0 To dv.Count - 1
                            With dv(i)
                                If auc_id <> .Item("auction_id") Or sce_id <> .Item("email_schedule_id") Or em_id <> .Item("email") Then
                                    SqlHelper.ExecuteNonQuery(DBConnection, "INSERT INTO tbl_auction_email_schedule_statuses(email_schedule_id, auction_id,email_id) VALUES (" & .Item("email_schedule_id") & ", " & .Item("auction_id") & ",'" & .Item("email") & "')")
                                End If

                                auc_id = .Item("auction_id")
                                sce_id = .Item("email_schedule_id")
                                em_id = .Item("email")
                                span = .Item("start_date").Subtract(DateTime.Now)

                                auction_title = .Item("title")
                                no_of_days = span.Days

                                If buyer_name.Trim() = "" Then
                                    buyer_name = .Item("name").ToString().Trim()
                                End If
                                If buyer_email.Trim() = "" Then
                                    buyer_email = .Item("email")
                                End If
                            End With
                            auction_title_temp = auction_title_temp & "<br />" & auction_title
                        Next
                        body = body.Replace("<<auction_title>>", auction_title_temp)
                        body = body.Replace("<<bidder_name>>", buyer_name)
                        body = body.Replace("<<site_url>>", ServerHttp)
                        body = body.Replace("<<bidder_id>>", EncryptDecrypt.EncryptValueFormatted(buyer_id))
                        sendEmail(str_subject, body, buyer_email, buyer_name)
                    Next
                End If
            End If
        Catch ex As Exception
            sendEmail("Error in Auction automail exe", "sendInvitationAfterMail : " & ex.Message, erroremail, "Bimal Giri")
        End Try
    End Sub
    Public Sub ReadTextFile()
        Try
            Dim oRead As System.IO.StreamReader
            Dim LineIn As String = ""
            oRead = File.OpenText(Application.StartupPath & "\DBConnection.ini")
            ' While oRead.Peek - 1
            LineIn &= oRead.ReadLine()
            'End While

            oRead.Close()

            Dim arr1 As String()
            Dim arr2 As String()

            arr1 = LineIn.Split(";")
            LineIn = ""

            For i As Integer = 0 To arr1.Length - 1
                arr2 = arr1(i).Split(",")
                If arr2.Length = 2 Then
                    LineIn = LineIn & arr2(0) & "=" & EncryptDecrypt.DecryptValue(arr2(1)) & ";"
                End If
            Next
            DBConnection = LineIn
        Catch ex As Exception
            sendEmail("Error in Auction automail exe", "ReadTextFile : " & ex.Message, erroremail, "Bimal Giri")
        End Try

    End Sub
    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Timer1.Enabled = False
        Close()
    End Sub
    Private Sub sendEmail(ByVal subject As String, ByVal body As String, ByVal to_email As String, ByVal buyer_name As String)
        Try
            Dim obj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient()

            Dim msgMail As New System.Net.Mail.MailMessage

            msgMail.IsBodyHtml = True
            msgMail.From = New System.Net.Mail.MailAddress("auctions@pcsww.com", "PCS Wireless Auctions")
            msgMail.To.Add(New System.Net.Mail.MailAddress(to_email, buyer_name))
            'msgMail.ReplyTo = New System.Net.Mail.MailAddress("auctions@pcsww.com", "PCS Wireless Auctions")
            If to_email <> erroremail Then
                msgMail.Bcc.Add(New System.Net.Mail.MailAddress(bccemail, "PCS Wireless Auctions"))
            End If
            obj.Host = "smtp.sendgrid.net"
            obj.UseDefaultCredentials = True
            obj.Credentials = New System.Net.NetworkCredential("auctions", "buyback@123!@3212")
            obj.Port = 25
            msgMail.Subject = subject
            msgMail.Body = body
            'Label1.Text = Label1.Text & "In Send Mail " & erroremail & bccemail & to_email
            If is_server = 1 Then
                'Label1.Text = Label1.Text & " - sending email."
                obj.Send(msgMail)
                SqlHelper.ExecuteNonQuery(DBConnection, "INSERT INTO tbl_sec_email_log(created_date, from_address, from_name, subject, body, to_address, to_name, bcc_address, bcc_name, cc_address, cc_name, is_send) VALUES (getdate(), '" & "auctions@pcsww.com" & "','" & "PCS Wireless Auctions" & "','" & subject.Replace("'", "''") & "','" & body.Replace("'", "''") & "','" & to_email & "','" & buyer_name & "','" & bccemail & "','" & "PCS Wireless Auctions" & "','','',1)")

            End If

        Catch ex As Exception
            'Label1.Text = Label1.Text & " - error in sending email."
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Close()
    End Sub
End Class
