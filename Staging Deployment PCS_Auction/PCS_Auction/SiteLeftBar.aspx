﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SiteLeftBar.aspx.vb" Inherits="SiteLeftBar"
    EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link type="text/css" rel="Stylesheet" href="/Style/backend_leftbar.css" />
    <script type="text/javascript" language="JavaScript">

        var hide_menu_cols;
        var show_menu_cols;

        function init() {
            // load menu vars
            var framset_menu = parent.window.document.getElementById('framset_menu');
            hide_menu_cols = framset_menu.getAttribute('hide_menu_width') + ',*';
            show_menu_cols = framset_menu.getAttribute('show_menu_width') + ',*';

            // check if menu is hidden
            var framset_menu = parent.window.document.getElementById('framset_menu');
            if (framset_menu.cols == hide_menu_cols) {
                framset_menu.cols = '220,*'
                framset_menu.setAttribute('show_menu_width', 220)
                document.getElementById('img_drag').src = "/Images/hide.png";
                document.getElementById('div_leftbar_arrow').className = "arrow_expanded";  //("class", );
                document.getElementById('div_leftbar').style.display = "block";
            }

            //            // check if menu need to hidden
            //            hu = window.location.search.substring(1);
            //            if (hu != 't=0') {
            //                framset_menu.cols = '20,*'
            //                framset_menu.setAttribute('show_menu_width', 20)
            //                document.getElementById('img_drag').src = "/Images/show.png";
            //                document.getElementById('div_leftbar_arrow').className = "arrow_collapsed";
            //                document.getElementById('div_leftbar').style.display = "none";
            //            }
            //            else {
            //                document.getElementById('div_leftbar_arrow').className = "arrow_expanded";

            //            }
        }

        function closeLeftBar() {

            // load menu vars
            var framset_menu = parent.window.document.getElementById('framset_menu');
            hide_menu_cols = framset_menu.getAttribute('hide_menu_width') + ',*';
            show_menu_cols = framset_menu.getAttribute('show_menu_width') + ',*';
            // check if menu is hidden
            var framset_menu = parent.window.document.getElementById('framset_menu');
            if (framset_menu.cols == hide_menu_cols) {
                framset_menu.cols = '220,*'
                framset_menu.setAttribute('show_menu_width', 220)
                document.getElementById('img_drag').src = "/Images/hide.png";
                document.getElementById('div_leftbar_arrow').className = "arrow_expanded";
                document.getElementById('div_leftbar').style.display = "block";
            }
            else {
                framset_menu.cols = '20,*'
                framset_menu.setAttribute('show_menu_width', 20)
                document.getElementById('img_drag').src = "/Images/show.png";
                document.getElementById('div_leftbar_arrow').className = "arrow_collapsed";
                document.getElementById('div_leftbar').style.display = "none";
            }

        }

        function redirectIframe(path1, path2, path3) {
            if (path1 != '')
                top.topFrame.location = path1;
            if (path2 != '')
                top.leftFrame.location = path2;
            if (path3 != '')
                top.mainFrame.location = path3;
        }
        function redirectPage(path) {
            if (path != '')
                top.location.href = path;
        }

        // (C) 2004 www.CodeLifter.com
        // Free for all users, but leave in this header
        function framePrint(whichFrame) {
            parent[whichFrame].focus();
            parent[whichFrame].print();
        }
    </script>
</head>
<body onload="init();" style="height: 100%; padding-left: 2px;">
    <form id="form1" runat="server">
    <div id="div_leftbar_arrow" class="arrow_expanded">
        <img src="/Images/hide.png" id="img_drag" width="15" height="92" onclick="closeLeftBar();"
            style="cursor: pointer;"></div>
    <div id="div_leftbar" style="float: left; padding-top: 20px; width: 200px; display: block;
        height: 400px;">
        <div id="tab0" runat="server" visible="false">
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/MyAccount.aspx?t=1#1')">
                    Bidder Information</a>
            </div>
            <% If CommonCode.Fetch_Cookie_Shared("is_buyer").ToString = "0" Then%>
            <div runat="server" id="div_personal" visible="true" class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/MyAccount.aspx?t=7#7')">
                    Personal Info</a>
            </div>
            <% End If%>
            <% If CommonCode.Fetch_Cookie_Shared("is_buyer").ToString = "1" Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/MyAccount.aspx?t=2#2')">
                    Sub-Logins</a>
            </div>
            <% End If%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/MyBids.aspx?t=4&c=1&mode=w')">
                    Auctions Won</a>
            </div>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/MyBids.aspx?t=9&mode=q')">
                    My Offers</a>
            </div>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/MyBids.aspx?t=2&mode=pr')">
                    Private Offers</a>
            </div>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/MyBids.aspx?t=8&mode=pa')">
                    Partial Offers</a>
            </div>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/MyBids.aspx?t=8&mode=bu')">
                    Buy it Now</a>
            </div>
            <%--<div class="leftmnu" id="div_myquery" runat="server">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/MyQueries.aspx?t=10')">My Queries</a>
            </div>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/AuctionList.aspx?t=5')">Current Bid</a>
            </div>--%>
        </div>
        <div id="tab1" runat="server" visible="false">
            <asp:Panel ID="pnl_auctions" runat="server">
                <div class="leftmnuF" style="margin-top: 10px;">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/AuctionList.aspx?t=<%=Request.QueryString.Get("t") %>&c=1')">
                        Auctions </a>
                </div>
                <asp:Repeater ID="rpt_auctions" runat="server">
                    <ItemTemplate>
                        <div class="leftSubmnuF" id="div_category" runat="server">
                            <a href="javascript:void(0);" onclick="redirectIframe('','','/AuctionList.aspx?t=<%=Request.QueryString.Get("t") %>&c=1&p=<%# Eval("product_catetory_id") %>')">
                                <%# Eval("name") & " (" & Eval("no_of_auction") & ")"%></a>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </asp:Panel>
            <asp:Panel ID="pnl_buy_now" runat="server">
                <div class="leftmnuF" style="margin-top: 10px;">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/AuctionList.aspx?t=<%=Request.QueryString.Get("t") %>&c=2')">
                        Buy Now Items </a>
                </div>
                <asp:Repeater ID="rpt_buy_now" runat="server">
                    <ItemTemplate>
                        <div class="leftSubmnuF" id="div_category" runat="server">
                            <a href="javascript:void(0);" onclick="redirectIframe('','','/AuctionList.aspx?t=<%=Request.QueryString.Get("t") %>&c=2&p=<%# Eval("product_catetory_id") %>')">
                                <%# Eval("name") & " (" & Eval("no_of_auction") & ")"%></a>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </asp:Panel>
            <asp:Panel ID="pnl_quote" runat="server">
                <div class="leftmnuF" style="margin-top: 10px;">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/AuctionList.aspx?t=<%=Request.QueryString.Get("t") %>&c=3')">
                        Offers </a>
                </div>
                <asp:Repeater ID="rpt_quote" runat="server">
                    <ItemTemplate>
                        <div class="leftSubmnuF" id="div_category" runat="server">
                            <a href="javascript:void(0);" onclick="redirectIframe('','','/AuctionList.aspx?t=<%=Request.QueryString.Get("t") %>&c=3&p=<%# Eval("product_catetory_id") %>')">
                                <%# Eval("name") & " (" & Eval("no_of_auction") & ")"%></a>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </asp:Panel>
        </div>
        <br />
        <br />
        <div class="leftmnu">
            <a href="javascript:void(0);" onclick="redirectIframe('/SiteTopBar.aspx?t=1&f=<%=(mnu_fav+1) mod 2 %>','/SiteLeftBar.aspx?t=1&f=<%=(mnu_fav+1) mod 2 %>','/AuctionList.aspx?t=1')">
                <%= IIf(mnu_fav = 0, "Show Favorites", "Hide Favorites")%></a>
        </div>
        <div class="printFrame">
            <a href="javascript:framePrint('mainFrame');">Print the Main Page</a>
        </div>
    </div>
    </form>
</body>
</html>
