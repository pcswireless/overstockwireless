﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EmailContent1.aspx.vb" Inherits="EmailContent1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h4>
            1. Bidder Registration Mail</h4>
        <asp:Literal ID="lit_Registration_Mail" runat="server"></asp:Literal>
        <hr />
        <h4>
            2. Bidder Registration Alert Mail to Admin(Auto Approve Bidder unchecked)</h4>
        <asp:Literal ID="lit_Bidderd_Admin_Active" runat="server"></asp:Literal>
        <hr />
        <h4>
            3. Bidder Registration Auto Approval Mail(Auto Approve Bidder checked)</h4>
        <asp:Literal ID="lit_Registration_Approve_Mail" runat="server"></asp:Literal>
        <hr />
        <h4>
            4. Change Password Mail</h4>
        <asp:Literal ID="lit_Send_ChangePassword_Mail" runat="server"></asp:Literal>
        <hr />
        <h4>
            5. Forgot Password Mail</h4>
        <asp:Literal ID="lit_Send_Forgot_Pwd" runat="server"></asp:Literal>
        <hr />
        <h4>
            6. Buyer Approval Mail</h4>
        <asp:Literal ID="lit_send_buyer_approval_email" runat="server"></asp:Literal>
        <hr />
        <h4>
            7. Buyer Status change to "Pending"</h4>
        <asp:Literal ID="lit_send_buyer_status_changed_email_Pending" runat="server"></asp:Literal>
        <hr />
        <h4>
            8. Buyer Status change to "More Details"</h4>
        <asp:Literal ID="lit_send_buyer_status_changed_email_details" runat="server"></asp:Literal>
        <hr />
        <h4>
            9. Buyer Status change to "On Hold"</h4>
        <asp:Literal ID="lit_send_buyer_status_changed_email_hold" runat="server"></asp:Literal>
        <hr />
        <h4>
            10. Buyer Status change to "Reject"</h4>
        <asp:Literal ID="lit_send_buyer_status_changed_email_Reject" runat="server"></asp:Literal>
        <hr />
        <h4>
            11. Buyer Status change to "Inactive"</h4>
        <asp:Literal ID="lit_send_buyer_status_changed_email_Inactive" runat="server"></asp:Literal>
        <hr />
        <h4>
            12. Buyer sub login creation email (with active status)</h4>
        <asp:Literal ID="lit_send_bidder_sub_login_creation_email_active" runat="server"></asp:Literal>
        <hr />
        <h4>
            13. Buyer sub login creation email (with inactive status)</h4>
        <asp:Literal ID="lit_send_bidder_sub_login_creation_email_inactive" runat="server"></asp:Literal>
        <hr />
        <h4>
            14. Buyer sub login change status (set to active)</h4>
        <asp:Literal ID="lit_send_bidder_sub_login_active_status_changed_email_active" runat="server"></asp:Literal>
        <hr />
        <h4>
            15. Buyer sub login change status (set to inactive)</h4>
        <asp:Literal ID="lit_send_bidder_sub_login_active_status_changed_email_inactive"
            runat="server"></asp:Literal>
        <hr />
        <h4>
            16. User creation email (with active status)</h4>
        <asp:Literal ID="lit_send_employee_creation_email_active" runat="server"></asp:Literal>
        <hr />
        <h4>
            17. User creation email (with inactive status)</h4>
        <asp:Literal ID="lit_send_employee_creation_email_inactive" runat="server"></asp:Literal>
        <hr />
        <h4>
            18. User change status (set to active)</h4>
        <asp:Literal ID="lit_send_employee_active_status_changed_email_active" runat="server"></asp:Literal>
        <hr />
        <h4>
            19. User change status (set to inactive)</h4>
        <asp:Literal ID="lit_send_employee_active_status_changed_email_inactive" runat="server"></asp:Literal>
        <hr />
        <h4>
            20. Bidder Query(Sales Rep Email)</h4>
        <asp:Literal ID="lit_set_rep_contact_request_to_salesrep" runat="server"></asp:Literal>
        <hr />
        <h4>
            21. Bidder Query(Bidder self email)</h4>
        <asp:Literal ID="lit_set_rep_contact_request_to_user" runat="server"></asp:Literal>
        <hr />
        <h4>
            22. Bidder Query(Sales Rep Reply to Bidder)</h4>
        <asp:Literal ID="lit_Send_Bidder_Reply" runat="server"></asp:Literal>
        <hr />
        <h4>
            23. Bidder Out Of Bid Mail</h4>
        <asp:Literal ID="lit_sendOutOfBidMail" runat="server"></asp:Literal>
        <hr />
        <h4>
            24. Order Confirmation(when bid placed)</h4>
        <font color="red"><b>(stopped right now)</b></font><br />
        <asp:Literal ID="lit_Send_Auction_Order_Mail_order_23" runat="server"></asp:Literal>
        <hr />
        <h4>
            25. Request for offer confirmation(Frontend)</h4>
        <asp:Literal ID="lit_Send_Auction_Order_Mail_order_4" runat="server"></asp:Literal>
        <hr />
        <h4>
            26. Buy now confirmation(Frontend)</h4>
        <asp:Literal ID="lit_Send_Auction_Order_Mail_order_5" runat="server"></asp:Literal>
        <hr />
        <h4>
            27. Private offer confirmation(Frontend)</h4>
        <asp:Literal ID="lit_Send_Auction_Order_Mail_order_6" runat="server"></asp:Literal>
        <hr />
        <h4>
            28. Partial offer confirmation(Frontend)</h4>
        <asp:Literal ID="lit_Send_Auction_Order_Mail_order_7" runat="server"></asp:Literal>
        <hr />
        <h4>
            29. Bid Awarded(Backend)</h4>
        <asp:Literal ID="lit_SendWinningBidEmail_Awarded" runat="server"></asp:Literal>
        <hr />
        <h4>
            30. Bid Denied(Lose)(Backend)</h4>
        <asp:Literal ID="lit_SendWinningBidEmail_Denied" runat="server"></asp:Literal>
        <hr />
        <h4>
            31. Buy Now Offer Accept Email(Backend)</h4>
        <asp:Literal ID="lit_SendBuyNowAcceptEmail_now" runat="server"></asp:Literal>
        <hr />
        <h4>
            32. Private Offer Accept Email(Backend)</h4>
        <asp:Literal ID="lit_SendBuyNowAcceptEmail_private" runat="server"></asp:Literal>
        <hr />
        <h4>
            33. Partial Offer Accept Email(Backend)</h4>
        <asp:Literal ID="lit_SendBuyNowAcceptEmail_partial" runat="server"></asp:Literal>
        <hr />
        <h4>
            34. Request for Offer Accept Email(Backend)</h4>
        <asp:Literal ID="lit_SendQuotationAcceptEmail" runat="server"></asp:Literal>
        <hr />
        <h4>
            35. Invitation Email (Before Start)</h4>
        <asp:Literal ID="lit_sendInvitationBeforeMail" runat="server"></asp:Literal>
        <hr />
        <h4>
            36. Invitation Email (Before End)</h4>
        <asp:Literal ID="lit_sendInvitationBeforeEndMail" runat="server"></asp:Literal>
        <hr />
        <h4>
            37. Invitation Email (After End)</h4>
        <asp:Literal ID="lit_sendInvitationAfterMail" runat="server"></asp:Literal>
        <hr />
    </div>     </form>
</body>
</html>
