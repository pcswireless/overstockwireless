﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="auction_detail_print.aspx.vb" Inherits="auction_detail_print" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="/Style/elastislide.css" rel="stylesheet" type="text/css" />
    <link href="/style/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/Style/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="/source/jquery.fancybox.css?v=2.1.5"
        media="screen" />
     <script type="text/javascript">

         /***********************************************
         * Different CSS depending on OS (mac/pc)- © Dynamic Drive (www.dynamicdrive.com)
         * This notice must stay intact for use
         * Visit http://www.dynamicdrive.com/ for full source code
         ***********************************************/
         ///////No need to edit beyond here////////////

         var mactest = navigator.userAgent.indexOf("Mac") != -1


         if (mactest) {
             document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet_mac.css"/>');
         }
         else {
             document.write('<link rel="stylesheet" type="text/css" href="/style/newstylesheet.css"/>');
         }
  </script>
     
    <script type="text/javascript" src="/js/jquery.js"></script>    
 <script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
   
    <script type="text/javascript">
        document.createElement('header');
        document.createElement('footer');
        document.createElement('section');
        document.createElement('aside');
        document.createElement('nav');
        document.createElement('article');
        document.createElement('hgroup');

        function question_wait(_valdt_grp, _aid) {
            if (Page_ClientValidate(_valdt_grp)) {
                var dv_ask = document.getElementById('dv_' + _aid + '_ask');
                var dv_msg = document.getElementById('dv_' + _aid + '_msg');
                var dv_cnf = document.getElementById('dv_' + _aid + '_confirm');
                if (dv_ask.style.display == 'block') {
                    dv_ask.style.display = 'none';
                    dv_cnf.style.display = 'none';
                    dv_msg.style.display = 'block';
                }
                else {
                    dv_ask.style.display = 'block';
                    dv_cnf.style.display = 'block';
                    dv_msg.style.display = 'none';
                }
            }
        }


        function change_category() {
            document.getElementById("id_category").innerHTML = "d"
        }


    </script>	
    <script type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-46171169-1', 'pcsww.com');
        ga('send', 'pageview');

</script>
     <style type="text/css">
        @media print
        {
            .noprint
            {
                display: none;
            }
           .item_size {
               font-size:14px;
               
              
           }
         .table {
             border:1px solid black;
         }
        }
      
    </style>
   
    <script type="text/javascript">
        $(function () {

            $(".typtxt").each(function () {
                $tb = $(this);
                if ($tb.val() != this.title) {

                }
            });

            $(".typtxt").focus(function () {
                $tb = $(this);
                if ($tb.val() == this.title) {
                    $tb.val("");

                }
            });

            $(".typtxt").blur(function () {
                $tb = $(this);
                if ($.trim($tb.val()) == "") {
                    $tb.val(this.title);
                    $tb.addClass("typtxt");
                }
            });
        });

    </script>
    <style type="text/css">
        .fancybox-custom .fancybox-skin {
            box-shadow: 0 0 50px #222;
        }
    </style>
    <script type="text/javascript" src="/js/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/js/jquery.elastislide.js"></script>
    <script type="text/javascript" src="/js/gallery.js"></script>
    <script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">
        <div class="rg-image-wrapper">
            {{if itemsCount > 1}}
        <div class="rg-image-nav">
        </div>
            {{/if}}
        <div class="rg-image" style="padding-left:5px\9;padding-right:5px\9;">
        </div>
            <div class="rg-loading">
            </div>
            <div class="rg-caption-wrapper">
                <div class="rg-caption" style="display: none;">
                    <p>
                    </p>
                </div>
            </div>
        </div>
    </script>
</head>
<body>
    <form id="form1" runat="server">
         <div class="wraper content">
             <asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
    <asp:HiddenField ID="hid_auction_type" runat="server" Value="0" />
    <div>
        <div class="innerwraper wraper1">
                      
            <div id="div_print">
                <div class="products1">
                    <div class="detailbxwrap" style="width:900px; background-color:#ffffff;">
                       
                        
                        <div style="float:left; width:250px; padding-top:20px;">
                            
                                        <asp:image ID="imgMain" runat="server" />
                                       
                                
                        </div>
                     
                        <!--lftside -->
                        <div class="rightside" style="width:570px;">
                            <h1>
                                <asp:Literal ID="ltr_title" Text="title" runat="server" /></h1>
                            <span class="grytxt">
                                <asp:Literal ID="ltr_sub_title" Text="sub title" runat="server" /></span><br />
                            <span class="grytxt">Auction # : <strong>
                                <asp:Literal ID="ltr_auction_code" runat="server" /></strong></span>
                            <div class="prodetail">
                                <asp:Panel runat="server" ID="pnl_from_live">
                                    <iframe id="iframe_price_summary" width="100%" height="200px" scrolling="no" frameborder="0" runat="server"></iframe>
                             
                                    <div class="clear">
                                    </div>
                                    <iframe id="iframe_auction_rank" scrolling="no" frameborder="0" width="100%" runat="server"
                                        height="55px"></iframe>
                                        <asp:Literal runat="server" ID="ltr_quote_msg"></asp:Literal>
                                    <div class="clear">
                                    </div>
                                    <div style=" padding-top:15px;padding-left:20px; font-family: 'dinbold'; font-size:18px; color:#575757; font-weight:normal;">
                                        <div class="tmlft">
                                            <span style="padding-top:0px;"><asp:Literal runat="server" ID="ltr_timer_caption" Text="Time Left"></asp:Literal></span>
                                        </div>
                                        <!--tmlft -->
                                        <div class="hrtime">
                                            <iframe id="iframe_auction_time" scrolling="no" frameborder="0" width="100%" runat="server"
                                                height="50px"></iframe>
                                        </div>
                                        <!--htime -->
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                    <!--timeklft -->
                                    <div class="shpmthd">
                                        <div style="float: left;">
                                            <asp:Panel runat="server" ID="dv_bidding" Visible="false" style=" padding-top:15px;" CssClass="noprint">
                                                <asp:TextBox runat="server" CssClass="typtxt" ID="txt_max_bid_amt" Text="Enter Your Bid Amount"
                                                    ToolTip="Enter Your Bid Amount"></asp:TextBox>
                                                <span class="timeleft_qty" style="float: left;">
                                                    <asp:Literal runat="server" ID="ltr_buy_qty_or_caption" Text=""></asp:Literal>
                                                </span>
                                                
                                                <asp:DropDownList CssClass="typtxt" ID="ddl_buy_now_qty" Style="width: 100px;" runat="server"
                                                    AutoPostBack="false"  />
                                            </asp:Panel>
                                            <div class="clear">
                                            </div>
                                            <iframe id="iframe_auction_amount" scrolling="no" frameborder="0" width="200px;" runat="server"
                                                height="35px;"></iframe>
                                            <asp:Panel runat="server" ID="dv_bidding1" Visible="false">
                                                <div class="clear">
                                                </div>
                                                <asp:Label ForeColor="Red" Font-Size="22" Text="Sold Out" Font-Names="'dinregular',arial" Visible="false" runat="server" ID="ltr_buynow_close"></asp:Label>
                                            </asp:Panel>
                                        </div>
                                        <div style="float: left;" runat="server" id="dv_shipping" visible="false" class="noprint"> 
                                            <div class="btnship">
                                                <asp:DropDownList ID="ddl_shipping" runat="server" CssClass="typtxt" Style="width: 238px; margin-top:15px;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                    </div>

                                    <asp:HiddenField ID="hid_buy_now_confirm_amount" runat="server" Value="0" />
                                    <div class="clear">
                                    </div>
                                    <!--shpmthd -->
                                    <div class="bidnw">
                                        <div class="cs_Validator" id="div_Validator">
                                            <span id="spn_valid"></span><span id="spn_amt_hlp" style="display: none;">
                                                <asp:Literal ID="ltr_amt_help" runat="server"></asp:Literal>
                                            </span>
                                        </div>
                                        <asp:Panel ID="pnlNoprint" runat="server" CssClass="noprint">

                                            <asp:Button runat="server" ID="but_main_auction" CssClass="bid" Text="Bid Now" style=" visibility:hidden;" />
                                            <asp:Literal runat="server" ID="ltr_offer_button"></asp:Literal>
                                        </asp:Panel>
                                        <!---------------------------------------------------------->
                                        <div id="buynow-popup" style="display: none;">
                                            <h2>
                                                <asp:Literal ID="ltr_buy_now_pop_heading" runat="server" Text=""></asp:Literal>
                                            </h2>
                                            <div class="inputboxes" style="width:550px;">
                                                <div class="fleft qnty">
                                                    <span class="text">I Want to buy</span>
                                                    <asp:DropDownList ID="ddl_buy_pop_qty" CssClass="slc typtxt" runat="server" AutoPostBack="false" />
                                                    <span class="text">unit(s) for </span>
                                                    <asp:HiddenField ID="hid_buy_now_pop_amount" runat="server" Value="0" />
                                                </div>
                                                <asp:Label runat="server" ID="lbl_popmsg_msg" CssClass="orange"></asp:Label>
                                                <div class="fleft">
                                                    <span class="text">$ </span><span class="text">
                                                        <asp:Literal ID="ltr_buy_pop_amt" runat="server" Text=""></asp:Literal>/unit</span>
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <p class="orange">
                                                Please note that PCSWireless reserve the to cancel any order.
                                            </p>
                                            <p class="orange">
                                                Rates are subject to change. Additional shipping and handing fees may apply
                                            </p>
                                            <p class="checkbox">
                                                <asp:CheckBox ID="chk_accept_term" runat="server" Text=" Accept our Terms & Conditions" />
                                            </p>
                                            <div class="clear">
                                            </div>
                                            <asp:Button runat="server" ID="btn_buy_now" CssClass="fleft" Text="Buy Now" />
                                            <input type="button" name="" id="popupClose" class="fleft" value="Cancel" />
                                        </div>
                                        <div id="bidnow-popup" style="display: none;">
                                            <iframe runat="server" id="iframe_auction_window" scrolling="no" frameborder="1"></iframe>
                                        </div>
                                       <!---------------------------------------------------------->
                                        <div class="clear">
                                        </div>
                                        <p>
                                            <asp:Literal ID="ltr_bid_note" runat="server"></asp:Literal>
                                        </p>
                                    </div>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnl_from_account">
                                    <asp:Literal runat="server" ID="ltr_offer_attachement"></asp:Literal>
                                    <asp:Literal runat="server" ID="ltr_current_status"></asp:Literal>
                                </asp:Panel>
                                <div class="clear">
                                </div>
                                <!--bidnow -->
                            </div>
                            <!--prodetail -->
                        </div>
                        <!--rightside -->
                        <div class="clear">
                        </div>
                    </div>
                    <!--detailbxwrap -->
                    <div class="clear">
                    </div>
                    <div class="middlerow">
                        <div class="askbx">
                            <span class="noprint">
                                <asp:LinkButton runat="server" ID="lnk_my_auction" CssClass="actn" CommandName="ins"
                                    Text="Add To My Live Auctions" Visible="false" ></asp:LinkButton>
                                <asp:Literal ID="lit_ask_question" runat="server" Visible ="false"  /></span>
                           
                            <div class="clear">
                            </div>
                        </div>
                        <!--ask -->
                       <%-- <asp:Literal runat="server" ID="ltr_bidder_history" Text=""></asp:Literal>--%>
                        <div class="clear">
                        </div>
                    </div>
                    <!--middlerow -->
                    <div id="history-popup" style="display: none;">
                        <div class="inputboxes">
                            <asp:Repeater ID="rep_bid_history" runat="server">
                                <ItemTemplate>
                                    <div style="clear: both;">
                                        <div style="float: left; width: 180px; padding: 5px 0px 5px 10px;">
                                            <b>
                                                <%# Eval("bidder")%>
                                            </b>
                                            <br />
                                            <span class="auctionSubTitle">
                                                <%# Eval("email")%></span>
                                        </div>
                                        <div style="float: left; width:100px; padding: 5px 0px 5px 10px; color: #006FD5;">
                                            <b>
                                                <%# "$" & FormatNumber(Eval("bid_amount"), 2)%></b>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <div style="background-color: #F5F5F5; overflow: hidden; clear: both;">
                                        <div style="float: left; width: 180px; padding: 5px 0px 5px 10px;">
                                            <b>
                                                <%# Eval("bidder")%>
                                            </b>
                                            <br />
                                            <span class="auctionSubTitle">
                                                <%# Eval("email")%></span>
                                        </div>
                                        <div style="float: left; width: 100px; padding: 5px 0px 5px 10px; color: #006FD5;">
                                            <b>
                                                <%# "$" & FormatNumber(Eval("bid_amount"), 2)%></b>
                                        </div>
                                    </div>
                                </AlternatingItemTemplate>
                            </asp:Repeater>
                            <span class="text">
                                <asp:Literal ID="empty_data" runat="server" Text="No bid submitted for this auction."></asp:Literal></span>
                        </div>
                        <div class="clear">
                        </div>
                        <input type="button" name="" id="historyClose" class="fleft" value="Cancel" />
                    </div>
                </div>
                <!--products -->
                <div class="clear">
                </div>
                <asp:Literal runat="server" ID="ltr_summary_detail"></asp:Literal>
                <!--summrybx -->
                <div runat="server" id="dv_items_details" class="details dtltxtbx">
                    <h1 class="pgtitle">Items</h1>

                    <div class="txt6">
                        <div class="item_size">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>Manufacturer
                                        </th>
                                        <th>Title
                                        </th>
                                        <th>Part #
                                        </th>
                                        <th>Quantity
                                        </th>
                                        <th>UPC
                                        </th>
                                        <th>SKU
                                        </th>
                                        <th>Estimated MSRP
                                        </th>
                                        <th>Total MSRP
                                        </th>
                                    </tr>
                                    <tr>
                                        <asp:Repeater ID="rptItems" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="background-color: White;">
                                                        <%# Eval("manufacturer")%>
                                                    </td>
                                                    <td style="background-color: White;">
                                                        <%# Eval("title")%>
                                                    </td>
                                                    <td style="background-color: White;">
                                                        <%# Eval("part_no")%>
                                                    </td>
                                                    <td style="background-color: White;">
                                                        <%# Eval("quantity")%>
                                                    </td>
                                                    <td style="background-color: White;">
                                                        <%# Eval("UPC")%>
                                                    </td>
                                                    <td style="background-color: White;">
                                                        <%# Eval("SKU")%>
                                                    </td>
                                                    <td style="background-color: White;">
                                                        <%# CommonCode.GetFormatedMoney(Eval("estimated_msrp"))%>
                                                    </td>
                                                    <td style="background-color: White;">
                                                        <%# CommonCode.GetFormatedMoney(Eval("total_msrp"))%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--text6 -->
                </div>
                <!--items -->
                <asp:Literal runat="server" ID="ltr_auction_details"></asp:Literal>
                <!--details -->
            </div>
        <!--print content end -->
       <%-- <asp:Literal runat="server" ID="ltr_term_condition"></asp:Literal>--%>
        <!--terms -->
                       
                <div class="wrapper_botm" style="display:none;">
                    <div class="askqstn dtltxtbx">
                        <div id="lft_botm">
                            <div class="ask_txtbx" style="float: left;">
                                <h1 class="pgtitle">Ask a Question</h1>
                                <asp:TextBox runat="server" ID="txt_thread_title" CssClass="qsttxt" Columns="50"
                                    Style="width: 100%; resize: vertical;" TextMode="MultiLine"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfv_create_thread" runat="server" ErrorMessage="*"
                                    ForeColor="red" Display="Static" ControlToValidate="txt_thread_title" ValidationGroup="create_thread"></asp:RequiredFieldValidator>
                                <div style="display: none; font-size: 12px;padding:0;margin:0;" id='dv_askmain_msg'>
                                    <span style="color: Red">Please Wait...</span>
                                </div>
                                    <div id='dv_askmain_ask'>
                                <asp:Button ID="img_btn_create_thread" runat="server" Text="Send" CssClass="sendbtn"
                                    ValidationGroup="create_thread" OnClientClick="askquestion_wait('create_thread','askmain')" />
                                    </div>
                                <div style="width: 100%; float: left; overflow: hidden;font-size: 12px;padding:0;margin:0;" id='dv_askmain_confirm'>
                                    <asp:Label ID="lit_ask_confirmation" EnableViewState="false" runat="server" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div id="rght_botm">
                            <div class="detailsSubHeading" style="font-family: 'dinregular',arial;">
                                <asp:Literal ID="lit_your_query" runat="server" Text="Your Queries"></asp:Literal>
                            </div>
                            <asp:Repeater ID="rep_after_queries" runat="server" OnItemCommand="rep_after_queries_ItemCommand"
                                OnItemDataBound="rep_after_queries_ItemDataBound">
                                <ItemTemplate>
                                    <div style="font-family: 'dinbold',arial; color: #3D3A37; font-size: 14px;">
                                        <%# Container.ItemIndex + 1 & ". " & Eval("title")%>
                                        <div style="padding: 0px 0px 0px 10px; display: inline; position: relative; bottom: -0.2em;">
                                            <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="/Images/close_modal_1.png"
                                                CommandArgument='<%#Eval("query_id") %>' CommandName="deleteQry" AlternateText="Delete Questions"
                                                ToolTip="Delete Questions" />
                                        </div>
                                    </div>
                                    <div style="padding: 0 0 0 10px!important; font-family: dinregularitalic; font-size: 14px; color: Gray; clear: both;">
                                        <%--Eval("company_name") & ", " & Eval("state") & --%>
                                        <%# "on " & Eval("submit_date")%>
                                    </div>
                                    <div style="padding: 0 10px; font-size: 14px;">
                                        <asp:Repeater ID="rep_inner_after_queries" runat="server">
                                            <ItemTemplate>
                                                <div style="font-family: 'dinregular',arial; color: #3D3A37; font-size: 14px;">
                                                    <img src='<%# IIF(Eval("image_path")<>"","/upload/users/" & Eval("user_id") & "/" & Eval("image_path"),"/images/buyer_icon.gif") %>'
                                                        alt="" style="float: left; padding: 2px; height: 25px; width: 23px;" />
                                                    <%#Eval("message") & "<br>" %>
                                                    <span style="font-style: italic; color: Gray;">
                                                        <%# IIf(Eval("buyer_title") = "", Eval("title"), Eval("buyer_title"))%>
                                                        <%# IIf(Eval("first_name") = "", Eval("buyer_first_name") & " " & Eval("buyer_last_name"), Eval("first_name") & " " & Eval("last_name"))%>,
                                                <%# Eval("submit_date")%>
                                                    </span>
                                                </div>
                                                <br />
                                                <br />
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
       


        <!--askqstn -->
    </div>
    <a id="openDummy" href="#bidnow-popup" style="display: none;"></a><a id="closeDummy"
        href="javascript:void(0);" style="display: none;"></a>
    <asp:Button ID="btn_history" runat="server" Style="display: none;" CommandName="history" />
    <asp:Button ID="btn_query" runat="server" Style="display: none;" CommandName="query" />
    </div>
    <script type="text/javascript">
        function refresh_history() {
            document.getElementById('<%=btn_history.ClientID%>').click();
            return true;
        }
        function refresh_query() {
            document.getElementById('<%=btn_query.ClientID%>').click();
            return true;
        }
        $('.btnship').click(function () { $('.shiping-methods').toggle(); });
        $("a#buynow").fancybox({ 'hideOnContentClick': true });
        $("a#openDummy").fancybox({ 'hideOnContentClick': true });
        $('#popupClose').click(function () { $.fancybox.close(); });
        $("a#question").fancybox({ 'hideOnContentClick': true });
        $('#questionClose').click(function () { $.fancybox.close(); });
        $("a#bidhistory").fancybox({ 'hideOnContentClick': true });
        $('#historyClose').click(function () { $.fancybox.close(); });
        $('#closeDummy').click(function () { $.fancybox.close(); });
        function openAuctionWindow() {
            $("a#openDummy").trigger('click');
            return false;
        }
        function closeAuctionWindow() {
            $.fancybox.close();
            return false;
        }
        function redirectConfirm(_qid, _prc, _aid, _type) {
            var ddq = document.getElementById(_qid);
            var _qty = 0;
            if (ddq != null)
                _qty = ddq.options[ddq.selectedIndex].value;
            else
                _qty = _qid;

            window.location.href = '/Auctionconfirmation.aspx?t=b&a=' + _aid + '&q=' + _qty + '&p=' + _prc + '&y=' + _type;
            return false;
        }
        function redirectBuyNow(_qid, _prc, _aid, _type) {
            var ddq = document.getElementById(_qid);
            var _qty = 0;
            if (ddq != null)
                _qty = ddq.options[ddq.selectedIndex].value;
            else
                _qty = _qid;

            var totamt = _prc * _qty

            window.location.href = '/Auctionconfirmation.aspx?t=b&a=' + _aid + '&q=' + _qty + '&m=' + totamt + '&y=' + _type;
            return false;
        }
        function changeShipping(bidAmt, shipAmt, hidOCtl, hidFCtl, hidSCtl, totalCtl) {

            document.getElementById(hidOCtl).value = 2;
            document.getElementById(hidFCtl).value = shipAmt;
            document.getElementById(hidSCtl).value = shipAmt;
            //document.getElementById(shipCtl).innerHTML = '$' + parseFloat(shipAmt).toFixed(2);
            document.getElementById(totalCtl).value = parseFloat(bidAmt) + parseFloat(shipAmt);
            //document.getElementById(totalCtl).innerHTML = 'Total:&nbsp;&nbsp;&nbsp;&nbsp;$' + ttlAmt.toFixed(2);
            return true;
        }




        function openBid(aucid, bidamt, auc_type, refr_id, shipOption, shipValue) {
            window.open('/AuctionBidPop.aspx?a=' + aucid + '&o=ti0i8u6Oh_l2RKo9llO2UQ==&t=' + auc_type + '&p=' + bidamt + '&r=' + refr_id + '&sOption=' + shipOption + '&sValue=' + shipValue, "_AuctionBidPop", "left=" + ((screen.width - 600) / 2) + ",top=" + ((screen.height - 420) / 2) + ",width=600,height=420,scrollbars=yes,toolbars=no,resizable=yes");
            window.focus();
            return false;
        }

        function openAuctionBid(aucid, auc_type, valToComp, txt_max_bid_amt, ddl_shipping) {
            // alert(valToComp);
            var bidamt;
            var sValue;
            bidamt = document.getElementById(txt_max_bid_amt).value;
            var sp_help = document.getElementById('spn_amt_hlp');
            var sp_valid = document.getElementById('spn_valid');
            //            if (parseFloat(bidamt) < parseFloat(valToComp))
            //                alert(parseFloat(bidamt))
            //            else
            //                alert(bidamt);


            sp_valid.innerHTML = '';
            if (sp_help.style.display != 'none') {
                sp_help.style.display = 'none';
                //alert(parseFloat(bidamt) < parseFloat(valToComp));
            }
            if (sp_valid.style.display != 'none') {
                sp_valid.style.display = 'none';
                //alert(parseFloat(bidamt) < parseFloat(valToComp));
            }

            if (bidamt == '' || bidamt == '0' || bidamt == 'Enter Your Bid Amount') {
                sp_valid.style.display = '';
                sp_valid.innerHTML = 'Amount Required';
            }

            else {

                if (isNaN(bidamt)) {
                    sp_valid.style.display = '';
                    sp_valid.innerHTML = 'Invalid Amount';
                }

                else {
                    if (parseFloat(bidamt) < parseFloat(valToComp)) {
                        sp_valid.style.display = '';
                        sp_valid.innerHTML = 'Amount not Accepted';
                        sp_help.style.display = '';
                    }
                    else {
                        var e = document.getElementById(ddl_shipping);
                        var strSel = e.options[e.selectedIndex].value; //  + " and text is: " + e.options[e.selectedIndex].text;
                        if (strSel == '0') {
                            sp_valid.style.display = '';
                            sp_valid.innerHTML = 'Please Select Shipping';
                        }
                        else {
                            if (strSel == '1' || strSel == '2')
                                sValue = 0;
                            else {
                                if (!isNaN(strSel))
                                    sValue = strSel;
                                else
                                    sValue = 0;
                            }

                            strSel = e.options[e.selectedIndex].text;

                            w = window.open('/AuctionBidPop.aspx?a=' + aucid + '&o=ti0i8u6Oh_l2RKo9llO2UQ==&t=' + auc_type + '&p=' + bidamt + '&sOption=' + strSel + '&sValue=' + sValue, "_AuctionBidPop", "left=" + ((screen.width - 600) / 2) + ",top=" + ((screen.height - 420) / 2) + ",width=600,height=420,scrollbars=yes,toolbars=no,resizable=yes");
                            w.focus();
                        }

                    }
                }
            }
            return false;
        }
        function validBid1(aucid, txtid, cmpVal, divid, buy_option, auc_type, shipOption, shipValue, help_id, frm_id, refr_id) {

            var sOption;
            sOption = document.getElementById(shipOption).value;
            var sValue;
            sValue = document.getElementById(shipValue).value;



            document.getElementById(divid).innerHTML = '';

            if (sOption == '0')
                document.getElementById(divid).innerHTML = 'Please select shipping';
            else {
                var bidamt;
                bidamt = document.getElementById(txtid).value;

                if (bidamt == '' || bidamt == '0')
                    document.getElementById(divid).innerHTML = 'Amount Required';

                else {
                    //alert(bidamt); alert(!isNaN(123));
                    if (isNaN(bidamt))
                        document.getElementById(divid).innerHTML = 'Invalid Amount';
                    else {
                        if (Number(bidamt) < Number(cmpVal)) {
                            document.getElementById(divid).innerHTML = 'Amount not Accepted';
                            document.getElementById(help_id).style.display = 'block';
                        }
                        else {
                            {
                                document.getElementById(help_id).style.display = 'none';
                                document.getElementById(frm_id).src = '/frame_auction_bid.aspx?a=' + aucid + '&o=' + buy_option + '&t=' + auc_type + '&p=' + bidamt + '&r=' + refr_id + '&sOption=' + sOption + '&sValue=' + sValue;
                                openAuctionWindow();
                            }
                        }
                    }
                }
            }
            return false;
        }
        //function openOffer(aucid, buy_option, auc_type, bidamt, frm_id) {
        //    document.getElementById(frm_id).src = '/frame_auction_bid.aspx?a=' + aucid + '&o=' + buy_option + '&t=' + auc_type + '&p=' + bidamt;
        //    openAuctionWindow();
        //    return false;
        //}

        function askquestion_wait(_valdt_grp, _aid) {
            if (Page_ClientValidate(_valdt_grp)) {
                var dv_ask = document.getElementById('dv_' + _aid + '_ask');
                var dv_msg = document.getElementById('dv_' + _aid + '_msg');
                var dv_cnf = document.getElementById('dv_' + _aid + '_confirm');
                if (dv_ask.style.display == 'block') {
                    dv_ask.style.display = 'none';
                    dv_cnf.style.display = 'none';
                    dv_msg.style.display = 'block';
                }
                else {
                    dv_ask.style.display = 'block';
                    dv_cnf.style.display = 'block';
                    dv_msg.style.display = 'none';
                }
                return true;
            }
            else
                return false;
        }

        function open_pop_win(_path, _id) {
            w = window.open(_path + '?i=' + _id, '_history', 'top=' + ((screen.height - 400) / 2) + ', left=' + ((screen.width - 324) / 2) + ', height=400, width=324,scrollbars=yes,toolbars=no,resizable=1;');
            w.focus();
            return false;
        }
        function openOffer(aucid, buy_option, auc_type, bidamt) {
            w = window.open('/AuctionBidPop.aspx?a=' + aucid + '&o=' + buy_option + '&t=' + auc_type + '&p=' + bidamt, "_AuctionBidPop", "left=" + ((screen.width - 600) / 2) + ",top=" + ((screen.height - 420) / 2) + ",width=600,height=420,scrollbars=yes,toolbars=no,resizable=yes");
            w.focus();
            return false;
        }
        function openAskQuestion(aucid) {
            w = window.open('/AuctionQuestion.aspx?i=' + aucid, "_AuctionQuestion", "left=" + ((screen.width - 600) / 2) + ",top=" + ((screen.height - 480) / 2) + ",width=600,height=480,scrollbars=yes,toolbars=no,resizable=yes");
            w.focus();
            return false;
        }
    </script>
         </div>
         <script type="text/javascript" >
             window.focus();
    </script>
    </form>
</body>
</html>
