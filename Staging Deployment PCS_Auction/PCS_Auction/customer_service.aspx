﻿<%@ Page Title="" Language="VB" MasterPageFile="~/SiteMain.master" AutoEventWireup="false" CodeFile="customer_service.aspx.vb" Inherits="customer_service" %>
<%@ Register Src="~/UserControls/bidder_salesrep.ascx" TagName="SalesRep" TagPrefix="UC1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentOfChild" runat="Server">

   <div class="innerwraper">
          <%--  <UC1:SalesRep runat="server" ID="UC_SalesRep"></UC1:SalesRep>--%>
 
            
        <div class="summrybx dtltxtbx_abt">
            
                <h1 class="pgtitle" style="margin-top: 2px;">
                    Customer service</h1>
               <p><%--<img src="/pcsww/media/images/page-level/warehouse6.jpg" style="margin: auto 20px; width: 217px; float: right; height: 247px" alt="">--%>We’re dedicated to putting our customers first. And as our customer, you can count on responsive customer service and support&nbsp;&mdash; each and every time you work with us. Our service department is available and on call&nbsp;during business hours, and&nbsp;our team is empowered to assist you with your requests and needs.<br>
<br>
<span class="priv_span">Service hours: <br/>
</span>Monday&nbsp;&mdash; Thursday: 8:30 a.m.&nbsp;to 6:15 p.m. Eastern<br>
Friday: 8:30 a.m. to 1:30 p.m. Eastern <br>
<br>
<span class="priv_span">Returns authorization:<br>
</span>We have a return material authorization (RMA) program, providing an easy method for returning products purchased from PCS Wireless for repair/replacement. We also have a convenient sales return authorization (SRA) program for items shipped back to PCS Wireless for credit purposes. To access the complete information and downloadable forms, <a href="http://partner.pcsww.com/login">please login</a> or <a href="http://partner.pcsww.com/signup">register for the PCS Wireless Partner Portal</a>.<br>
<br>
For assistance, please call 973.805.7400 or email <a href="mailto:info@pcsww.com">info@pcsww.com</a>.</p>
            </div>
        
    </div>
   
</asp:Content>

