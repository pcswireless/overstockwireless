﻿<%@ Page Title="New User Account" Language="VB" MasterPageFile="~/SiteMain.master" AutoEventWireup="false" CodeFile="signup_old.aspx.vb" Inherits="signup_old" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentOfChild" runat="Server">
<asp:HiddenField ID="hid_comp_id" runat="server" Value="0" />
    <h1>
        Become a PCS Wireless Auction Partner</h1>
    <p>
        PCS Wireless online auction portal is an auction site that allows buyers and sellers
            to come together & enjoy the online auction experience. We allow you the advantage
            of purchasing a product at best prices by the means of competitive bidding.</p>

                <table style="font-size:13px;" cellpadding="2" cellspacing="2">
                    <tbody>
                    <tr>
                    <td colspan="2" align="center"> <asp:Label ID="lblMessage" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                                            <asp:HiddenField ID="hid_password" runat="server" /></td>
                    </tr>
                        <tr>
                            <td colspan="2">
                                <h4>
                                    STEP 1: Your company details</h4>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                      Name:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                     <asp:TextBox ID="txt_company" TabIndex="1" runat="server" CssClass="TextBoxField"></asp:TextBox>
                                         <asp:RequiredFieldValidator ID="rfv_company" runat="server" ControlToValidate="txt_company" Font-Size="10px"
                                                ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="<br />Name Required"
                                                CssClass="error"></asp:RequiredFieldValidator>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                   Company Website:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                    <asp:TextBox ID="txt_website" runat="server" TabIndex="2" CssClass="TextBoxField"></asp:TextBox>
                                        
                                </div>
                            </td>
                        </tr>
                                                 
                        <tr>
                            <td colspan="2">
                                <br />
                                <h4>
                                    STEP 2: Contact details</h4>
                            </td>
                        </tr>
                        
                        <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                    First name:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                   <asp:TextBox ID="txt_first_name" TabIndex="3" runat="server" CssClass="TextBoxField"></asp:TextBox>
                                              <asp:RequiredFieldValidator ID="rfv_contact_name" runat="server" Font-Size="10px" ControlToValidate="txt_first_name"
                                                ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="<br />First Name Required"
                                                CssClass="error"></asp:RequiredFieldValidator>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                    Last name:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                    <asp:TextBox ID="txt_last_name" TabIndex="4" runat="server" CssClass="TextBoxField"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                       <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                    Title:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                     <asp:DropDownList ID="ddl_title" Font-Size="12px" TabIndex="5" Width="155px" runat="server" DataTextField="name" DataValueField="country_id">
                                            </asp:DropDownList>
                                </div>
                            </td>
                        </tr>
                         <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                    Street:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                    <asp:TextBox ID="txt_address1" runat="server" TabIndex="6" CssClass="TextBoxField"></asp:TextBox>
                                          
                                            <asp:RequiredFieldValidator ID="rfv_address1" runat="server" Font-Size="10px" ControlToValidate="txt_address1"
                                                ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="<br />Street Required"
                                                CssClass="error"></asp:RequiredFieldValidator>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                    Street Address 2:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                     <asp:TextBox ID="txt_address2" runat="server" TabIndex="7" CssClass="TextBoxField"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                          <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                    City:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                   <asp:TextBox ID="txt_city" runat="server" TabIndex="8" CssClass="TextBoxField"></asp:TextBox>
                                           
                                            <asp:RequiredFieldValidator ID="rfv_city" runat="server" Font-Size="10px" ControlToValidate="txt_city"
                                                ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="<br />City Required"
                                                CssClass="error"></asp:RequiredFieldValidator>
                                </div>
                            </td>
                        </tr>
                         
                        <tr>
                            <td valign="top">
                                <label class="EditingFormLabel" >
                                Country:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                    <asp:DropDownList ID="ddl_country" Font-Size="12px" TabIndex="9" Width="155px" onchange="status_drop_txt();" runat="server" DataTextField="name" DataValueField="country_id">
                                            </asp:DropDownList>
                                            
                                            <asp:RequiredFieldValidator ID="rfv_country" runat="server" Font-Size="10px" ControlToValidate="ddl_country"
                                                ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="<br />Country Required"
                                                CssClass="error"></asp:RequiredFieldValidator>
                                </div>
                                
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                    State:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                     <asp:DropDownList ID="ddl_state" Font-Size="12px" Width="155px" TabIndex="10" runat="server" ></asp:DropDownList>
                                            <asp:TextBox ID="txt_other_state" runat="server" TabIndex="16" MaxLength="55" CssClass="TextBoxField"></asp:TextBox>
                                          
                                            <asp:RequiredFieldValidator ID="rfv_state" runat="server" Font-Size="10px" InitialValue="0" ControlToValidate="ddl_state"
                                                ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="<br />State Required"
                                                CssClass="error"></asp:RequiredFieldValidator>
                                                 <asp:RequiredFieldValidator ID="rfv_txt_state" runat="server" Font-Size="10px" ControlToValidate="txt_other_state"
                                                ValidationGroup="val_signup_info" Display="Dynamic" Enabled="false" ErrorMessage="<br />State Required"
                                                CssClass="error"></asp:RequiredFieldValidator>
                                                </div>
                                
                            </td>
                        </tr>
                      <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                   Postal code: </label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                  <asp:TextBox ID="txt_zip" runat="server" TabIndex="11" CssClass="TextBoxField" MaxLength="5"></asp:TextBox>
                                          
                                            <asp:RequiredFieldValidator ID="rfv_zip" runat="server" Font-Size="10px" ControlToValidate="txt_zip"
                                                ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="<br />Postal Code Required"
                                                CssClass="error"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Font-Size="10px" ControlToValidate="txt_zip"
                                                ValidationExpression="[0-9]{5}" EnableClientScript="true" Display="Dynamic" ErrorMessage="<br />Invalid Postal Code"
                                                runat="server" ValidationGroup="val_signup_info" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                    Phone 1:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                    <asp:TextBox ID="txt_mobile" runat="server" TabIndex="12" CssClass="TextBoxField" MaxLength="15"></asp:TextBox>
                                           
                                            <asp:RequiredFieldValidator ID="rfv_mobile" Font-Size="10px" runat="server" ControlToValidate="txt_mobile"
                                                ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="<br />Phone 1 Required"
                                                CssClass="error"></asp:RequiredFieldValidator>
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" Font-Size="10px" ControlToValidate="txt_mobile"
                                                ValidationExpression="[0-9]{10,13}" EnableClientScript="true" Display="Dynamic"
                                                ErrorMessage="<br />Invalid Phone 1" runat="server" ValidationGroup="val_signup_info" />--%>
                                                <br />
                                                <asp:RadioButton ID="rdo_ph_1_landline" runat="server" Text="Landline" GroupName="p1" Checked="true" /><asp:RadioButton ID="rdo_ph_1_mobile" runat="server" Text="Mobile" GroupName="p1" />
                                </div>
                            
				
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                    Phone 2:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                    <asp:TextBox ID="txt_phone" runat="server" CssClass="TextBoxField" TabIndex="13" MaxLength="15"></asp:TextBox>
                                          
                                           <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Font-Size="10px" ControlToValidate="txt_phone"
                                                ValidationExpression="[0-9]{10,13}" Display="Dynamic" EnableClientScript="true"
                                                ErrorMessage="<br />Invalid Phone 2" runat="server" ValidationGroup="val_signup_info" />--%>
                                                <br />
                                                
                                                <asp:RadioButton ID="rdo_ph_2_landline" runat="server" Text="Landline" GroupName="p2" Checked="true" /><asp:RadioButton ID="rdo_ph_2_mobile" runat="server" Text="Mobile" GroupName="p2" />
                                               
                                </div>
                                 
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                    Fax:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                    <asp:TextBox ID="txt_fax" runat="server" TabIndex="14" CssClass="TextBoxField" MaxLength="15"></asp:TextBox>
                                          
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator3" Font-Size="10px" ControlToValidate="txt_fax"
                                                ValidationExpression="[0-9]{10,13}" Display="Dynamic" EnableClientScript="true"
                                                ErrorMessage="<br />Invalid Fax" runat="server" ValidationGroup="val_signup_info" />--%>
                                </div>
                            </td>
                        </tr>
                      
                          
                        <tr>
                            <td colspan="2">
                                <br />
                                <h4>
                                    STEP 3: Account setup</h4>
                            </td>
                        </tr>
                         <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                   Your Email:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                    <asp:TextBox ID="txt_email" runat="server" TabIndex="15" CssClass="TextBoxField"></asp:TextBox>
                                           
                                            <asp:RequiredFieldValidator ID="rfv_email" runat="server" Font-Size="10px" ControlToValidate="txt_email"
                                                ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="<br />Email Required"
                                                CssClass="error"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ValidationGroup="val_signup_info" ID="rev_email" runat="server"
                                                ControlToValidate="txt_email" Display="Dynamic" ErrorMessage="<br />Invalid Email" Font-Size="10px"
                                                ValidationExpression="^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$"
                                                CssClass="error"></asp:RegularExpressionValidator>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                    User ID:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                    <asp:TextBox ID="txt_user_id" TabIndex="16" runat="server" CssClass="TextBoxField"></asp:TextBox>
                                                
                                                <asp:RequiredFieldValidator ID="rfv_user_id" runat="server" Font-Size="10px" ControlToValidate="txt_user_id"
                                                    ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="<br />User ID Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                              <label class="EditingFormLabel">
                                Password:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                     <asp:TextBox ID="txt_password" runat="server" TabIndex="17" CssClass="TextBoxField" TextMode="Password"></asp:TextBox>
                                             
                                                <asp:RequiredFieldValidator Font-Size="10px" ID="rfv_password" runat="server" ControlToValidate="txt_password"
                                                    ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="<br />Password Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Font-Size="10px" runat="server" ControlToValidate="txt_password"
                                                    ValidationGroup="val_signup_info" ErrorMessage="<br />Password atleast 6 chars."
                                                    ValidationExpression="^.{6,}$" Display="Dynamic"></asp:RegularExpressionValidator>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                    Retype Password:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                     <asp:TextBox ID="txt_retype_password" runat="server" TabIndex="18" CssClass="TextBoxField" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_retype_password" runat="server" Font-Size="10px" ControlToValidate="txt_retype_password"
                                                ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="<br />Retype Password Required"
                                                CssClass="error"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="comp_retype_password" runat="server" Font-Size="10px" ControlToValidate="txt_retype_password"
                                                ControlToCompare="txt_password" CssClass="error" Display="Dynamic" ValidationGroup="val_signup_info"
                                                ErrorMessage="<br />Password Mismatch"></asp:CompareValidator>
                                </div>
                            </td>
                        </tr>
                        <tr >
                            <td valign="top">
                                <label class="EditingFormLabel">
                                    Add comments here:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                   <asp:TextBox ID="txt_comment" runat="server" TabIndex="19" CssClass="TextBoxField" Width="155px" Height="45px" TextMode="MultiLine"></asp:TextBox>
                                        
                                </div>
                            </td>
                        </tr>
                          <tr>
                            <td colspan="2">
                                <br />
                                <h4>
                                    STEP 4: Other details</h4>
                            </td>
                        </tr>
                         
                          <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                     Business Type:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                   <asp:CheckBoxList ID="chklst_business_types" TabIndex="21" Font-Size="12px" runat="server" CellPadding="0" CellSpacing="0"
                                                BorderWidth="0" Width="100%">
                                            </asp:CheckBoxList>
                                        
                                </div>
                            </td>
                        </tr>
                         <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                     Industry Type:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                   <asp:CheckBoxList ID="chklst_industry_types" TabIndex="22" Font-Size="12px" runat="server" CellPadding="0" CellSpacing="0"
                                                BorderWidth="0" Width="100%">
                                            </asp:CheckBoxList>
                                        
                                </div>
                            </td>
                        </tr>
                         <tr runat="server" id="tr_company">
                            <td valign="top">
                                <label class="EditingFormLabel">
                                    Linked Companies:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                   <asp:CheckBoxList ID="chklst_companies_linked" TabIndex="23" Font-Size="12px" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                                Width="100%" CellPadding="0" CellSpacing="0" BorderWidth="0">
                                            </asp:CheckBoxList>
                                        
                                </div>
                            </td>
                        </tr>
                          <tr>
                            <td valign="top">
                                <label class="EditingFormLabel">
                                     How did you find us?:</label>
                            </td>
                            <td>
                                <div class="EditingFormControlNestedControl">
                                   <asp:DropDownList ID="ddl_how_find_us" Font-Size="12px" TabIndex="20" runat="server" Width="155px" >
                                            </asp:DropDownList>
                                        
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
          <br />
          <asp:Button ID="imgbtn_basic_save" TabIndex="24" runat="server" ValidationGroup="val_signup_info" Text="Submit"  />
  <%--  <asp:ImageButton ID="imgbtn_basic_save" TabIndex="23" ValidationGroup="val_signup_info" runat="server"
                                    AlternateText="Register" ImageUrl="/images/fend/register.png" />--%>
                                    <script type="text/javascript">

                                        function status_drop_txt() {
                                            // var Page_Validators = new Array();

                                            var ddlcntry = document.getElementById("<%=ddl_country.ClientID%>");
                                            var _cntry = /usa/;
                                            var _dflt = /select/;
                                            var _check = document.getElementById('<%=rfv_txt_state.ClientID%>');
                                            var ddlstat = document.getElementById("<%=ddl_state.ClientID%>");
                                            var txtstat = document.getElementById("<%=txt_other_state.ClientID%>");
                                            try {
                                                if (Page_Validators.length > 0) {

                                                    var _Text = ddlcntry.options[ddlcntry.selectedIndex].text.toLowerCase();
                                                    var _Value = ddlcntry.options[ddlcntry.selectedIndex].value;
                                                    if (ddlcntry.options.length > 1) {
                                                        if (_Text.match(_cntry)) {
                                                            ddlstat.style.display = '';
                                                            txtstat.style.display = 'none';
                                                            //alert('');
                                                            ValidatorEnable(document.getElementById('<%=rfv_txt_state.ClientID%>'), false);
                                                            ValidatorEnable(document.getElementById('<%=rfv_state.ClientID%>'), true);
                                                        }
                                                        else if (_Text.match(_dflt)) {
                                                            ddlstat.style.display = '';
                                                            txtstat.style.display = 'none';
                                                            ddlstat.selectedIndex = 0;
                                                            //alert('');
                                                            ValidatorEnable(document.getElementById('<%=rfv_txt_state.ClientID%>'), false);
                                                            ValidatorEnable(document.getElementById('<%=rfv_state.ClientID%>'), true);

                                                        }
                                                        else {
                                                            ddlstat.style.display = 'none';
                                                            txtstat.style.display = '';
                                                            ValidatorEnable(document.getElementById('<%=rfv_txt_state.ClientID%>'), true);
                                                            ValidatorEnable(document.getElementById('<%=rfv_state.ClientID%>'), false);
                                                        }
                                                    }
                                                    else {
                                                        ddlstat.style.display = '';
                                                        txtstat.style.display = 'none';


                                                    }
                                                }
                                                else {
                                                    ddlstat.style.display = '';
                                                    txtstat.style.display = 'none';
                                                }

                                            }
                                            catch (error) {
                                                ddlstat.style.display = '';
                                                txtstat.style.display = 'none';
                                            }

                                        }
                                        window.onload = status_drop_txt;
</script>
</asp:Content>