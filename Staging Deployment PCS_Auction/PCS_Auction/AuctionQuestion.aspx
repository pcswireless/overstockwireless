﻿<%@ Page Title="" Language="VB" MasterPageFile="~/SitePopUp.master" AutoEventWireup="false"
    CodeFile="AuctionQuestion.aspx.vb" Inherits="AuctionQuestion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">

        /***********************************************
        * Different CSS depending on OS (mac/pc)- © Dynamic Drive (www.dynamicdrive.com)
        * This notice must stay intact for use
        * Visit http://www.dynamicdrive.com/ for full source code
        ***********************************************/
        ///////No need to edit beyond here////////////

        var mactest = navigator.userAgent.indexOf("Mac") != -1


        if (mactest) {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet_mac.css"/>');
        }
        else {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet.css"/>');
        }
    </script>
    <asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
    <div style="padding: 10px; width: auto; height: auto;">
        <%--<div id="askques_pop1" style="margin-bottom: 0px; padding-bottom: 0px; visibility:hidden; ">
            <h2>
                <asp:Literal ID="lbl_title" runat="server" /></h2>
            <asp:Label ID="lbl_sub_title" runat="server" CssClass="auctionSubTitle" />
            <div class="auctionSubTitle">
                Auction # :
                <asp:Label Font-Size="11px" ID="lbl_auction_code" runat="server" />
            </div>
        </div>--%>
        <%--<asp:UpdatePanel runat="server" ID="ajax_upd">
                <ContentTemplate>
                    <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="ajax_upd" DynamicLayout="true">
                        <ProgressTemplate>
                            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                                <img id="Image8" src="/images/img_loading.gif" alt="Loading..." style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>--%>
        <div class="detailsSubHeading" style="padding: 0px;">
            <div class="askqstn " style="padding-bottom: 0px; padding-left: 10px; background: none repeat scroll 0 0 #F9F9F9!important;">
                <div class="ask_txtbx">
                    <h1 class="pgtitle">
                        Ask a Question
                    </h1>
                </div>
            </div>
        </div>
        <div class="grBackDescription" style="padding-left: 2px; margin-top: 0px; margin-bottom: 1px;
            width: auto; overflow: hidden;">
            <div id="askques_pop" style="padding: 0px;">
                <div style="overflow: hidden; padding: 5px;">
                    <div class="ask_txtbx" style="">
                        <asp:TextBox runat="server" ID="txt_thread_title" Height="100" Columns="50" Style="resize: vertical;
                            float: left;" CssClass="qsttxt" TextMode="MultiLine"></asp:TextBox>
                    </div>
                    <div>
                        <asp:RequiredFieldValidator ID="rfv_create_thread" runat="server" ErrorMessage="*"
                            ForeColor="red" Display="Dynamic" ControlToValidate="txt_thread_title" ValidationGroup="create_thread"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div id='dv_askpop_ask' style="padding-left: 10px; width: 120px; float: left;">
                    <p style="display: block; padding-top: 0px;" id='dv_qukcont_ask'>
                        <%--<asp:ImageButton ID="img_btn_create_thread" runat="server" OnClientClick="question_wait('create_thread','qukcont')" AlternateText="Create" ImageUrl="/Images/fend/send.png" ValidationGroup="create_thread" />--%>
                        <asp:Button ID="img_btn_create_thread" runat="server" OnClientClick="question_wait('create_thread','qukcont')"
                            Text="Send" ValidationGroup="create_thread" Style="float: left;" /></p>
                    <p style="display: none; font-size: 11px;" id='dv_qukcont_msg'>
                        <span style="color: Red">Please Wait...</span>
                    </p>
                </div>
                <div style="float: left;"><p>
                    <input type="button" name="" id="popupClose" onclick="javascript:window.close();"
                        value="Cancel" /></p></div>
                <div style="width: 435px; float: left; overflow: hidden;" id='dv_qukcont_confirm'>
                    <asp:Label ID="lit_msg_confirmation" runat="server" ForeColor="Red"></asp:Label>
                </div>
            </div>
        </div>
        <div id="rght_botm">
            <div class="detailsSubHeading" style="font-family: 'dinregular',arial;">
                <asp:Literal ID="lit_your_query" runat="server" Text="Your Queries"></asp:Literal>
            </div>
            <asp:Repeater ID="rep_after_queries" runat="server" OnItemCommand="rep_after_queries_ItemCommand"
                OnItemDataBound="rep_after_queries_ItemDataBound">
                <ItemTemplate>
                    <div style="font-family: 'dinbold',arial; color: #3D3A37; font-size: 14px;">
                        <%# Container.ItemIndex + 1 & ". " & Eval("title")%>
                        <div style="padding: 0px 0px 0px 10px; display: inline; position: relative; bottom: -0.2em;">
                            <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="/Images/close_modal_1.png"
                                CommandArgument='<%#Eval("query_id") %>' CommandName="deleteQry" AlternateText="Delete Questions"
                                ToolTip="Delete Questions" />
                        </div>
                    </div>
                    <div style="padding: 0 0 0 10px!important; font-family: dinregularitalic; font-size: 14px;
                        color: Gray; clear: both;">
                        <%# "on " & Eval("submit_date")%>
                    </div>
                    <div style="padding: 0 10px; font-size: 14px;">
                        <asp:Repeater ID="rep_inner_after_queries" runat="server">
                            <ItemTemplate>
                                <div style="font-family: 'dinregular',arial; color: #3D3A37; font-size: 14px;">
                                    <img src='<%# IIF(Eval("image_path")<>"","/upload/users/" & Eval("user_id") & "/" & Eval("image_path"),"/images/buyer_icon.gif") %>'
                                        alt="" style="float: left; padding: 2px; height: 25px; width: 23px;" />
                                    <%#Eval("message") & "<br>" %>
                                    <span style="font-style: italic; color: Gray;">
                                        <%# IIf(Eval("buyer_title") = "", Eval("title"), Eval("buyer_title"))%>
                                        <%# IIf(Eval("first_name") = "", Eval("buyer_first_name") & " " & Eval("buyer_last_name"), Eval("first_name") & " " & Eval("last_name"))%>,
                                        <%# Eval("submit_date")%>
                                    </span>
                                </div>
                                <br />
                                <br />
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <%--
                            </ContentTemplate>
            </asp:UpdatePanel>
        --%>
    </div>
    <asp:Button ID="btn_query" runat="server" Style="display: none;" CommandName="query" />
    <script type="text/javascript">

        function question_wait(_valdt_grp, _aid) {
            if (Page_ClientValidate(_valdt_grp)) {
                var dv_ask = document.getElementById('dv_' + _aid + '_ask');
                var dv_msg = document.getElementById('dv_' + _aid + '_msg');
                var dv_cnf = document.getElementById('dv_' + _aid + '_confirm');
                if (dv_ask.style.display == 'block') {
                    dv_ask.style.display = 'none';
                    dv_cnf.style.display = 'none';
                    dv_msg.style.display = 'block';
                }
                else {
                    dv_ask.style.display = 'block';
                    dv_cnf.style.display = 'block';
                    dv_msg.style.display = 'none';
                }
            }
        }



    </script>
    <script type ="text/javascript" >
        window.focus();
    </script>
</asp:Content>
