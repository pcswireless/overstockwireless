﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Auctions.aspx.vb" Inherits="Auctions"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControls/AuctionGridListing.ascx" TagName="AuctionGridListing"
    TagPrefix="UC1" %>
<%@ Register Src="~/UserControls/SalesRep.ascx" TagName="SalesRep" TagPrefix="UC1" %>
<%@ Register Src="~/UserControls/AuctionListing.ascx" TagName="AuctionListing" TagPrefix="UC1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Auctions</title><![if IE]>
    <link rel="stylesheet" type="text/css" href="/Style/font_ie.css" />
    <![endif]> <![if !IE]>
    <link rel="stylesheet" type="text/css" href="/Style/font_other.css" />
    <![endif]>
    <link type="text/css" rel="Stylesheet" href="/Style/AuctionBody.css" />
    <script type="text/javascript" src="/lightbox/prototype.js"></script>
    <script type="text/javascript" src="/lightbox/scriptaculous.js?load=effects"></script>
    <script type="text/javascript" src="/lightbox/lightbox.js"></script>
    <link rel="stylesheet" href="/lightbox/lightbox.css" type="text/css" media="screen" />
    <style type="text/css">
        .modal
        {
            display: none;
            position: absolute;
            top: 0px;
            left: 0px;
            background-color: black;
            z-index: 100;
            opacity: 0.8;
            filter: alpha(opacity=60);
            -moz-opacity: 0.8;
            min-height: 100%;
        }
        #divImage
        {
            display: none;
            z-index: 1000;
            position: fixed;
            top: 0;
            left: 0;
            background-color: White;
            height: 300px;
            width: 410px;
            padding: 3px;
            border: solid 1px black;
        }
    </style>
    <script type="text/javascript" language="JavaScript">

        function redirectIframe(path1, path2, path3) {
            if (path1 != '')
                top.topFrame.location = path1;
            if (path2 != '')
                top.leftFrame.location = path2;
            if (path3 != '')
                top.mainFrame.location = path3;
        }

        function open_pop_win(_path, _id) {
            w = window.open(_path + '?i=' + _id, '_history', 'top=' + ((screen.height - 400) / 2) + ', left=' + ((screen.width - 324) / 2) + ', height=400, width=324,scrollbars=yes,toolbars=no,resizable=1;');
            w.focus();
            return false;
        }


        hu = window.location.search.substring(1);
        if (hu.indexOf('&a=') > 0) {
            redirectIframe(top.topFrame.location, '', '')
        }
           
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="ScriptManager" runat="server" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="pn_auction_content">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pn_auction_content" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="lnk_view">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pn_auction_content" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function conditionalPostback(sender, eventArgs) {
                alert(eventArgs.get_enableAjax());
                eventArgs.set_enableAjax(true);

            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" alt="" />
    </telerik:RadAjaxLoadingPanel>
    <center>
        <UC1:SalesRep runat="server" ID="UC_SalesRep"></UC1:SalesRep>
    </center>
     <%--<div class="pageheading">
        Dashboard</div>--%>
    <div id="div_topmsg" runat="server" style="background-color: #EDF9FF; padding: 5px;
        padding-left: 40px; background-image: url('/images/bulb.gif'); background-repeat: no-repeat;
        background-position: 10px 5px;">
        <span class="duplAuction">Message for you...</span>
        <asp:Repeater ID="rep_message" runat="server">
            <ItemTemplate>
                <div style="border: 1px solid #FCAD35; padding: 5px; margin-top: 15px; text-align: justify;">
                    <b>
                        <%# Eval("title")%></b> -
                    <%# Eval("description")%>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <br />
    <div style="padding: 30px 5px 10px 5px;">
        <div style="float: right; padding-right: 10px;">
            <div id="google_translate_element">
            </div>
            <script>

                function googleTranslateElementInit() {

                    new google.translate.TranslateElement({

                        pageLanguage: 'en',

                        layout: google.translate.TranslateElement.InlineLayout.SIMPLE

                    }, 'google_translate_element');

                }

            </script>
            <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
        <div class="viewStyle">
            <asp:Literal ID="lnk_view" runat="server" Text="Switch to Grid View"></asp:Literal>
        </div>
        <telerik:RadAjaxPanel ID="RadAjaxPanel_Auction" runat="server" CssClass="TabGrid">
            <asp:Panel ID="pn_auction_content" runat="server">
                <asp:Repeater ID="rpt_auctions" runat="server">
                    <ItemTemplate>
                        <UC1:AuctionListing runat="server" ID="UC_AuctionListing" auction_id='<%# Eval("auction_id")%>' />
                    </ItemTemplate>
                </asp:Repeater>
                <asp:DataList ID="rpt_auctions_gridview" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                    CellSpacing="0" ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <div style='width: 328px; padding: 10px; text-align: left; <%# iif(((Container.ItemIndex + 1) mod 3) = 0,"border-bottom: 1px dotted LightGray;","border-bottom: 1px dotted LightGray;border-right: 1px dotted LightGray;")%>'>
                            <UC1:AuctionGridListing runat="server" ID="UC_AuctionGridListing" auction_id='<%# Eval("auction_id")%>' />
                        </div>
                    </ItemTemplate>
                </asp:DataList>
            </asp:Panel>
        </telerik:RadAjaxPanel>
        <div class="viewStyle">
            <asp:Literal ID="lnk_view1" runat="server" Text="Switch to Grid View"></asp:Literal>
        </div>
        <asp:Panel ID="pnl_noItem" runat="server">
            <div style="border: 1px solid #10AAEA; background: url('/Images/fend/grdnt_template.jpg') repeat-x;
                height: 112px; padding-top: 25px; text-align: center;">
                <b>Auction Not available in this section</b>
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
