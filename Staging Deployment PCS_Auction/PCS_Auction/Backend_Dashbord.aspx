﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="Backend_Dashbord.aspx.vb" Inherits="Backend_Dashbord" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" />
    </telerik:RadAjaxLoadingPanel>
    <script type="text/javascript" language="javascript">
        function redirect_to_auction(id, auction_id, tab, invitation_id) {
            var a = id.options[id.selectedIndex].value;
            if (a == 1) {

                window.open('/Auctions/ListInvitedBidders.aspx?i=' + auction_id, 'invited_bidder', 'left=' + ((screen.width - 800) / 2) + ',top=' + (0) + ',width=800,height=700,scrollbars=yes,resizable=no,toolbars=no');
                window.focus();
            }
            else if (a == 2) {
                redirectIframe('/Backend_TopBar.aspx?t=' + tab + '&a=' + auction_id, '/Backend_Leftbar.aspx?t=3&a=' + auction_id, '/Auctions/AddEditAuction.aspx?t=5&i=' + auction_id)
            }
            else if (a == 3) {
                redirectIframe('/Backend_TopBar.aspx?t=' + tab + '&a=' + auction_id, '/Backend_Leftbar.aspx?t=3&a=' + auction_id, '/Auctions/AddEditAuction.aspx?t=7&i=' + auction_id)
            }
            else if (a == 4) {
                redirectIframe('/Backend_TopBar.aspx?t=' + tab + '&a=' + auction_id, '/Backend_Leftbar.aspx?t=3&a=' + auction_id, '/Auctions/AddEditAuction.aspx?t=7&i=' + auction_id)
            }
        }
    </script>
    <div class="pageheading">
        Dashboard</div>
    <div id="div_topmsg" runat="server" style="background-color: #EDF9FF; padding: 5px;
        padding-left: 40px; background-image: url('/images/bulb.gif'); background-repeat: no-repeat;
        background-position: 10px 5px;">
        <span class="duplAuction">Message for you...</span>
        <asp:Repeater ID="rep_message" runat="server">
            <ItemTemplate>
                <div style="border: 1px solid #FCAD35; padding: 5px; margin-top: 15px; text-align: justify;">
                    <b>
                        <%# Eval("title")%></b> -
                    <%# Eval("description")%>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <br />
    <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <table cellspacing="0" cellpadding="0" border="0" width="988">
            <tr>
                <td>
                    <div style="float: right; color: Blue; padding: 15px;">
                        <asp:LinkButton ID="lnk_all" runat="server" Text="All" Visible="false" />
                        <span id="span_bar_auctions" runat="server" visible="false">|</span>
                        <asp:LinkButton ID="lnk_auctions" runat="server" Text="Auctions" Visible="false" />
                        <span id="span_bar_buy_now" runat="server" visible="false">|</span>
                        <asp:LinkButton ID="lnk_buy_now" runat="server" Text="Buy it Now" Visible="false" />
                        <span id="span_bar_quote" runat="server" visible="false">|</span>
                        <asp:LinkButton ID="lnk_quote" runat="server" Text="Offers" Visible="false" /></div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hid_user_id" runat="server" Value="0" />
                    <asp:DataList ID="rpt_auctions" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                        CellSpacing="0" ItemStyle-VerticalAlign="Top">
                        <ItemTemplate>
                            <div style='width: 328px; padding: 10px; text-align: left; <%# iif(((Container.ItemIndex + 1) mod 3) = 0,"border-bottom: 1px dotted LightGray;","border-bottom: 1px dotted LightGray;border-right: 1px dotted LightGray;")%>'>
                                <div style="overflow: hidden;">
                                    <div style="width: 312px; padding-top: 0px; text-align: center; color: #243E5A; font-weight: bold;
                                        font-size: 13px; height: 40px;">
                                        <%  If Vew_Auction = True Then%>
                                        <a style="color: #243E5A; text-decoration: none;" href="javascript:void(0);" onmouseout="hide_tip_new();"
                                            onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');"
                                            onclick="redirectIframe('/Backend_TopBar.aspx?t=<%# CommonCode.getAuctionTabPosition(Eval("auction_id")) %>&a=<%# Eval("auction_id") %>','/Backend_Leftbar.aspx?t=3&a=<%# Eval("auction_id") %>','/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %><%# IIf(Eval("status") = 1,"&t=9","")%>')">
                                            <span style='text-decoration: none; font-size: <%# IIf(Eval("title").ToString.Length < 70, "13px",IIf(Eval("title").ToString.Length < 150, "10px",IIf(Eval("title").ToString.Length < 200, "10px", "10px")))%>;'>
                                                <%#Eval("title")%></span></a>
                                        <% Else%>
                                        <a style="color: #243E5A; text-decoration: none;" href="javascript:void(0);" onmouseout="hide_tip_new();"
                                            onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');">
                                            <span style='text-decoration: none; font-size: <%# IIf(Eval("title").ToString.Length < 70, "13px",IIf(Eval("title").ToString.Length < 150, "10px",IIf(Eval("title").ToString.Length < 200, "10px", "10px")))%>;'>
                                                <%#Eval("title")%></span></a>
                                        <%  End If%>
                                        <a target="_blank" title='Open in New Window' href='/Auctions/AddEditAuction.aspx?t=9&i=<%# Eval("auction_id") %>' >
                                           <img src='/Images/newwin.gif' alt="Open in New Window"></a>
                                    </div>
                                    <div style="float: left;">
                                        <div style="border: 2px solid #D7D7D7;">
                                            <asp:Image ID="img_image" runat="server" CssClass="aucImageBackend" ImageUrl='<%# Eval("image_path") %>' />
                                        </div>
                                        <div style="padding-top: 10px; text-align: center;">
                                            <asp:Literal ID="ltrl_status" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div style="float: left; line-height: 17px; font-size: 13px; padding-left: 8px; padding-top: 3px;
                                        border: 0px solid Red; width: 205px; overflow: hidden;">
                                        <div style="color: #105386;">
                                            <div>
                                                <iframe id="iframe_price" src="/timerframe.aspx?i=<%# Eval("auction_id") %>" scrolling="no"
                                                    frameborder="0" width="205px" height="23px"></iframe>
                                                <asp:Literal ID="ltrl_time_left" runat="server"></asp:Literal>
                                            </div>
                                            <div>
                                                <asp:Literal ID="ltrl_current_bid_status" runat="server"></asp:Literal>
                                            </div>
                                        </div>
                                        <div style="color: #303030; padding-top: 26px;">
                                            <asp:Literal ID="ltrl_price" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                                <%  If Vew_Auction Then%>
                                <div style="width: 312px; text-align: center; padding-top: 10px;">
                                    <asp:DropDownList ID="ddl_View_Page" runat="server" Width="280">
                                    </asp:DropDownList>
                                </div>
                                <%  End If%>
                                <div class="dashbought">
                                    <a href="javascript:void(0);" <%  If Vew_Auction  Then%> onclick="redirectIframe('/Backend_TopBar.aspx?t=<%# CommonCode.getAuctionTabPosition(Eval("auction_id")) %>&a=<%# Eval("auction_id") %>','/Backend_Leftbar.aspx?t=3&a=<%# Eval("auction_id") %>','/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %><%# IIf(Eval("status") = 1,"&t=7","")%>')"
                                        <%  End If%>>
                                        <asp:Label ID="lbl_bought" runat="server"></asp:Label>
                                    </a>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnl_noItem" runat="server">
                        <div style="border: 1px solid #10AAEA; background: url('/Images/fend/grdnt_template.jpg') repeat-x;
                            height: 112px; padding-top: 25px; text-align: center;">
                            <b>Auction Not available in this section</b>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
</asp:Content>
