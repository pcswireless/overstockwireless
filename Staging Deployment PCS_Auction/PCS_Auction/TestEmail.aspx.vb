﻿Imports System.IO
Imports iTextSharp.text.pdf
Imports iTextSharp.text
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.html

Partial Class TestEmail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim obj As New Email()
        'Response.Write(obj.SendWinningBidEmail(441))
        'lbl_test.Text = obj.SendWinningBidEmail(441)
        ' Response.Write("<br />#########################################################")
        ' Response.Write(obj.SendBuyNowAcceptEmail(45))
        'Response.Write(obj.SendQuotationAcceptEmail(65))
        lbl_test.Text = obj.send_employee_creation_email(31)
        ' Response.Write("<br />#########################################################")
        lbl_test.Text = lbl_test.Text & obj.send_bidder_sub_login_creation_email(51)
    End Sub

    Protected Sub btn_CreatePDF_Click(sender As Object, e As System.EventArgs) Handles btn_CreatePDF.Click
        Response.ContentType = "application/pdf"
        Dim attachment As String = "attachment; filename=test.pdf"
        'Response.ClearContent()
        Response.AddHeader("content-disposition", attachment)
        Dim stw As New StringWriter()
        Dim str As New StringReader(stw.ToString())
        Dim htextw As New HtmlTextWriter(stw)
        htextw.AddStyleAttribute("font-size", "7pt")
        htextw.AddStyleAttribute("color", "Black")
        Me.Page.RenderControl(htextw)
        'Panel_Name.RenderControl(htextw)
        'Name of the Panel
        Dim document As New Document()
        document = New Document()
        'FontFactory.GetFont("Arial", 50, iTextSharp.text.BaseColor.BLUE)
        PdfWriter.GetInstance(document, Response.OutputStream)

        document.Open()


        Dim htmlworker As New HTMLWorker(document)
        htmlworker.Parse(str)

        document.Close()
        Response.Write(document)

    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(control As Control)


    End Sub

End Class
