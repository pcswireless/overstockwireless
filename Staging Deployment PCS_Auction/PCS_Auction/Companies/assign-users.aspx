﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="assign-users.aspx.vb" Inherits="Companies_assign_users"
    MasterPageFile="~/BackendPopUP.master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hd_seller_id" runat="server" Value="0" />
    <!-- content start -->
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
        <asp:Image ID="Image1" runat="server" ImageUrl="/images/LoadingProgressBar.gif">
        </asp:Image>
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager runat="server" ID="radAjax" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="grdParent">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdParent" />
                    <telerik:AjaxUpdatedControl ControlID="grdChild" />
                    <telerik:AjaxUpdatedControl ControlID="msg" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="grdChild">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdChild" />
                    <telerik:AjaxUpdatedControl ControlID="grdParent" />
                    <telerik:AjaxUpdatedControl ControlID="msg" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="UseDragColumnCheckBox">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdParent" />
                    <telerik:AjaxUpdatedControl ControlID="grdChild" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadScriptBlock runat="server" ID="scriptBlock">
        <script type="text/javascript">
                <!--
            function onRowDropping(sender, args) {
                if (sender.get_id() == "<%=grdParent.ClientID %>") {
                    var node = args.get_destinationHtmlElement();
                    if (!isChildOf('<%=grdChild.ClientID %>', node) && !isChildOf('<%=grdParent.ClientID %>', node)) {
                        args.set_cancel(true);
                    }
                }
                else if (sender.get_id() == "<%=grdChild.ClientID %>") {
                    var node = args.get_destinationHtmlElement();
                    if (!isChildOf('<%=grdParent.ClientID %>', node) && !isChildOf('<%=grdChild.ClientID %>', node)) {
                        args.set_cancel(true);
                    }
                }
                else {
                    var node = args.get_destinationHtmlElement();
                    if (!isChildOf('trashCan', node)) {
                        args.set_cancel(true);
                    }
                    else {
                        if (confirm("Are you sure you want to delete this order?"))
                            args.set_destinationHtmlElement($get('trashCan'));
                        else
                            args.set_cancel(true);
                    }
                }
            }

            function isChildOf(parentId, element) {
                while (element) {
                    if (element.id && element.id.indexOf(parentId) > -1) {
                        return true;
                    }
                    element = element.parentNode;
                }
                return false;
            }
                -->
        </script>
    </telerik:RadScriptBlock>
    <div >
        <%--   <div class="msgTop">
<asp:CheckBox ID="UseDragColumnCheckBox" runat="server" OnCheckedChanged="UseDragColumnCheckBox_CheckedChanged" 
AutoPostBack="true" Text="Use GridDragDropColumn" />
            </div>
            <p class="howto">
                Select and drag orders from pending to shipped when dispatched<br />
                Reorder pending orders on priority by drag and drop<br />
                Drop a shipped order over the recycle bin to delete it</p>--%>
        <div style="float: left; padding: 0 6px 0 10px; width: 360; text-align: left;">
            <h2 style="color: #9c3608">
                Select Users</h2>
            <telerik:RadGrid runat="server" ID="grdParent" OnNeedDataSource="grdParent_NeedDataSource"
                AllowPaging="True" Width="360px" OnRowDrop="grdParent_RowDrop" AllowMultiRowSelection="true"
                PageSize="20" Skin="Simple" AutoGenerateColumns="false"><GroupingSettings CaseSensitive="false" />
                <HeaderStyle BackColor="#BEBEBE" />
                <ItemStyle Font-Size="11px" />
                <MasterTableView DataKeyNames="user_id" Width="100%">
                <NoRecordsTemplate>
                                                    Users not available
                                                    </NoRecordsTemplate>
                    <Columns>
                        <telerik:GridBoundColumn DataField="User_Id" HeaderText="User Id" SortExpression="User_Id"
                            UniqueName="userid">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Profile" HeaderText="Profile" SortExpression="Profile"
                            UniqueName="profile">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Name" HeaderText="Name" SortExpression="Name"
                            UniqueName="name">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email"
                            UniqueName="email">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IsVisible" HeaderText="IsVisible" SortExpression="Buyer_Id"
                            UniqueName="isvisible" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridDragDropColumn Visible="false" />
                    </Columns>
                    <PagerStyle Mode="NumericPages" PageButtonCount="4" />
                </MasterTableView>
                <ClientSettings AllowRowsDragDrop="True">
                    <Selecting AllowRowSelect="True" EnableDragToSelectRows="false" />
                    <%--<ClientEvents OnRowDropping="onRowDropping" />--%>
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" ScrollHeight="320px" />
                </ClientSettings>
                <PagerStyle Mode="NumericPages" PageButtonCount="4" />
            </telerik:RadGrid>
        </div>
        <div style="float: left; padding: 0 10px 0 6px; width: 360px; overflow: hidden; text-align: left;">
            <h2 style="color: #3c8b04">
                Selected Users</h2>
            <telerik:RadGrid runat="server" ID="grdChild" OnNeedDataSource="grdChild_NeedDataSource"
                Skin="Simple" AllowPaging="True" Width="360px" OnRowDrop="grdChild_RowDrop" AllowMultiRowSelection="true"
                PageSize="20" AutoGenerateColumns="false">
                <HeaderStyle BackColor="#BEBEBE" />
                <ItemStyle Font-Size="11px" />
                <MasterTableView DataKeyNames="user_id" Width="100%">
                <NoRecordsTemplate>
                                                    Users not selected
                                                    </NoRecordsTemplate>
                    <Columns>
                        <telerik:GridBoundColumn DataField="User_Id" HeaderText="User Id" SortExpression="User_Id"
                            UniqueName="userid">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Profile" HeaderText="Profile" SortExpression="Profile"
                            UniqueName="profile">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Name" HeaderText="Name" SortExpression="Name"
                            UniqueName="name">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email"
                            UniqueName="email">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IsVisible" HeaderText="IsVisible" SortExpression="Buyer_Id"
                            UniqueName="isvisible" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridDragDropColumn Visible="false" />
                    </Columns>
                    
                    <PagerStyle Mode="NumericPages" PageButtonCount="4" />
                </MasterTableView>
                <ClientSettings AllowRowsDragDrop="True">
                    <Selecting AllowRowSelect="True" EnableDragToSelectRows="false" />
                    <%-- <ClientEvents OnRowDropping="onRowDropping" />--%>
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" ScrollHeight="320px" />
                </ClientSettings>
                <PagerStyle Mode="NumericPages" PageButtonCount="4" />
            </telerik:RadGrid>
        </div>
        <%--   <div style="clear: both;">
                <!-- -->
            </div><div class="exFooter">
                <div id="trashCan">
                    Recycle Bin</div>
                <div class="exMessage" runat="server" id="msg" visible="false"  enableviewstate="false">
                    Order(s) successfully deleted!
                </div>
            </div>--%>
    </div>
    <div style="clear: both; text-align: center;">
        <br />
        <asp:ImageButton ID="but_save" AlternateText="Save Selection" ImageUrl="/images/saveselection.gif"
            runat="server" />
    </div>
</asp:Content>
