﻿<%@ Page Title="PCS Bidding" Language="VB" MasterPageFile="~/BackendPopUP.master" AutoEventWireup="false"
    CodeFile="assign_bidders_new.aspx.vb" Inherits="Companies_assign_bidders_new" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" style="position:fixed;left:420px;top:180px;z-index:9999" runat="server" BackgroundPosition="Center">
      <img id="Image8" src="/images/img_loading.gif" />
  </telerik:RadAjaxLoadingPanel>
  
    <center>
       <div class="pageheading">
            Select Bidders</div>
    </center>
    <asp:HiddenField ID="hd_seller_id" runat="server" Value="0" />
   
        <telerik:RadAjaxPanel ID="AjaxPanel_Bidder" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
            CssClass="TabGrid">
            <telerik:RadGrid ID="RadGrid_BidderAssign" runat="server" AllowAutomaticDeletes="True"
                PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True" AutoGenerateColumns="False" AllowSorting="true"
                Skin="Vista" AllowFilteringByColumn="True" ShowGroupPanel="true">
                <PagerStyle Mode="NextPrevAndNumeric" />
                
                <MasterTableView DataKeyNames="buyer_id" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                    AllowFilteringByColumn="True" ItemStyle-Height="40" AlternatingItemStyle-Height="40">
                      <NoRecordsTemplate>
                                                    Bidders list not available
                                                    </NoRecordsTemplate>
                                                    <SortExpressions>  <telerik:GridSortExpression FieldName="company_name" SortOrder="Ascending" /></SortExpressions>
                    <Columns>
                        <telerik:GridBoundColumn DataField="Buyer_Id" HeaderText="Buyer Id" SortExpression="Buyer_Id"
                            UniqueName="buyerid" Visible="false">
                        </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Company Name" SortExpression="company_name" ShowSortIcon="true" UniqueName="company_name" DataField="company_name" FilterControlWidth="120" GroupByExpression="company_name [Company Name] group by company_name" >
                                <ItemTemplate >
                                    <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');">
                                         <%# Eval("company_name")%></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                             <telerik:GridTemplateColumn  DataField="contact" HeaderText="Contact Person" SortExpression="contact" UniqueName="contact" FilterControlWidth="120" GroupByExpression="contact [Contact Person] group by contact" >
                                <ItemTemplate >
                                    <%# Eval("contact")%>
                                         <%# IIf(Eval("contact_title") <> "", "<br>" & Eval("contact_title"), "")%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                             
                       
                        <telerik:GridTemplateColumn HeaderText="Email Address" SortExpression="email" UniqueName="email" DataField="email" FilterControlWidth="180" GroupByExpression="email [Email Address] group by email" >
                                <ItemTemplate >
                                    <a href="mailto:<%# Eval("email")%>">
                                         <%# Eval("email")%></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                             <telerik:GridBoundColumn DataField="phone" HeaderText="Phone" FilterControlWidth="100" SortExpression="phone"
                            UniqueName="phone">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn AllowFiltering="false" ItemStyle-Width="90">
                            <HeaderTemplate>
                                Select All
                                <asp:CheckBox ID="chkHeader" runat="server" AutoPostBack="true" OnCheckedChanged="CheckAll" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chk1" runat="server" Checked='<%#is_map(Eval("is_select")) %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                 <ClientSettings AllowDragToGroup="true">
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="true" />
            </telerik:RadGrid>
        </telerik:RadAjaxPanel>
        <center>
            <div style="clear: both;">
                <br />
                <asp:ImageButton ID="but_save" AlternateText="Save Selection" ImageUrl="/images/saveselection.gif"
                    runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:ImageButton ID="ImageButton1" OnClientClick="javascript:window.close();" runat="server" AlternateText="Close" ImageUrl="/images/close.gif" />
            </div>
        </center>
    
</asp:Content>
