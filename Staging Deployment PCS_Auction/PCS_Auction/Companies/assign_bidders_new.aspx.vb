﻿Imports Telerik.Web.UI

Partial Class Companies_assign_bidders_new
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            If IsNumeric(Request.QueryString.Get("i")) Then
                hd_seller_id.Value = Request.QueryString.Get("i")
            Else
                hd_seller_id.Value = 0
            End If
        End If
    End Sub

    Protected Function is_map(ByVal a As Integer) As Boolean
        If a = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Protected Sub CheckAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Integer
        If sender.checked Then
            For i = 0 To Me.RadGrid_BidderAssign.Items.Count - 1
                CType(RadGrid_BidderAssign.Items(i).FindControl("chk1"), CheckBox).Checked = True
            Next
        Else
            For i = 0 To Me.RadGrid_BidderAssign.Items.Count - 1
                CType(RadGrid_BidderAssign.Items(i).FindControl("chk1"), CheckBox).Checked = False
            Next
        End If
    End Sub

    Protected Sub RadGrid_BidderAssign_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid_BidderAssign.NeedDataSource
        RadGrid_BidderAssign.DataSource = SqlHelper.ExecuteDataTable("SELECT S.buyer_id, S.company_name,isnull((S.contact_first_name+' '+S.contact_last_name),'') as contact,isnull(S.contact_title,'') as contact_title,S.email,S.phone,is_select=0 FROM tbl_reg_buyers S where S.status_id=2 and S.buyer_id not in (select buyer_id from tbl_reg_buyer_seller_mapping where seller_id=" & hd_seller_id.Value & ") union SELECT S.buyer_id, S.company_name, isnull((S.contact_first_name+' '+S.contact_last_name),'') as contact, S.contact_title,S.email,S.phone,is_select=1 FROM tbl_reg_buyers S where S.status_id=2 and S.buyer_id in (select buyer_id from tbl_reg_buyer_seller_mapping where seller_id=" & hd_seller_id.Value & ")")
    End Sub

    Protected Sub but_save_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles but_save.Click
        If hd_seller_id.Value > 0 Then
            Dim dt As New DataTable
            Dim obj As New CommonCode

            Dim ins_qry As String = ""
            Dim del_qry As String = "delete s from tbl_reg_buyer_seller_mapping s inner join tbl_reg_buyers b on s.buyer_id=b.buyer_id where b.status_id=2 and s.seller_id=" & hd_seller_id.Value & ";"
            dt = SqlHelper.ExecuteDataTable(del_qry)
            For i = 0 To Me.RadGrid_BidderAssign.Items.Count - 1
                If CType(RadGrid_BidderAssign.Items(i).FindControl("chk1"), CheckBox).Checked = True Then
                    ins_qry = "insert into tbl_reg_buyer_seller_mapping (seller_id,buyer_id) values (" & hd_seller_id.Value & "," & RadGrid_BidderAssign.Items(i).GetDataKeyValue("buyer_id") & ")"
                    dt = New DataTable
                    dt = SqlHelper.ExecuteDataTable(ins_qry)
                End If
            Next
            CommonCode.insert_system_log("Bidder Added", "but_save_Click", Request.QueryString.Get("i"), "", "Seller")
            Response.Write("<script>window.opener.popup_PostBackFunction();window.close();</script>")
        End If
    End Sub

    Protected Sub RadGrid_BidderAssign_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid_BidderAssign.PreRender
        RadGrid_BidderAssign.GroupingSettings.CaseSensitive = False
        Dim menu As GridFilterMenu = RadGrid_BidderAssign.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub

End Class
