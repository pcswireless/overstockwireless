﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    MaintainScrollPositionOnPostback="true" CodeFile="seller-details.aspx.vb" Inherits="Companies_seller_details"
    EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/Log/UserControls/SystemLogLink.ascx" TagName="SystemLogLink"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <asp:HiddenField ID="hd_seller_id" runat="server" Value="0" />
    <script type="text/javascript">

        function open_buyer_assign(sid) {
            window.open('assign_bidders_new.aspx?i=' + sid, '_assign2', 'top=' + ((screen.height - 480) / 2) + ', left=' + ((screen.width - 840) / 2) + ', height=620, width=820,scrollbars=yes,toolbars=no,resizable=0;');
            window.focus();
            set_panel_popup_status('bidder')
            return false;

        }

        function open_user_assign(sid) {
            window.open('assign_users_new.aspx?i=' + sid, '_assign3', 'top=' + ((screen.height - 510) / 2) + ', left=' + ((screen.width - 800) / 2) + ', height=540, width=800,scrollbars=yes,toolbars=no,resizable=0;');
            window.focus();
            set_panel_popup_status('user')
            return false;

        }
        function open_pop_win(_path, _id, _winname) {
            w1 = window.open(_path + '?i=' + _id, _winname, 'top=' + ((screen.height - 510) / 2) + ', left=' + ((screen.width - 800) / 2) + ', height=540, width=800,scrollbars=yes,toolbars=no,resizable=1;');
            w1.focus();
            return false;

        }


        function popup_PostBackFunction() {
            //alert(document.getElementById('<%=hid_popup_status.ClientID %>').value);
            document.getElementById('<%=but_bind_user_bidder.ClientID %>').disabled = false;
            document.getElementById('<%=but_bind_user_bidder.ClientID %>').click();

            //__doPostBack('<%=but_bind_user_bidder.ClientID %>', ''); document.getElementById("b1").disabled=false;
        }

        function set_panel_popup_status(_popup) {
            document.getElementById('<%=hid_popup_status.ClientID %>').value = _popup;
        }
    </script>
    <telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
        <script type="text/javascript">
        

            
            
            function conditionalPostback(sender, eventArgs) {
                var theRegexp = new RegExp("\.UpdateButton$|\.PerformInsertButton$", "ig");
                if (eventArgs.get_eventTarget().match(theRegexp)) {
                    eventArgs.set_enableAjax(false);
                    //                    var upload = $find(window['fl_others_upload_1']);

                    //                    //AJAX is disabled only if file is selected for upload
                    //                    if (upload.getFileInputs()[0].value != "") {
                    //                        eventArgs.set_enableAjax(false);
                    //                    }
                }
            }
            function conditionalPostback1(e, sender) {

                sender.set_enableAjax(false);

            }

            //            function validateRadUpload(source, e) {
            //                e.IsValid = false;

            //                var upload = $find(source.parentNode.getElementsByTagName('div')[0].id);
            //                var inputs = upload.getFileInputs();
            //                for (var i = 0; i < inputs.length; i++) {
            //                    //check for empty string or invalid extension
            //                    if (inputs[i].value != "" && upload.isExtensionValid(inputs[i].value)) {
            //                        e.IsValid = true;
            //                        break;
            //                    }
            //                }
            //            }
            function pageLoad_1() {
                // alert(Page_ClientValidate());
                if (Page_ClientValidate() == false) {
                    alert('false');
                    // Page_BlockSubmit = false;
                    return false;
                }
                else {

                    //alert('true'); // Page_BlockSubmit = true; 
                    return true;
                }
            }
            function TriggerValidator(sender, args) {
                return Page_ClientValidate();
            }
            //On insert and update buttons click temporarily disables ajax to perform upload actions

            //            function RequestStart(e, sender) {
            //                var upload1 = window.find('fl_img_basic_upload_1');
            //             //  if (upload1 == null) { alert('upload1'); }
            //               // else {
            //                    if (upload1.getFileInputs()[0].value != "") {
            //                        sender.set_enableAjax(false);
            //                    } 
            //               // }
            //                var upload2 = $find(window['fl_img_basic_upload_2']);
            //              //  if (upload2 != null) {
            //                    if (upload2.getFileInputs()[0].value != "") {
            //                        sender.set_enableAjax(false);
            //                    }
            //               // }
            //                var upload3 = $find(window['fl_img_basic_upload_3']);
            //             //   if (upload3 != null) {
            //                    if (upload3.getFileInputs()[0].value != "") {
            //                        sender.set_enableAjax(false);
            //                    }
            //              //  }

            //                }
            function find_div_class() {

                var divCollection = document.getElementsByTagName("div");
                for (var i = 0; i < divCollection.length; i++) {

                    if (divCollection[i].className == "InpBg") {

                        if (divCollection[i].id.indexOf("basic") != -1)
                            divCollection[i].className = "InpBg_basic";
                        //findMeText = divCollection[i].innerHTML;
                        // alert(i);

                    }

                }
            }

            function find_div_class_change() {
                //  alert('edit');
                var divCollection = document.getElementsByTagName("div");
                for (var i = 0; i < divCollection.length; i++) {
                    if (divCollection[i].className == "InpBg_basic" & divCollection[i].id.indexOf("basic") != -1) {
                        divCollection[i].className = "InpBg";
                        // findMeText = divCollection[i].innerHTML;
                        //alert(i);
                    }
                }
            }

            //  window.onunload = find_div_class();
            function ButtonPostback(sender, args) {
                // alert(args.get_eventTarget().toString);
                //args.set_enableAjax(false)
                //args.set_enableAjax(false);
                if (args.get_eventTarget() == "<%= but_basic_cancel.UniqueID %>") {
                    // args.set_enableAjax(false);

                }
                if (args.get_eventTarget() == "<%= but_basic_save.UniqueID %>") {
                    //  alert(args.get_eventTarget().toString);
                    args.set_enableAjax(false);
                    //                    alert(args.get_eventTarget().toString);
                }
                if (args.get_eventTarget() == "<%= but_basic_edit.UniqueID %>") {
                    // args.set_enableAjax(false);

                }
                if (args.get_eventTarget() == "<%= but_basic_update.UniqueID %>") {
                    //  alert(args.get_eventTarget().toString);
                    args.set_enableAjax(false);
                    //                    alert(args.get_eventTarget().toString);
                }

            }
            function Cancel_Ajax(sender, args) {
                // alert(args.get_eventTarget().toString);
                // args.set_enableAjax(true);
                if (args.get_eventTarget() == "<%= but_basic_update.UniqueID %>") {
                    //  alert(args.get_eventTarget().toString);
                    args.set_enableAjax(true);
                    //                    alert(args.get_eventTarget().toString);
                }
                if (args.get_eventTarget() == "<%= but_basic_edit.UniqueID %>") {
                    args.set_enableAjax(false);

                }
                if (args.get_eventTarget() == "<%= but_basic_save.UniqueID %>") {
                    //  alert(args.get_eventTarget().toString);
                    args.set_enableAjax(true);
                    //                    alert(args.get_eventTarget().toString);
                }
                if (args.get_eventTarget() == "<%= but_basic_cancel.UniqueID %>") {
                    //  args.set_enableAjax(false);

                }

            }
            function realPostBack(eventTarget, eventArgument, _a1) {
                $find(_a1).set_enableAjax(false);
                __doPostBack(eventTarget, eventArgument);
            }

            //                       function conditionalPostback(sender, args) {
            //               
            //                    args.set_enableAjax(false);

            //                }
            function ButtonPostback_profile(sender, args) {
                //args.IsValid = TriggerValidator(sender, args);
                //return TriggerValidator(sender, args);
                //                    alert(Page_ClientValidate());
                //                    if (Page_ClientValidate()) {
                //                        if (Page_ClientValidate() == false) {
                //                            args.IsValid = false;
                //                            return false;
                //                        }
                //                        else {
                //                            args.IsValid = true;
                //                            return true;
                //                        }
                //                    }
                //                    else { return true;}
                // alert(args.get_eventTarget().toString);
                //                    args.set_enableAjax(false);
                //                    if (args.get_eventTarget() == "<%= but_profile_update.UniqueID %>") {
                //                        //  alert(args.get_eventTarget().toString);
                //                        args.set_enableAjax(true);
                //                        //                    alert(args.get_eventTarget().toString);
                //                    }
                //                    if (args.get_eventTarget() == "<%= but_profile_edit.UniqueID %>") {
                //                        args.set_enableAjax(false);

                //                    }
            }
            function Cancel_Ajax_profile(sender, args) {
                // alert(args.get_eventTarget().toString);
                //                    args.set_enableAjax(true);
                //                    if (args.get_eventTarget() == "<%= but_profile_edit.UniqueID %>") {
                //                        args.set_enableAjax(false);

                //                    }
                //                    if (args.get_eventTarget() == "<%= but_profile_update.UniqueID %>") {
                //                        //  alert(args.get_eventTarget().toString);
                //                        args.set_enableAjax(true);
                //                        //                    alert(args.get_eventTarget().toString);
                //                    }
            }
        </script>
    </telerik:RadScriptBlock>
    <asp:HiddenField runat="server" ID="hid_popup_status" />
    <asp:Button ID="but_bind_user_bidder" runat="server" BorderStyle="None" BackColor="Transparent"
        ForeColor="Transparent" Width="0" Height="0" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <%-- <telerik:AjaxSetting AjaxControlID="RadAjaxPanel_Basic">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dd_basic_country">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="but_basic_edit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic_2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_basic_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic_2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_basic_cancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic_2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_basic_save">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic_2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_bind_user_bidder">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid_Bidder" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_bind_user_bidder">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid_User" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_Bidder">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid_Bidder" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_User">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid_User" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_comment_submit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid_Comment" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="txt_comment" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_profile_edit" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Profile" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="but_profile_update" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="div_profile_cancel" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="but_profile_edit" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_profile_update" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Profile" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="div_profile_cancel" />
                    <telerik:AjaxUpdatedControl ControlID="but_profile_edit" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--     <telerik:AjaxSetting AjaxControlID="but_profile_update" > 
                    <UpdatedControls> 
                        <telerik:AjaxUpdatedControl ControlID="Panel2_Content" /> 
                    </UpdatedControls> 
                </telerik:AjaxSetting> --%>
            <telerik:AjaxSetting AjaxControlID="but_profile_cancel" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Profile" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="but_profile_update" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="but_profile_edit" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="div_profile_cancel" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_Others">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Other" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div style="float: left;">
                    <div class="pageheading">
                        <asp:Literal ID="page_heading" runat="server" Text="New Company"></asp:Literal></div>
                </div>
                <div style="float: right; padding-right: 20px; width: 180px;">
                    <div style="float: left;">
                        <uc4:SystemLogLink ID="UC_System_log_link" runat="server" />
                    </div>
                    <div style="float: left; padding-left: 10px;">
                        <a href="javascript:void(0);" onclick="javascript:open_help('3');">
                            <img src="/Images/help.gif" border="none" alt="" />
                        </a>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="PageTab">
                    <span>
                        <%= IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), "Add Company Details", "Edit Company")%></span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="PageTabContentEdit">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td class="tdTabItem">
                            <asp:Panel ID="Panel1_Header" runat="server" CssClass="pnlTabItemHeader">
                                <div class="tabItemHeader" style="background: url(/Images/basic_info.gif) no-repeat;
                                    background-position: 10px 0px;">
                                    Basic Information
                                </div>
                                <asp:Image ID="Image1" runat="server" ImageAlign="left" ImageUrl="/Images/up_Arrow.gif"
                                    CssClass="panelimage" />
                            </asp:Panel>
                            <ajax:CollapsiblePanelExtender ID="cpe1" BehaviorID="cpe1" runat="server" Enabled="True"
                                TargetControlID="Panel1_Content" CollapseControlID="Panel1_Header" ExpandControlID="Panel1_Header"
                                Collapsed="false" ImageControlID="Image1" CollapsedImage="/Images/down_Arrow.gif"
                                ExpandedImage="/Images/up_Arrow.gif">
                            </ajax:CollapsiblePanelExtender>
                            <ajax:CollapsiblePanelExtender ID="cpe11" BehaviorID="cpe1" runat="server" Enabled="True"
                                TargetControlID="Panel1_Content1button" CollapseControlID="Panel1_Header" ExpandControlID="Panel1_Header"
                                Collapsed="false" ImageControlID="Image1" CollapsedImage="/Images/down_Arrow.gif"
                                CollapsedSize="0" ExpandedImage="/Images/up_Arrow.gif">
                            </ajax:CollapsiblePanelExtender>
                            <asp:Panel ID="Panel1_Content" runat="server" CssClass="collapsePanel">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel_Basic" runat="server" CssClass="TabGrid" LoadingPanelID="RadAjaxLoadingPanel1">
                                    <div style="padding-left: 5px;">
                                        <asp:Label ID="lbl_error" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                                        <asp:HiddenField ID="hid_seller_active_modify" runat="server" Value="1" />
                                    </div>
                                    <table cellpadding="0" cellspacing="2" border="0" width="838px">
                                        <tr>
                                            <td class="caption" style="width: 145px;">
                                                Company&nbsp;<span id="span_basic_company_name" runat="server" class="req_star">*</span>
                                            </td>
                                            <td style="width: 270px;" class="details">
                                                <asp:TextBox ID="txt_basic_company_name" CssClass="inputtype" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requ_txt_basic_company_name" runat="server" Font-Size="10px"
                                                    ControlToValidate="txt_basic_company_name" ValidationGroup="_basic" Display="Dynamic"
                                                    ErrorMessage="<br>Name Required"></asp:RequiredFieldValidator>
                                                <asp:Label ID="lbl_basic_company_name" runat="server" />
                                            </td>
                                            <td class="caption">
                                                Company Website
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_basic_website" CssClass="inputtype" runat="server"></asp:TextBox>
                                                <asp:Label ID="lbl_basic_website" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                Time Zone
                                            </td>
                                            <td class="details">
                                                <%--<div class="InpBg" id="div_basic_timezone" runat="server"></div>--%>
                                                <asp:TextBox ID="txt_basic_timezone" CssClass="inputtype" runat="server"></asp:TextBox>
                                                <asp:Label ID="lbl_basic_timezone" runat="server" />
                                            </td>
                                            <td class="caption">
                                                Tax ID/SSN #
                                            </td>
                                            <td class="details">
                                                <%--<div class="InpBg" id="div_basic_ssn" runat="server"></div>--%>
                                                <asp:TextBox ID="txt_basic_ssn" CssClass="inputtype" runat="server"></asp:TextBox>
                                                <asp:Label ID="lbl_basic_ssn" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="dv_md_sub_heading">
                                                Contact details
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 145px;">
                                                First Name&nbsp;<span id="span_basic_first_name" runat="server" class="req_star">*</span>
                                            </td>
                                            <td style="width: 270px;" class="details">
                                                <asp:TextBox ID="txt_basic_first_name" CssClass="inputtype" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Required_txt_basic_first_name" runat="server" Font-Size="10px"
                                                    ControlToValidate="txt_basic_first_name" ValidationGroup="_basic" Display="Dynamic"
                                                    ErrorMessage="<br>First Name Required"></asp:RequiredFieldValidator>
                                                <asp:Label ID="lbl_basic_first_name" runat="server" />
                                            </td>
                                            <td class="caption" style="width: 145px;">
                                                Last Name
                                            </td>
                                            <td style="width: 270px;" class="details">
                                                <asp:TextBox ID="txt_basic_last_name" CssClass="inputtype" runat="server"></asp:TextBox>
                                                <asp:Label ID="lbl_basic_last_name" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 145px;">
                                                Title
                                            </td>
                                            <td style="width: 270px;" class="details">
                                                <%--<div class="InpBg" id="div_basic_contact" runat="server"></div>--%>
                                                <asp:DropDownList ID="ddl_basic_title" runat="server" DataTextField="name" DataValueField="name"
                                                    CssClass="DropDown" Width="204" />
                                                <asp:Label ID="lbl_basic_title" runat="server" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                Street&nbsp;<span id="span_basic_address_1" runat="server" class="req_star">*</span>
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_basic_address_1" TextMode="MultiLine" Height="30px" CssClass="inputtype"
                                                    runat="server"></asp:TextBox>
                                                <asp:Label ID="lbl_basic_address_1" runat="server" />
                                                <asp:RequiredFieldValidator ID="rfv_address1" runat="server" ControlToValidate="txt_basic_address_1"
                                                    ValidationGroup="_basic" Display="Dynamic" ErrorMessage="<br />Street Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                            </td>
                                            <td class="caption">
                                                Street Address 2
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_basic_address_2" TextMode="MultiLine" Height="30px" CssClass="inputtype"
                                                    runat="server"></asp:TextBox>
                                                <asp:Label ID="lbl_basic_address_2" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                City&nbsp;<span id="span_basic_city" runat="server" class="req_star">*</span>
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_basic_city" CssClass="inputtype" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfv_city" runat="server" ControlToValidate="txt_basic_city"
                                                    ValidationGroup="_basic" Display="Dynamic" ErrorMessage="<br />City Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                                <asp:Label ID="lbl_basic_city" runat="server" />
                                            </td>
                                            <td class="caption">
                                                Country&nbsp;<span id="span_basic_country" runat="server" class="req_star">*</span>
                                            </td>
                                            <td class="details">
                                                <asp:DropDownList ID="dd_basic_country" CssClass="DropDown" ValidationGroup="_basic"
                                                    runat="server" Width="204">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Require_dd_basic_country" runat="server" Font-Size="10px"
                                                    ControlToValidate="dd_basic_country" ValidationGroup="_basic" InitialValue="0"
                                                    Display="Dynamic" ErrorMessage="<br>Country Required"></asp:RequiredFieldValidator>
                                                <asp:Label ID="lbl_basic_country" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                State/Province&nbsp;<span id="span_basic_state" runat="server" class="req_star">*</span>
                                            </td>
                                            <td class="details">
                                                <asp:DropDownList ID="dd_basic_state" runat="server" CssClass="DropDown" Width="204">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Require_dd_basic_state" runat="server" Font-Size="10px"
                                                    ControlToValidate="dd_basic_state" ValidationGroup="_basic" InitialValue="0"
                                                    Display="Dynamic" ErrorMessage="<br>State Required"></asp:RequiredFieldValidator>
                                                <asp:Label ID="lbl_basic_state" runat="server" />
                                            </td>
                                            <td class="caption">
                                                Postal Code&nbsp;<span id="span_basic_zip" runat="server" class="req_star">*</span>
                                            </td>
                                            <td class="details">
                                                <%--<div class="InpBg" id="div_basic_zip" runat="server"></div>--%>
                                                <asp:TextBox ID="txt_basic_zip" CssClass="inputtype" runat="server" MaxLength="15"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfv_zip" runat="server" ControlToValidate="txt_basic_zip"
                                                    ValidationGroup="_basic" Display="Dynamic" ErrorMessage="<br />Postal Code Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="Regular_txt_basic_zip" runat="server" Font-Size="10px"
                                                    ControlToValidate="txt_basic_zip" ValidationExpression="^[a-zA-Z0-9 ]*$" ValidationGroup="_basic"
                                                    Display="Dynamic" ErrorMessage="<br>Invalid Postal Code"></asp:RegularExpressionValidator>
                                                <asp:Label ID="lbl_basic_zip" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                Phone&nbsp;<span id="span_basic_phone" runat="server" class="req_star">*</span>
                                            </td>
                                            <td class="details">
                                                <%--<div class="InpBg" id="div_basic_phone" runat="server"></div>--%>
                                                <asp:TextBox ID="txt_basic_phone" CssClass="inputtype" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Font-Size="10px"
                                                    ControlToValidate="txt_basic_phone" ValidationGroup="_basic" Display="Dynamic"
                                                    ErrorMessage="<br>Phone Required"></asp:RequiredFieldValidator>
                                                <%--<asp:RegularExpressionValidator ID="Regular_txt_basic_phone" runat="server" Font-Size="10px"
                                                    ControlToValidate="txt_basic_phone" ValidationExpression="[0-9]{10,13}" ValidationGroup="_basic"
                                                    Display="Dynamic" ErrorMessage="<br>Invalid Phone"></asp:RegularExpressionValidator>--%>
                                                <asp:Label ID="lbl_basic_phone" runat="server" />
                                            </td>
                                            <td class="caption">
                                                Fax
                                            </td>
                                            <td class="details">
                                                <%--<div class="InpBg" id="div_basic_fax" runat="server"></div>--%>
                                                <asp:TextBox ID="txt_basic_fax" CssClass="inputtype" runat="server"></asp:TextBox>
                                                <%--<asp:RegularExpressionValidator ID="Regular_txt_basic_fax" runat="server" Font-Size="10px"
                                                    ControlToValidate="txt_basic_fax" ValidationExpression="[0-9]{10,13}" ValidationGroup="_basic"
                                                    Display="Dynamic" ErrorMessage="<br>Invalid Fax"></asp:RegularExpressionValidator>--%>
                                                <asp:Label ID="lbl_basic_fax" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                Email&nbsp;<span id="span_basic_email" runat="server" class="req_star">*</span>
                                            </td>
                                            <td class="details">
                                                <%--<div class="InpBg" id="div_basic_email" runat="server"></div>--%>
                                                <asp:TextBox ID="txt_basic_email" CssClass="inputtype" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Require_txt_basic_email" runat="server" Font-Size="10px"
                                                    ControlToValidate="txt_basic_email" ValidationGroup="_basic" Display="Dynamic"
                                                    ErrorMessage="<br>Email Required"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="Regular_txt_basic_email" ValidationGroup="_basic"
                                                    runat="server" ControlToValidate="txt_basic_email" ErrorMessage="<br>Invalid Email"
                                                    Display="Dynamic" Font-Size="10px" ValidationExpression="^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$"></asp:RegularExpressionValidator>
                                                <asp:Label ID="lbl_basic_email" runat="server" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="dv_md_sub_heading">
                                                Other details
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                Shipping Value
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_basic_ship_value" CssClass="inputtype" MaxLength="6" runat="server"></asp:TextBox>
                                                <asp:Label ID="lbl_basic_ship_value" runat="server" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Font-Size="10px"
                                                    ControlToValidate="txt_basic_ship_value" ValidationExpression="[0-9]" ValidationGroup="_basic"
                                                    Display="Dynamic" ErrorMessage="<br>Invalid Shipping Value"></asp:RegularExpressionValidator>
                                            </td>
                                            <td class="caption">
                                                Shipping Operator
                                            </td>
                                            <td class="details">
                                                <asp:DropDownList ID="dd_basic_ship_operator" runat="server" CssClass="DropDown"
                                                    Width="204">
                                                    <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="$" Value="$"></asp:ListItem>
                                                    <asp:ListItem Text="%" Value="%"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:Label ID="lbl_basic_ship_operator" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <%--<td class="caption">
                                                Auto Approve Bidder
                                            </td>
                                            <td class="details">
                                                <asp:CheckBox ID="chk_basic_approve_bidder" runat="server" />
                                                <asp:Label ID="lbl_basic_approve_bidder" runat="server" />
                                               
                                            </td>--%>
                                            <td class="caption">
                                                VAT #
                                            </td>
                                            <td class="details">
                                               <asp:TextBox ID="txt_basic_vat" CssClass="inputtype" runat="server"></asp:TextBox>
                                                <asp:Label ID="lbl_basic_vat" runat="server" />
                                            </td>
                                            <td class="caption">
                                                Is Active
                                            </td>
                                            <td class="details">
                                                <asp:CheckBox ID="chk_basic_active" runat="server" />
                                                <asp:Label ID="lbl_basic_active" runat="server" />
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="caption">
                                                Logo
                                            </td>
                                            <td valign="top" class="details">
                                                <asp:FileUpload ID="fl_img_basic_upload_1" runat="server" CssClass="TextBox" Width="148"
                                                    size="6" />
                                                <asp:RegularExpressionValidator ID="revImage1" Font-Size="10px" ValidationGroup="_basic"
                                                    ControlToValidate="fl_img_basic_upload_1" ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G))$"
                                                    Text="*" runat="server" />
                                                <asp:CheckBox ID="CHK_Image1" Text="Remove" runat="server" Visible="false" />
                                                <br />
                                                <div style="float: left; padding-top: 6px;">
                                                    <a id="a_img_image1" runat="server" href="javascript:void(0);" target="_blank" style="font-family: Arial;
                                                        font-size: 10pt; text-decoration: none;">
                                                        <div runat="server" onmouseout="hide_tip_new();" style="cursor: hand;" id="DIV_IMG_Default_Logo1_Img">
                                                            <asp:Image BorderWidth="0" ID="IMG_Image_1" Width="100" Height="60" runat="server"
                                                                ImageUrl="/images/imagenotavailable.gif"></asp:Image>
                                                        </div>
                                                    </a>
                                                </div>
                                            </td>
                                            <td class="caption">
                                                Image 2<br />
                                                <span style="color: Gray;">Future use</span>
                                            </td>
                                            <td class="details">
                                                <asp:FileUpload ID="fl_img_basic_upload_2" runat="server" CssClass="TextBox" Width="148"
                                                    size="6" />
                                                <asp:RegularExpressionValidator ID="revImage2" Font-Size="10px" ValidationGroup="_basic"
                                                    ControlToValidate="fl_img_basic_upload_2" ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G))$"
                                                    Text="*" runat="server" />
                                                <asp:CheckBox ID="CHK_Image2" Text="Remove" runat="server" Visible="false" />
                                                <br />
                                                <div style="float: left; padding-top: 6px;">
                                                    <a id="a_IMG_Image_2" runat="server" href="javascript:void(0);" target="_blank" style="font-family: Arial;
                                                        font-size: 10pt; text-decoration: none;">
                                                        <div runat="server" onmouseout="hide_tip_new();" style="cursor: hand;" id="DIV_IMG_Default_Logo2_Img">
                                                            <asp:Image BorderWidth="0" ID="IMG_Image_2" Width="100" Height="60" runat="server"
                                                                ImageUrl="/images/imagenotavailable.gif"></asp:Image>
                                                        </div>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                Image 3<br />
                                                <span style="color: Gray;">Future use</span>
                                            </td>
                                            <td class="details">
                                                <asp:FileUpload ID="fl_img_basic_upload_3" runat="server" CssClass="TextBox" Width="148"
                                                    size="6" />
                                                <asp:RegularExpressionValidator ID="revImage3" Font-Size="10px" ValidationGroup="_basic"
                                                    ControlToValidate="fl_img_basic_upload_3" ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G))$"
                                                    Text="*" runat="server" />
                                                <asp:CheckBox ID="CHK_Image3" Text="Remove" runat="server" Visible="false" />&nbsp;
                                                <br />
                                                <div style="float: left; padding-top: 6px;">
                                                    <a id="a_IMG_Image_3" runat="server" href="javascript:void(0);" target="_blank" style="font-family: Arial;
                                                        font-size: 10pt; text-decoration: none;">
                                                        <div runat="server" onmouseout="hide_tip_new();" style="cursor: hand;" id="DIV_IMG_Default_Logo3_Img">
                                                            <asp:Image BorderWidth="0" ID="IMG_Image_3" Width="100" Height="60" runat="server"
                                                                ImageUrl="/images/imagenotavailable.gif"></asp:Image>
                                                        </div>
                                                    </a>
                                                </div>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </telerik:RadAjaxPanel>
                            </asp:Panel>
                        </td>
                        <td class="fixedcolumn" valign="top">
                            <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                            <asp:Panel ID="Panel1_Content1button" runat="server" CssClass="collapsePanel">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel_Basic_2" runat="server" ClientEvents-OnRequestStart="ButtonPostback"
                                    ClientEvents-OnResponseEnd="Cancel_Ajax">
                                    <asp:Panel ID="Panel1_Content1button_per" runat="server">
                                        <div class="addButton">
                                            <asp:ImageButton ID="but_basic_save" ValidationGroup="_basic" runat="server" AlternateText="Save"
                                                ImageUrl="/images/save.gif" />
                                            <asp:ImageButton ID="but_basic_update" ValidationGroup="_basic" runat="server" AlternateText="Update"
                                                ImageUrl="/images/update.gif"  OnClientClick ="return chk_aprv();"/>
                                            <asp:ImageButton ID="but_basic_edit" runat="server" AlternateText="Edit" ImageUrl="/images/edit.gif" />
                                        </div>
                                        <div class="cancelButton" id="div_basic_cancel" runat="server">
                                            <asp:ImageButton ID="but_basic_cancel" CausesValidation="false" runat="server" AlternateText="Cancel"
                                                ImageUrl="/images/cancel.gif" /></div>
                                    </asp:Panel>
                                </telerik:RadAjaxPanel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <asp:Panel ID="pnl_edit_mode" runat="server">
                        <tr>
                            <td class="tdTabItem">
                                <asp:Panel ID="Panel2_Header" runat="server" CssClass="pnlTabItemHeader">
                                    <div class="tabItemHeader" style="background: url(/Images/profile_master_icon.gif) no-repeat;
                                        background-position: 10px 0px;">
                                        Profile
                                    </div>
                                    <asp:Image ID="Image2" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                        CssClass="panelimage" />
                                </asp:Panel>
                                <ajax:CollapsiblePanelExtender ID="cpe2" BehaviorID="cpe2" runat="server" Enabled="True"
                                    ImageControlID="Image2" TargetControlID="Panel2_Content" CollapseControlID="Panel2_Header"
                                    ExpandControlID="Panel2_Header" Collapsed="True" CollapsedImage="/Images/down_Arrow.gif"
                                    ExpandedImage="/Images/up_Arrow.gif">
                                </ajax:CollapsiblePanelExtender>
                                <ajax:CollapsiblePanelExtender ID="cpe22" BehaviorID="cpe2" runat="server" Enabled="True"
                                    TargetControlID="Panel2_Content2button" CollapseControlID="Panel2_Header" ExpandControlID="Panel2_Header"
                                    Collapsed="true" ImageControlID="Image2" CollapsedImage="/Images/down_Arrow.gif"
                                    ExpandedImage="/Images/up_Arrow.gif">
                                </ajax:CollapsiblePanelExtender>
                                <asp:Panel ID="Panel2_Content" runat="server" CssClass="collapsePanel">
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel_Profile" EnableEmbeddedScripts="true" runat="server"
                                        CssClass="TabGrid">
                                        <table cellpadding="0" cellspacing="2" border="0" width="838px">
                                            <tr>
                                                <td class="caption" style="width: 145px;">
                                                    Business Type
                                                </td>
                                                <td class="details" colspan="3">
                                                    <asp:CheckBoxList ID="chk_profile_business" RepeatColumns="3" RepeatDirection="Vertical"
                                                        runat="server" ValidationGroup="_profile" CellPadding="2" CellSpacing="1">
                                                    </asp:CheckBoxList>
                                                    <asp:Label ID="lbl_profile_business" runat="server" />
                                                    <%--<telerik:RadGrid ID="lst_profile_business" GridLines="None" runat="server" AllowPaging="False"
                                                    AutoGenerateColumns="False" AllowMultiRowSelection="false" Skin="Simple" AllowFilteringByColumn="false"
                                                    ShowHeader="false" ShowFooter="false" Width="200" CellPadding="5">

                                                    <MasterTableView DataKeyNames="business_type_id" HorizontalAlign="NotSet" AutoGenerateColumns="False">
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="name" HeaderText="name" UniqueName="name">
                                                            </telerik:GridBoundColumn>
                                                        </Columns>
                                                        <NoRecordsTemplate>
                                                            No Business type selected.
                                                        </NoRecordsTemplate>
                                                    </MasterTableView>
                                                </telerik:RadGrid>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption" style="width: 145px;">
                                                    Industry Type
                                                </td>
                                                <td class="details" colspan="3">
                                                    <asp:CheckBoxList ID="chk_profile_industry" RepeatColumns="3" RepeatDirection="Vertical"
                                                        runat="server" ValidationGroup="_profile" CellPadding="2" CellSpacing="1">
                                                    </asp:CheckBoxList>
                                                    <asp:Label ID="lbl_profile_industry" runat="server" />
                                                    <%--<telerik:RadGrid ID="lst_profile_industry" GridLines="None" runat="server" AllowPaging="False"
                                                    AutoGenerateColumns="False" AllowMultiRowSelection="false" Skin="Simple" AllowFilteringByColumn="false"
                                                    ShowHeader="false" ShowFooter="false" Width="200">
                                                    <HeaderStyle BackColor="#BEBEBE" />
                                                    <MasterTableView DataKeyNames="industry_type_id" HorizontalAlign="NotSet" AutoGenerateColumns="False">
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="name" HeaderText="name" UniqueName="name">
                                                            </telerik:GridBoundColumn>
                                                        </Columns>
                                                        <NoRecordsTemplate>
                                                            No Industry type selected.
                                                        </NoRecordsTemplate>
                                                    </MasterTableView>
                                                </telerik:RadGrid>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Routing #&nbsp;<span id="span_profile_routing" runat="server" class="req_star">*</span>
                                                </td>
                                                <td class="details">
                                                    <%--<div class="InpBg" id="div_profile_routing" runat="server"></div>--%>
                                                    <asp:TextBox ID="txt_profile_routing" CssClass="inputtype" runat="server"></asp:TextBox>
                                                    <asp:Label ID="lbl_profile_routing" runat="server" />
                                                    <asp:RequiredFieldValidator ID="Required_profile_routing" runat="server" Font-Size="10px"
                                                        ControlToValidate="txt_profile_routing" ValidationGroup="_profile" Display="Dynamic"
                                                        ErrorMessage="<br>Routing# Required"></asp:RequiredFieldValidator>
                                                </td>
                                                <td class="caption">
                                                    Account #&nbsp;<span id="span_profile_account" runat="server" class="req_star">*</span>
                                                </td>
                                                <td class="details">
                                                    <%--<div class="InpBg" id="div_profile_account" runat="server"></div>--%>
                                                    <asp:TextBox ID="txt_profile_account" CssClass="inputtype" runat="server"></asp:TextBox>
                                                    <asp:Label ID="lbl_profile_account" runat="server" />
                                                    <asp:RequiredFieldValidator ID="Required_profile_account" runat="server" Font-Size="10px"
                                                        ControlToValidate="txt_profile_account" ValidationGroup="_profile" Display="Dynamic"
                                                        ErrorMessage="<br>Acc.# Required"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Name of Bank&nbsp;<span id="span_profile_bank_name" runat="server" class="req_star">*</span>
                                                </td>
                                                <td class="details">
                                                    <%--<div class="InpBg" id="div_profile_bank_name" runat="server"></div>--%>
                                                    <asp:TextBox ID="txt_profile_bank_name" CssClass="inputtype" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Required_txt_profile_bank_name" runat="server" Font-Size="10px"
                                                        ControlToValidate="txt_profile_bank_name" ValidationGroup="_profile" Display="Dynamic"
                                                        ErrorMessage="<br>Name Required"></asp:RequiredFieldValidator>
                                                    <asp:Label ID="lbl_profile_bank_name" runat="server" />
                                                </td>
                                                <td class="caption">
                                                    Bank Contact #
                                                </td>
                                                <td class="details">
                                                    <%--<div class="InpBg" id="div_profile_bank_contact" runat="server"></div>--%>
                                                    <asp:TextBox ID="txt_profile_bank_contact" CssClass="inputtype" runat="server"></asp:TextBox>
                                                    <asp:Label ID="lbl_profile_bank_contact" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Bank Address
                                                </td>
                                                <td class="details">
                                                    <%--<div class="InpBg" id="div_profile_bank_address" runat="server"></div>--%>
                                                    <asp:TextBox ID="txt_profile_bank_address" CssClass="inputtype" runat="server"></asp:TextBox>
                                                    <asp:Label ID="lbl_profile_bank_address" runat="server" />
                                                </td>
                                                <td class="caption">
                                                    Bank Phone #
                                                </td>
                                                <td class="details">
                                                    <%--<div class="InpBg" id="div_profile_bank_phone" runat="server"></div>--%>
                                                    <asp:TextBox ID="txt_profile_bank_phone" CssClass="inputtype" runat="server"></asp:TextBox>
                                                    <%-- <asp:RegularExpressionValidator ID="Regulartxt_profile_bank_phone" runat="server"
                                                        Font-Size="10px" ControlToValidate="txt_profile_bank_phone" ValidationExpression="[0-9]{10,13}"
                                                        ValidationGroup="_profile" Display="Dynamic" ErrorMessage="<br>Wrong Phone"></asp:RegularExpressionValidator>--%>
                                                    <asp:Label ID="lbl_profile_bank_phone" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </telerik:RadAjaxPanel>
                                </asp:Panel>
                            </td>
                            <td class="fixedcolumn" valign="top">
                                <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                <asp:Panel ID="Panel2_Content2button" runat="server" CssClass="collapsePanel">
                                    <%--<telerik:RadAjaxPanel ID="RadAjaxPanel_Profile_2" runat="server" EnableEmbeddedScripts="true"  ClientEvents-OnRequestStart="ButtonPostback_profile" ClientEvents-OnResponseEnd="Cancel_Ajax_profile">--%>
                                    <asp:Panel ID="Panel2_Content2button_per" runat="server">
                                        <div class="addButton">
                                            <asp:ImageButton ID="but_profile_update" ValidationGroup="_profile" runat="server"
                                                AlternateText="Update" ImageUrl="/images/update.gif" />
                                            <asp:ImageButton ID="but_profile_edit" runat="server" AlternateText="Edit" ImageUrl="/images/edit.gif" />
                                        </div>
                                        <div class="cancelButton" id="div_profile_cancel" runat="server">
                                            <asp:ImageButton ID="but_profile_cancel" runat="server" CausesValidation="false"
                                                AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                        </div>
                                    </asp:Panel>
                                    <%-- </telerik:RadAjaxPanel>--%>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr id="trBidders" runat="server">
                            <td class="tdTabItem">
                                <asp:Panel ID="Panel3_Header" runat="server" CssClass="pnlTabItemHeader">
                                    <div class="tabItemHeader" style="background: url(/Images/bidders_icon.gif) no-repeat;
                                        background-position: 10px 0px;">
                                        Bidders
                                    </div>
                                    <asp:Image ID="Image3" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                        CssClass="panelimage" />
                                </asp:Panel>
                                <ajax:CollapsiblePanelExtender ID="cpe3" BehaviorID="cpe3" runat="server" Enabled="True"
                                    ImageControlID="Image3" TargetControlID="Panel3_Content" CollapseControlID="Panel3_Header"
                                    ExpandControlID="Panel3_Header" Collapsed="True" CollapsedImage="~/Images/down_Arrow.gif"
                                    ExpandedImage="~/Images/up_Arrow.gif">
                                </ajax:CollapsiblePanelExtender>
                                <asp:Panel ID="Panel3_Content" runat="server" CssClass="collapsePanel">
                                    <div class="TabGrid">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td style="font-size: 11px; font-weight: bold; padding-left: 15px;">
                                                    &nbsp;Selected Bidders
                                                </td>
                                                <td class="dv_md_sub_heading" style="padding-right: 10px; text-align: right;" id="div_assign_bidders"
                                                    runat="server">
                                                    <a runat="server" id="a_bid_assign" style="text-decoration: none; cursor: pointer;">
                                                        <img src="/images/assignbidders.gif" alt="ASSIGN BIDDERS" style="border: none;" /></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="sub_details">
                                                    <telerik:RadGrid ID="RadGrid_Bidder" DataSourceID="RadGrid_Bidder_SqlDataSource"
                                                        Skin="Vista" runat="server" AutoGenerateColumns="False" AllowSorting="True" AllowMultiRowSelection="False"
                                                        AllowPaging="True" PageSize="10" AllowAutomaticDeletes="true" AllowFilteringByColumn="true">
                                                        <PagerStyle Mode="NextPrevAndNumeric" />
                                                        <MasterTableView DataSourceID="RadGrid_Bidder_SqlDataSource" DataKeyNames="mapping_id"
                                                            AllowMultiColumnSorting="True" ItemStyle-Height="40" AlternatingItemStyle-Height="40"
                                                            AllowFilteringByColumn="True">
                                                            <CommandItemStyle BackColor="#d6d6d6" />
                                                            <NoRecordsTemplate>
                                                                No bidder selected to display
                                                            </NoRecordsTemplate>
                                                            <SortExpressions>
                                                                <telerik:GridSortExpression FieldName="company_name" SortOrder="Ascending" />
                                                            </SortExpressions>
                                                            <Columns>
                                                                <telerik:GridTemplateColumn HeaderText="Company Name" DataField="company_name" UniqueName="company_name"
                                                                    HeaderStyle-Width="250" ItemStyle-Width="250" SortExpression="company_name" FilterControlWidth="120"
                                                                    GroupByExpression="company_name [Company Name] Group By company_name">
                                                                    <ItemTemplate>
                                                                        <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');">
                                                                            <%# Eval("company_name")%>
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridBoundColumn SortExpression="name" HeaderText="Industry" HeaderButtonType="TextButton"
                                                                    DataField="name" UniqueName="name">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridButtonColumn CommandName="Delete" UniqueName="mapping_id" DataTextField="mapping_id"
                                                                    ConfirmText="Are you sure you want to delete this buyer?" ImageUrl="/Images/delete_grid.gif"
                                                                    HeaderStyle-Width="35px" ButtonType="ImageButton">
                                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                                </telerik:GridButtonColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                        <ClientSettings>
                                                            <Selecting AllowRowSelect="True"></Selecting>
                                                        </ClientSettings>
                                                    </telerik:RadGrid>
                                                    <asp:SqlDataSource ID="RadGrid_Bidder_SqlDataSource" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                                        ProviderName="System.Data.SqlClient" SelectCommand="SELECT A.mapping_id,B.buyer_id, dbo.fun_fetch_buyer_industries(B.buyer_id) as name, B.company_name FROM tbl_reg_buyer_seller_mapping A INNER JOIN tbl_reg_buyers B ON A.buyer_id = B.buyer_id where B.status_id=2 and A.seller_id=@seller_id"
                                                        runat="server" DeleteCommand="delete from tbl_reg_buyer_seller_mapping where mapping_id=@mapping_id">
                                                        <SelectParameters>
                                                            <asp:ControlParameter ControlID="hd_seller_id" Type="Int32" PropertyName="Value"
                                                                Name="seller_id" />
                                                        </SelectParameters>
                                                        <DeleteParameters>
                                                            <asp:Parameter Name="mapping_id" Type="Int32" />
                                                        </DeleteParameters>
                                                    </asp:SqlDataSource>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </td>
                            <td class="fixedcolumn" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTabItem">
                                <asp:Panel ID="Panel4_Header" runat="server" CssClass="pnlTabItemHeader">
                                    <div class="tabItemHeader" style="background: url(/Images/users_icon.gif) no-repeat;
                                        background-position: 10px 0px;">
                                        Users
                                    </div>
                                    <asp:Image ID="Image4" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                        CssClass="panelimage" />
                                </asp:Panel>
                                <ajax:CollapsiblePanelExtender ID="cpe4" BehaviorID="cpe4" runat="server" Enabled="True"
                                    ImageControlID="Image4" TargetControlID="Panel4_Content" CollapseControlID="Panel4_Header"
                                    ExpandControlID="Panel4_Header" Collapsed="True" CollapsedImage="/Images/down_Arrow.gif"
                                    ExpandedImage="/Images/up_Arrow.gif">
                                </ajax:CollapsiblePanelExtender>
                                <asp:Panel ID="Panel4_Content" runat="server" CssClass="collapsePanel">
                                    <div class="TabGrid">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td style="font-size: 11px; font-weight: bold; padding-left: 15px;">
                                                    Selected Users
                                                </td>
                                                <td class="dv_md_sub_heading" style="padding-right: 10px; text-align: right;" id="div_assign_users"
                                                    runat="server">
                                                    <a runat="server" id="a_user_assign" style="text-decoration: none; cursor: pointer;">
                                                        <img src="/images/assignuser.gif" alt="ASSIGN USERS" style="border: none;" /></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="sub_details">
                                                    <telerik:RadGrid ID="RadGrid_User" DataSourceID="RadGrid_User_SqlDataSource" runat="server"
                                                        Skin="Vista" AutoGenerateColumns="False" AllowSorting="True" AllowAutomaticDeletes="true"
                                                        AllowPaging="True" PageSize="10">
                                                        <PagerStyle Mode="NextPrevAndNumeric" />
                                                        <MasterTableView DataSourceID="RadGrid_User_SqlDataSource" DataKeyNames="mapping_id"
                                                            AllowMultiColumnSorting="false" ItemStyle-Height="40" AlternatingItemStyle-Height="40">
                                                            <NoRecordsTemplate>
                                                                No selected users to display
                                                            </NoRecordsTemplate>
                                                            <SortExpressions>
                                                                <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                                                            </SortExpressions>
                                                            <Columns>
                                                                <telerik:GridTemplateColumn HeaderText="Name" SortExpression="name" UniqueName="name"
                                                                    DataType="System.String" AutoPostBackOnFilter="false" FilterListOptions="VaryByDataType"
                                                                    ShowFilterIcon="true" DataField="name" HeaderStyle-Width="150" FilterControlWidth="150"
                                                                    GroupByExpression="name [name] Group By name">
                                                                    <ItemTemplate>
                                                                        <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Users/user_mouse_over.aspx?i=<%# Eval("user_id") %>','270','white','true');"
                                                                            onclick="redirectIframe('','','/Users/Userdetail.aspx?i=<%# Eval("user_id") %>')">
                                                                            <%# Eval("name")%></a>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridBoundColumn SortExpression="title" HeaderText="Title" HeaderButtonType="TextButton"
                                                                    DataField="title" UniqueName="title">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="username" HeaderText="User Name" AutoPostBackOnFilter="false"
                                                                    DataType="System.String" ShowFilterIcon="true" SortExpression="username" FilterListOptions="VaryByDataType"
                                                                    ItemStyle-Width="120" UniqueName="username" FilterControlWidth="100">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="mobile" HeaderText="Phone" AutoPostBackOnFilter="false"
                                                                    FilterListOptions="VaryByDataType" ShowFilterIcon="true" SortExpression="mobile"
                                                                    ItemStyle-Width="120" UniqueName="mobile" FilterControlWidth="100">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridButtonColumn CommandName="Delete" ConfirmText="Are you sure you want to delete this user?"
                                                                    HeaderStyle-Width="35px" ButtonType="ImageButton" UniqueName="DeleteColumn" ImageUrl="/Images/delete_grid.gif">
                                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                                </telerik:GridButtonColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                        <ClientSettings>
                                                            <Selecting AllowRowSelect="True"></Selecting>
                                                        </ClientSettings>
                                                    </telerik:RadGrid>
                                                    <asp:SqlDataSource ID="RadGrid_User_SqlDataSource" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                                        ProviderName="System.Data.SqlClient" SelectCommand="SELECT A.mapping_id, A.user_id, B.title,isnull((B.first_name+' '+B.last_name),'') as name,username,mobile,isnull(image_path,'') as image_path
                FROM  tbl_reg_seller_user_mapping A INNER JOIN tbl_sec_users B ON A.user_id = B.user_id where A.seller_id=@seller_id"
                                                        runat="server" DeleteCommand="delete from tbl_reg_seller_user_mapping where mapping_id=@mapping_id">
                                                        <SelectParameters>
                                                            <asp:ControlParameter ControlID="hd_seller_id" Type="Int32" PropertyName="Value"
                                                                Name="seller_id" />
                                                        </SelectParameters>
                                                        <DeleteParameters>
                                                            <asp:Parameter Name="mapping_id" Type="Int32" />
                                                        </DeleteParameters>
                                                    </asp:SqlDataSource>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </td>
                            <td class="fixedcolumn" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTabItem">
                                <asp:Panel ID="Panel5_Header" runat="server" CssClass="pnlTabItemHeader">
                                    <div class="tabItemHeader" style="background: url(/Images/others_icon.gif) no-repeat;
                                        background-position: 10px 0px;">
                                        Others
                                    </div>
                                    <asp:Image ID="Image5" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                        CssClass="panelimage" />
                                </asp:Panel>
                                <ajax:CollapsiblePanelExtender ID="cpe5" BehaviorID="cpe5" runat="server" Enabled="True"
                                    ImageControlID="Image5" TargetControlID="Panel5_Content" CollapseControlID="Panel5_Header"
                                    ExpandControlID="Panel5_Header" Collapsed="True" CollapsedImage="~/Images/down_Arrow.gif"
                                    ExpandedImage="~/Images/up_Arrow.gif">
                                </ajax:CollapsiblePanelExtender>
                                <asp:Panel ID="Panel5_Content" runat="server" CssClass="collapsePanel">
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel_Other" runat="server" ClientEvents-OnRequestStart="conditionalPostback1">
                                        <div class="TabGrid">
                                            <telerik:RadProgressManager ID="RadProgressManager1" runat="server" Skin="Simple" />
                                            <telerik:RadProgressArea ID="RadProgressArea1" runat="server" Skin="Simple" />
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td colspan="2" style="font-size: 11px; font-weight: bold; padding-left: 15px;">
                                                        Attachments
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" class="sub_details">
                                                        <telerik:RadGrid ID="RadGrid_Others" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                                            AllowPaging="True" PageSize="10" Skin="Vista" OnNeedDataSource="RadGrid_Others_NeedDatasource"
                                                            OnUpdateCommand="RadGrid_Others_UpdateCommand" OnDeleteCommand="RadGrid_Others_DeleteCommand"
                                                            enableajax="true" OnInsertCommand="RadGrid_Others_InsertCommand">
                                                            <PagerStyle Mode="NextPrevAndNumeric" />
                                                            <EditItemStyle CssClass="set_color" />
                                                            <AlternatingItemStyle CssClass="set_color" />
                                                            <MasterTableView DataKeyNames="seller_attachment_id" CommandItemDisplay="Top" EditMode="EditForms"
                                                                AllowFilteringByColumn="true" ItemStyle-Height="40" AlternatingItemStyle-Height="40">
                                                                <NoRecordsTemplate>
                                                                    No attachment to display
                                                                </NoRecordsTemplate>
                                                                <SortExpressions>
                                                                    <telerik:GridSortExpression FieldName="filename" SortOrder="Ascending" />
                                                                </SortExpressions>
                                                                <CommandItemSettings AddNewRecordText="Add New Attachment" ShowRefreshButton="false" />
                                                                <Columns>
                                                                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                        CancelImageUrl="/images/cancel.gif" UpdateImageUrl="/images/update.gif" InsertImageUrl="/images/save.gif"
                                                                        EditImageUrl="/Images/edit_grid.gif">
                                                                        <ItemStyle HorizontalAlign="center" CssClass="MyImageButton" Width="90" />
                                                                    </telerik:GridEditCommandColumn>
                                                                    <telerik:GridBoundColumn DataField="title" HeaderText="Title" SortExpression="title"
                                                                        UniqueName="Title">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="File Name" DataField="filename" SortExpression="filename"
                                                                        UniqueName="filename">
                                                                        <ItemTemplate>
                                                                            <a href="/Upload/Companies/attachments/<%#Eval("seller_id") %>/<%#Eval("filename") %>"
                                                                                target="_blank">
                                                                                <%#Eval("filename") %></a>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                </Columns>
                                                                <EditFormSettings EditFormType="Template" EditColumn-ItemStyle-Width="100%" EditColumn-HeaderStyle-HorizontalAlign="Left"
                                                                    EditColumn-ItemStyle-HorizontalAlign="Left">
                                                                    <FormTemplate>
                                                                        <table cellpadding="0" cellspacing="2" border="0" width="850">
                                                                            <tr>
                                                                                <td style="font-weight: bold; padding-top: 10px;" colspan="6">
                                                                                    Attachment Detail
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="caption" style="width: 125px;">
                                                                                    Title
                                                                                </td>
                                                                                <td class="details" style="width: 175px;">
                                                                                    <asp:TextBox ID="Title" runat="server" Text='<%# Bind("title") %>'></asp:TextBox>
                                                                                </td>
                                                                                <td class="caption" style="width: 125px;">
                                                                                    Attachment
                                                                                </td>
                                                                                <td class="details" style="width: 175px;">
                                                                                    <asp:FileUpload ID="fl_others_upload_1" runat="server" />
                                                                                </td>
                                                                                <td valign="middle" style="width: 100px;">
                                                                                    <asp:ImageButton ID="img_btn_save_attach" runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif", "/images/update.gif") %>'
                                                                                        CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "PerformInsert", "Update")%>' />
                                                                                </td>
                                                                                <td valign="middle" style="width: 100px;">
                                                                                    <asp:ImageButton ID="img_btn_cancel" runat="server" ImageUrl="/images/cancel.gif"
                                                                                        CausesValidation="false" CommandName="cancel" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </FormTemplate>
                                                                </EditFormSettings>
                                                            </MasterTableView>
                                                            <ClientSettings>
                                                                <Selecting AllowRowSelect="True"></Selecting>
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                        <%--<telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditor1" runat="server" TextBoxStyle-Width="136px" />--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </telerik:RadAjaxPanel>
                                </asp:Panel>
                            </td>
                            <td class="fixedcolumn" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTabItem">
                                <asp:Panel ID="Panel6_Header" runat="server" CssClass="pnlTabItemHeader">
                                    <div class="tabItemHeader" style="background: url(/Images/after_lounch.gif) no-repeat;
                                        background-position: 10px 0px;">
                                        Auctions
                                    </div>
                                    <asp:Image ID="Image6" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                        CssClass="panelimage" />
                                </asp:Panel>
                                <ajax:CollapsiblePanelExtender ID="cpe6" BehaviorID="cpe6" runat="server" Enabled="True"
                                    ImageControlID="Image6" TargetControlID="Panel6_Content" CollapseControlID="Panel6_Header"
                                    ExpandControlID="Panel6_Header" Collapsed="True" CollapsedImage="~/Images/down_Arrow.gif"
                                    ExpandedImage="~/Images/up_Arrow.gif">
                                </ajax:CollapsiblePanelExtender>
                                <asp:Panel ID="Panel6_Content" runat="server" CssClass="collapsePanel">
                                    <div class="TabGrid" style="padding: 10px;">
                                        <%--<div class="dv_md_heading">
                    <div style="float: left;">
                        &nbsp;Auctions</div>
                    <div style="float: right;">
                        <img src="/Images/Q.mark.png" alt="help?" />&nbsp;
                    </div>
                </div>--%>
                                        <telerik:RadGrid ID="RadGrid_Auction" DataSourceID="RadGrid_Auction_SqlDataSource"
                                            AllowFilteringByColumn="False" runat="server" GridLines="None" AllowPaging="true"
                                            ShowHeader="false" AllowSorting="False" OnPreRender="RadGrid_Auction_PreRender"
                                            Skin="Vista" PageSize="12">
                                            <MasterTableView TableLayout="Fixed" DataKeyNames="auction_id">
                                                <NoRecordsTemplate>
                                                    No Auction to display
                                                </NoRecordsTemplate>
                                                <ItemTemplate>
                                                    <%#IIf(Not CType(Container, GridItem).ItemIndex = 0, "</td></tr></table>", "")%>
                                                    <asp:Panel ID="ItemContainer" CssClass='<%# IIf(CType(Container, GridItem).ItemType = GridItemType.Item, "cmp_auc_item", "cmp_auc_alternatingItem") %>'
                                                        runat="server">
                                                        <div style="height: 77px; display: block;">
                                                            <b>Auction Code</b>
                                                            <%# Eval("code")%>
                                                            <br />
                                                            <b>Title</b>
                                                            <%# Eval("title")%>
                                                            <br />
                                                            <b>Status</b>
                                                            <%# Eval("Status")%>
                                                        </div>
                                                        <div style="text-align: right;">
                                                            <a href="javascript:void(0);" onclick="redirectIframe('/Backend_TopBar.aspx?t=<%# CommonCode.getAuctionTabPosition(Eval("auction_id")) %>&a=<%# Eval("auction_id") %>','/Backend_Leftbar.aspx?t=3&a=<%# Eval("auction_id") %>','/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %>')">
                                                                <img alt="More Info" src="/images/edit_icon.png" style="border: none;" /></a>
                                                        </div>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                        <asp:SqlDataSource ID="RadGrid_Auction_SqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                            SelectCommand="SELECT A1.auction_id, code, title, A2.name, dbo.auction_status(a1.auction_id) as Status FROM tbl_auctions A1 WITH (NOLOCK) inner join tbl_auction_categories A2 on A1.auction_category_id=A2.auction_category_id where A1.is_active=1 and A1.seller_id=@seller_id">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="hd_seller_id" Type="Int32" PropertyName="Value"
                                                    Name="seller_id" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </asp:Panel>
                            </td>
                            <td class="fixedcolumn" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTabItem">
                                <asp:Panel ID="Panel7_Header" runat="server" CssClass="pnlTabItemHeader">
                                    <div class="tabItemHeader" style="background: url(/Images/comments_icon.gif) no-repeat;
                                        background-position: 10px 0px;">
                                        Comments
                                    </div>
                                    <asp:Image ID="Image7" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                        CssClass="panelimage" />
                                </asp:Panel>
                                <ajax:CollapsiblePanelExtender ID="cpe7" BehaviorID="cpe7" runat="server" Enabled="True"
                                    ImageControlID="Image7" TargetControlID="Panel7_Content" CollapseControlID="Panel7_Header"
                                    ExpandControlID="Panel7_Header" Collapsed="True" CollapsedImage="~/Images/down_Arrow.gif"
                                    ExpandedImage="~/Images/up_Arrow.gif">
                                </ajax:CollapsiblePanelExtender>
                                <ajax:CollapsiblePanelExtender ID="cpe77" BehaviorID="cpe7" runat="server" Enabled="True"
                                    ImageControlID="Image7" TargetControlID="Panel7_Content7button" CollapseControlID="Panel7_Header"
                                    ExpandControlID="Panel7_Header" Collapsed="True" CollapsedImage="~/Images/down_Arrow.gif"
                                    ExpandedImage="~/Images/up_Arrow.gif">
                                </ajax:CollapsiblePanelExtender>
                                <asp:Panel ID="Panel7_Content" runat="server" CssClass="collapsePanel">
                                    <div class="TabGrid" style="padding: 10px;">
                                        <%--<div class="dv_md_heading">
                    <div style="float: left;">
                        &nbsp;Comments</div>
                    <div style="float: right;">
                        <img src="/Images/Q.mark.png" alt="help?" />&nbsp;
                    </div>
                </div>--%>
                                        <table cellpadding="0" cellspacing="2" border="0" width="850">
                                            <tr>
                                                <td class="caption" style="width: 140px;">
                                                    Comments&nbsp;<span id="span_comment" runat="server" class="req_star">*</span>
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_comment" runat="server" TextMode="MultiLine" CssClass="TextBox"
                                                        Height="140px" Width="600px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Required_txt_comment" runat="server" Font-Size="10px"
                                                        ControlToValidate="txt_comment" ValidationGroup="_comment" Display="Dynamic"
                                                        ErrorMessage="<br>Comment Required"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                        <telerik:RadGrid ID="RadGrid_Comment" DataSourceID="RadGrid_Comment_SqlDataSource"
                                            runat="server" AllowAutomaticDeletes="True" AllowPaging="True" PageSize="10"
                                            AutoGenerateColumns="false" Skin="Vista" AllowSorting="true">
                                            <PagerStyle Mode="NextPrevAndNumeric" />
                                            <MasterTableView TableLayout="Fixed" DataKeyNames="seller_comment_id" DataSourceID="RadGrid_Comment_SqlDataSource"
                                                AutoGenerateColumns="False" SkinID="Vista" AllowFilteringByColumn="true" ItemStyle-Height="40"
                                                AlternatingItemStyle-Height="40">
                                                <NoRecordsTemplate>
                                                    No comment to display
                                                </NoRecordsTemplate>
                                                <Columns>
                                                    <telerik:GridTemplateColumn HeaderText="User" FilterListOptions="VaryByDataTypeAllowCustom"
                                                        SortExpression="name" UniqueName="name" DataField="name" FilterControlWidth="150"
                                                        ItemStyle-Width="200px">
                                                        <ItemTemplate>
                                                            <div style="font-weight: bold;">
                                                                <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/users/user_mouse_over.aspx?i=<%# Eval("user_id") %>','270','white','true');">
                                                                    <%# Eval("name")%></a></div>
                                                            <div style="color: Gray;">
                                                                <%# Eval("submit_date")%></div>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridBoundColumn DataField="comment" HeaderText="Comment" FilterControlWidth="220"
                                                        SortExpression="comment" UniqueName="comment" />
                                                    <%-- <telerik:GridDateTimeColumn DataField="submit_date" FilterListOptions="VaryByDataType"
                                                        DataType="System.DateTime" HeaderText="Date" FilterControlWidth="120" HeaderStyle-Width="180"
                                                        SortExpression="submit_date" UniqueName="submit_date" />--%>
                                                    <telerik:GridButtonColumn ConfirmText="Delete this comment?" ConfirmDialogType="RadWindow"
                                                        HeaderStyle-Width="35px" ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete"
                                                        Text="Delete" UniqueName="DeleteColumn" ImageUrl="/Images/delete_grid.gif">
                                                        <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                    </telerik:GridButtonColumn>
                                                </Columns>
                                            </MasterTableView>
                                            <ClientSettings AllowDragToGroup="false">
                                                <Selecting AllowRowSelect="True"></Selecting>
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                        <asp:SqlDataSource ID="RadGrid_Comment_SqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                            ProviderName="System.Data.SqlClient" SelectCommand="SELECT A1.seller_comment_id, comment, (A2.first_name+' '+A2.last_name) as name, CONVERT(varchar(10), A1.submit_date, 101) as submit_date,A2.user_id FROM tbl_reg_seller_comments A1 inner join tbl_sec_users A2 on A1.submit_by_user_id=A2.user_id where A1.seller_id =@seller_id"
                                            DeleteCommand="delete from tbl_reg_seller_comments where seller_comment_id=@seller_comment_id">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="hd_seller_id" PropertyName="Value" Type="Int32"
                                                    Name="seller_id" />
                                            </SelectParameters>
                                            <DeleteParameters>
                                                <asp:Parameter Name="seller_comment_id" Type="Int32" />
                                            </DeleteParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </asp:Panel>
                            </td>
                            <td class="fixedcolumn" valign="top">
                                <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                <asp:Panel ID="Panel7_Content7button" runat="server" CssClass="collapsePanel">
                                    <div class="addButton">
                                        <asp:ImageButton runat="server" ID="but_comment_submit" ValidationGroup="_comment"
                                            AlternateText="Submit" ImageUrl="/images/save.gif" />
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
