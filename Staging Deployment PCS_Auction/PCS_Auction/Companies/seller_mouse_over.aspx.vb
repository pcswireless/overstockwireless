﻿
Partial Class Companies_seller_mouse_over
    Inherits System.Web.UI.Page

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        'dv_load.Visible = True
    End Sub

    
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.QueryString.Get("i") <> Nothing AndAlso Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDataTable("SELECT A.seller_id,isnull(A.company_name,'') as company_name,isnull(logo1,'') as logo1, isnull(A.email,'') as email,isnull(A.city,'') as city,isnull(A.address1,'') as address1,isnull(A.address2,'') as address2,ISNULL(A.zip,'') As zip,isnull(A.website,'') as website,ISNULL(A.fax,'') AS fax, ISNULL(A.phone,'') As phone,ISNULL(A.mobile,'') AS mobile, A.zip,isnull(S.name,'') as state,ISNULL(C.code,'') As country FROM tbl_reg_sellers A left join tbl_master_states S on A.state_id=S.state_id  left join tbl_master_countries C ON A.country_id=C.country_id where A.seller_id=" & Request.QueryString.Get("i") & "")
            If dt.Rows.Count > 0 Then
                Dim strPhone As String = ""
                If dt.Rows(0)("phone") <> "" Then
                    strPhone = "<br /><br />(P) " & dt.Rows(0)("phone")
                End If
                If dt.Rows(0)("mobile") <> "" Then
                    If strPhone <> "" Then
                        strPhone = strPhone & ", " & dt.Rows(0)("mobile")
                    Else
                        strPhone = "<br /><br />(P) " & dt.Rows(0)("mobile")
                    End If
                End If
                If dt.Rows(0)("fax") <> "" Then
                    strPhone = strPhone & "<br />(F) " & dt.Rows(0)("fax")
                End If
                If dt.Rows(0)("email") <> "" Then
                    strPhone = strPhone & "<br />(E) " & dt.Rows(0)("email")
                End If

                img_image.ImageUrl = IIf(dt.Rows(0)("logo1").ToString = "", "/images/imagenotavailable.gif", "/Upload/Companies/" & dt.Rows(0)("seller_id") & "/" & dt.Rows(0)("logo1"))
                lbl_company.Text = "<b>" & dt.Rows(0)("company_name") & "</b>" & "<br>" & dt.Rows(0)("address1") & IIf(dt.Rows(0)("address2") <> "", "<br>" & dt.Rows(0)("address2"), "") & "<br>" & dt.Rows(0)("city") & "<br>" & dt.Rows(0)("state") & " - " & dt.Rows(0)("zip") & "<br />" & dt.Rows(0)("country") & strPhone
            Else
                img_image.Visible = False
                lbl_company.Text = "No record for this company."
            End If
        Else
            img_image.Visible = False
            lbl_company.Text = "No record for this company."
        End If
        'dv_load.Visible = False

    End Sub
End Class
