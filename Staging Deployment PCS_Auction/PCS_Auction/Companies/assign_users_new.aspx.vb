﻿Imports Telerik.Web.UI

Partial Class Companies_assign_users_new
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If IsNumeric(Request.QueryString.Get("i")) Then
                hd_seller_id.Value = Request.QueryString.Get("i")
            Else
                hd_seller_id.Value = 0
            End If
        End If
    End Sub

    Protected Sub RadGrid_UserAssign_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid_UserAssign.NeedDataSource
        RadGrid_UserAssign.DataSource = SqlHelper.ExecuteDataTable("SELECT  B1.user_id, (isnull(B1.first_name ,'') + ' ' + isnull( B1.last_name,'')) as name,B1.mobile as phone1,B1.email FROM  tbl_sec_users B1 where B1.is_active=1 and B1.user_id not in (select B2.user_id from tbl_reg_seller_user_mapping B2 where B2.seller_id=" & hd_seller_id.Value & ")")
    End Sub

    Protected Sub but_save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_save.Click
        If hd_seller_id.Value > 0 Then
            For i = 0 To Me.RadGrid_UserAssign.Items.Count - 1
                If CType(RadGrid_UserAssign.Items(i).FindControl("chk1"), CheckBox).Checked = True Then
                    SqlHelper.ExecuteNonQuery("insert into tbl_reg_seller_user_mapping (seller_id,user_id) values (" & hd_seller_id.Value & "," & RadGrid_UserAssign.Items(i).GetDataKeyValue("user_id") & ")")
                End If
            Next
            CommonCode.insert_system_log("User Added", "but_save_Click", Request.QueryString.Get("i"), "", "Seller")

            Response.Write("<script>window.opener.popup_PostBackFunction();window.close();</script>")
        End If
    End Sub

    Protected Sub CheckAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Integer
        If sender.checked Then
            For i = 0 To Me.RadGrid_UserAssign.Items.Count - 1
                CType(RadGrid_UserAssign.Items(i).FindControl("chk1"), CheckBox).Checked = True
            Next
        Else
            For i = 0 To Me.RadGrid_UserAssign.Items.Count - 1
                CType(RadGrid_UserAssign.Items(i).FindControl("chk1"), CheckBox).Checked = False
            Next
        End If
    End Sub

   
    Protected Sub RadGrid_UserAssign_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid_UserAssign.PreRender
        RadGrid_UserAssign.GroupingSettings.CaseSensitive = False
        Dim menu As GridFilterMenu = RadGrid_UserAssign.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub
End Class
