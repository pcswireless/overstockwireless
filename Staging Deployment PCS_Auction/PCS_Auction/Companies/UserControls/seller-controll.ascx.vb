﻿
Partial Class Companies_user_controls_seller_controll
    Inherits System.Web.UI.UserControl
    Dim obj_class As New Seller
    Protected Sub but_basic_save_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles but_basic_save.Click
        obj_class.insert_tbl_reg_sellers(txt_basic_company_name.Text.Trim, txt_basic_contact.Text.Trim, txt_basic_email.Text.Trim, txt_basic_website.Text.Trim, txt_basic_phone.Text.Trim, txt_basic_fax.Text.Trim, txt_basic_address_1.Text.Trim, txt_basic_address_2.Text.Trim, txt_basic_city.Text.Trim, dd_basic_state.SelectedValue, txt_basic_zip.Text, dd_basic_country.SelectedValue, txt_basic_vat.Text.Trim, txt_basic_ssn.Text.Trim, txt_basic_timezone.Text.Trim, IIf(chk_basic_approve_bidder.Checked, True, False), fl_img_upload_1.PostedFile.FileName, fl_img_upload_2.PostedFile.FileName, fl_img_upload_3.PostedFile.FileName, IIf(chk_basic_active.Checked, True, False), Today.Now, 1, 0)
    End Sub
End Class
