﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="seller-controll.ascx.vb"
    Inherits="Companies_user_controls_seller_controll" %>
<div class="dv_md_heading">
    Company Profile</div>
<div class="dv_md_heading_help">
    <img alt="help?" src="/images/Q.mark.gif" />
</div>
<div class="dv_md_heading_corner">
</div>
<table class="right_content_tb">
    <tr>
        <td colspan="2" class="dv_md_sub_heading">
            Contact Information
        </td>
    </tr>
    <tr>
        <td>
            Company Name:<br />
            <asp:TextBox ID="txt_basic_company_name" CssClass="txt_box" runat="server"></asp:TextBox>
        </td>
        <td>
            Contact Title:<br />
            <asp:TextBox ID="txt_basic_contact" CssClass="txt_box" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Email:<br />
            <asp:TextBox ID="txt_basic_email" CssClass="txt_box" runat="server"></asp:TextBox>
        </td>
        <td>
            Confirm Email:<br />
            <asp:TextBox ID="txt_basic_confirm_email" CssClass="txt_box" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Website:<br />
            <asp:TextBox ID="txt_basic_website" CssClass="txt_box" runat="server"></asp:TextBox>
        </td>
        <td>
            Phone:<br />
            <asp:TextBox ID="txt_basic_phone" CssClass="txt_box" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Fax:<br />
            <asp:TextBox ID="txt_basic_fax" CssClass="txt_box" runat="server"></asp:TextBox>
        </td>
        <td>
            Address 1:<br />
            <asp:TextBox ID="txt_basic_address_1" CssClass="txt_box" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Address 2:<br />
            <asp:TextBox ID="txt_basic_address_2" CssClass="txt_box" runat="server"></asp:TextBox>
        </td>
        <td>
            Zip:<br />
            <asp:TextBox ID="txt_basic_zip" CssClass="txt_box" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            City:<br />
            <asp:TextBox ID="txt_basic_city" CssClass="txt_box" runat="server"></asp:TextBox>
        </td>
        <td>
            State:<br />
            <asp:DropDownList CssClass="drop_down" ID="dd_basic_state" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            Country:<br />
            <asp:DropDownList ID="dd_basic_country" CssClass="drop_down" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="dv_md_sub_heading">
            Other Information
        </td>
    </tr>
    <tr>
        <td>
            VAT#:<br />
            <asp:TextBox ID="txt_basic_vat" CssClass="txt_box" runat="server"></asp:TextBox>
        </td>
        <td>
            Tax ID/SSN#:<br />
            <asp:TextBox ID="txt_basic_ssn" CssClass="txt_box" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Time Zone:<br />
            <asp:TextBox ID="txt_basic_timezone" CssClass="txt_box" runat="server"></asp:TextBox>
        </td>
        <td>
            Auto Approve Bidders:<br />
            <asp:CheckBox ID="chk_basic_approve_bidder" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td>
                        <div class="dv_seller_basic_logo">
                            Logo</div>
                        <div class="dv_seller_basic_up">
                            <div>
                                Img 1
                                <asp:FileUpload ID="fl_img_upload_1" runat="server" /></div>
                            <div>
                                Img 2
                                <asp:FileUpload ID="fl_img_upload_2" runat="server" /></div>
                            <div>
                                Img 3
                                <asp:FileUpload ID="fl_img_upload_3" runat="server" /></div>
                        </div>
                        <div class="dv_seller_basic_chk">
                            Is Active
                            <asp:CheckBox ID="chk_basic_active" runat="server" />
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div>
    <table>
        <tr>
            <td>
                <asp:ImageButton ID="but_basic_save" ValidationGroup="_basic" runat="server" AlternateText="Save"
                    ImageUrl="/images/save.gif" />
                <asp:ImageButton ID="but_basic_update" ValidationGroup="_basic" runat="server" AlternateText="Update"
                    ImageUrl="/images/update.gif" />
            </td>
            <td>
                <asp:ImageButton ID="but_basic_cancel" ValidationGroup="_basic" runat="server" AlternateText="Cancel"
                    ImageUrl="/images/cancel.gif" />
            </td>
        </tr>
    </table>
</div>
