﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="seller-listing.aspx.vb" Inherits="Companies_seller_listing" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                     args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
                else {
                    args.set_enableAjax(true);
                }
            }

        </script>
    </telerik:RadScriptBlock>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Company Listing</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; width: 40%; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    Please select the company name to edit from below
                </div>
                <div style="float: right; width: 40%; text-align: right; margin-bottom: 3px;" id="div_new_auction"
                    runat="server">
                    <a style="text-decoration: none" href="/Companies/seller-details.aspx">
                        <img src="/images/add_new_company.gif" style="border: none" alt="Add New Company" /></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                    <ClientEvents OnRequestStart="Cancel_Ajax" />
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="RadGrid_Seller">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid_Seller" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid_Seller" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
                    left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
                    <img id="Image8" src="/images/img_loading.gif" />
                </telerik:RadAjaxLoadingPanel>
                <telerik:RadAjaxPanel ID="RadAjaxPanel_Basic" runat="server" CssClass="TabGrid" LoadingPanelID="RadAjaxLoadingPanel1">
                    <telerik:RadGrid ID="RadGrid_Seller" runat="server" AllowFilteringByColumn="True"
                        AutoGenerateColumns="false" AllowSorting="True" PageSize="10" ShowFooter="False"
                        ShowGroupPanel="True" AllowPaging="True" Skin="Vista" EnableLinqExpressions="false">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <ExportSettings IgnorePaging="true" FileName="Company_Export" Pdf-AllowAdd="true"
                            Pdf-AllowCopy="true" Pdf-AllowPrinting="true" Pdf-PaperSize="A4" Pdf-Creator="a1"
                            OpenInNewWindow="true" ExportOnlyData="true" />
                        <MasterTableView DataKeyNames="seller_id" AutoGenerateColumns="false" AllowFilteringByColumn="True"
                            ItemStyle-Height="40" AlternatingItemStyle-Height="40" CommandItemDisplay="Top">
                            <CommandItemStyle BackColor="#d6d6d6" />
                            <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false"
                                ShowExportToPdfButton="false" ShowExportToCsvButton="false" ShowAddNewRecordButton="false"
                                ShowRefreshButton="false" />
                            <NoRecordsTemplate>
                                No company to display
                            </NoRecordsTemplate>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="company_name" SortOrder="Ascending" />
                            </SortExpressions>
                            <Columns>
                                <%--<telerik:GridTemplateColumn HeaderText="Company Name" SortExpression="company_name" UniqueName="company_name" FilterControlWidth="50">
                   
                    <ItemTemplate>
                     <asp:LinkButton ID="grd_lnk_select" runat="server" Text='<%#Eval("company_name") %>' CommandName="detail" CommandArgument='<%#Eval("seller_id") %>'></asp:LinkButton>
                    </ItemTemplate>
                    </telerik:GridTemplateColumn>--%>
                                <telerik:GridTemplateColumn HeaderText="Company Name" SortExpression="company_name"
                                    UniqueName="company_name" DataField="company_name" FilterControlWidth="150" GroupByExpression="company_name [Company Name] Group By company_name">
                                    <ItemTemplate>
                                        <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('seller_mouse_over.aspx?i=<%# Eval("seller_id") %>','250','white','true');"
                                            onclick="redirectIframe('','','/companies/seller-details.aspx?i=<%# Eval("seller_id") %>');">
                                            <%# Eval("company_name")%></a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Contact Person" SortExpression="contact_name"
                                    UniqueName="contact_name" DataField="contact_name" FilterControlWidth="160" GroupByExpression="contact_name [Contact Person] Group By contact_name">
                                    <ItemTemplate>
                                        <%# Eval("contact_name")%>
                                        <%#IIf(Eval("contact_title") <> "", "<br>" & Eval("contact_title"), "")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Email Address" SortExpression="email" UniqueName="email"
                                    DataField="email" FilterControlWidth="190" GroupByExpression="email [Email Address] Group By email">
                                    <ItemTemplate>
                                        <a href="mailto:<%# Eval("email")%>">
                                            <%# Eval("email")%></a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="phone" HeaderText="Phone" SortExpression="phone"
                                    UniqueName="phone" FilterControlWidth="100">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="is_active" DataType="System.Int32" DataField="is_active"
                                    SortExpression="is_active" HeaderText="Is Active" GroupByExpression="is_active [Is Active] Group By is_active">
                                    <ItemTemplate>
                                        <img src='/Images/<%#IIf(Convert.ToBoolean(Eval("is_active")),"true.gif","false.gif")%>'
                                            alt="" border="none" />
                                    </ItemTemplate>
                                    <FilterTemplate>
                                        <telerik:RadComboBox ID="RadComboBo_Chk_is_active" runat="server" OnClientSelectedIndexChanged="ActiveIndexChanged"
                                            Width="120px" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("is_active").CurrentFilterValue %>'>
                                            <Items>
                                                <telerik:RadComboBoxItem Text="All" Value="" />
                                                <telerik:RadComboBoxItem Text="Active" Value="1" />
                                                <telerik:RadComboBoxItem Text="In-Active" Value="0" />
                                            </Items>
                                        </telerik:RadComboBox>
                                        <telerik:RadScriptBlock ID="RadScriptBlock_is_active" runat="server">
                                            <script type="text/javascript">
                                                function ActiveIndexChanged(sender, args) {
                                                    var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                    tableView.filter("is_active", args.get_item().get_value(), "EqualTo");

                                                }
                                            </script>
                                        </telerik:RadScriptBlock>
                                    </FilterTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false"
                                    ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <a href="javascript:void(0);" onclick="redirectIframe('','','/companies/seller-details.aspx?i=<%# Eval("seller_id") %>')">
                                            <img src="/images/edit.png" style="border: none;" alt="Edit" /></a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings AllowDragToGroup="true">
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                        <GroupingSettings ShowUnGroupButton="true" />
                    </telerik:RadGrid>
                </telerik:RadAjaxPanel>
                <%-- <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
        ProviderName="System.Data.SqlClient" SelectCommand="select [seller_id],[company_name] ,(contact_first_name+' '+contact_last_name) as contact_name,email ,name as state,is_active from [tbl_reg_sellers] inner join tbl_master_states on tbl_master_states.state_id=tbl_reg_sellers.state_id">
        
    </asp:SqlDataSource>--%>
            </td>
        </tr>
    </table>
</asp:Content>
