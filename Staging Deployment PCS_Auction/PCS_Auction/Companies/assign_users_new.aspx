﻿<%@ Page Title="PCS Bidding" Language="VB" MasterPageFile="~/BackendPopUP.master" AutoEventWireup="false"
    CodeFile="assign_users_new.aspx.vb" Inherits="Companies_assign_users_new" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" />
    </telerik:RadAjaxLoadingPanel>
    <center>
        <div class="pageheading">
            Select Users</div>
    </center>
    <asp:HiddenField ID="hd_seller_id" runat="server" Value="0" />
    <telerik:RadAjaxPanel ID="AjaxPanel_User" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
        CssClass="TabGrid">
        <telerik:RadGrid ID="RadGrid_UserAssign" runat="server" AllowAutomaticDeletes="False"
            PageSize="10" AllowAutomaticUpdates="False" AllowPaging="False" AutoGenerateColumns="False"
            AllowSorting="true" Skin="Vista" AllowFilteringByColumn="true" ShowGroupPanel="true">
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView DataKeyNames="user_id" AutoGenerateColumns="False" ItemStyle-Height="40"
                AlternatingItemStyle-Height="40">
                <NoRecordsTemplate>
                    Users list not available
                </NoRecordsTemplate>
                <SortExpressions>
                    <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                </SortExpressions>
                <Columns>
                    <telerik:GridTemplateColumn HeaderText="Name" SortExpression="name" UniqueName="name" GroupByExpression="name [Name] group by name"
                        DataField="name" FilterControlWidth="150">
                        <ItemTemplate>
                            <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/users/user_mouse_over.aspx?i=<%# Eval("user_id") %>','270','white','true');" >
                                <%# Eval("name")%>
                            </a>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Email Address" SortExpression="email" UniqueName="email" GroupByExpression="email [Email Address] group by email" 
                        DataField="email" FilterControlWidth="190">
                        <ItemTemplate>
                            <a href="mailto:<%# Eval("email")%>">
                                <%# Eval("email")%></a>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="phone1" HeaderText="Phone" GroupByExpression="phone1 [Phone] group by phone1" 
                        SortExpression="phone1" UniqueName="phone1">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn AllowFiltering="false" ItemStyle-Width="90">
                        <HeaderTemplate>
                            Select All
                            <asp:CheckBox ID="chkHeader" runat="server" AutoPostBack="true" OnCheckedChanged="CheckAll" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chk1" runat="server" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings AllowDragToGroup="true">
                <Selecting AllowRowSelect="True"></Selecting>
            </ClientSettings>
            <GroupingSettings ShowUnGroupButton="true" />
        </telerik:RadGrid>
    </telerik:RadAjaxPanel>
    <center>
        <div style="clear: both;">
            <br />
            <asp:ImageButton ID="but_save" AlternateText="Save Selection" ImageUrl="/images/saveselection.gif"
                runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="ImageButton1" OnClientClick="javascript:window.close();" runat="server"
                AlternateText="Close" ImageUrl="/images/close.gif" />
        </div>
    </center>
</asp:Content>
