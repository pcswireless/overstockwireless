﻿Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Linq

Partial Class Companies_assign_bidders
    Inherits System.Web.UI.Page
    Shared dv_list As List(Of Child)
    Shared or_list As List(Of Parent)

    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

       
        If Not Page.IsPostBack Then
            ' Response.Write("<script>alert('IsPostBack')</script>")
            Response.ExpiresAbsolute = DateTime.Now.AddDays(-1D)
            If IsNumeric(Request.QueryString.Get("i")) Then
                hd_seller_id.Value = Request.QueryString.Get("i")
            Else
                hd_seller_id.Value = 0
            End If
        End If
    End Sub


    'Protected Sub UseDragColumnCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    grdParent.MasterTableView.GetColumn("DragDropColumn").Visible = UseDragColumnCheckBox.Checked
    '    grdParent.Rebind()

    '    grdChild.MasterTableView.GetColumn("DragDropColumn").Visible = UseDragColumnCheckBox.Checked
    '    grdChild.Rebind()
    'End Sub


    Protected Property PendingParents_VB() As IList(Of Parent)
        Get
            Return DirectCast(ViewState("_ParentView_State"), IList(Of Parent))
        End Get
        Set(value As IList(Of Parent))
            ViewState("_ParentView_State") = value
        End Set
    End Property



    Protected Property ShippedParents_VB() As IList(Of Child)
        Get
            Return DirectCast(ViewState("_ChildView_State"), IList(Of Child))
        End Get
        Set(value As IList(Of Child))
            ViewState("_ChildView_State") = value
        End Set
    End Property

    'Protected Property ParentStore_DT() As DataTable
    '    Get
    '        Try
    '            'Response.Write("<script>alert('" & PendingParents_VB.Rows.Count.ToString & "')</script>")
    '            Dim obj As Object = PendingParents_VB()

    '            If obj Is Nothing Then
    '                obj = GetParents()
    '                PendingParents_VB = obj
    '            End If
    '            Return DirectCast(obj, DataTable)
    '        Catch ex As Exception
    '            PendingParents_VB = Nothing
    '        End Try
    '        Return New List(Of Parent)
    '    End Get
    '    Set(ByVal value As IList(Of Parent))
    '        PendingParents_VB = value
    '    End Set
    'End Property
    Protected Property ParentStore() As IList(Of Parent)
        Get
            Try
                'Response.Write("<script>alert('" & PendingParents_VB.Rows.Count.ToString & "')</script>")
                'Dim obj As Object = Session("PendingParents_VB") 'PendingParents_VB()
                Dim obj As New Object
                If or_list IsNot Nothing Then
                    obj = or_list ' (From li In or_list Where li.IsVisible = True Select li).ToList() ').FirstOrDefault, IList(Of Parent))
                Else
                    obj = Nothing
                End If

                If obj Is Nothing Then
                    obj = GetParents()
                    or_list = obj
                    'PendingParents_VB = obj
                End If
                Return obj ' DirectCast(obj, IList(Of Parent))
            Catch ex As Exception
                or_list = Nothing
                ' PendingParents_VB = Nothing
            End Try
            Return New List(Of Parent)
        End Get
        Set(ByVal value As IList(Of Parent))
            or_list = value ' PendingParents_VB = value
        End Set
    End Property

    Protected Property ChildStore() As IList(Of Child)
        Get
            Try
                'Dim it=From li In dv_list Where li.
                Dim obj As New Object
                If dv_list IsNot Nothing Then
                    obj = dv_list ' (From li In dv_list Where li.IsVisible = True Select li).ToList()
                Else
                    obj = Nothing
                End If

                If obj Is Nothing Then
                    obj = GetChild()
                    dv_list = obj
                    ' ShippedParents_VB = obj
                End If
                Return obj
            Catch ex As Exception
                dv_list = Nothing
                ' ShippedParents_VB = Nothing
            End Try
            Return New List(Of Child)
        End Get
        Set(ByVal value As IList(Of Child))
            dv_list = value '  ShippedParents_VB = value
        End Set
    End Property

    Protected Sub grdParent_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        grdParent.DataSource = (From li In ParentStore Where li.IsVisible = True Select li).ToList
    End Sub

    Protected Function GetParents() As IList(Of Parent)
        Dim results As IList(Of Parent) = New List(Of Parent)()
        Using connection As SqlConnection = New SqlConnection(SqlHelper.of_getConnectString) ' DbProviderFactories.GetFactory("System.Data.SqlClient").CreateConnection()
            'connection.ConnectionString = ConfigurationManager.ConnectionStrings("telerik").ConnectionString

            Using command As SqlCommand = connection.CreateCommand()
                command.CommandText = "SELECT S.buyer_id, S.company_name, isnull((S.contact_first_name+' '+S.contact_last_name),'') as contact, S.contact_title,S.email,Visible=1 FROM tbl_reg_buyers S where S.is_active=1 and S.buyer_id not in (select buyer_id from tbl_reg_buyer_seller_mapping where seller_id=" & hd_seller_id.Value & ") union SELECT S.buyer_id, S.company_name, isnull((S.contact_first_name+' '+S.contact_last_name),'') as contact, S.contact_title,S.email,Visible=0 FROM tbl_reg_buyers S where S.is_active=1 and S.buyer_id in (select buyer_id from tbl_reg_buyer_seller_mapping where seller_id=" & hd_seller_id.Value & ")"
                ' Response.Write(command.CommandText)
                connection.Open()
                Try
                    Dim reader As SqlDataReader = command.ExecuteReader()
                    While reader.Read()

                        Dim buyer_id As Integer = reader("buyer_id") ' DirectCast(reader.GetValue(reader.GetOrdinal("ParentID")), Integer)
                        Dim company_name As String = reader("company_name") 'IIf((Not reader.IsDBNull(reader.GetOrdinal("CustomerID"))), DirectCast(reader.GetValue(reader.GetOrdinal("CustomerID")), String), String.Empty)
                        Dim contact As String = reader("contact") ' IIf((Not reader.IsDBNull(reader.GetOrdinal("RequiredDate"))), DirectCast(reader.GetValue(reader.GetOrdinal("RequiredDate")), Date), Date.MinValue)
                        Dim contact_title As String = reader("contact_title") ' IIf((Not reader.IsDBNull(reader.GetOrdinal("CompanyName"))), DirectCast(reader.GetValue(reader.GetOrdinal("CompanyName")), String), String.Empty)
                        Dim email As String = reader("email")
                        Dim isvisible As Boolean = reader("Visible")
                        results.Add(New Parent(buyer_id, company_name, contact, email, contact_title, isvisible))

                    End While
                Catch ex As SqlException
                    'Response.Write(ex.Message)
                    'results.Clear()
                End Try
            End Using
        End Using
        Return results
    End Function


    Protected Function GetChild() As IList(Of Child)
        Dim results As IList(Of Child) = New List(Of Child)()
        'Using connection As SqlConnection = New SqlConnection(SqlHelper.of_getConnectString) ' DbProviderFactories.GetFactory("System.Data.SqlClient").CreateConnection()
        'connection.ConnectionString = ConfigurationManager.ConnectionStrings("telerik").ConnectionString

        ' Using command As SqlCommand = connection.CreateCommand()

        ' Command.CommandText = "SELECT S.buyer_id, S.company_name, isnull((S.contact_first_name+' '+S.contact_last_name),'') as contact, S.contact_title,S.email,Visible=1 FROM tbl_reg_buyers S inner join tbl_reg_buyer_seller_mapping B on S.buyer_id=B.buyer_id where seller_id=" & hd_seller_id.Value & ""

        '  connection.Open()
        Try
            'Dim reader As SqlDataReader = Command.ExecuteReader()
            ' While reader.Read()
            For Each reader As Parent In (From li In ParentStore Where li.IsVisible = False Select li).ToList
                Dim buyer_id As Integer = reader.Buyer_Id ' DirectCast(reader.GetValue(reader.GetOrdinal("ParentID")), Integer)
                Dim company_name As String = reader.Company_Name 'IIf((Not reader.IsDBNull(reader.GetOrdinal("CustomerID"))), DirectCast(reader.GetValue(reader.GetOrdinal("CustomerID")), String), String.Empty)
                Dim contact As String = reader.Contact ' IIf((Not reader.IsDBNull(reader.GetOrdinal("RequiredDate"))), DirectCast(reader.GetValue(reader.GetOrdinal("RequiredDate")), Date), Date.MinValue)
                Dim contact_title As String = reader.Contact_Title ' IIf((Not reader.IsDBNull(reader.GetOrdinal("CompanyName"))), DirectCast(reader.GetValue(reader.GetOrdinal("CompanyName")), String), String.Empty)
                Dim email As String = reader.Email
                Dim isvisible As Boolean = Not reader.IsVisible
                results.Add(New Child(buyer_id, company_name, contact, email, contact_title, isvisible))
            Next



            ' End While
        Catch ex As Exception
            ' Response.Write(ex.Message)
            results.Clear()
        End Try
        ' End Using
        ' End Using
        Return results
    End Function
    Protected Sub grdChild_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        grdChild.DataSource = (From li In ChildStore Where li.IsVisible = True).ToList
    End Sub

    Protected Sub grdParent_RowDrop(ByVal sender As Object, ByVal e As GridDragDropEventArgs)
        If String.IsNullOrEmpty(e.HtmlElement) Then
            If e.DraggedItems(0).OwnerGridID = grdParent.ClientID Then
                ' items are drag from pending to shipped grid
                If (e.DestDataItem Is Nothing AndAlso ChildStore.Count = 0) OrElse (e.DestDataItem IsNot Nothing AndAlso e.DestDataItem.OwnerGridID = grdChild.ClientID) Then
                    Dim ChildView As IList(Of Child)
                    Dim ParentView As IList(Of Parent)

                    ChildView = ChildStore
                    ParentView = ParentStore

                    Dim destinationIndex As Int32 = -1
                    If (e.DestDataItem IsNot Nothing) Then
                        'Dim child_obj As Child = GetChild(ChildView, DirectCast(e.DestDataItem.GetDataKeyValue("ParentId"), Integer))
                        'destinationIndex = IIf(child_obj IsNot Nothing, ChildView.IndexOf(child_obj), -1)
                    End If

                    For Each draggedItem As GridDataItem In e.DraggedItems
                        Dim child_obj As Child = GetChild(ChildView, DirectCast(draggedItem.GetDataKeyValue("buyer_id"), Integer))
                        destinationIndex = IIf(child_obj IsNot Nothing, ChildView.IndexOf(child_obj), -1)
                        Dim tmpParent As Parent = GetParent(ParentView, DirectCast(draggedItem.GetDataKeyValue("buyer_id"), Integer))

                        If tmpParent IsNot Nothing Then

                            If destinationIndex > -1 Then
                                If e.DropPosition = GridItemDropPosition.Below Then
                                    'destinationIndex += 1
                                End If
                                'ChildView.Insert(destinationIndex, New Child(orderId:=tmpParent.ParentID, customerId:=tmpParent.CustomerID, isactive:=1, customerName:=tmpParent.CustName, requiredDate:=tmpParent.RequiredDate))
                                ChildView.Item(destinationIndex).IsVisible = True ', New Child(orderId:=tmpParent.ParentID, customerId:=tmpParent.CustomerID, isactive:=1, customerName:=tmpParent.CustName, requiredDate:=tmpParent.RequiredDate))

                            Else
                                ChildView.Add(New Child(Buyer_Id:=tmpParent.Buyer_Id, company_name:=tmpParent.Company_Name, contact:=tmpParent.Contact, email:=tmpParent.Email, contact_title:=tmpParent.Contact_Title, isvisible:=True))
                            End If
                            ParentView(ParentView.IndexOf(tmpParent)).IsVisible = False
                        End If
                    Next

                    ChildStore = ChildView
                    ParentStore = ParentView
                    grdParent.Rebind()
                    grdChild.Rebind()
                ElseIf e.DestDataItem IsNot Nothing AndAlso e.DestDataItem.OwnerGridID = grdParent.ClientID Then

                    'reorder items in pending grid
                    'Dim ParentView As IList(Of Parent)
                    'ParentView = ParentStore

                    'Dim order As Parent = GetParent(ParentView, DirectCast(e.DestDataItem.GetDataKeyValue("ParentId"), Integer))
                    'Dim destinationIndex As Integer = ParentView.IndexOf(order)

                    'If ((e.DropPosition = GridItemDropPosition.Above) _
                    '        AndAlso (e.DestDataItem.ItemIndex > e.DraggedItems(0).ItemIndex)) Then
                    '    destinationIndex = (destinationIndex - 1)
                    'End If
                    'If ((e.DropPosition = GridItemDropPosition.Below) _
                    '        AndAlso (e.DestDataItem.ItemIndex < e.DraggedItems(0).ItemIndex)) Then
                    '    destinationIndex = (destinationIndex + 1)
                    'End If

                    'Dim ordersToMove As New List(Of Parent)()
                    'For Each draggedItem As GridDataItem In e.DraggedItems
                    '    Dim tmpParent As Parent = GetParent(ParentView, DirectCast(draggedItem.GetDataKeyValue("ParentId"), Integer))
                    '    If tmpParent IsNot Nothing Then
                    '        ordersToMove.Add(tmpParent)
                    '    End If
                    'Next

                    'For Each orderToMove As Parent In ordersToMove
                    '    ParentView.Remove(orderToMove)
                    '    ParentView.Insert(destinationIndex, orderToMove)
                    'Next
                    'ParentStore = ParentView
                    'grdParent.Rebind()

                    'Dim destinationItemIndex As Integer = destinationIndex - (grdParent.PageSize * grdParent.CurrentPageIndex)
                    'e.DestinationTableView.Items(destinationItemIndex).Selected = True
                End If
            End If
        End If
    End Sub

    Private Shared Function GetParent(ByVal ordersToSearchIn As IEnumerable(Of Parent), ByVal byer_id As Integer) As Parent
        For Each order As Parent In ordersToSearchIn
            If order.Buyer_Id = byer_id Then
                Return order
            End If
        Next
        Return Nothing
    End Function

    Private Shared Function GetChild(ByVal deliversToSearchIn As IEnumerable(Of Child), ByVal byer_id As Integer) As Child
        For Each child_obj As Child In deliversToSearchIn
            If child_obj.Buyer_Id = byer_id Then
                Return child_obj
            End If
        Next
        Return Nothing
    End Function

    Protected Sub grdChild_RowDrop(ByVal sender As Object, ByVal e As GridDragDropEventArgs)

        If String.IsNullOrEmpty(e.HtmlElement) Then
            If e.DraggedItems(0).OwnerGridID = grdChild.ClientID Then
                ' items are drag from pending to shipped grid
                '' -----------------Below line comment by AJAY-----------------------------------
                'If (e.DestDataItem Is Nothing AndAlso ParentStore.Count = 0) OrElse (e.DestDataItem IsNot Nothing AndAlso e.DestDataItem.OwnerGridID = grdParent.ClientID) Then
                Dim ChildView As IList(Of Child)
                Dim ParentView As IList(Of Parent)

                ChildView = ChildStore
                ParentView = ParentStore

                Dim destinationIndex As Int32 = -1
                Dim destinationIndexneew As Int32 = 0
                If (e.DestDataItem IsNot Nothing) Then
                    ' Dim order As Parent = GetParent(ParentView, DirectCast(e.DestDataItem.GetDataKeyValue("buyer_id"), Integer))
                    'destinationIndex = IIf(order IsNot Nothing, ParentView.IndexOf(order), -1)
                End If

                For Each draggedItem As GridDataItem In e.DraggedItems
                    Dim order As Parent = GetParent(ParentView, DirectCast(draggedItem.GetDataKeyValue("buyer_id"), Integer))
                    destinationIndex = IIf(order IsNot Nothing, ParentView.IndexOf(order), -1)
                    Dim child_obj As Child = GetChild(ChildView, DirectCast(draggedItem.GetDataKeyValue("buyer_id"), Integer))

                    If child_obj IsNot Nothing Then

                        If destinationIndex > -1 Then
                            If e.DropPosition = GridItemDropPosition.Below Then
                                ' destinationIndex += 1
                            End If
                            '' ParentView.Insert(destinationIndex, New Parent(orderId:=tmpParent.ParentID, customerId:=tmpParent.CustomerID, isactive:=1, customerName:=tmpParent.CustName, requiredDate:=tmpParent.RequiredDate))
                            ParentView.Item(destinationIndex).IsVisible = True
                            ' ChildView(ChildView.IndexOf(child_obj)).IsVisible = False
                            ChildView.Remove(child_obj)
                        Else
                            ' ParentView.Add(New Parent(orderId:=tmpParent.ParentID, customerId:=tmpParent.CustomerID, isactive:=1, customerName:=tmpParent.CustName, requiredDate:=tmpParent.RequiredDate))
                        End If
                        ' ChildView.Remove(tmpParent)
                    End If
                Next

                ChildStore = ChildView
                ParentStore = ParentView
                grdParent.Rebind()
                grdChild.Rebind()
            ElseIf e.DestDataItem IsNot Nothing AndAlso e.DestDataItem.OwnerGridID = grdChild.ClientID Then

                'reorder items in pending grid
                'Dim ChildView As IList(Of Child)
                'ChildView = ChildStore

                'Dim child_obj As Child = GetChild(ChildView, DirectCast(e.DestDataItem.GetDataKeyValue("ParentId"), Integer))
                'Dim destinationIndex As Integer = ChildView.IndexOf(child_obj)

                'If ((e.DropPosition = GridItemDropPosition.Above) _
                '        AndAlso (e.DestDataItem.ItemIndex > e.DraggedItems(0).ItemIndex)) Then
                '    destinationIndex = (destinationIndex - 1)
                'End If
                'If ((e.DropPosition = GridItemDropPosition.Below) _
                '        AndAlso (e.DestDataItem.ItemIndex < e.DraggedItems(0).ItemIndex)) Then
                '    destinationIndex = (destinationIndex + 1)
                'End If

                'Dim ordersToMove As New List(Of Child)()
                'For Each draggedItem As GridDataItem In e.DraggedItems
                '    Dim tmpParent As Child = GetChild(ChildView, DirectCast(draggedItem.GetDataKeyValue("ParentId"), Integer))
                '    If tmpParent IsNot Nothing Then
                '        ordersToMove.Add(tmpParent)
                '    End If
                'Next

                'For Each orderToMove As Child In ordersToMove
                '    ChildView.Remove(orderToMove)
                '    ChildView.Insert(destinationIndex, orderToMove)
                'Next
                'ChildStore = ChildView
                'grdChild.Rebind()

                'Dim destinationItemIndex As Integer = destinationIndex - (grdChild.PageSize * grdChild.CurrentPageIndex)
                'e.DestinationTableView.Items(destinationItemIndex).Selected = True
            End If
            'End If
        End If

        'If Not String.IsNullOrEmpty(e.HtmlElement) AndAlso e.HtmlElement = "trashCan" Then
        '    Dim ChildView As IList(Of Child)
        '    ChildView = ChildStore
        '    Dim deleted As Boolean = False

        '    For Each draggedItem As GridDataItem In e.DraggedItems
        '        Dim tmpParent As Child = GetChild(ChildView, DirectCast(draggedItem.GetDataKeyValue("buyer_id"), Integer))

        '        If tmpParent IsNot Nothing Then
        '            deleted = True
        '            ChildView.Remove(tmpParent)
        '        End If
        '    Next
        '    If (deleted) Then
        '        msg.Visible = True
        '    End If
        '    ChildStore = ChildView
        '    grdChild.Rebind()
        'End If
    End Sub

    ' (user_id, name, profile, email, isvisible)

    Public Class Parent
        Private _contact As String
        Private _company_name As String
        Private _buyer_id As Integer
        Private _email As String
        Private _contact_title As String
        Private _isvisible As Boolean


        Public Sub New(ByVal buyer_id As Integer, ByVal company_name As String, ByVal contact As String, ByVal email As String, ByVal contact_title As String, ByVal isvisible As Boolean)
            _buyer_id = buyer_id
            _contact = contact
            _company_name = company_name
            _email = email
            _contact_title = contact_title
            _isvisible = isvisible
        End Sub

        Public Property Buyer_Id() As Integer
            Get
                Return _buyer_id
            End Get
            Set(value As Integer)
                _buyer_id = value
            End Set
        End Property

        Public Property Contact() As String
            Get
                Return _contact
            End Get
            Set(value As String)
                _contact = value
            End Set
        End Property

        Public Property Company_Name() As String
            Get
                Return _company_name
            End Get
            Set(value As String)
                _company_name = value
            End Set
        End Property

        Public Property Contact_Title() As String
            Get
                Return _contact_title
            End Get
            Set(value As String)
                _contact_title = value
            End Set
        End Property

        Public Property Email() As String
            Get
                Return _email
            End Get
            Set(value As String)
                _email = value
            End Set
        End Property

        Public Property IsVisible() As Boolean
            Get
                Return _isvisible
            End Get
            Set(value As Boolean)
                _isvisible = value
            End Set
        End Property
    End Class

    Public Class Child

        Private _contact As String
        Private _company_name As String
        Private _buyer_id As Integer
        Private _email As String
        Private _contact_title As String
        Private _isvisible As Boolean


        Public Sub New(ByVal buyer_id As Integer, ByVal company_name As String, ByVal contact As String, ByVal email As String, ByVal contact_title As String, ByVal isvisible As Boolean)
            _buyer_id = buyer_id
            _contact = contact
            _company_name = company_name
            _email = email
            _contact_title = contact_title
            _isvisible = isvisible
        End Sub

        Public Property Buyer_Id() As Integer
            Get
                Return _buyer_id
            End Get
            Set(value As Integer)
                _buyer_id = value
            End Set
        End Property

        Public Property Contact() As String
            Get
                Return _contact
            End Get
            Set(value As String)
                _contact = value
            End Set
        End Property

        Public Property Company_Name() As String
            Get
                Return _company_name
            End Get
            Set(value As String)
                _company_name = value
            End Set
        End Property

        Public Property Email() As String
            Get
                Return _email
            End Get
            Set(value As String)
                _email = value
            End Set
        End Property

        Public Property Contact_Title() As String
            Get
                Return _contact_title
            End Get
            Set(value As String)
                _contact_title = value
            End Set
        End Property

        Public Property IsVisible() As Boolean
            Get
                Return _isvisible
            End Get
            Set(value As Boolean)
                _isvisible = value
            End Set
        End Property
    End Class

    Protected Sub but_save_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles but_save.Click
        If hd_seller_id.Value > 0 Then
            Dim dt As New DataTable
            Dim obj As New CommonCode

            Dim ins_qry As String = ""
            Dim del_qry As String = "delete s from tbl_reg_buyer_seller_mapping s inner join tbl_reg_buyers b on s.buyer_id=b.buyer_id where b.is_active=1 and s.seller_id=" & hd_seller_id.Value & ";"
            dt = SqlHelper.ExecuteDataTable(del_qry)
            For Each dr As GridDataItem In grdChild.Items
                ins_qry = "insert into tbl_reg_buyer_seller_mapping (seller_id,buyer_id) values (" & hd_seller_id.Value & "," & dr.GetDataKeyValue("buyer_id") & ")"
                dt = New DataTable
                dt = SqlHelper.ExecuteDataTable(ins_qry)
            Next
            '  Response.Write("<script>window.opener.location.href=window.opener.location.href;window.close();</script>")
            Response.Write("<script>window.close();opener.location.reload();</script>")
        End If
    End Sub
End Class
