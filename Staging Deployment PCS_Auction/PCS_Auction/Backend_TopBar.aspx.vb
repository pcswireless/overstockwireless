﻿
Partial Class Backend_TopBar
    Inherits System.Web.UI.Page
    Public show_auction As Boolean = False
    Public show_bidder As Boolean = False
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Private Sub setPermission()
       

        If CommonCode.is_super_admin() Then
            show_auction = True
            rptItems.Visible = True
            show_bidder = True
        Else
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 29
                        show_auction = True
                        rptItems.Visible = True
                    Case 30
                        show_bidder = True
                End Select
            Next
            dt.Dispose()
            dt = Nothing
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If CommonCode.Fetch_Cookie_Shared("user_id") = "" Then
            Response.Write("<script language='javascript'>top.location = '/login.html';</script>")
        End If
        If CommonCode.Fetch_Cookie_Shared("is_backend") = "0" Then
            Response.Write("<script language='javascript'>top.location = '/default.aspx';</script>")
        End If
        If Not Page.IsPostBack Then
            If show_auction Then
                bindAuctions()
            End If

            lit_username.Text = CommonCode.Fetch_Cookie_Shared("displayname")
        End If
        'lit_systime.Text = String.Format("{0:t}", Now()) & ", " & String.Format("{0:MMM dd, yyyy}", Now())
    End Sub
    Public Function getCss(ByVal tabid As Integer, ByVal index As Integer) As String
        Dim strCss As String = ""
        Dim strUrl As String = Request.Url.ToString().ToLower()
        If tabid = Request.QueryString.Get("t") Then
            strCss = getcssString(index)
        End If

        Return strCss
    End Function
    Private Function getcssString(ByVal index As Integer) As String
        If index = 0 Then
            Return "m"
        Else
            Return "n"
        End If
    End Function
    Private Sub bindAuctions()
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" AndAlso CommonCode.Fetch_Cookie_Shared("user_id") > 0 Then


            '---------- remove auction from menu
            If Not String.IsNullOrEmpty(Request.QueryString.Get("r")) Then
                Dim rem_sno As Integer = SqlHelper.ExecuteScalar("select sno from tbl_login_user_auctions where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " and auction_id=" & Request.QueryString.Get("r") & "")
                SqlHelper.ExecuteNonQuery("update tbl_login_user_auctions set sno = sno-1 where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " and sno > " & rem_sno & "; delete from tbl_login_user_auctions where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " and auction_id=" & Request.QueryString.Get("r") & "")
            End If
            '---------- add a auction to menu
            If Not String.IsNullOrEmpty(Request.QueryString.Get("a")) Then
                Dim ins_sno As Integer = SqlHelper.ExecuteScalar("select isnull(max(sno),0) from tbl_login_user_auctions where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id"))
                SqlHelper.ExecuteNonQuery("if not exists(select mapping_id from tbl_login_user_auctions where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " and auction_id=" & Request.QueryString.Get("a") & ") begin insert into tbl_login_user_auctions (user_id,auction_id,sno) values (" & CommonCode.Fetch_Cookie_Shared("user_id") & "," & Request.QueryString.Get("a") & "," & ins_sno + 1 & ") end")
                hid_sel_auc_id.Value = Request.QueryString.Get("a")

                'if selected item not in top 4 then change position to last
                Dim dt As New DataTable()
                Dim auc_exists As Boolean = False
                dt = SqlHelper.ExecuteDataTable("select top 4 auction_id from tbl_login_user_auctions where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " order by sno desc")
                For Each dr As DataRow In dt.Rows
                    If dr.Item("auction_id") = Request.QueryString.Get("a") Then
                        auc_exists = True
                    End If
                Next

                If auc_exists = False Then
                    Dim curr_sno As Integer = SqlHelper.ExecuteScalar("select sno from tbl_login_user_auctions where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " and auction_id=" & Request.QueryString.Get("a"))
                    Dim max_sno As Integer = SqlHelper.ExecuteScalar("select max(sno) from tbl_login_user_auctions where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id"))
                    SqlHelper.ExecuteScalar("update tbl_login_user_auctions set sno = sno - 1 where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " and sno > " & curr_sno)
                    SqlHelper.ExecuteScalar("update tbl_login_user_auctions set sno = " & max_sno & " where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " and auction_id=" & Request.QueryString.Get("a"))
                End If

                'end of change position

            End If

            rptItems.DataSource = SqlHelper.ExecuteDatatable("select A.auction_id,isnull(A.title,'') as title from tbl_auctions A WITH (NOLOCK) inner join tbl_login_user_auctions M on A.auction_id = M.auction_id and isnull(A.discontinue,0)=0 and M.auction_id in (select top 4 auction_id from tbl_login_user_auctions where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " order by sno desc) where M.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " order by M.sno")
            rptItems.DataBind()
        End If
    End Sub
    Protected Sub rptItems_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptItems.ItemDataBound

        Dim _atag As HtmlAnchor = TryCast(e.Item.FindControl("auc_atag"), HtmlAnchor)
        Dim _span As Label = TryCast(e.Item.FindControl("auc_span"), Label)
        Dim _img As HtmlImage = TryCast(e.Item.FindControl("auc_img"), HtmlImage)

        If Request.QueryString.Get("t") = e.Item.ItemIndex + 4 Then 'current item selected(tab between 4 to 7)
            _atag.Attributes.Add("class", "m")
            _span.Attributes.Add("class", "n")

            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDataTable("select auction_id from tbl_login_user_auctions where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " order by sno desc")
            Select Case e.Item.ItemIndex
                Case 0
                    If dt.Rows.Count = 1 Then
                        _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=3&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3','/Auctions/AuctionListing.aspx');")
                    ElseIf dt.Rows.Count = 2 Then
                        _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=4&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "&a=" & dt.Rows(0).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3&a=" & dt.Rows(0).Item("auction_id") & "','/Auctions/AddEditAuction.aspx?i=" & dt.Rows(0).Item("auction_id") & "');")
                    ElseIf dt.Rows.Count = 3 Then
                        _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=4&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "&a=" & dt.Rows(1).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3&a=" & dt.Rows(1).Item("auction_id") & "','/Auctions/AddEditAuction.aspx?i=" & dt.Rows(1).Item("auction_id") & "');")
                    ElseIf dt.Rows.Count = 4 Then
                        _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=4&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "&a=" & dt.Rows(2).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3&a=" & dt.Rows(2).Item("auction_id") & "','/Auctions/AddEditAuction.aspx?i=" & dt.Rows(2).Item("auction_id") & "');")
                    Else
                        _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=4&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "&a=" & dt.Rows(4).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3&a=" & dt.Rows(4).Item("auction_id") & "','/Auctions/AddEditAuction.aspx?i=" & dt.Rows(4).Item("auction_id") & "');")
                    End If
                Case 1
                    If dt.Rows.Count = 2 Then
                        _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=4&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "&a=" & dt.Rows(1).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3&a=" & dt.Rows(1).Item("auction_id") & "','/Auctions/AddEditAuction.aspx?i=" & dt.Rows(1).Item("auction_id") & "');")
                    ElseIf dt.Rows.Count = 3 Then
                        _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=5&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "&a=" & dt.Rows(0).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3&a=" & dt.Rows(0).Item("auction_id") & "','/Auctions/AddEditAuction.aspx?i=" & dt.Rows(0).Item("auction_id") & "');")
                    ElseIf dt.Rows.Count = 4 Then
                        _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=5&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "&a=" & dt.Rows(1).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3&a=" & dt.Rows(1).Item("auction_id") & "','/Auctions/AddEditAuction.aspx?i=" & dt.Rows(1).Item("auction_id") & "');")
                    Else
                        _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=5&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "&a=" & dt.Rows(3).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3&a=" & dt.Rows(3).Item("auction_id") & "','/Auctions/AddEditAuction.aspx?i=" & dt.Rows(3).Item("auction_id") & "');")
                    End If

                Case 2
                    If dt.Rows.Count = 3 Then
                        _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=5&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "&a=" & dt.Rows(1).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3&a=" & dt.Rows(1).Item("auction_id") & "','/Auctions/AddEditAuction.aspx?i=" & dt.Rows(1).Item("auction_id") & "');")
                    ElseIf dt.Rows.Count = 4 Then
                        _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=6&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "&a=" & dt.Rows(0).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3&a=" & dt.Rows(0).Item("auction_id") & "','/Auctions/AddEditAuction.aspx?i=" & dt.Rows(0).Item("auction_id") & "');")
                    Else
                        _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=6&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "&a=" & dt.Rows(2).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3&a=" & dt.Rows(2).Item("auction_id") & "','/Auctions/AddEditAuction.aspx?i=" & dt.Rows(2).Item("auction_id") & "');")
                    End If
                Case 3
                    If dt.Rows.Count = 4 Then
                        _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=6&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "&a=" & dt.Rows(1).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3&a=" & dt.Rows(1).Item("auction_id") & "','/Auctions/AddEditAuction.aspx?i=" & dt.Rows(1).Item("auction_id") & "');")
                    Else
                        _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=7&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "&a=" & dt.Rows(1).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3&a=" & dt.Rows(1).Item("auction_id") & "','/Auctions/AddEditAuction.aspx?i=" & dt.Rows(1).Item("auction_id") & "');")
                    End If
            End Select
        Else
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDataTable("select auction_id from tbl_login_user_auctions where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " order by sno desc")
            If Request.QueryString.Get("t") < 4 Or dt.Rows.Count > 4 Then
                'if auction not selected
                _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=" & Request.QueryString.Get("t") & IIf(String.IsNullOrEmpty(Request.QueryString.Get("a")), "", "&a=" & Request.QueryString.Get("a")) & "&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "','','');")
            Else
                _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=" & Request.QueryString.Get("t") - 1 & IIf(String.IsNullOrEmpty(Request.QueryString.Get("a")), "", "&a=" & Request.QueryString.Get("a")) & "&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "','','');")
            End If
        End If



        'Dim tab_id As String = String.Empty

        'If hid_sel_auc_id.Value = 0 Then
        '    If Request.UrlReferrer.ToString <> Nothing AndAlso Request.UrlReferrer.ToString <> "" Then
        '        If Request.QueryString.Get("t") = "1" Then tab_id = "/Backend_Dashbord.aspx?st=r"
        '        If Request.QueryString.Get("t") = "2" Then tab_id = "/Bidders/Buyers.aspx"
        '        If Request.QueryString.Get("t") = "3" Then tab_id = "/Auctions/AuctionListing.aspx"
        '    Else
        '        tab_id = "/Backend_Dashbord.aspx?st=r"
        '    End If

        '    _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=" & Request.QueryString.Get("t") & "&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "','/Backend_Leftbar.aspx?t=" & Request.QueryString.Get("t") & "','" & tab_id & "');")
        '    _atag.Attributes.Add("class", "")
        '    _span.Attributes.Add("class", "n")

        'ElseIf hid_sel_auc_id.Value = DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") Then
        '    _atag.Attributes.Add("class", "m")
        '    _span.Attributes.Add("class", "n")
        '    _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=3&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "','/Backend_Leftbar.aspx?t=3','/Auctions/AuctionListing.aspx');")
        'Else
        '    _img.Attributes.Add("onclick", "redirectIframe('/Backend_TopBar.aspx?t=" & e.Item.ItemIndex + 4 & "&r=" & DirectCast(e.Item.DataItem, DataRowView).Item("auction_id") & "&a=" & hid_sel_auc_id.Value & "','/Backend_Leftbar.aspx?t=3&a=" & hid_sel_auc_id.Value & "','/Auctions/AddEditAuction.aspx?i=" & hid_sel_auc_id.Value & "');")
        '    _atag.Attributes.Add("class", "")
        '    _span.Attributes.Add("class", "n")
        'End If

    End Sub

End Class
