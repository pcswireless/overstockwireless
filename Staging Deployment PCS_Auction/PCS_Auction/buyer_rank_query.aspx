﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="buyer_rank_query.aspx.vb"
    Inherits="buyer_rank_query" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="refresh" content="20" />
    <title></title>
    <link type="text/css" rel="Stylesheet" href="/Style/AuctionBody.css" />
    <link type="text/css" rel="Stylesheet" href="/Style/pcs.css" />
    <style type="text/css">
        .currentRank
        {
            padding: 4px 0px 10px 0px;
            color: #18472C;
            font-weight: bold;
            font-size: 12px;
            white-space: nowrap;
            height: 10px;
            display: block;
            text-align: center;
        }
    </style>
</head>
<body style="margin: 0; padding: 0; background-color: #E6E2E1;">
    <form id="form1" runat="server">
    <div>
        <div class="currentRank">
            <asp:Literal ID="lit_rank_amount" runat="server" />
        </div>
        <div style="color: #363A83; font-weight: bold; font-size: 15px; text-align: center;
            padding-bottom: 5px;">
            <asp:Literal runat="server" ID="ltrl_reply_pending"></asp:Literal>
        </div>
    </div>
    </form>
</body>
</html>
