﻿
Partial Class default_terms_conditions
    Inherits System.Web.UI.Page

    Protected Sub btnReject_Click(sender As Object, e As System.EventArgs) Handles btnReject.Click
        Response.Redirect("/")
    End Sub

    Protected Sub accept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles accept.Click
        Dim flag As Boolean = False
        Dim str As String = "select * from vw_login_users where user_id=" & Security.EncryptionDecryption.DecryptValueFormatted(Request("u"))
        Dim dt As New DataTable
        dt = SqlHelper.ExecuteDatatable(str)

        If dt.Rows.Count > 0 Then
            flag = True
            CommonCode.Create_Cookie_shared("user_id", dt.Rows(0)("user_id").ToString)
            CommonCode.Create_Cookie_shared("displayname", IIf(dt.Rows(0)("first_name").ToString <> "", dt.Rows(0)("first_name").ToString & IIf(dt.Rows(0)("last_name").ToString <> "", " " & dt.Rows(0)("last_name").ToString, ""), dt.Rows(0)("last_name").ToString))
            CommonCode.Create_Cookie_shared("username", dt.Rows(0)("username").ToString)
            CommonCode.Create_Cookie_shared("buyer_id", dt.Rows(0)("buyer_id").ToString)
            CommonCode.Create_Cookie_shared("is_backend", dt.Rows(0)("is_backend").ToString)
            CommonCode.Create_Cookie_shared("is_buyer", dt.Rows(0)("is_buyer").ToString)
            CommonCode.Create_Cookie_shared("is_admin", dt.Rows(0)("is_admin").ToString)
            CommonCode.Create_Cookie_shared("is_super_admin", dt.Rows(0)("super_admin").ToString)
            CommonCode.Create_Cookie_shared("profile_code", dt.Rows(0)("profile_code").ToString)
            str = "insert into tbl_sec_login_log (user_id,buyer_id,buyer_user_id,login_date,ip_address,browser_info,log_type) values (" & dt.Rows(0)("user_id") & "," & dt.Rows(0)("buyer_id").ToString & "," & dt.Rows(0)("user_id").ToString & ",getdate(),'" & HttpContext.Current.Request.UserHostAddress & "','" & Request.Browser.Browser.ToString & " - " & Request.Browser.Version.ToString & " - " & Request.Browser.Platform.ToString & "','Login')"
            SqlHelper.ExecuteNonQuery(str)

            str = "update tbl_reg_buyer_users set tc_accepted_date=getdate(), tc_is_accepted=1, tc_accept_ip='" & HttpContext.Current.Request.UserHostAddress & "'  where buyer_user_id=" & dt.Rows(0)("user_id")

            'Response.Write("update tbl_reg_buyer_users set tc_accepted_date=getdate(), tc_is_accepted=1, tc_accept_ip='" & Request.UserHostAddress & "' where buyer_user_id=" & dt.Rows(0)("user_id"))

            SqlHelper.ExecuteNonQuery(str)

        End If

        dt.Dispose()
        dt = Nothing

        If flag Then
            Response.Redirect("/live-auctions.html")
        Else
            Response.Redirect("/")
        End If
    End Sub

   
End Class
