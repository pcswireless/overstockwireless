﻿
Partial Class IncreaseBiddingLimit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                Dim bid_limit As Double = SqlHelper.ExecuteScalar("select case when exists(select buyer_user_id from tbl_reg_buyer_users where buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " ) then (select isnull(bidding_limit,0) from tbl_reg_buyer_users where buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ") else 0 end")
                lit_increase_bid_limit.Text = "<div class='bidlimitpop'>Your current bidding limit is $" & FormatNumber(bid_limit, 0) & "<br><br><a href='mailto:" & SqlHelper.of_FetchKey("site_email_to") & "?Subject=Increase%20Bidding%20limit' target='_top'>Request bid limit increase</a></div>"
            Else
                lit_increase_bid_limit.Text = "<div class='bidlimitpop'>Invalid Request</div>"
            End If
            
        End If
    End Sub
End Class
