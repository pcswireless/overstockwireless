﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SiteTopBar.aspx.vb" Inherits="SiteTopBar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="/Style/menu.css" />
    <script type="text/javascript" src="/pcs_js/dropdowntabs.js">

        /***********************************************
        * Drop Down Tabs Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
        * This notice MUST stay intact for legal use
        * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
        ***********************************************/

    </script>
    <script type="text/javascript" language="JavaScript">
        function redirectIframe(path1, path2, path3) {

            if (path1 != '')
                top.topFrame.location = path1;
            if (path2 != '')
                top.leftFrame.location = path2;
            if (path3 != '')
                top.mainFrame.location = path3;
        }
        function redirectPage(path) {
            if (path != '') {// alert(path);
                top.location.href = path;
            }
        }

    </script>
    <!--script type='text/javascript'>        (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://www.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({ c: '16043132-05cd-4602-a161-b7c3e1339364', f: true }); done = true; } }; })();</script-->
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="mainBody">
            <div class="topR1">
                <div class="l1">
                    <a href="javascript:void(0);" onclick="top.location.href = '/'" style="text-decoration:none;"><img src="/Images/logo.gif" border="0" alt="logo pcs wireless" /></a>
                </div>
                <div class="l2_front">
                    
                </div>
                <div class="l3">
                    <div class="l2R1">
                        <div class="topmnu1">
                            <a href="javascript:void(0);" onclick="<%=getRedirectPath("l") %>">
                                <asp:Literal ID="lit_logout" runat="server" /></a>
                        </div>
                        <div class="topmnusep">
                            |</div>
                        <div class="topmnu3">
                            <a href="javascript:void(0);" onclick="<%=getRedirectPath("m") %>">My Account</a>
                        </div>
                        <div class="topmnusep">
                            |</div>
                        <div class="topmnu4">
                            <a href="javascript:void(0);" onclick="redirectIframe('/SiteTopBar.aspx?t=1','/SiteLeftBar.aspx?t=1','/AuctionList.aspx?t=1');"
                            title="Live Auctions">
                            <asp:Literal ID="lit_username" runat="server"></asp:Literal></a>
                        </div>
                    </div>
                    <div class="l2R2">
                        <asp:Literal ID="lit_systime" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
            <div class="topR2">
                <div id="glowmenu" class="glowingtabs">
                    <ul>
                        <li><a href="javascript:void(0);" onclick="redirectIframe('/SiteTopBar.aspx?t=1','/SiteLeftBar.aspx?t=1','/AuctionList.aspx?t=1');"
                            title="Live Auctions" class='<%=getCss(1,0) %>'><span class='<%=getCss(1,1) %>'>Live Auctions</span></a></li>
                        <% If is_hidden_auction Then%>
                        <li><a href="javascript:void(0);" onclick="redirectIframe('/SiteTopBar.aspx?t=2','/SiteLeftBar.aspx?t=2','/AuctionList.aspx?t=2');"
                            title="Hidden Auctions" class='<%=getCss(2,0) %>'><span class='<%=getCss(2,1) %>'>Hidden Auctions</span></a></li>
                        <% End If%>
                        <% If is_upcoming_auction Then%>
                        <li><a href="javascript:void(0);" onclick="redirectIframe('/SiteTopBar.aspx?t=3','/SiteLeftBar.aspx?t=3','/AuctionList.aspx?t=3');"
                            title="Upcoming Auctions" class='<%=getCss(3,0) %>'><span class='<%=getCss(3,1) %>'>Upcoming Auctions</span></a></li>
                        <% End If%>
                        <% If mnu_fav Then%>
                         <li><a href="javascript:void(0);" onclick="redirectIframe('/SiteTopBar.aspx?t=4','/SiteLeftBar.aspx?t=4','/AuctionList.aspx?t=4');"
                            title="Favorite Auctions" class='<%=getCss(4,0) %>'><span class='<%=getCss(4,1) %>'>Favorites</span></a></li>
                         <% End If%>
                         <% If is_bid_auction Then%>
                        <li><a href="javascript:void(0);" onclick="redirectIframe('/SiteTopBar.aspx?t=5','/SiteLeftBar.aspx?t=5','/AuctionList.aspx?t=5');"
                            title="Upcoming Auctions" class='<%=getCss(5,0) %>'><span class='<%=getCss(5,1) %>'>
                                Bidding Auctions</span></a></li>
                        <% End If%>
                        <li><a href="javascript:void(0);" onclick="redirectIframe('/SiteTopBar.aspx?t=6','/SiteLeftBar.aspx?t=6','/AuctionList.aspx?t=6');"
                            title="Upcoming Auctions" class='<%=getCss(6,0) %>'><span class='<%=getCss(6,1) %>'>Auction History</span></a></li>
                    </ul>
                </div>
                <div class="lang_flag">
                   
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
