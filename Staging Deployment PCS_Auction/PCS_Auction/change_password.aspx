﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="change_password.aspx.vb"
    Inherits="change_password" MasterPageFile="~/SitePopUp.master" Title="PCS Bidding" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script type="text/javascript">

    /***********************************************
    * Different CSS depending on OS (mac/pc)- © Dynamic Drive (www.dynamicdrive.com)
    * This notice must stay intact for use
    * Visit http://www.dynamicdrive.com/ for full source code
    ***********************************************/
    ///////No need to edit beyond here////////////

    var mactest = navigator.userAgent.indexOf("Mac") != -1


    if (mactest) {
        document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet_mac.css"/>');
    }
    else {
        document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet.css"/>');
    }
  </script>
    <script>
        function closeWin() {
            myWindow.close();                                                // Closes the new window
        }
    </script>
    
     <div id="bid-popup" style="width:100%;">
     <div>
    <h2>
        <span class="blue">Change Password</span></h2>

        </div>
        <br /><br />
    <!-- <table cellpadding="5" cellspacing="0" width="100%" border="0">-->
    <div class="inputboxes" style="margin:0;">
        <%--<tr id="tr_price" runat="server"> --%>
        <!-- <td>-->
        <div id="tr_price" class="" runat="server" style="padding-bottom:10px; ">
            <%--<br />--%>
            <%-- <div style="text-align: center;">--%>
            <div id="tr_old_pwd" runat="server">
                <div class ="fleft" style=" padding:10px 36px 10px 0px;"><span id="Span1" class="" runat="server">Old Password</span> <span id="span_auction_title"
                    runat="server" class="req_star">*</span></div>
                    <div>
                <asp:TextBox ID="txt_old_password" runat="server" CssClass="" style="margin-top:5px;"  Width="150" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_old_password"
                    ValidationGroup="chn_pwd" Display="Dynamic" ErrorMessage="<br />Old Password Required"
                    CssClass="error"></asp:RequiredFieldValidator>
              
                    </div>
            </div><div class="clear"><br /> 
            </div>
            <div>
                <div class ="fleft"  style=" padding:10px 30px 10px 0px;"><span id="Span2" class="" runat="server">New Password</span><span id="span3" runat="server"
                    class="req_star"> *</span></div>
                <asp:TextBox ID="txt_password" runat="server" CssClass="" Width="150" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_password" runat="server" ControlToValidate="txt_password"
                    ValidationGroup="chn_pwd" Display="Dynamic" ErrorMessage="<br />New Password Required"
                    CssClass="error"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txt_password"
                    ValidationGroup="chn_pwd" ErrorMessage="<br /> <span style='font-size:12px;'>Alphanumeric atleast 6 chars.</span>"
                    ValidationExpression="(?=.*\d)(?=.*[a-zA-Z]).{6,}" Display="Dynamic"></asp:RegularExpressionValidator>
            </div> <div class="clear"><br />
            </div>
            <div>
                <div class ="fleft"  style=" padding:10px 2px 10px 0px;"><span id="Span4" class="text" runat="server">Confirm Password</span><span id="span5" runat="server"
                    class="req_star"> *</span></div>
                <asp:TextBox ID="txt_retype_password" runat="server" CssClass="" Width="150" TextMode="Password"></asp:TextBox>
                <asp:CompareValidator ID="comp_retype_password" runat="server" ControlToValidate="txt_retype_password"
                    ControlToCompare="txt_password" CssClass="error" Display="Dynamic" ValidationGroup="chn_pwd"
                    ErrorMessage="<br />Confirm Password Mismatch"></asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_password"
                    ValidationGroup="chn_pwd" Display="Dynamic" ErrorMessage="<br />Confirm Password Required"
                    CssClass="error"></asp:RequiredFieldValidator>
            </div>
            <!--</td>-->
            <div class="clear"><br />
            </div>
        </div>
        </div>
        </div>
    <div>
        <asp:Literal ID="lit_message" runat="server"></asp:Literal>
        <asp:Label ID="lbl_error" runat ="server" EnableViewState="false" ForeColor="Red" ></asp:Label>
    </div>
        <!--inputboxes class ends-->
         <div id="change_pwd" style="width:100%;">
        <div style =" float:left;">
        <asp:Button ID="imgbtn_update" ValidationGroup="chn_pwd" runat="server" Text="Update"
            CssClass="" /></div>
            <div>
        <input type="button" name="" id="popupClose" onclick="javascript:window.close();"
            class="fleft" value="Cancel" /></div>
    </div>
    <br />
    
<script type ="text/javascript" >
    window.focus();
    </script>

</asp:Content>
