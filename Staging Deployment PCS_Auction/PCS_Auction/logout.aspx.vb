﻿
Partial Class logout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Or CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            SqlHelper.ExecuteNonQuery("insert into tbl_sec_login_log (user_id,buyer_id,buyer_user_id,logout_date,ip_address,browser_info,log_type) values (" & CommonCode.Fetch_Cookie_Shared("user_id") & "," & CommonCode.Fetch_Cookie_Shared("buyer_id") & "," & CommonCode.Fetch_Cookie_Shared("user_id") & ",getdate(),'" & HttpContext.Current.Request.UserHostAddress & "','" & Request.Browser.Browser.ToString & " - " & Request.Browser.Version.ToString & " - " & Request.Browser.Platform.ToString & "','Logout')")
        End If

        CommonCode.Remove_Cookie_shared("user_id")
        CommonCode.Remove_Cookie_shared("username")
        CommonCode.Remove_Cookie_shared("buyer_id")
        CommonCode.Remove_Cookie_shared("is_backend")
        CommonCode.Remove_Cookie_shared("is_buyer")
        CommonCode.Remove_Cookie_shared("is_admin")
        CommonCode.Remove_Cookie_shared("is_super_admin")
        CommonCode.Remove_Cookie_shared("displayname")

        CommonCode.Create_Cookie_shared("user_id", "")
        CommonCode.Create_Cookie_shared("username", "")
        CommonCode.Create_Cookie_shared("buyer_id", "")
        CommonCode.Create_Cookie_shared("is_backend", "")
        CommonCode.Create_Cookie_shared("is_buyer", "")
        CommonCode.Create_Cookie_shared("is_admin", "")
        CommonCode.Create_Cookie_shared("is_super_admin", "")
        CommonCode.Create_Cookie_shared("displayname", "")

        Response.Redirect("/?out=" & Guid.NewGuid().ToString)

    End Sub
End Class
