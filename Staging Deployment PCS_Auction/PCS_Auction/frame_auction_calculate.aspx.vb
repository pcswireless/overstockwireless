﻿
Partial Class frame_auction_calculate
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim obj As New Auction
            ViewState("rank_list") = ""
            Dim dt As New DataTable
            dt = obj.fetch_auction_Listing_new(IIf(String.IsNullOrEmpty(Request.QueryString.Get("t")), 1, Request.QueryString.Get("t")), IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), 0, Request.QueryString.Get("c")), IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), 0, Request.QueryString.Get("p")), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("user_id")), CommonCode.Fetch_Cookie_Shared("user_id"), 0), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("buyer_id")), CommonCode.Fetch_Cookie_Shared("buyer_id"), 0), SqlHelper.of_FetchKey("frontend_login_needed"), IIf(String.IsNullOrEmpty(Request.QueryString.Get("a")), 0, Request.QueryString.Get("a"))).Tables(0)
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    setAuction(dr("auction_id"))
                Next
                If ViewState("rank_list") <> "" Then
                    'Response.Write(ViewState("rank_list"))
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "_send", "window.parent.calculate_rank('" & ViewState("rank_list") & "')", True)
                End If

            End If
            obj = Nothing
        End If
    End Sub

    Private Sub setAuction(ByVal auction_id As Int32)
        Me.litRefresh.Visible = True
        Dim str As String = "SELECT A.auction_id,"
        str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"

        str = str & "ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing,"

        str = str & "ISNULL(A.increament_amount, 0) AS increament_amount,"
        str = str & "ISNULL(A.buy_now_price, 0) AS buy_now_price,"
        str = str & "ISNULL(A.is_show_rank, 0) AS is_show_rank,"
        str = str & "dbo.get_auction_status(A.auction_id) as auction_status"
        str = str & " FROM "
        str = str & "tbl_auctions A WITH (NOLOCK) WHERE "
        str = str & "A.auction_id =" & auction_id

        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDatatable(str)

        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)

                If .Item("auction_status") <> 3 Then 'running or upcoming

                    Dim bid_amount As Double = 0
                    Dim bidder_max_amount As Double = 0
                    'panel setting
                    If .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then

                        Dim dtHBid As New DataTable()
                        dtHBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids WITH (NOLOCK) where auction_id=" & auction_id & " order by bid_amount desc")

                        Dim c_bid_amount As Double = 0
                        If dtHBid.Rows.Count > 0 Then
                            c_bid_amount = dtHBid.Rows(0).Item("bid_amount")
                        End If

                        Dim bidder_price As Double = 0
                        Dim bidder_max_price As Double = 0

                        If .Item("is_show_rank") Then
                            If String.IsNullOrEmpty(CommonCode.Fetch_Cookie_Shared("buyer_id")) Then
                                ViewState("rank_list") = ViewState("rank_list") & auction_id & "#Current Rank : --~"
                            Else
                                Dim rak As Integer = SqlHelper.ExecuteScalar("select dbo.buyer_bid_rank(" & auction_id & "," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
                                ViewState("rank_list") = ViewState("rank_list") & auction_id & "#Current Rank : " & IIf(rak = 0, "--", rak) & "~"

                            End If
                        End If

                    Else
                        'lit_amount_caption.Visible = False
                        ''lit_rank.Visible = False
                    End If

                End If


            End With
        End If
        dtTable = Nothing
    End Sub
End Class
