﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="MyAccount.aspx.vb" Inherits="MyAccount"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>User Account</title>
          <![if IE]>
<link rel="stylesheet" type="text/css" href="/Style/font_ie.css" />
<![endif]>
        <![if !IE]>
<link rel="stylesheet" type="text/css" href="/Style/font_other.css" />
<![endif]>
    <link type="text/css" rel="Stylesheet" href="/Style/master_menu.css" />
    <link rel="stylesheet" type="text/css" href="/Style/menu.css" />
    <link type="text/css" rel="Stylesheet" href="/Style/pcs.css" />
    <script type="text/javascript">
        function open_pass_win(_path, _pwd) {
            w1 = window.open(_path + '?' + _pwd, '_paswd', 'top=' + ((screen.height - 350) / 2) + ', left=' + ((screen.width - 500) / 2) + ', height=350, width=500,scrollbars=yes,toolbars=no,resizable=1;');
            w1.focus();
            return false;

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="ScriptManager" runat="server" />
    <div>
        <asp:HiddenField ID="hd_buyer_id" runat="server" Value="0" />
        <asp:HiddenField ID="hd_buyer_user_id" runat="server" Value="0" />
        <asp:HiddenField ID="hd_is_buyer" runat="server" Value="0" />
        <asp:HiddenField ID="hid_flang" runat="server" Value="0" />
        <asp:HiddenField ID="hid_buyer_max_bid_amount" runat="server" Value="0" />
        <asp:Label runat="server" ID="lbl_show" ForeColor="Red"></asp:Label>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="imgbtn_basic_edit" EventName="Click">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Account" />
                        <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Account_2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="imgbtn_basic_update" EventName="Click">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Account" />
                        <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Account_2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="imgbtn_basic_cancel" EventName="Click">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Account" />
                        <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Account_2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="imgbtn_basic_save">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Account" />
                        <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Account_2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="imgbtn_sub_edit">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_subinfo" />
                        <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_subinfo_cancel" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="imgbtn_basic_update">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="lbl_show" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="imgbtn_sub_update">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_subinfo" />
                        <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_subinfo_cancel" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="imgbtn_sub_cancel">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_subinfo" />
                        <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_subinfo_cancel" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
            left: 580px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
            <img id="Image8" src="/images/img_loading.gif" alt="" />
        </telerik:RadAjaxLoadingPanel>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <div style="float: left; margin: 10px;">
                        <div class="pageheading">
                            <asp:Label runat="server" meta:resourcekey="lbl_page_heading_Resource1" ID="lbl_page_heading"></asp:Label>
                        </div>
                    </div>
                   <%-- <div style="float: right; margin: 10px; padding-right: 20px;">
                        <a href="javascript:void(0);" onclick="javascript:open_help('1');">
                            <img src="/Images/help-button.gif" border="none" />
                        </a>
                    </div>--%>
                </td>
            </tr>
            <tr>
                <td class="PageTabContentEdit">
                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td class="tdTabItem">
                                <a name="1"></a>
                                <asp:Panel ID="Panel1_Header1" runat="server" CssClass="pnlTabItemHeader">
                                    <div class="tabItemHeader" style="background: url(/Images/basic_info.gif) no-repeat;
                                        background-position: 10px 0px;">
                                        <asp:Literal runat="server" meta:resourcekey="lit_basic_info" ID="lit_basic_info"></asp:Literal>
                                    </div>
                                    <asp:Image ID="pnl_img1" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.jpg"
                                        CssClass="panelimage" />
                                </asp:Panel>
                                <ajax:CollapsiblePanelExtender ID="cpe1" BehaviorID="cpe1" runat="server" Enabled="True"
                                    TargetControlID="Panel1_Content1" CollapseControlID="Panel1_Header1" ExpandControlID="Panel1_Header1"
                                    Collapsed="true" ImageControlID="pnl_img1" CollapsedImage="/Images/down_Arrow.png"
                                    ExpandedImage="/Images/up_Arrow.png">
                                </ajax:CollapsiblePanelExtender>
                                <ajax:CollapsiblePanelExtender ID="cpe11" BehaviorID="cpe11" runat="server" Enabled="True"
                                    TargetControlID="Panel1_Content1button" CollapseControlID="Panel1_Header1" ExpandControlID="Panel1_Header1"
                                    Collapsed="true" ImageControlID="pnl_img1" CollapsedImage="/Images/down_Arrow.png"
                                    ExpandedImage="/Images/up_Arrow.png">
                                </ajax:CollapsiblePanelExtender>
                                <asp:Panel ID="Panel1_Content1" runat="server" CssClass="collapsePanel">
                                    <div style="padding: 10px;">
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel_Account" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                            CssClass="TabGrid" ClientEvents-OnRequestStart="Cancel_Ajax">
                                            <table cellpadding="0" cellspacing="2" border="0" width="838">
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Label ID="lblMessage" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                                                        <asp:HiddenField ID="hid_password" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" class="dv_md_sub_heading">
                                                        Company details
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 145px;" class="caption">
                                                        Company&nbsp;<span id="span_company_name" runat="server" class="req_star">*</span>
                                                    </td>
                                                    <td style="width: 270px;" class="details">
                                                        <asp:TextBox ID="txt_company" runat="server" CssClass="inputtype"></asp:TextBox>
                                                        <asp:Label ID="lbl_company" runat="server"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="rfv_company" runat="server" ControlToValidate="txt_company"
                                                            ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Company Required"
                                                            CssClass="error"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td class="caption" style="width: 145px;">
                                                        Company Website
                                                    </td>
                                                    <td style="width: 270px;" class="details">
                                                        <asp:TextBox ID="txt_website" runat="server" CssClass="inputtype"></asp:TextBox>
                                                        <asp:Label ID="lbl_website" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" class="dv_md_sub_heading">
                                                        Contact details
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="caption">
                                                        First Name&nbsp;<span id="span_first_name" runat="server" class="req_star">*</span>
                                                    </td>
                                                    <td class="details">
                                                        <asp:TextBox ID="txt_first_name" runat="server" CssClass="inputtype"></asp:TextBox>
                                                        <asp:Label ID="lbl_first_name" runat="server"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="rfv_contact_name" runat="server" ControlToValidate="txt_first_name"
                                                            ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />First Name Required"
                                                            CssClass="error"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td class="caption">
                                                        Last Name
                                                    </td>
                                                    <td class="details">
                                                        <asp:TextBox ID="txt_last_name" runat="server" CssClass="inputtype"></asp:TextBox>
                                                        <asp:Label ID="lbl_last_name" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td  class="caption">
                                                        Title
                                                    </td>
                                                    <td  class="details">
                                                        <asp:DropDownList ID="ddl_title" runat="server" DataTextField="name" DataValueField="name"
                                                            CssClass="DropDown" Width="204">
                                                        </asp:DropDownList>
                                                        <asp:Label ID="lbl_title" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="caption">
                                                        Street&nbsp;<span id="span_address1" runat="server" class="req_star">*</span>
                                                    </td>
                                                    <td class="details">
                                                        <asp:TextBox ID="txt_address1" TextMode="MultiLine" Height="30px" runat="server"
                                                            CssClass="inputtype"></asp:TextBox>
                                                        <asp:Label ID="lbl_address1" runat="server"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="rfv_address1" runat="server" ControlToValidate="txt_address1"
                                                            ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Street Required"
                                                            CssClass="error"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td class="caption">
                                                        Street Address 2
                                                    </td>
                                                    <td class="details">
                                                        <asp:TextBox ID="txt_address2" TextMode="MultiLine" Height="30px" runat="server"
                                                            CssClass="inputtype"></asp:TextBox>
                                                        <asp:Label ID="lbl_address2" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="caption">
                                                        City&nbsp;<span id="span_city" runat="server" class="req_star">*</span>
                                                    </td>
                                                    <td class="details">
                                                        <asp:TextBox ID="txt_city" runat="server" CssClass="inputtype"></asp:TextBox>
                                                        <asp:Label ID="lbl_city" runat="server"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="rfv_city" runat="server" ControlToValidate="txt_city"
                                                            ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />City Required"
                                                            CssClass="error"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td class="caption">
                                                        Country&nbsp;<span id="span_country" runat="server" class="req_star">*</span>
                                                    </td>
                                                    <td class="details">
                                                        <asp:DropDownList ID="ddl_country" runat="server" DataTextField="name" DataValueField="country_id"
                                                            CssClass="DropDown" Width="204">
                                                        </asp:DropDownList>
                                                        <asp:Label ID="lbl_country" runat="server"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="rfv_country" runat="server" ControlToValidate="ddl_country"
                                                            ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Country Required"
                                                            CssClass="error"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="caption">
                                                        State/Province&nbsp;<span id="span_state" runat="server" class="req_star">*</span>
                                                    </td>
                                                    <td class="details">
                                                        <asp:TextBox ID="txt_other_state" runat="server" MaxLength="55" CssClass="inputtype"></asp:TextBox>
                                                        <asp:Label ID="lbl_state" runat="server"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="rfv_txt_state" runat="server" ControlToValidate="txt_other_state"
                                                            ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />State Required"
                                                            CssClass="error"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td class="caption">
                                                        Postal Code&nbsp;<span id="span_zip" runat="server" class="req_star">*</span>
                                                    </td>
                                                    <td class="details">
                                                        <asp:TextBox ID="txt_zip" runat="server" CssClass="inputtype" MaxLength="5"></asp:TextBox>
                                                        <asp:Label ID="lbl_zip" runat="server"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="rfv_zip" runat="server" ControlToValidate="txt_zip"
                                                            ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Postal Code Required"
                                                            CssClass="error"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txt_zip"
                                                            ValidationExpression="[0-9]{5}" EnableClientScript="true" Display="Dynamic" ErrorMessage="<br />Invalid Postal Code"
                                                            runat="server" ValidationGroup="val_basic_info" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="caption">
                                                        Phone 1&nbsp;<span id="span_mobile" runat="server" class="req_star">*</span>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lbl_mobile" runat="server"></asp:Label>
                                                        <asp:TextBox ID="txt_mobile" runat="server" CssClass="inputtype" MaxLength="15"></asp:TextBox>
                                                        <br />
                                                        <asp:RadioButton ID="rdo_ph_1_landline" runat="server" Text="Landline" GroupName="p1"
                                                            Checked="true" /><asp:RadioButton ID="rdo_ph_1_mobile" runat="server" Text="Mobile"
                                                                GroupName="p1" />
                                                        <asp:RequiredFieldValidator ID="rfv_mobile" runat="server" ControlToValidate="txt_mobile"
                                                            ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Phone 1 Required"
                                                            CssClass="error"></asp:RequiredFieldValidator>
                                                        <%--<asp:RegularExpressionValidator ID="Reg_phone2" ControlToValidate="txt_mobile" ValidationExpression="[0-9]{10,13}"
                                                            EnableClientScript="true" Display="Dynamic" ErrorMessage="<br />Invalid Phone 1"
                                                            runat="server" ValidationGroup="val_basic_info" />--%>
                                                    </td>
                                                    <td class="caption">
                                                        Phone 2
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lbl_phone" runat="server"></asp:Label>
                                                        <asp:TextBox ID="txt_phone" runat="server" CssClass="inputtype" MaxLength="15"></asp:TextBox>
                                                        <br />
                                                        <asp:RadioButton ID="rdo_ph_2_landline" runat="server" Text="Landline" GroupName="p2"
                                                            Checked="true" /><asp:RadioButton ID="rdo_ph_2_mobile" runat="server" Text="Mobile"
                                                                GroupName="p2" />
                                                       <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txt_phone"
                                                            ValidationExpression="[0-9]{10,13}" Display="Dynamic" EnableClientScript="true"
                                                            ErrorMessage="<br />Invalid Phone 2" runat="server" ValidationGroup="val_basic_info" />--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <asp:Panel Visible="true" runat="server" ID="pnl_admin_email">
                                                        <td class="caption">
                                                            Email&nbsp;<span id="span_email" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_email" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_email" runat="server"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="rfv_email" runat="server" ControlToValidate="txt_email"
                                                                ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Email Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ValidationGroup="val_basic_info" ID="rev_email" runat="server"
                                                                ControlToValidate="txt_email" Display="Dynamic" ErrorMessage="<br />Invalid Email"
                                                                ValidationExpression="^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$" CssClass="error"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </asp:Panel>
                                                    <td class="caption">
                                                        Fax
                                                    </td>
                                                    <td class="details">
                                                        <asp:TextBox ID="txt_fax" runat="server" CssClass="inputtype" MaxLength="15"></asp:TextBox>
                                                        <asp:Label ID="lbl_fax" runat="server"></asp:Label>
                                                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txt_fax"
                                                            ValidationExpression="[0-9]{10,13}" Display="Dynamic" EnableClientScript="true"
                                                            ErrorMessage="<br />Invalid Fax" runat="server" ValidationGroup="val_basic_info" />--%>
                                                    </td>
                                                </tr>
                                                <asp:Panel Visible="true" runat="server" ID="pnl_admin">
                                                    <tr>
                                                        <td colspan="4" class="dv_md_sub_heading">
                                                            Account setup
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            User ID&nbsp;<span id="span_user_id" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_user_id" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_user_id" runat="server"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="rfv_user_id" runat="server" ControlToValidate="txt_user_id"
                                                                ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />User ID Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="caption">
                                                            Password&nbsp;<span id="span_password" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:Literal ID="lit_change_password" runat="server"></asp:Literal>
                                                            <asp:Label ID="lbl_password" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            Add comments here
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_comment" runat="server" CssClass="inputtype" TextMode="MultiLine"
                                                                Height="30px"></asp:TextBox>
                                                            <asp:Label ID="lbl_comment" runat="server"></asp:Label>
                                                        </td>
                                                        <td>
                                                            &nbsp
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </asp:Panel>
                                                <tr>
                                                    <td colspan="4" class="dv_md_sub_heading">
                                                        Other details
                                                    </td>
                                                </tr>
                                                <%--<tr>
                                                    <td class="caption">
                                                        Business Type
                                                    </td>
                                                    <td class="details">
                                                        <asp:CheckBoxList ID="chklst_business_types" runat="server" CellPadding="0" CellSpacing="0"
                                                            BorderWidth="0" Width="100%">
                                                        </asp:CheckBoxList>
                                                        <asp:Literal ID="ltrl_business_types" runat="server"></asp:Literal>
                                                    </td>
                                                    <td class="caption">
                                                        Industry Type
                                                    </td>
                                                    <td class="details">
                                                        <asp:CheckBoxList ID="chklst_industry_types" runat="server" CellPadding="0" CellSpacing="0"
                                                            BorderWidth="0" Width="100%">
                                                        </asp:CheckBoxList>
                                                        <asp:Literal ID="ltrl_industry_type" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>--%>
                                                <tr>
                                                    <td class="caption">
                                                        Linked Companies
                                                    </td>
                                                    <td class="details">
                                                        <asp:CheckBoxList ID="chklst_companies_linked" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                                            Width="100%" CellPadding="0" CellSpacing="0" BorderWidth="0">
                                                        </asp:CheckBoxList>
                                                        <asp:Literal ID="ltrl_linked_companies" runat="server"></asp:Literal>
                                                    </td>
                                                    <td class="caption">
                                                        How did you find us?
                                                    </td>
                                                    <td class="details">
                                                        <asp:DropDownList ID="ddl_how_find_us" runat="server" CssClass="DropDown" Width="204">
                                                        </asp:DropDownList>
                                                        <asp:Literal ID="ltrl_how_find_us" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="caption">Unsubscribe for Auction Invitation</td>
                                                    <td colspan="3">
                                                        <asp:CheckBox ID="chk_invite_auction" runat="server" />
                                                         <asp:Label ID="lbl_invite_auction_unsubscribe" runat="server"></asp:Label>
                                                        <asp:HiddenField ID="hid_unsubscribe_invite" runat="server" Value="0" />
                                                    </td>
                                                </tr>

                                            </table>
                                        </telerik:RadAjaxPanel>
                                    </div>
                                </asp:Panel>
                            </td>
                            <td class="fixedcolumn" valign="top">
                                <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                <asp:Panel ID="Panel1_Content1button" runat="server" CssClass="collapsePanel">
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel_Account_2" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                        ClientEvents-OnRequestStart="Cancel_Ajax">
                                        <div class="addButton">
                                            <asp:ImageButton ID="imgbtn_basic_edit" Visible="false" ImageUrl="/Images/edit.gif" runat="server" />
                                            <asp:ImageButton ID="imgbtn_basic_pwd" Visible="false" ImageUrl="/Images/changepassword.png" runat="server" OnClientClick="return open_pass_win('/change_password.aspx','b=0&i=" & hd_buyer_user_id.Value & "&o=1');" />
                                            <asp:ImageButton ID="imgbtn_basic_save" ValidationGroup="val_basic_info" runat="server"
                                                AlternateText="Save" ImageUrl="/images/save.gif" />
                                            <asp:ImageButton ID="imgbtn_basic_update" ValidationGroup="val_basic_info" runat="server"
                                                AlternateText="Update" ImageUrl="/images/update.gif" />
                                        </div>
                                        <div class="cancelButton" id="div_basic_cancel" runat="server">
                                            <asp:ImageButton ID="imgbtn_basic_cancel" runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                        </div>
                                    </telerik:RadAjaxPanel>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr runat="server" id="tr_sub_info">
                            <td class="tdTabItem">
                                <a name="7"></a>
                                <asp:Panel ID="Panel7_Header" runat="server" CssClass="pnlTabItemHeader">
                                    <div class="tabItemHeader" style="background: url(/Images/personalinfo.png) no-repeat;
                                        background-position: 10px 0px;">
                                        <asp:Literal runat="server" meta:resourcekey="lit_personal_info" ID="lit_personal_info"></asp:Literal>
                                    </div>
                                    <asp:Image ID="pnl_img7" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.png"
                                        CssClass="panelimage" />
                                </asp:Panel>
                                <ajax:CollapsiblePanelExtender ID="cpe7" BehaviorID="cpe7" runat="server" Enabled="True"
                                    TargetControlID="Panel7_Content" CollapseControlID="Panel7_Header" ExpandControlID="Panel7_Header"
                                    Collapsed="true" ImageControlID="pnl_img7" CollapsedImage="/Images/down_Arrow.png"
                                    ExpandedImage="/Images/up_Arrow.png">
                                </ajax:CollapsiblePanelExtender>
                                <ajax:CollapsiblePanelExtender ID="cpe77" BehaviorID="cpe77" runat="server" Enabled="True"
                                    TargetControlID="Panel7_Content7button" CollapseControlID="Panel7_Header" ExpandControlID="Panel7_Header"
                                    Collapsed="true" ImageControlID="pnl_img7" CollapsedImage="/Images/down_Arrow.png"
                                    ExpandedImage="/Images/up_Arrow.png">
                                </ajax:CollapsiblePanelExtender>
                                <asp:Panel ID="Panel7_Content" runat="server" CssClass="collapsePanel">
                                    <div style="padding: 10px;">
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel_subinfo" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                            CssClass="TabGrid" ClientEvents-OnRequestStart="Cancel_Ajax">
                                            <table cellpadding="0" cellspacing="2" border="0" width="838">
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Label ID="Label1" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                                                        <asp:HiddenField ID="hid_sub_password" runat="server" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="caption">
                                                        First Name&nbsp;<span id="span_sub_first" runat="server" class="req_star">*</span>
                                                    </td>
                                                    <td class="details">
                                                        <asp:TextBox ID="txt_sub_first" runat="server" CssClass="inputtype"></asp:TextBox>
                                                        <asp:Label ID="lbl_sub_first" runat="server"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_sub_first"
                                                            ValidationGroup="val_sub_info" Display="Dynamic" ErrorMessage="<br />Name Required"
                                                            CssClass="error"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td class="caption">
                                                        Last Name&nbsp;
                                                    </td>
                                                    <td class="details">
                                                        <asp:TextBox ID="txt_sub_last" runat="server" CssClass="inputtype"></asp:TextBox>
                                                        <asp:Label ID="lbl_sub_last" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="caption">
                                                        Title
                                                    </td>
                                                    <td class="details">
                                                        <asp:DropDownList ID="dd_sub_title" runat="server" DataSourceID="sub_login_title"
                                                            DataTextField="name" DataValueField="title_id" CssClass="DropDown" Width="204">
                                                        </asp:DropDownList>
                                                        <asp:Label ID="lbl_sub_title" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="caption">
                                                        Email&nbsp;<span id="span_sub_email" runat="server" class="req_star">*</span>
                                                    </td>
                                                    <td class="details">
                                                        <asp:TextBox ID="txt_sub_email" runat="server" CssClass="inputtype"></asp:TextBox>
                                                        <asp:Label ID="lbl_sub_email" runat="server"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_sub_email"
                                                            ValidationGroup="val_sub_info" Display="Dynamic" ErrorMessage="<br />Email Required"
                                                            CssClass="error"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ValidationGroup="val_sub_info" ID="RegularExpressionValidator5"
                                                            runat="server" ControlToValidate="txt_sub_email" Display="Dynamic" ErrorMessage="<br>Invalid Email"
                                                            ValidationExpression="^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$"
                                                            CssClass="error"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 145px;" class="caption">
                                                        User Name
                                                    </td>
                                                    <td style="width: 270px;" class="details">
                                                        <asp:Label ID="lbl_sub_username" runat="server"></asp:Label>
                                                    </td>
                                                    <td style="width: 145px;" class="caption">
                                                        Password&nbsp;<span id="span_sub_password" runat="server" class="req_star">*</span>
                                                    </td>
                                                    <td style="width: 270px;" class="details">
                                                        <asp:Literal ID="lit_sub_change_password" runat="server"></asp:Literal>
                                                        <asp:Label ID="lbl_sub_password" Text="******" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </telerik:RadAjaxPanel>
                                    </div>
                                </asp:Panel>
                            </td>
                            <td class="fixedcolumn" valign="top">
                                <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                <asp:Panel ID="Panel7_Content7button" runat="server" CssClass="collapsePanel">
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel_subinfo_cancel" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                        ClientEvents-OnRequestStart="Cancel_Ajax">
                                        <div class="addButton">
                                            <asp:ImageButton ID="imgbtn_sub_edit" ImageUrl="/Images/edit.gif" runat="server" />
                                            <asp:ImageButton ID="imgbtn_sub_update" ValidationGroup="val_sub_info" runat="server"
                                                AlternateText="Update" ImageUrl="/images/update.gif" />
                                        </div>
                                        <div class="cancelButton" id="div_sub_info" runat="server">
                                            <asp:ImageButton ID="imgbtn_sub_cancel" runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                        </div>
                                    </telerik:RadAjaxPanel>
                                </asp:Panel>
                            </td>
                        </tr>
                        <asp:Panel ID="pnl_edit_mode" runat="server">
                            <tr runat="server" id="tr_sublogin">
                                <td class="tdTabItem">
                                    <a name="2"></a>
                                    <asp:Panel ID="Panel2_Header" runat="server" CssClass="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/sub_logins_icon.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            <asp:Literal runat="server" meta:resourcekey="lit_sub_login" ID="lit_sub_login"></asp:Literal>
                                        </div>
                                        <asp:Image ID="pnl_img2" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.png"
                                            CssClass="panelimage" />
                                    </asp:Panel>
                                    <ajax:CollapsiblePanelExtender ID="cpe2" BehaviorID="cpe2" runat="server" Enabled="True"
                                        TargetControlID="Panel2_Content" CollapseControlID="Panel2_Header" ExpandControlID="Panel2_Header"
                                        Collapsed="true" ImageControlID="pnl_img2" CollapsedImage="/Images/down_Arrow.png"
                                        ExpandedImage="/Images/up_Arrow.png">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="Panel2_Content" runat="server" CssClass="collapsePanel">
                                        <div style="padding: 10px;">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel_Sublogin" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                CssClass="TabGrid">
                                                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadGrid ID="rad_grid_sublogins" runat="server" Skin="Vista" GridLines="None"
                                                                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                                                OnNeedDataSource="rad_grid_sublogins_NeedDataSource" OnDeleteCommand="rad_grid_sublogins_DeleteCommand"
                                                                OnInsertCommand="rad_grid_sublogins_InsertCommand" OnUpdateCommand="rad_grid_sublogins_UpdateCommand"
                                                                enableajax="true" ClientSettings-Selecting-AllowRowSelect="false" PageSize="10"
                                                                ShowGroupPanel="true">
                                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="buyer_user_id"
                                                                    ItemStyle-Height="40" AlternatingItemStyle-Height="40" HorizontalAlign="NotSet"
                                                                    AutoGenerateColumns="False" EditMode="EditForms">
                                                                    <CommandItemStyle BackColor="#E1DDDD" />
                                                                    <NoRecordsTemplate>
                                                                        Sub login not available
                                                                    </NoRecordsTemplate>
                                                                    <SortExpressions>
                                                                        <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                                                                    </SortExpressions>
                                                                    <CommandItemSettings AddNewRecordText="Add New Sub-Login" />
                                                                    <Columns>
                                                                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                            EditImageUrl="/Images/edit_grid.gif" HeaderStyle-Width="90">
                                                                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="90" />
                                                                        </telerik:GridEditCommandColumn>
                                                                        <telerik:GridBoundColumn DataField="buyer_user_id" HeaderText="buyer_user_id" UniqueName="buyer_user_id"
                                                                            ReadOnly="True" Visible="false">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="buyer_id" HeaderText="buyer_id" UniqueName="buyer_id"
                                                                            ReadOnly="True" Visible="false">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Name" SortExpression="name" UniqueName="first_name"
                                                                            EditFormColumnIndex="1" HeaderStyle-Width="180" ItemStyle-Width="180" GroupByExpression="first_name [Name] Group By first_name">
                                                                            <ItemTemplate>
                                                                                <%# Eval("first_name") & " " & Eval("last_name")%>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Title" AllowFiltering="true" UniqueName="title"
                                                                            HeaderStyle-Width="100" ItemStyle-Width="90" GroupByExpression="title [Title] Group By title">
                                                                            <ItemTemplate>
                                                                                <%# Eval("title")%>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="User Name" AllowFiltering="true" UniqueName="username"
                                                                            HeaderStyle-Width="100" ItemStyle-Width="90" GroupByExpression="username [User Name] Group By username">
                                                                            <ItemTemplate>
                                                                                <%# Eval("username")%>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Password" AllowFiltering="false" UniqueName="password"
                                                                            EditFormColumnIndex="1" HeaderStyle-Width="90" ItemStyle-Width="80" Groupable="false">
                                                                            <ItemTemplate>
                                                                                ******
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Email" DataField="email" AllowFiltering="true"
                                                                            UniqueName="email" HeaderStyle-Width="100" ItemStyle-Width="90" GroupByExpression="email [Email] Group By email">
                                                                            <ItemTemplate>
                                                                                <a href='mailto:<%# Eval("email")%>'>
                                                                                    <%# Eval("email")%></a>
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate>
                                                                            </EditItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn UniqueName="bidding_limit" SortExpression="bidding_limit"
                                                                            HeaderText="Bidding Limit" HeaderStyle-Width="90" ItemStyle-Width="90" ItemStyle-HorizontalAlign="Right"
                                                                            GroupByExpression="bidding_limit [Bidding Limit] Group By bidding_limit">
                                                                            <ItemTemplate>
                                                                                <%# String.Format("{0:C2}", Eval("bidding_limit"))%>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Is Active" UniqueName="is_active" EditFormColumnIndex="1"
                                                                            HeaderStyle-Width="70" ItemStyle-Width="70" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Center" GroupByExpression="is_active [Is Active] Group By is_active">
                                                                            <ItemTemplate>
                                                                                <img src='<%#IIf(Eval("is_active"),"/Images/true.gif","/Images/false.gif") %>' style="border: none;"
                                                                                    alt="" />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"
                                                                            ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                                                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                                                        </telerik:GridButtonColumn>
                                                                    </Columns>
                                                                    <EditFormSettings InsertCaption="Insert New Sub-Login" EditFormType="Template">
                                                                        <FormTemplate>
                                                                            <table id="tbl_grd_item_edit" cellpadding="0" cellspacing="2" border="0" width="800px">
                                                                                <tr>
                                                                                    <td style="font-weight: bold; padding-top: 10px;" colspan="4">
                                                                                        Sub-Login Details
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="caption" style="width: 135px;">
                                                                                        First Name&nbsp;<span class="req_star">*</span>
                                                                                    </td>
                                                                                    <td style="width: 260px;" class="details">
                                                                                        <asp:TextBox ID="txt_sub_first_name" runat="server" Text='<%# Bind("first_name")%>'
                                                                                            CssClass="inputtype"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfv_first_name" runat="server" ErrorMessage="*" ForeColor="red"
                                                                                            ControlToValidate="txt_sub_first_name" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="caption" style="width: 135px;">
                                                                                        Last Name
                                                                                    </td>
                                                                                    <td style="width: 260px;" class="details">
                                                                                        <asp:TextBox ID="txt_sub_last_name" runat="server" Text='<%# Bind("last_name")%>'
                                                                                            CssClass="inputtype"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="caption">
                                                                                        Title
                                                                                    </td>
                                                                                    <td class="details">
                                                                                        <asp:DropDownList ID="dd_sub_title" DataSourceID="sub_login_title" DataTextField="name"
                                                                                            DataValueField="title_id" runat="server" SelectedValue='<%# Bind("title")%>'
                                                                                            CssClass="DropDown" Width="204">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                    <td class="caption">
                                                                                        Email&nbsp;<span class="req_star">*</span>
                                                                                    </td>
                                                                                    <td class="details">
                                                                                        <asp:TextBox ID="txt_sub_email" runat="server" Text='<%# Bind("email") %>' CssClass="inputtype"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfv_email" runat="server" ErrorMessage="*" ForeColor="red"
                                                                                            ControlToValidate="txt_sub_email" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ValidationGroup="val1_sub" ID="rev_email" runat="server"
                                                                                            ControlToValidate="txt_sub_email" Display="Dynamic" ErrorMessage="*" ValidationExpression="^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$"
                                                                                            CssClass="error"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="caption">
                                                                                        User Name&nbsp;<span class="req_star">*</span>
                                                                                    </td>
                                                                                    <td class="details">
                                                                                        <asp:TextBox ID="txt_sub_username" Enabled='<%#IIf(Eval("username").ToString<>"",false,true) %>'
                                                                                            runat="server" Text='<%# Bind("username") %>' CssClass="inputtypePWD"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfv_username" runat="server" ErrorMessage="*" ForeColor="red"
                                                                                            ControlToValidate="txt_sub_username" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="caption" style="width: 145px;">
                                                                                        Password&nbsp;<span class="req_star">*</span>
                                                                                    </td>
                                                                                    <td class="details">
                                                                                        <asp:TextBox ID="txt_sub_password" runat="server" TextMode="Password" Text='<%# Security.EncryptionDecryption.DecryptValue(IIF(Eval("password")IS DBNULL.Value,"",Eval("password"))) %>'
                                                                                            CssClass="inputtypePWD"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfv_password" runat="server" ErrorMessage="*" ForeColor="red"
                                                                                            ControlToValidate="txt_sub_password" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="rev_password" runat="server" Display="Dynamic"
                                                                                            ControlToValidate="txt_sub_password" ValidationGroup="val1_sub" CssClass="error"
                                                                                            ErrorMessage="<br>Alphanumeric of atleast 6 chars" ValidationExpression="(?=.*\d)(?=.*[a-zA-Z]).{6,}"></asp:RegularExpressionValidator>
                                                                                        <asp:HiddenField ID="hid_sub_password" runat="server" Value='<%# Security.EncryptionDecryption.DecryptValue(IIF(Eval("password") IS DBNULL.Value,"",Eval("password"))) %>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="caption">
                                                                                        Bidding Limit&nbsp;<span class="req_star">*</span>
                                                                                    </td>
                                                                                    <td class="details">
                                                                                        <asp:TextBox ID="txt_sub_bidding_limit" runat="server" MaxLength="8" Text='<%# Bind("bidding_limit") %>' CssClass="inputtype">
                                                                                        </asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfv_bidding_limit" runat="server" ErrorMessage="*"
                                                                                            ForeColor="red" ControlToValidate="txt_sub_bidding_limit" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="rev_bidding_limit" runat="server" Display="Dynamic"
                                                                                            ControlToValidate="txt_sub_bidding_limit" ValidationGroup="val1_sub" CssClass="error"
                                                                                            ErrorMessage="<br />Invalid" ValidationExpression="^\d*\.?\d*$"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                    <td class="caption">
                                                                                        Is Active
                                                                                    </td>
                                                                                    <td class="details">
                                                                                        <asp:CheckBox ID="chk_sub_is_active" runat="server" Checked='<%# IIF(checked(Eval("is_active")),True,False) %>' />
                                                                                        <asp:HiddenField ID="hid_old_active_status" runat="server" Value='<%# IIF(checked(Eval("is_active")),1,0) %>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" align="right">
                                                                                    <asp:Literal ID="lbl_grid_error" runat="server"></asp:Literal>
                                                                                    </td>
                                                                                    <td align="right" colspan="2">
                                                                                        <asp:ImageButton ID="but_grd_submit" CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem),"PerformInsert","Update") %>'
                                                                                            ValidationGroup="val1_sub" runat="server" AlternateText='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "Save" , "Update") %>'
                                                                                            ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif" , "/images/update.gif") %>' />
                                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="but_grd_cancel" CausesValidation="false" CommandName="Cancel"
                                                                                            runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </FormTemplate>
            
                                                                    </EditFormSettings>
                                                                </MasterTableView>
                                                                <ClientSettings AllowDragToGroup="true">
                                                                    <Selecting AllowRowSelect="True"></Selecting>
                                                                </ClientSettings>
                                                                <GroupingSettings ShowUnGroupButton="true" />
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </telerik:RadAjaxPanel>
                                        </div>
                                    </asp:Panel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <asp:SqlDataSource ID="sub_login_title" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                        runat="server" ProviderName="System.Data.SqlClient" SelectCommand="select '--Select--' as name,'' as title_id union all select isnull(name,'') as name,isnull(name,'') as title_id from tbl_master_titles order by name">
                                    </asp:SqlDataSource>
                                    &nbsp;
                                </td>
                            </tr>
                            <%--<tr>
                                <td class="tdTabItem">
                                    <a name="3"></a>
                                    <asp:Panel ID="Panel3_Header" runat="server" CssClass="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/wonbid.png) no-repeat;
                                            background-position: 10px 0px;">
                                            <asp:Literal runat="server" meta:resourcekey="lit_won_bid" ID="lit_won_bid"></asp:Literal>
                                        </div>
                                        <asp:Image ID="pnl_img3" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.png"
                                            CssClass="panelimage" />
                                    </asp:Panel>
                                    <ajax:CollapsiblePanelExtender ID="cpe3" BehaviorID="cpe3" runat="server" Enabled="True"
                                        TargetControlID="Panel3_Content" CollapseControlID="Panel3_Header" ExpandControlID="Panel3_Header"
                                        Collapsed="true" ImageControlID="pnl_img3" CollapsedImage="/Images/down_Arrow.png"
                                        ExpandedImage="/Images/up_Arrow.png">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="Panel3_Content" runat="server" CssClass="collapsePanel">
                                        <div style="padding: 10px;">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel_WinAuction" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                CssClass="TabGrid">
                                                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadGrid ID="RadGrid_WinAuction" runat="server" Skin="Vista" GridLines="None"
                                                                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                                                enableajax="true" PageSize="10">
                                                                <HeaderStyle BackColor="#BEBEBE" />
                                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                <ItemStyle Font-Size="11px" />
                                                                <MasterTableView Width="100%" DataKeyNames="auction_id" AllowSorting="True" HorizontalAlign="NotSet"
                                                                    AutoGenerateColumns="False" SkinID="Vista">
                                                                    <CommandItemStyle BackColor="#E1DDDD" />
                                                                    <NoRecordsTemplate>
                                                                        no win bid
                                                                    </NoRecordsTemplate>
                                                                    <Columns>
                                                                     <telerik:GridTemplateColumn DataField="title" HeaderText="Title" SortExpression="title"
                                UniqueName="title" FilterControlWidth="300" GroupByExpression="title [Title] group by title">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Auction_Bid_Detail.aspx?i=<%# Eval("auction_id") %>')">
                                        <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("title") %>'></asp:Label></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                                                                       
                                                                        <telerik:GridBoundColumn DataField="display_end_time" HeaderText="End Date" UniqueName="display_end_time"
                                                                            SortExpression="display_end_time">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Amount" UniqueName="price" SortExpression="price"
                                                                            DataField="price" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemTemplate>
                                                                                <%#"$" & FormatNumber(Eval("bid_amount"), 2)%>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                      
                                                                        <telerik:GridBoundColumn DataField="bid_type" HeaderText="Bid Type" UniqueName="bid_type"
                                                                            SortExpression="bid_type">
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </telerik:RadAjaxPanel>
                                        </div>
                                    </asp:Panel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    &nbsp;
                                </td>
                            </tr>--%>
                          <%--  <tr>
                                <td class="tdTabItem">
                                    <a name="4"></a>
                                    <asp:Panel ID="Panel4_Header" runat="server" CssClass="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/quotations.png) no-repeat;
                                            background-position: 10px 0px;">
                                            <asp:Literal runat="server" meta:resourcekey="lit_quotation" ID="lit_quotation"></asp:Literal>
                                        </div>
                                        <asp:Image ID="pnl_img4" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.png"
                                            CssClass="panelimage" />
                                    </asp:Panel>
                                    <ajax:CollapsiblePanelExtender ID="cpe4" BehaviorID="cpe4" runat="server" Enabled="True"
                                        TargetControlID="Panel4_Content" CollapseControlID="Panel4_Header" ExpandControlID="Panel4_Header"
                                        Collapsed="true" ImageControlID="pnl_img4" CollapsedImage="/Images/down_Arrow.png"
                                        ExpandedImage="/Images/up_Arrow.png">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="Panel4_Content" runat="server" CssClass="collapsePanel">
                                        <div style="padding: 10px;">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel_Quotation" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                CssClass="TabGrid">
                                                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadGrid ID="RadGrid_Quotation" runat="server" Skin="Vista" GridLines="None"
                                                                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                                                enableajax="true" PageSize="10">
                                                                <HeaderStyle BackColor="#BEBEBE" />
                                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                <ItemStyle Font-Size="11px" />
                                                                <MasterTableView Width="100%" DataKeyNames="auction_id" AllowSorting="True" HorizontalAlign="NotSet"
                                                                    AutoGenerateColumns="False" SkinID="Vista">
                                                                    <CommandItemStyle BackColor="#E1DDDD" />
                                                                    <NoRecordsTemplate>
                                                                        no quotation
                                                                    </NoRecordsTemplate>
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn DataField="title" HeaderText="Title" SortExpression="title"
                                UniqueName="title" FilterControlWidth="300" GroupByExpression="title [Title] group by title">
                                <ItemTemplate>
                                    <a href="/Auction_Bid_Detail.aspx?i=<%# Eval("auction_id") %>">
                                        <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("title") %>'></asp:Label></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                                                                       
                                                                        <telerik:GridBoundColumn DataField="display_end_time" HeaderText="End Date" UniqueName="display_end_time"
                                                                            SortExpression="display_end_time">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="details" HeaderText="Details" UniqueName="details"
                                                                            SortExpression="details">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn DataField="filename" HeaderText="File" UniqueName="filename"
                                                                            SortExpression="filename">
                                                                            <ItemTemplate>
                                                                                <a target="_blank" href='/Upload/Quotation/<%# Eval("auction_id") & "/" & Eval("filename") %>'>
                                                                                    <%# Eval("filename")%></a>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </telerik:RadAjaxPanel>
                                        </div>
                                    </asp:Panel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="tdTabItem">
                                    <a name="5"></a>
                                    <asp:Panel ID="Panel5_Header" runat="server" CssClass="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/buynow-partial-etc.png) no-repeat;
                                            background-position: 10px 0px;">
                                            <asp:Literal runat="server" meta:resourcekey="lit_private_offer" ID="lit_private_offer"></asp:Literal>
                                        </div>
                                        <asp:Image ID="pnl_img5" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.png"
                                            CssClass="panelimage" />
                                    </asp:Panel>
                                    <ajax:CollapsiblePanelExtender ID="cpe5" BehaviorID="cpe5" runat="server" Enabled="True"
                                        TargetControlID="Panel5_Content" CollapseControlID="Panel5_Header" ExpandControlID="Panel5_Header"
                                        Collapsed="true" ImageControlID="pnl_img5" CollapsedImage="/Images/down_Arrow.png"
                                        ExpandedImage="/Images/up_Arrow.png">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="Panel5_Content" runat="server" CssClass="collapsePanel">
                                        <div style="padding: 10px;">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel_Offer" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                CssClass="TabGrid">
                                                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadGrid ID="RadGrid_Offer" runat="server" Skin="Vista" GridLines="None"
                                                                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                                                enableajax="true" PageSize="10">
                                                                <HeaderStyle BackColor="#BEBEBE" />
                                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                <ItemStyle Font-Size="11px" />
                                                                <MasterTableView Width="100%" DataKeyNames="auction_id" AllowSorting="True" HorizontalAlign="NotSet"
                                                                    AutoGenerateColumns="False" SkinID="Vista">
                                                                    <CommandItemStyle BackColor="#E1DDDD" />
                                                                    <NoRecordsTemplate>
                                                                        no offer
                                                                    </NoRecordsTemplate>
                                                                    <Columns>
                                                                         <telerik:GridTemplateColumn DataField="title" HeaderText="Title" SortExpression="title"
                                UniqueName="title" FilterControlWidth="300" GroupByExpression="title [Title] group by title">
                                <ItemTemplate>
                                    <a href="/Auction_Bid_Detail.aspx?i=<%# Eval("auction_id") %>">
                                        <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("title") %>'></asp:Label></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                                                                        <telerik:GridBoundColumn DataField="display_end_time" HeaderText="End Date" UniqueName="display_end_time"
                                                                            SortExpression="display_end_time">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Amount" UniqueName="price" SortExpression="price"
                                                                            DataField="price" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemTemplate>
                                                                                <%#"$" & FormatNumber(Eval("price"), 2)%>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridBoundColumn DataField="status" HeaderText="Status" UniqueName="status"
                                                                            SortExpression="status">
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </telerik:RadAjaxPanel>
                                        </div>
                                    </asp:Panel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="tdTabItem">
                                    <a name="8"></a>
                                    <asp:Panel ID="Panel8_Header" runat="server" CssClass="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/partial-icon.png) no-repeat;
                                            background-position: 10px 0px;">
                                            <asp:Literal runat="server" meta:resourcekey="lit_partial_offer" ID="lit_partial_offer"></asp:Literal>
                                        </div>
                                        <asp:Image ID="pnl_img8" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.png"
                                            CssClass="panelimage" />
                                    </asp:Panel>
                                    <ajax:CollapsiblePanelExtender ID="cpe8" BehaviorID="cpe8" runat="server" Enabled="True"
                                        TargetControlID="Panel8_Content" CollapseControlID="Panel8_Header" ExpandControlID="Panel8_Header"
                                        Collapsed="true" ImageControlID="pnl_img8" CollapsedImage="/Images/down_Arrow.png"
                                        ExpandedImage="/Images/up_Arrow.png">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="Panel8_Content" runat="server" CssClass="collapsePanel">
                                        <div style="padding: 10px;">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel_Partial_Offer" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                CssClass="TabGrid">
                                                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadGrid ID="RadGrid_Partial_Offer" runat="server" Skin="Vista" GridLines="None"
                                                                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                                                enableajax="true" PageSize="10">
                                                                <HeaderStyle BackColor="#BEBEBE" />
                                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                <ItemStyle Font-Size="11px" />
                                                                <MasterTableView Width="100%" DataKeyNames="auction_id" AllowSorting="True" HorizontalAlign="NotSet"
                                                                    AutoGenerateColumns="False" SkinID="Vista">
                                                                    <CommandItemStyle BackColor="#E1DDDD" />
                                                                    <NoRecordsTemplate>
                                                                        no offer
                                                                    </NoRecordsTemplate>
                                                                    <Columns>
                                                                         <telerik:GridTemplateColumn DataField="title" HeaderText="Title" SortExpression="title"
                                UniqueName="title" FilterControlWidth="300" GroupByExpression="title [Title] group by title">
                                <ItemTemplate>
                                    <a href="/Auction_Bid_Detail.aspx?i=<%# Eval("auction_id") %>">
                                        <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("title") %>'></asp:Label></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                                                                        <telerik:GridBoundColumn DataField="display_end_time" HeaderText="End Date" UniqueName="display_end_time"
                                                                            SortExpression="display_end_time">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Amount" UniqueName="price" SortExpression="price"
                                                                            DataField="price" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemTemplate>
                                                                                <%#"$" & FormatNumber(Eval("price"), 2)%>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridBoundColumn DataField="quantity" HeaderText="Quantity" UniqueName="quantity"
                                                                            SortExpression="quantity">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="status" HeaderText="Status" UniqueName="status"
                                                                            SortExpression="status">
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </telerik:RadAjaxPanel>
                                        </div>
                                    </asp:Panel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="tdTabItem">
                                    <a name="9"></a>
                                    <asp:Panel ID="Panel9_Header" runat="server" CssClass="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/buynow-icon.png) no-repeat;
                                            background-position: 10px 0px;">
                                            <asp:Literal runat="server" meta:resourcekey="lit_buynow_offer" ID="lit_my_offer"></asp:Literal>
                                        </div>
                                        <asp:Image ID="pnl_img9" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.png"
                                            CssClass="panelimage" />
                                    </asp:Panel>
                                    <ajax:CollapsiblePanelExtender ID="cpe9" BehaviorID="cpe9" runat="server" Enabled="True"
                                        TargetControlID="Panel9_Content" CollapseControlID="Panel9_Header" ExpandControlID="Panel9_Header"
                                        Collapsed="true" ImageControlID="pnl_img9" CollapsedImage="/Images/down_Arrow.png"
                                        ExpandedImage="/Images/up_Arrow.png">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="Panel9_Content" runat="server" CssClass="collapsePanel">
                                        <div style="padding: 10px;">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel_Buynow" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                CssClass="TabGrid">
                                                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadGrid ID="RadGrid_Buynow" runat="server" Skin="Vista" GridLines="None"
                                                                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                                                enableajax="true" PageSize="10">
                                                                <HeaderStyle BackColor="#BEBEBE" />
                                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                <ItemStyle Font-Size="11px" />
                                                                <MasterTableView Width="100%" DataKeyNames="auction_id" AllowSorting="True" HorizontalAlign="NotSet"
                                                                    AutoGenerateColumns="False" SkinID="Vista">
                                                                    <CommandItemStyle BackColor="#E1DDDD" />
                                                                    <NoRecordsTemplate>
                                                                        no offer
                                                                    </NoRecordsTemplate>
                                                                    <Columns>
                                                                         <telerik:GridTemplateColumn DataField="title" HeaderText="Title" SortExpression="title"
                                UniqueName="title" FilterControlWidth="300" GroupByExpression="title [Title] group by title">
                                <ItemTemplate>
                                    <a href="/Auction_Bid_Detail.aspx?i=<%# Eval("auction_id") %>">
                                        <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("title") %>'></asp:Label></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                                                                        <telerik:GridBoundColumn DataField="display_end_time" HeaderText="End Date" UniqueName="display_end_time"
                                                                            SortExpression="display_end_time">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Amount" UniqueName="price" SortExpression="price"
                                                                            DataField="price" ItemStyle-HorizontalAlign="Right">
                                                                            <ItemTemplate>
                                                                                <%#"$" & FormatNumber(Eval("price"), 2)%>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridBoundColumn DataField="status" HeaderText="Status" UniqueName="status"
                                                                            SortExpression="status">
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </telerik:RadAjaxPanel>
                                        </div>
                                    </asp:Panel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    &nbsp;
                                </td>
                            </tr>--%>
                            <%--<tr>
                                <td class="tdTabItem">
                                    <a name="10"></a>
                                    <asp:Panel ID="Panel10_Header" runat="server" CssClass="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/query-icon.png) no-repeat;
                                            background-position: 10px 0px;">
                                            <asp:Literal runat="server" meta:resourcekey="lit_my_queries" ID="Literal1"></asp:Literal>
                                        </div>
                                        <asp:Image ID="pnl_img10" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.png"
                                            CssClass="panelimage" />
                                    </asp:Panel>
                                    <ajax:CollapsiblePanelExtender ID="cpe10" BehaviorID="cpe10" runat="server" Enabled="True"
                                        TargetControlID="Panel10_Content" CollapseControlID="Panel10_Header" ExpandControlID="Panel10_Header"
                                        Collapsed="true" ImageControlID="pnl_img10" CollapsedImage="/Images/down_Arrow.png"
                                        ExpandedImage="/Images/up_Arrow.png">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="Panel10_Content" runat="server" CssClass="collapsePanel">
                                        <div style="padding: 10px;">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel_my_query" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                CssClass="TabGrid">
                                                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <div style="overflow: auto; padding: 10px 5px 5px 20px; text-align: left;">
                                                                <asp:Label ID="lbl_no_query" runat="server"></asp:Label>
                                                                <asp:Repeater ID="rep_after_queries" runat="server">
                                                                    <ItemTemplate>
                                                                        <div class="qryTitle">
                                                                            <%# Container.ItemIndex + 1 & ". " & Eval("title")%>
                                                                        </div>
                                                                        <div class="qryAlt" style="padding-left: 16px;">
                                                                            <%# Eval("auction_title")%>
                                                                        </div>
                                                                        <div style="padding: 20px;">
                                                                            <asp:Repeater ID="rep_inner_after_queries" runat="server">
                                                                                <ItemTemplate>
                                                                                    <div class="qryDesc">
                                                                                        <img src='<%# IIF(Eval("image_path")<>"","/upload/users/" & Eval("user_id") & "/" & Eval("image_path"),"/images/buyer_icon.gif") %>'
                                                                                            alt="" style="float: left; padding: 2px; height: 23px; width: 22px;" />
                                                                                        <%#Eval("message") %>
                                                                                    </div>
                                                                                    <div class="qryAlt">
                                                                                        <%# IIf(Eval("buyer_title") = "", Eval("title"), Eval("buyer_title"))%>
                                                                                        <%# IIf(Eval("first_name") = "", Eval("buyer_first_name") & " " & Eval("buyer_last_name"), Eval("first_name") & " " & Eval("last_name"))%>,
                                                                                        <%# Eval("submit_date")%>
                                                                                    </div>
                                                                                    <br />
                                                                                    <br />
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </telerik:RadAjaxPanel>
                                        </div>
                                    </asp:Panel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    &nbsp;
                                </td>
                            </tr>--%>
                        </asp:Panel>
                    </table>
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
        <script type="text/javascript">

            function Cancel_Ajax(sender, args) {
                args.set_enableAjax(false);

            }

        </script>
    </telerik:RadScriptBlock>
    </form>
</body>
</html>
