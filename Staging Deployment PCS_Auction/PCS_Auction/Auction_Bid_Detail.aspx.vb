﻿Imports Telerik.Web.UI
Imports System.Globalization
Partial Class Auction_Bid_Detail
    Inherits BasePage

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            If CommonCode.Fetch_Cookie_Shared("is_backend") = "1" Then
                Response.Write("<script language='javascript'>top.location.href = '/Backend_Home.aspx?t=1';</script>")
            End If
        End If
        hid_flang.Value = System.Threading.Thread.CurrentThread.CurrentCulture.Name 'Application("_lang").ToString
        If Not IsPostBack Then

            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                hid_auction_id.Value = Request.QueryString("i")
                hid_language.Value = System.Threading.Thread.CurrentThread.CurrentCulture.Name
                If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                    hid_buyer_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
                    hid_buyer_id.Value = CommonCode.Fetch_Cookie_Shared("buyer_id")
                Else
                    hid_buyer_user_id.Value = 0
                    hid_buyer_id.Value = 0
                End If
                set_form()
                bind_auction_detail()
                load_auction_bidder_queries()
            End If
        End If
    End Sub

    Private Sub set_form()
        iframe_price.Attributes.Add("src", "/Auction_Price_Detail.aspx?i=" & Request.QueryString.Get("i"))

        Dim str As String = ""
        Dim auction_id As Integer = hid_auction_id.Value
        Dim is_prepaid_accept As Boolean = False
        Dim dt As DataTable = New DataTable
        Dim is_private_offer As Boolean = False
        Dim is_partial_offer As Boolean = False
        Dim is_buy_it_now As Boolean = False
        Dim startDate As DateTime = "1/1/1999"
        Dim endDate As DateTime = "1/1/1999"
        Dim curDate As DateTime = DateTime.Now
        Dim auction_status As Integer = 0
        Dim increament_amount As Double = 0
        Dim start_price As Double = 0
        Dim num_clicks As Integer = 0
        Dim num_buy_now_bids As Integer = 0
        Dim is_display_start_date As Boolean = False
        Dim is_display_end_date As Boolean = False
        str = "SELECT " & _
              "ISNULL(A.start_date, '1/1/1999') as start_date," & _              "ISNULL(A.end_date, '1/1/1999') as end_date," & _
              "ISNULL(A.is_display_start_date, 0) as is_display_start_date," & _
              "ISNULL(A.display_end_time, '1/1/1999') as display_end_time," & _
              "ISNULL(A.is_display_start_date, 0) as is_display_start_date," & _
              "ISNULL(A.is_display_end_date, 0) as is_display_end_date," & _
              "ISNULL(A.auction_type_id, 0) AS auction_type_id," & _              "ISNULL(A.is_private_offer, 0) AS is_private_offer," & _              "ISNULL(A.is_partial_offer, 0) AS is_partial_offer," & _              "ISNULL(A.is_buy_it_now, 0) AS is_buy_it_now," & _              " dbo.get_auction_status(A.auction_id) as auction_status," & _              "ISNULL(A.auto_acceptance_price_private, 0) AS auto_acceptance_price_private," & _              "ISNULL(A.auto_acceptance_price_partial, 0) AS auto_acceptance_price_partial," & _              "ISNULL(A.auto_acceptance_quantity, 0) AS auto_acceptance_quantity," & _              "ISNULL(A.no_of_clicks, 0) AS no_of_clicks," & _              "(select COUNT(buy_id) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and status='Accept' and auction_id=" & auction_id & ") As num_buy_now_bids," & _              "ISNULL(A.show_price, 0) AS show_price," & _               "ISNULL(A.start_price, 0) AS start_price," & _              "ISNULL(increament_amount,0) AS increament_amount," & _              "ISNULL((select max(bid_amount) from tbl_auction_bids WITH (NOLOCK) where auction_id=A.auction_id), 0) AS actual_price," & _              "ISNULL(rank1_bid_id,0) AS rank1_bid_id," & _               "ISNULL(A.buy_now_price, 0) AS buy_now_price, " & _               "ISNULL(A.is_accept_pre_bid, 0) AS is_accept_pre_bid,isnull(case when rank1_bid_id >0 then (select bid_amount from tbl_auction_bids WITH (NOLOCK) where bid_id=rank1_bid_id)+(increament_amount) else start_price end,0) as tradition_amount,isnull((select bidding_limit from tbl_reg_buyer_users where buyer_user_id='" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")) & "'),0) AS bidding_limit " & _               "FROM tbl_auctions A WITH (NOLOCK) WHERE A.auction_id =" & auction_id
        dt = SqlHelper.ExecuteDataTable(str)
        If dt.Rows.Count > 0 Then
            increament_amount = dt.Rows(0)("increament_amount")
            auction_status = dt.Rows(0)("auction_status")
            hid_bidlimit_amount.Value = dt.Rows(0)("bidding_limit")
            hid_bynow_amount.Value = dt.Rows(0)("buy_now_price")
            hid_show_price.Value = dt.Rows(0)("show_price")
            hid_tradition_amount.Value = dt.Rows(0)("tradition_amount")
            is_display_start_date = CBool(dt.Rows(0)("is_display_start_date"))
            is_display_end_date = CBool(dt.Rows(0)("is_display_end_date"))
            Dim rank1_bid_id As Integer = 0
            rank1_bid_id = dt.Rows(0)("rank1_bid_id")
            If rank1_bid_id > 0 Then
                Dim obj As New Bid()
                Dim dtBid As DataTable = New DataTable()
                dtBid = obj.get_bid_detail(rank1_bid_id)
                If dtBid.Rows.Count > 0 Then
                    hid_rank1_bid_id.Value = rank1_bid_id
                    hid_rank1_bid_amount.Value = dtBid.Rows(0)("bid_amount")
                End If
                obj = Nothing
                dtBid = Nothing
            Else
                hid_rank1_bid_amount.Value = dt.Rows(0)("actual_price")
            End If
            start_price = dt.Rows(0)("start_price")
            hid_auction_type_id.Value = dt.Rows(0)("auction_type_id")
            hid_auto_acceptance_price_private.Value = dt.Rows(0)("auto_acceptance_price_private")
            hid_auto_acceptance_price_partial.Value = dt.Rows(0)("auto_acceptance_price_partial")
            hid_auto_acceptance_quantity.Value = dt.Rows(0)("auto_acceptance_quantity")
            is_private_offer = dt.Rows(0)("is_private_offer")
            is_partial_offer = dt.Rows(0)("is_partial_offer")
            is_buy_it_now = CBool(dt.Rows(0)("is_buy_it_now"))
            num_clicks = dt.Rows(0)("no_of_clicks")
            num_buy_now_bids = dt.Rows(0)("num_buy_now_bids")
            startDate = dt.Rows(0)("start_date")
            endDate = dt.Rows(0)("display_end_time")
            is_prepaid_accept = dt.Rows(0)("is_accept_pre_bid")
            lit_trad_proxy_buy_now_price.Text = "Buy it now for $" & FormatNumber(hid_bynow_amount.Value, 2)
            If is_prepaid_accept Then
                hid_is_prebid_offer.Value = "1"
            End If
            If auction_status = 1 Then
                hid_auction_status.Value = "C"
            ElseIf auction_status = 2 Then
                hid_auction_status.Value = "U"
            Else
                hid_auction_status.Value = "P"
            End If
            If hid_auction_status.Value = "C" Then
                If hid_buyer_user_id.Value > 0 Then
                    pnl_after_launch.Visible = True
                End If
                If is_display_start_date Then
                    lbl_auction_status_start.Text = "Bid started at: " & startDate.ToString("MMM dd, yyyy hh:mm:ss") '& String.Format("{0:MMM dd,yyyy hh:mm:ss}", startDate.ToString())
                End If
                If is_display_end_date Then
                    lbl_auction_status_end.Text = "Bid closing at: " & endDate.ToString("MMM dd, yyyy hh:mm:ss")
                End If

                iframe1.Attributes.Add("src", "/timerframe.aspx?i=" & hid_auction_id.Value)
            ElseIf hid_auction_status.Value = "U" Then
                pnl_request_for_quote.Visible = False
                iframe1.Attributes.Add("src", "/timerframe.aspx?i=" & hid_auction_id.Value)
            ElseIf hid_auction_status.Value = "P" Then
                pnl_request_for_quote.Visible = False
                iframe1.Visible = False
            End If
        End If
        If hid_auction_type_id.Value = "1" Then
            If hid_auction_status.Value = "C" Then
                'pnl_buy_now.Visible = True
                RadTabStrip1.Visible = True
                pnl_buy_now_direct_bid.Visible = True
                RadTabStrip1.Tabs(0).Text = GetLocalResourceObject("lit_buy_now.Text")
                RadTabStrip1.Tabs(3).Visible = False
                If is_private_offer Then
                    'tr_buy_now_private_offer.Visible = True
                    pnl_buy_now_private_bid.Visible = True
                Else
                    RadTabStrip1.Tabs(1).Visible = False
                End If
                If is_partial_offer Then
                    'tr_buy_now_partial_offer.Visible = True
                    pnl_buy_now_partial_bid.Visible = True
                Else
                    RadTabStrip1.Tabs(2).Visible = False
                End If
            End If
        ElseIf hid_auction_type_id.Value = "2" Or hid_auction_type_id.Value = "3" Then
            If hid_auction_status.Value = "C" Then
                ' txt_test.Text = is_private_offer ' is_buy_it_now
                'pnl_trad_proxy.Visible = True
                RadTabStrip1.Visible = True
                pnl_trad_proxy_direct_bid.Visible = True
                pnl_after_launch_message.Visible = True
                If hid_auction_type_id.Value = "2" Then
                    RadTabStrip1.Tabs(0).Text = GetLocalResourceObject("lit_bid_now.Text")
                    lbl_trad_proxy_max_bid_amt_caption.Visible = False
                    rfv_bid_amt.EnableClientScript = False
                    txt_trad_proxy_max_bid_amount.Visible = False

                    If hid_rank1_bid_amount.Value > 0 Then
                        lbl_min_amount_to_bid.Text = "Bid now for " & CommonCode.GetFormatedMoney(hid_rank1_bid_amount.Value + increament_amount)

                    Else
                        lbl_min_amount_to_bid.Text = "Bid now for " & CommonCode.GetFormatedMoney(start_price)
                    End If

                Else
                    RadTabStrip1.Tabs(0).Text = GetLocalResourceObject("lit_bid_now.Text")
                    lbl_trad_proxy_max_bid_amt_caption.Visible = True
                    rfv_bid_amt.EnableClientScript = True
                    txt_trad_proxy_max_bid_amount.Visible = True
                    If hid_rank1_bid_amount.Value > 0 Then
                        lbl_min_amount_to_bid.Text = "Enter " & CommonCode.GetFormatedMoney(hid_rank1_bid_amount.Value + increament_amount) & " or more"
                        comvRG.ValueToCompare = hid_rank1_bid_amount.Value + increament_amount
                    Else
                        lbl_min_amount_to_bid.Text = "Enter " & CommonCode.GetFormatedMoney(start_price) & " or more"
                        comvRG.ValueToCompare = start_price
                    End If

                End If
                If is_private_offer Then
                    RadTabStrip1.Tabs(1).Visible = True
                    pnl_trad_proxy_private_bid.Visible = True
                Else
                    RadTabStrip1.Tabs(1).Visible = False
                End If
                If is_partial_offer Then
                    RadTabStrip1.Tabs(2).Visible = True
                    pnl_trad_proxy_partial_bid.Visible = True
                Else
                    RadTabStrip1.Tabs(2).Visible = False
                End If
                If is_buy_it_now And num_buy_now_bids < num_clicks Then
                    RadTabStrip1.Tabs(3).Visible = True
                    pnl_trad_proxy_buy_now_bid.Visible = True
                Else
                    RadTabStrip1.Tabs(3).Visible = False
                End If
                pnl_history.Visible = True
            ElseIf hid_auction_status.Value = "U" Then
                If hid_auction_type_id.Value = "3" Or hid_auction_type_id.Value = "2" Then
                    If is_prepaid_accept Then
                        RadTabStrip1.Visible = True
                        RadTabStrip1.Tabs(0).Visible = True
                        If hid_auction_type_id.Value = "2" Then
                            RadTabStrip1.Tabs(0).Text = GetLocalResourceObject("lit_bid_now.Text")
                        Else
                            RadTabStrip1.Tabs(0).Text = GetLocalResourceObject("lit_bid_now.Text")
                        End If

                        pnl_trad_proxy_direct_bid.Visible = True
                        If is_private_offer Then
                            RadTabStrip1.Tabs(1).Visible = True
                            pnl_trad_proxy_private_bid.Visible = True
                        Else
                            RadTabStrip1.Tabs(1).Visible = False

                        End If
                        If is_partial_offer Then
                            RadTabStrip1.Tabs(2).Visible = True
                            pnl_trad_proxy_partial_bid.Visible = True
                        Else
                            RadTabStrip1.Tabs(2).Visible = False
                        End If
                        If is_buy_it_now And num_buy_now_bids < num_clicks Then
                            RadTabStrip1.Tabs(3).Visible = True
                            pnl_trad_proxy_buy_now_bid.Visible = True
                        Else
                            RadTabStrip1.Tabs(3).Visible = False
                        End If
                    Else
                        RadTabStrip1.Visible = False
                    End If

                End If
            End If
        ElseIf hid_auction_type_id.Value = "4" Then
            If hid_auction_status.Value = "C" Then
                pnl_price.Visible = False
                RadTabStrip1.Visible = True
                RadTabStrip1.Tabs(0).Visible = True
                RadTabStrip1.Tabs(0).Text = GetLocalResourceObject("lit_send_quotation.Text")
                pnl_request_for_quote.Visible = True

                RadTabStrip1.Tabs(1).Visible = False
                RadTabStrip1.Tabs(2).Visible = False
                RadTabStrip1.Tabs(3).Visible = False
            End If
        End If
    End Sub

    Private Sub bind_auction_detail()
        Dim dtImage As DataTable
        Dim auction_id As Integer = hid_auction_id.Value
        Dim qry As String = "SELECT A.auction_id, " & _
              "ISNULL(A.code, '') AS code," & _              "ISNULL(A.title, '') AS title," & _              "ISNULL(A.sub_title, '') AS sub_title," & _              "ISNULL(A.product_catetory_id, 0) AS product_catetory_id," & _              "ISNULL(A.stock_condition_id, 0) AS stock_condition_id," & _              "ISNULL(A.packaging_id, 0) AS packaging_id," & _              "ISNULL(A.seller_id, 0) AS seller_id," & _              "ISNULL(L.name, '') AS stock_location," & _              "ISNULL(A.start_date, '1/1/1999') as start_date," & _              "ISNULL(A.display_end_time, '1/1/1999') as end_date1," & _              "convert(varchar, ISNULL(A.end_date, '1/1/1999'), 120) as end_date," & _              "ISNULL(A.is_active, 0) AS is_active," & _              "ISNULL(A.description, '') AS description," & _              "ISNULL(A.payment_terms, '') AS payment_terms," & _              "ISNULL(A.shipping_terms, '') AS shipping_terms," & _              "ISNULL(A.other_terms, '') AS other_terms," & _              "ISNULL(A.code_f, '') AS code_f," & _              "ISNULL(A.code_s, '') AS code_s," & _              "ISNULL(A.code_c, '') AS code_c," & _              "ISNULL(A.title_f, '') AS title_f," & _              "ISNULL(A.title_s, '') AS title_s," & _              "ISNULL(A.title_c, '') AS title_c," & _              "ISNULL(A.sub_title_f, '') AS sub_title_f," & _              "ISNULL(A.sub_title_s, '') AS sub_title_s," & _              "ISNULL(A.sub_title_c, '') AS sub_title_c," & _              "ISNULL(A.description_f, '') AS description_f," & _              "ISNULL(A.description_s, '') AS description_s," & _              "ISNULL(A.description_c, '') AS description_c," & _              "ISNULL(A.special_conditions_f, '') AS special_conditions_f," & _              "ISNULL(A.special_conditions_s, '') AS special_conditions_s," & _              "ISNULL(A.special_conditions_c, '') AS special_conditions_c," & _              "ISNULL(A.payment_terms_f, '') AS payment_terms_f," & _              "ISNULL(A.payment_terms_s, '') AS payment_terms_s," & _              "ISNULL(A.payment_terms_c, '') AS payment_terms_c," & _              "ISNULL(A.shipping_terms_f, '') AS shipping_terms_f," & _              "ISNULL(A.shipping_terms_s, '') AS shipping_terms_s," & _              "ISNULL(A.shipping_terms_c, '') AS shipping_terms_c," & _              "ISNULL(A.other_terms_f, '') AS other_terms_f," & _              "ISNULL(A.other_terms_s, '') AS other_terms_s," & _              "ISNULL(A.other_terms_c, '') AS other_terms_c, " & _              "ISNULL(A.short_description, '') AS short_description, " & _              "ISNULL(A.short_description_f, '') AS short_description_f, " & _              "ISNULL(A.short_description_s, '') AS short_description_s, " & _              "ISNULL(A.short_description_c, '') AS short_description_c, " & _               "ISNULL(A.request_for_quote_message, '') AS request_for_quote_message, " & _              "ISNULL(A.after_launch_message,'') AS after_launch_message" & _               " FROM tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations L on A.stock_location_id=L.stock_location_id WHERE A.auction_id = " & auction_id

        Dim dt As DataTable = New DataTable()
        dt = SqlHelper.ExecuteDataTable(qry)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                If hid_language.Value.ToString().ToUpper() = "FR-FR" Then 'french
                    lbl_auction_code.Text = CommonCode.decodeSingleQuote(.Item("code_f"))
                    lbl_title.Text = CommonCode.decodeSingleQuote(.Item("title_f"))
                    lbl_sub_title.Text = CommonCode.decodeSingleQuote(.Item("sub_title_f"))
                    ltrl_description.Text = CommonCode.decodeSingleQuote(dt.Rows(0)("description_f"))
                    lbl_payment_term.Text = CommonCode.decodeSingleQuote(.Item("payment_terms_f"))
                    lbl_shipping_term.Text = CommonCode.decodeSingleQuote(.Item("shipping_terms_f"))
                    lbl_other_terms.Text = CommonCode.decodeSingleQuote(.Item("other_terms_f"))
                ElseIf hid_language.Value.ToString().ToUpper() = "ES-ES" Then 'spanish
                    lbl_auction_code.Text = CommonCode.decodeSingleQuote(.Item("code_s"))
                    lbl_title.Text = CommonCode.decodeSingleQuote(.Item("title_s"))
                    lbl_sub_title.Text = CommonCode.decodeSingleQuote(.Item("sub_title_s"))
                    ltrl_description.Text = CommonCode.decodeSingleQuote(dt.Rows(0)("description_s"))
                    lbl_payment_term.Text = CommonCode.decodeSingleQuote(.Item("payment_terms_s"))
                    lbl_shipping_term.Text = CommonCode.decodeSingleQuote(.Item("shipping_terms_s"))
                    lbl_other_terms.Text = CommonCode.decodeSingleQuote(.Item("other_terms_s"))
                ElseIf hid_language.Value.ToString().ToUpper() = "ZH-CN" Then 'Chinese
                    lbl_auction_code.Text = CommonCode.decodeSingleQuote(.Item("code_c"))
                    lbl_title.Text = lbl_auction_code.Text
                    lbl_title.Text = CommonCode.decodeSingleQuote(.Item("title_c"))
                    lbl_sub_title.Text = CommonCode.decodeSingleQuote(.Item("sub_title_c"))
                    ltrl_description.Text = CommonCode.decodeSingleQuote(dt.Rows(0)("description_c"))
                    lbl_payment_term.Text = CommonCode.decodeSingleQuote(.Item("payment_terms_c"))
                    lbl_shipping_term.Text = CommonCode.decodeSingleQuote(.Item("shipping_terms_c"))
                    lbl_other_terms.Text = CommonCode.decodeSingleQuote(.Item("other_terms_c"))
                Else 'english
                    lbl_auction_code.Text = CommonCode.decodeSingleQuote(.Item("code"))
                    lbl_title.Text = CommonCode.decodeSingleQuote(.Item("title"))
                    lbl_sub_title.Text = CommonCode.decodeSingleQuote(.Item("sub_title"))
                    ltrl_description.Text = CommonCode.decodeSingleQuote(dt.Rows(0)("description"))
                    lbl_payment_term.Text = CommonCode.decodeSingleQuote(.Item("payment_terms"))
                    lbl_shipping_term.Text = CommonCode.decodeSingleQuote(.Item("shipping_terms"))
                    lbl_other_terms.Text = CommonCode.decodeSingleQuote(.Item("other_terms"))
                    lbl_short_desc.Text = CommonCode.decodeSingleQuote(.Item("short_description"))
                End If
                If .Item("request_for_quote_message") <> "" Then
                    ltrl_quote_message.Text = "<div style='padding-bottom:15px;'><span style='font-weight:bold;'>Message : </span>" & .Item("request_for_quote_message") & "</div>"
                End If
                lit_after_launch_message.Text = .Item("after_launch_message")

            End With
        End If
        qry = "select top 4 stock_image_id,filename from tbl_auction_stock_images where auction_id=" & auction_id & " order by stock_image_id"
        dtImage = SqlHelper.ExecuteDataTable(qry)
        Dim strMainImage As String = ""
        Dim strThumbImages As String = ""
        If dtImage.Rows.Count > 0 Then
            strMainImage = "<div id='loadarea' style='text-align:center; width:82; height:100px;'> <img  width='82px' height='100px' src='" & "/Upload/Auctions/stock_images/" & auction_id & "/" & dtImage.Rows(0)("stock_image_id") & "/" & dtImage.Rows(0)("filename") & "' border='0' /></div>"
            LTRL_Main_Image.Text = strMainImage
            Dim i As Integer = 0
            strThumbImages = "<table cellspacing='0' cellpadding='0' border='0'><tr><td align='center'>"
            For i = 0 To dtImage.Rows.Count - 1
                strThumbImages = strThumbImages & "<div style='float:left; height:38px; width:34px; border:solid 1px #dddee0; margin-left:4px; margin-top:4px;'><a href=" & "/Upload/Auctions/stock_images/" & auction_id & "/" & dtImage.Rows(i)("stock_image_id") & "/" & dtImage.Rows(i)("filename") & " rel='enlargeimage::mouseover' rev='loadarea'><img src=" & "/Upload/Auctions/stock_images/" & auction_id & "/" & dtImage.Rows(i)("stock_image_id") & "/" & dtImage.Rows(i)("filename") & " border='0' height='38' width='30' /></a></div>"
            Next
            strThumbImages = strThumbImages & "</td></tr></table>"
            LTRL_Thumb_Images.Text = strThumbImages
        Else
            strMainImage = "<div id='loadarea' style='text-align:center; width:82; height:100px;'> <img  width='82px' height='100px' src='" & "/images/imagenotavailable.gif' border='0' /></div>"
            LTRL_Main_Image.Text = strMainImage
        End If

        Dim dt1 As DataTable = New DataTable()
        dt1 = SqlHelper.ExecuteDataTable("select top 1 product_attachment_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_product_attachments where auction_id=" & auction_id & " order by product_attachment_id desc")
        If dt1.Rows.Count > 0 Then
            With dt1.Rows(0)
                Dim file_name As String = ""
                If hid_language.Value.ToString().ToUpper() = "FR-FR" Then
                    file_name = .Item("filename_f")
                ElseIf hid_language.Value.ToString().ToUpper() = "ES-ES" Then
                    file_name = .Item("filename_s")
                ElseIf hid_language.Value.ToString().ToUpper() = "ZH-CN" Then
                    file_name = .Item("filename_c")
                Else
                    file_name = .Item("filename")
                End If

                If file_name = "" Then
                    lbl_product_attachment.Text = ""
                Else
                    lbl_product_attachment.Text = "<a href='" & "/Upload/Auctions/product_attachments/" & auction_id & "/" & .Item("product_attachment_id") & "/" & file_name & "' target='_blank'>Product Details</a>"
                End If

            End With
        End If
        dt1 = Nothing

        Dim dt2 As DataTable = New DataTable()
        dt2 = SqlHelper.ExecuteDataTable("select top 1 tax_attachment_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_tax_attachments where auction_id=" & auction_id & " order by tax_attachment_id desc")
        If dt2.Rows.Count > 0 Then
            With dt2.Rows(0)
                Dim file_name As String = ""
                If hid_language.Value.ToString().ToUpper() = "FR-FR" Then
                    file_name = .Item("filename_f")
                ElseIf hid_language.Value.ToString().ToUpper() = "ES-ES" Then
                    file_name = .Item("filename_s")
                ElseIf hid_language.Value.ToString().ToUpper() = "ZH-CN" Then
                    file_name = .Item("filename_c")
                Else
                    file_name = .Item("filename")
                End If
                If file_name = "" Then
                    lbl_tax_details.Text = ""
                Else
                    lbl_tax_details.Text = "<a href='" & "/Upload/Auctions/tax_attachments/" & auction_id & "/" & .Item("tax_attachment_id") & "/" & file_name & "' target='_blank'>Tax Details</a>"
                End If

            End With
        End If
        dt2 = Nothing
        Dim dt3 As DataTable = New DataTable()
        dt3 = SqlHelper.ExecuteDataTable("select top 1 payment_term_attachment_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_payment_term_attachments where auction_id=" & auction_id & " order by payment_term_attachment_id desc")
        If dt3.Rows.Count > 0 Then
            With dt3.Rows(0)
                Dim file_name As String = ""
                If hid_language.Value.ToString().ToUpper() = "FR-FR" Then
                    file_name = .Item("filename_f")
                ElseIf hid_language.Value.ToString().ToUpper() = "ES-ES" Then
                    file_name = .Item("filename_s")
                ElseIf hid_language.Value.ToString().ToUpper() = "ZH-CN" Then
                    file_name = .Item("filename_c")
                Else
                    file_name = .Item("filename")
                End If
                If file_name = "" Then
                    lbl_payment_term_attachment.Text = ""
                Else
                    lbl_payment_term_attachment.Text = "<a href='" & "/Upload/Auctions/payment_term_attachments/" & auction_id & "/" & .Item("payment_term_attachment_id") & "/" & file_name & "' target='_blank'>View Attachment</a>"
                End If

            End With

        End If
        dt3 = Nothing

        Dim dt4 As DataTable = New DataTable()
        dt4 = SqlHelper.ExecuteDataTable("select top 1 shipping_term_attachment_id,ISNULL(filename,'') AS filename,ISNULL(filename_f,'') AS filename_f,ISNULL(filename_s,'') AS filename_s,ISNULL(filename_c,'') AS filename_c from tbl_auction_shipping_term_attachments where auction_id=" & auction_id & " order by shipping_term_attachment_id desc")
        If dt4.Rows.Count > 0 Then
            With dt4.Rows(0)
                Dim file_name As String = ""
                If hid_language.Value.ToString().ToUpper() = "FR-FR" Then
                    file_name = .Item("filename_f")
                ElseIf hid_language.Value.ToString().ToUpper() = "ES-ES" Then
                    file_name = .Item("filename_s")
                ElseIf hid_language.Value.ToString().ToUpper() = "ZH-CN" Then
                    file_name = .Item("filename_c")
                Else
                    file_name = .Item("filename")
                End If
                If file_name = "" Then
                    lbl_shipping_term_attachment.Text = ""
                Else
                    lbl_shipping_term_attachment.Text = "<a href='" & "/Upload/Auctions/shipping_term_attachments/" & auction_id & "/" & .Item("shipping_term_attachment_id") & "/" & file_name & "' target='_blank'>View Attachment</a>"
                End If

            End With
        End If
        dt4 = Nothing

        Dim dt5 As DataTable = New DataTable()
        dt5 = SqlHelper.ExecuteDataTable("select top 1 term_cond_attachment_id,isnull(filename,'') as filename,isnull(filename_f,'') as filename_f,isnull(filename_s,'') as filename_s,isnull(filename_c,'') as filename_c from tbl_auction_terms_cond_attachments where auction_id=" & auction_id & " order by term_cond_attachment_id desc")
        If dt5.Rows.Count > 0 Then
            With dt5.Rows(0)
                Dim file_name As String = ""
                If hid_language.Value.ToString().ToUpper() = "FR-FR" Then
                    file_name = .Item("filename_f")
                ElseIf hid_language.Value.ToString().ToUpper() = "ES-ES" Then
                    file_name = .Item("filename_s")
                ElseIf hid_language.Value.ToString().ToUpper() = "ZH-CN" Then
                    file_name = .Item("filename_c")
                Else
                    file_name = .Item("filename")
                End If
                If file_name = "" Then
                    lbl_other_term_attachment.Text = ""
                Else
                    lbl_other_term_attachment.Text = "<a href='" & "/upload/auctions/terms_cond_attachments/" & auction_id & "/" & .Item("term_cond_attachment_id") & "/" & file_name & "' target='_blank'>View Attachment</a>"
                End If
            End With
        End If
        dt5 = Nothing
        dt = Nothing
    End Sub

    Protected Sub btn_buy_now_bid_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btn_buy_now_bid.Click
        ViewState("option") = "buy_now_bid"
        buy_now_bid()
    End Sub
    Private Sub buy_now_bid()
        If hid_buyer_user_id.Value = "" Or hid_buyer_user_id.Value = "0" Then
            show_login("buy_now_bid")
            Exit Sub
        End If
        Dim is_bid_allow As Boolean = False
        If hid_auction_status.Value = "C" Then
            is_bid_allow = True
       
        End If
        If is_bid_allow Then
            Dim price As Double = 0
            Dim qty As Integer = 0
            Dim str As String = ""
            Dim auction_id As Integer = hid_auction_id.Value
            Dim obj As New Bid()
            Dim buy_id As Integer = 0
            If CommonCode.Fetch_Cookie_Shared("is_buyer") = "1" Then
                buy_id = obj.save_buy_now_bid(auction_id, price, qty, "buy now", hid_buyer_user_id.Value, hid_buyer_id.Value)
                If buy_id > 0 Then
                    send_to_confirmation_page(buy_id, "d")
                End If
                lbl_msg_buy_now_bid.Visible = True
                obj = Nothing
            ElseIf CDbl(hid_show_price.Value) <= CDbl(hid_bidlimit_amount.Value) Then

                buy_id = obj.save_buy_now_bid(auction_id, price, qty, "buy now", hid_buyer_user_id.Value, hid_buyer_id.Value)
                If buy_id > 0 Then
                    send_to_confirmation_page(buy_id, "d")
                End If
                lbl_msg_buy_now_bid.Visible = True
                obj = Nothing
            Else
                send_to_confirmation_page(0, "l")
            End If
        Else
            send_to_confirmation_page(0, "o")
        End If

    End Sub

    Protected Sub btn_buy_now_private_offer_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btn_buy_now_private_offer.Click
        ViewState("option") = "buy_now_private"
        buy_now_private_offer()
    End Sub

    Private Sub buy_now_private_offer()
        If hid_buyer_user_id.Value = "" Or hid_buyer_user_id.Value = "0" Then
            show_login("buy_now_private")
            Exit Sub
        End If
        Dim obj As New Bid()
        Dim buy_id As Integer = 0
        If CommonCode.Fetch_Cookie_Shared("is_buyer") = "1" Then
            buy_id = obj.save_buy_now_bid(hid_auction_id.Value, txt_private_buy_now_auto_accept_price.Text.Trim(), 0, "private", hid_buyer_user_id.Value, hid_buyer_id.Value)
            If buy_id > 0 Then
                send_to_confirmation_page(buy_id, "pr")
            End If
            'lbl_msg_buy_now_private.Text = "Your bid has been submitted."
            obj = Nothing
        ElseIf CDbl(txt_private_buy_now_auto_accept_price.Text.Trim()) <= CDbl(hid_bidlimit_amount.Value) Then
            'If txt_private_buy_now_auto_accept_price.Text.Trim() > hid_auto_acceptance_price_private.Value Then
           buy_id = obj.save_buy_now_bid(hid_auction_id.Value, txt_private_buy_now_auto_accept_price.Text.Trim(), 0, "private", hid_buyer_user_id.Value, hid_buyer_id.Value)
            If buy_id > 0 Then
                send_to_confirmation_page(buy_id, "pr")
            End If
            'lbl_msg_buy_now_private.Text = "Your bid has been submitted."
            obj = Nothing
        Else
            send_to_confirmation_page(0, "l")
        End If
        'Else
        'lbl_msg_buy_now_private.Text = "Auto accept price must be more than predefined auto accept price"
        'End If
    End Sub

    Protected Sub btn_buy_now_partial_offer_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btn_buy_now_partial_offer.Click
        ViewState("option") = "buy_now_partial"
        buy_now_partial_offer()
    End Sub

    Private Sub buy_now_partial_offer()
        If hid_buyer_user_id.Value = "" Or hid_buyer_user_id.Value = "0" Then
            show_login("buy_now_partial")
            Exit Sub
        End If
        Dim obj As New Bid()
        Dim buy_id As Integer = 0
        If CommonCode.Fetch_Cookie_Shared("is_buyer") = "1" Then
            buy_id = obj.save_buy_now_bid(hid_auction_id.Value, txt_partial_buy_now_auto_accept_price.Text.Trim(), txt_partial_buy_now_auto_accept_qty.Text.Trim(), "partial", hid_buyer_user_id.Value, hid_buyer_id.Value)
            If buy_id > 0 Then
                send_to_confirmation_page(buy_id, "pa")
            End If
            'lbl_msg_trad_proxy_partial.Text = "Your bid has been submitted."
            obj = Nothing
        ElseIf CDbl(txt_partial_buy_now_auto_accept_price.Text.Trim()) <= CDbl(hid_bidlimit_amount.Value) Then
            'If txt_partial_buy_now_auto_accept_price.Text.Trim() > hid_auto_acceptance_price_private.Value Or txt_partial_buy_now_auto_accept_qty.Text.Trim() > hid_auto_acceptance_quantity.Value Then
            buy_id = obj.save_buy_now_bid(hid_auction_id.Value, txt_partial_buy_now_auto_accept_price.Text.Trim(), txt_partial_buy_now_auto_accept_qty.Text.Trim(), "partial", hid_buyer_user_id.Value, hid_buyer_id.Value)
            If buy_id > 0 Then
                send_to_confirmation_page(buy_id, "pa")
            End If
            'lbl_msg_trad_proxy_partial.Text = "Your bid has been submitted."
            obj = Nothing
        Else
            send_to_confirmation_page(0, "l")
        End If
        'Else
        'lbl_msg_trad_proxy_partial.Text = "Auto accept price must be more than predefined auto accept price Or auto accept quantity must be more than predefined auto accept qty."
        'End If
    End Sub

    Protected Sub btn_trad_proxy_bid_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btn_trad_proxy_bid.Click
        ViewState("option") = "trad_proxy_bid"
        trad_proxy_bid()
    End Sub

    Private Sub trad_proxy_bid()
        If hid_buyer_user_id.Value = "" Or hid_buyer_user_id.Value = "0" Then
            show_login("trad_proxy_bid")
            Exit Sub
        End If
        Dim is_bid_allow As Boolean = False
        If hid_auction_status.Value = "C" Then
            is_bid_allow = True
        ElseIf hid_auction_status.Value = "U" Then
            If hid_is_prebid_offer.Value = "1" And (hid_auction_type_id.Value = "2" Or hid_auction_type_id.Value = "3") Then
                is_bid_allow = True
            End If
        End If
        If is_bid_allow Then

            Dim obj As New Bid()
            Dim auction_id As Integer = hid_auction_id.Value
            Dim buyer_user_id As Integer = hid_buyer_user_id.Value
            Dim buy_type As String = "Manual"
            Dim price As Double = 0
            If CommonCode.Fetch_Cookie_Shared("is_buyer") = "1" Then
                If hid_auction_type_id.Value = "3" Then

                    price = txt_trad_proxy_max_bid_amount.Text.Trim()

                End If

                Dim i As Integer = obj.insert_tbl_auction_bids(auction_id, buyer_user_id, price, "", 0)
                'txt_test.Text = price
                send_to_confirmation_page(i, "d")
               
                obj = Nothing
            Else

                If hid_auction_type_id.Value = "3" Then
                    If CDbl(txt_trad_proxy_max_bid_amount.Text.Trim()) <= CDbl(hid_bidlimit_amount.Value) Then
                        price = txt_trad_proxy_max_bid_amount.Text.Trim()
                        Dim i As Integer = obj.insert_tbl_auction_bids(auction_id, buyer_user_id, price, "", 0)

                        send_to_confirmation_page(i, "d")

                        obj = Nothing
                    Else
                        send_to_confirmation_page(0, "l")
                    End If
                End If
                If CDbl(hid_tradition_amount.Value) <= CDbl(hid_bidlimit_amount.Value) Then
                    Dim i As Integer = obj.insert_tbl_auction_bids(auction_id, buyer_user_id, 0, "", 0)
                    'txt_test.Text = price
                    send_to_confirmation_page(i, "d")

                    obj = Nothing
                Else
                    send_to_confirmation_page(0, "l")
                End If

            End If
        Else
            send_to_confirmation_page(0, "o")
        End If
    End Sub

    Private Sub send_to_confirmation_page(ByVal bid_id As Integer, ByVal bid_type As String)
        Response.Redirect("/Bid_confirmation.aspx?bid_id=" & bid_id & "&bid_type=" & bid_type & "&t=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("t")), "0", Request.QueryString.Get("t")))
    End Sub

    Protected Sub btn_trad_proxy_private_offer_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btn_trad_proxy_private_offer.Click
        ViewState("option") = "trad_proxy_private"
        trad_proxy_private_offer()
    End Sub

    Private Sub trad_proxy_private_offer()
        If hid_buyer_user_id.Value = "" Or hid_buyer_user_id.Value = "0" Then
            show_login("trad_proxy_private")
            Exit Sub
        End If
        Dim obj As New Bid()
        Dim buy_id As Integer = 0
        'If txt_trad_proxy_private_auto_accept_price.Text.Trim() > hid_auto_acceptance_price_private.Value Then
        If CommonCode.Fetch_Cookie_Shared("is_buyer") = "1" Then
            buy_id = obj.save_buy_now_bid(hid_auction_id.Value, txt_trad_proxy_private_auto_accept_price.Text.Trim(), 0, "private", hid_buyer_user_id.Value, hid_buyer_id.Value)
            If buy_id > 0 Then
                send_to_confirmation_page(buy_id, "pr")
            End If
            'lbl_msg_trad_proxy_private.Text = "Your bid has been submitted."
            obj = Nothing
        ElseIf CDbl(txt_trad_proxy_private_auto_accept_price.Text.Trim()) <= CDbl(hid_bidlimit_amount.Value) Then
            buy_id = obj.save_buy_now_bid(hid_auction_id.Value, txt_trad_proxy_private_auto_accept_price.Text.Trim(), 0, "private", hid_buyer_user_id.Value, hid_buyer_id.Value)
            If buy_id > 0 Then
                send_to_confirmation_page(buy_id, "pr")
            End If
            'lbl_msg_trad_proxy_private.Text = "Your bid has been submitted."
            obj = Nothing
        Else
            send_to_confirmation_page(0, "l")
        End If
        'Else
        'lbl_msg_trad_proxy_private.Text = "Auto accept price must be more than predefined auto accept price"
        'End If
    End Sub

    Protected Sub btn_trad_proxy_partial_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btn_trad_proxy_partial.Click
        ViewState("option") = "trad_proxy_partial"
        trad_proxy_partial()
    End Sub

    Private Sub trad_proxy_partial()
        If hid_buyer_user_id.Value = "" Or hid_buyer_user_id.Value = "0" Then
            show_login("trad_proxy_partial")
            Exit Sub
        End If
        Dim obj As New Bid()
        Dim buy_id As Integer = 0
        If CommonCode.Fetch_Cookie_Shared("is_buyer") = "1" Then
            buy_id = obj.save_buy_now_bid(hid_auction_id.Value, txt_trad_proxy_partial_auto_accept_price.Text.Trim(), txt_trad_proxy_partial_auto_accept_qty.Text.Trim(), "partial", hid_buyer_user_id.Value, hid_buyer_id.Value)
            If buy_id > 0 Then
                send_to_confirmation_page(buy_id, "pa")
            End If
            obj = Nothing
        ElseIf CDbl(txt_trad_proxy_partial_auto_accept_price.Text.Trim()) <= CDbl(hid_bidlimit_amount.Value) Then
            buy_id = obj.save_buy_now_bid(hid_auction_id.Value, txt_trad_proxy_partial_auto_accept_price.Text.Trim(), txt_trad_proxy_partial_auto_accept_qty.Text.Trim(), "partial", hid_buyer_user_id.Value, hid_buyer_id.Value)
            If buy_id > 0 Then
                send_to_confirmation_page(buy_id, "pa")
            End If
            obj = Nothing
        Else
            send_to_confirmation_page(0, "l")
        End If
    End Sub

    Protected Sub btn_trad_proxy_buy_now_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btn_trad_proxy_buy_now.Click
        ViewState("option") = "trad_proxy_buy"
        trad_proxy_buy_now()
    End Sub

    Private Sub trad_proxy_buy_now()
        If hid_buyer_user_id.Value = "" Or hid_buyer_user_id.Value = "0" Then
            show_login("trad_proxy_buy")
            Exit Sub
        End If
        Dim obj As New Bid()
        Dim buy_id As Integer = 0
        If CommonCode.Fetch_Cookie_Shared("is_buyer") = "1" Then
            buy_id = obj.save_buy_now_bid(hid_auction_id.Value, 0, 0, "buy now", hid_buyer_user_id.Value, hid_buyer_id.Value)
            obj = Nothing
            If buy_id > 0 Then
                send_to_confirmation_page(buy_id, "bu")
            End If
        ElseIf CDbl(hid_bynow_amount.Value) <= CDbl(hid_bidlimit_amount.Value) Then
            buy_id = obj.save_buy_now_bid(hid_auction_id.Value, 0, 0, "buy now", hid_buyer_user_id.Value, hid_buyer_id.Value)
            obj = Nothing
            If buy_id > 0 Then
                send_to_confirmation_page(buy_id, "bu")
            End If
        Else
            send_to_confirmation_page(0, "l")
        End If
    End Sub

    Protected Sub btn_request_quote_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btn_request_quote.Click
        ViewState("option") = "quote"
        request_quote()
    End Sub

    Private Sub request_quote()
        If hid_buyer_user_id.Value = "" Or hid_buyer_user_id.Value = "0" Then
            show_login("quote")
            Exit Sub
        End If

        Try
            Dim auction_id As Integer = hid_auction_id.Value
            Dim buyer_user_id As Integer = hid_buyer_user_id.Value 'CommonCode.Fetch_Cookie_Shared("user_id")
            Dim details As String = txt_quote.Text.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")
            Dim str As String = ""
            Dim Quotation_id As Integer = 0

            Dim filename As String = ""
            If fl_quote_file.HasFile Then
                filename = fl_quote_file.FileName
                filename = buyer_user_id.ToString() & "_" & filename
            End If
            str = "Insert into tbl_auction_quotations(auction_id,buyer_user_id,bid_date,details,filename)Values(" & auction_id & "," & buyer_user_id & ",getdate(),'" & details & "','" & filename & "') select scope_identity()"
            Dim obj As New Bid()
            Quotation_id = obj.send_quotation(auction_id, txt_quote.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>"), buyer_user_id, hid_buyer_id.Value)

            If fl_quote_file.HasFile Then
                If Quotation_id > 0 Then
                    obj.upload_quotation_file(auction_id, Quotation_id, fl_quote_file, buyer_user_id)
                End If
            End If
            lbl_msg_quotation.Visible = True
            obj = Nothing
            send_to_confirmation_page(Quotation_id, "q")
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub load_auction_bidder_queries()
        If hid_auction_id.Value > 0 Then
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDatatable("select A.query_id,A.title,A.submit_date,A.buyer_id,B.company_name,isnull(case when B.state_id<>0 then (select name from tbl_master_states where state_id=B.state_id) else B.state_text end,'') as state from tbl_auction_queries A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id inner join tbl_auctions A1 WITH (NOLOCK) on A.auction_id=A1.auction_id where isnull(A1.discontinue,0)=0 and isnull(B.discontinue,0)=0 and  A.parent_query_id=0 and A.auction_id=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), 0, Request.QueryString.Get("i")) & " and A.buyer_id=" & hid_buyer_id.Value & " order by submit_date desc")
            rep_after_queries.DataSource = dt
            rep_after_queries.DataBind()
        End If
    End Sub

    Protected Sub rep_after_queries_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rep_after_queries.ItemCommand
        If e.CommandName = "send_query" Then
            Dim Rsp As String = DirectCast(e.Item.FindControl("rep_txt_message"), TextBox).Text.Trim.Replace("'", "''")
            If Rsp <> "Response" Then
                SqlHelper.ExecuteNonQuery("insert into tbl_auction_queries(auction_id,message,buyer_id,buyer_user_id,parent_query_id,submit_date)values(" & hid_auction_id.Value & ",'" & Rsp & "'," & hid_buyer_id.Value & "," & hid_buyer_user_id.Value & "," & e.CommandArgument & ",getdate())")
                load_auction_bidder_queries()
            End If
        End If
    End Sub

    Protected Sub rep_after_queries_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rep_after_queries.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim rep_inner As Repeater = DirectCast(e.Item.FindControl("rep_inner_after_queries"), Repeater)
            rep_inner.DataSource = SqlHelper.ExecuteDatatable("select A.message, A.submit_date, ISNULL(C.first_name,'') AS first_name,ISNULL(C.last_name,'') AS last_name,ISNULL(D.first_name,'') AS buyer_first_name,ISNULL(D.last_name,'') AS buyer_last_name,isnull(C.title,'') as title,isnull(D.title,'') as buyer_title,case ISNULL(A.user_id,0) when 0 then cast(0 as Bit) else cast(1 as bit) end AS is_admin,isnull(C.image_path,'') as image_path,C.user_id from tbl_auction_queries A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id left join tbl_sec_users C on A.user_id=C.user_id LEFT JOIN tbl_reg_buyer_users D ON A.buyer_user_id=D.buyer_user_id where isnull(B.discontinue,0)=0 and A.parent_query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & " order by submit_date desc")
            rep_inner.DataBind()
        End If
    End Sub
    Private Sub show_login(ByVal bid_mode As String)
        If hid_buyer_user_id.Value = "0" Or hid_buyer_user_id.Value = "" Then
            modalPopUpExtender1.Show()
            Exit Sub
        End If
    End Sub

    Protected Sub img_btnsend_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles img_btnsend.Click
        If txt_login_name.Text.Trim() <> "" And txt_login_pwd.Text.Trim() <> "" Then
            Dim str As String = "select * from vw_login_users where username='" & txt_login_name.Text.Trim() & "' and password='" & Security.EncryptionDecryption.EncryptValue(txt_login_pwd.Text.Trim()) & "'"
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDataTable(str)
            If dt.Rows.Count > 0 Then
                CommonCode.Create_Cookie_shared("user_id", dt.Rows(0)("user_id").ToString)
                hid_buyer_user_id.Value = dt.Rows(0)("user_id").ToString
                hid_buyer_id.Value = dt.Rows(0)("buyer_id").ToString
                CommonCode.Create_Cookie_shared("username", dt.Rows(0)("username").ToString)
                CommonCode.Create_Cookie_shared("buyer_id", dt.Rows(0)("buyer_id").ToString)
                CommonCode.Create_Cookie_shared("is_backend", dt.Rows(0)("is_backend").ToString)
                CommonCode.Create_Cookie_shared("is_buyer", dt.Rows(0)("is_buyer").ToString)
                str = "insert into tbl_sec_login_log (user_id,buyer_id,buyer_user_id,login_date,ip_address,browser_info,log_type) values (" & dt.Rows(0)("user_id") & "," & dt.Rows(0)("buyer_id").ToString & "," & dt.Rows(0)("user_id").ToString & ",getdate(),'" & HttpContext.Current.Request.UserHostAddress & "','" & Request.Browser.Browser.ToString & " - " & Request.Browser.Version.ToString & " - " & Request.Browser.Platform.ToString & "','Login')"
                SqlHelper.ExecuteNonQuery(str)
                Dim bid_mode As String = ViewState("option")
                Dim bid_mode_local As String = bid_mode.ToLower()
                If bid_mode = "buy_now_bid" Then
                    buy_now_bid()
                ElseIf bid_mode = "buy_now_private" Then
                    buy_now_private_offer()
                ElseIf bid_mode = "buy_now_partial" Then
                    buy_now_partial_offer()
                ElseIf bid_mode = "trad_proxy_bid" Then
                    trad_proxy_bid()
                ElseIf bid_mode = "trad_proxy_private" Then
                    trad_proxy_private_offer()
                ElseIf bid_mode = "trad_proxy_partial" Then
                    trad_proxy_partial()
                ElseIf bid_mode = "trad_proxy_buy" Then
                    trad_proxy_buy_now()
                ElseIf bid_mode = "quote" Then
                    request_quote()
                End If
            Else
                lbl_error.Visible = True
                modalPopUpExtender1.Show()
            End If
        End If
    End Sub

    Protected Sub img_btn_create_thread_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles img_btn_create_thread.Click
        Dim str As String = "INSERT into tbl_auction_queries(auction_id,title,buyer_id,buyer_user_id,parent_query_id,submit_date) " & _
        "Values(" & hid_auction_id.Value & ",'" & txt_thread_title.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'," & hid_buyer_id.Value & "," & hid_buyer_user_id.Value & ",0,getdate())"
        SqlHelper.ExecuteNonQuery(str)
        txt_thread_title.Text = ""
        load_auction_bidder_queries()
    End Sub
End Class
