﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="profiledetail.aspx.vb" Inherits="Users_profiledetail" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <!-- #include file="/pcs_js/ToolTip.inc" -->
    <script type="text/javascript">

        function open_pop_win(_path, _id) {
            w1 = window.open(_path + '?i=' + _id, '_assign', 'top=' + ((screen.height - 510) / 2) + ', left=' + ((screen.width - 800) / 2) + ', height=540, width=800,scrollbars=yes,toolbars=no,resizable=1;');
            w1.focus();
            return false;

        }
    </script>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxPanel2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel2" />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid_UserAssign" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxPanelProfile">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanelProfile" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_1_save">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_1_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_1_edit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_1_cancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--  <telerik:AjaxSetting AjaxControlID="but_2_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel2" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="but_manage_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Manage" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_manage_edit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Manage" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_manage_cancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Manage" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="top">
                <div style="float: left;">
                    <div class="pageheading">
                        <asp:Literal ID="page_heading" runat="server" Text="New Profile"></asp:Literal></div>
                </div>
                <div style="float: right; margin: 10px; padding-right: 20px;">
                    <a href="javascript:void(0);" onclick="javascript:open_help('5');">
                        <img src="/Images/help-button.gif" border="none" />
                    </a>
                </div>
            </td>
        </tr>
        <tr>
            <td align="left">
                <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="" MultiPageID="RadMultiPage1"
                    SelectedIndex="0" Align="Left">
                    <Tabs>
                        <telerik:RadTab CssClass="auctiontabSelected">
                            <TabTemplate>
                                <span>
                                    <%= IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), "Add Profile Details", "Edit Profile")%></span>
                            </TabTemplate>
                        </telerik:RadTab>
                        <telerik:RadTab CssClass="auctiontab">
                            <TabTemplate>
                                <span>User Assigned</span>
                            </TabTemplate>
                        </telerik:RadTab>
                        <telerik:RadTab CssClass="auctiontab">
                            <TabTemplate>
                                <span>Manage Profiles</span>
                            </TabTemplate>
                        </telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
            </td>
        </tr>
        <tr>
            <td class="PageTabContentEdit">
                <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" CssClass="multiPage">
                    <telerik:RadPageView ID="RadPageView1" runat="server">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td class="tdTabItem">
                                    <div class="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/profile_master_icon.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            Permission Assigned
                                        </div>
                                    </div>
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                        CssClass="TabGrid">
                                        <div style="padding-left: 5px;">
                                            <asp:HiddenField ID="hid_profie_active_modify" runat="server" Value="1" />
                                            <asp:Label ID="lbl_msg" CssClass="txt_lbl" ForeColor="Red" Text="" runat="server" /></div>
                                        <table cellpadding="0" cellspacing="2" border="0" width="850px">
                                            <tr>
                                                <td style="width: 135px; height: 30px;" class="caption">
                                                    Profile Name&nbsp;<span id="span_profile_name" runat="server" class="req_star">*</span>
                                                </td>
                                                <td style="width: 270px; height: 30px;" class="details">
                                                    <asp:TextBox ID="txt_profile_name" CssClass="inputtype" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Required_txt_profile_name" runat="server" Font-Size="10px"
                                                        ControlToValidate="txt_profile_name" ValidationGroup="_profile" Display="Dynamic"
                                                        ErrorMessage="<br>Name Required"></asp:RequiredFieldValidator>
                                                        <div onmouseover="ddrivetip('');" onmouseout="hideddrivetip();">
                                                    <asp:Label ID="lbl_profile_name" runat="server"  /></div>
                                                </td>
                                                <td style="width: 135px;" class="caption">
                                                    Is Active
                                                </td>
                                                <td style="width: 270px;" class="details">
                                                    <asp:Label ID="lbl_is_active" runat="server"></asp:Label>
                                                    <asp:CheckBox ID="chk_is_active" runat="server" Checked="true" />
                                                </td>
                                            </tr> <tr>
                                                <td style="width: 135px; height: 30px;" class="caption">
                                                    Profile Code&nbsp;<span id="span_profile_code" runat="server" class="req_star">*</span>
                                                </td>
                                                <td style="width: 270px; height: 30px;" class="details">
                                                    <asp:TextBox ID="txt_profile_code" CssClass="inputtype" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Required_txt_profile_code" runat="server" Font-Size="10px"
                                                        ControlToValidate="txt_profile_code" ValidationGroup="_profile" Display="Dynamic"
                                                        ErrorMessage="<br>Code Required"></asp:RequiredFieldValidator>
                                                        <div onmouseover="ddrivetip('');" onmouseout="hideddrivetip();">
                                                    <asp:Label ID="lbl_profile_code" runat="server"  /></div>
                                                </td>
                                                <td style="width: 135px;" class="caption">
                                                   
                                                </td>
                                                <td style="width: 270px;" class="details">
                                                  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption" valign="top">
                                                    Permissions
                                                </td>
                                                <td class="details" valign="top">
                                                    <asp:Label ID="ltr_permission" runat="server" />
                                                    <telerik:RadGrid ID="dg_permission" runat="server" PageSize="100" AllowPaging="false"
                                                        GroupingEnabled="true" AutoGenerateColumns="False" Skin="Simple" EnableLinqExpressions="false"
                                                        ShowHeader="false">
                                                        <MasterTableView DataKeyNames="permission_id" HorizontalAlign="NotSet" ShowHeader="false"
                                                            CommandItemDisplay="None" AutoGenerateColumns="False" ItemStyle-Height="20" AlternatingItemStyle-Height="20">
                                                            <GroupByExpressions>
                                                                <telerik:GridGroupByExpression>
                                                                    <GroupByFields>
                                                                        <telerik:GridGroupByField HeaderText="Profile" FieldName="Name" />
                                                                    </GroupByFields>
                                                                    <SelectFields>
                                                                        <telerik:GridGroupByField HeaderText="Profile" FieldName="Name" />
                                                                    </SelectFields>
                                                                </telerik:GridGroupByExpression>
                                                            </GroupByExpressions>
                                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                            <Columns>
                                                                <telerik:GridTemplateColumn ItemStyle-BorderStyle="None">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="img_permission" runat="server" />
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn ItemStyle-BorderStyle="None">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chk_permission" runat="server" Text="" Checked="false" />
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn>
                                                                    <ItemTemplate>
                                                                        <div onmouseover="ddrivetip('<%#Eval("description") %>');" onmouseout="hideddrivetip();">
                                                                            <%#Eval("description") %></div>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                        <ClientSettings AllowDragToGroup="true">
                                                            <Selecting AllowRowSelect="True"></Selecting>
                                                        </ClientSettings>
                                                        <GroupingSettings ShowUnGroupButton="true" />
                                                    </telerik:RadGrid>
                                                </td>
                                                <td class="caption" valign="top">
                                                    Description
                                                </td>
                                                <td class="details" valign="top">
                                                    <asp:TextBox ID="txt_profile_description" TextMode="MultiLine" Rows="10" Columns="28"
                                                        CssClass="TextBox" runat="server"></asp:TextBox>
                                                    <asp:Label ID="lbl_profile_description" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </telerik:RadAjaxPanel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel11" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                                        <asp:Panel ID="pnl_Edit_User_Buttons" runat="server">
                                            <div class="addButton">
                                                <asp:ImageButton ID="but_1_update" ValidationGroup="_profile" runat="server" AlternateText="Update"
                                                    ImageUrl="/images/update.gif" />
                                                <asp:ImageButton ID="but_1_edit" CausesValidation="false" runat="server" AlternateText="Edit"
                                                    ImageUrl="/images/edit.gif" />
                                                <asp:ImageButton ID="but_1_save" ValidationGroup="_profile" runat="server" AlternateText="Save"
                                                    ImageUrl="/images/save.gif" />
                                            </div>
                                            <div class="cancelButton" id="div_1_cancel" runat="server">
                                                <asp:ImageButton ID="but_1_cancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                    ImageUrl="/images/cancel.gif" />
                                            </div>
                                        </asp:Panel>
                                    </telerik:RadAjaxPanel>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView2" runat="server" CssClass="pageViewEducation">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td class="tdTabItem">
                                    <div class="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/user_assigned_icon.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            User Assigned
                                        </div>
                                    </div>
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                        CssClass="TabGrid">
                                        <div class="TabGrid" style="padding: 10px;">
                                            <%--<div class="dv_md_heading">
                                            <div style="float: left;">
                                                &nbsp;User Assigned
                                            </div>
                                            <div style="float: right;">
                                                <img src="/Images/Q.mark.png" alt="help?" />&nbsp;
                                            </div>
                                        </div>--%>
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <%--  User Assigned to
                                                        <asp:Label ID="lbl_profile" runat="server" Text=""></asp:Label>
                                                        Profile--%>
                                                        <asp:Label ID="lbl_profile" runat="server" Text="" CssClass="error" EnableViewState="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <telerik:RadGrid ID="RadGrid_UserAssign" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                                                            ShowGroupPanel="true" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True"
                                                            OnNeedDataSource="RadGrid_UserAssign_NeedDataSource" AutoGenerateColumns="False"
                                                            Skin="Simple" AllowFilteringByColumn="True" AllowSorting="true">
                                                            <PagerStyle Mode="NextPrevAndNumeric" />
                                                            <MasterTableView DataKeyNames="user_id" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                                                                AllowFilteringByColumn="True" ItemStyle-Height="40" AlternatingItemStyle-Height="40">
                                                                <NoRecordsTemplate>
                                                                    No users to display
                                                                </NoRecordsTemplate>
                                                                <SortExpressions>
                                                                    <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                                                                </SortExpressions>
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="user_id" HeaderText="user_id" SortExpression="user_id"
                                                                        UniqueName="user_id" FilterControlWidth="50" Visible="false">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Name" SortExpression="name" UniqueName="name"
                                                                        DataField="name" FilterControlWidth="150" GroupByExpression="name [Profile Name] group by name">
                                                                        <ItemTemplate>
                                                                            <%# Eval("name")%>
                                                                            <a href="javascript:void(0);" onclick="return open_pop_win('/Users/Userdetail.aspx','<%# Eval("user_id") %>');">
                                                                                <br />
                                                                                <%# Eval("title")%></a>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Email Address" SortExpression="email" UniqueName="email"
                                                                        DataField="email" FilterControlWidth="190" GroupByExpression="email [Email Address] group by email">
                                                                        <ItemTemplate>
                                                                            <a href="mailto:<%# Eval("email")%>">
                                                                                <%# Eval("email")%></a>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridBoundColumn DataField="phone" HeaderText="Phone" SortExpression="phone"
                                                                        UniqueName="phone" FilterControlWidth="100">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridTemplateColumn UniqueName="is_map" AllowFiltering="true" HeaderStyle-Width="90"
                                                                        Groupable="false" DataField="is_map">
                                                                        <HeaderTemplate>
                                                                            Select All
                                                                            <asp:CheckBox ID="chkHeader" runat="server" AutoPostBack="true" OnCheckedChanged="CheckAll" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chk1" runat="server" Checked='<%#is_map(Eval("is_map")) %>' />
                                                                        </ItemTemplate>
                                                                        <FilterTemplate>
                                                                            <telerik:RadComboBox ID="RadComboBo_Chk_is_active" runat="server" OnClientSelectedIndexChanged="ActiveIndexChanged"
                                                                                Width="120px" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("is_map").CurrentFilterValue %>'>
                                                                                <Items>
                                                                                    <telerik:RadComboBoxItem Text="All" Value="" />
                                                                                    <telerik:RadComboBoxItem Text="Assigned" Value="1" />
                                                                                    <telerik:RadComboBoxItem Text="Unassigned" Value="0" />
                                                                                </Items>
                                                                            </telerik:RadComboBox>
                                                                            <telerik:RadScriptBlock ID="RadScriptBlock_is_active" runat="server">
                                                                                <script type="text/javascript">
                                                                                    function ActiveIndexChanged(sender, args) {
                                                                                        var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                                                        tableView.filter("is_map", args.get_item().get_value(), "EqualTo");

                                                                                    }
                                                                                </script>
                                                                            </telerik:RadScriptBlock>
                                                                        </FilterTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                            <ClientSettings AllowDragToGroup="true">
                                                                <Selecting AllowRowSelect="True"></Selecting>
                                                            </ClientSettings>
                                                            <GroupingSettings ShowUnGroupButton="true" />
                                                        </telerik:RadGrid>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 0px 0px 10px 0px; text-align: right;">
                                                        <div class="addButton">
                                                            <asp:ImageButton ID="but_2_update" runat="server" AlternateText="Update" ImageUrl="/images/update.gif" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </telerik:RadAjaxPanel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <%--<img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                            <asp:Panel ID="Panel2_Content2button" runat="server" CssClass="collapsePanel">
                            </asp:Panel>--%>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView3" runat="server">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td class="tdTabItem">
                                    <div class="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/manage_profile_icon.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            Manage Profiles
                                        </div>
                                    </div>
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel_Manage" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                        CssClass="TabGrid" EnableAJAX="true">
                                        <div class="TabGrid" style="padding: 10px;">
                                            <asp:Label ID="lbl_man_msg" runat="server" ForeColor="Red"></asp:Label>
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <telerik:RadGrid ID="RadGrid_Manage_All" runat="server" PageSize="10" AllowPaging="false"
                                                            AutoGenerateColumns="false" Skin="Simple" AllowFilteringByColumn="false">
                                                            <PagerStyle Mode="NextPrevAndNumeric" />
                                                            <MasterTableView DataKeyNames="permission_id" HorizontalAlign="NotSet" AutoGenerateColumns="false"
                                                                AllowFilteringByColumn="false" ItemStyle-Height="40" AlternatingItemStyle-Height="40"
                                                                AllowSorting="true">
                                                                <NoRecordsTemplate>
                                                                    No permission to display
                                                                </NoRecordsTemplate>
                                                                <GroupByExpressions>
                                                                    <telerik:GridGroupByExpression>
                                                                        <GroupByFields>
                                                                            <telerik:GridGroupByField HeaderText="Profile" FieldName="Name" />
                                                                        </GroupByFields>
                                                                        <SelectFields>
                                                                            <telerik:GridGroupByField HeaderText="Profile" FieldName="Name" />
                                                                        </SelectFields>
                                                                    </telerik:GridGroupByExpression>
                                                                </GroupByExpressions>
                                                                <SortExpressions>
                                                                    <telerik:GridSortExpression FieldName="description" SortOrder="Ascending" />
                                                                </SortExpressions>
                                                                <Columns>
                                                                </Columns>
                                                            </MasterTableView>
                                                            <ClientSettings>
                                                                <Selecting AllowRowSelect="True"></Selecting>
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                        <telerik:RadGrid ID="RadGrid_Manage_Edit" GridLines="None" runat="server" PageSize="10"
                                                            AllowPaging="false" AutoGenerateColumns="false" Skin="Simple" AllowFilteringByColumn="false">
                                                            <PagerStyle Mode="NextPrevAndNumeric" />
                                                            <MasterTableView DataKeyNames="permission_id" HorizontalAlign="NotSet" AutoGenerateColumns="false"
                                                                AllowFilteringByColumn="false" ItemStyle-Height="40" AlternatingItemStyle-Height="40"
                                                                AllowSorting="true">
                                                                <NoRecordsTemplate>
                                                                    No permission to display
                                                                </NoRecordsTemplate>
                                                                <GroupByExpressions>
                                                                    <telerik:GridGroupByExpression>
                                                                        <GroupByFields>
                                                                            <telerik:GridGroupByField HeaderText="Profile" FieldName="Name" />
                                                                        </GroupByFields>
                                                                        <SelectFields>
                                                                            <telerik:GridGroupByField HeaderText="Profile" FieldName="Name" />
                                                                        </SelectFields>
                                                                    </telerik:GridGroupByExpression>
                                                                </GroupByExpressions>
                                                            </MasterTableView>
                                                            <ClientSettings>
                                                                <Selecting AllowRowSelect="True"></Selecting>
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:SqlDataSource ID="SqlData_Pro_Manage" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                                            ProviderName="System.Data.SqlClient" SelectCommand="sel_profile_manage" SelectCommandType="StoredProcedure"
                                                            runat="server"></asp:SqlDataSource>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </telerik:RadAjaxPanel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel_Manage_Edit" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                        EnableAJAX="true">
                                        <asp:Panel ID="pnl_manage_profile_buttons" runat="server">
                                            <div class="addButton">
                                                <asp:ImageButton ID="but_manage_update" ValidationGroup="_manage" runat="server"
                                                    AlternateText="Update" ImageUrl="/images/update.gif" />
                                                <asp:ImageButton ID="but_manage_edit" CausesValidation="false" runat="server" AlternateText="Edit"
                                                    ImageUrl="/images/edit.gif" />
                                            </div>
                                            <div class="cancelButton" id="dv_manage_cancel" runat="server">
                                                <asp:ImageButton ID="but_manage_cancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                    ImageUrl="/images/cancel.gif" />
                                            </div>
                                        </asp:Panel>
                                    </telerik:RadAjaxPanel>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="sqlDataSource_manage" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    SelectCommandType="StoredProcedure" ProviderName="System.Data.SqlClient" SelectCommand="sel_profile_manage" />
            </td>
        </tr>
    </table>
  
</asp:Content>
