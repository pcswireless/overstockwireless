﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="Userdetail.aspx.vb" Inherits="Users_Userdetail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <asp:HiddenField ID="hid_user_id" runat="server" Value="0" />
    <script type="text/javascript">

        function open_buyer_assign(sid) {
            window.open('salerep_bidders.aspx?t=u&i=' + sid, '_salerep', 'top=' + ((screen.height - 480) / 2) + ', left=' + ((screen.width - 840) / 2) + ', height=620, width=820,scrollbars=yes,toolbars=no,resizable=0;');
            window.focus();
            return false;

        }
        function open_pass_win(_path, _pwd) {
            w1 = window.open(_path + '?' + _pwd, '_paswd', 'top=' + ((screen.height - 350) / 2) + ', left=' + ((screen.width - 500) / 2) + ', height=350, width=500,scrollbars=yes,toolbars=no,resizable=1;');
            w1.focus();
            return false;

        }
        
    </script>
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            //On insert and update buttons click temporarily disables ajax to perform upload actions
            function Cancel_Ajax(sender, args) {
                args.set_enableAjax(false);

            }

            function BasicStart(e, sender) {
                var upload1 = $find(window['fileupload_picture']);
                if (upload1.getFileInputs()[0].value != "") {
                    sender.set_enableAjax(false);
                }
            }

            function popup_PostBackFunction() {

                var tabStrip = $find('<%=RadTabStrip1.ClientID%>');
                if (tabStrip != null) {

                    tabStrip.set_selectedIndex(1);
                }
                window.location.reload(true);
            }

            //]]>
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Simple">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_edit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_save">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_cancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Skin="Simple" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div style="float: left;">
                    <div class="pageheading">
                        <asp:Literal ID="page_heading" runat="server" Text="New User"></asp:Literal></div>
                </div>
                <div style="float: right; margin: 10px; padding-right: 20px;">
                    <a href="javascript:void(0);" onclick="javascript:open_help('4');">
                        <img src="/Images/help-button.gif" border="none" />
                    </a>
                </div>
            </td>
        </tr>
        <tr>
            <td align="left">
                <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="" MultiPageID="RadMultiPage1"
                    SelectedIndex="0" Align="Left">
                    <Tabs>
                        <telerik:RadTab CssClass="auctiontab" SelectedCssClass="auctiontabSelected">
                            <TabTemplate>
                                <span>
                                    <%= IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), "Add User Details", "Edit User")%></span>
                            </TabTemplate>
                        </telerik:RadTab>
                        <telerik:RadTab CssClass="auctiontab" SelectedCssClass="auctiontabSelected">
                            <TabTemplate>
                                <span>Bidder Assigned</span>
                            </TabTemplate>
                        </telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
            </td>
        </tr>
        <tr>
            <td class="PageTabContentEdit">
                <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" CssClass="multiPage">
                    <telerik:RadPageView ID="RadPageView1" runat="server">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td class="tdTabItem">
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                        ClientEvents-OnRequestStart="BasicStart" CssClass="TabGrid">
                                        <div style="padding-left: 5px;">
                                            <asp:Label ID="lbl_msg" runat="server" Text="" ForeColor="Red" EnableViewState="false"></asp:Label>
                                        </div>
                                        <table cellpadding="0" cellspacing="2" border="0" width="838">
                                            <tr>
                                                <td colspan="4" class="dv_md_sub_heading">
                                                    Personal Details
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 145px;" class="caption">
                                                    First Name&nbsp;<span id="span_first_name" runat="server" class="req_star">*</span>
                                                </td>
                                                <td style="width: 270px;" class="details">
                                                    <asp:TextBox ID="txt_first_name" ValidationGroup="a" runat="server" CssClass="inputtype"
                                                        MaxLength="20"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="req_first_name" runat="server" ControlToValidate="txt_first_name"
                                                        ValidationGroup="a" Display="Dynamic" ErrorMessage="<br>First Name Required"></asp:RequiredFieldValidator>
                                                    <asp:Label ID="lbl_first_name" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td style="width: 145px;" class="caption">
                                                    Last Name
                                                </td>
                                                <td style="width: 270px;" class="details">
                                                    <asp:TextBox ID="txt_last_name" ValidationGroup="a" runat="server" CssClass="inputtype"
                                                        MaxLength="20"></asp:TextBox>
                                                    <asp:Label ID="lbl_last_name" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Title
                                                </td>
                                                <td class="details">
                                                    <asp:DropDownList ID="ddl_title" runat="server" DataTextField="name" DataValueField="name"
                                                        CssClass="drop_down">
                                                    </asp:DropDownList>
                                                    <asp:Label ID="lbl_title" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="dv_md_sub_heading">
                                                    Contact Details
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Street&nbsp;<span id="span_address1" runat="server" class="req_star">*</span>
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_address1" runat="server" TextMode="MultiLine" CssClass="inputtype"
                                                        Height="35px"></asp:TextBox>
                                                    <asp:Label ID="lbl_address1" runat="server" Text=""></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfv_address1" runat="server" ControlToValidate="txt_address1"
                                                        ValidationGroup="a" Display="Dynamic" ErrorMessage="<br />Street Required" CssClass="error"></asp:RequiredFieldValidator>
                                                </td>
                                                <td class="caption">
                                                    Street Address 2
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_address2" runat="server" TextMode="MultiLine" CssClass="inputtype"
                                                        Height="35px"></asp:TextBox>
                                                    <asp:Label ID="lbl_address2" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    City&nbsp;<span id="span_city" runat="server" class="req_star">*</span>
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_city" runat="server" ValidationGroup="a" CssClass="inputtype"
                                                        MaxLength="50"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Required_city" ControlToValidate="txt_city" runat="server"
                                                        ErrorMessage="<br>City Required" Display="Dynamic" ValidationGroup="a"></asp:RequiredFieldValidator>
                                                    <asp:Label ID="lbl_city" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td class="caption">
                                                    State/Province&nbsp;<span id="span_state" runat="server" class="req_star">*</span>
                                                </td>
                                                <td class="details">
                                                    <asp:DropDownList ID="ddl_state" runat="server" DataTextField="name" DataValueField="state_id"
                                                        CssClass="drop_down">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="Required_state" ValidationGroup="a" runat="server"
                                                        ControlToValidate="ddl_state" InitialValue="0" Display="Dynamic" ErrorMessage="<br>State Required"></asp:RequiredFieldValidator>
                                                    <asp:Label ID="lbl_state" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Postal Code&nbsp;<span id="span_zip" runat="server" class="req_star">*</span>
                                                </td>
                                                <td valign="top" class="details">
                                                    <asp:TextBox ID="txt_zip" runat="server" ValidationGroup="a" CssClass="inputtype"
                                                        MaxLength="15"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_zip" ValidationGroup="a"
                                                        ErrorMessage="<br>Postal Code Required" ID="Required_zip" Display="Dynamic"> </asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="Regular_zip" runat="server" ControlToValidate="txt_zip"
                                                        ValidationGroup="a" ErrorMessage="<br>Invalid Postal Code" ValidationExpression="^[a-zA-Z0-9 ]*$"
                                                        Display="Dynamic"></asp:RegularExpressionValidator>
                                                    <asp:Label ID="lbl_zip" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Phone 1&nbsp;<span id="span_mobile" runat="server" class="req_star">*</span>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lbl_mobile" runat="server" Text=""></asp:Label>
                                                    <asp:TextBox ID="txt_mobile" ValidationGroup="a" runat="server" CssClass="inputtype"
                                                        MaxLength="10"></asp:TextBox>
                                                    <br />
                                                    <asp:RadioButton ID="rdo_ph_1_landline" runat="server" Text="Landline" GroupName="p1"
                                                        Checked="true" /><asp:RadioButton ID="rdo_ph_1_mobile" runat="server" Text="Mobile"
                                                            GroupName="p1" />
                                                    <asp:RequiredFieldValidator ValidationGroup="a" ID="Required_mobile" runat="server"
                                                        ControlToValidate="txt_mobile" Display="Dynamic" ErrorMessage="<br>Phone 1 Required"></asp:RequiredFieldValidator>
                                                    <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" 
                                                        ControlToValidate="txt_mobile" Display="Dynamic"  ErrorMessage="<br>Phone Number should be minimum 10 digit" ValidationGroup="a"
                                                        ValidationExpression="^[0-9]{10,15}$"></asp:RegularExpressionValidator>--%>
                                                </td>
                                                <td class="caption">
                                                    Phone Ext
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lbl_phone_ext" runat="server"></asp:Label>
                                                    <asp:TextBox ID="txt_phone_ext" runat="server" CssClass="inputtype" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Phone 2
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lbl_phone" runat="server" Text=""></asp:Label>
                                                    <asp:TextBox ID="txt_phone" runat="server" ValidationGroup="a" CssClass="inputtype"
                                                        MaxLength="15"></asp:TextBox>
                                                    <br />
                                                    <asp:RadioButton ID="rdo_ph_2_landline" runat="server" Text="Landline" GroupName="p2"
                                                        Checked="true" /><asp:RadioButton ID="rdo_ph_2_mobile" runat="server" Text="Mobile"
                                                            GroupName="p2" />
                                                    <%--   <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="a"
                                                        ControlToValidate="txt_phone" Display="Dynamic"  ErrorMessage="<br>Phone Number should be minimum 10 digit"
                                                        ValidationExpression="^[0-9]{10,15}$"></asp:RegularExpressionValidator>--%>
                                                </td>
                                                <td class="caption">
                                                    Email&nbsp;<span id="span_email" runat="server" class="req_star">*</span>
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_email" ValidationGroup="a" runat="server" CssClass="inputtype"
                                                        MaxLength="50"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ValidationGroup="a" ID="Required_email" runat="server"
                                                        ControlToValidate="txt_email" Display="Dynamic" ErrorMessage="<br>Email Required"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ValidationGroup="a" ID="Regular_email" runat="server"
                                                        ControlToValidate="txt_email" Display="Dynamic" ErrorMessage="<br>Invalid Email"
                                                        ValidationExpression="^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$"></asp:RegularExpressionValidator>
                                                    <asp:Label ID="lbl_email" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="dv_md_sub_heading">
                                                    Account setup
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    User name&nbsp;<span id="span_username" runat="server" class="req_star">*</span>
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_username" runat="server" ValidationGroup="a" CssClass="inputtypePWD"
                                                        MaxLength="50"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ValidationGroup="a" ID="Required_username" ControlToValidate="txt_username"
                                                        runat="server" Display="Dynamic" ErrorMessage="<br>User Name Required"></asp:RequiredFieldValidator>
                                                    <asp:Label ID="lbl_username" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td class="caption">
                                                    Password&nbsp;<span id="span_password" runat="server" class="req_star">*</span>
                                                </td>
                                                <td class="details">
                                                    <asp:Literal ID="lit_change_password" runat="server"></asp:Literal>
                                                    <asp:TextBox ID="txt_password" runat="server" TextMode="Password" CssClass="inputtypePWD"
                                                        ValidationGroup="a" MaxLength="20"> </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Required_password" ControlToValidate="txt_password"
                                                        runat="server" ValidationGroup="a" ErrorMessage="<br>Password Required" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="Regular_password" runat="server" ControlToValidate="txt_password"
                                                        ValidationGroup="a" ErrorMessage="<br>Alphanumeric atleast 6 chars." ValidationExpression="(?=.*\d)(?=.*[a-zA-Z]).{6,}"
                                                        Display="Dynamic"></asp:RegularExpressionValidator>
                                                    <asp:Label ID="lbl_password" runat="server" Text="******"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trRetypePassword" runat="server">
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td class="caption">
                                                    <asp:Literal ID="lit_retype_password_caption" runat="server" Text="Retype Password"></asp:Literal><span
                                                        class="req_star">*</span>
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_retype_password" ValidationGroup="a" TextMode="Password" runat="server"
                                                        CssClass="inputtypePWD" MaxLength="20"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="Required_retype_password" ControlToValidate="txt_retype_password"
                                                        ValidationGroup="a" runat="server" ErrorMessage="<br>Retype Password Required"
                                                        Display="Dynamic"></asp:RequiredFieldValidator><asp:CompareValidator ID="com_pass"
                                                            runat="server" ErrorMessage="<br>Confirm Password not Match !" ValidationGroup="a"
                                                            ControlToCompare="txt_password" ControlToValidate="txt_retype_password" Operator="Equal"
                                                            Display="Dynamic"></asp:CompareValidator>
                                                    <asp:Label ID="lbl_retype_password" runat="server" Text="******"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                </td>
                                                <td class="details">
                                                    <asp:CheckBox ID="chk_ip" runat="server" />
                                                    <asp:Label ID="lbl_ip" runat="server" />
                                                    Can access the System Outside Office
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="dv_md_sub_heading">
                                                    Other Details
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Companies
                                                </td>
                                                <td valign="top" class="details">
                                                    <asp:CheckBoxList ID="chklst_companies" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                                        DataTextField="company_name" DataValueField="seller_id" Width="100%" CellPadding="0"
                                                        CellSpacing="0" BorderWidth="0">
                                                    </asp:CheckBoxList>
                                                    <asp:Label ID="lbl_companies" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td class="caption">
                                                    Profiles
                                                </td>
                                                <td valign="top" class="details">
                                                    <asp:CheckBoxList ID="chklst_profiles" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                                        DataTextField="name" DataValueField="profile_id" Width="100%" CellPadding="0"
                                                        CellSpacing="0" BorderWidth="0">
                                                    </asp:CheckBoxList>
                                                    <asp:Label ID="lbl_profiles" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Picture
                                                </td>
                                                <td valign="top" class="details">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <a id="a_IMG_Image_1" runat="server" href="javascript:void(0);" target="_blank" style="font-family: Arial;
                                                                    font-size: 10pt;">
                                                                    <div runat="server" onmouseout="hide_tip_new();" style="cursor: hand;" id="DIV_IMG_Default_Logo1_Img">
                                                                        <asp:Image BorderWidth="1" BorderColor="Black" ID="img_user" Width="60" Height="72"
                                                                            ImageUrl="~/Images/buyer_icon.gif" runat="server"></asp:Image>
                                                                    </div>
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <asp:FileUpload ID="fileupload_picture" runat="server" CssClass="TextBox" Width="148"
                                                                    size="6" />
                                                                <asp:RegularExpressionValidator ID="revImage2" ControlToValidate="fileupload_picture"
                                                                    ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G))$"
                                                                    ValidationGroup="a" Text="*" runat="server" /><br />
                                                                <asp:CheckBox ID="CHK_Image" Text="Remove" runat="server" Visible="false" />&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="caption">
                                                    Is Active<br />
                                                    <br />
                                                    <div id="div_super_admin" runat="server" visible="false">
                                                        Is Super Admin
                                                    </div>
                                                </td>
                                                <td class="details">
                                                    <asp:CheckBox ID="chk_active" runat="server" />
                                                    <asp:Label ID="lbl_active" runat="server" />
                                                    <br />
                                                    <br />
                                                    <div style="margin-top: -5px;" id="div_is_super_admin_check" runat="server" visible="false">
                                                        <asp:Label ID="lbl_is_super_admin" runat="server"></asp:Label>
                                                        <asp:CheckBox ID="chk_is_super_admin" runat="server" Checked="false" /></div>
                                                    <asp:HiddenField ID="hid_is_active" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </telerik:RadAjaxPanel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                    <asp:Panel ID="Panel1_Content1button" runat="server" CssClass="collapsePanel">
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel11" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                            ClientEvents-OnRequestStart="BasicStart">
                                            <asp:Panel ID="Panel1_Content1button_per" runat="server">
                                                <div class="addButton">
                                                    <asp:ImageButton ID="img_button_edit" ImageUrl="~/Images/edit.gif" runat="server" />
                                                    <asp:ImageButton ID="img_button_save" ValidationGroup="a" ImageUrl="~/Images/save.gif"
                                                        runat="server" />
                                                    <asp:ImageButton ID="img_button_update" ValidationGroup="a" ImageUrl="~/Images/update.gif"
                                                        runat="server" />
                                                </div>
                                                <div class="cancelButton" id="div_basic_cancel" runat="server">
                                                    <asp:ImageButton ID="img_button_cancel" ImageUrl="~/Images/cancel.gif" runat="server" />
                                                </div>
                                            </asp:Panel>
                                        </telerik:RadAjaxPanel>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView2" runat="server" CssClass="pageViewEducation">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td class="tdTabItem">
                                    <div class="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/bidders_icon.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            Bidder Assigned
                                        </div>
                                    </div>
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                        CssClass="TabGrid">
                                        <div class="TabGrid" style="padding: 10px;">
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td style="font-size: 11px; font-weight: bold; padding-left: 15px;">
                                                        &nbsp;Selected Bidders
                                                    </td>
                                                    <td class="dv_md_sub_heading" style="padding-right: 10px; text-align: right;" id="div_assign_bidders"
                                                        runat="server">
                                                        <a runat="server" id="a_bid_assign" style="text-decoration: none; cursor: pointer;">
                                                            <img src="/images/assignbidders.gif" alt="ASSIGN BIDDERS" style="border: none;" /></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" class="sub_details">
                                                        <telerik:RadGrid ID="RadGrid_Bidder" DataSourceID="RadGrid_Bidder_SqlDataSource"
                                                            Skin="Vista" runat="server" AutoGenerateColumns="False" AllowSorting="True" AllowMultiRowSelection="False"
                                                            AllowPaging="True" PageSize="10" AllowAutomaticDeletes="true" AllowFilteringByColumn="true">
                                                            <PagerStyle Mode="NextPrevAndNumeric" />
                                                            <MasterTableView DataSourceID="RadGrid_Bidder_SqlDataSource" DataKeyNames="mapping_id"
                                                                AllowMultiColumnSorting="True" ItemStyle-Height="40" AlternatingItemStyle-Height="40"
                                                                AllowFilteringByColumn="True">
                                                                <CommandItemStyle BackColor="#d6d6d6" />
                                                                <NoRecordsTemplate>
                                                                    No bidder selected to display
                                                                </NoRecordsTemplate>
                                                                <SortExpressions>
                                                                    <telerik:GridSortExpression FieldName="company_name" SortOrder="Ascending" />
                                                                </SortExpressions>
                                                                <Columns>
                                                                    <telerik:GridTemplateColumn HeaderText="Company Name" DataField="company_name" UniqueName="company_name"
                                                                        HeaderStyle-Width="250" ItemStyle-Width="250" SortExpression="company_name" FilterControlWidth="120"
                                                                        GroupByExpression="company_name [Company Name] Group By company_name">
                                                                        <ItemTemplate>
                                                                            <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');">
                                                                                <%# Eval("company_name")%>
                                                                            </a>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridBoundColumn SortExpression="name" HeaderText="Industry" HeaderButtonType="TextButton"
                                                                        DataField="name" UniqueName="name">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridButtonColumn CommandName="Delete" UniqueName="mapping_id" DataTextField="mapping_id"
                                                                        ConfirmText="Are you sure you want to delete this buyer?" ImageUrl="/Images/delete_grid.gif"
                                                                        HeaderStyle-Width="35px" ButtonType="ImageButton">
                                                                        <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                                    </telerik:GridButtonColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                            <ClientSettings>
                                                                <Selecting AllowRowSelect="True"></Selecting>
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                        <asp:SqlDataSource ID="RadGrid_Bidder_SqlDataSource" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                                            ProviderName="System.Data.SqlClient" SelectCommand="SELECT A.mapping_id,B.buyer_id, dbo.fun_fetch_buyer_industries(B.buyer_id) as name, B.company_name FROM tbl_reg_sale_rep_buyer_mapping A INNER JOIN tbl_reg_buyers B ON A.buyer_id = B.buyer_id where B.status_id=2 and A.user_id=@user_id"
                                                            runat="server" DeleteCommand="delete from tbl_reg_sale_rep_buyer_mapping where mapping_id=@mapping_id">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="hid_user_id" Type="Int32" PropertyName="Value" Name="user_id" />
                                                            </SelectParameters>
                                                            <DeleteParameters>
                                                                <asp:Parameter Name="mapping_id" Type="Int32" />
                                                            </DeleteParameters>
                                                        </asp:SqlDataSource>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </telerik:RadAjaxPanel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
</asp:Content>
