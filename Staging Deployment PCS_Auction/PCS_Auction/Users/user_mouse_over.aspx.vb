﻿
Partial Class Users_user_mouse_over
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.QueryString.Get("i") <> Nothing AndAlso Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDataTable("SELECT A.user_id,(A.first_name+' '+A.last_name) as name,isnull(A.mobile,'') as mobile,ISNULL(A.phone,'') As phone,isnull(image_path,'') as image_path, isnull(A.email,'') as email,isnull(A.city,'') as city,isnull(A.address1,'') as address1,isnull(A.address2,'') as address2, isnull(A.title,'') as title,isnull(A.zip,'') as zip,isnull(A.is_phone1_mobile,0) as is_phone1_mobile,isnull(S.name,'') as state, ISNULL(C.code,'') As country FROM tbl_sec_users A left join tbl_master_states S on A.state_id=S.state_id left join tbl_master_countries C ON S.country_id=C.country_id where A.user_id=" & Request.QueryString.Get("i") & "")
            If dt.Rows.Count > 0 Then
                Dim strPhone As String = ""
                If dt.Rows(0)("phone") <> "" Then
                    strPhone = "<br /><br />(P) " & dt.Rows(0)("phone")
                End If
                If dt.Rows(0)("mobile") <> "" Then
                    If strPhone <> "" Then
                        strPhone = strPhone & ", " & dt.Rows(0)("mobile")
                    Else
                        strPhone = "<br /><br />(P) " & dt.Rows(0)("mobile")
                    End If
                End If
               
                If dt.Rows(0)("email") <> "" Then
                    strPhone = strPhone & "<br />(E) " & dt.Rows(0)("email")
                End If

                img_image.ImageUrl = IIf(dt.Rows(0)("image_path").ToString = "", "/images/imagenotavailable.gif", "/Upload/Users/" & dt.Rows(0)("user_id") & "/" & dt.Rows(0)("image_path"))
                lbl_user.Text = "<b>" & dt.Rows(0)("name") & "</b>" & IIf(dt.Rows(0)("title") <> "", "<br>" & dt.Rows(0)("title"), "") & "<br>" & dt.Rows(0)("address1") & IIf(dt.Rows(0)("address2") <> "", "<br>" & dt.Rows(0)("address2"), "") & "<br>" & dt.Rows(0)("city") & "<br />" & dt.Rows(0)("state").ToString().Trim() & " - " & dt.Rows(0)("zip") & "<br />" & dt.Rows(0)("country") & strPhone
            Else
                img_image.Visible = False
                lbl_user.Text = "No record for this user."
            End If
        Else
            img_image.Visible = False
            lbl_user.Text = "No record for this user."
        End If
        'dv_load.Visible = False

    End Sub
End Class
