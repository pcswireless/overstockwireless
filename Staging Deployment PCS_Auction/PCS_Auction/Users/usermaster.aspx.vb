﻿Imports Telerik.Web.UI

Partial Class Users_usermaster
    Inherits System.Web.UI.Page
    Private gridMessage1 As String = Nothing, gridMessage2 As String = Nothing, gridMessage3 As String = Nothing
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Private Sub setPermission()

        If CommonCode.is_super_admin() Then
            div_new_user.Visible = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
        Else
            div_new_user.Visible = False

            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 14
                        div_new_user.Visible = True
                    Case 35
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
                End Select
            Next
            dt = Nothing


        End If
    End Sub


    Protected Sub RadGrid1_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid1.NeedDataSource
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            Dim qry As String = ""
            If CommonCode.is_admin_user Then
                qry = "select [user_id],([first_name] + ' ' + last_name) AS name,[Email],title,[mobile],isnull(is_active,0) as is_active,isnull(is_super_admin,0) as is_super_admin,isnull(image_path,'') as image_path,isnull(username,'') as username from [tbl_sec_users] where lower(username)<>'admin'"
            Else
                qry = "select U.[user_id],(U.[first_name] + ' ' + U.last_name) AS name,U.[Email],U.title,U.[mobile],isnull(U.is_active,0) as is_active,isnull(U.is_super_admin,0) as is_super_admin,isnull(image_path,'') as image_path,isnull(U.username,'') as username from [tbl_sec_users] U where user_id in (select S.user_id from tbl_reg_seller_user_mapping S inner join tbl_reg_seller_user_mapping M on S.seller_id=M.seller_id where M.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ") and lower(username)<>'admin'"
            End If
            RadGrid1.DataSource = SqlHelper.ExecuteDataTable(qry)
        End If
    End Sub
  
    Protected Sub RadGrid1_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid1.PreRender

        Dim menu As GridFilterMenu = RadGrid1.FilterMenu
        
        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
        

        If (Not Page.IsPostBack) Then
            RadGrid1.MasterTableView.FilterExpression = "([is_active] = True) "
            Dim column As GridColumn = RadGrid1.MasterTableView.GetColumnSafe("is_active")
            column.CurrentFilterFunction = GridKnownFunction.EqualTo
            column.CurrentFilterValue = "True"
            RadGrid1.MasterTableView.Rebind()
        End If
    End Sub

    Protected Sub RadGrid1_ItemCommand(ByVal source As Object, ByVal e As GridCommandEventArgs) Handles RadGrid1.ItemCommand
        'If (e.CommandName = RadGrid.FilterCommandName) Then
        '    Dim filterPair As Pair = e.CommandArgument
        '    If filterPair.First = "Custom" Then
        '        e.Canceled = True
        '        gridMessage1 = "Filter function chosen: '" & filterPair.First & "' for column '" & filterPair.Second & "'"
        '        Dim filterPattern As String = CType(CType(e.Item, GridFilteringItem)(filterPair.Second).Controls(0), TextBox).Text
        '        gridMessage2 = "<br> Entered pattern for search: " & filterPattern
        '        CType(e.Item, GridFilteringItem).FireCommandEvent("Filter", New Pair("Between", filterPair.Second))
        '        gridMessage3 = "<br/>Actual filter operation performed: 'Between' <br/> for range: " & filterPattern
        '    End If

        'End If

        'If e.CommandName = RadGrid.ExpandCollapseCommandName And TypeOf e.Item Is GridDataItem Then
        '    DirectCast(e.Item, GridDataItem).ChildItem.FindControl("InnerContainer").Visible = Not e.Item.Expanded

        'End If
    End Sub


    'Protected Sub RadGrid1_DataBound(ByVal sender As Object, ByVal e As EventArgs)
    '    If Not String.IsNullOrEmpty(gridMessage1) Then
    '        DisplayMessage(gridMessage1)
    '        DisplayMessage(gridMessage2)
    '        DisplayMessage(gridMessage3)
    '    End If
    'End Sub
    'Private Sub DisplayMessage(ByVal text As String)
    '    RadGrid1.Controls.Add(New LiteralControl(String.Format("<span style='color:red'>{0}</span>", text)))
    'End Sub
End Class
