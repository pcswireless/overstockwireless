﻿
Partial Class Users_my_account
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not String.IsNullOrEmpty(CommonCode.Fetch_Cookie_Shared("user_id")) Then
            If Not IsPostBack Then
                fillDropdown()


                ViewState("user_id") = CommonCode.Fetch_Cookie_Shared("user_id")
                filldata()
                visibleData(True)
            End If
        Else
            Clear()
            visibleData(False)

        End If
    End Sub
    Private Sub fillDropdown()

        'chklst_companies.DataSource = SqlHelper.ExecuteDataTable("select seller_id,isnull(company_name,'') as company_name from tbl_reg_sellers where ISNULL(is_active,0)=1 order by company_name")
        'chklst_companies.DataBind()

        'chklst_profiles.DataSource = SqlHelper.ExecuteDataTable("select profile_id,isnull(name,'') as name from tbl_sec_profiles order by name")
        'chklst_profiles.DataBind()

        ddl_state.DataSource = SqlHelper.ExecuteDataTable("select state_id,isnull(name,'') as name from tbl_master_states order by name")
        ddl_state.DataBind()
        ddl_state.Items.Insert(0, New ListItem("--Please Select--", "0"))
        ddl_title.DataSource = SqlHelper.ExecuteDataTable("select isnull(name,'') as name,title_id from tbl_master_titles order by name")
        ddl_title.DataTextField = "name"
        ddl_title.DataValueField = "name"
        ddl_title.DataBind()
        ddl_title.Items.Insert(0, New ListItem("--Select--", ""))
    End Sub

    Private Sub visibleData(ByVal readMode As Boolean)

        If readMode Then
            img_button_edit.Visible = True
            'img_button_save.Visible = False
            img_button_update.Visible = False
            'img_button_cancel.Visible = False
            div_basic_cancel.Visible = False
        Else
            img_button_edit.Visible = False
            If String.IsNullOrEmpty(CommonCode.Fetch_Cookie_Shared("user_id")) Then
                'img_button_save.Visible = True
                img_button_update.Visible = False
                'img_button_cancel.Visible = False
                div_basic_cancel.Visible = False
            Else
                'img_button_save.Visible = False
                img_button_update.Visible = True
                'img_button_cancel.Visible = True
                div_basic_cancel.Visible = True
            End If
        End If
        rdo_ph_1_landline.Visible = Not readMode
        rdo_ph_2_landline.Visible = Not readMode
        rdo_ph_1_mobile.Visible = Not readMode
        rdo_ph_2_mobile.Visible = Not readMode
        ' fileupload_picture.Visible = Not readMode
        'CHK_Image.Visible = Not readMode
        txt_address1.Visible = Not readMode
        txt_address2.Visible = Not readMode
        txt_city.Visible = Not readMode
        'txt_email.Visible = Not readMode
        txt_first_name.Visible = Not readMode
        txt_last_name.Visible = Not readMode
        txt_mobile.Visible = Not readMode
        txt_phone_ext.Visible = Not readMode
        lit_change_password.Visible = Not readMode

        txt_phone.Visible = Not readMode
        ddl_title.Visible = Not readMode
        'txt_username.Visible = Not readMode
        txt_zip.Visible = Not readMode
        ddl_state.Visible = Not readMode
        'chklst_companies.Visible = Not readMode
        'chklst_profiles.Visible = Not readMode

        span_city.Visible = Not readMode
        'span_email.Visible = Not readMode
        span_first_name.Visible = Not readMode
        span_mobile.Visible = Not readMode
        span_password.Visible = Not readMode

        span_address1.Visible = Not readMode
        'span_username.Visible = Not readMode
        span_zip.Visible = Not readMode
        span_state.Visible = Not readMode
        'chk_active.Visible = Not readMode

        lbl_address1.Visible = readMode
        lbl_address2.Visible = readMode
        lbl_city.Visible = readMode
        'lbl_email.Visible = readMode
        lbl_first_name.Visible = readMode
        lbl_last_name.Visible = readMode
        lbl_mobile.Visible = readMode
        lbl_phone_ext.Visible = readMode
        lbl_password.Visible = readMode

        lbl_phone.Visible = readMode
        lbl_state.Visible = readMode
        lbl_title.Visible = readMode
        'lbl_username.Visible = readMode
        lbl_zip.Visible = readMode
        'lbl_companies.Visible = readMode
        'lbl_profiles.Visible = readMode
        'lbl_active.Visible = readMode

    End Sub
    Private Sub filldata()
        lbl_msg.Text = ""

        Dim ds As New DataSet

        ds = SqlHelper.ExecuteDataset("select isnull(u.user_id,0) as user_id,isnull(u.first_name,'') as first_name,isnull(u.last_name,'') as last_name,isnull(u.title,'') as title,isnull(u.email,'') as email,isnull(u.phone,'') as phone,isnull(u.mobile,'') as mobile,isnull(u.username,'') as username,isnull(u.password,'') as password,isnull(u.address1,'') as address1,isnull(u.address2,'') as address2,isnull(u.city,'') as city,isnull(u.state_id,0) as state_id,isnull(s.name,'') as state_name ,isnull(u.zip,'') as zip" & _
        ",isnull(u.is_active,0) as is_active,isnull(u.image_path,'') as image_path,isnull(is_phone1_mobile,0) as is_phone1_mobile,isnull(phone_ext,'') as phone_ext,isnull(is_phone2_mobile,0) as is_phone2_mobile from tbl_sec_users u left join tbl_master_states s on s.state_id=u.state_id where u.user_id=" & ViewState("user_id") & "; " & _
        "select m.seller_id,m.user_id,isnull(s.company_name,'') as company_name from tbl_reg_seller_user_mapping m left join tbl_reg_sellers s on m.seller_id=s.seller_id where ISNULL(s.is_active,0)=1 and m.user_id=" & ViewState("user_id") & "; select m.profile_id,m.user_id,isnull(p.name,'') as profile_name from tbl_sec_user_profile_mapping m left join tbl_sec_profiles p  on p.profile_id=m.profile_id where m.user_id=" & ViewState("user_id") & "; " & _
        "Declare @str varchar(max) select @str=COALESCE(@str + ', ', '') + ISNULL(s.company_name,'') from tbl_reg_seller_user_mapping m left join tbl_reg_sellers s on m.seller_id=s.seller_id where ISNULL(s.is_active,0)=1 and m.user_id=" & ViewState("user_id") & " select isnull(@str,''); Declare @str1 varchar(max) select @str1=COALESCE(@str1 + ', ', '') + ISNULL(p.name,'')  from tbl_sec_user_profile_mapping m left join tbl_sec_profiles p  on p.profile_id=m.profile_id where m.user_id=" & ViewState("user_id") & " select isnull(@str1,'')")
        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)

                page_heading.Text = .Item("username")
                txt_address1.Text = .Item("address1").Replace("<br/>", Char.ConvertFromUtf32(13)).Replace("<br/>", Char.ConvertFromUtf32(10))
                txt_address2.Text = .Item("address2").Replace("<br/>", Char.ConvertFromUtf32(13)).Replace("<br/>", Char.ConvertFromUtf32(10))
                txt_city.Text = .Item("city")
                'txt_email.Text = .Item("email")
                txt_first_name.Text = .Item("first_name")
                txt_last_name.Text = .Item("last_name")
                txt_mobile.Text = .Item("mobile")
                txt_phone_ext.Text = .Item("phone_ext")
                lit_change_password.Text = "<a href='javascript:void(0);' onclick=""return open_pass_win('/change_password.aspx','b=1&i=" & CommonCode.Fetch_Cookie_Shared("user_id") & "&o=1');"">Click to Change Password</a>"
                rdo_ph_1_mobile.Checked = .Item("is_phone1_mobile")
                rdo_ph_2_mobile.Checked = .Item("is_phone2_mobile")
                txt_phone.Text = .Item("phone")
                If Not ddl_title.Items.FindByText(.Item("title")) Is Nothing Then
                    ddl_title.ClearSelection()
                    ddl_title.Items.FindByText(.Item("title")).Selected = True
                End If
                If .Item("image_path") = "" Then
                    img_user.ImageUrl = "/images/buyer_icon.gif"
                    a_IMG_Image_1.HRef = "/images/buyer_icon.gif"
                Else
                    img_user.ImageUrl = "/Upload/Users/" & .Item("user_id") & "/" & .Item("image_path")
                    a_IMG_Image_1.HRef = "/Upload/Users/" & .Item("user_id") & "/" & .Item("image_path")
                    DIV_IMG_Default_Logo1_Img.Attributes.Add("onmouseover", "tip_new('<img width=250 height=200 src=" & img_user.ImageUrl.ToString & " border=0 />','250','white');")
                End If
                'txt_username.Text = .Item("username")
                txt_zip.Text = .Item("zip")
                ddl_state.SelectedValue = .Item("state_id")

                lbl_address1.Text = .Item("address1")
                lbl_address2.Text = .Item("address2")
                lbl_city.Text = .Item("city")
                lbl_email.Text = .Item("email")
                lbl_first_name.Text = .Item("first_name")
                lbl_last_name.Text = .Item("last_name")
                lbl_mobile.Text = .Item("mobile")
                lbl_phone_ext.Text = .Item("phone_ext")

                'lbl_password.Text = Security.EncryptionDecryption.DecryptValue(.Item("password"))
                'lbl_retype_password.Text = Security.EncryptionDecryption.DecryptValue(.Item("password"))
                lbl_phone.Text = .Item("phone")
                lbl_state.Text = .Item("state_name")
                lbl_title.Text = .Item("title")
                lbl_username.Text = .Item("username")
                lbl_zip.Text = .Item("zip")
                'chk_active.Checked = .Item("is_active")
                If .Item("is_active") Then
                    lbl_active.Text = "<img src='/Images/true.gif' alt=''>"
                Else
                    lbl_active.Text = "<img src='/Images/false.gif' alt=''>"
                End If
            End With
        End If


        'For i = 0 To ds.Tables(1).Rows.Count - 1
        '    chklst_companies.Items.FindByValue(ds.Tables(1).Rows(i)("seller_id")).Selected = True
        'Next


        'For i = 0 To ds.Tables(2).Rows.Count - 1
        '    chklst_profiles.Items.FindByValue(ds.Tables(2).Rows(i)("profile_id")).Selected = True
        'Next
        'lbl_companies.Text = ds.Tables(3).Rows(0)(0)
        'lbl_profiles.Text = ds.Tables(4).Rows(0)(0)



    End Sub
    'Protected Sub img_button_save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_button_save.Click

    '    If Page.IsValid Then
    '        If Check_Exist(txt_username.Text.Trim()) = True Then
    '            Exit Sub
    '        End If

    '        Dim scope_ident As Integer

    '        scope_ident = SqlHelper.ExecuteScalar("insert into tbl_sec_users (first_name,last_name,title,email,phone,mobile,username,password,address1,address2,city,state_id,zip,submit_date,submit_by_user_id,is_active) values ('" & txt_first_name.Text.Trim() & "','" & txt_last_name.Text.Trim() & "','" & txt_title.Text.Trim() & "','" & txt_email.Text.Trim() & "','" & txt_phone.Text.Trim() & "','" & txt_mobile.Text.Trim() & "','" & txt_username.Text.Trim() & "','" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "','" & txt_address1.Text.Trim() & "','" & txt_address2.Text.Trim() & "','" & txt_city.Text.Trim() & "'," & ddl_state.SelectedValue & ",'" & txt_zip.Text.Trim() & "',getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & "," & IIf(chk_active.Checked, 1, 0) & ")  select scope_identity()")
    '        For Each l As ListItem In chklst_companies.Items
    '            If l.Selected = True Then
    '                SqlHelper.ExecuteNonQuery("insert into tbl_reg_seller_user_mapping (seller_id,user_id) values (" & l.Value & "," & scope_ident & ")")
    '            End If
    '        Next

    '        For Each l As ListItem In chklst_profiles.Items
    '            If l.Selected = True Then
    '                SqlHelper.ExecuteNonQuery("insert into tbl_sec_user_profile_mapping (user_id,profile_id) values (" & scope_ident & "," & l.Value & ")")
    '            End If
    '        Next

    '        Response.Redirect("/Users/usermaster.aspx")
    '    End If

    'End Sub
    'Protected Function Check_Exist(ByVal username As String) As Boolean
    '    Dim kount As Integer = SqlHelper.ExecuteScalar("select count(username) from tbl_sec_users where username='" & txt_username.Text.Trim() & "'")
    '    If kount > 0 Then
    '        lbl_msg.Text = "User Name Already Exist !"
    '        Return True
    '    End If
    '    Return False
    'End Function

    Protected Sub Clear()
        txt_address1.Text = ""
        txt_address2.Text = ""
        txt_city.Text = ""
        'txt_email.Text = ""
        txt_first_name.Text = ""
        txt_last_name.Text = ""
        txt_mobile.Text = ""
        txt_phone_ext.Text = ""
        lit_change_password.Text = ""
        txt_phone.Text = ""
        ddl_title.ClearSelection()
        'txt_username.Text = ""
        txt_zip.Text = ""
        ddl_state.SelectedIndex = 0
        'chk_active.Checked = False
        lbl_active.Text = ""
        lbl_phone_ext.Text = ""
        'lbl_companies.Text = ""
        'lbl_profiles.Text = ""
        'For i = 0 To chklst_companies.Items.Count - 1
        '    chklst_companies.Items(i).Selected = False
        'Next
        'For i = 0 To chklst_profiles.Items.Count - 1
        '    chklst_profiles.Items(i).Selected = False
        'Next
    End Sub

    Protected Sub img_button_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_button_cancel.Click
        visibleData(True)
    End Sub

    Protected Sub img_button_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_button_update.Click
        If Page.IsValid Then

            Dim is_update_required As Boolean = False
            Dim updpara As String = ""

            If txt_first_name.Text.Trim() <> lbl_first_name.Text Then
                updpara = updpara & "first_name='" & txt_first_name.Text.Trim() & "'"
                is_update_required = True
            End If
            If txt_last_name.Text.Trim() <> lbl_last_name.Text Then
                If is_update_required Then
                    updpara = updpara & ",last_name='" & txt_last_name.Text.Trim() & "'"
                Else
                    updpara = updpara & "last_name='" & txt_last_name.Text.Trim() & "'"
                End If

                is_update_required = True
            End If

            If ddl_title.SelectedValue <> lbl_title.Text Then
                If is_update_required Then
                    updpara = updpara & ",title='" & ddl_title.SelectedValue & "'"
                Else
                    updpara = updpara & "title='" & ddl_title.SelectedValue & "'"
                End If
                is_update_required = True
            End If

            'If txt_email.Text.Trim() <> lbl_email.Text Then
            '    If is_update_required Then
            '        updpara = updpara & ",email='" & txt_email.Text.Trim() & "'"
            '    Else
            '        updpara = updpara & "email='" & txt_email.Text.Trim() & "'"
            '    End If
            '    is_update_required = True
            'End If
            If txt_phone.Text.Trim() <> lbl_phone.Text Then
                If is_update_required Then
                    updpara = updpara & ",phone='" & txt_phone.Text.Trim() & "'"
                Else
                    updpara = updpara & "phone='" & txt_phone.Text.Trim() & "'"
                End If
                is_update_required = True
            End If
            If txt_phone_ext.Text.Trim() <> lbl_phone_ext.Text Then
                If is_update_required Then
                    updpara = updpara & ",phone_ext='" & txt_phone_ext.Text.Trim() & "'"
                Else
                    updpara = updpara & "phone_ext='" & txt_phone_ext.Text.Trim() & "'"
                End If
                is_update_required = True
            End If
            If txt_mobile.Text.Trim() <> lbl_mobile.Text Then
                If is_update_required Then
                    updpara = updpara & ",mobile='" & txt_mobile.Text.Trim() & "'"
                Else
                    updpara = updpara & "mobile='" & txt_mobile.Text.Trim() & "'"
                End If
                is_update_required = True
            End If
            'If txt_username.Text.Trim() <> lbl_username.Text Then
            '    If is_update_required Then
            '        updpara = updpara & ",username='" & txt_username.Text.Trim() & "'"
            '    Else
            '        updpara = updpara & "username='" & txt_username.Text.Trim() & "'"
            '    End If
            '    is_update_required = True
            'End If
            'If txt_password.Text.Trim() <> lbl_password.Text Then
            '    If is_update_required Then
            '        updpara = updpara & ",password='" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "'"
            '    Else
            '        updpara = updpara & "password='" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "'"
            '    End If
            '    is_update_required = True
            'End If

            If txt_address1.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") <> lbl_address1.Text Then
                If is_update_required Then
                    updpara = updpara & ",address1='" & txt_address1.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'"
                Else
                    updpara = updpara & "address1='" & txt_address1.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'"
                End If
                is_update_required = True
            End If
            If txt_address2.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") <> lbl_address2.Text Then
                If is_update_required Then
                    updpara = updpara & ",address2='" & txt_address2.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'"
                Else
                    updpara = updpara & "address2='" & txt_address2.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'"
                End If
                is_update_required = True
            End If
            If txt_city.Text.Trim() <> lbl_city.Text Then
                If is_update_required Then
                    updpara = updpara & ",city='" & txt_city.Text.Trim() & "'"
                Else
                    updpara = updpara & "city='" & txt_city.Text.Trim() & "'"
                End If
                is_update_required = True
            End If

            If txt_zip.Text.Trim() <> lbl_zip.Text Then
                If is_update_required Then
                    updpara = updpara & ",zip='" & txt_zip.Text.Trim() & "'"
                Else
                    updpara = updpara & "zip='" & txt_zip.Text.Trim() & "'"
                End If
                is_update_required = True
            End If

            If lbl_state.Text <> ddl_state.SelectedItem.Text Then
                If lbl_state.Text <> "" Or ddl_state.SelectedValue <> "0" Then
                    If is_update_required Then
                        updpara = updpara & ",state_id='" & ddl_state.SelectedValue & "'"
                    Else
                        updpara = updpara & "state_id='" & ddl_state.SelectedValue & "'"
                    End If
                    is_update_required = True
                End If
            End If
            'If is_update_required Then
            '    updpara = updpara & ",is_active='" & IIf(chk_active.Checked, 1, 0) & "'"
            'Else
            '    updpara = updpara & "is_active='" & IIf(chk_active.Checked, 1, 0) & "'"
            'End If
            If is_update_required Then
                updpara = updpara & ",is_phone1_mobile = " & IIf(rdo_ph_1_landline.Checked, 0, 1) & ""
                updpara = updpara & ",is_phone2_mobile = " & IIf(rdo_ph_2_landline.Checked, 0, 1) & ""
            Else
                updpara = updpara & "is_phone1_mobile = " & IIf(rdo_ph_1_landline.Checked, 0, 1) & ""
                updpara = updpara & ",is_phone2_mobile = " & IIf(rdo_ph_2_landline.Checked, 0, 1) & ""
            End If
            is_update_required = True
            If is_update_required Then
                SqlHelper.ExecuteNonQuery("update tbl_sec_users set " & updpara & " where user_id=" & ViewState("user_id"))
            End If

            'For Each l As ListItem In chklst_companies.Items
            '    If l.Selected = True Then
            '        SqlHelper.ExecuteNonQuery("if exists(select user_id from tbl_reg_seller_user_mapping where seller_id=" & l.Value & " and user_id=" & ViewState("user_id") & " ) begin return; end else begin insert into tbl_reg_seller_user_mapping(seller_id,user_id) values (" & l.Value & "," & ViewState("user_id") & ") end ")
            '    Else
            '        SqlHelper.ExecuteNonQuery(" delete from tbl_reg_seller_user_mapping where seller_id=" & l.Value & " and user_id=" & ViewState("user_id"))
            '    End If
            'Next

            'For Each l As ListItem In chklst_profiles.Items
            '    If l.Selected = True Then
            '        SqlHelper.ExecuteNonQuery("if exists(select user_id from tbl_sec_user_profile_mapping where profile_id=" & l.Value & " and user_id=" & ViewState("user_id") & " ) begin return; end else begin insert into tbl_sec_user_profile_mapping(profile_id,user_id) values (" & l.Value & "," & ViewState("user_id") & ") end ")
            '    Else
            '        SqlHelper.ExecuteNonQuery(" delete from tbl_sec_user_profile_mapping where profile_id=" & l.Value & " and user_id=" & ViewState("user_id"))
            '    End If
            'Next
            'UploadFileUserImages(ViewState("user_id"))
            filldata()
            visibleData(True)
            lbl_msg.Text = "User details Successfully Updated"
        End If
    End Sub

    Protected Sub img_button_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_button_edit.Click
        visibleData(False)
    End Sub
    'Private Sub UploadFileUserImages(ByVal user_id As Integer)
    '    Try

    '        Dim filename As String = ""
    '        Dim pathToCreate As String = "../Upload/Users/" & user_id
    '        'Image 1 start
    '        If CHK_Image.Checked And user_id <> "0" Then
    '            filename = SqlHelper.ExecuteScalar("select image_path from tbl_sec_users where user_id=" & user_id)
    '            SqlHelper.ExecuteNonQuery("Update tbl_sec_users set image_path=NULL where user_id=" & user_id)
    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
    '            End If
    '        End If
    '        CHK_Image.Checked = False
    '        filename = ""
    '        If fileupload_picture.HasFile Then
    '            filename = fileupload_picture.FileName

    '            If user_id <> "0" Then
    '                SqlHelper.ExecuteNonQuery("update tbl_sec_users set image_path='" & filename & "' where user_id=" & user_id)
    '            End If

    '            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
    '            If infoFile.Exists Then
    '                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
    '            End If
    '            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
    '                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
    '            End If
    '            fileupload_picture.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

    '        End If

    '        'Image 1 end



    '    Catch ex As Exception
    '        'RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))
    '    End Try
    'End Sub
End Class
