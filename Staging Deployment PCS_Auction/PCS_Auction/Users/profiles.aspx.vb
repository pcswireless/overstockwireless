﻿Imports Telerik.Web.UI

Partial Class Users_profiles
    Inherits System.Web.UI.Page
    'Protected Sub RadGrid1_ItemCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrid1.ItemCommand
    '    If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
    '        If e.CommandName = "edit_item" Then
    '            Dim profile_id As Integer = 0
    '            profile_id = RadGrid1.MasterTableView.Items(e.Item.ItemIndex).GetDataKeyValue("profile_id").ToString()
    '            Response.Redirect("/Users/profiledetail.aspx?i=" & profile_id)
    '        End If
    '    End If
    'End Sub
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Private Sub setPermission()

        If CommonCode.is_super_admin() Then
            div_new_auction.Visible = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
            RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
        Else
            div_new_auction.Visible = False

            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 18
                        div_new_auction.Visible = True
                    Case 35
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = True
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = True
                        RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = True
                End Select
            Next
            dt = Nothing


        End If
    End Sub

    Protected Sub RadGrid1_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid1.PreRender
        Dim menu As GridFilterMenu = RadGrid1.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
        If (Not Page.IsPostBack) Then
            RadGrid1.MasterTableView.FilterExpression = "([is_active] = True) "
            Dim column As GridColumn = RadGrid1.MasterTableView.GetColumnSafe("is_active")
            column.CurrentFilterFunction = GridKnownFunction.EqualTo
            column.CurrentFilterValue = "True"
            RadGrid1.MasterTableView.Rebind()
        End If
    End Sub

    
End Class

