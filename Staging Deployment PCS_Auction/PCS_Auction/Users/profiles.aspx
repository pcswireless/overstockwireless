﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="profiles.aspx.vb" Inherits="Users_profiles" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
<telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                     args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
                else {
                    args.set_enableAjax(true);
                }
            }


           

        </script>
    </telerik:RadScriptBlock>
   <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
            <td>
                <div class="pageheading">
                    Profile Listing</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; width: 40%; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    Please select the profile name to edit from below
                </div>
                <div style="float: right; width: 40%; text-align: right; margin-bottom: 3px;" id="div_new_auction"
                    runat="server">
                    <a style="text-decoration: none" href="/Users/profiledetail.aspx">
                        <img src="/images/add_new_profile.gif" style="border: none" alt="Add New Profile" /></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
         <ClientEvents OnRequestStart="Cancel_Ajax" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxPanel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" style="position:fixed;left:420px;top:180px;z-index:9999" runat="server" BackgroundPosition="Center">
      <img id="Image8" src="/images/img_loading.gif" />
  </telerik:RadAjaxLoadingPanel>
    <%--start main div--%>
    <telerik:RadGrid ID="RadGrid1" runat="server" AllowSorting="true"
        PageSize="10" AllowPaging="True" AutoGenerateColumns="False" ShowGroupPanel="true"
        DataSourceID="SqlDataSource1" Skin="Vista" EnableLinqExpressions="false" AllowFilteringByColumn="True">
       <PagerStyle Mode="NextPrevAndNumeric" />
         <ExportSettings IgnorePaging="true" FileName="Profile_Export"
                        OpenInNewWindow="true" ExportOnlyData="true" />          
        <MasterTableView DataKeyNames="profile_id" DataSourceID="SqlDataSource1" HorizontalAlign="NotSet" CommandItemDisplay="Top"
            AutoGenerateColumns="False" AllowFilteringByColumn="True" ItemStyle-Height="40" AlternatingItemStyle-Height="40">
             <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false" ShowExportToPdfButton="false"
                            ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
            <NoRecordsTemplate>
                                                    No profiles to display
                                                    </NoRecordsTemplate>
                                                    <SortExpressions>
                            <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                        </SortExpressions>
            <Columns>
              <telerik:GridTemplateColumn HeaderText="Profile Name" SortExpression="name" ShowSortIcon="true" UniqueName="name" 
              DataField="name" FilterControlWidth="150" GroupByExpression="name [Profile Name] group by name">
                                <ItemTemplate >
                                   <a href="javascript:void(0);" onclick="redirectIframe('','','/Users/profiledetail.aspx?i=<%# Eval("profile_id") %>')">
                                         <%# Eval("name")%></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
               
                <telerik:GridBoundColumn DataField="permission" HeaderText="Permission" SortExpression="permission"
                    UniqueName="permission" FilterControlWidth="280">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn UniqueName="is_active" SortExpression="is_active" DataField="is_active" 
                HeaderText="Is Active" ItemStyle-Width="65" GroupByExpression="is_active [Is Active] group by is_active">
                 <FilterTemplate>
                                   
                                        <telerik:RadComboBox ID="RadComboBo_Chk_Active" runat="server" OnClientSelectedIndexChanged="StatusActiveIndexChanged"
                                             Width="120px"  SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("is_active").CurrentFilterValue %>' >
                                           <Items>
                                           <telerik:RadComboBoxItem Text="All" Value="" />
                                            <telerik:RadComboBoxItem Text="Active" Value="True" />
                                            <telerik:RadComboBoxItem Text="Inactive" Value="False" />
                                            </Items>
                                        </telerik:RadComboBox>
                                   
                                    <telerik:RadScriptBlock ID="RadScriptBlock_combo" runat="server">
                                         <script type="text/javascript">
                                             function StatusActiveIndexChanged(sender, args) {
                                                 var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                 if (args.get_item().get_value() == "") {
                                                     tableView.filter("is_active", args.get_item().get_value(), "NoFilter");
                                                 }
                                                 else {
                                                     tableView.filter("is_active", args.get_item().get_value(), "EqualTo");
                                                 }
                                             }
                                         </script>
                                    </telerik:RadScriptBlock>
                                </FilterTemplate>
                                <ItemTemplate>
                                   
                                        <img src='<%#IIf(Eval("is_active"),"/Images/true.gif","/Images/false.gif") %>' style="border: none;" alt="" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false" ItemStyle-Width="50">
                                <ItemTemplate >
                                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Users/profiledetail.aspx?i=<%# Eval("profile_id") %>')">
                                         <img src="/images/edit.png" style="border:none;" alt="Edit"/></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowDragToGroup="true">
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                 <GroupingSettings ShowUnGroupButton="true" />
    </telerik:RadGrid>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
        ProviderName="System.Data.SqlClient" SelectCommand="select profile_id,name,isnull((Stuff((Select ' / ' + name From tbl_sec_permissions  
		Where  permission_id in (select permission_id from tbl_sec_profile_permission_mapping where profile_id=A.profile_id )  FOR XML PATH('')),1,2,'')),'') as permission,isnull(is_active,0) as is_active from tbl_sec_profiles A">
    </asp:SqlDataSource>
     </td>
        </tr>
        </table>
</asp:Content>
