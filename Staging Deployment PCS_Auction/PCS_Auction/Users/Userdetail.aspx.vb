﻿
Partial Class Users_Userdetail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            fillDropdown()
            If Request.QueryString.Get("e") = "1" Then
                lbl_msg.Text = "New User created successfully"
            Else
                lbl_msg.Text = ""
            End If
            If CommonCode.is_super_admin() Then
                div_super_admin.Visible = True
                div_is_super_admin_check.Visible = True
            End If

            
            If Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
                hid_user_id.Value = Request.QueryString.Get("i")
                If CommonCode.is_admin_user = False Then
                    If CommonCode.check_by_input_admin_user(Request.QueryString.Get("i")) = True Then
                        ViewState("user_id") = 0
                        Clear()
                        visibleData(False)
                    Else
                        ViewState("user_id") = Request.QueryString.Get("i")
                        filldata()
                        visibleData(True)
                    End If
                Else
                    ViewState("user_id") = Request.QueryString.Get("i")
                    filldata()
                    visibleData(True)
                End If

            Else
                hid_user_id.Value = 0
                RadTabStrip1.Tabs(1).Visible = False
                Clear()
                visibleData(False)
            End If
            If img_user.ImageUrl = "~/Images/buyer_icon.gif" Then
                CHK_Image.Visible = False
            End If
        End If
        
    End Sub
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
    Private Sub setPermission()

        If Not CommonCode.is_super_admin() Then

            Dim Vew_User As Boolean = False
            Dim Edit_User As Boolean = False
            Dim New_User As Boolean = False
            Dim i As Integer
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select distinct B.permission_id from tbl_sec_permissions B Inner JOIN tbl_sec_profile_permission_mapping M ON B.permission_id=M.permission_id inner join tbl_sec_user_profile_mapping P on M.profile_id=P.profile_id where P.user_id=" & IIf(CommonCode.Fetch_Cookie_Shared("user_id") = "", 0, CommonCode.Fetch_Cookie_Shared("user_id")))

            For i = 0 To dt.Rows.Count - 1

                Select Case dt.Rows(i).Item("permission_id")
                    Case 14
                        New_User = True
                    Case 15
                        Vew_User = True
                    Case 16
                        Edit_User = True
                End Select
            Next
            dt = Nothing
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                If Vew_User = False Then Response.Redirect("/NoPermission.aspx")
                If Not Edit_User Then
                    Panel1_Content1button_per.Visible = False
                End If
            Else
                If New_User = False Then Response.Redirect("/NoPermission.aspx")
            End If
        End If
    End Sub
    Private Sub fillDropdown()

        chklst_companies.DataSource = SqlHelper.ExecuteDataTable("select seller_id,isnull(company_name,'') as company_name from tbl_reg_sellers where ISNULL(is_active,0)=1 order by company_name")
        chklst_companies.DataBind()

        If String.IsNullOrEmpty(Request.QueryString("i")) Then

            For Each itm As ListItem In chklst_companies.Items
                If itm.Value = 1 Then
                    itm.Selected = True

                End If
            Next
        End If

        chklst_profiles.DataSource = SqlHelper.ExecuteDataTable("select profile_id,isnull(name,'') as name from tbl_sec_profiles where is_active=1 order by name")
        chklst_profiles.DataBind()

        ddl_state.DataSource = SqlHelper.ExecuteDataTable("select state_id,isnull(name,'') as name from tbl_master_states order by name")
        ddl_state.DataBind()
        ddl_state.Items.Insert(0, New ListItem("--Please Select--", "0"))


        ddl_title.DataSource = SqlHelper.ExecuteDataTable("select isnull(name,'') as name,title_id from tbl_master_titles order by name")
        ddl_title.DataTextField = "name"
        ddl_title.DataValueField = "name"
        ddl_title.DataBind()
        ddl_title.Items.Insert(0, New ListItem("--Select--", ""))

    End Sub

    Private Sub visibleData(ByVal readMode As Boolean)


        txt_address1.Visible = Not readMode
        txt_address2.Visible = Not readMode
        txt_city.Visible = Not readMode
        txt_email.Visible = Not readMode
        txt_first_name.Visible = Not readMode
        txt_last_name.Visible = Not readMode
        txt_mobile.Visible = Not readMode
        txt_phone_ext.Visible = Not readMode
        rdo_ph_1_landline.Visible = Not readMode
        rdo_ph_2_landline.Visible = Not readMode
        rdo_ph_1_mobile.Visible = Not readMode
        rdo_ph_2_mobile.Visible = Not readMode
        lit_change_password.Visible = Not readMode
        txt_phone.Visible = Not readMode
        ddl_title.Visible = Not readMode
        txt_username.Visible = Not readMode
        txt_zip.Visible = Not readMode
        ddl_state.Visible = Not readMode
        chklst_companies.Visible = Not readMode
        chklst_profiles.Visible = Not readMode
        chk_is_super_admin.Visible = Not readMode
        span_city.Visible = Not readMode
        span_email.Visible = Not readMode
        span_first_name.Visible = Not readMode
        span_mobile.Visible = Not readMode
        span_password.Visible = Not readMode


        span_username.Visible = Not readMode
        span_zip.Visible = Not readMode
        span_address1.Visible = Not readMode
        span_state.Visible = Not readMode
        chk_active.Visible = Not readMode
        chk_ip.Visible = Not readMode
        fileupload_picture.Visible = Not readMode
        CHK_Image.Visible = Not readMode
        lbl_address1.Visible = readMode
        lbl_address2.Visible = readMode
        lbl_city.Visible = readMode
        lbl_email.Visible = readMode
        lbl_first_name.Visible = readMode
        lbl_last_name.Visible = readMode
        lbl_mobile.Visible = readMode
        lbl_phone_ext.Visible = readMode
        lbl_password.Visible = readMode
        lbl_retype_password.Visible = readMode
        lbl_phone.Visible = readMode
        lbl_state.Visible = readMode
        lbl_title.Visible = readMode
        lbl_username.Visible = readMode
        lbl_zip.Visible = readMode
        lbl_companies.Visible = readMode
        lbl_profiles.Visible = readMode
        lbl_active.Visible = readMode
        lbl_ip.Visible = readMode
        lbl_is_super_admin.Visible = readMode
        If readMode Then
            img_button_edit.Visible = True
            img_button_save.Visible = False
            img_button_update.Visible = False
            txt_password.Visible = False
            txt_retype_password.Visible = False
            Required_password.Enabled = False
            Required_retype_password.Enabled = False
            lit_retype_password_caption.Text = "&nbsp;"
            lbl_retype_password.Text = "&nbsp;"
            div_basic_cancel.Visible = False
            trRetypePassword.Visible = False
        Else
            img_button_edit.Visible = False
            If String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
                img_button_save.Visible = True
                txt_password.Visible = True
                Required_password.Enabled = True
                Required_retype_password.Enabled = True
                txt_retype_password.Visible = True
                lit_retype_password_caption.Text = "Retype Password"
                lbl_retype_password.Text = "******"
                img_button_update.Visible = False
                'img_button_cancel.Visible = False
                div_basic_cancel.Visible = False
                trRetypePassword.Visible = True
            Else
                img_button_save.Visible = False
                txt_password.Visible = False
                Required_password.Enabled = False
                Required_retype_password.Enabled = False
                txt_retype_password.Visible = False
                lit_retype_password_caption.Text = "&nbsp;"
                lbl_retype_password.Text = "&nbsp;"
                img_button_update.Visible = True
                'img_button_cancel.Visible = True
                div_basic_cancel.Visible = True
                trRetypePassword.Visible = False
            End If
        End If

        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            txt_username.Visible = False
            lbl_username.Visible = True
            If Request.QueryString("i") = SqlHelper.of_FetchKey("default_sales_rep_id") Then
                chklst_profiles.Visible = False
                chk_active.Visible = False
                lbl_active.Visible = True
                lbl_profiles.Visible = True
            End If

        End If
        If img_user.ImageUrl.Contains("/buyer_icon.gif") Then
            CHK_Image.Visible = False
        End If


    End Sub
    Private Sub filldata()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset("select isnull(u.user_id,0) as user_id,isnull(u.first_name,'') as first_name,isnull(u.last_name,'') as last_name,isnull(u.title,'') as title,isnull(u.email,'') as email,isnull(u.phone,'') as phone,isnull(u.mobile,'') as mobile,isnull(u.username,'') as username,isnull(u.password,'') as password,isnull(u.address1,'') as address1,isnull(u.address2,'') as address2,isnull(u.city,'') as city,isnull(u.state_id,0) as state_id,isnull(s.name,'') as state_name" & _
        ",isnull(u.zip,'') as zip,isnull(u.is_active,0) as is_active,isnull(u.is_super_admin,0) as is_super_admin,isnull(u.image_path,'') as image_path,isnull(is_phone1_mobile,0) as is_phone1_mobile,isnull(phone_ext,'') as phone_ext,isnull(is_phone2_mobile,0) as is_phone2_mobile,isnull(u.all_ip_allow,0) as all_ip_allow from tbl_sec_users u left join tbl_master_states s on s.state_id=u.state_id where u.user_id=" & ViewState("user_id") & "; " & _
        "select m.seller_id,m.user_id,isnull(s.company_name,'') as company_name from tbl_reg_seller_user_mapping m left join tbl_reg_sellers s on m.seller_id=s.seller_id where ISNULL(s.is_active,0)=1 and m.user_id=" & ViewState("user_id") & "; select m.profile_id,m.user_id,isnull(p.name,'') as profile_name from tbl_sec_user_profile_mapping m left join tbl_sec_profiles p  on p.profile_id=m.profile_id where m.user_id=" & ViewState("user_id") & "; " & _
        "Declare @str varchar(max) select @str=COALESCE(@str + ', ', '') + ISNULL(s.company_name,'') from tbl_reg_seller_user_mapping m left join tbl_reg_sellers s on m.seller_id=s.seller_id where ISNULL(s.is_active,0)=1 and m.user_id=" & ViewState("user_id") & " select isnull(@str,''); Declare @str1 varchar(max) select @str1=COALESCE(@str1 + ', ', '') + ISNULL(p.name,'')  from tbl_sec_user_profile_mapping m left join tbl_sec_profiles p  on p.profile_id=m.profile_id where m.user_id=" & ViewState("user_id") & " select isnull(@str1,'')")
        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)
                page_heading.Text = .Item("username")
                txt_address1.Text = .Item("address1").Replace("<br/>", Environment.NewLine).Replace("<br/>", Char.ConvertFromUtf32(10))
                txt_address2.Text = .Item("address2").Replace("<br/>", Environment.NewLine).Replace("<br/>", Char.ConvertFromUtf32(10))
                txt_city.Text = .Item("city")
                txt_email.Text = .Item("email")
                txt_first_name.Text = .Item("first_name")
                txt_last_name.Text = .Item("last_name")
                txt_mobile.Text = .Item("mobile")
                txt_phone_ext.Text = .Item("phone_ext")
                lit_change_password.Text = "<a href='javascript:void(0);' onclick=""return open_pass_win('/change_password.aspx','b=1&i=" & ViewState("user_id") & "&o=0');"">Click to Change Password</a>"
                rdo_ph_1_mobile.Checked = .Item("is_phone1_mobile")
                rdo_ph_2_mobile.Checked = .Item("is_phone2_mobile")
                txt_phone.Text = .Item("phone")
                If Not ddl_title.Items.FindByText(.Item("title")) Is Nothing Then
                    ddl_title.ClearSelection()
                    ddl_title.Items.FindByText(.Item("title")).Selected = True
                End If
                txt_username.Text = .Item("username")
                txt_zip.Text = .Item("zip")
                ddl_state.SelectedValue = .Item("state_id")
                chk_is_super_admin.Checked = IIf(.Item("is_super_admin"), True, False)
                If CommonCode.Fetch_Cookie_Shared("username").ToString().ToLower = "admin" And .Item("username").ToString().ToLower = "admin" Then
                    chk_is_super_admin.Enabled = False
                End If
                If .Item("image_path") = "" Then
                    img_user.ImageUrl = "/images/buyer_icon.gif"
                    a_IMG_Image_1.HRef = "/images/buyer_icon.gif"
                    CHK_Image.Visible = False
                Else
                    img_user.ImageUrl = "/Upload/Users/" & .Item("user_id") & "/" & .Item("image_path")
                    a_IMG_Image_1.HRef = "/Upload/Users/" & .Item("user_id") & "/" & .Item("image_path")
                    DIV_IMG_Default_Logo1_Img.Attributes.Add("onmouseover", "tip_new('<img width=250 height=200 src=" & img_user.ImageUrl.ToString & " border=0 />','250','white');")
                End If
                'DIV_IMG_Default_Logo1_Img.Attributes.Add("onmouseover", "ddrivetip('<img width=250 height=200 src=" & img_user.ImageUrl.ToString & " border=0 />','gray','250');")
                ViewState("image_path") = .Item("image_path")
                lbl_address1.Text = .Item("address1")
                lbl_address2.Text = .Item("address2")
                lbl_city.Text = .Item("city")
                lbl_email.Text = .Item("email")
                lbl_first_name.Text = .Item("first_name")
                lbl_last_name.Text = .Item("last_name")
                lbl_mobile.Text = .Item("mobile")
                lbl_phone_ext.Text = .Item("phone_ext")
                ' lbl_password.Text = Security.EncryptionDecryption.DecryptValue(.Item("password"))
                'lbl_retype_password.Text = Security.EncryptionDecryption.DecryptValue(.Item("password"))
                lbl_phone.Text = .Item("phone")
                lbl_state.Text = .Item("state_name")
                lbl_title.Text = .Item("title")
                lbl_username.Text = .Item("username")
                lbl_zip.Text = .Item("zip")
                lbl_is_super_admin.Text = IIf(.Item("is_super_admin"), "Yes", "No")

                chk_active.Checked = .Item("is_active")
                If .Item("is_active") Then
                    lbl_active.Text = "<img src='/Images/true.gif' alt=''>"
                    hid_is_active.Value = "1"
                Else
                    lbl_active.Text = "<img src='/Images/false.gif' alt=''>"
                    hid_is_active.Value = "0"
                End If

                chk_ip.Checked = .Item("all_ip_allow")
                If .Item("all_ip_allow") Then
                    lbl_ip.Text = "<img src='/Images/true.gif' alt=''>"
                Else
                    lbl_ip.Text = "<img src='/Images/false.gif' alt=''>"
                End If

            End With
        End If


        For i = 0 To ds.Tables(1).Rows.Count - 1
            If Not chklst_companies.Items.FindByValue(ds.Tables(1).Rows(i)("seller_id")) Is Nothing Then
                chklst_companies.Items.FindByValue(ds.Tables(1).Rows(i)("seller_id")).Selected = True
            End If

        Next


        For i = 0 To ds.Tables(2).Rows.Count - 1
            If Not chklst_profiles.Items.FindByValue(ds.Tables(2).Rows(i)("profile_id")) Is Nothing Then
                chklst_profiles.Items.FindByValue(ds.Tables(2).Rows(i)("profile_id")).Selected = True
            End If
        Next


        lbl_companies.Text = ds.Tables(3).Rows(0)(0)
        lbl_profiles.Text = ds.Tables(4).Rows(0)(0)
        If lbl_profiles.Text.Trim.ToLower.Contains("sale") Then
            RadTabStrip1.Tabs(1).Visible = True
            a_bid_assign.Attributes.Add("onclick", "return open_buyer_assign('" & hid_user_id.Value & "')")
        Else
            RadTabStrip1.Tabs(1).Visible = False
            a_bid_assign.HRef = "javascript:void(0);"
        End If


    End Sub
    Protected Sub img_button_save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_button_save.Click

        If Page.IsValid Then
            If Check_Exist(txt_username.Text.Trim().Replace("'", ""), "name") = True Then
                Exit Sub
            ElseIf Check_Exist(txt_email.Text.Trim().Replace("'", ""), "email") = True Then
                Exit Sub
            End If

            Dim scope_ident As Integer

            scope_ident = SqlHelper.ExecuteScalar("insert into tbl_sec_users (first_name,last_name,title,email,phone,mobile,username,password,address1,address2,city,state_id,zip,submit_date,submit_by_user_id,is_active,is_phone1_mobile,is_phone2_mobile,all_ip_allow,phone_ext,is_super_admin) values ('" & CommonCode.encodeSingleQuote(txt_first_name.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_last_name.Text.Trim()) & "','" & ddl_title.SelectedValue & "','" & txt_email.Text.Trim().Replace("'", "") & "','" & CommonCode.encodeSingleQuote(txt_phone.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_mobile.Text.Trim()) & "','" & txt_username.Text.Trim().Replace("'", "") & "','" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "','" & CommonCode.encodeSingleQuote(txt_address1.Text.Trim().Replace(Environment.NewLine, "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "','" & CommonCode.encodeSingleQuote(txt_address2.Text.Trim().Replace(Environment.NewLine, "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "','" & CommonCode.encodeSingleQuote(txt_city.Text.Trim()) & "'," & ddl_state.SelectedValue & ",'" & CommonCode.encodeSingleQuote(txt_zip.Text.Trim()) & "',getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & "," & IIf(chk_active.Checked, 1, 0) & "," & IIf(rdo_ph_1_landline.Checked, 0, 1) & "," & IIf(rdo_ph_2_landline.Checked, 0, 1) & "," & IIf(chk_ip.Checked, 1, 0) & ",'" & txt_phone_ext.Text & "'," & IIf(chk_is_super_admin.Checked, 1, 0) & ")  select scope_identity()")
            For Each l As ListItem In chklst_companies.Items
                If l.Selected = True Then
                    SqlHelper.ExecuteNonQuery("insert into tbl_reg_seller_user_mapping (seller_id,user_id) values (" & l.Value & "," & scope_ident & ")")
                End If
            Next

            For Each l As ListItem In chklst_profiles.Items
                If l.Selected = True Then
                    SqlHelper.ExecuteNonQuery("insert into tbl_sec_user_profile_mapping (user_id,profile_id) values (" & scope_ident & "," & l.Value & ")")
                End If
            Next
            If scope_ident > 0 Then
                'send an email to employee
                Dim objEmail As New Email()
                objEmail.send_employee_creation_email(scope_ident)
                objEmail = Nothing
            End If
            UploadFileUserImages(scope_ident)
            Response.Redirect("/Users/Userdetail.aspx?e=1&i=" & scope_ident)
        End If

    End Sub
    Private Sub UploadFileUserImages(ByVal user_id As Integer)
        Try

            Dim filename As String = ""
            Dim pathToCreate As String = "../Upload/Users/" & user_id
            'Image 1 start
            If CHK_Image.Checked And user_id <> "0" Then
                filename = SqlHelper.ExecuteScalar("select image_path from tbl_sec_users where user_id=" & user_id)
                SqlHelper.ExecuteNonQuery("Update tbl_sec_users set image_path=NULL where user_id=" & user_id)
                Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
                If infoFile.Exists Then
                    System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
                End If
            End If
            CHK_Image.Checked = False
            filename = ""
            If fileupload_picture.HasFile Then
                filename = fileupload_picture.FileName

                If user_id <> "0" Then
                    SqlHelper.ExecuteNonQuery("update tbl_sec_users set image_path='" & filename & "' where user_id=" & user_id)
                End If

                Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
                If infoFile.Exists Then
                    System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
                End If
                If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
                    System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
                End If
                fileupload_picture.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

            End If

            'Image 1 end



        Catch ex As Exception
            'RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))
        End Try
    End Sub
    Protected Function Check_Exist(ByVal _check As String, ByVal compare As String) As Boolean
        Dim kount As Integer = 0
        _check = txt_username.Text
        If compare = "name" Then
            kount = SqlHelper.ExecuteScalar("select count(username) from vw_login_users where username='" & _check & "'")
            If kount > 0 Then
                lbl_msg.Text = "User Name Already Exist !"
                Return True
            End If
        ElseIf compare = "email" Then
            If Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
                kount = SqlHelper.ExecuteScalar("select count(email) from vw_login_users where email='" & _check & "' and user_id<>" & Request.QueryString.Get("i") & "")
            Else
                kount = SqlHelper.ExecuteScalar("select count(email) from vw_login_users where email='" & _check & "'")
            End If

            If kount > 0 Then
                lbl_msg.Text = "Email Already Exist !"
                Return True
            End If
        End If

        Return False
    End Function
    Protected Sub Clear()
        txt_address1.Text = ""
        txt_address2.Text = ""
        txt_city.Text = ""
        txt_email.Text = ""
        txt_first_name.Text = ""
        txt_last_name.Text = ""
        txt_mobile.Text = ""
        txt_phone_ext.Text = ""
        lit_change_password.Text = ""
        txt_password.Attributes.Add("value", "")
        txt_retype_password.Attributes.Add("value", "")
        txt_phone.Text = ""
        ddl_title.ClearSelection()
        txt_username.Text = ""
        txt_zip.Text = ""
        ddl_state.SelectedIndex = 0
        chk_active.Checked = True
        chk_is_super_admin.Checked = False
        lbl_active.Text = ""
        chk_ip.Checked = False
        lbl_ip.Text = ""
        lbl_companies.Text = ""
        lbl_profiles.Text = ""
        lbl_phone_ext.Text = ""
        lbl_is_super_admin.Text = ""
        For i = 0 To chklst_profiles.Items.Count - 1
            chklst_profiles.Items(i).Selected = False
        Next
    End Sub
    Protected Sub img_button_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_button_cancel.Click
        visibleData(True)
    End Sub

    Protected Sub img_button_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_button_update.Click
        If Page.IsValid Then
            If Check_Exist(txt_email.Text.Trim().Replace("'", ""), "email") = True Then
                Exit Sub
            End If
            Dim bidder_apporval_agent_profile_id As Integer = 0
            bidder_apporval_agent_profile_id = SqlHelper.ExecuteScalar("select profile_id from tbl_Sec_profiles where profile_code='" & SqlHelper.of_FetchKey("bidder_approval_agent_profile_code") & "'")
            Dim is_checked As Boolean = False
            Dim selected_profile As Integer = 0 'for count how many profile selected
            Dim i As Integer
            For i = 0 To chklst_profiles.Items.Count - 1
                If chklst_profiles.Items(i).Selected = True Then
                    selected_profile = selected_profile + 1
                    If chklst_profiles.Items(i).Value = bidder_apporval_agent_profile_id Then
                        is_checked = True
                    End If

                End If
            Next
            If selected_profile > 1 And is_checked Then
                lbl_msg.Text = "You can not Assign another profile, if you are assigning Bidder Approval Agent"
                Exit Sub
            End If
            Dim is_update_required As Boolean = False
            Dim updpara As String = ""

            If txt_first_name.Text.Trim() <> lbl_first_name.Text Then
                updpara = updpara & "first_name='" & CommonCode.encodeSingleQuote(txt_first_name.Text.Trim()) & "'"
                is_update_required = True
            End If
            If txt_last_name.Text.Trim() <> lbl_last_name.Text Then
                If is_update_required Then
                    updpara = updpara & ",last_name='" & CommonCode.encodeSingleQuote(txt_last_name.Text.Trim()) & "'"
                Else
                    updpara = updpara & "last_name='" & CommonCode.encodeSingleQuote(txt_last_name.Text.Trim()) & "'"
                End If

                is_update_required = True
            End If

            If ddl_title.SelectedValue <> lbl_title.Text Then
                If is_update_required Then
                    updpara = updpara & ",title='" & ddl_title.SelectedValue & "'"
                Else
                    updpara = updpara & "title='" & ddl_title.SelectedValue & "'"
                End If
                is_update_required = True
            End If

            If txt_email.Text.Trim() <> lbl_email.Text Then
                If is_update_required Then
                    updpara = updpara & ",email='" & txt_email.Text.Trim().Replace("'", "") & "'"
                Else
                    updpara = updpara & "email='" & txt_email.Text.Trim().Replace("'", "") & "'"
                End If
                is_update_required = True
            End If
            If txt_phone.Text.Trim() <> lbl_phone.Text Then
                If is_update_required Then
                    updpara = updpara & ",phone='" & CommonCode.encodeSingleQuote(txt_phone.Text.Trim()) & "'"
                Else
                    updpara = updpara & "phone='" & CommonCode.encodeSingleQuote(txt_phone.Text.Trim()) & "'"
                End If
                is_update_required = True
            End If
            If txt_phone_ext.Text.Trim() <> lbl_phone_ext.Text Then
                If is_update_required Then
                    updpara = updpara & ",phone_ext='" & CommonCode.encodeSingleQuote(txt_phone_ext.Text.Trim()) & "'"
                Else
                    updpara = updpara & "phone_ext='" & CommonCode.encodeSingleQuote(txt_phone_ext.Text.Trim()) & "'"
                End If
                is_update_required = True
            End If

            If txt_mobile.Text.Trim() <> lbl_mobile.Text Then
                If is_update_required Then
                    updpara = updpara & ",mobile='" & CommonCode.encodeSingleQuote(txt_mobile.Text.Trim()) & "'"
                Else
                    updpara = updpara & "mobile='" & CommonCode.encodeSingleQuote(txt_mobile.Text.Trim()) & "'"
                End If
                is_update_required = True
            End If
            If txt_username.Text.Trim() <> lbl_username.Text Then
                If is_update_required Then
                    updpara = updpara & ",username='" & CommonCode.encodeSingleQuote(txt_username.Text.Trim()) & "'"
                Else
                    updpara = updpara & "username='" & CommonCode.encodeSingleQuote(txt_username.Text.Trim()) & "'"
                End If
                is_update_required = True
            End If
            'If txt_password.Text.Trim() <> lbl_password.Text Then
            '    If is_update_required Then
            '        updpara = updpara & ",password='" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "'"
            '    Else
            '        updpara = updpara & "password='" & Security.EncryptionDecryption.EncryptValue(txt_password.Text.Trim()) & "'"
            '    End If
            '    is_update_required = True
            'End If

            If txt_address1.Text.Trim().Replace(Environment.NewLine, "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") <> lbl_address1.Text Then
                If is_update_required Then
                    updpara = updpara & ",address1='" & CommonCode.encodeSingleQuote(txt_address1.Text.Trim().Replace(Environment.NewLine, "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "'"
                Else
                    updpara = updpara & "address1='" & CommonCode.encodeSingleQuote(txt_address1.Text.Trim().Replace(Environment.NewLine, "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "'"
                End If
                is_update_required = True
            End If
            If CommonCode.encodeSingleQuote(txt_address2.Text.Trim().Replace(Environment.NewLine, "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) <> lbl_address2.Text Then
                If is_update_required Then
                    updpara = updpara & ",address2='" & CommonCode.encodeSingleQuote(txt_address2.Text.Trim().Replace(Environment.NewLine, "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "'"
                Else
                    updpara = updpara & "address2='" & CommonCode.encodeSingleQuote(txt_address2.Text.Trim().Replace(Environment.NewLine, "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")) & "'"
                End If
                is_update_required = True
            End If
            If txt_city.Text.Trim() <> lbl_city.Text Then
                If is_update_required Then
                    updpara = updpara & ",city='" & CommonCode.encodeSingleQuote(txt_city.Text.Trim()) & "'"
                Else
                    updpara = updpara & "city='" & CommonCode.encodeSingleQuote(txt_city.Text.Trim()) & "'"
                End If
                is_update_required = True
            End If

            If txt_zip.Text.Trim() <> lbl_zip.Text Then
                If is_update_required Then
                    updpara = updpara & ",zip='" & CommonCode.encodeSingleQuote(txt_zip.Text.Trim()) & "'"
                Else
                    updpara = updpara & "zip='" & CommonCode.encodeSingleQuote(txt_zip.Text.Trim()) & "'"
                End If
                is_update_required = True
            End If

            If lbl_state.Text <> ddl_state.SelectedItem.Text Then
                If lbl_state.Text <> "" Or ddl_state.SelectedValue <> "0" Then
                    If is_update_required Then
                        updpara = updpara & ",state_id='" & ddl_state.SelectedValue & "'"
                    Else
                        updpara = updpara & "state_id='" & ddl_state.SelectedValue & "'"
                    End If
                    is_update_required = True
                End If
            End If

            If is_update_required Then
                updpara = updpara & ",is_active='" & IIf(chk_active.Checked, 1, 0) & "'"
            Else
                updpara = updpara & "is_active='" & IIf(chk_active.Checked, 1, 0) & "'"
                is_update_required = True
            End If
            If CommonCode.is_super_admin Then
                If is_update_required Then
                    updpara = updpara & ",is_super_admin=" & IIf(chk_is_super_admin.Checked, 1, 0)
                Else
                    updpara = updpara & "is_super_admin=" & IIf(chk_is_super_admin.Checked, 1, 0)
                    is_update_required = True
                End If
            End If
            


            If chk_active.Checked = False Then
                Dim stquery As String
                stquery = "select count(user_id) from tbl_reg_sale_rep_buyer_mapping where user_id=" & ViewState("user_id")
                If SqlHelper.ExecuteScalar(stquery) > 0 Or SqlHelper.of_FetchKey("default_sales_rep_id") = ViewState("user_id") Then
                    lbl_msg.Text = "This user can`t be InActive, because its used in another Place."
                    Return
                End If
            End If
            If is_update_required Then
                updpara = updpara & ",is_phone1_mobile = " & IIf(rdo_ph_1_landline.Checked, 0, 1) & ""
                updpara = updpara & ",is_phone2_mobile = " & IIf(rdo_ph_2_landline.Checked, 0, 1) & ""
            Else
                updpara = updpara & "is_phone1_mobile = " & IIf(rdo_ph_1_landline.Checked, 0, 1) & ""
                updpara = updpara & ",is_phone2_mobile = " & IIf(rdo_ph_2_landline.Checked, 0, 1) & ""
                is_update_required = True
            End If

            updpara = updpara & ",all_ip_allow = " & IIf(chk_ip.Checked, 1, 0)
            is_update_required = True
            If is_update_required Then
                SqlHelper.ExecuteNonQuery("update tbl_sec_users set " & updpara & " where user_id=" & ViewState("user_id"))
                'Send email
                Dim is_active As Integer = IIf(chk_active.Checked, 1, 0)
                Dim objEmail As New Email
                If is_active <> hid_is_active.Value Then
                    objEmail.send_employee_active_status_changed_email(ViewState("user_id"))
                End If
                objEmail = Nothing
            End If

            For Each l As ListItem In chklst_companies.Items
                If l.Selected = True Then
                    SqlHelper.ExecuteNonQuery("if exists(select user_id from tbl_reg_seller_user_mapping where seller_id=" & l.Value & " and user_id=" & ViewState("user_id") & " ) begin return; end else begin insert into tbl_reg_seller_user_mapping(seller_id,user_id) values (" & l.Value & "," & ViewState("user_id") & ") end ")
                Else
                    SqlHelper.ExecuteNonQuery(" delete from tbl_reg_seller_user_mapping where seller_id=" & l.Value & " and user_id=" & ViewState("user_id"))
                End If
            Next

            For Each l As ListItem In chklst_profiles.Items
                If l.Selected = True Then
                    SqlHelper.ExecuteNonQuery("if exists(select user_id from tbl_sec_user_profile_mapping where profile_id=" & l.Value & " and user_id=" & ViewState("user_id") & " ) begin return; end else begin insert into tbl_sec_user_profile_mapping(profile_id,user_id) values (" & l.Value & "," & ViewState("user_id") & ") end ")
                Else
                    SqlHelper.ExecuteNonQuery(" delete from tbl_sec_user_profile_mapping where profile_id=" & l.Value & " and user_id=" & ViewState("user_id"))
                End If
            Next

            UploadFileUserImages(ViewState("user_id"))
            filldata()
            visibleData(True)
            lbl_msg.Text = "User details Successfully Updated"
        End If
    End Sub

    Protected Sub img_button_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_button_edit.Click
        visibleData(False)
    End Sub
End Class
