﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="TitleListing.aspx.vb" Inherits="Users_TitleListing" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Skin="Simple" />
    <input type="hidden" id="hid_manufacturer_combo_id" name="hid_manufacturer_combo_id"
        value="0" />
    <asp:TextBox ID="txt_test" runat="server" Visible="false"></asp:TextBox>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" alt="" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Position Listing</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxPanel ID="pnl1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                    <telerik:RadGrid ID="RadGrid_Titles" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                        AllowAutomaticInserts="True" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True"
                        AllowSorting="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                        AllowMultiRowSelection="false" AllowMultiRowEdit="false" Skin="Vista" ShowGroupPanel="False">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <HeaderStyle BackColor="#BEBEBE" />
                        <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="title_id" DataSourceID="SqlDataSource1"
                            HorizontalAlign="NotSet" AutoGenerateColumns="False" SkinID="Vista" ItemStyle-Height="40"
                            EditMode="EditForms" AlternatingItemStyle-Height="40">
                            <CommandItemStyle BackColor="#E1DDDD" />
                            <NoRecordsTemplate>
                                Position not available
                            </NoRecordsTemplate>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                            </SortExpressions>
                            <CommandItemSettings AddNewRecordText="Add New Title" />
                            <Columns>
                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                    EditImageUrl="/Images/edit_grid.gif">
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                </telerik:GridEditCommandColumn>
                                <telerik:GridTemplateColumn HeaderText="Title Name" SortExpression="name" UniqueName="name"
                                    DataField="name" EditFormHeaderTextFormat="Title Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("name")%>' ></asp:Label>
                                        <asp:HiddenField ID="hd_count" runat="server" Value='<%# Eval("count")%>' />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"   HeaderText="Delete" 
                                    ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                    CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="50" />
                                    <HeaderStyle  HorizontalAlign="Center"  Width="50" />
                                </telerik:GridButtonColumn>
                            </Columns>
                            <EditFormSettings InsertCaption="New Items" EditFormType="Template">
                                <FormTemplate>
                                    <table id="tbl_grd_item_edit" cellpadding="0" cellspacing="2" border="0" width="800px">
                                        <tr>
                                            <td style="font-weight: bold; padding-top: 10px;" colspan="4">
                                                Position Details
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 135px;">
                                                Name&nbsp;<span class="req_star">*</span>
                                            </td>
                                            <td style="width: 260px;" class="details">
                                                <asp:TextBox ID="txt_grd_name" runat="server" Text='<%#Bind("name") %>' CssClass="inputtype" />
                                                <asp:RequiredFieldValidator ID="Req_txt_grd_name" runat="server" Font-Size="10px"
                                                    ValidationGroup="grd_items" ControlToValidate="txt_grd_name" Display="Dynamic"
                                                    ErrorMessage="<br>Name Required"></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="CustomValidator1" ControlToValidate="txt_grd_name" Font-Size="10px" ForeColor="red" OnServerValidate="Checkname"
                                                    ErrorMessage="</br>Name Already Exists" runat="server" Display="Dynamic" ValidationGroup="grd_items"/>
                                                    <asp:HiddenField ID="hd_title_id" runat="server" Value='<%#Bind("title_id") %>' />
                                            </td>
                                            <td align="right" colspan="2">
                                                <asp:ImageButton ID="but_grd_submit" CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem),"PerformInsert","Update") %>'
                                                    ValidationGroup="grd_items" OnClientClick="return ValidationGroupEnable('grd_items', true);"
                                                    AlternateText='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "Save" , "Update") %>'
                                                    runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif" , "/images/update.gif") %>' />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:ImageButton ID="but_grd_cancel" CausesValidation="false" CommandName="Cancel"
                                                    runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </FormTemplate>
                            </EditFormSettings>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                    </telerik:RadGrid>
                </telerik:RadAjaxPanel>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="select t.[title_id],t.[name],(select count(title) from tbl_sec_users  where title=t.name)+(select count(title) from tbl_reg_buyer_users  where title=t.name) as count from [tbl_master_titles] t "
                    DeleteCommand="DELETE FROM [tbl_master_titles] WHERE [title_id] = @title_id"
                    InsertCommand="INSERT INTO tbl_master_titles(name) 
    VALUES (@name)" UpdateCommand="UPDATE tbl_master_titles SET 
       name= @name WHERE title_id= @title_id">
                    <DeleteParameters>
                        <asp:Parameter Name="title_id" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="title_id" Type="Int32" />
                        <asp:Parameter Name="name" Type="String" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="name" Type="String" />
                    </InsertParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
