﻿Imports System.IO
Imports System.Collections.Generic
Imports System.Data.OleDb

Public Class csvFile
    Private filename As String = Nothing
    Private _valid As Boolean = False
    Private _empty As Boolean = True
    'Private _byColumn As New Dictionary(Of String, Int16)
    Private _rows As New List(Of List(Of String))
    Private _dataHeadersCaseSensitive As Boolean = True

    '
    '----   Constructors
    '
    Public Sub New()
        '
        '----   initializes a new table with no rows or columns
        '
    End Sub

    Public Sub New(ByVal width As Int16, ByVal height As Int16)
        Dim i As Int16, j As Int16

        For i = 1 To width
            Me.addColumn()
        Next

        For j = 2 To height
            Me.addRow()
        Next
    End Sub

    Public Sub New(ByVal filename As String)
        Me.New(1, 1)

        Dim i As Int16

        Me.csvDecode(File.ReadAllText(filename))
        For i = 0 To Me.width - 1
            'Me._byColumn.Add(Me._rows(0)(i), i)
        Next

        Me.filename = filename
        Me._empty = False
        Me._valid = True
    End Sub

    Public Function ConvertCSV2Dataset(ByVal fileName As String, Optional ByVal mTablename As String = "", Optional ByVal delimiter As String = "") As DataSet
        Dim pathName As String = System.IO.Path.GetDirectoryName(fileName)
        Dim file As String = System.IO.Path.GetFileName(fileName)
        Dim OleDbCommandexcelCommand As OleDbCommand
        Dim OleDbDataAdapterexcelAdapter As OleDbDataAdapter
        Dim ds As DataSet
        Dim excelConnection As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & pathName & ";Extended Properties=Text;")
        OleDbCommandexcelCommand = New OleDbCommand("SELECT * FROM [" & file & "]", excelConnection)
        OleDbDataAdapterexcelAdapter = New OleDbDataAdapter(OleDbCommandexcelCommand)
        excelConnection.Open()
        ds = New DataSet
        OleDbDataAdapterexcelAdapter.Fill(ds)
        excelConnection.Close()
        Return ds
    End Function


    Public Function ConvertCSV2Dataset1(ByVal mFile As String, ByVal mTablename As String, ByVal delimiter As String) As DataSet
        ' The dataset to return
        Dim ds As New DataSet

        ' Open the file with a stream reader
        Dim sr As New StreamReader(mFile)

        ' Split the first line into the fields and add to string array called columns
        Dim columns As String() = sr.ReadLine().Split(delimiter.ToCharArray())

        ' Add the new Datatable to the DataSet
        ds.Tables.Add(mTablename)

        ' Cylcle the columns, adding those that do not exist yet and sequencing the ones that do
        For Each col As String In columns
            Dim added As Boolean = False
            Dim _next As String = ""
            Dim i As Integer = 0

            While Not added
                ' Build the column name and remove any unwanted characters
                Dim columnname As String = col + _next
                'columnname = columnname.Replace("#", "")
                columnname = columnname.Replace("'", "")
                columnname = columnname.Replace("&", "")
                columnname = columnname.Replace("""", "")

                ' See if the column already exists
                If Not ds.Tables(mTablename).Columns.Contains(columnname) Then
                    ds.Tables(mTablename).Columns.Add(columnname)
                    added = True
                Else
                    ' If it did exist then we increment the sequencer and try again
                    i = i + 1
                    _next = "_" + i.ToString()
                End If
            End While
        Next


        ' Read the rest of the data in the file
        Dim allData As String = sr.ReadToEnd()

        ' Split off each row at the Carriage Return / Line Feed
        ' Default line ending in most windows exports
        ' You may have to edit this to match your particular file
        ' This will work for Excel, Access etc default exports.
        Dim rows As String() = allData.Split(vbCr.ToCharArray)

        ' Add each row to the Dataset
        For Each rowValue As String In rows

            Dim str As String = rowValue.ToString()
            Dim s1 As String
            Dim s2 As String
            Dim i1 As Integer
            Dim i2 As Integer

            i1 = str.IndexOf("""")
            While i1 > -1
                i2 = str.IndexOf("""", i1 + 1)
                s1 = str.Substring(i1 + 1, i2 - i1 - 1)
                s2 = s1.Replace(",", "^")
                str = str.Replace("""" & s1 & """", s2)
                i1 = str.IndexOf("""")
            End While

            ' Remove quotation field markers
            Dim row As String = str.Replace("""", "")

            ' Split the row at the delimiter
            Dim items As String() = row.Split(delimiter.ToCharArray())

            For i = 0 To items.Length - 1
                items(i) = items(i).Replace("^", ",")
            Next

            ' Add the item to the dataset
            ds.Tables(mTablename).Rows.Add(items)


            '' Remove quotation field markers
            'Dim row As String = rowValue.ToString().Replace("""", "")

            '' Split the row at the delimiter
            'Dim items As String() = row.Split(delimiter.ToCharArray())

            '' Add the item to the dataset
            'ds.Tables(mTablename).Rows.Add(items)


        Next

        ' Cleanup - Release StreamReader Resources
        sr.Close()
        sr.Dispose()

        ' Return the imported data
        Return ds

    End Function

    '
    '----   Private Functions
    '
    Private Sub csvDecode(ByRef source As String)
        Dim currRow As Int16 = 0, currCol As Int16 = 0
        Dim maxWidth As Int16 = 0
        Dim currData As String = ""
        Dim i As Int16, ch As Char, ch2 As String, cc As Int16
        Dim insideQuotes As Boolean = False
        Dim ignoreFurther As Boolean = False
        Dim finished As Boolean = False
        Dim rowFinished As Boolean = False

        For i = 1 To Len(source)
            ch = CChar(Mid(source, i, 1))
            ch2 = Mid(source, i, 2)
            cc = Asc(ch)

            Select Case ch
                Case Chr(34)
                    If (currData = "") Then
                        insideQuotes = True
                    Else
                        If (insideQuotes) Then
                            If (ch2 = (Chr(34) & Chr(34))) Then
                                currData &= Chr(34)
                                i += 1
                                Continue For
                            Else
                                insideQuotes = False
                                ignoreFurther = True
                            End If
                        End If
                    End If
                Case ","c
                    If (insideQuotes) Then
                        currData &= ch
                    Else
                        finished = True
                    End If
                Case vbLf
                    If (insideQuotes) Then
                        currData &= ch
                    Else
                        finished = True
                        rowFinished = True
                    End If
                Case vbCr
                    If (insideQuotes) Then
                        currData &= ch
                    End If
                Case Else
                    If ((ignoreFurther = False) AndAlso (cc <> 13)) Then currData &= ch
            End Select
            If (i = Len(source)) Then finished = True
            If (finished) Then
                If ((currRow = 0) AndAlso (currCol = 0)) Then
                    Me._rows(0)(0) = currData
                Else
                    Me._rows(currRow).Add(currData)
                End If
                currCol += 1
                finished = False
                ignoreFurther = False
                insideQuotes = False
                currData = ""
            End If
            If (rowFinished) Then
                If (currCol > maxWidth) Then
                    maxWidth = currCol
                End If
                currCol = 0
                currRow += 1
                Me._rows.Add(New List(Of String))
                rowFinished = False
            End If
        Next

        For i = 0 To Me._rows.Count - 1
            While (Me._rows(i).Count < maxWidth)
                Me._rows(i).Add("")
            End While
        Next
    End Sub
    Private Function csvEncode(ByVal source As String) As String
        Dim ret As String
        Dim i As Int16, ch As String

        ret = Chr(34)
        For i = 1 To Len(source)
            ch = Mid(source, i, 1)
            Select Case ch
                Case Chr(34)
                    ret &= (Chr(34) & Chr(34))
                Case Else
                    ret &= ch
            End Select
        Next
        ret &= Chr(34)

        Return (ret)
    End Function
    '
    '----   Public Functions
    '
    Public Sub addColumn(Optional ByVal defaultValue As String = "")
        Dim i As Int16, lastColumn As Int16

        Me.addField("")
        lastColumn = Me.width - 1

        For i = 0 To Me._rows.Count - 1
            Me.cell(i, lastColumn) = defaultValue
        Next
    End Sub
    Public Sub addField(ByVal name As String, Optional ByVal defaultValue As String = "")
        '
        '----   does not check to see if a field by the same name already exists
        '
        Dim i As Int16

        If (Me._empty) Then
            Me._rows.Add(New List(Of String))
            Me._empty = False
        End If

        Me._rows(0).Add(name)
        'Me._byColumn.Add(name, Me._rows(0).Count - 1)

        For i = 1 To Me._rows.Count - 1
            Me._rows(i).Add(defaultValue)
        Next
    End Sub
    Public Function addRecord() As Int16
        '
        '----   returns the record number
        '
        Dim i As Int16
        Dim recordNumber As Int16

        If (Me._empty) Then
            Throw New fieldNotFoundException
        End If

        recordNumber = Me._rows.Count

        Me._rows.Add(New List(Of String))
        For i = 1 To Me._rows(0).Count
            Me._rows(recordNumber).Add("")
        Next

        Return (recordNumber)
    End Function

    Public Function addRecord(ByVal data() As String) As Int16
        '
        '----   creates a new record, inserts the data, and returns the record number
        '
        '       How data are handled:  Data are inserted left-to-right.
        '           - Excess data (more than will fit) are ignored.
        '           - Table is padded if not enough data are given.
        '
        Dim recordNumber As Int16
        Dim i As Int16

        recordNumber = Me.addRecord()

        For i = 0 To Me._rows(0).Count - 1
            If (i > UBound(data)) Then
                Me.cell(recordNumber, i) = ""
            Else
                Me.cell(recordNumber, i) = data(i)
            End If
        Next

        Return (recordNumber)
    End Function

    Public Sub addRow(Optional ByVal defaultValue As String = "")
        Dim i As Int16
        Dim lastRow As Int16

        Me.addRecord()
        If (defaultValue = "") Then Exit Sub
        lastRow = Me._rows.Count - 1
        For i = 0 To Me.width - 1
            Me.cell(lastRow, i) = defaultValue
        Next
    End Sub

    Public Sub deleteColumn(ByVal col As Int16)
        Dim i As Int16

        If (col >= Me._rows(0).Count) Then
            Throw New deleteNonexistentColumnException
        End If
        'Me._byColumn.Remove(Me._rows(0)(col))

        'For i = 0 To Me._rows(0).Count - 1
        '    If (Me._byColumn(Me._rows(0)(i)) > col) Then
        '        Me._byColumn(Me._rows(0)(i)) -= 1
        '    End If
        'Next

        For i = 0 To Me._rows.Count - 1
            Me._rows(i).RemoveAt(col)
        Next
    End Sub

    Public Sub deleteField(ByVal name As String)
        Dim columnNumber As Int16

        columnNumber = Me.fieldColumnNumber(name)

        If (columnNumber = -1) Then
            Throw New fieldNotFoundException
        End If

        Me.deleteColumn(columnNumber)
    End Sub

    Public Sub deleteRecord(ByVal num As Int16)
        Me.deleteRow(num)
    End Sub

    Public Sub deleteRow(ByVal num As Int16)
        If (num >= Me._rows.Count) Then
            Throw New readPastEndOfTableException
        End If

        Me._rows.RemoveAt(num)
    End Sub

    Public Function fieldExists(ByVal st As String) As Boolean
        Return (Me.fieldColumnNumber(st) <> -1)
    End Function

    Public Sub insertColumn(ByVal before As Int16, Optional ByVal defaultValue As String = "")
        Dim i As Int16

        If (before >= Me._rows(0).Count) Then
            Throw New modifyPastEndOfTableException
        End If
        For i = 0 To Me._rows.Count - 1
            Me._rows(0).Insert(before, defaultValue)
        Next
    End Sub

    Public Sub insertField(ByVal name As String, ByVal before As String, Optional ByVal defaultValue As String = "")
        Dim i As Int16
        Dim insertBeforeColumn As Int16

        insertBeforeColumn = Me.fieldColumnNumber(before)
        If (insertBeforeColumn = -1) Then
            Throw New fieldNotFoundException
        End If

        'If (Me._byColumn.ContainsKey(before) = False) Then
        '    Throw New fieldNotFoundException
        'End If

        'For i = before To Me._rows(0).Count - 1
        '    Me._byColumn(Me._rows(0)(i)) += 1
        'Next
        'Me._byColumn.Add(name, before)

        Me._rows(0).Insert(insertBeforeColumn, name)
        For i = 1 To Me._rows.Count - 1
            Me._rows(i).Insert(insertBeforeColumn, defaultValue)
        Next
    End Sub

    Public Sub insertRecord(ByVal before As Int16)
        Dim i As Int16
        Dim width As Int16

        If (before >= Me._rows.Count) Then
            Throw New modifyPastEndOfTableException
        End If

        width = Me._rows.Count

        Me._rows.Insert(before, New List(Of String))
        For i = 1 To width
            Me._rows(before)(i - 1) = ""
        Next
    End Sub

    Public Sub insertRecord(ByVal before As Int16, ByVal data() As String)
        Dim i As Int16

        Me.insertRecord(before)

        For i = 0 To Me._rows(0).Count - 1
            If (i > UBound(data)) Then
                Me._rows(before)(i) = ""
            Else
                Me._rows(before)(i) = data(i)
            End If
        Next
    End Sub

    Public Sub insertRow(ByVal before As Int16, Optional ByVal defaultValue As String = "")
        Me.insertRecord(before)

        For i As Int16 = 0 To Me._rows(0).Count - 1
            Me._rows(before)(i) = defaultValue
        Next
    End Sub

    Public Sub save()
        If (Me.filename Is Nothing) Then
            Throw New filenameRequiredException
        End If

        Me.save(Me.filename)
    End Sub

    Public Sub save(ByVal filename As String)
        Dim i As Int16, j As Int16
        Dim text As String = ""

        For i = 0 To Me.height - 1
            For j = 0 To Me.width - 1
                text &= Me.csvEncode(Me._rows(i)(j))
                If (j <> Me.width - 1) Then text &= ","
            Next
            If (i <> Me.height - 1) Then text &= vbCrLf
        Next

        System.IO.File.WriteAllText(filename, text)

        Me.filename = filename
    End Sub

    '
    '----   Properties
    '

    Public Property cell(ByVal row As Int16, ByVal col As Int16) As String
        Get
            If ((row >= Me._rows.Count) OrElse (col >= Me._rows(0).Count)) Then
                Return Nothing
            End If
            Return (Me._rows(row)(col))
        End Get
        Set(ByVal value As String)
            If ((row >= Me._rows.Count) OrElse (col >= Me._rows(0).Count)) Then
            Else
                Me._rows(row)(col) = value
            End If
        End Set
    End Property

    Public Property data(ByVal field As String, ByVal record As Int16) As String
        Get
            If (record >= Me._rows.Count) Then
                Throw New readPastEndOfTableException
            End If
            'If (Me._byColumn.ContainsKey(field) = False) Then
            '    Throw New fieldNotFoundException
            'End If
            If (Me.fieldColumnNumber(field) = -1) Then
                Throw New fieldNotFoundException
            End If
            'Return (Me._rows(record)(Me._byColumn(field)))
            Return (Me._rows(record)(Me.fieldColumnNumber(field)))
        End Get
        Set(ByVal value As String)
            Dim i As Int16
            Dim origHeight As Int16

            If (Me._empty) Then
                Me.addField(field)
            End If

            If (record >= Me._rows.Count) Then
                origHeight = Me._rows.Count
                For i = origHeight To record
                    Me.addRecord()
                Next
            End If
            'If (Me._byColumn.ContainsKey(field) = False) Then
            '    Me.addField(field)
            'End If
            If (Me.fieldExists(field) = False) Then
                Me.addField(field)
            End If
            'Me._rows(record)(Me._byColumn(field)) = value
            Me._rows(record)(Me.fieldColumnNumber(field)) = value
        End Set
    End Property

    Public Property dbTableCaseSensitive() As Boolean
        Get
            Return Me._dataHeadersCaseSensitive
        End Get
        Set(ByVal value As Boolean)
            Me._dataHeadersCaseSensitive = value
        End Set
    End Property

    Private ReadOnly Property fieldColumnNumber(ByVal fieldName As String) As Int16
        Get
            Dim i As Int16

            For i = 0 To Me._rows(0).Count - 1
                If (String.Compare(fieldName, Me._rows(0)(i), Not (Me._dataHeadersCaseSensitive)) = 0) Then Return i
            Next

            Return -1
        End Get
    End Property

    Public ReadOnly Property height() As Int16
        Get
            Return (Me._rows.Count)
        End Get
    End Property

    Public ReadOnly Property width() As Int16
        Get
            Return Me._rows(0).Count
        End Get
    End Property

    '
    '----   Exceptions
    '
    Public Class deleteNonexistentColumnException
        Inherits Exception
    End Class

    Public Class deleteNonexistentRowException
        Inherits Exception
    End Class

    Public Class fieldNotFoundException
        Inherits Exception
    End Class

    Public Class filenameRequiredException
        Inherits Exception
    End Class

    Public Class modifyPastEndOfTableException
        Inherits Exception
    End Class

    Public Class readPastEndOfTableException
        Inherits Exception
    End Class
End Class

