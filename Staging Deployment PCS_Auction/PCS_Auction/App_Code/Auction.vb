﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Globalization
Imports System.IO
Imports System.Drawing
Public Class Auction
    Public Function copyAuction(ByVal auction_id As Integer, ByVal user_id As Integer) As Integer
        Dim new_id As Integer = get_auction_duplicate(auction_id, user_id)
        If new_id <> 0 Then
            copy_stock_images(auction_id, new_id)
            copy_attachment(auction_id, new_id)
            copy_payment_term_attachments(auction_id, new_id)
            copy_product_attachments(auction_id, new_id)
            copy_shipping_term_attachments(auction_id, new_id)
            copy_special_conditions(auction_id, new_id)
            copy_tax_attachments(auction_id, new_id)
            copy_terms_cond_attachments(auction_id, new_id)
            copy_invitation(auction_id, new_id)
            copy_product_item_attachment(auction_id, new_id)
            copy_after_launch_attachment(auction_id, new_id)
            copy_flag(auction_id, new_id)
        End If

        Return new_id

    End Function
    Private Sub copy_invitation(ByVal auction_id As Integer, ByVal new_id As Integer)
        Dim str As String = "select ISNULL(invitation_id,0) from tbl_auction_invitations where auction_id=" & auction_id
        Dim old_inv_id As Integer = 0
        Dim new_inv_id As Integer = 0
        Dim qry As String = ""
        Dim invitation_id As Integer = 0
        Dim title As String = ""
        old_inv_id = SqlHelper.ExecuteScalar(str)
        If old_inv_id > 0 Then
            invitation_id = SqlHelper.ExecuteScalar("select isnull(max(invitation_id),0) from tbl_auction_invitations")
            invitation_id = invitation_id + 1
            title = "My Fav " & invitation_id

            qry = "INSERT INTO tbl_auction_invitations(auction_id, name, submit_date, submit_by_user_id) " & _
                "VALUES (" & new_id & ", '" & title & "', getdate(), '" & CommonCode.Fetch_Cookie_Shared("user_id") & "')  select scope_identity()"
            new_inv_id = SqlHelper.ExecuteScalar(qry)

            Dim new_invitation_filter_id As Integer
            Dim old_invitation_filter_id As Integer

            Dim filter_type As String = ""
            Dim i As Integer
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDataTable("SELECT A.invitation_filter_id,ISNULL(A.filter_type, '') AS filter_type	FROM tbl_auction_invitation_filters A WHERE A.invitation_id = " & old_inv_id & " ORDER BY A.invitation_filter_id")
            For i = 0 To dt.Rows.Count - 1
                With dt.Rows(i)
                    old_invitation_filter_id = .Item("invitation_filter_id")
                    filter_type = .Item("filter_type")
                    new_invitation_filter_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_auction_invitation_filters(invitation_id, filter_type) VALUES ('" & new_inv_id & "', '" & filter_type & "'); SELECT SCOPE_IDENTITY()")

                    qry = "INSERT INTO tbl_auction_invitation_filter_values(invitation_filter_id, business_type_id, industry_type_id, buyer_id, auction_id,bucket_id) SELECT " & new_invitation_filter_id & ",business_type_id,industry_type_id,buyer_id," & new_id & ",bucket_id from tbl_auction_invitation_filter_values where invitation_filter_id=" & old_invitation_filter_id
                    SqlHelper.ExecuteNonQuery(qry)
                End With
            Next
        End If
       
       
    End Sub
    Private Sub copy_flag(ByVal auction_id As Integer, ByVal new_id As Integer)
        'Dim str As String = "select flag_id from tbl_auction_flags where auction_id=" & auction_id
        SqlHelper.ExecuteNonQuery("insert into tbl_auction_flags (auction_id,flag_id) select " & new_id & ",flag_id from tbl_auction_flags where auction_id=" & auction_id)
        


    End Sub
    Private Sub copy_stock_images(ByVal auction_id As Integer, ByVal new_id As Integer)
        
        Dim filename As String = ""
        Dim pathFromCreate As String = "../Upload/Auctions/stock_images/" & auction_id.ToString()
        Dim pathToCreate As String = "../Upload/Auctions/stock_images/" & new_id.ToString()

        If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate)) Then
            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate))
        End If
        Dim img_position As Integer = 1
        Dim stock_image_id As Integer = 0
        Dim stock_image_new_id As Integer = 0
        Dim i As Integer = 0
        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDataTable("SELECT A.stock_image_id,ISNULL(A.filename, '') AS filename FROM tbl_auction_stock_images A WHERE A.auction_id=" & auction_id)
        For i = 0 To dtTable.Rows.Count - 1
            Try

                stock_image_id = dtTable.Rows(i).Item("stock_image_id")
                filename = dtTable.Rows(i).Item("filename")
                If filename <> "" Then
                    stock_image_new_id = SqlHelper.ExecuteScalar("SELECT A.stock_image_id FROM tbl_auction_stock_images A WHERE A.auction_id=" & new_id & " and filename='" & filename & "'")

                    If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "\" & stock_image_new_id)) Then
                        System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "\" & stock_image_new_id))
                    End If

                    Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename)
                    If infoFile.Exists Then
                        If filename.Contains(".") Then
                            Dim infoFile_thumb As System.IO.FileInfo
                            infoFile_thumb = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_95" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            If infoFile_thumb.Exists Then
                                System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_95" & filename.Remove(0, filename.ToString.LastIndexOf(".")), System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "\" & stock_image_new_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_95" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            End If
                            infoFile_thumb = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_230" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            If infoFile_thumb.Exists Then
                                System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_230" & filename.Remove(0, filename.ToString.LastIndexOf(".")), System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "\" & stock_image_new_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_230" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            End If
                            infoFile_thumb = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_375" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            If infoFile_thumb.Exists Then
                                System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_375" & filename.Remove(0, filename.ToString.LastIndexOf(".")), System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "\" & stock_image_new_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_375" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            End If
                            infoFile_thumb = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_500" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            If infoFile_thumb.Exists Then
                                System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_500" & filename.Remove(0, filename.ToString.LastIndexOf(".")), System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "\" & stock_image_new_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_500" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            End If
                        End If
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "\" & stock_image_new_id & "\" & filename)
                    Else
                        SqlHelper.ExecuteNonQuery("delete from tbl_auction_stock_images where stock_image_id" & stock_image_new_id)
                    End If

                End If
            Catch ex As Exception
            End Try
        Next

        dtTable = SqlHelper.ExecuteDataTable("SELECT A.stock_image_id FROM tbl_auction_stock_images A WHERE A.auction_id=" & auction_id)
        For i = 0 To dtTable.Rows.Count - 1
            Try

                stock_image_id = dtTable.Rows(i).Item("stock_image_id")
                filename = dtTable.Rows(i).Item("filename")
                If filename <> "" Then
                    stock_image_new_id = SqlHelper.ExecuteScalar("SELECT A.stock_image_id FROM tbl_auction_stock_images A WHERE A.auction_id=" & new_id & " and filename='" & filename & "'")

                    If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "\" & stock_image_new_id)) Then
                        System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "\" & stock_image_new_id))
                    End If

                    Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "\" & stock_image_new_id & "\" & filename)
                        If filename.Contains(".") Then
                            Dim infoFile_thumb As System.IO.FileInfo
                            infoFile_thumb = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_95" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            If infoFile_thumb.Exists Then
                                System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_95" & filename.Remove(0, filename.ToString.LastIndexOf(".")), System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "\" & stock_image_new_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_95" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            End If
                            infoFile_thumb = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_230" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            If infoFile_thumb.Exists Then
                                System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_230" & filename.Remove(0, filename.ToString.LastIndexOf(".")), System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "\" & stock_image_new_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_230" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            End If
                            infoFile_thumb = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_375" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            If infoFile_thumb.Exists Then
                                System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_375" & filename.Remove(0, filename.ToString.LastIndexOf(".")), System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "\" & stock_image_new_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_375" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            End If
                            infoFile_thumb = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_500" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            If infoFile_thumb.Exists Then
                                System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & stock_image_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_500" & filename.Remove(0, filename.ToString.LastIndexOf(".")), System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "\" & stock_image_new_id & "\" & filename.Remove(filename.LastIndexOf(".")) & "_500" & filename.Remove(0, filename.ToString.LastIndexOf(".")))
                            End If
                        End If
                        SqlHelper.ExecuteNonQuery("update tbl_auction_stock_images set position=" & img_position & " where stock_image_id=" & stock_image_new_id)
                        img_position = img_position + 1
                    Else
                        SqlHelper.ExecuteNonQuery("delete from tbl_auction_stock_images where stock_image_id" & stock_image_new_id)
                    End If

                End If
            Catch ex As Exception

            End Try
        Next


    End Sub
    Private Sub copy_attachment(ByVal auction_id As Integer, ByVal new_id As Integer)
        Dim pathFromCreate As String = "../Upload/Auctions/auction_attachments/" & auction_id.ToString()
        Dim pathToCreate As String = "../Upload/Auctions/auction_attachments/" & new_id.ToString()

        If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate)) Then
            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate))
        End If

        Dim filename As String = ""
        Dim attachment_id As Integer = 0
        Dim attachment_new_id As Integer = 0
        Dim i As Integer = 0
        Dim infoFile As System.IO.FileInfo
        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDataTable("SELECT A.attachment_id,ISNULL(A.filename, '') AS filename FROM tbl_auction_attachments A WHERE A.auction_id=" & auction_id)
        For i = 0 To dtTable.Rows.Count - 1
            Try

                attachment_id = dtTable.Rows(i).Item("attachment_id")
                filename = dtTable.Rows(i).Item("filename")
                If filename <> "" Then
                    attachment_new_id = SqlHelper.ExecuteScalar("SELECT A.attachment_id FROM tbl_auction_attachments A WHERE A.auction_id=" & new_id & " and filename='" & filename & "'")

                    If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "\" & attachment_new_id)) Then
                        System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "\" & attachment_new_id))
                    End If

                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & attachment_id & "\" & filename)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "\" & attachment_id & "\" & filename, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "\" & attachment_new_id & "\" & filename)
                    Else
                        SqlHelper.ExecuteNonQuery("delete from tbl_auction_attachments where attachment_id=" & attachment_new_id)
                    End If
                End If
            Catch ex As Exception

            End Try

        Next

    End Sub
    Private Sub copy_payment_term_attachments(ByVal auction_id As Integer, ByVal new_id As Integer)
        Dim pathFromCreate As String = "../Upload/Auctions/payment_term_attachments/" & auction_id.ToString()
        Dim pathToCreate As String = "../Upload/Auctions/payment_term_attachments/" & new_id.ToString()

        If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate)) Then
            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate))
        End If

        Dim filename As String = ""
        Dim filename_f As String = ""
        Dim filename_s As String = ""
        Dim filename_c As String = ""

        Dim attachment_id As Integer = 0
        Dim attachment_new_id As Integer = 0
        Dim i As Integer = 0
        Dim infoFile As System.IO.FileInfo
        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDataTable("SELECT A.payment_term_attachment_id,ISNULL(A.filename, '') AS filename,ISNULL(A.filename_f, '') AS filename_f,ISNULL(A.filename_s, '') AS filename_s,ISNULL(A.filename_c, '') AS filename_c FROM tbl_auction_payment_term_attachments A WHERE A.auction_id=" & auction_id)
        For i = 0 To dtTable.Rows.Count - 1

            Try
                attachment_id = dtTable.Rows(i).Item("payment_term_attachment_id")
                filename = dtTable.Rows(i).Item("filename")
                filename_f = dtTable.Rows(i).Item("filename_f")
                filename_s = dtTable.Rows(i).Item("filename_s")
                filename_c = dtTable.Rows(i).Item("filename_c")
                attachment_new_id = SqlHelper.ExecuteScalar("SELECT A.payment_term_attachment_id FROM tbl_auction_payment_term_attachments A WHERE A.auction_id=" & new_id)
                If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "/" & attachment_new_id)) Then
                    System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "/" & attachment_new_id))
                End If

                If filename <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_payment_term_attachments set filename='' where payment_term_attachment_id=" & attachment_new_id)
                    End If
                End If

                If filename_f <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_f)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_f, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_f)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_payment_term_attachments set filename_f='' where payment_term_attachment_id=" & attachment_new_id)
                    End If
                End If

                If filename_s <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_s)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_s, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_s)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_payment_term_attachments set filename_s='' where payment_term_attachment_id=" & attachment_new_id)
                    End If
                End If

                If filename_c <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_c)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_c, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_c)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_payment_term_attachments set filename_c='' where payment_term_attachment_id=" & attachment_new_id)
                    End If
                End If
            Catch ex As Exception

            End Try
        Next

    End Sub
    Private Sub copy_product_attachments(ByVal auction_id As Integer, ByVal new_id As Integer)
        Dim pathFromCreate As String = "../Upload/Auctions/product_attachments/" & auction_id.ToString()
        Dim pathToCreate As String = "../Upload/Auctions/product_attachments/" & new_id.ToString()

        If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate)) Then
            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate))
        End If

        Dim filename As String = ""
        Dim filename_f As String = ""
        Dim filename_s As String = ""
        Dim filename_c As String = ""

        Dim attachment_id As Integer = 0
        Dim attachment_new_id As Integer = 0
        Dim i As Integer = 0
        Dim infoFile As System.IO.FileInfo
        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDataTable("SELECT A.product_attachment_id,ISNULL(A.filename, '') AS filename,ISNULL(A.filename_f, '') AS filename_f,ISNULL(A.filename_s, '') AS filename_s,ISNULL(A.filename_c, '') AS filename_c FROM tbl_auction_product_attachments A WHERE A.auction_id=" & auction_id)
        For i = 0 To dtTable.Rows.Count - 1
            Try

                attachment_id = dtTable.Rows(i).Item("product_attachment_id")
                filename = dtTable.Rows(i).Item("filename")
                filename_f = dtTable.Rows(i).Item("filename_f")
                filename_s = dtTable.Rows(i).Item("filename_s")
                filename_c = dtTable.Rows(i).Item("filename_c")

                attachment_new_id = SqlHelper.ExecuteScalar("SELECT A.product_attachment_id FROM tbl_auction_product_attachments A WHERE A.auction_id=" & new_id)
                If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "/" & attachment_new_id)) Then
                    System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "/" & attachment_new_id))
                End If

                If filename <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_product_attachments set filename='' where product_attachment_id=" & attachment_new_id)
                    End If
                End If

                If filename_f <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_f)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_f, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_f)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_product_attachments set filename_f='' where product_attachment_id=" & attachment_new_id)
                    End If
                End If

                If filename_s <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_s)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_s, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_s)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_product_attachments set filename_s='' where product_attachment_id=" & attachment_new_id)
                    End If
                End If

                If filename_c <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_c)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_c, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_c)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_product_attachments set filename_c='' where product_attachment_id=" & attachment_new_id)
                    End If
                End If
            Catch ex As Exception

            End Try



        Next

    End Sub
    Private Sub copy_shipping_term_attachments(ByVal auction_id As Integer, ByVal new_id As Integer)
        Dim pathFromCreate As String = "../Upload/Auctions/shipping_term_attachments/" & auction_id.ToString()
        Dim pathToCreate As String = "../Upload/Auctions/shipping_term_attachments/" & new_id.ToString()

        If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate)) Then
            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate))
        End If

        Dim filename As String = ""
        Dim filename_f As String = ""
        Dim filename_s As String = ""
        Dim filename_c As String = ""

        Dim attachment_id As Integer = 0
        Dim attachment_new_id As Integer = 0
        Dim i As Integer = 0
        Dim infoFile As System.IO.FileInfo
        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDataTable("SELECT A.shipping_term_attachment_id,ISNULL(A.filename, '') AS filename,ISNULL(A.filename_f, '') AS filename_f,ISNULL(A.filename_s, '') AS filename_s,ISNULL(A.filename_c, '') AS filename_c FROM tbl_auction_shipping_term_attachments A WHERE A.auction_id=" & auction_id)
        For i = 0 To dtTable.Rows.Count - 1
            'Try


            attachment_id = dtTable.Rows(i).Item("shipping_term_attachment_id")
            filename = dtTable.Rows(i).Item("filename")
            filename_f = dtTable.Rows(i).Item("filename_f")
            filename_s = dtTable.Rows(i).Item("filename_s")
            filename_c = dtTable.Rows(i).Item("filename_c")
            attachment_new_id = SqlHelper.ExecuteScalar("SELECT A.shipping_term_attachment_id FROM tbl_auction_shipping_term_attachments A WHERE A.auction_id=" & new_id)
            If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "/" & attachment_new_id)) Then
                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "/" & attachment_new_id))
            End If

            If filename <> "" Then
                infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename)
                If infoFile.Exists Then
                    System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename)
                Else
                    SqlHelper.ExecuteNonQuery("update tbl_auction_shipping_term_attachments set filename='' where shipping_term_attachment_id=" & attachment_new_id)
                End If
            End If

            If filename_f <> "" Then
                infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_f)
                If infoFile.Exists Then
                    System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_f, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_f)
                Else
                    SqlHelper.ExecuteNonQuery("update tbl_auction_shipping_term_attachments set filename_f='' where shipping_term_attachment_id=" & attachment_new_id)
                End If
            End If

            If filename_s <> "" Then
                infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_s)
                If infoFile.Exists Then
                    System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_s, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_s)
                Else
                    SqlHelper.ExecuteNonQuery("update tbl_auction_shipping_term_attachments set filename_s='' where shipping_term_attachment_id=" & attachment_new_id)
                End If
            End If

            If filename_c <> "" Then
                infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_c)
                If infoFile.Exists Then
                    System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_c, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_c)
                Else
                    SqlHelper.ExecuteNonQuery("update tbl_auction_shipping_term_attachments set filename_c='' where shipping_term_attachment_id=" & attachment_new_id)
                End If
            End If

            'Catch ex As Exception
            'End Try


        Next

    End Sub
    Private Sub copy_special_conditions(ByVal auction_id As Integer, ByVal new_id As Integer)
        Dim pathFromCreate As String = "../Upload/Auctions/special_conditions/" & auction_id.ToString()
        Dim pathToCreate As String = "../Upload/Auctions/special_conditions/" & new_id.ToString()

        If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate)) Then
            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate))
        End If

        Dim filename As String = ""
        Dim filename_f As String = ""
        Dim filename_s As String = ""
        Dim filename_c As String = ""

        Dim attachment_id As Integer = 0
        Dim attachment_new_id As Integer = 0
        Dim i As Integer = 0
        Dim infoFile As System.IO.FileInfo
        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDataTable("SELECT A.special_condition_id,ISNULL(A.filename, '') AS filename,ISNULL(A.filename_f, '') AS filename_f,ISNULL(A.filename_s, '') AS filename_s,ISNULL(A.filename_c, '') AS filename_c FROM tbl_auction_special_conditions A WHERE A.auction_id=" & auction_id)
        For i = 0 To dtTable.Rows.Count - 1
            Try
            Catch ex As Exception

            End Try
            attachment_id = dtTable.Rows(i).Item("special_condition_id")
            filename = dtTable.Rows(i).Item("filename")
            filename_f = dtTable.Rows(i).Item("filename_f")
            filename_s = dtTable.Rows(i).Item("filename_s")
            filename_c = dtTable.Rows(i).Item("filename_c")
            attachment_new_id = SqlHelper.ExecuteScalar("SELECT A.special_condition_id FROM tbl_auction_special_conditions A WHERE A.auction_id=" & new_id)
            If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "/" & attachment_new_id)) Then
                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "/" & attachment_new_id))
            End If

            If filename <> "" Then
                infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename)
                If infoFile.Exists Then
                    System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename)
                Else
                    SqlHelper.ExecuteNonQuery("update tbl_auction_special_conditions set filename='' where special_condition_id=" & attachment_new_id)
                End If
            End If

            If filename_f <> "" Then
                infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_f)
                If infoFile.Exists Then
                    System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_f, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_f)
                Else
                    SqlHelper.ExecuteNonQuery("update tbl_auction_special_conditions set filename_f='' where special_condition_id=" & attachment_new_id)
                End If
            End If

            If filename_s <> "" Then
                infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_s)
                If infoFile.Exists Then
                    System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_s, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_s)
                Else
                    SqlHelper.ExecuteNonQuery("update tbl_auction_special_conditions set filename_s='' where special_condition_id=" & attachment_new_id)
                End If
            End If

            If filename_c <> "" Then
                infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_c)
                If infoFile.Exists Then
                    System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_c, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_c)
                Else
                    SqlHelper.ExecuteNonQuery("update tbl_auction_special_conditions set filename_c='' where special_condition_id=" & attachment_new_id)
                End If
            End If



        Next

    End Sub
    Private Sub copy_tax_attachments(ByVal auction_id As Integer, ByVal new_id As Integer)
        Dim pathFromCreate As String = "../Upload/Auctions/tax_attachments/" & auction_id.ToString()
        Dim pathToCreate As String = "../Upload/Auctions/tax_attachments/" & new_id.ToString()

        If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate)) Then
            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate))
        End If

        Dim filename As String = ""
        Dim filename_f As String = ""
        Dim filename_s As String = ""
        Dim filename_c As String = ""

        Dim attachment_id As Integer = 0
        Dim attachment_new_id As Integer = 0
        Dim i As Integer = 0
        Dim infoFile As System.IO.FileInfo
        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDataTable("SELECT A.tax_attachment_id,ISNULL(A.filename, '') AS filename,ISNULL(A.filename_f, '') AS filename_f,ISNULL(A.filename_s, '') AS filename_s,ISNULL(A.filename_c, '') AS filename_c FROM tbl_auction_tax_attachments A WHERE A.auction_id=" & auction_id)
        For i = 0 To dtTable.Rows.Count - 1

            Try

                attachment_id = dtTable.Rows(i).Item("tax_attachment_id")
                filename = dtTable.Rows(i).Item("filename")
                filename_f = dtTable.Rows(i).Item("filename_f")
                filename_s = dtTable.Rows(i).Item("filename_s")
                filename_c = dtTable.Rows(i).Item("filename_c")
                attachment_new_id = SqlHelper.ExecuteScalar("SELECT A.tax_attachment_id FROM tbl_auction_tax_attachments A WHERE A.auction_id=" & new_id)
                If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "/" & attachment_new_id)) Then
                    System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "/" & attachment_new_id))
                End If

                If filename <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_tax_attachments set filename='' where tax_attachment_id=" & attachment_new_id)
                    End If
                End If

                If filename_f <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_f)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_f, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_f)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_tax_attachments set filename_f='' where tax_attachment_id=" & attachment_new_id)
                    End If
                End If

                If filename_s <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_s)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_s, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_s)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_tax_attachments set filename_s='' where tax_attachment_id=" & attachment_new_id)
                    End If
                End If

                If filename_c <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_c)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_c, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_c)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_tax_attachments set filename_c='' where tax_attachment_id=" & attachment_new_id)
                    End If
                End If

            Catch ex As Exception

            End Try


        Next

    End Sub
    Private Sub copy_terms_cond_attachments(ByVal auction_id As Integer, ByVal new_id As Integer)
        Dim pathFromCreate As String = "../Upload/Auctions/terms_cond_attachments/" & auction_id.ToString()
        Dim pathToCreate As String = "../Upload/Auctions/terms_cond_attachments/" & new_id.ToString()

        If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate)) Then
            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate))
        End If

        Dim filename As String = ""
        Dim filename_f As String = ""
        Dim filename_s As String = ""
        Dim filename_c As String = ""

        Dim attachment_id As Integer = 0
        Dim attachment_new_id As Integer = 0
        Dim i As Integer = 0
        Dim infoFile As System.IO.FileInfo
        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDataTable("SELECT A.term_cond_attachment_id,ISNULL(A.filename, '') AS filename,ISNULL(A.filename_f, '') AS filename_f,ISNULL(A.filename_s, '') AS filename_s,ISNULL(A.filename_c, '') AS filename_c FROM tbl_auction_terms_cond_attachments A WHERE A.auction_id=" & auction_id)
        For i = 0 To dtTable.Rows.Count - 1
            Try
                attachment_id = dtTable.Rows(i).Item("term_cond_attachment_id")
                filename = dtTable.Rows(i).Item("filename")
                filename_f = dtTable.Rows(i).Item("filename_f")
                filename_s = dtTable.Rows(i).Item("filename_s")
                filename_c = dtTable.Rows(i).Item("filename_c")
                attachment_new_id = SqlHelper.ExecuteScalar("SELECT A.term_cond_attachment_id FROM tbl_auction_terms_cond_attachments A WHERE A.auction_id=" & new_id)
                If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "/" & attachment_new_id)) Then
                    System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "/" & attachment_new_id))
                End If

                If filename <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_terms_cond_attachments set filename='' where term_cond_attachment_id=" & attachment_new_id)
                    End If
                End If

                If filename_f <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_f)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_f, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_f)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_terms_cond_attachments set filename_f='' where term_cond_attachment_id=" & attachment_new_id)
                    End If
                End If

                If filename_s <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_s)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_s, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_s)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_terms_cond_attachments set filename_s='' where term_cond_attachment_id=" & attachment_new_id)
                    End If
                End If

                If filename_c <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_c)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename_c, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename_c)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_terms_cond_attachments set filename_c='' where term_cond_attachment_id=" & attachment_new_id)
                    End If
                End If

            Catch ex As Exception

            End Try


        Next

    End Sub
    Private Sub copy_product_item_attachment(ByVal auction_id As Integer, ByVal new_id As Integer)
        Dim pathFromCreate As String = "../Upload/Auctions/product_items/" & auction_id.ToString()
        Dim pathToCreate As String = "../Upload/Auctions/product_items/" & new_id.ToString()

        If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate)) Then
            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate))
        End If

        Dim filename As String = ""


        Dim attachment_id As Integer = 0
        Dim attachment_new_id As Integer = 0
        Dim i As Integer = 0
        Dim infoFile As System.IO.FileInfo
        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDataTable("SELECT A.product_item_attachment_id,ISNULL(A.filename, '') AS filename FROM tbl_auction_product_item_attachments A WHERE A.auction_id=" & auction_id)
        For i = 0 To dtTable.Rows.Count - 1

            Try
                attachment_id = dtTable.Rows(i).Item("product_item_attachment_id")
                filename = dtTable.Rows(i).Item("filename")
                attachment_new_id = SqlHelper.ExecuteScalar("SELECT A.product_item_attachment_id FROM tbl_auction_product_item_attachments A WHERE A.auction_id=" & new_id)
                If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "/" & attachment_new_id)) Then
                    System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate & "/" & attachment_new_id))
                End If

                If filename <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & attachment_id & "/" & filename, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & attachment_new_id & "/" & filename)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auction_product_item_attachments set filename='' where product_item_attachment_id=" & attachment_new_id)
                    End If
                End If


            Catch ex As Exception

            End Try
        Next

    End Sub
    Private Sub copy_after_launch_attachment(ByVal auction_id As Integer, ByVal new_id As Integer)
        Dim pathFromCreate As String = "../Upload/Auctions/after_launch/" & auction_id.ToString()
        Dim pathToCreate As String = "../Upload/Auctions/after_launch/" & new_id.ToString()

        If Not System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(pathToCreate)) Then
            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(pathToCreate))
        End If

        Dim filename As String = ""


        Dim i As Integer = 0
        Dim infoFile As System.IO.FileInfo
        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDataTable("SELECT ISNULL(A.after_launch_filename, '') AS filename FROM tbl_auctions A WHERE A.auction_id=" & auction_id)
        For i = 0 To dtTable.Rows.Count - 1

            Try

                filename = dtTable.Rows(i).Item("filename")
               
                If filename <> "" Then
                    infoFile = New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & filename)
                    If infoFile.Exists Then
                        System.IO.File.Copy(System.Web.HttpContext.Current.Server.MapPath(pathFromCreate) & "/" & filename, System.Web.HttpContext.Current.Server.MapPath(pathToCreate) & "/" & filename)
                    Else
                        SqlHelper.ExecuteNonQuery("update tbl_auctions set after_launch_filename='' where auction_id=" & new_id)
                    End If
                End If


            Catch ex As Exception

            End Try
        Next

    End Sub
    Private Function get_auction_duplicate(ByVal auction_id As Integer, ByVal user_id As Integer) As Integer
        Dim id_generated As Integer
        Dim SqlParameter() As SqlParameter = New SqlParameter(2) {}

        SqlParameter(0) = New SqlParameter("@auction_id", SqlDbType.Int)
        SqlParameter(0).Direction = ParameterDirection.Input
        SqlParameter(0).Value = auction_id

        SqlParameter(1) = New SqlParameter("@user_id", SqlDbType.Int)
        SqlParameter(1).Direction = ParameterDirection.Input
        SqlParameter(1).Value = user_id

        SqlParameter(2) = New SqlParameter("@id_generated", SqlDbType.Int)
        SqlParameter(2).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(SqlHelper.of_getConnectString(), CommandType.StoredProcedure, "get_auction_duplicate", SqlParameter)

        id_generated = SqlParameter(2).Value
        SqlParameter = Nothing

        Return id_generated
    End Function
    Public Function fetch_auction_Listing(ByVal tab_id As Integer, ByVal auction_type_id As Integer, ByVal product_category_id As Integer, ByVal buyer_user_id As Integer, ByVal buyer_id As Integer, ByVal is_allow_without_login As Boolean, Optional ByVal auction_id As Integer = 0) As DataSet

        Dim dsDataset As New DataSet
        Dim SqlParameter() As SqlParameter = New SqlParameter(6) {}

        SqlParameter(0) = New SqlParameter("@tab_id", SqlDbType.Int)
        SqlParameter(0).Direction = ParameterDirection.Input
        SqlParameter(0).Value = tab_id

        SqlParameter(1) = New SqlParameter("@auction_type_id", SqlDbType.Int)
        SqlParameter(1).Direction = ParameterDirection.Input
        SqlParameter(1).Value = auction_type_id

        SqlParameter(2) = New SqlParameter("@product_category_id", SqlDbType.Int)
        SqlParameter(2).Direction = ParameterDirection.Input
        SqlParameter(2).Value = product_category_id

        SqlParameter(3) = New SqlParameter("@buyer_user_id", SqlDbType.Int)
        SqlParameter(3).Direction = ParameterDirection.Input
        SqlParameter(3).Value = buyer_user_id

        SqlParameter(4) = New SqlParameter("@buyer_id", SqlDbType.Int)
        SqlParameter(4).Direction = ParameterDirection.Input
        SqlParameter(4).Value = buyer_id

        SqlParameter(5) = New SqlParameter("@frontend_login_needed", SqlDbType.Bit)
        SqlParameter(5).Direction = ParameterDirection.Input
        SqlParameter(5).Value = is_allow_without_login


        SqlParameter(6) = New SqlParameter("@auction_id", SqlDbType.Int)
        SqlParameter(6).Direction = ParameterDirection.Input
        SqlParameter(6).Value = auction_id


        dsDataset = SqlHelper.ExecuteDataset(SqlHelper.of_getConnectString(), CommandType.StoredProcedure, "usp_fetch_auction_Listing", SqlParameter)

        Return dsDataset
        dsDataset.Dispose()
        dsDataset = Nothing
        SqlParameter = Nothing
    End Function
    Public Function fetch_auction_Listing_new(ByVal tab_id As Integer, ByVal auction_type_id As Integer, ByVal product_category_id As Integer, ByVal buyer_user_id As Integer, ByVal buyer_id As Integer, ByVal is_allow_without_login As Boolean, Optional ByVal auction_id As Integer = 0) As DataSet

        Dim dsDataset As New DataSet
        Dim SqlParameter() As SqlParameter = New SqlParameter(6) {}

        SqlParameter(0) = New SqlParameter("@tab_id", SqlDbType.Int)
        SqlParameter(0).Direction = ParameterDirection.Input
        SqlParameter(0).Value = tab_id

        SqlParameter(1) = New SqlParameter("@auction_type_id", SqlDbType.Int)
        SqlParameter(1).Direction = ParameterDirection.Input
        SqlParameter(1).Value = auction_type_id

        SqlParameter(2) = New SqlParameter("@product_category_id", SqlDbType.Int)
        SqlParameter(2).Direction = ParameterDirection.Input
        SqlParameter(2).Value = product_category_id

        SqlParameter(3) = New SqlParameter("@buyer_user_id", SqlDbType.Int)
        SqlParameter(3).Direction = ParameterDirection.Input
        SqlParameter(3).Value = buyer_user_id

        SqlParameter(4) = New SqlParameter("@buyer_id", SqlDbType.Int)
        SqlParameter(4).Direction = ParameterDirection.Input
        SqlParameter(4).Value = buyer_id

        SqlParameter(5) = New SqlParameter("@frontend_login_needed", SqlDbType.Bit)
        SqlParameter(5).Direction = ParameterDirection.Input
        SqlParameter(5).Value = is_allow_without_login


        SqlParameter(6) = New SqlParameter("@auction_id", SqlDbType.Int)
        SqlParameter(6).Direction = ParameterDirection.Input
        SqlParameter(6).Value = auction_id


        dsDataset = SqlHelper.ExecuteDataset(SqlHelper.of_getConnectString(), CommandType.StoredProcedure, "usp_fetch_auction_Listing_New", SqlParameter)

        Return dsDataset
        dsDataset.Dispose()
        dsDataset = Nothing
        SqlParameter = Nothing
    End Function
    Public Function fetch_auction_Detail(ByVal buyer_user_id As Integer, ByVal buyer_id As Integer, ByVal frontend_login_needed As Int16, ByVal auction_id As Integer) As DataTable

        Dim dtDataTable As New DataTable
        Dim SqlParameter() As SqlParameter = New SqlParameter(3) {}

        SqlParameter(0) = New SqlParameter("@buyer_user_id", SqlDbType.Int)
        SqlParameter(0).Direction = ParameterDirection.Input
        SqlParameter(0).Value = buyer_user_id

        SqlParameter(1) = New SqlParameter("@buyer_id", SqlDbType.Int)
        SqlParameter(1).Direction = ParameterDirection.Input
        SqlParameter(1).Value = buyer_id

        SqlParameter(2) = New SqlParameter("@frontend_login_needed", SqlDbType.Bit)
        SqlParameter(2).Direction = ParameterDirection.Input
        SqlParameter(2).Value = frontend_login_needed

        SqlParameter(3) = New SqlParameter("@auction_id", SqlDbType.Int)
        SqlParameter(3).Direction = ParameterDirection.Input
        SqlParameter(3).Value = auction_id


        dtDataTable = SqlHelper.ExecuteDatatable(SqlHelper.of_getConnectString(), CommandType.StoredProcedure, "usp_fetch_auction_Detail", SqlParameter)

        Return dtDataTable
        dtDataTable.Dispose()
        dtDataTable = Nothing
        SqlParameter = Nothing
    End Function
    '---------------------------------------------------------
    '-- Method Name : delete_auction
    '-- Store PROCEDURE Called : usp_delete_auction
    '-- Description : 
    '-- Creation Date : 10/31/2013
    '---------------------------------------------------------
    Public Function delete_auction(ByVal auction_id As Integer) As Integer
        Dim SqlParameter() As SqlParameter = New SqlParameter(0) {}

        SqlParameter(0) = New SqlParameter("@auction_id", SqlDbType.int)
        SqlParameter(0).Direction = ParameterDirection.Input
        SqlParameter(0).Value = auction_id



        Return SqlHelper.ExecuteScalar(SqlHelper.of_getConnectString(), CommandType.StoredProcedure, "usp_delete_auction", SqlParameter)

        SqlParameter = Nothing
    End Function
    '---------------------------------------------------------
    '-- Method Name : update_auction_bidder_invitation
    '-- Store PROCEDURE Called : Usp_update_auction_bidder_invitation
    '-- Description : 
    '-- Creation Date : 8/4/2014
    '---------------------------------------------------------
    Public Sub update_auction_bidder_invitation(ByVal auction_id As Integer)
        Dim SqlParameter() As SqlParameter = New SqlParameter(0) {}

        SqlParameter(0) = New SqlParameter("@auction_id", SqlDbType.int)
        SqlParameter(0).Direction = ParameterDirection.Input
        SqlParameter(0).Value = auction_id



        SqlHelper.ExecuteNonQuery(SqlHelper.of_getConnectString(), CommandType.StoredProcedure, "Usp_update_auction_bidder_invitation", SqlParameter)

        SqlParameter = Nothing
    End Sub

End Class
