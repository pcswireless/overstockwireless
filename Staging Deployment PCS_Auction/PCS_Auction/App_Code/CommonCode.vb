Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Globalization
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Text
Imports System.IO

Public Class CommonCode

    Public Sub sendEmail(ByVal from_address As String, ByVal from_name As String, ByVal subject As String, ByVal body As String, ByVal to_address As String, ByVal to_name As String, ByVal bcc_address As String, ByVal bcc_name As String, ByVal cc_address As String, ByVal cc_name As String)

        If SqlHelper.of_FetchKey("ServerHttp").Contains("166.78.120.180:81") Then subject = "Test server " & subject

        Dim obj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient()

        Dim msgMail As New System.Net.Mail.MailMessage 'System.Web.Mail.MailMessage

        Dim str1 As String = ""

        Dim str2 As String = ""

        Dim arr1(), arr2()

        Dim i As Integer

        msgMail.IsBodyHtml = True

        'System.Web.HttpContext.Current.Response.Write("<br>strFrom-" & strFrom & ", " & strFromName)

        If from_address = "" Then

            from_address = SqlHelper.of_FetchKey("email_from")
            from_name = SqlHelper.of_FetchKey("email_from_name")

        End If

        msgMail.From = New System.Net.Mail.MailAddress(from_address, from_name)



        arr1 = to_address.Split(",")

        arr2 = to_name.Split(",")

        For i = 0 To arr1.Length - 1

            str1 = ""

            str2 = ""

            str1 = Trim(arr1(i))

            If ((arr2.Length - 1 >= i)) Then

                str2 = Trim(arr2(i))

            End If

            If str1 <> "" Then

                'System.Web.HttpContext.Current.Response.Write("<br>to.add-" & str1 & ", " & str2)

                msgMail.To.Add(New System.Net.Mail.MailAddress(str1, str2))

            End If

        Next

        'System.Web.HttpContext.Current.Response.Write("<br>" & strTo)

        'If cc_address = "" Then
        '    cc_address = SqlHelper.of_FetchKey("email_CC")
        'End If

        arr1 = cc_address.Split(",")

        arr2 = cc_name.Split(",")

        For i = 0 To arr1.Length - 1

            str1 = ""

            str2 = ""

            str1 = Trim(arr1(i))

            If ((arr2.Length - 1 >= i)) Then

                str2 = Trim(arr2(i))

            End If

            If str1 <> "" Then

                'System.Web.HttpContext.Current.Response.Write("<br>cc.add-" & str1)

                msgMail.CC.Add(New System.Net.Mail.MailAddress(str1, str2))
                'msgMail.Bcc.Add(New System.Net.Mail.MailAddress(str1, str2))

            End If

        Next



        arr1 = bcc_address.Split(",")

        arr2 = bcc_name.Split(",")

        For i = 0 To arr1.Length - 1

            str1 = ""

            str2 = ""

            str1 = Trim(arr1(i))

            If ((arr2.Length - 1 >= i)) Then

                str2 = Trim(arr2(i))

            End If

            If str1 <> "" Then

                'System.Web.HttpContext.Current.Response.Write("<br>cc.add-" & str1)

                msgMail.Bcc.Add(New System.Net.Mail.MailAddress(str1, str2))

            End If

        Next
        msgMail.Bcc.Add(New System.Net.Mail.MailAddress(SqlHelper.of_FetchKey("email_BCC"), "Auction-Mail Bind Copy"))
        msgMail.ReplyTo = New System.Net.Mail.MailAddress(SqlHelper.of_FetchKey("site_email_to"), "PCS Wireless Auctions")

        obj.Host = SqlHelper.of_FetchKey("smtp_server")

        obj.UseDefaultCredentials = True

        'obj.Credentials = New System.Net.NetworkCredential("usr142242", "B7tarHery*")

        'obj.Port = 2500

        obj.Credentials = New System.Net.NetworkCredential(SqlHelper.of_FetchKey("smtp_user_name"), SqlHelper.of_FetchKey("smtp_password"))

        obj.Port = SqlHelper.of_FetchKey("smtp_server_port")

        'System.Web.HttpContext.Current.Response.Write("<br>subject: " & subject)

        'System.Web.HttpContext.Current.Response.Write("<br>body: " & body)



        msgMail.Subject = Replace(subject, vbCrLf, "")

        msgMail.Body = body
        Dim is_send As Boolean = False
        If SqlHelper.of_FetchKey("is_email_allow") = "1" Then
            obj.Send(msgMail)
            is_send = True
        End If

        Try
           ' SqlHelper.ExecuteNonQuery("INSERT INTO tbl_sec_email_log(created_date, from_address, from_name, subject, body, to_address, to_name, bcc_address, bcc_name, cc_address, cc_name, is_send) VALUES (getdate(), '" & from_address & "','" & from_name & "','" & subject.Replace("'", "''") & "','" & body.Replace("'", "''") & "','" & to_address & "','" & to_name & "','" & bcc_address & "','" & bcc_name & "','" & cc_address & "','" & cc_name & "'," & IIf(is_send = True, 1, 0) & ")")
        Catch ex As Exception

        End Try
    End Sub
    Public Sub sendEmail_transaction(ByVal from_address As String, ByVal from_name As String, ByVal subject As String, ByVal body As String, ByVal to_address As String, ByVal to_name As String, ByVal bcc_address As String, ByVal bcc_name As String, ByVal cc_address As String, ByVal cc_name As String)
        If SqlHelper.of_FetchKey("ServerHttp").Contains("166.78.120.180:81") Then subject = "Test server " & subject
        Dim obj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient()

        Dim msgMail As New System.Net.Mail.MailMessage 'System.Web.Mail.MailMessage

        Dim str1 As String = ""

        Dim str2 As String = ""

        Dim arr1(), arr2()

        Dim i As Integer

        msgMail.IsBodyHtml = True

        

        msgMail.From = New System.Net.Mail.MailAddress(SqlHelper.of_FetchKey("email_from_trans"), SqlHelper.of_FetchKey("email_from_name_trans"))



        arr1 = to_address.Split(",")

        arr2 = to_name.Split(",")

        For i = 0 To arr1.Length - 1

            str1 = ""

            str2 = ""

            str1 = Trim(arr1(i))

            If ((arr2.Length - 1 >= i)) Then

                str2 = Trim(arr2(i))

            End If

            If str1 <> "" Then

                msgMail.To.Add(New System.Net.Mail.MailAddress(str1, str2))

            End If

        Next

        
        msgMail.Bcc.Add(New System.Net.Mail.MailAddress(SqlHelper.of_FetchKey("email_BCC"), "Auction-Mail Bind Copy"))
        
        obj.Host = SqlHelper.of_FetchKey("smtp_server_trans")

        obj.UseDefaultCredentials = True

        obj.Credentials = New System.Net.NetworkCredential(SqlHelper.of_FetchKey("smtp_user_name_trans"), SqlHelper.of_FetchKey("smtp_password_trans"))

        obj.Port = SqlHelper.of_FetchKey("smtp_server_port_trans")

        
        msgMail.Subject = Replace(subject, vbCrLf, "")

        msgMail.Body = body
        Dim is_send As Boolean = False
        If SqlHelper.of_FetchKey("is_email_allow") = "1" Then
            obj.Send(msgMail)
            is_send = True
        End If

        Try
            ' SqlHelper.ExecuteNonQuery("INSERT INTO tbl_sec_email_log(created_date, from_address, from_name, subject, body, to_address, to_name, bcc_address, bcc_name, cc_address, cc_name, is_send) VALUES (getdate(), '" & from_address & "','" & from_name & "','" & subject.Replace("'", "''") & "','" & body.Replace("'", "''") & "','" & to_address & "','" & to_name & "','" & bcc_address & "','" & bcc_name & "','" & cc_address & "','" & cc_name & "'," & IIf(is_send = True, 1, 0) & ")")
        Catch ex As Exception

        End Try
    End Sub
    Public Sub sendEmail_error(ByVal from_address As String, ByVal from_name As String, ByVal subject As String, ByVal body As String, ByVal to_address As String, ByVal to_name As String, ByVal bcc_address As String, ByVal bcc_name As String, ByVal cc_address As String, ByVal cc_name As String, Optional ByVal reply_to As String = "")
        Try

            If SqlHelper.of_FetchKey("ServerHttp").Contains("166.78.120.180:81") Then subject = "Test server " & subject

            Dim obj As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient()

            Dim msgMail As New System.Net.Mail.MailMessage 'System.Web.Mail.MailMessage

            Dim str1 As String = ""

            Dim str2 As String = ""

            Dim arr1(), arr2()

            Dim i As Integer

            msgMail.IsBodyHtml = True

            'System.Web.HttpContext.Current.Response.Write("<br>strFrom-" & strFrom & ", " & strFromName)

            If from_address = "" Then

                from_address = SqlHelper.of_FetchKey("email_from_error")
                from_name = SqlHelper.of_FetchKey("email_from_name_error")

            End If

            msgMail.From = New System.Net.Mail.MailAddress(from_address, from_name)

            If reply_to <> "" Then msgMail.ReplyTo = New System.Net.Mail.MailAddress(reply_to)

            arr1 = to_address.Split(",")

            arr2 = to_name.Split(",")

            For i = 0 To arr1.Length - 1

                str1 = ""

                str2 = ""

                str1 = Trim(arr1(i))

                If ((arr2.Length - 1 >= i)) Then

                    str2 = Trim(arr2(i))

                End If

                If str1 <> "" Then

                    'System.Web.HttpContext.Current.Response.Write("<br>to.add-" & str1 & ", " & str2)

                    msgMail.To.Add(New System.Net.Mail.MailAddress(str1, str2))

                End If

            Next

            'System.Web.HttpContext.Current.Response.Write("<br>" & strTo)

            arr1 = cc_address.Split(",")

            arr2 = cc_name.Split(",")

            For i = 0 To arr1.Length - 1

                str1 = ""

                str2 = ""

                str1 = Trim(arr1(i))

                If ((arr2.Length - 1 >= i)) Then

                    str2 = Trim(arr2(i))

                End If

                If str1 <> "" Then

                    'System.Web.HttpContext.Current.Response.Write("<br>cc.add-" & str1)

                    msgMail.CC.Add(New System.Net.Mail.MailAddress(str1, str2))

                End If

            Next



            arr1 = bcc_address.Split(",")

            arr2 = bcc_name.Split(",")

            For i = 0 To arr1.Length - 1

                str1 = ""

                str2 = ""

                str1 = Trim(arr1(i))

                If ((arr2.Length - 1 >= i)) Then

                    str2 = Trim(arr2(i))

                End If

                If str1 <> "" Then

                    'System.Web.HttpContext.Current.Response.Write("<br>cc.add-" & str1)

                    msgMail.Bcc.Add(New System.Net.Mail.MailAddress(str1, str2))

                End If

            Next



            obj.Host = SqlHelper.of_FetchKey("smtp_server_error")

            obj.UseDefaultCredentials = True

            'obj.Credentials = New System.Net.NetworkCredential("usr142242", "B7tarHery*")

            'obj.Port = 2500

            obj.Credentials = New System.Net.NetworkCredential(SqlHelper.of_FetchKey("smtp_user_name_error"), SqlHelper.of_FetchKey("smtp_password_error"))

            obj.Port = SqlHelper.of_FetchKey("smtp_server_port_error")

            ' System.Web.HttpContext.Current.Response.Write("<br>subject-" & subject)

            ' System.Web.HttpContext.Current.Response.Write("<br>body-" & body)



            msgMail.Subject = Replace(subject, vbCrLf, "")

            msgMail.Body = body

            'If UCase(System.Web.HttpContext.Current.Server.MachineName) <> UCase(SqlHelper.of_FetchKey("ServerMachineName")) Then
            '      obj.Send(msgMail)
            'End If
            If SqlHelper.of_FetchKey("is_email_allow") = 1 Then
                obj.Send(msgMail)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub Remove_Cookie(ByVal strCookie As String)
        Dim objCookie As HttpCookie = System.Web.HttpContext.Current.Request.Cookies(strCookie)
        System.Web.HttpContext.Current.Response.Cookies.Remove(strCookie)
        objCookie.Values("value") = ""

    End Sub
    Public Shared Function getAuctionTabPosition(ByVal auction_id As Integer) As Integer
        Dim pos As Integer = 0
        Dim dt As New DataTable()
        Dim i As Integer
        dt = SqlHelper.ExecuteDatatable("select top 4 auction_id from tbl_login_user_auctions where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " order by sno desc")
        For i = 0 To dt.Rows.Count - 1
            If dt.Rows(i).Item("auction_id") = auction_id Then
                pos = 4 + (dt.Rows.Count - 1 - i)
            End If
        Next
        If pos = 0 Then pos = 4 + dt.Rows.Count
        If pos > 7 Then pos = 7
        Return pos
    End Function
    Public Shared Function Fetch_Cookie_Shared(ByVal strCookieName As String) As String
        Dim obj As New CommonCode
        Return obj.Fetch_Cookie(strCookieName)
    End Function

    Public Function Fetch_Cookie(ByVal strCookieName As String) As String
        Dim objCookie As HttpCookie = System.Web.HttpContext.Current.Request.Cookies(strCookieName)

        If Not (objCookie Is Nothing) Then
            Return objCookie.Values("value")
        Else
            Return ""
        End If
        objCookie = Nothing
    End Function

    Public Sub Create_Cookie(ByVal strCookie As String, ByVal strValue As String)
        Dim objCookie As New HttpCookie(strCookie)

        Remove_Cookie(strCookie)

        objCookie.Values.Add("value", strValue)

        System.Web.HttpContext.Current.Response.AppendCookie(objCookie)
        objCookie = Nothing
    End Sub

    Public Shared Sub Create_Cookie_shared(ByVal strCookie As String, ByVal strValue As String)
        Dim objCookie As New HttpCookie(strCookie)

        Remove_Cookie_shared(strCookie)

        objCookie.Values.Add("value", strValue)

        System.Web.HttpContext.Current.Response.AppendCookie(objCookie)
        objCookie = Nothing
    End Sub

    Public Shared Sub Remove_Cookie_shared(ByVal strCookie As String)
        Dim objCookie As HttpCookie = System.Web.HttpContext.Current.Request.Cookies(strCookie)
        System.Web.HttpContext.Current.Response.Cookies.Remove(strCookie)
        objCookie = Nothing
    End Sub
    Public Shared Function encodeSingleQuote(ByVal strString As String) As String
        Return strString.Replace("'", "''")
    End Function
    Public Shared Function decodeSingleQuote(ByVal strString As String) As String
        Return strString.Replace(" ", " ")
    End Function
    Public Shared Function GetFileName(ByVal str As String) As String
        str = str.Replace(" ", "-")
        str = str.Replace("_", "-")
        str = str.Replace("'", "")
        str = str.Replace("#", "")
        str = str.Replace("%", "")
        str = str.Replace("/", "")
        str = str.Replace("\", "")
        str = str.Replace(":", "")
        str = str.Replace("*", "")
        str = str.Replace("?", "")
        str = str.Replace("<", "")
        str = str.Replace(">", "")
        str = str.Replace("|", "")
        str = str.Replace("""", "")
        str = str.Replace("&quot;", "")
        Return str
    End Function
    Public Function RandomNumber(ByVal MaxNumber As Integer, _
       Optional ByVal MinNumber As Integer = 0) As Integer

        'initialize random number generator
        Dim r As New Random(System.DateTime.Now.Millisecond)

        'if passed incorrect arguments, swap them
        'can also throw exception or return 0

        If MinNumber > MaxNumber Then
            Dim t As Integer = MinNumber
            MinNumber = MaxNumber
            MaxNumber = t
        End If

        Return r.Next(MinNumber, MaxNumber)

    End Function
    Public Shared Function FormatDate(ByVal dt As Object) As String
        Dim str As String = ""

        If Not IsDBNull(dt) Then
            If IsDate(dt) Then
                If FormatDateTime(dt, DateFormat.ShortDate) <> "1/1/1900" Then
                    str = FormatDateTime(dt, DateFormat.ShortDate)
                End If
            End If
        End If

        Return str
    End Function

    Public Shared Function GetValidDate(ByVal dt As String) As Date
        Dim dt1 As Date = "1/1/1900"

        If IsDate(dt) Then
            dt1 = CDate(dt)
        End If

        Return dt1
    End Function

    Public Shared Function GetValidDateTime(ByVal dt As String) As Date
        Dim dt1 As Date = "1/1/1900"

        If IsDate(dt) Then
            dt1 = CDate(dt & " " & Format(Now, "hh:mm:ss tt").ToString)
        End If

        Return dt1
    End Function

    Public Shared Function GetValidTime(ByVal dt As String) As Date
        Dim dt1 As Date = "1/1/1900"

        If dt <> "" Then
            dt = "1/1/2000 " & dt
            If IsDate(dt) Then
                dt1 = CDate(dt)
            End If
        End If

        Return dt1
    End Function

    Public Shared Function FormatTime(ByVal dt As Object) As String
        Dim str As String = ""

        If Not IsDBNull(dt) Then
            If IsDate(dt) Then
                If FormatDateTime(dt, DateFormat.ShortDate) <> "1/1/1900" Then
                    str = Format(CDate(dt), "hh:mm:ss tt").ToString
                End If
            End If
        End If

        Return str
    End Function
    Public Shared Function FormatMoney(ByVal amt As Object) As String
        Dim str As String = ""
        If Not IsDBNull(amt) Then
            If IsNumeric(amt) Then
                If amt <> 0 Then
                    str = FormatNumber(amt, 2, TriState.True, TriState.UseDefault, TriState.False)
                End If
            End If
        End If

        Return str
    End Function

    Public Shared Function FormatInteger(ByVal val As Object) As String
        Dim str As String = ""

        If Not IsDBNull(val) Then
            If IsNumeric(val) Then
                If val <> 0 Then
                    str = val
                End If
            End If
        End If

        Return str
    End Function

    Public Shared Function GetValidNumeric(ByVal val As String) As String
        Dim str As String = "0"

        If IsNumeric(val) Then
            str = val
        End If

        Return str
    End Function

    Public Shared Function FormatD2(ByVal dt As Object) As String
        Dim str As String = ""

        If Not IsDBNull(dt) Then
            If IsDate(dt) Then
                If FormatDateTime(dt, DateFormat.ShortDate) <> "1/1/1900" Then
                    str = FormatDateTime(dt, DateFormat.ShortDate)
                End If
            End If
        End If

        Return str
    End Function

    Public Shared Function FormatD3(ByVal dt As Object) As String
        Dim str As String = ""

        If Not IsDBNull(dt) Then
            If IsDate(dt) Then
                If FormatDateTime(dt, DateFormat.ShortDate) <> "1/1/1900" Then
                    str = Format(dt, "d-MMM-yy").ToString
                End If
            End If
        End If

        Return str
    End Function

    Public Shared Function FormatD4(ByVal dt As Object) As String
        Dim str As String = ""

        If Not IsDBNull(dt) Then
            If IsDate(dt) Then
                If FormatDateTime(dt, DateFormat.ShortDate) <> "1/1/1900" Then
                    str = Format(dt, "d-MMM-yy").ToString & ", " & DateAndTime.WeekdayName(dt.DayOfWeek + 1, True)
                End If
            End If
        End If

        Return str
    End Function
    ''' <summary>
    ''' "Get Money with $ sign"
    ''' </summary>
    ''' <param name="obj"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetFormatedMoney(ByVal obj As Object) As String
        Dim str As String = ""
        If obj.ToString() <> "" Then
            If IsNumeric(obj.ToString()) Then
                str = Convert.ToDecimal(obj).ToString("C", CultureInfo.CurrentCulture)
            End If
        End If
        Return str.Replace("(", "-").Replace(")", "")
    End Function
    Public Shared Function GetAuctionInvitedBidders(ByVal auction_id As Integer, ByVal is_for_fedex As Boolean) As DataTable
        Dim dt As DataTable = New DataTable
        Dim strQuery As String = "select A.buyer_id,ISNULL(B.contact_title,'') As contact_title,ISNULL(B.mobile,'') AS mobile,ISNULL(B.email,'') as email,ISNULL(B.contact_first_name,'') + ' ' + ISNULL(B.contact_last_name,'') as name, ISNULL(B.company_name,'') AS company_name" & _
        " from dbo.fn_get_invited_buyers(" & auction_id.ToString() & ") A INNER JOIN tbl_reg_buyers B ON A.buyer_id=B.buyer_id " & IIf(is_for_fedex, "where isnull(B.discontinue,0)=0 and A.buyer_id not in (select buyer_id from tbl_auction_fedex_rates where auction_id=" & auction_id.ToString() & ")", "") & " order by A.buyer_id "

        dt = SqlHelper.ExecuteDatatable(strQuery)

        Return dt
    End Function
    Public Shared Function GetBidderInvitedAuctionsUpcoming(ByVal buyer_id As Integer) As DataTable
        Dim bidder_max_bid As Double = SqlHelper.ExecuteScalar("select isnull(max_amt_bid,0) from tbl_reg_buyers where buyer_id=" & buyer_id)
        Dim dt As DataTable = New DataTable
        Dim strQuery As String = ""
        If CommonCode.is_admin_user() Then
            strQuery = "select A.auction_type_id, A.auction_id,A.code,A.title,isnull(C.name,'') as product_category,isnull(D.name,'') as auction_type,A.start_date,A.end_date,A.display_end_time, dbo.auction_status(a.auction_id) as Status,'0' as rank,0 as max_bid_amt,0 as bidder_max_bid_amt from [tbl_auctions] A " & _
           "left join tbl_master_product_categories C on A.product_catetory_id=C.product_catetory_id " & _
           "left join tbl_auction_types D on A.auction_type_id=D.auction_type_id " & _
           "where isnull(A.discontinue,0)=0 and ISNULL(A.reserve_price,0)<=" & bidder_max_bid & " and A.is_active=1 and (dbo.is_buyer_invited(A.auction_id," & buyer_id & ")=1 or A.show_relevant_bidders_only=0) and dbo.get_auction_status(a.auction_id)=2"
        Else
            strQuery = "select A.auction_type_id, A.auction_id,A.code,A.title,isnull(C.name,'') as product_category,isnull(D.name,'') as auction_type,A.start_date,A.end_date,A.display_end_time, dbo.auction_status(a.auction_id) as Status,'0' as rank,0 as max_bid_amt,0 as bidder_max_bid_amt from [tbl_auctions] A " & _                        "inner join [tbl_reg_seller_user_mapping] B on A.seller_id=B.seller_id " & _                        "left join tbl_master_product_categories C on A.product_catetory_id=C.product_catetory_id " & _                         "left join tbl_auction_types D on A.auction_type_id=D.auction_type_id " & _
                         "where isnull(A.discontinue,0)=0 and B.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " and ISNULL(A.reserve_price,0)<=" & bidder_max_bid & " and A.is_active=1  And (dbo.is_buyer_invited(A.auction_id," & buyer_id & ")=1  or A.show_relevant_bidders_only=0) and dbo.get_auction_status(a.auction_id)=2"
        End If
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function
    Public Shared Function GetBidderInvitedAuctionsLive(ByVal buyer_id As Integer) As DataTable
        Dim bidder_max_bid As Double = SqlHelper.ExecuteScalar("select isnull(max_amt_bid,0) from tbl_reg_buyers where buyer_id=" & buyer_id)
        Dim dt As DataTable = New DataTable
        Dim strQuery As String = ""
        If CommonCode.is_admin_user() Then
            strQuery = "select A.auction_type_id, A.auction_id,A.code,A.title,isnull(C.name,'') as product_category,isnull(D.name,'') as auction_type,A.start_date,A.end_date,A.display_end_time, dbo.auction_status(a.auction_id) as Status,case a.auction_type_id when 3 then dbo.buyer_bid_rank(a.auction_id," & buyer_id & ") else '0' end as rank,0 as max_bid_amt,0 as bidder_max_bid_amt from [tbl_auctions] A " & _
           "left join tbl_master_product_categories C on A.product_catetory_id=C.product_catetory_id " & _
           "left join tbl_auction_types D on A.auction_type_id=D.auction_type_id " & _
           "where isnull(A.discontinue,0)=0 and ISNULL(A.reserve_price,0)<=" & bidder_max_bid & "  and A.is_active=1 and (dbo.is_buyer_invited(A.auction_id," & buyer_id & ")=1 or A.show_relevant_bidders_only=0) and dbo.get_auction_status(a.auction_id)=1"
        Else
            strQuery = "select A.auction_type_id,A.auction_id,A.code,A.title,isnull(C.name,'') as product_category,isnull(D.name,'') as auction_type,A.start_date,A.end_date,A.display_end_time, dbo.auction_status(a.auction_id) as Status, case a.auction_type_id when 3 then dbo.buyer_bid_rank(a.auction_id," & buyer_id & ") else '0' end as rank,0 as max_bid_amt,0 as bidder_max_bid_amt from [tbl_auctions] A " & _                        "inner join [tbl_reg_seller_user_mapping] B on A.seller_id=B.seller_id " & _                        "left join tbl_master_product_categories C on A.product_catetory_id=C.product_catetory_id " & _                         "left join tbl_auction_types D on A.auction_type_id=D.auction_type_id " & _
                         "where isnull(A.discontinue,0)=0 and B.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " and ISNULL(A.reserve_price,0)<=" & bidder_max_bid & " and A.is_active=1 And (dbo.is_buyer_invited(A.auction_id," & buyer_id & ")=1  or A.show_relevant_bidders_only=0) and dbo.get_auction_status(a.auction_id)=1"
        End If
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function
    Public Shared Function GetBidderWonAuctions(ByVal buyer_id As Integer) As DataTable
        Dim dt As DataTable = New DataTable
        Dim strQuery As String = ""

        If CommonCode.is_admin_user() Then
            strQuery = "select distinct A.auction_type_id, A.auction_id,A.code,A.title,A.display_end_time,isnull(C.name,'') as product_category,dbo.get_auction_status(A.auction_id) as status_id,isnull(D.name,'') as auction_type,A.start_date,A.end_date,A.display_end_time,dbo.auction_status(a.auction_id) as Status,'0' as rank,ISNULL((select bid_amount from tbl_auction_bids where bid_id=A.rank1_bid_id),0) as max_bid_amt,0 as bidder_max_bid_amt " & _
                " from tbl_auctions A left join tbl_master_product_categories C on A.product_catetory_id=C.product_catetory_id left join tbl_auction_types D on A.auction_type_id=D.auction_type_id " & _
                "where isnull(A.discontinue,0)=0 and A.auction_type_id=3 and dbo.get_auction_status(A.auction_id)=3 and exists(select bid_id from tbl_auction_bids where auction_id=A.auction_id and buyer_id=" & buyer_id & ") and A.rank1_bid_id in (select bid_id from tbl_auction_bids where auction_id=A.auction_id and buyer_id=" & buyer_id & ")"
        Else
            strQuery = "select distinct A.auction_type_id, A.auction_id,A.code,A.title,A.display_end_time,isnull(C.name,'') as product_category,dbo.get_auction_status(A.auction_id) as status_id,isnull(D.name,'') as auction_type,A.start_date,A.end_date,A.display_end_time,dbo.auction_status(a.auction_id) as Status,'0' as rank,ISNULL((select bid_amount from tbl_auction_bids where bid_id=A.rank1_bid_id),0) as max_bid_amt,0 as bidder_max_bid_amt " & _
                " from tbl_auctions A left join tbl_master_product_categories C on A.product_catetory_id=C.product_catetory_id left join tbl_auction_types D on A.auction_type_id=D.auction_type_id " & _
          "inner join [tbl_reg_seller_user_mapping] E on A.seller_id=E.seller_id " & _
          "where isnull(A.discontinue,0)=0 and A.auction_type_id=3 and dbo.get_auction_status(A.auction_id)=3 and exists(select bid_id from tbl_auction_bids where auction_id=A.auction_id and buyer_id=" & buyer_id & ") and A.rank1_bid_id in (select bid_id from tbl_auction_bids where auction_id=A.auction_id and buyer_id=" & buyer_id & ") and E.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
          
        End If
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function
    Public Shared Function GetBidderLostAuctions(ByVal buyer_id As Integer) As DataTable
        Dim dt As DataTable = New DataTable
        Dim strQuery As String = ""

        If CommonCode.is_admin_user() Then
            strQuery = "select distinct A.auction_type_id, A.auction_id,A.code,A.title,A.display_end_time,isnull(C.name,'') as product_category,dbo.get_auction_status(A.auction_id) as status_id,isnull(D.name,'') as auction_type,A.start_date,A.end_date,A.display_end_time,dbo.auction_status(a.auction_id) as Status,'0' as rank,ISNULL((select bid_amount from tbl_auction_bids where bid_id=A.rank1_bid_id),0) as max_bid_amt,ISNULL((select max(bid_amount) from tbl_auction_bids where auction_id=A.auction_id and buyer_id=" & buyer_id & "),0) as bidder_max_bid_amt " & _
                " from tbl_auctions A left join tbl_master_product_categories C on A.product_catetory_id=C.product_catetory_id left join tbl_auction_types D on A.auction_type_id=D.auction_type_id " & _
                "where isnull(A.discontinue,0)=0 and A.auction_type_id=3 and dbo.get_auction_status(A.auction_id)=3 and exists(select bid_id from tbl_auction_bids where auction_id=A.auction_id and buyer_id=" & buyer_id & ") and A.rank1_bid_id not in(select bid_id from tbl_auction_bids where auction_id=A.auction_id and buyer_id=" & buyer_id & ")"
            ' strQuery = "select distinct A.auction_id,A.code,A.title,A.display_end_time,isnull(C.name,'') as product_category,dbo.get_auction_status(A.auction_id) as status_id,isnull(D.name,'') as auction_type," & _
            '"from tbl_auctions A inner join tbl_auction_bids B on A.auction_id=B.auction_id left join tbl_master_product_categories C on A.product_catetory_id=C.product_catetory_id left join tbl_auction_types D on A.auction_type_id=D.auction_type_id " & _
            '"where Convert(varchar,A.display_end_time,101) <=Convert(varchar,getdate(),101) and A.rank1_bid_id<>B.bid_id " & _
            '"and B.buyer_id=" & buyer_id & " and A.auction_id<>(select auction_id from tbl_auction_bids where bid_id=A.rank1_bid_id)"
        Else
            strQuery = "select distinct A.auction_type_id, A.auction_id,A.code,A.title,A.display_end_time,isnull(C.name,'') as product_category,dbo.get_auction_status(A.auction_id) as status_id,isnull(D.name,'') as auction_type,A.start_date,A.end_date,A.display_end_time,dbo.auction_status(a.auction_id) as Status,'0' as rank,ISNULL((select bid_amount from tbl_auction_bids where bid_id=A.rank1_bid_id),0) as max_bid_amt,ISNULL((select max(bid_amount) from tbl_auction_bids where auction_id=A.auction_id and buyer_id=" & buyer_id & "),0) as bidder_max_bid_amt " & _
                " from tbl_auctions A left join tbl_master_product_categories C on A.product_catetory_id=C.product_catetory_id left join tbl_auction_types D on A.auction_type_id=D.auction_type_id " & _
          "inner join [tbl_reg_seller_user_mapping] E on A.seller_id=E.seller_id " & _
          "where isnull(A.discontinue,0)=0 and A.auction_type_id=3 and dbo.get_auction_status(A.auction_id)=3 and exists(select bid_id from tbl_auction_bids where auction_id=A.auction_id and buyer_id=" & buyer_id & ") and A.rank1_bid_id not in(select bid_id from tbl_auction_bids where auction_id=A.auction_id and buyer_id=" & buyer_id & ") and E.user_id=" & CommonCode.Fetch_Cookie_Shared("user_id")
        End If
        'HttpContext.Current.Response.Write(strQuery)
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function
    Public Shared Function GetMessageInvitedBidders(ByVal invitaion_id As Integer) As DataTable
        Dim dt As DataTable = New DataTable
        Dim strQuery As String = ""
        strQuery = "select A.buyer_id,ISNULL(B.contact_title,'') As contact_title,ISNULL(B.mobile,'') AS mobile,ISNULL(B.email,'') as email,ISNULL(B.contact_first_name,'') + ' ' + ISNULL(B.contact_last_name,'') as name, ISNULL(B.company_name,'') AS company_name " & _
        "from dbo.fn_get_message_invited_buyers(" & invitaion_id.ToString() & ") A INNER JOIN tbl_reg_buyers B ON A.buyer_id=B.buyer_id order by B.company_name "
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function
    Public Shared Function GetMessageInvitedSellers(ByVal invitaion_id As Integer) As DataTable
        Dim dt As DataTable = New DataTable
        Dim strQuery As String = ""
        strQuery = "select A.user_id,ISNULL(B.title,'') As contact_title,ISNULL(B.mobile,'') AS mobile,ISNULL(B.email,'') as email,ISNULL(B.first_name,'') + ' ' + ISNULL(B.last_name,'') as name, '' AS company_name " & _
        "from dbo.fn_get_message_invited_sellers(" & invitaion_id.ToString() & ") A INNER JOIN tbl_sec_users B ON A.user_id=B.user_id order by name "
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function

    ''' <summary>
    ''' Check whether the logged in user is admin master
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Public Shared Function is_super_admin() As Boolean

        Dim userName As String = Fetch_Cookie_Shared("username")
        Dim tmp_super_admin As Boolean = False
        If userName <> "" Then
            If userName.ToUpper() = "ADMIN" Then
                tmp_super_admin = True
            End If
        End If

        If Not String.IsNullOrEmpty(Fetch_Cookie_Shared("is_super_admin")) Then
            tmp_super_admin = IIf(Fetch_Cookie_Shared("is_super_admin") = "1", True, False)
        End If

        Return tmp_super_admin
    End Function

    Public Shared Function is_admin_user() As Boolean
        Dim userName As String = Fetch_Cookie_Shared("username")
        Dim is_admin As Boolean = False
        If userName <> "" Then
            If userName.ToUpper() = "ADMIN" Or is_super_admin() Then
                is_admin = True
            End If
        ElseIf Not String.IsNullOrEmpty(Fetch_Cookie_Shared("is_admin")) Then
            is_admin = IIf(Fetch_Cookie_Shared("is_admin") = "1", True, False)
        End If

        Return is_admin
    End Function
    Public Shared Function is_sales_rep() As Boolean
        Dim userid As String = CommonCode.Fetch_Cookie_Shared("user_id")
        Dim sales_rep As Boolean = SqlHelper.ExecuteScalar("if exists(select mapping_id from tbl_sec_user_profile_mapping where profile_id=2 and user_id='" & userid & "') select 1 else select 0")
        Return sales_rep
    End Function
    Private Function HasPermission(ByVal dt As DataTable, ByVal ValueToSearch As String) As Boolean
        Dim does_exist As Boolean = False
        For Each dr As DataRow In dt.Rows
            If dr("name").ToString().ToLower() = ValueToSearch.ToLower() Then
                does_exist = True
                Exit For
            End If
        Next
        Return does_exist
    End Function
    Public Shared Function check_by_input_admin_user(ByVal _uid As Int32) As Boolean
        Dim is_admin As Boolean = False
        If _uid > 0 Then
            Dim userName As String = SqlHelper.ExecuteScalar("select username from tbl_sec_users where user_id=" & _uid & "")

            If userName <> "" Then
                If userName.ToUpper() = "ADMIN" Then
                    is_admin = True
                End If
            End If
        End If

        Return is_admin
    End Function

    Public Shared Function fetch_profile_user_emails(ByVal profile_code As String) As String
        Dim str As String = "select isnull((select top 1 isnull(email_records,'') as email_records from tbl_master_email_template where code='" & profile_code & "'),'')"
        Dim email As String = SqlHelper.ExecuteScalar(str)
        Return email
    End Function

    Public Shared Sub insert_system_log(ByVal Description As String, ByVal FunctionName As String, ByVal Unique_id As Integer, Optional ByVal PageName As String = "", Optional ByVal ModuleName As String = "")
        Dim ThisPage As String = HttpContext.Current.Request.FilePath
        Dim SlashPos As Integer = InStrRev(ThisPage, "/")
        Dim userid As Integer = 0
        userid = CommonCode.Fetch_Cookie_Shared("user_id")

        If PageName = "" Then
            PageName = Right(ThisPage, Len(ThisPage) - SlashPos)
        End If
        Dim strIP As String = System.Web.HttpContext.Current.Request.UserHostAddress
        Dim str As String = ""
        str = "INSERT INTO tbl_sec_system_log(description, module_name, page_name, function_name,  row_create_date, row_create_user_id, Unique_id, ip_address) VALUES" & _
              "('" & Description & "','" & ModuleName & "','" & PageName & "','" & FunctionName & "',getdate()," & userid & "," & Unique_id & ",'" & strIP & "')"
        SqlHelper.ExecuteNonQuery(str)

    End Sub

    Function GetThumbnailFileStamp(ByVal Filename As String, ByVal str_thumb As String) As String
        Dim i As Integer
        i = Filename.Length
        'System.Web.HttpContext.Current.Response.Write("<br>found-" & Filename)
        'System.Web.HttpContext.Current.Response.End()
        Do While i >= 1
            If Mid(Filename, i, 1) = "." Then
                Exit Do
            End If
            i = i - 1
        Loop
        Return Mid(Filename, 1, i - 1) & str_thumb & Mid(Filename, i, Filename.Length)
    End Function
    Function IsFileExist(ByVal FilePath As String) As Boolean
        Dim fso
        Dim flag As Boolean = False
        fso = CreateObject("Scripting.FileSystemObject")
        If fso.FileExists(FilePath) Then
            flag = True
        End If
        fso = Nothing
        Return flag
    End Function
    Public Function GetThumbNail(ByVal ImageUrl As String, ByVal ThumbHeight As Integer, ByVal ThumbWidth As Integer, ByVal Thumb As String) As String
        If Not IsFileExist(ImageUrl) Then
            Return ""
        End If

        If IsFileExist(GetThumbnailFileStamp(ImageUrl, Thumb)) Then

            Return GetThumbnailFileStamp(ImageUrl, Thumb)
        End If

        Dim imgMyImage As System.Drawing.Image = System.Drawing.Image.FromFile(ImageUrl)
        Dim w, h As Integer

        h = imgMyImage.Height
        w = imgMyImage.Width
        If imgMyImage.Height > imgMyImage.Width Then
            If imgMyImage.Height > ThumbHeight Then
                h = ThumbHeight
                w = (h / imgMyImage.Height) * imgMyImage.Width
            End If
        ElseIf imgMyImage.Height < imgMyImage.Width Then
            If imgMyImage.Width > ThumbWidth Then
                w = ThumbWidth
                h = (w / imgMyImage.Width) * imgMyImage.Height
            End If
        Else
            w = ThumbWidth
            h = ThumbHeight
        End If

        Dim thumbnail As System.Drawing.Image = New Bitmap(w, h)
        Dim graphic As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(thumbnail)

        graphic.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
        graphic.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        graphic.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
        graphic.CompositingQuality = Drawing2D.CompositingQuality.HighQuality

        graphic.DrawImage(imgMyImage, 0, 0, w, h)

        Dim info() As System.Drawing.Imaging.ImageCodecInfo
        info = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders()
        Dim encoderParameters As System.Drawing.Imaging.EncoderParameters
        encoderParameters = New System.Drawing.Imaging.EncoderParameters(1)
        encoderParameters.Param(0) = New System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 90)
        System.Web.HttpContext.Current.Response.ContentType = "image/jpeg"

        Dim strThumbImageUrl As String

        strThumbImageUrl = GetThumbnailFileStamp(ImageUrl, Thumb)

        thumbnail.Save(strThumbImageUrl, info(1), encoderParameters)

        Return strThumbImageUrl
    End Function

    Public Function Send_Confirm_Receipt_Mail(ByVal _id As Int32, ByVal page_name As String, ByVal type As String, Optional ByVal for_payment As Boolean = False) As String

        Dim strMail As String = ""
        If _id > 0 Then
            Dim objEmail As New Email
            'If type.ToLower = "b" Then
            Dim strFrom As String = "<!--Add the middle section here-->"
            Dim strTo As String = "<!--middle section ends here-->"

            'HttpContext.Current.Response.Write(SqlHelper.of_FetchKey("ServerHttp") & "/" & page_name & "?e=1&t=" & type & "&b=" & _id)
            Dim strHTML As String = getHtml(SqlHelper.of_FetchKey("ServerHttp") & "/" & page_name & "?e=1&t=" & type & "&b=" & _id & "&u=" & Security.EncryptionDecryption.EncryptValueFormatted(CommonCode.Fetch_Cookie_Shared("user_id")))


            If strHTML.IndexOf(strFrom) <> -1 And strHTML.IndexOf(strTo) <> -1 Then
                strMail = strHTML.Substring(strHTML.IndexOf(strFrom) + Len(strFrom), strHTML.IndexOf(strTo) - strHTML.IndexOf(strFrom) - Len(strFrom))
                If Not for_payment Then
                    objEmail.Send_Mail_Confirm_Receipt(_id, type, strMail)
                Else
                    strMail = strHTML & "<div style='clear: both;'><br /><br />Please <a href='<<payment_link>>' target='_blank' style='font-size:14px;font-weight:bold;'>click here</a> to make payment."
                End If

                'HttpContext.Current.Response.Write(strMail & "Inside")
                'HttpContext.Current.Response.Write(strMail)
            End If
            'HttpContext.Current.Response.Write(strHTML)
            'HttpContext.Current.Response.Write(SqlHelper.of_FetchKey("ServerHttp") & "/" & page_name & "?e=1&t=" & type & "&b=" & _id)
            '
            'End If

            objEmail = Nothing


        End If
        Return strMail
    End Function

    Public Function getHtml(ByVal url As String) As String
        Dim myWebRequest As System.Net.HttpWebRequest = DirectCast(System.Net.HttpWebRequest.Create(url), System.Net.HttpWebRequest)
        myWebRequest.Method = "GET"
        Dim myWebResponse As System.Net.HttpWebResponse = DirectCast(myWebRequest.GetResponse(), System.Net.HttpWebResponse)
        Dim myWebSource As New StreamReader(myWebResponse.GetResponseStream())
        Dim myPageSource As String = String.Empty
        myPageSource = myWebSource.ReadToEnd()
        myWebResponse.Close()
        'If myPageSource.Trim = "" Then
        '    HttpContext.Current.Response.Write("blank!!")
        'Else
        '    HttpContext.Current.Response.Write(myPageSource)
        'End If
        'HttpContext.Current.Response.Write("blank!!---" & myPageSource)
        Return myPageSource
    End Function

    Public Function GetEmail_ByCode(ByVal email_code As String, Optional ByVal qry As String = "") As DataSet
        Dim ds As DataSet = New DataSet
        'HttpContext.Current.Response.Write(email_code)
        Dim strQuery As String = ""
        strQuery = "select isnull(A.email_name,'') as email_name,isnull(A.email_subject,'') as email_subject, cast(isnull(B.header_html,'') as nvarchar(max))+cast(isnull(A.email_html,'') as  nvarchar(max))+cast(isnull(C.footer_html,'')as nvarchar(max)) as html_body " & _
        "from tbl_master_emails A INNER JOIN tbl_master_email_headers B ON A.header_id=B.header_id INNER JOIN tbl_master_email_footers C ON A.footer_id=C.footer_id where A.is_active=1 and B.is_active=1 and C.is_active=1 and A.email_code='" & email_code & "'"
        If qry.Trim <> "" Then
            strQuery = strQuery & ";" & qry
        End If
        ds = SqlHelper.ExecuteDataset(strQuery)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            ds.Tables(0).Rows(0)("html_body").ToString().Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
        End If
        Return ds
    End Function
    Public Function GetEmail_ByCode_AddBody(ByVal email_code As String, ByVal concatenate_body As String, Optional ByVal qry As String = "") As DataSet
        Dim ds As DataSet = New DataSet
        'HttpContext.Current.Response.Write(email_code)
        Dim strQuery As String = ""
        strQuery = "select isnull(A.email_name,'') as email_name,isnull(A.email_subject,'') as email_subject, cast(isnull(B.header_html,'') as nvarchar(max))+cast(isnull(A.email_html,'') as  nvarchar(max))+cast('" & concatenate_body.Replace("'", "''") & "' as nvarchar(max))+cast(isnull(C.footer_html,'')as nvarchar(max)) as html_body " & _
        "from tbl_master_emails A INNER JOIN tbl_master_email_headers B ON A.header_id=B.header_id INNER JOIN tbl_master_email_footers C ON A.footer_id=C.footer_id where A.is_active=1 and B.is_active=1 and C.is_active=1 and A.email_code='" & email_code & "'"
        If qry.Trim <> "" Then
            strQuery = strQuery & ";" & qry
        End If
        ds = SqlHelper.ExecuteDataset(strQuery)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            ds.Tables(0).Rows(0)("html_body").ToString().Replace("<<site_url>>", SqlHelper.of_FetchKey("ServerHttp"))
        End If
        Return ds
    End Function
    Function UploadFile(ByVal InputFileControl As Web.UI.HtmlControls.HtmlInputFile, ByVal FolderPath As String, Optional ByVal SaveAs As String = "", Optional ByVal UploadonRemoveServer As Boolean = True) As String

        Dim inpFileUp As New HtmlInputFile
        Dim ServerFolderPath As String

        If InputFileControl.PostedFile.FileName = "" Then
            Return ""
        Else
            If Not IsFolderExist(GetMapPath(FolderPath)) Then
                Return ""
            End If
            Dim strfile As String
            Dim strfile_as As String
            Dim orgstrfile As String


            'Return ""
            inpFileUp = CType(InputFileControl, HtmlInputFile)

            If SaveAs = "" Then
                strfile = inpFileUp.PostedFile.FileName
                strfile_as = strfile
                '   strfile = Path.GetFileName(strfile)
            Else
                strfile = inpFileUp.PostedFile.FileName
                strfile_as = SaveAs
            End If
            orgstrfile = strfile

            strfile = GetMapPath(FolderPath) & strfile_as
            'HttpContext.Current.Response.Write(inpFileUp.PostedFile.FileName)
            'Return ""
            inpFileUp.PostedFile.SaveAs(strfile)
            GetThumbNail_New(strfile, 230, 275, "_230")
            GetThumbNail_New(strfile, 375, 330, "_375")
            GetThumbNail_New(strfile, 95, 105, "_95")
            GetThumbNail_New(strfile, 500, 500, "_500")
            'GetThumbNail_New(strfile, 120, 100, "_thumb1")
            'inpFileUp = Nothing
            inpFileUp.Dispose()


            Return orgstrfile
        End If
    End Function
    Function IsFolderExist(ByVal FolderPath As String) As Boolean
        Dim fso
        Dim flag As Boolean = False
        fso = CreateObject("Scripting.FileSystemObject")
        If fso.FolderExists(FolderPath) Then
            flag = True
        End If
        fso = Nothing
        Return flag
    End Function
    Function GetMapPath(ByVal Path As String) As String
        Return HttpContext.Current.Server.MapPath(Path)
    End Function

    Public Function GetThumbNail_New(ByVal ImageUrl As String, ByVal ThumbHeight As Integer, ByVal ThumbWidth As Integer, ByVal Thumb As String) As String
        If Not IsFileExist(ImageUrl) Then
            Return ""
        End If

        Dim imgMyImage As System.Drawing.Image = System.Drawing.Image.FromFile(ImageUrl)
        Dim w, h As Integer

        h = imgMyImage.Height
        w = imgMyImage.Width
        If imgMyImage.Height > imgMyImage.Width Then
            If imgMyImage.Height > ThumbHeight Then
                h = ThumbHeight
                w = (h / imgMyImage.Height) * imgMyImage.Width
                If w > ThumbWidth Then
                    w = ThumbWidth
                    h = (w / imgMyImage.Width) * imgMyImage.Height
                End If
            End If
        ElseIf imgMyImage.Height < imgMyImage.Width Then
            If imgMyImage.Width > ThumbWidth Then
                w = ThumbWidth
                h = (w / imgMyImage.Width) * imgMyImage.Height
                If h > ThumbHeight Then
                    h = ThumbHeight
                    w = (h / imgMyImage.Height) * imgMyImage.Width
                End If
            End If
        Else
            If imgMyImage.Height > ThumbHeight Then
                h = ThumbHeight
                w = h
                If w > ThumbWidth Then
                    w = ThumbWidth
                    h = w
                End If
            Else
                h = imgMyImage.Height
                w = h
                If w > ThumbWidth Then
                    w = imgMyImage.Width
                    h = w
                End If
            End If
        End If

        Dim thumbnail As System.Drawing.Image = New Bitmap(w, h)
        Dim graphic As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(thumbnail)

        graphic.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
        graphic.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        graphic.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
        graphic.CompositingQuality = Drawing2D.CompositingQuality.HighQuality

        graphic.DrawImage(imgMyImage, 0, 0, w, h)

        Dim info() As System.Drawing.Imaging.ImageCodecInfo
        info = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders()
        Dim encoderParameters As System.Drawing.Imaging.EncoderParameters
        encoderParameters = New System.Drawing.Imaging.EncoderParameters(1)
        encoderParameters.Param(0) = New System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 90)
        System.Web.HttpContext.Current.Response.ContentType = "image/jpeg"

        Dim strThumbImageUrl As String
        Dim strThumbImageUrlnew As String

        strThumbImageUrl = GetThumbnailFileStamp(ImageUrl, Thumb & "_t")

        thumbnail.Save(strThumbImageUrl, info(1), encoderParameters)

        info = Nothing
        encoderParameters.Dispose()
        thumbnail.Dispose()
        imgMyImage.Dispose()
        graphic.Dispose()

        strThumbImageUrlnew = SetThumbNailFrame(strThumbImageUrl, ThumbHeight, ThumbWidth, Thumb)

        DeleteFile(strThumbImageUrl)

        Return strThumbImageUrlnew
    End Function
    Public Function SetThumbNailFrame(ByVal ImageUrl As String, ByVal ThumbHeight As Integer, ByVal ThumbWidth As Integer, ByVal Thumb As String) As String
        If Not IsFileExist(ImageUrl) Then
            Return ""
        End If

        Dim imgMyImage As System.Drawing.Image = System.Drawing.Image.FromFile(ImageUrl)
        Dim w, h As Integer

        h = imgMyImage.Height
        w = imgMyImage.Width

        Dim thumbnail As System.Drawing.Image = New Bitmap(ThumbWidth, ThumbHeight)
        Dim graphic As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(thumbnail)

        graphic.Clear(Color.White)
        graphic.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
        graphic.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        graphic.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
        graphic.CompositingQuality = Drawing2D.CompositingQuality.HighQuality

        Dim space_width As Integer = 0
        Dim space_height As Integer = 0
        space_width = (ThumbWidth - w) / 2
        space_height = (ThumbHeight - h) / 2
        graphic.DrawImageUnscaled(imgMyImage, space_width, space_height, w, h)

        Dim info() As System.Drawing.Imaging.ImageCodecInfo
        info = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders()
        Dim encoderParameters As System.Drawing.Imaging.EncoderParameters
        encoderParameters = New System.Drawing.Imaging.EncoderParameters(1)
        encoderParameters.Param(0) = New System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 90)
        System.Web.HttpContext.Current.Response.ContentType = "image/jpeg"

        Dim strThumbImageUrl As String

        strThumbImageUrl = GetThumbnailFileStamp(ImageUrl.Replace(Thumb & "_t", ""), Thumb)

        thumbnail.Save(strThumbImageUrl, info(1), encoderParameters)

        info = Nothing
        encoderParameters.Dispose()
        thumbnail.Dispose()
        imgMyImage.Dispose()
        graphic.Dispose()
        Return strThumbImageUrl
    End Function

    Sub DeleteFile(ByVal FilePath As String)
        Dim fso = CreateObject("Scripting.FileSystemObject")
        If fso.FileExists(FilePath) Then
            fso.DeleteFile(FilePath)
        End If
        fso = Nothing
        'fso.Dispose()
    End Sub
    'Public Function GetThumbNailFixedPath(ByVal ImagePath As String, ByVal ThumbHeight As Integer, ByVal ThumbWidth As Integer, ByVal strname As String, ByVal overwrite As Integer) As String
    '    Try

    '        Dim strThumbImageUrl As String
    '        Dim obj As New CommonClass
    '        If Not obj.IsFileExist(ImagePath) Then
    '            Return "NOT FOUND"
    '        End If
    '        strThumbImageUrl = GetThumbnailFileStamp(ImagePath, strname)
    '        If overwrite = 0 Then
    '            If obj.IsFileExist(strThumbImageUrl) Then
    '                Return "ALREADY EXIST"
    '            End If
    '        End If


    '        GetResizeImage(GetMapPath(ImagePath), GetMapPath(strThumbImageUrl), ThumbHeight, ThumbWidth)
    '        'Return GetMapPath(strThumbImageUrl)
    '        Return strThumbImageUrl

    '    Catch ex As Exception
    '        Return "ERR: " & ex.Message
    '    End Try

    'End Function
    Public Shared Function is_bidder_approval_agent() As Boolean
        If SqlHelper.of_FetchKey("bidder_approval_agent_profile_code") = CommonCode.Fetch_Cookie_Shared("profile_code") Then
            Return True
        Else
            Return False

        End If

    End Function
End Class
