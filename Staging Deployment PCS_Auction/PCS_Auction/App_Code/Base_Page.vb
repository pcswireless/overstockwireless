﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Threading
Imports System.Globalization

Public Class BasePage
    Inherits Page
    Protected lang As String
    Protected Overrides Sub InitializeCulture()
        If CommonCode.Fetch_Cookie_Shared("_lang") IsNot Nothing AndAlso CommonCode.Fetch_Cookie_Shared("_lang") <> "" Then
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(CommonCode.Fetch_Cookie_Shared("_lang"))
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(CommonCode.Fetch_Cookie_Shared("_lang"))
        Else
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US")
            Thread.CurrentThread.CurrentUICulture = New CultureInfo("en-US")
        End If
        MyBase.InitializeCulture()
    End Sub

   
End Class


