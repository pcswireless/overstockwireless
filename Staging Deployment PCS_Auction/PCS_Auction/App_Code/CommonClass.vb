﻿Imports System
Imports System.IO
Imports System.Drawing
Imports Microsoft.VisualBasic
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.Security


Public Class CommonClass
    Inherits System.Web.UI.Page


    Function UploadFile(ByVal InputFileControl As Web.UI.WebControls.FileUpload, ByVal FolderPath As String, Optional ByVal SaveAs As String = "", Optional ByVal UploadonRemoveServer As Boolean = True) As String

        Dim inpFileUp As New FileUpload
        If InputFileControl.PostedFile.FileName = "" Then
            Return ""
        Else
            If Not IsFolderExist(GetMapPath(FolderPath)) Then
                Return ""
            End If
            Dim strfile As String
            Dim orgstrfile As String

            inpFileUp = CType(InputFileControl, FileUpload)

            If SaveAs = "" Then
                strfile = inpFileUp.PostedFile.FileName
                '   strfile = Path.GetFileName(strfile)
            Else
                strfile = SaveAs
            End If
            orgstrfile = strfile

            strfile = GetMapPath(FolderPath) & "\" & strfile
            inpFileUp.PostedFile.SaveAs(strfile)
            'Return strfile
            Return strfile

        End If
    End Function
    Function GetThumbnailFileStamp(ByVal Filename As String, Optional ByVal strname As String = "_thumb") As String
        Dim i As Integer
        i = Filename.Length
        Do While i >= 1
            If Mid(Filename, i, 1) = "." Then
                Exit Do
            End If
            i = i - 1
        Loop
        Return Mid(Filename, 1, i - 1) & strname & Mid(Filename, i, Filename.Length)
    End Function
    Function IsFolderExist(ByVal FolderPath As String) As Boolean
        Dim flag As Boolean = False
        If System.IO.Directory.Exists(FolderPath) Then
            flag = True
        End If
        Return flag
    End Function

    Function GetMapPath(ByVal Path As String) As String
        Return Server.MapPath(Path)
    End Function

    Function IsFileExist(ByVal FilePath As String) As Boolean
        Dim flag As Boolean = False
        Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(GetMapPath(FilePath))
        If infoFile.Exists Then
            flag = True
        End If
        Return flag
    End Function

    Public Function GetThumbNailFixedPath(ByVal ImagePath As String, ByVal ThumbHeight As Integer, ByVal ThumbWidth As Integer, ByVal strname As String, ByVal overwrite As Integer) As String
        Try

            Dim strThumbImageUrl As String
            Dim obj As New CommonClass
            If Not obj.IsFileExist(ImagePath) Then
                Return "NOT FOUND"
            End If
            strThumbImageUrl = GetThumbnailFileStamp(ImagePath, strname)
            If overwrite = 0 Then
                If obj.IsFileExist(strThumbImageUrl) Then
                    Return "ALREADY EXIST"
                End If
            End If


            GetResizeImage(GetMapPath(ImagePath), GetMapPath(strThumbImageUrl), ThumbHeight, ThumbWidth)
            'Return GetMapPath(strThumbImageUrl)
            Return strThumbImageUrl

        Catch ex As Exception
            Return "ERR: " & ex.Message
        End Try

    End Function

    Public Sub GetResizeImage(ByVal ImageUrl As String, ByVal DestinationImageUrl As String, ByVal ThumbHeight As Integer, ByVal ThumbWidth As Integer)
        If Not File.Exists(ImageUrl) Then
            Exit Sub
        End If

        Dim imgMyImage As System.Drawing.Image = System.Drawing.Image.FromFile(ImageUrl)
        Dim w, h As Integer

        h = imgMyImage.Height
        w = imgMyImage.Width
        If imgMyImage.Height > imgMyImage.Width Then
            h = ThumbHeight
            w = (h / imgMyImage.Height) * imgMyImage.Width
        ElseIf imgMyImage.Height < imgMyImage.Width Then
            w = ThumbWidth
            h = (w / imgMyImage.Width) * imgMyImage.Height
        Else
            w = ThumbWidth
            h = ThumbHeight
        End If

        Dim thumbnail As System.Drawing.Image = New Bitmap(w, h)
        Dim graphic As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(thumbnail)

        graphic.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
        graphic.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        graphic.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
        graphic.CompositingQuality = Drawing2D.CompositingQuality.HighQuality

        graphic.DrawImage(imgMyImage, 0, 0, w, h)

        Dim info() As System.Drawing.Imaging.ImageCodecInfo
        info = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders()
        Dim encoderParameters As System.Drawing.Imaging.EncoderParameters
        encoderParameters = New System.Drawing.Imaging.EncoderParameters(1)
        encoderParameters.Param(0) = New System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 90)
        System.Web.HttpContext.Current.Response.ContentType = "image/jpeg"

        thumbnail.Save(DestinationImageUrl, info(1), encoderParameters)

        graphic.Dispose()
        thumbnail.Dispose()
        graphic = Nothing
        thumbnail = Nothing
    End Sub


    

End Class

