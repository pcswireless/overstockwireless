﻿Imports Telerik.Web.UI
Partial Class SalesHome
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                hid_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
                Dim ds As New DataSet
                Dim sel_qry As String = ""
                If CommonCode.is_admin_user() Then
                    sel_qry = "select (first_name + ' ' + last_name) as name,email,isnull(mobile,0) as phone1,isnull(image_path,'') as image_path,(select count(query_id) from tbl_auction_queries A left join tbl_auctions Q WITH (NOLOCK) on A.auction_id=Q.auction_id where (Q.is_active=1 or isnull(A.auction_id,0)=0) and isnull(parent_query_id,0)=0 and isnull(admin_marked,0)=0 and query_id not in (select isnull(parent_query_id,0) from tbl_auction_queries)) as query from tbl_sec_users where user_id=" & hid_user_id.Value & ""
                Else
                    sel_qry = "select (first_name + ' ' + last_name) as name,email,isnull(mobile,0) as phone1,isnull(image_path,'') as image_path,(select count(query_id) from tbl_auction_queries A left join tbl_auctions Q WITH (NOLOCK) on A.auction_id=Q.auction_id where (Q.is_active=1 or isnull(A.auction_id,0)=0) and isnull(parent_query_id,0)=0 and isnull(admin_marked,0)=0 and buyer_id in (select distinct buyer_id from tbl_reg_sale_rep_buyer_mapping where user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ") and query_id not in (select isnull(parent_query_id,0) from tbl_auction_queries)) as query from tbl_sec_users where user_id=" & hid_user_id.Value & ""
                End If
                'Response.Write(sel_qry & "<br><br>")
                ds = SqlHelper.ExecuteDataset(sel_qry)
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0)("image_path") <> "" Then
                        img_sale_rep.ImageUrl = "/Upload/users/" & hid_user_id.Value & "/" & ds.Tables(0).Rows(0)("image_path")
                    Else
                        img_sale_rep.ImageUrl = "/images/buyer_icon.gif"
                    End If
                    lbl_sale_rep_name.Text = "<a href='javascript:void(0);' style='color:#4B708D;text-decoration:none;font-size:14px;font-weight:bold;' onmouseout=""hide_tip_new();"" onmouseover=""tip_new('/users/user_mouse_over.aspx?i=" & hid_user_id.Value & "','270','white','true');"">" & ds.Tables(0).Rows(0)("name") & "</a>"
                    lbl_sale_rep_email.Text = "<a style='color:#363A83;text-decoration:none;font-size:13px;font-weight:bold;' href='mailto:" & ds.Tables(0).Rows(0)("email") & "'>" & ds.Tables(0).Rows(0)("email") & "</a>"
                    lbl_sale_rep_query.Text = ds.Tables(0).Rows(0)("query") & " Pending Queries"
                    lbl_sale_rep_phone.Text = "Ph: " & ds.Tables(0).Rows(0)("phone1")
                    hid_search.Value = "%%"
                    'fun_search(hid_search.Value)
                End If



            Else
                hid_user_id.Value = 0
            End If
        End If
    End Sub


    Protected Sub RadGrid1_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        RadGrid1.DataSource = SqlHelper.ExecuteDatatable(fun_search(hid_search.Value))
        'set_grid()
    End Sub
    Protected Sub radgrid1_itemcommand(ByVal source As Object, ByVal e As GridCommandEventArgs) Handles RadGrid1.ItemCommand
        If e.commandname = radgrid.expandcollapsecommandname And TypeOf e.item Is griddataitem Then
            DirectCast(e.item, griddataitem).childitem.findcontrol("innercontainer").visible = Not e.item.expanded
        End If
    End Sub
    Protected Sub radgrid1_itemcreated(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGrid1.ItemCreated
        If TypeOf e.item Is gridnestedviewitem Then
            e.Item.FindControl("innercontainer").Visible = (DirectCast(e.Item, GridNestedViewItem)).ParentItem.Expanded

        End If
    End Sub

    Protected Function fun_search(ByVal srh As String) As String
        If Not IsPostBack Then
            If CommonCode.is_admin_user() Then
                SqlDataSource_Auctions.SelectCommand = "Select distinct(A.auction_id) as auction_id,'' as srh,isnull(A.title,'') as title,(select count(Q.query_id) As query_num from tbl_auction_queries Q where Q.auction_id=A.auction_id and ISNULL(parent_query_id,0)=0 and isnull(admin_marked,0)=0 and not exists(select query_id from tbl_auction_queries where parent_query_id=Q.query_id)) as query_count " & _
                          "from tbl_auctions A WITH (NOLOCK) where A.is_active=1 and " & _
                         "A.auction_id in (select distinct auction_id from (select auction_id, ab.buyer_id from (select distinct auction_id,buyer_id from tbl_auction_queries) ab inner join tbl_reg_buyers b WITH (NOLOCK) on ab.buyer_id=b.buyer_id inner join tbl_reg_sale_rep_buyer_mapping sm on sm.buyer_id=b.buyer_id ) B)"
                
            Else
                SqlDataSource_Auctions.SelectCommand = "Select distinct(A.auction_id) as auction_id,'' as srh,isnull(A.title,'') as title,(select count(Q.query_id) As query_num from tbl_auction_queries Q where Q.auction_id=A.auction_id and Q.buyer_id in (select distinct buyer_id from tbl_reg_sale_rep_buyer_mapping where user_id=" & hid_user_id.Value & ") and ISNULL(parent_query_id,0)=0 and isnull(admin_marked,0)=0 and not exists(select query_id from tbl_auction_queries where parent_query_id=Q.query_id)) as query_count,A.display_end_time " & _
                          "from tbl_auctions A WITH (NOLOCK) where A.is_active=1 and " & _
                         "1=case when isnull((select count(C.buyer_id) from dbo.fn_get_invited_buyers(A.auction_id) B inner join tbl_reg_sale_rep_buyer_mapping C on B.buyer_id=C.buyer_id where C.user_id=" & hid_user_id.Value & "),0)>0 then 1 else 0 end order by A.display_end_time desc"
            End If

        Else
            If CommonCode.is_admin_user() Then
                SqlDataSource_Auctions.SelectCommand = "Select distinct(A.auction_id) as auction_id,'' as srh,isnull(A.title,'') as title,(select count(Q.query_id) As query_num from tbl_auction_queries Q where Q.auction_id=A.auction_id and ISNULL(parent_query_id,0)=0 and isnull(admin_marked,0)=0 and not exists(select query_id from tbl_auction_queries where parent_query_id=Q.query_id)) as query_count " & _
                          "from tbl_auctions A WITH (NOLOCK) where A.is_active=1 and " & _
                         "A.auction_id in (select distinct auction_id from (select auction_id, ab.buyer_id from (select distinct auction_id,buyer_id from tbl_auction_queries) ab inner join tbl_reg_buyers b WITH (NOLOCK) on ab.buyer_id=b.buyer_id inner join tbl_reg_sale_rep_buyer_mapping sm on sm.buyer_id=b.buyer_id where (a.title like '%" & srh & "%' or b.contact_first_name like '%" & srh & "%' or b.contact_last_name like '%" & srh & "%')) B) "
            Else
                'SqlDataSource_Auctions.SelectCommand = "Select distinct(A.auction_id) as auction_id,'' as srh,isnull(A.title,'') as title,(select count(Q.query_id) As query_num from tbl_auction_queries Q where Q.auction_id=A.auction_id and Q.buyer_id in (select distinct buyer_id from tbl_reg_sale_rep_buyer_mapping where user_id=" & hid_user_id.Value & ") and ISNULL(parent_query_id,0)=0 and isnull(admin_marked,0)=0 and not exists(select query_id from tbl_auction_queries where parent_query_id=Q.query_id)) as query_count " & _
                '          "from tbl_auctions A where A.is_active=1 and " & _
                '         "A.auction_id in (select distinct auction_id from (select auction_id, ab.buyer_id from (select distinct auction_id,buyer_id from tbl_auction_queries) ab inner join tbl_reg_buyers b on ab.buyer_id=b.buyer_id inner join tbl_reg_sale_rep_buyer_mapping sm on sm.buyer_id=b.buyer_id   where (a.title like '%" & srh & "%' or b.contact_first_name like '%" & srh & "%' or b.contact_last_name like '%" & srh & "%') and sm.user_id=" & hid_user_id.Value & ") B) "
                SqlDataSource_Auctions.SelectCommand = "Select distinct(A.auction_id) as auction_id,'' as srh,isnull(A.title,'') as title,(select count(Q.query_id) As query_num from tbl_auction_queries Q where Q.auction_id=A.auction_id and Q.buyer_id in (select distinct buyer_id from tbl_reg_sale_rep_buyer_mapping where user_id=" & hid_user_id.Value & ") and ISNULL(parent_query_id,0)=0 and isnull(admin_marked,0)=0 and not exists(select query_id from tbl_auction_queries where parent_query_id=Q.query_id)) as query_count,A.display_end_time " & _
                         "from tbl_auctions A WITH (NOLOCK) where A.is_active=1 and " & _
                        "1=case when isnull((select count(C.buyer_id) from dbo.fn_get_invited_buyers(A.auction_id) B inner join tbl_reg_sale_rep_buyer_mapping C on B.buyer_id=C.buyer_id inner join tbl_reg_buyers D WITH (NOLOCK) on C.buyer_id=D.buyer_id where (A.title like '%" & srh & "%' or D.contact_first_name like '%" & srh & "%' or D.contact_last_name like '%" & srh & "%') and C.user_id=" & hid_user_id.Value & "),0)>0 then 1 else 0 end order by A.display_end_time desc"
            End If
        End If
        ' Response.Write(SqlDataSource_Auctions.SelectCommand)
        'RadGrid1.DataSourceID = "SqlDataSource_Auctions"
        'RadGrid1.Rebind()
        Return SqlDataSource_Auctions.SelectCommand
    End Function
    Protected Sub set_grid()

        RadGrid1.CurrentPageIndex = 0
        If RadGrid1.Items.Count > 0 Then
            RadGrid1.Visible = True
            pnl_noItem.Visible = False
        Else
            RadGrid1.Visible = False
            pnl_noItem.Visible = True
        End If
    End Sub
    Protected Sub txt_search_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_search.TextChanged

        If txt_search.Text.Trim <> "" Then
            hid_search.Value = "%" & txt_search.Text.Trim() & "%"
            RadGrid1.Rebind()
            set_grid()
        Else
            hid_search.Value = "%%"
            RadGrid1.Rebind()
            set_grid()
        End If

    End Sub

    Protected Sub RadGrid1_SortCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridSortCommandEventArgs) Handles RadGrid1.SortCommand
        'RadGrid1.Rebind()
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        set_grid()
    End Sub
End Class
