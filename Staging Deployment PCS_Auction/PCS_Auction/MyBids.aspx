﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MyBids.aspx.vb" Inherits="MyBids"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControls/MyBids.ascx" TagName="AuctionListing" TagPrefix="UC1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Auctions</title>
    <link type="text/css" rel="Stylesheet" href="/Style/AuctionBody.css" />
     <link type="text/css" rel="Stylesheet" href="/Style/master_menu.css" />
    <link rel="stylesheet" type="text/css" href="/Style/menu.css" />
    <link type="text/css" rel="Stylesheet" href="/Style/pcs.css" />
</head>
<body style="background-image:none;">
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="ScriptManager" runat="server" />
    <script type="text/javascript" language="JavaScript">
        function open_pop_win(_path, _id) {
            w = window.open(_path + '?i=' + _id, '_history', 'top=' + ((screen.height - 400) / 2) + ', left=' + ((screen.width - 324) / 2) + ', height=400, width=324,scrollbars=yes,toolbars=no,resizable=1;');
            w.focus();
            return false;
        }
        function open_pop_order_confirmation(_path, _id,au_mode) {
            w = window.open(_path + '?b=' + _id + '&t=' + au_mode, '_ord_Confirm', 'top=' + ((screen.height - 700) / 2) + ', left=' + ((screen.width - 890) / 2) + ', height=700, width=890,scrollbars=yes,toolbars=no,resizable=1;');
            w.focus();
            return false;
        }
        function set_fl_id(_id, txt_id) {
            document.getElementById('hid_fl_upload_button').value = document.getElementById(_id).name;
            var temp = _id;
            var temp = temp.substr(0, temp.lastIndexOf('AuctionListing_')) + 'AuctionListing_' + txt_id;
            if (document.getElementById(temp).value != "") {
                return true;
            }
            else { return false; }
        }

        function Focus(objname, waterMarkText) {
            obj = document.getElementById(objname);
            if (obj.value == waterMarkText) {
                obj.value = "";
                obj.className = "WaterMarkText1";
                if (obj.value == "Response" || obj.value == "" || obj.value == null) {
                    obj.style.color = "black";
                }
            }
        }
        function Blur(objname, waterMarkText) {

            obj = document.getElementById(objname);
            if (obj.value == "") {
                obj.value = waterMarkText;
                //                if (objname != "txtPwd") {
                //                    obj.className = "WaterMarkedTextBox";
                //                }
                //                else {
                obj.className = "WaterMarkedTextBox1";
                //                }
            }
            else {
                obj.className = "WaterMarkText1";
            }

            if (obj.value == "Response" || obj.value == "" || obj.value == null) {
                obj.style.color = "gray";
            }
        }
    

    </script>
    <telerik:RadCodeBlock ID="RadCodeBlock_Main" runat="server">
        <script type="text/javascript" language="JavaScript">
            function ButtonPostback(sender, args) {

                if (args.get_eventTarget() == document.getElementById('hid_fl_upload_button').value) {
                    args.set_enableAjax(false);
                }
                else { args.set_enableAjax(true); }
            }
        </script>
    </telerik:RadCodeBlock>
    <input type="hidden" id="hid_fl_upload_button" name="hid_fl_upload_button" />
   
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div style="float: left; margin: 10px;">
                    <div class="pageheading">
                        <asp:Label runat="server" ID="lbl_page_heading"></asp:Label>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <br />
    <div class="pageHeading" style="padding: 0px 0px 0px 5px;">
        <asp:Label ID="lbl_test" runat="server"></asp:Label>
    </div>
    <div style="padding: 0px 0px 0px 5px;">
        <br />
       
        <asp:Repeater ID="rpt_auctions" runat="server">
            <ItemTemplate>
                <UC1:AuctionListing runat="server" ID="UC_AuctionListing" auction_id='<%# Eval("auction_id")%>'
                    buy_id='<%# Eval("buy_id") %>' quotation_id='<%# Eval("quotation_id") %>' />
            </ItemTemplate>
            <SeparatorTemplate>
                <br />
                <br />
                <br />
                <br />
            </SeparatorTemplate>
        </asp:Repeater>
        <asp:Panel ID="pnl_noItem" runat="server">
            <div style="border: 1px solid #10AAEA; background: url('/Images/fend/grdnt_template.jpg') repeat-x;
        height: 112px; padding-top: 25px; text-align: center;">
                <b>Auction Not available in this section</b>
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
