﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="auction_sale_report.aspx.vb" Inherits="Reports_auction_sale_report" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">

            function Cancel_Ajax(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                     args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
                else {
                    args.set_enableAjax(true);
                }
            }

        </script>
    </telerik:RadScriptBlock>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading" style="padding-bottom: 15px;">
                    Auction Sale Report</div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Vista">
                    <ClientEvents OnRequestStart="Cancel_Ajax" />
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="RadGrid_Auction_List">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid_Auction_List" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid_Auction_List" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
                <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" />
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
                    left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
                    <img id="Image8" src="/images/img_loading.gif" />
                </telerik:RadAjaxLoadingPanel>
                <telerik:RadGrid runat="server" ID="RadGrid_Auction_List" AllowFilteringByColumn="True"
                    AllowSorting="True" PageSize="10" ShowFooter="False" ShowGroupPanel="True" AllowPaging="True"
                    AutoGenerateColumns="false" Skin="Vista" EnableLinqExpressions="false">
                    <PagerStyle Mode="NextPrevAndNumeric" />
                    <ExportSettings IgnorePaging="true" FileName="Auction_Sale_Report" OpenInNewWindow="true"
                        ExportOnlyData="true" />
                    <ClientSettings AllowDragToGroup="true">
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="true" />
                    <MasterTableView DataKeyNames="auction_id" AutoGenerateColumns="false" ItemStyle-Height="40"
                        AlternatingItemStyle-Height="40" CommandItemDisplay="Top">
                        <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true"
                            ShowExportToPdfButton="false" ShowExportToCsvButton="true" ShowAddNewRecordButton="false"
                            ShowRefreshButton="false" />
                        <NoRecordsTemplate>
                            No Auction to display
                        </NoRecordsTemplate>
                        <SortExpressions>
                            <telerik:GridSortExpression FieldName="title" SortOrder="Ascending" />
                        </SortExpressions>
                        <Columns>
                            <telerik:GridTemplateColumn DataField="title" HeaderText="Auction Detail" SortExpression="title"
                                UniqueName="title" FilterControlWidth="335" ItemStyle-Width="335" GroupByExpression="title [Auction Detail] group by title">
                                <ItemTemplate>
                                    <div style="width: 100%; height: 45px; display: block; clear: both;">
                                        <div style="float: left; border: 1px solid #D7D7D7; width: 60px; height: 45px;">
                                            <img width="60px" height="45px" src='<%#Eval("image_path") %>' />
                                        </div>
                                        <div style="float: left; font-size: 11px; padding-left: 8px; border: 0px solid Red;
                                            width: 240px; text-align: left;">
                                            <b>
                                                <%#Eval("title") %></b><br />
                                            <span style="color: Gray; font-size: 10px;">
                                                <%#Eval("code") %></span><br />
                                            <%#Eval("sub_title")%>
                                        </div>
                                    </div>
                                    <%-- <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');" onclick="redirectIframe('/Backend_TopBar.aspx?t=<%# CommonCode.getAuctionTabPosition(Eval("auction_id")) %>&a=<%# Eval("auction_id") %>','/Backend_Leftbar.aspx?t=3&a=<%# Eval("auction_id") %>','/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %>')">
                                        <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("title") %>'></asp:Label></a>--%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="start_date" HeaderText="Start Date" FilterControlWidth="60"
                                ItemStyle-Width="60" SortExpression="start_date" UniqueName="start_date" GroupByExpression="start_date [Start Date] group by start_date">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="end_date" HeaderText="End Date" FilterControlWidth="60"
                                ItemStyle-Width="60" SortExpression="end_date" UniqueName="end_date" GroupByExpression="end_date [End Date] group by end_date">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="bidder" HeaderText="Bidder" SortExpression="bidder"
                                ItemStyle-Width="95" UniqueName="bidder" GroupByExpression="bidder [Bidder] group by bidder">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="country" HeaderText="Country" FilterControlWidth="55"
                                ItemStyle-Width="55" SortExpression="country" UniqueName="country" GroupByExpression="country [Country] group by country">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="quantity" HeaderText="Quantity" AllowFiltering="false"
                                FilterControlWidth="30" SortExpression="quantity" UniqueName="quantity" GroupByExpression="quantity [Quantity] group by quantity">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn DataField="price" ItemStyle-HorizontalAlign="Right" HeaderText="Price"
                                SortExpression="price" UniqueName="price" GroupByExpression="price [Price] group by price">
                                <ItemTemplate>
                                    <%#FormatCurrency(Eval("price"), 2)%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="action" HeaderText="Backend Updated?" FilterControlWidth="45"
                                ItemStyle-Width="105" UniqueName="action" Groupable="false" AllowSorting="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="sale_rep" HeaderText="Sales Agent" FilterControlWidth="65"
                                SortExpression="sale_rep" ItemStyle-Width="95" UniqueName="sale_rep" GroupByExpression="sale_rep [Sales Agent] group by sale_rep">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</asp:Content>
