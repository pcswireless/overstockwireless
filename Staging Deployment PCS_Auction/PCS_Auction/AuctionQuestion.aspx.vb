﻿
Partial Class AuctionQuestion
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lit_msg_confirmation.Text = ""
        If Not Page.IsPostBack Then

            If Not String.IsNullOrEmpty(Request.QueryString.Get("i")) Then
                hid_auction_id.Value = Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("i"))
                'bindGrid(hid_auction_id.Value)
                load_auction_bidder_queries(hid_auction_id.Value)
            Else
                hid_auction_id.Value = 0
            End If

            'Dim str As String = "SELECT A.auction_id,"
            'str = str & "ISNULL(A.code, '') AS code,"
            'str = str & "ISNULL(A.title, '') AS title,"
            'str = str & "ISNULL(A.sub_title, '') AS sub_title"
            'str = str & " FROM "
            'str = str & "tbl_auctions A"
            'str = str & " WHERE "
            'str = str & "A.auction_id =" & Request.QueryString.Get("i")

            'Dim dtTable As New DataTable()
            ''dtTable = SqlHelper.ExecuteDatatable(str)

            'If dtTable.Rows.Count > 0 Then
            '    With dtTable.Rows(0)
            '        'lbl_auction_code.Text = .Item("code")
            '        'lbl_title.Text = "<span class='blue'>" & .Item("title") & "</span> "

            '        '<span class='auctionPartNo'>Quantity " & .Item("quantity") & "</span>"
            '        'lbl_sub_title.Text = "<h1 style='padding:0px; font-size:14px;'>" & .Item("sub_title") & "</h1>"
            '    End With
            'End If

        End If

    End Sub

    

    'Protected Sub img_btn_create_thread_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_btn_create_thread.Click
    '    Dim postStr As String = txt_thread_title.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>").Replace("'", "`")
    '    Dim str As String = "INSERT into tbl_auction_queries(sales_rep_id,auction_id,title,buyer_id,buyer_user_id,parent_query_id,submit_date) " & _
    '            " Values ((select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")," & Request.QueryString.Get("i") & ",'" & postStr & "'," & CommonCode.Fetch_Cookie_Shared("buyer_id") & "," & CommonCode.Fetch_Cookie_Shared("user_id") & ",0,getdate()); select SCOPE_IDENTITY();"
    '    Dim query_id As Integer = 0
    '    query_id = SqlHelper.ExecuteScalar(str)

    '    Dim sales_rep_id As Integer = 0
    '    Dim dt As New DataTable
    '    'dt = SqlHelper.ExecuteDatatable("SELECT A.user_id FROM tbl_sec_users A where A.user_id in (select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
    '    dt = SqlHelper.ExecuteDatatable("select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id"))
    '    If dt.Rows.Count > 0 Then
    '        sales_rep_id = dt.Rows(0)("user_id")
    '        Dim mailid As String
    '        Dim Comm As New Email()
    '        mailid = Comm.set_rep_contact_request_to_salesrep(sales_rep_id, txt_thread_title.Text.Trim(), "", query_id:=query_id)
    '        mailid = Comm.set_rep_contact_request_to_user(CommonCode.Fetch_Cookie_Shared("user_id"), txt_thread_title.Text.Trim(), "")
    '        Comm = Nothing
    '    End If
    '    txt_thread_title.Text = ""
    '    lit_msg_confirmation.Text = "Thanks for your question. Someone will get back to you within 24 hours."

    'End Sub

    Protected Sub load_auction_bidder_queries(ByVal auction_id As Integer)
        Dim dt As New DataTable
        dt = SqlHelper.ExecuteDatatable("select A.query_id,A.title,A.submit_date,A.buyer_id,B.company_name,isnull(case when B.state_id<>0 then (select name from tbl_master_states where state_id=B.state_id) else B.state_text end,'') as state from tbl_auction_queries A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id inner join tbl_auctions A1 WITH (NOLOCK) on A.auction_id=A1.auction_id where isnull(A1.discontinue,0)=0 and A.parent_query_id=0 and A.is_active=1 and A.auction_id=" & auction_id & " and A.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by submit_date desc")

        'Response.Write("select A.query_id,A.title,A.submit_date,A.buyer_id,B.company_name,isnull(case when B.state_id<>0 then (select name from tbl_master_states where state_id=B.state_id) else B.state_text end,'') as state from tbl_auction_queries A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id inner join tbl_auctions A1 on A.auction_id=A1.auction_id where A.parent_query_id=0 and A.is_active=1 and A.auction_id=" & Request.QueryString.Get("i") & " and A.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by submit_date desc")

        rep_after_queries.DataSource = dt
        rep_after_queries.DataBind()
        If dt.Rows.Count = 0 Then
            lit_your_query.Visible = False
        End If
        dt.Dispose()
    End Sub

    Protected Sub img_btn_create_thread_Click(sender As Object, e As System.EventArgs) Handles img_btn_create_thread.Click
        Dim postStr As String = txt_thread_title.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>").Replace("'", "`")
        Dim str As String = "INSERT into tbl_auction_queries(sales_rep_id,auction_id,title,buyer_id,buyer_user_id,parent_query_id,submit_date) " & _
                " Values ((select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")," & hid_auction_id.Value & ",'" & postStr & "'," & CommonCode.Fetch_Cookie_Shared("buyer_id") & "," & CommonCode.Fetch_Cookie_Shared("user_id") & ",0,getdate()); select SCOPE_IDENTITY();"
        Dim query_id As Integer = 0
        query_id = SqlHelper.ExecuteScalar(str)

        Dim sales_rep_id As Integer = 0
        Dim dt As New DataTable
        'dt = SqlHelper.ExecuteDatatable("SELECT A.user_id FROM tbl_sec_users A where A.user_id in (select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
        dt = SqlHelper.ExecuteDatatable("select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id"))
        If dt.Rows.Count > 0 Then
            sales_rep_id = dt.Rows(0)("user_id")
            Dim mailid As String
            Dim Comm As New Email()
            If sales_rep_id > 0 Then
                mailid = Comm.set_rep_contact_request_to_salesrep(sales_rep_id, "", txt_thread_title.Text.Trim(), query_id:=query_id)
            End If
            mailid = Comm.set_rep_contact_request_to_user(CommonCode.Fetch_Cookie_Shared("user_id"), "", txt_thread_title.Text.Trim())
            Comm = Nothing
            Response.Write("<script>window.opener.refresh_query();</script>")
        End If
        txt_thread_title.Text = ""
        lit_msg_confirmation.Text = "Thanks for your question. Someone will get back to you within 24 hours."
        load_auction_bidder_queries(hid_auction_id.Value)

    End Sub
    
    Protected Sub rep_after_queries_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        If e.CommandName = "deleteQry" Then
            SqlHelper.ExecuteNonQuery("update tbl_auction_queries set is_active=0 where query_id=" & e.CommandArgument)
            load_auction_bidder_queries(hid_auction_id.Value)

            'e.Item.Visible = False
        End If
    End Sub

    Protected Sub rep_after_queries_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            CType(e.Item.FindControl("btnDelete"), ImageButton).Attributes.Add("onclick", "return confirm('Are you sure to delete this question?');")
            Dim rep_inner As Repeater = DirectCast(e.Item.FindControl("rep_inner_after_queries"), Repeater)
            rep_inner.DataSource = SqlHelper.ExecuteDatatable("select A.message, A.submit_date, ISNULL(C.first_name,'') AS first_name,ISNULL(C.last_name,'') AS last_name,ISNULL(D.first_name,'') AS buyer_first_name,ISNULL(D.last_name,'') AS buyer_last_name,isnull(C.title,'') as title,isnull(D.title,'') as buyer_title,isnull(C.image_path,'') as image_path,C.user_id from tbl_auction_queries A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id left join tbl_sec_users C on A.user_id=C.user_id LEFT JOIN tbl_reg_buyer_users D ON A.buyer_user_id=D.buyer_user_id where A.parent_query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & " order by submit_date DESC")
            rep_inner.DataBind()
        End If
    End Sub

    'Protected Sub btn_query_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_query.Click
    '    load_auction_bidder_queries(hid_auction_id.Value)
    'End Sub
End Class
