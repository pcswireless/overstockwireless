﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SystemLogList.aspx.vb" Inherits="Log_SystemLogList"
    EnableViewState="false" MasterPageFile="~/BackendPopUP.master" Title="PCS Bidding" %>

<%@ Register Src="UserControls/SystemLogListing.ascx" TagName="SystemLogListing"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SystemLogListing ID="UC_System_Log_List" runat="server" />
</asp:Content>
