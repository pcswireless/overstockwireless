﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SystemLogLink.ascx.vb"
    Inherits="Log_UserControls_SystemLogLink" %>
<script language="javascript">
    function OpenWindow_SystemLogList() {
        window.open("/log/SystemLogList.aspx?i=<%=intUniqueID%>&module=<%=strModuleName %>", "SystemLogList", "left=" + ((screen.width - 990) / 2) + ",top=" + ((screen.height - 600) / 2) + ",width=990,height=500,scrollbars=yes,toolbars=no,resizable=yes");
        return false;
    }
</script>

<a href="#" id="LNK_SystemLog" runat="server" onclick="return OpenWindow_SystemLogList();">
    <img src="/Images/viewlog.gif" alt="View System Log" border="none" /> </a>