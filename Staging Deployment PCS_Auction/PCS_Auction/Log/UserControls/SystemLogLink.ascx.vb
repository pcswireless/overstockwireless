﻿
Partial Class Log_UserControls_SystemLogLink
    Inherits System.Web.UI.UserControl
    Public intUniqueID As Integer
    Public strModuleName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        setPermission()
    End Sub
   
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

    End Sub
    Private Sub setPermission()
        If Not CommonCode.is_super_admin() Then
            LNK_SystemLog.Visible = False
        End If
    End Sub
    Public WriteOnly Property Unique_ID() As Integer
        Set(ByVal value As Integer)
            intUniqueID = value
        End Set
    End Property
    Public WriteOnly Property Module_Name() As String
        Set(value As String)
            strModuleName = value
        End Set
    End Property
End Class
