﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="bidder_wise_auction.aspx.vb" Inherits="Users_bidder_wise_auction" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <div class="pageheading">
        Dashboard</div>
    <div class="statusCompany">
        <asp:Literal ID="ltrl_buyer" runat="server"></asp:Literal>
    </div>
    <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <table cellspacing="0" cellpadding="0" border="0" width="988">
            <tr>
                <td>
                    <asp:HiddenField ID="hid_user_id" runat="server" Value="0" />
                    <table cellpadding="0" cellspacing="0" style="padding-top: 20px">
                        <tr>
                            <td colspan="2">
                                <div style="width: 250px; text-align: left; color: #243E5A; font-weight: bold; font-size: 13px;
                                    height: 30px; float: left;">
                                    <asp:Literal ID="lit_query_pending" runat="server"></asp:Literal></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HiddenField ID="hid_auction_id" runat="server" Value='<%# Eval("auction_id") %>' />
                                <asp:DataList ID="rpt_auction" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                                    CellSpacing="0" ItemStyle-VerticalAlign="Top" DataSourceID="SqlDataSource_Auction">
                                    <ItemTemplate>
                                        <div style='width: 328px; padding: 0px 10px; text-align: left; <%# iif(((Container.ItemIndex + 1) mod 3) = 0,"border-bottom: 1px dotted LightGray;","border-bottom: 1px dotted LightGray;border-right: 1px dotted LightGray;")%>'>
                                            <div style="overflow: hidden;">
                                                <div style="float: left; line-height: 17px; font-size: 13px; padding-left: 8px; padding-top: 3px;
                                                    border: 0px solid Red; width: 185px; overflow: hidden;">
                                                    <div style="color: #303030;">
                                                        <a style="color: #243E5A; text-decoration: none;" href="javascript:void(0);" onmouseout="hide_tip_new();"
                                                            onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');">
                                                            <asp:Literal ID="ltrl_auction_title" Text='<%# Eval("title") %>' runat="server"></asp:Literal></a>
                                                    </div>
                                                    <div style="color: #303030;">
                                                        <iframe id="iframe_time" src="/timerframe.aspx?i=<%# Eval("auction_id") %>" scrolling="no"
                                                            frameborder="0" width="165px" height="23px"></iframe>
                                                    </div>
                                                    <div style="color: #303030;">
                                                        <%# "$" & FormatNumber(Eval("amount"), 2) & " (Rank " & Eval("rank") & ")" %>
                                                    </div>
                                                    <div style="color: #303030;">
                                                        <asp:Literal ID="ltrl_reply_pending" Text='<%# Eval("query_count") & " Question Pending" %>'
                                                            runat="server"></asp:Literal>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </td>
                            <td>
                                <div style="color: #105386;">
                                    <div>
                                        <asp:Literal ID="ltrl_current_bid_status" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <asp:SqlDataSource ID="SqlDataSource_Auction" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                        ProviderName="System.Data.SqlClient" runat="server" SelectCommand="Select distinct(A.auction_id) as auction_id,isnull(A.title,'') as title,(select count(Q.query_id) As query_num from tbl_auction_queries Q where Q.auction_id=A.auction_id and Q.buyer_id in (select B1.buyer_id from tbl_reg_buyers B1 WITH (NOLOCK) inner join tbl_reg_sale_rep_buyer_mapping S1 on B1.buyer_id=S1.buyer_id and S1.user_id=@user_id) and ISNULL(parent_query_id,0)=0 and not exists(select query_id from tbl_auction_queries where parent_query_id=Q.query_id)) as query_count from tbl_auctions A WITH (NOLOCK) where isnull(A.discontinue,0)=0 and  A.display_end_time >=getdate() and (1=(case when (A.show_relevant_bidders_only)=1 then (case when (select count(*) from dbo.fn_get_invited_buyers(A.auction_id) where buyer_id in (select S.buyer_id from tbl_reg_sale_rep_buyer_mapping S where S.user_id=@user_id))>0 then 1 else 0 end) else 1 end)) and A.is_active=1 and A.auction_type_id in (2,3)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="hid_user_id" PropertyName="Value" Name="user_id" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnl_noItem" runat="server">
                        <div style="border: 1px solid #10AAEA; background: url('/Images/fend/grdnt_template.jpg') repeat-x;
        height: 112px; padding-top: 25px; text-align: center;">
                            <b>Bidder Not exist in this section</b>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
</asp:Content>
