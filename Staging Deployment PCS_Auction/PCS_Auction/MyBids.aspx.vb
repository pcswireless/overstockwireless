﻿
Partial Class MyBids
    Inherits BasePage
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            If CommonCode.Fetch_Cookie_Shared("is_backend") = "1" Then
                Response.Write("<script language='javascript'>top.location.href = '/Backend_Home.aspx?t=1';</script>")
            End If
        End If
        If Not Page.IsPostBack Then
            Dim str As String = ""
            'Dim strQuery As String = "select auction_id,dbo.get_auction_status(auction_id) as status_id,case dbo.get_auction_status(auction_id) when 1 then display_end_time else start_date end as timeleft from tbl_auctions where tbl_auctions.is_active=1 " & str
            Dim strQuery As String = ""
            If Not String.IsNullOrEmpty("mode") Then
                If Request.QueryString("mode") = "q" Then
                    strQuery = "select A.auction_id,A.title,dbo.get_auction_status(A.auction_id) as status_id,case dbo.get_auction_status(A.auction_id) when 1 then display_end_time else start_date end as timeleft,B.filename,B.action,B.quotation_id,ISNULL(details,'') AS details,0 as buy_id from tbl_auctions A WITH (NOLOCK) right join tbl_auction_quotations B on A.auction_id=B.auction_id  where B.buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ""
                    lbl_page_heading.Text = GetLocalResourceObject("lbl_page_heading_my_quotations.Text").ToString()
                ElseIf Request.QueryString("mode") = "pr" Then
                    strQuery = "select A.auction_id,A.title,dbo.get_auction_status(A.auction_id) as status_id,case dbo.get_auction_status(A.auction_id) when 1 then display_end_time else start_date end as timeleft,B.price,B.status,ISNULL(B.buy_id,0) As buy_id,0 as quotation_id from tbl_auctions A WITH (NOLOCK) inner join  tbl_auction_buy B WITH (NOLOCK) on A.auction_id = B.auction_id where B.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and B.buy_type='private'"
                    lbl_page_heading.Text = GetLocalResourceObject("lbl_page_heading_private_offers.Text").ToString()
                ElseIf Request.QueryString("mode") = "pa" Then
                    strQuery = "select A.auction_id,A.title,dbo.get_auction_status(A.auction_id) as status_id,case dbo.get_auction_status(A.auction_id) when 1 then display_end_time else start_date end as timeleft,B.price,B.quantity,B.status,ISNULL(B.buy_id,0) As buy_id,0 as quotation_id from tbl_auctions A WITH (NOLOCK) inner join  tbl_auction_buy B  WITH (NOLOCK) on A.auction_id = B.auction_id where B.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and B.buy_type='partial'"
                    lbl_page_heading.Text = GetLocalResourceObject("lbl_page_heading_partial_offers.Text").ToString()
                ElseIf Request.QueryString("mode") = "bu" Then
                    strQuery = "select A.auction_id,A.title,dbo.get_auction_status(A.auction_id) as status_id,case dbo.get_auction_status(A.auction_id) when 1 then display_end_time else start_date end as timeleft,B.price,B.status,ISNULL(B.buy_id,0) As buy_id,0 as quotation_id from tbl_auctions A WITH (NOLOCK) inner join  tbl_auction_buy B WITH (NOLOCK) on A.auction_id = B.auction_id where B.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and B.buy_type='buy now'"
                    lbl_page_heading.Text = GetLocalResourceObject("lbl_page_heading_buy_now.Text").ToString()
                Else
                    strQuery = "select A.auction_id,A.title,A.display_end_time,dbo.get_auction_status(A.auction_id) as status_id,case dbo.get_auction_status(A.auction_id) when 1 then display_end_time else start_date end as timeleft,B.bid_amount,B.bid_type,B.bid_id as buy_id,0 as quotation_id from tbl_auctions A WITH (NOLOCK) inner join tbl_auction_bids B WITH (NOLOCK) on A.rank1_bid_id=B.bid_id where A.display_end_time <=getdate() and B.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & ""
                    lbl_page_heading.Text = GetLocalResourceObject("lbl_page_heading_won_bids.Text").ToString()
                End If
            End If

            Select Case Request.QueryString.Get("t")
                Case "1"
                    strQuery = strQuery & "and dbo.get_auction_status(A.auction_id)=1"
                Case "2"
                    strQuery = strQuery & " and dbo.get_auction_status(A.auction_id)=1 "
                Case "3"
                    strQuery = strQuery & " and  dbo.get_auction_status(A.auction_id)=2"
                Case "4"
                    strQuery = strQuery & " and  dbo.get_auction_status(A.auction_id)=3 "
                Case "5"
                    strQuery = strQuery & " and display_end_time >= getdate() "
                    If IsNumeric(CommonCode.Fetch_Cookie_Shared("user_id")) Then
                        strQuery = strQuery & " and A.auction_id in (select auction_id from tbl_auction_bids WITH (NOLOCK) where buyer_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ") "
                    End If
                Case Else


            End Select
            strQuery = strQuery & "  order by status_id,timeleft "
            'lbl_test.Text = strQuery
            rpt_auctions.DataSource = SqlHelper.ExecuteDataTable(strQuery)
            rpt_auctions.DataBind()

            If rpt_auctions.Items.Count > 0 Then
                rpt_auctions.Visible = True
                pnl_noItem.Visible = False
            Else
                rpt_auctions.Visible = False
                pnl_noItem.Visible = True
            End If
        End If
    End Sub
End Class
