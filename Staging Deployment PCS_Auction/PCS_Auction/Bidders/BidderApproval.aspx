﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="true"
    CodeFile="BidderApproval.aspx.vb" Inherits="Bidders_BidderApproval" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                     args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
                else {
                    args.set_enableAjax(true);
                }
            }
            function open_bidder_attachment(bidder_id) {
                window.open("/Bidders/Bidder_Attachment.aspx?i=" + bidder_id, "_bidder_attach", "left=" + ((screen.width - 600) / 2) + ",top=" + ((screen.height - 700) / 2) + ",width=600,height=500,scrollbars=yes,toolbars=no,resizable=yes");
                window.focus();
                return false;
            }

            function rebind_grid()
            {
                document.getElementById('<%=btnRefresh.ClientID%>').click();

            }
        </script>

    </telerik:RadScriptBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="Cancel_Ajax" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
           
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Pending Approvals
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    Please approve the bidders from following list
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxPanel ID="RadAjaxPanel1" EnableAJAX="false" runat="server">
                    <div style="padding-bottom: 10px;">
                        <asp:Label ID="lblMessage" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                    </div>
                    <asp:button ID="btnRefresh" runat="server" style="display:none;" OnClick="btnRefresh_Click" />
                    <%-- <asp:TextBox ID="txt_test" runat="server"></asp:TextBox>--%>
                    <telerik:RadGrid AutoGenerateColumns="false" ID="RadGrid2" AllowFilteringByColumn="True"
                        AllowSorting="True" PageSize="10" AllowPaging="true" runat="server" Skin="Vista"
                        Width="100%" ShowGroupPanel="true" EnableLinqExpressions="false">
                        <PagerStyle Mode="NextPrevAndNumeric"></PagerStyle>
                        <ExportSettings IgnorePaging="true" FileName="BidderApproval_Export" OpenInNewWindow="true"
                            ExportOnlyData="true" />
                        <ClientSettings AllowDragToGroup="true">
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                        <GroupingSettings ShowUnGroupButton="true" />
                        <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="Top" AllowFilteringByColumn="True"
                            ShowFooter="false" ItemStyle-Height="40" AlternatingItemStyle-Height="40">
                            <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false"
                                ShowExportToPdfButton="false" ShowExportToCsvButton="false" ShowAddNewRecordButton="false"
                                ShowRefreshButton="false" />
                            <NoRecordsTemplate>
                                No bidder available with the selected status.
                            </NoRecordsTemplate>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                            </SortExpressions>
                            <Columns>
                                <telerik:GridBoundColumn DataField="buyer_id" HeaderText="buyer_id" SortExpression="buyer_id"
                                    UniqueName="buyer_id" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="Bidder Name" DataField="name" UniqueName="name"
                                    HeaderStyle-Width="210" ItemStyle-Width="210" SortExpression="name" FilterControlWidth="120"
                                    GroupByExpression="name [Bidder Name] Group By name">
                                    <ItemTemplate>
                                        <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','200','white','true');"
                                            onclick="redirectIframe('','/Backend_Leftbar.aspx?t=2&b=<%# Eval("buyer_id")%>','/Bidders/AddEditBuyer.aspx?i=<%# Eval("buyer_id") %>')">
                                            <%# Eval("name")%>
                                        </a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="company_name" HeaderText="Company" SortExpression="company_name"
                                    FilterControlWidth="140" UniqueName="company_name" HeaderStyle-Width="170" ItemStyle-Width="160">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="phone1" HeaderText="Phone" SortExpression="phone1"
                                    UniqueName="phone1" FilterControlWidth="70" HeaderStyle-Width="90" ItemStyle-Width="80">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="Registration Date" DataField="submit_date"
                                    UniqueName="submit_date" HeaderStyle-Width="80" ItemStyle-Width="80" SortExpression="submit_date"
                                    AllowFiltering="false" Groupable="false">
                                    <ItemTemplate>
                                        <%# CDate(Eval("submit_date")).ToShortDateString()%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Bidder Comments" Groupable="false"
                                    ItemStyle-Width="150" HeaderStyle-Width="150" ItemStyle-HorizontalAlign="Left"
                                    HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        Enter Comment Here:
                                        <asp:TextBox ID="txt_comment" runat="server" TextMode="MultiLine" Height="50"></asp:TextBox>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn AllowFiltering="true" DataField="status" Groupable="false"
                                    HeaderStyle-Width="130" ItemStyle-Width="120" UniqueName="status" ItemStyle-Wrap="true">
                                    <FilterTemplate>
                                        <telerik:RadComboBox ID="RadComboBoxStatus" DataSourceID="SqlDataSource2" DataTextField="status"
                                            DataValueField="status" Height="200px" AppendDataBoundItems="true" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("status").CurrentFilterValue %>'
                                            runat="server" OnClientSelectedIndexChanged="StatusIndexChanged1">
                                            <Items>
                                                <telerik:RadComboBoxItem Text="All" />
                                            </Items>
                                        </telerik:RadComboBox>
                                        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                            <script type="text/javascript">
                                                function StatusIndexChanged1(sender, args) {
                                                    var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                    tableView.filter("status", args.get_item().get_value(), "EqualTo");

                                                }
                                            </script>
                                        </telerik:RadScriptBlock>
                                    </FilterTemplate>
                                    <HeaderTemplate>
                                        Action
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hid_grd_status_id" runat="server" Value='<%# Eval("status_id") %>' />
                                        <table cellpadding="1" cellspacing="0" style="background-color: #F2F3F5;">
                                            <tr>
                                                <td>
                                                    Max Amt. of Bid
                                                    <br />
                                                    <telerik:RadNumericTextBox ID="txt_max_amt" runat="server" 
                                                        Height="18"  CssClass="TextBox" Value='<%# Convert.ToDouble(Eval("max_amt_bid")) %>'
                                                        Width="100"  MaxValue="9999999999">
                                                        <NumberFormat DecimalDigits="2" DecimalSeparator="." AllowRounding="false"   />
                                                    </telerik:RadNumericTextBox>
                                                    <%--<asp:TextBox ID="txt_max_amt" runat="server"  Width="100" Text='<%# Eval("max_amt_bid") %>'></asp:TextBox>--%>
                                                    

                                                </td>
                                                <td >
                                                     <asp:DropDownList ID="ddl_action" CssClass="TextBox" runat="server" DataSourceID="SqlDataSource_Combo"
                                                        DataTextField="status" DataValueField="status_id"
                                                        SelectedValue='<%#Eval("status_id") %>'  Width="130"
                                                       >
                                                    </asp:DropDownList>
<asp:HiddenField ID="hd_buyer_id" runat="server" Value='<%#Eval("buyer_id") %>'></asp:HiddenField>
                                                <asp:HiddenField ID="hd_attachment_count" runat="server" Value='<%#Eval("attachments") %>'></asp:HiddenField>
                                                </td>
                                                
                                            </tr>
                                            
                                            <tr>
                                                <td colspan="2">
                                                     <asp:button ID="btnUpdate" runat="server" Text="Save" CommandName="SA_update" />&nbsp;
                                                    <asp:button ID="btnAttach" runat="server" Text="Attachment" />
<asp:Label ID="lbl_error" runat="server" ForeColor="Red"></asp:Label>
                                                </td>
                                               
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false"
                                    ItemStyle-Width="50" HeaderStyle-Width="50" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <a href="javascript:void(0);" onclick="redirectIframe('','/Backend_Leftbar.aspx?t=2&b=<%# Eval("buyer_id")%>','/Bidders/AddEditBuyer.aspx?i=<%# Eval("buyer_id") %>')">
                                            <img src="/images/edit.png" style="border: none;" alt="Edit" /></a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <%-- <div style="text-align: right; padding: 10px 0px 10px 0px;">
                        <asp:Panel ID="pnlButton" runat="server">
                            <asp:ImageButton ID="but_basic_update" runat="server" AlternateText="Update" ImageUrl="/images/update.gif" />
                        </asp:Panel>
                    </div>--%>
                </telerik:RadAjaxPanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SqlDataSource_Combo" ConnectionString='<%$ ConnectionStrings:TConnectionString %>'
                    ProviderName="System.Data.SqlClient" SelectCommand='select status_id,status from tbl_reg_buyser_statuses order by sno'
                    runat="server"></asp:SqlDataSource>
                 <asp:SqlDataSource ID="SqlDataSource2" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="select status_id,status from [tbl_reg_buyser_statuses] where status_id in (1,3,4)"
                    runat="server"></asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
