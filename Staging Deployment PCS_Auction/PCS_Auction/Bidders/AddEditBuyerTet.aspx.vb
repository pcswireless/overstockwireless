﻿Imports Telerik.Web.UI
Imports System.Drawing
Partial Class Bidders_AddEditBuyerTet
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            fill_countries()
            fill_states()
            fill_how_find_us()
            'fill_business_types()
            'fill_industry_types()
            fill_companies()
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                set_form_mode(False)
                set_button_visibility("View")
                BindBuyerBasicInfo()

                'official info
                sqlDataSource_comment.SelectParameters("buyer_id").DefaultValue = Request.QueryString("i")
                'Bind_buyer_attachments()
                bind_official_detail()
                set_form_mode_official(False)
                set_button_visibility_official("View")

            Else
                set_form_mode(True)
                set_button_visibility("Add")
            End If
        End If
    End Sub

#Region "Basic INFO"

    Private Sub BindBuyerBasicInfo()
        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            Dim buyer_id As Integer = Request.QueryString.Get("i")
            Dim dt As DataTable = New DataTable()
            Dim strQuery As String = ""

            strQuery = "SELECT A.buyer_id, " & _
                          "ISNULL(A.company_name, '') AS company_name, " & _
                          "ISNULL(A.contact_title, '') AS contact_title, " & _
                          "ISNULL(A.contact_first_name, '') AS contact_first_name, " & _
                          "ISNULL(A.contact_last_name, '') AS contact_last_name, " & _
                          "ISNULL(A.email, '') AS email, " & _
                          "ISNULL(A.website, '') AS website, " & _
                          "ISNULL(A.phone, '') AS phone, " & _
                          "ISNULL(A.mobile, '') AS mobile, " & _
                          "ISNULL(A.fax, '') AS fax," & _
                          "ISNULL(A.address1, '') AS address1, " & _
                          "ISNULL(A.address2, '') AS address2, " & _
                          "ISNULL(A.city, '') AS city, " & _
                          "ISNULL(A.state_id, 0) AS state_id," & _
                          "ISNULL(S.name,'') As state, " & _
                          "ISNULL(A.zip, '') AS zip, " & _
                          "ISNULL(A.country_id, 0) AS country_id, " & _
                          "ISNULL(C.name, '') AS country,ISNULL(A.is_active, 0) AS is_active " & _
                          "FROM tbl_reg_buyers A  Left join tbl_master_states S ON A.state_id=S.state_id Left JOIN tbl_master_countries C ON S.country_id=C.country_id WHERE A.buyer_id =" & buyer_id.ToString()
            dt = SqlHelper.ExecuteDatatable(strQuery)
            If dt.Rows.Count > 0 Then
                txt_company.Text = dt.Rows(0)("company_name")
                lbl_company.Text = dt.Rows(0)("company_name")
                txt_contact_title.Text = dt.Rows(0)("contact_title")
                lbl_contact_title.Text = dt.Rows(0)("contact_title")
                txt_first_name.Text = dt.Rows(0)("contact_first_name")
                lbl_first_name.Text = dt.Rows(0)("contact_first_name")
                txt_last_name.Text = dt.Rows(0)("contact_last_name")
                lbl_last_name.Text = dt.Rows(0)("contact_last_name")
                txt_email.Text = dt.Rows(0)("email")
                txt_confirm_email.Text = dt.Rows(0)("email")
                lbl_email.Text = dt.Rows(0)("email")
                lbl_confirm_email.Text = dt.Rows(0)("email")
                txt_website.Text = dt.Rows(0)("website")
                lbl_website.Text = dt.Rows(0)("website")
                txt_mobile.Text = dt.Rows(0)("mobile")
                lbl_mobile.Text = dt.Rows(0)("mobile")
                txt_phone.Text = dt.Rows(0)("phone")
                lbl_phone.Text = dt.Rows(0)("phone")
                txt_fax.Text = dt.Rows(0)("fax")
                lbl_fax.Text = dt.Rows(0)("fax")
                txt_address1.Text = dt.Rows(0)("address1")
                lbl_address1.Text = dt.Rows(0)("address1")
                txt_address2.Text = dt.Rows(0)("address2")
                lbl_address2.Text = dt.Rows(0)("address2")
                txt_city.Text = dt.Rows(0)("city")
                lbl_city.Text = dt.Rows(0)("city")
                chk_is_active.Checked = dt.Rows(0)("is_active")
                If dt.Rows(0)("is_active") Then
                    lbl_is_active.Text = "Yes"
                Else
                    lbl_is_active.Text = "No"
                End If

                txt_zip.Text = dt.Rows(0)("zip")
                lbl_zip.Text = dt.Rows(0)("zip")
                If Not ddl_country.Items.FindByValue(dt.Rows(0)("country_id")) Is Nothing Then
                    ddl_country.ClearSelection()
                    ddl_country.Items.FindByValue(dt.Rows(0)("country_id")).Selected = True
                End If
                fill_states()
                If Not ddl_state.Items.FindByValue(dt.Rows(0)("state_id")) Is Nothing Then
                    ddl_state.ClearSelection()
                    ddl_state.Items.FindByValue(dt.Rows(0)("state_id")).Selected = True
                End If
                lbl_state.Text = dt.Rows(0)("state")
                lbl_country.Text = dt.Rows(0)("country")
                dt = Nothing

                dt = get_buyer_admin_detail(buyer_id)
                If dt.Rows.Count > 0 Then
                    txt_user_id.Text = dt.Rows(0)("username")
                    lbl_user_id.Text = dt.Rows(0)("username")
                    txt_password.Attributes.Add("value", dt.Rows(0)("password"))
                    ViewState("password") = Security.EncryptionDecryption.DecryptValue(dt.Rows(0)("password"))
                    txt_retype_password.Attributes.Add("value", dt.Rows(0)("password"))

                    lbl_password.Text = dt.Rows(0)("password").ToString().Substring(0, IIf(dt.Rows(0)("password").ToString().Length > 3, 3, dt.Rows(0)("password").ToString().Length)).PadRight(10, "X")

                End If
                dt = Nothing

                dt = get_buyer_how_find_us(buyer_id)
                lst_how_find_us.DataSource = dt
                lst_how_find_us.DataBind()
                For i As Integer = 0 To dt.Rows.Count - 1
                    For Each lst As ListItem In chklst_how_find_us.Items
                        If dt.Rows(i)("how_find_us_id") = lst.Value Then
                            lst.Selected = True
                        End If
                    Next
                Next
                dt = Nothing
                dt = get_buyer_business_types(buyer_id)
                lst_business_types.DataSource = dt
                lst_business_types.DataBind()
                For i As Integer = 0 To dt.Rows.Count - 1
                    For Each lst As ListItem In chklst_business_types.Items
                        If dt.Rows(i)("business_type_id") = lst.Value Then
                            lst.Selected = True
                        End If
                    Next
                Next
                dt = Nothing
                dt = get_buyer_industry_types(buyer_id)
                lst_industry_types.DataSource = dt
                lst_industry_types.DataBind()
                For i As Integer = 0 To dt.Rows.Count - 1
                    For Each lst As ListItem In chklst_industry_types.Items
                        If dt.Rows(i)("industry_type_id") = lst.Value Then
                            lst.Selected = True
                        End If
                    Next
                Next
                dt = Nothing
                dt = get_buyer_companies_linked(buyer_id)
                lst_companies_linked.DataSource = dt
                lst_companies_linked.DataBind()
                For i As Integer = 0 To dt.Rows.Count - 1
                    For Each lst As ListItem In chklst_companies_linked.Items
                        If dt.Rows(i)("seller_id") = lst.Value Then
                            lst.Selected = True
                        End If
                    Next
                Next
            End If
            dt.Dispose()
        End If
    End Sub
    Private Function get_buyer_companies_linked(ByVal buyer_id As Integer) As DataTable
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable()
        strQuery = "SELECT 	ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.seller_id, 0) AS seller_id,ISNULL(S.company_name,'') as company_name " & _
                    "FROM tbl_reg_buyer_seller_mapping A INNER JOIN tbl_reg_sellers S ON A.seller_id=S.seller_id " & _
                    "WHERE ISNULL(S.is_active,0)=1 and A.buyer_id =" & buyer_id.ToString()
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function
    'Private Function get_buyer_industry_types(ByVal buyer_id As Integer) As DataTable
    '    Dim strQuery As String = ""
    '    Dim dt As DataTable = New DataTable()
    '    strQuery = "SELECT 	ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.industry_type_id, 0) AS industry_type_id,ISNULL(I.name,'') as industry_type " & _
    '                "FROM tbl_reg_buyer_industry_type_mapping A INNER JOIN tbl_master_industry_types I ON A.industry_type_id=I.industry_type_id " & _
    '                "WHERE A.buyer_id =" & buyer_id.ToString()
    '    dt = SqlHelper.ExecuteDatatable(strQuery)
    '    Return dt
    'End Function
    'Private Function get_buyer_business_types(ByVal buyer_id As Integer) As DataTable
    '    Dim strQuery As String = ""
    '    Dim dt As DataTable = New DataTable()
    '    strQuery = "SELECT 	ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.business_type_id, 0) AS business_type_id,ISNULL(B.name,'') as business_type " & _
    '                "FROM tbl_reg_buyer_business_type_mapping A INNER JOIN tbl_master_business_types B ON A.business_type_id=B.business_type_id " & _
    '                "WHERE A.buyer_id =" & buyer_id.ToString()
    '    dt = SqlHelper.ExecuteDatatable(strQuery)
    '    Return dt
    'End Function
    Private Function get_buyer_how_find_us(ByVal buyer_id As Integer) As DataTable
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable()
        strQuery = "SELECT 	ISNULL(A.buyer_id, 0) AS buyer_id,ISNULL(A.how_find_us_id, 0) AS how_find_us_id,ISNULL(F.name,'') As how_find_us FROM tbl_reg_buyer_find_us_assignment A INNER JOIN tbl_master_how_find_us F ON A.how_find_us_id=F.how_find_us_id WHERE buyer_id=" & buyer_id.ToString()
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function
    Private Function get_buyer_admin_detail(ByVal buyer_id As Integer) As DataTable
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable()
        strQuery = "SELECT ISNULL(A.username, '') AS username,ISNULL(A.password, '') AS password FROM tbl_reg_buyer_users A WHERE A.buyer_id =" & buyer_id.ToString()
        dt = SqlHelper.ExecuteDatatable(strQuery)
        Return dt
    End Function
    Private Sub set_button_visibility(ByVal mode As String)
        If mode.ToUpper() = "VIEW" Then
            imgbtn_basic_edit.Visible = True
            imgbtn_basic_save.Visible = False
            imgbtn_basic_update.Visible = False
            imgbtn_basic_cancel.Visible = False
            rad_captcha1.Visible = False
            chk_agree.Visible = False

        ElseIf mode.ToUpper() = "EDIT" Then
            imgbtn_basic_edit.Visible = False
            imgbtn_basic_save.Visible = False
            imgbtn_basic_update.Visible = True
            imgbtn_basic_cancel.Visible = True
            rad_captcha1.Visible = False
            chk_agree.Visible = False

        Else
            imgbtn_basic_edit.Visible = False
            imgbtn_basic_save.Visible = True
            imgbtn_basic_update.Visible = False
            imgbtn_basic_cancel.Visible = False
            rad_captcha1.Visible = True
            chk_agree.Visible = True
            'Tabstrip1.Tabs(1).Enabled = False
            'Tabstrip1.Tabs(2).Enabled = False
            'Tabstrip1.Tabs(3).Enabled = False
        End If
    End Sub
    Private Sub fill_countries()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select country_id,name,ISNULL(code,'') As code from tbl_master_countries"
        ds = SqlHelper.ExecuteDataset(strQuery)
        ddl_country.DataSource = ds
        ddl_country.DataTextField = "name"
        ddl_country.DataValueField = "country_id"
        ddl_country.DataBind()
        ddl_country.Items.Insert(0, New ListItem("--Select--", ""))
        ds.Dispose()
    End Sub
    Private Sub fill_states()
        Dim ds As New DataSet
        Dim strQuery As String = ""
        Dim country_id As Integer = 0
        If ddl_country.SelectedItem.Value <> "" Then
            country_id = ddl_country.SelectedItem.Value
            strQuery = "select state_id,country_id,name,ISNULL(code,'') from tbl_master_states where country_id=" & country_id.ToString()
            ds = SqlHelper.ExecuteDataset(strQuery)
            ddl_state.DataSource = ds
            ddl_state.DataTextField = "name"
            ddl_state.DataValueField = "state_id"
            ddl_state.DataBind()
        End If
        ddl_state.Items.Insert(0, New ListItem("--Select--", ""))
        ds.Dispose()

    End Sub
    Private Sub fill_how_find_us()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select how_find_us_id,name,ISNULL(description,'') as description,ISNULL(sno,99)as sno from tbl_master_how_find_us order by sno"
        ds = SqlHelper.ExecuteDataset(strQuery)
        chklst_how_find_us.DataSource = ds
        chklst_how_find_us.DataTextField = "name"
        chklst_how_find_us.DataValueField = "how_find_us_id"
        chklst_how_find_us.DataBind()
        ds.Dispose()
    End Sub
    'Private Sub fill_business_types()
    '    Dim ds As DataSet
    '    Dim strQuery As String = ""
    '    strQuery = "select business_type_id,name,ISNULL(description,'') as description,ISNULL(sno,99)as sno from tbl_master_business_types order by sno"
    '    ds = SqlHelper.ExecuteDataset(strQuery)
    '    chklst_business_types.DataSource = ds
    '    chklst_business_types.DataTextField = "name"
    '    chklst_business_types.DataValueField = "business_type_id"
    '    chklst_business_types.DataBind()
    '    ds.Dispose()
    'End Sub
    'Private Sub fill_industry_types()
    '    Dim ds As DataSet
    '    Dim strQuery As String = ""
    '    strQuery = "select industry_type_id,name,ISNULL(description,'') AS description,ISNULL(sno,999) as sno from tbl_master_industry_types order by sno"
    '    ds = SqlHelper.ExecuteDataset(strQuery)
    '    chklst_industry_types.DataSource = ds
    '    chklst_industry_types.DataTextField = "name"
    '    chklst_industry_types.DataValueField = "industry_type_id"
    '    chklst_industry_types.DataBind()
    '    ds.Dispose()
    'End Sub
    Private Sub fill_companies()
        Dim ds As DataSet
        Dim strQuery As String = ""
        strQuery = "select seller_id,company_name from tbl_reg_sellers where is_active=1 order by company_name"
        ds = SqlHelper.ExecuteDataset(strQuery)
        chklst_companies_linked.DataSource = ds
        chklst_companies_linked.DataTextField = "company_name"
        chklst_companies_linked.DataValueField = "seller_id"
        chklst_companies_linked.DataBind()
        ds.Dispose()
    End Sub
    Private Sub set_form_mode(ByVal flag As Boolean)
        txt_company.Visible = flag
        txt_contact_title.Visible = flag
        txt_first_name.Visible = flag
        txt_last_name.Visible = flag
        txt_email.Visible = flag
        txt_confirm_email.Visible = flag
        txt_website.Visible = flag
        txt_mobile.Visible = flag
        txt_phone.Visible = flag
        txt_fax.Visible = flag
        txt_address1.Visible = flag
        txt_address2.Visible = flag
        txt_city.Visible = flag
        ddl_state.Visible = flag
        txt_zip.Visible = flag
        ddl_country.Visible = flag
        txt_user_id.Visible = flag
        txt_password.Visible = flag
        txt_retype_password.Visible = flag
        chklst_how_find_us.Visible = flag
        chklst_business_types.Visible = flag
        chklst_industry_types.Visible = flag
        chk_is_active.Visible = flag
        chklst_companies_linked.Visible = flag
        'lbl_confirm_email_caption.Visible = flag
        lbl_retype_password_caption.Visible = flag

        lbl_company.Visible = Not (flag)
        lbl_contact_title.Visible = Not (flag)
        lbl_first_name.Visible = Not (flag)
        lbl_last_name.Visible = Not (flag)
        lbl_email.Visible = Not (flag)
        lbl_confirm_email.Visible = Not (flag)

        lbl_website.Visible = Not (flag)
        lbl_mobile.Visible = Not (flag)
        lbl_phone.Visible = Not (flag)
        lbl_fax.Visible = Not (flag)
        lbl_address1.Visible = Not (flag)
        lbl_address2.Visible = Not (flag)
        lbl_city.Visible = Not (flag)
        lbl_state.Visible = Not (flag)
        lbl_zip.Visible = Not (flag)
        lbl_country.Visible = Not (flag)
        lbl_user_id.Visible = Not (flag)
        lbl_password.Visible = Not (flag)
        lst_how_find_us.Visible = Not (flag)
        lst_business_types.Visible = Not (flag)
        lst_industry_types.Visible = Not (flag)
        lst_companies_linked.Visible = Not (flag)
        lbl_is_active.Visible = Not (flag)
        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            txt_user_id.Visible = False
            lbl_user_id.Visible = True
        End If

    End Sub

    Protected Sub ddl_country_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_country.SelectedIndexChanged
        If ddl_country.SelectedItem.Value <> "" Then
            fill_states()
        Else
            ddl_state.Items.Clear()
            ddl_state.Items.Insert(0, New ListItem("--Select--", ""))
        End If
    End Sub

    Protected Sub imgbtn_basic_save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_basic_save.Click
        ViewState("password") = txt_password.Text.Trim()
        If Page.IsValid Then
            If Not chk_agree.Checked Then
                lblMessage.Text = "Please agree with our terms & conditions."
                txt_password.Attributes.Add("value", ViewState("password"))
                txt_retype_password.Attributes.Add("value", ViewState("password"))
                Exit Sub
            End If
            Dim i As Integer = validate_user_id()
            If i = 1 Then
                lblMessage.Text = "User exists. Please choose other user id."
                Exit Sub
            End If
            i = Validate_email()
            If i = 1 Then
                lblMessage.Text = "Email is already in our record."
                Exit Sub
            End If
            Dim buyer_id As Integer = 0
            Dim strQuery As String = ""
            strQuery = "Insert into tbl_reg_buyers(company_name,contact_title,contact_first_name,contact_last_name,email,website,mobile,phone,fax,address1,address2,city,state_id,zip,country_id,is_active)Values('" & txt_company.Text.Trim() & "','" & txt_contact_title.Text.Trim() & "','" & txt_first_name.Text.Trim() & "','" & txt_last_name.Text.Trim() & "','" & txt_email.Text.Trim() & "','" & txt_website.Text.Trim() & "','" & txt_mobile.Text.Trim() & "','" & txt_phone.Text.Trim() & "','" & txt_fax.Text.Trim() & "','" & txt_address1.Text.Trim() & "','" & txt_address2.Text.Trim() & "','" & txt_city.Text.Trim() & "'," & ddl_state.SelectedItem.Value & ",'" & txt_zip.Text.Trim() & "'," & ddl_country.SelectedItem.Value & "," & CInt(chk_is_active.Checked) & ") select SCOPE_IDENTITY()"
            buyer_id = SqlHelper.ExecuteScalar(strQuery)
            If buyer_id > 0 Then
                Dim is_admin_buyer As Integer = check_is_admin_buyer(buyer_id)
                strQuery = "Insert into tbl_reg_buyer_users(buyer_id,first_name,last_name,title,username,password,email,is_active,submit_date,submit_by_user_id,is_admin) Values(" & buyer_id.ToString & ",'" & txt_first_name.Text.Trim() & "','" & txt_last_name.Text.Trim() & "','" & txt_contact_title.Text.Trim() & "','" & txt_user_id.Text.Trim() & "','" & txt_password.Text.Trim() & "','" & txt_email.Text.Trim() & "'," & CInt(chk_is_active.Checked) & ",GETDATE()," & CommonCode.Fetch_Cookie_Shared("user_id") & "," & is_admin_buyer & ")"
                SqlHelper.ExecuteNonQuery(strQuery)
                Update_buyer_how_find_us(buyer_id)
                Update_buyer_business_types(buyer_id)
                Update_buyer_industry_types(buyer_id)
                Update_buyer_companies_linked(buyer_id)
                lblMessage.Text = "New buyer created successfully."
                Response.Redirect("/Bidders/Buyers.aspx")
            Else
                txt_password.Attributes.Add("value", ViewState("password"))
                txt_retype_password.Attributes.Add("value", ViewState("password"))
                lblMessage.Text = "Error in creating new buyer. Please contact administrator."
            End If
        Else
            txt_password.Attributes.Add("value", ViewState("password"))
            txt_retype_password.Attributes.Add("value", ViewState("password"))
        End If
    End Sub
    Private Function check_is_admin_buyer(ByVal buyer_id As Integer) As Integer
        Dim is_admin_buyer As Integer = 0
        Dim strQuery As String = ""
        strQuery = "if exists(select buyer_user_id from tbl_reg_buyer_users where buyer_id=" & buyer_id.ToString() & ") select 0 else select 1"
        Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)

        Return is_admin_buyer
    End Function

    Private Function validate_user_id() As Integer
        Dim strQuery As String = ""
        strQuery = "if exists(select buyer_id from vw_login_users where username='" & txt_user_id.Text.Trim() & "') select 1 else select 0"
        Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)
        Return i
    End Function
    Private Function Validate_email() As Integer
        Dim strQuery As String = ""
        If Not String.IsNullOrEmpty(Request("i")) Then
            strQuery = "if exists(select buyer_id from tbl_reg_buyers where email='" & txt_email.Text.Trim() & "' and buyer_id <> " & Request("i") & ") select 1 else select 0"
        Else
            strQuery = "if exists(select buyer_id from tbl_reg_buyers where email='" & txt_email.Text.Trim() & "') select 1 else select 0"
        End If

        Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)
        Return i
    End Function
    Private Sub Update_buyer_business_types(ByVal buyer_id As Integer)
        For Each lst As ListItem In chklst_business_types.Items
            Dim strQuery As String = ""
            Dim i As String = lst.Value
            If lst.Selected Then
                strQuery = "if not exists(select mapping_id from tbl_reg_buyer_business_type_mapping where buyer_id=" & buyer_id.ToString() & " and business_type_id=" & i & ") insert into tbl_reg_buyer_business_type_mapping(buyer_id,business_type_id) Values(" & buyer_id.ToString() & "," & i & ")"
            Else
                strQuery = "delete from tbl_reg_buyer_business_type_mapping where buyer_id=" & buyer_id.ToString() & " and business_type_id=" & i
            End If

            SqlHelper.ExecuteNonQuery(strQuery)
        Next
    End Sub
    Private Sub Update_buyer_industry_types(ByVal buyer_id As Integer)
        For Each lst As ListItem In chklst_industry_types.Items
            Dim strQuery As String = ""
            Dim i As String = lst.Value
            If lst.Selected Then
                strQuery = "if not exists(select mapping_id from tbl_reg_buyer_industry_type_mapping where buyer_id=" & buyer_id.ToString() & " and industry_type_id=" & i & ") insert into tbl_reg_buyer_industry_type_mapping(buyer_id,industry_type_id) Values(" & buyer_id.ToString() & "," & i & ")"
            Else
                strQuery = "delete from tbl_reg_buyer_industry_type_mapping where buyer_id=" & buyer_id.ToString() & " and industry_type_id=" & i
            End If
            SqlHelper.ExecuteNonQuery(strQuery)
        Next
    End Sub
    Private Sub Update_buyer_companies_linked(ByVal buyer_id As Integer)
        For Each lst As ListItem In chklst_companies_linked.Items
            Dim strQuery As String = ""
            Dim i As String = lst.Value
            If lst.Selected Then
                strQuery = "if not exists(select mapping_id from tbl_reg_buyer_seller_mapping where buyer_id=" & buyer_id.ToString() & " and seller_id=" & i & ") insert into tbl_reg_buyer_seller_mapping(buyer_id,seller_id) Values(" & buyer_id.ToString() & "," & i & ")"
            Else
                strQuery = "delete from tbl_reg_buyer_seller_mapping where buyer_id=" & buyer_id.ToString() & " and seller_id=" & i
            End If
            SqlHelper.ExecuteNonQuery(strQuery)
        Next
    End Sub
    Private Sub Update_buyer_how_find_us(ByVal buyer_id As Integer)
        For Each lst As ListItem In chklst_how_find_us.Items
            Dim strQuery As String = ""
            Dim i As String = lst.Value
            If lst.Selected Then
                strQuery = "if not exists(select assignment_id from tbl_reg_buyer_find_us_assignment where buyer_id=" & buyer_id.ToString() & " and how_find_us_id=" & i & ") insert into tbl_reg_buyer_find_us_assignment(buyer_id,how_find_us_id) Values(" & buyer_id.ToString() & "," & i & ")"
            Else
                strQuery = "delete from tbl_reg_buyer_find_us_assignment where buyer_id=" & buyer_id.ToString() & " and how_find_us_id=" & i
            End If

            SqlHelper.ExecuteNonQuery(strQuery)
        Next
    End Sub

    Protected Sub imgbtn_basic_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_basic_edit.Click
        set_form_mode(True)
        set_button_visibility("Edit")
    End Sub

    Protected Sub imgbtn_basic_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_basic_cancel.Click
        set_form_mode(False)
        set_button_visibility("View")
    End Sub

    Protected Sub imgbtn_basic_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_basic_update.Click
        If Page.IsValid Then

            Dim i As Integer = 0
            i = Validate_email()
            If i = 1 Then
                lblMessage.Text = "Email is already in our record."
                Exit Sub
            End If
            Dim buyer_id As Integer = Request.QueryString.Get("i")
            Dim strQuery As String = ""
            strQuery = "UPDATE tbl_reg_buyers SET " & _
                       "company_name ='" & txt_company.Text.Trim() & "' " & _
                       ",contact_title ='" & txt_contact_title.Text.Trim() & "' " & _
                       ",contact_first_name ='" & txt_first_name.Text.Trim() & "' " & _
                       ",contact_last_name = '" & txt_last_name.Text.Trim() & "' " & _
                       ",email ='" & txt_email.Text.Trim() & "' " & _
                       ",website = '" & txt_website.Text.Trim() & "' " & _
                      ",phone ='" & txt_phone.Text.Trim() & "' " & _
                      ",mobile ='" & txt_mobile.Text.Trim() & "' " & _
                      ",fax = '" & txt_fax.Text.Trim() & "' " & _
                      ",address1 ='" & txt_address1.Text.Trim() & "' " & _
                      ",address2 ='" & txt_address2.Text.Trim() & "' " & _
                      ",city ='" & txt_city.Text.Trim() & "' " & _
                      ",state_id =" & ddl_state.SelectedItem.Value & _
                      ",zip ='" & txt_zip.Text.Trim() & "' " & _
                      ",country_id =" & ddl_country.SelectedItem.Value & _
                    ",is_active =" & CInt(chk_is_active.Checked) & _
                      " WHERE buyer_id = " & buyer_id & " select 1"
            ' txt_company.Text = strQuery
            Dim j As Integer = SqlHelper.ExecuteScalar(strQuery)

            If j = 1 Then
                Dim is_admin_buyer As Integer = check_is_admin_buyer(buyer_id)
                strQuery = "update tbl_reg_buyer_users set first_name='" & txt_first_name.Text.Trim() & "',last_name='" & txt_last_name.Text.Trim() & "',title='" & txt_contact_title.Text.Trim() & "',password='" & txt_password.Text.Trim() & "',email='" & txt_email.Text.Trim() & "',submit_by_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & " where buyer_id=" & buyer_id
                SqlHelper.ExecuteNonQuery(strQuery)
                Update_buyer_how_find_us(buyer_id)
                Update_buyer_business_types(buyer_id)
                Update_buyer_industry_types(buyer_id)
                Update_buyer_companies_linked(buyer_id)
                lblMessage.Text = "Buyer updated successfully."
                BindBuyerBasicInfo()
                set_form_mode(False)
                set_button_visibility("View")
            Else
                lblMessage.Text = "Error in updating. Please contact administrator."
            End If
        End If
    End Sub

#End Region

#Region "OFFICIAL INFO"
    Private Sub Bind_buyer_attachments()
        'Dim strQuery As String = ""
        'strQuery = "SELECT buyer_attachment_id, ISNULL(A.buyer_id, 0) AS buyer_id, ISNULL(A.title, '') AS title,	ISNULL(A.upload_date, '1/1/1900') AS upload_date, " & _
        '"ISNULL(A.upload_by_user_id, 0) AS upload_by_user_id, " & _
        '"ISNULL(A.filename, '') AS filename, " & _
        '"(ISNULL(U.first_name,'') + ISNULL(U.last_name,'')) AS upload_by_user " & _
        '"FROM tbl_reg_buyer_attachments A INNER JOIN tbl_sec_users U on A.upload_by_user_id=U.user_id where A.buyer_id=" & Request.QueryString.Get("i")
        'SqlDataSource1.SelectCommand = strQuery
        'rad_grid_attachment.Rebind()
    End Sub
    Private Sub set_form_mode_official(ByVal flag As Boolean)
        txt_max_amount_bid.Visible = flag
        txt_routing_no.Visible = flag
        txt_account_no.Visible = flag
        txt_name_bank.Visible = flag
        txt_branch_name.Visible = flag
        txt_bank_contact_detail.Visible = flag
        txt_bank_address.Visible = flag
        txt_tax_id_ssn_no.Visible = flag
        txt_no_of_employees.Visible = flag
        txt_annual_turnover.Visible = flag
        lbl_approval_status.Visible = True

        lbl_max_amount_bid.Visible = Not (flag)
        lbl_routing_no.Visible = Not (flag)
        lbl_account_no.Visible = Not (flag)
        lbl_name_bank.Visible = Not (flag)
        lbl_branch_name.Visible = Not (flag)
        lbl_bank_address.Visible = Not (flag)
        lbl_bank_contact_detail.Visible = Not (flag)
        lbl_tax_id_ssn_no.Visible = Not (flag)
        lbl_no_of_employees.Visible = Not (flag)
        lbl_annual_turnover.Visible = Not (flag)
    End Sub
    Private Sub set_button_visibility_official(ByVal mode As String)
        If mode.ToUpper() = "VIEW" Then
            imgbtn_official_edit.Visible = True
            imgbtn_official_update.Visible = False
            imgbtn_official_cancel.Visible = False
            btn_approve.Visible = False
            btn_on_hold.Visible = False
            btn_reject.Visible = False
            btn_panding.Visible = False
            btn_more_details.Visible = False
            btn_activate_deactivate.Visible = False
        ElseIf mode.ToUpper() = "EDIT" Then
            imgbtn_official_edit.Visible = False
            imgbtn_official_update.Visible = True
            imgbtn_official_cancel.Visible = True
            btn_approve.Visible = True
            btn_on_hold.Visible = True
            btn_reject.Visible = True
            btn_panding.Visible = True
            btn_more_details.Visible = True
            btn_activate_deactivate.Visible = True
        Else
            imgbtn_official_edit.Visible = False
            imgbtn_official_update.Visible = False
            imgbtn_official_cancel.Visible = False
            btn_approve.Visible = False
            btn_on_hold.Visible = False
            btn_reject.Visible = False
            btn_panding.Visible = False
            btn_more_details.Visible = False
            btn_activate_deactivate.Visible = False
        End If
    End Sub

    Protected Sub imgbtn_official_edit_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_official_edit.Click
        set_form_mode_official(True)
        set_button_visibility_official("Edit")
    End Sub
    Protected Sub imgbtn_official_update_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_official_update.Click
        'If Page.IsValid Then
        Update_official_info()
        'End If
        'Response.Redirect("/")
    End Sub

    Protected Sub imgbtn_official_cancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgbtn_official_cancel.Click
        set_form_mode_official(False)
        set_button_visibility_official("View")
    End Sub
    Private Sub Update_official_info()
        Dim strQuery As String = ""
        strQuery = "UPDATE tbl_reg_buyers SET " & _
        "max_amt_bid =" & txt_max_amount_bid.Text.Trim() & _
        ",no_of_employees =" & txt_no_of_employees.Text.Trim() & _
        ",annual_turnover =" & txt_annual_turnover.Text.Trim() & _
        ",bank_routing_no ='" & txt_routing_no.Text.Trim() & "'" & _
        ",bank_account_no ='" & txt_account_no.Text.Trim() & "'" & _
        ",bank_name ='" & txt_name_bank.Text.Trim() & "'" & _
        ",bank_contact ='" & txt_bank_contact_detail.Text.Trim() & "'" & _
        ",bank_address ='" & txt_bank_address.Text.Trim() & "'" & _
        ",bank_phone ='" & txt_phone.Text.Trim() & "'" & _
        ",tax_id_ssn_no ='" & txt_tax_id_ssn_no.Text.Trim() & "'" & _
        ",bank_branch_name ='" & txt_branch_name.Text.Trim() & "'" & _
        " WHERE buyer_id =" & Request.QueryString.Get("i")
        SqlHelper.ExecuteNonQuery(strQuery)

        set_form_mode_official(False)
        set_button_visibility_official("View")
        bind_official_detail()
    End Sub
    Private Sub bind_official_detail()
        Dim strQuery As String = ""
        Dim dt As DataTable = New DataTable
        strQuery = "SELECT " & _
                     "ISNULL(A.max_amt_bid, 0) AS max_amt_bid, " & _
                    "ISNULL(A.no_of_employees, 0) AS no_of_employees," & _
                    "ISNULL(A.annual_turnover, 0) AS annual_turnover, " & _
                    "ISNULL(A.bank_routing_no, '') AS bank_routing_no," & _
                    "ISNULL(A.bank_account_no, '') AS bank_account_no," & _
                    "ISNULL(A.bank_name, '') AS bank_name," & _
                    "ISNULL(A.bank_contact, '') AS bank_contact," & _
                    "ISNULL(A.bank_address, '') AS bank_address," & _
                    "ISNULL(A.bank_phone, '') AS bank_phone," & _
                    "ISNULL(A.approval_status,'') AS approval_status," & _
                    "ISNULL(A.is_active,'') AS is_active, " & _
                    "ISNULL(A.tax_id_ssn_no,'') AS tax_id_ssn_no, " & _
                    "ISNULL(A.bank_branch_name,'') AS bank_branch_name " & _
                    "FROM tbl_reg_buyers A WHERE A.buyer_id =" & Request.QueryString.Get("i")
        dt = SqlHelper.ExecuteDataTable(strQuery)

        txt_max_amount_bid.Text = CommonCode.FormatMoney(dt.Rows(0)("max_amt_bid"))
        lbl_max_amount_bid.Text = CommonCode.GetFormatedMoney(dt.Rows(0)("max_amt_bid"))
        txt_routing_no.Text = dt.Rows(0)("bank_routing_no")
        lbl_routing_no.Text = dt.Rows(0)("bank_routing_no")
        txt_account_no.Text = dt.Rows(0)("bank_routing_no")
        lbl_account_no.Text = dt.Rows(0)("bank_routing_no")
        txt_name_bank.Text = dt.Rows(0)("bank_name")
        lbl_name_bank.Text = dt.Rows(0)("bank_name")
        txt_branch_name.Text = dt.Rows(0)("bank_branch_name")
        lbl_branch_name.Text = dt.Rows(0)("bank_branch_name")
        txt_bank_contact_detail.Text = dt.Rows(0)("bank_contact")
        lbl_bank_contact_detail.Text = dt.Rows(0)("bank_contact")
        txt_bank_address.Text = dt.Rows(0)("bank_address")
        lbl_bank_address.Text = dt.Rows(0)("bank_address")
        txt_tax_id_ssn_no.Text = dt.Rows(0)("tax_id_ssn_no")
        lbl_tax_id_ssn_no.Text = dt.Rows(0)("tax_id_ssn_no")
        If dt.Rows(0)("is_active") Then
            btn_activate_deactivate.Text = "Deactivate"
        Else
            btn_activate_deactivate.Text = "Activate"
        End If
        txt_no_of_employees.Text = dt.Rows(0)("no_of_employees")
        lbl_no_of_employees.Text = dt.Rows(0)("no_of_employees")
        txt_annual_turnover.Text = CommonCode.FormatMoney(dt.Rows(0)("annual_turnover"))
        lbl_annual_turnover.Text = CommonCode.GetFormatedMoney(dt.Rows(0)("annual_turnover"))
        'If Not ddl_approval_status.Items.FindByText(dt.Rows(0)("approval_status")) Is Nothing Then
        '    ddl_approval_status.ClearSelection()
        '    ddl_approval_status.Items.FindByText(dt.Rows(0)("approval_status")).Selected = True
        'End If
        lbl_approval_status.Text = dt.Rows(0)("approval_status")

        dt.Dispose()
    End Sub


    Protected Sub btn_approve_Click(sender As Object, e As System.EventArgs) Handles btn_approve.Click
        update_buyer_approval_status("Approved")
    End Sub

    Protected Sub btn_more_details_Click(sender As Object, e As System.EventArgs) Handles btn_more_details.Click
        update_buyer_approval_status("More Detail")

    End Sub

    Protected Sub btn_on_hold_Click(sender As Object, e As System.EventArgs) Handles btn_on_hold.Click
        update_buyer_approval_status("On Hold")

    End Sub

    Protected Sub btn_reject_Click(sender As Object, e As System.EventArgs) Handles btn_reject.Click
        update_buyer_approval_status("Rejected")

    End Sub
    Protected Sub btn_panding_Click(sender As Object, e As System.EventArgs) Handles btn_panding.Click
        update_buyer_approval_status("Pending")
    End Sub
    Protected Sub btn_activate_deactivate_Click(sender As Object, e As System.EventArgs) Handles btn_activate_deactivate.Click
        activate_deactivate_buyer()
        If btn_activate_deactivate.Text = "Deactivate" Then
            update_buyer_approval_status("Deactivate")
        Else
            update_buyer_approval_status("Activate")
        End If


    End Sub
    Private Sub update_buyer_approval_status(ByVal status As String)
        Dim strQuery As String = ""
        strQuery = "UPDATE tbl_reg_buyers SET " & _
        "approval_status ='" & status & "'" & _
        ",approval_status_date =getdate()" & _
        ",approval_status_by_user_id =" & CommonCode.Fetch_Cookie_Shared("user_id") & _
        " WHERE buyer_id =" & Request.QueryString.Get("i")
        SqlHelper.ExecuteNonQuery(strQuery)
        set_form_mode_official(False)
        set_button_visibility_official("View")
        bind_official_detail()
    End Sub
    Private Sub activate_deactivate_buyer()

        Dim strQuery As String = ""
        strQuery = "UPDATE tbl_reg_buyers SET " & _
        "is_active = ~ is_active" & _
        " WHERE buyer_id =" & Request.QueryString.Get("i")
        SqlHelper.ExecuteNonQuery(strQuery)

    End Sub
    Protected Sub btn_submit_comment_Click(sender As Object, e As System.EventArgs) Handles btn_submit_comment.Click
        Dim strQuery As String = ""
        strQuery = "INSERT INTO tbl_reg_buyer_comments(buyer_id, comment, submit_date, submit_by_user_id)" & _
        "VALUES(" & Request.QueryString("i") & ",'" & txt_comment.Text.Trim() & "',getdate()," & CommonCode.Fetch_Cookie_Shared("user_id") & ")"
        SqlHelper.ExecuteNonQuery(strQuery)
        rad_grid_comment.Rebind()
        txt_comment.Text = ""
    End Sub

#Region "Attachments"
    Protected Sub RadGrid_Attachment_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            Dim strQuery As String = ""
            strQuery = "SELECT buyer_attachment_id, ISNULL(A.buyer_id, 0) AS buyer_id, ISNULL(A.title, '') AS title,	ISNULL(A.upload_date, '1/1/1900') AS upload_date, " & _
            "ISNULL(A.upload_by_user_id, 0) AS upload_by_user_id, " & _
            "ISNULL(A.filename, '') AS filename, " & _
            "(ISNULL(U.first_name,'') + ' ' + ISNULL(U.last_name,'')) AS upload_by_user " & _
            "FROM tbl_reg_buyer_attachments A INNER JOIN tbl_sec_users U on A.upload_by_user_id=U.user_id where A.buyer_id=" & Request.QueryString.Get("i") & " order by upload_date desc"
            RadGrid_Attachment.DataSource = SqlHelper.ExecuteDataTable(strQuery)
        End If
    End Sub
    Protected Sub RadGrid_Attachment_DeleteCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)

        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        Dim attachment_id As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("buyer_attachment_id").ToString()
        Try
            SqlHelper.ExecuteNonQuery("DELETE from tbl_reg_buyer_attachments where buyer_attachment_id='" & attachment_id & "'")
        Catch ex As Exception
            RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to delete Attachment. Reason: " + ex.Message))
            e.Canceled = True
        End Try

    End Sub
    Protected Sub RadGrid_Attachment_UpdateCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditableItem of the RadGrid        
        Dim editedItem As GridEditableItem = TryCast(e.Item, GridEditableItem)
        'Get the primary key value using the DataKeyValue.        
        Dim attachment_id As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("buyer_attachment_id").ToString()
        'Access the textbox from the edit form template and store the values in string variables.        

        Dim Title As String = (TryCast(editedItem("Title").Controls(0), TextBox)).Text
        Dim filename As String = ""
        Dim FileUpload As FileUpload = TryCast(editedItem.FindControl("FileUpload1"), FileUpload)
        If FileUpload.HasFile Then filename = FileUpload.FileName
        If FileUpload.HasFile Then
            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_attachments set title='" & Title & "', filename='" & filename & "' where buyer_attachment_id=" & attachment_id)
            UploadFileAttachment(FileUpload, Request.QueryString.Get("i"), attachment_id)
        Else
            SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_attachments set title='" & Title & "' where buyer_attachment_id=" & attachment_id)

        End If


    End Sub
    Protected Sub RadGrid_Attachment_InsertCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditFormInsertItem of the RadGrid        
        Dim insertedItem As GridDataInsertItem = DirectCast(e.Item, GridDataInsertItem)
        Dim attachment_id As Integer = 0
        Dim Title As String = (TryCast(insertedItem("Title").Controls(0), TextBox)).Text
        Dim filename As String = ""
        Dim FileUpload As FileUpload = TryCast(insertedItem.FindControl("FileUpload1"), FileUpload)

        If FileUpload.HasFile Then
            filename = FileUpload.FileName
        End If

        Try
            If FileUpload.HasFile Then
                attachment_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_reg_buyer_attachments(buyer_id, title, upload_date, upload_by_user_id, filename) VALUES (" & Request.QueryString.Get("i") & ", '" & Title & "', getdate(), " & CommonCode.Fetch_Cookie_Shared("user_id") & ", '" & filename & "' )   select scope_identity()")
                UploadFileAttachment(FileUpload, Request.QueryString.Get("i"), attachment_id)
            End If
        Catch ex As Exception
            RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to insert attachment. Reason: " + ex.Message))
            e.Canceled = True
        End Try

    End Sub
    Private Sub UploadFileAttachment(ByVal FileUpload As FileUpload, ByVal buyer_id As Integer, ByVal attachment_id As Integer)
        Try
            Dim filename As String = ""
            If FileUpload.HasFile Then
                filename = FileUpload.FileName

            End If

            Dim pathToCreate As String = "../Upload/Bidders/official_attachments/" & buyer_id
            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
            End If

            pathToCreate = pathToCreate & "/" & attachment_id
            If Not System.IO.Directory.Exists(Server.MapPath(pathToCreate)) Then
                System.IO.Directory.CreateDirectory(Server.MapPath(pathToCreate))
            End If
            Dim infoFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath(pathToCreate) & "/" & filename)
            If infoFile.Exists Then
                System.IO.File.Delete(Server.MapPath(pathToCreate) & "/" & filename)
            End If
            FileUpload.PostedFile.SaveAs(Server.MapPath(pathToCreate) & "/" & filename)

        Catch ex As Exception
            RadGrid_Attachment.Controls.Add(New LiteralControl("Unable to update File. Reason: " + ex.Message))

        End Try
    End Sub
#End Region


#End Region

#Region "Sub-Logins"
    Protected Sub rad_grid_sublogins_InsertCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        'Get the GridEditFormInsertItem of the RadGrid        
        Dim insertedItem As GridDataInsertItem = DirectCast(e.Item, GridDataInsertItem)

        Dim buyer_id As Integer = Request.QueryString.Get("i")
        Dim first_name As String = (TryCast(insertedItem("first_name").Controls(0), TextBox)).Text

        Dim last_name As String = (TryCast(insertedItem("last_name").Controls(0), TextBox)).Text
        Dim title As String = (TryCast(insertedItem("title").Controls(0), TextBox)).Text
        Dim username As String = (TryCast(insertedItem("username").Controls(0), TextBox)).Text
        Dim password As String = (TryCast(insertedItem("password").Controls(0), TextBox)).Text
        Dim bidding_limit As Double = (TryCast(insertedItem("bidding_limit").Controls(0), TextBox)).Text
        Dim rdolst_is_active As RadioButtonList = (TryCast(insertedItem.FindControl("rdo_is_active"), RadioButtonList))
        Dim is_active As Integer = 0

        If rdolst_is_active.SelectedItem.Value = "y" Then
            is_active = 1

        Else
            is_active = 0
        End If
        Try
            Dim is_valid As Integer = validate_sub_login_id(username)
            If is_valid = 1 Then
                rad_grid_sublogins.Controls.Add(New LiteralControl("Username already exists."))
            Else
                Dim buyer_user_id = SqlHelper.ExecuteScalar("INSERT INTO tbl_reg_buyer_users(buyer_id, first_name, last_name,title,username,password,bidding_limit,is_active,submit_date,submit_by_user_id,is_admin) VALUES (" & buyer_id & ", '" & first_name & "','" & last_name & "','" & title & "','" & username & "','" & password & "'," & bidding_limit & "," & is_active & ",getdate(), " & CommonCode.Fetch_Cookie_Shared("user_id") & ",0)   select scope_identity()")
            End If
        Catch ex As Exception
            rad_grid_sublogins.Controls.Add(New LiteralControl("Unable to add Sub-Login. Reason: " + ex.Message))
            e.Canceled = True
        End Try
    End Sub

    'Protected Sub rad_grid_sublogins_ItemCreated(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles rad_grid_sublogins.ItemCreated
    '    If (TypeOf e.Item Is GridEditableItem AndAlso e.Item.IsInEditMode) Then
    '        Dim item As GridEditableItem = CType(e.Item, GridEditableItem)
    '        If (TypeOf e.Item Is GridDataInsertItem) Then
    '            CType(item.FindControl("PerformInsertButton"), ImageButton).ValidationGroup = "val1"
    '        Else
    '            CType(item.FindControl("UpdateButton"), ImageButton).ValidationGroup = "val1"
    '        End If
    '    End If
    'End Sub
    Protected Sub rad_grid_sublogins_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles rad_grid_sublogins.NeedDataSource
        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            Dim strQuery As String = ""
            strQuery = "SELECT " & _
            "buyer_user_id," & _
            "ISNULL(A.buyer_id, 0) AS buyer_id," & _
            "ISNULL(A.first_name, '') AS first_name," & _
            "ISNULL(A.last_name, '') AS last_name," & _
            "ISNULL(A.title, '') AS title," & _
            "ISNULL(A.username, '') AS username," & _
            "ISNULL(A.password, '') AS password," & _
            "ISNULL(A.bidding_limit, 0) AS bidding_limit," & _
            "ISNULL(A.is_active, 0) AS is_active" & _
            " FROM tbl_reg_buyer_users A where buyer_id=" & Request.QueryString.Get("i") & " and is_admin=0"
            rad_grid_sublogins.DataSource = SqlHelper.ExecuteDataTable(strQuery)
        End If
    End Sub

    Protected Sub rad_grid_sublogins_UpdateCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles rad_grid_sublogins.UpdateCommand
        'Get the GridEditableItem of the RadGrid        
        Dim editedItem As GridEditableItem = TryCast(e.Item, GridEditableItem)
        'Get the primary key value using the DataKeyValue.        
        Dim buyer_user_id As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("buyer_user_id").ToString()
        'Access the textbox from the edit form template and store the values in string variables.        
        Dim first_name As String = (TryCast(editedItem("first_name").Controls(0), TextBox)).Text
        Dim last_name As String = (TryCast(editedItem("last_name").Controls(0), TextBox)).Text
        Dim title As String = (TryCast(editedItem("title").Controls(0), TextBox)).Text
        Dim username As String = (TryCast(editedItem("username").Controls(0), TextBox)).Text
        Dim password As String = (TryCast(editedItem("password").Controls(0), TextBox)).Text
        Dim bidding_limit As Double = (TryCast(editedItem("bidding_limit").Controls(0), TextBox)).Text
        Dim rdolst_is_active As RadioButtonList = TryCast(editedItem.FindControl("rdo_is_active"), RadioButtonList)
        Dim is_active As Integer = 0
        If rdolst_is_active.SelectedItem.Value = "y" Then
            is_active = 1
        Else
            is_active = 0
        End If
        SqlHelper.ExecuteNonQuery("update tbl_reg_buyer_users set first_name='" & first_name & "',last_name='" & last_name & "',title='" & title & "',username='" & username & "', password='" & password & "',bidding_limit=" & bidding_limit & ",is_active=" & is_active & " where buyer_user_id=" & buyer_user_id)
    End Sub
    Protected Sub rad_grid_sublogins_DeleteCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles rad_grid_sublogins.DeleteCommand
        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        Dim buyer_user_id As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("buyer_user_id").ToString()
        Dim buyer_id As String = item("buyer_id").Text
        Try
            SqlHelper.ExecuteNonQuery("DELETE from tbl_reg_buyer_users where buyer_user_id='" & buyer_user_id & "'")
            SqlHelper.ExecuteNonQuery("DELETE from tbl_reg_buyers where buyer_id='" & buyer_id & "'")
        Catch ex As Exception
            rad_grid_sublogins.Controls.Add(New LiteralControl("Unable to delete Sub-Login. Reason: " + ex.Message))
            e.Canceled = True
        End Try

    End Sub
    Private Function validate_sub_login_id(ByVal sub_login_username As String) As Integer
        Dim strQuery As String = ""
        strQuery = "if exists(select buyer_id from vw_login_users where username='" & sub_login_username & "') select 1 else select 0"
        Dim i As Integer = SqlHelper.ExecuteScalar(strQuery)
        Return i
    End Function
    'Protected Sub rad_grid_sublogins_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles rad_grid_sublogins.ItemDataBound
    '    Dim is_active As Boolean = False
    '    'If TypeOf e.Item Is GridEditableItem AndAlso e.Item.IsInEditMode Then
    '    '    Dim dataItem As GridEditableItem = CType(e.Item, GridEditableItem)
    '    '    'txtTest.Text = CType(dataItem.FindControl("lbl_is_active1"), Label).Text  '. "sanjay" 'is_active
    '    '    Dim rdo_is_active As RadioButtonList = DirectCast(dataItem.FindControl("rdo_is_active"), RadioButtonList)
    '    '    If CType(dataItem.FindControl("lbl_is_active1"), Label).Text.ToLower() = "yes" Then
    '    '        is_active = True
    '    '    Else
    '    '        is_active = False
    '    '    End If
    '    '    If is_active Then
    '    '        rdo_is_active.Items.FindByValue("y").Selected = True
    '    '    Else
    '    '        rdo_is_active.Items.FindByValue("n").Selected = True
    '    '    End If
    '    'End If
    'End Sub
    Protected Function checked(ByVal r As Object) As Boolean
        If r Is DBNull.Value Then
            Return True
        ElseIf r = True Then
            Return True
        Else
            Return False
        End If
    End Function

#End Region

#Region "Invite Auctions"

    Protected Sub rad_grid_invite_auctions_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles rad_grid_invite_auctions.NeedDataSource
        If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
            Dim strQuery As String = ""
            strQuery = "SELECT  " & _
                       "A.auction_id, " & _
                      "ISNULL(A.code, '') AS code," & _
                      "ISNULL(A.title, '') AS title," & _
                      "ISNULL(L.name, '') AS stock_location," & _
                      "ISNULL(A.start_date, '1/1/1900') AS start_date," & _
                      "ISNULL(A.end_date, '1/1/1900') AS end_date," & _
                      "ISNULL(A.time_zone, 0) AS time_zone," & _
                      "case	when ISNULL(A.start_date, '1/1/1900')<getdate() and ISNULL(A.end_date, '1/1/1900')>getdate() then 'Running'" & _
                      "when ISNULL(A.end_date, '1/1/1900')<getdate() then 'Bid Over'" & _
                      "else 'Upcoming' End as status, " & _
                      "case	when ISNULL(I.auction_id,0) <> 0 then 1 " & _
                      "else 0 End as is_invited " & _
                     "FROM tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations L on A.stock_location_id=L.stock_location_id LEFT JOIN tbl_auction_invitation_filter_values I on A.auction_id=I.auction_id and I.buyer_id=" & Request.QueryString.Get("i") & " WHERE exists(select mapping_id from tbl_reg_buyer_seller_mapping where seller_id=A.seller_id and buyer_id=" & Request.QueryString.Get("i") & ") and ISNULL(A.end_date, '1/1/1900')>getdate() and A.is_active=1"
            rad_grid_invite_auctions.DataSource = SqlHelper.ExecuteDatatable(strQuery)
        End If
    End Sub
    Protected Sub btn_invite_all_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_invite_all.Click
        For Each dataItem As GridDataItem In rad_grid_invite_auctions.MasterTableView.Items
            Dim is_checked As Boolean = TryCast(dataItem.FindControl("CheckBox1"), CheckBox).Checked
            Dim strQuery As String = ""
            Dim auction_id As String = dataItem("auction_id").Text.ToString()
            If is_checked Then
                strQuery = "if not exists(select invitation_filter_value_id from tbl_auction_invitation_filter_values where buyer_id=" & Request.QueryString.Get("i") & " and ISNULL(invitation_filter_id,0)=0 and auction_id=" & auction_id & ")Insert into tbl_auction_invitation_filter_values(buyer_id,auction_id,invitation_filter_id,business_type_id,industry_type_id)Values(" & Request.QueryString.Get("i") & "," & auction_id & ",0,0,0) "
            Else
                strQuery = "delete from tbl_auction_invitation_filter_values where buyer_id=" & Request.QueryString.Get("i") & " and auction_id=" & auction_id
            End If


            SqlHelper.ExecuteNonQuery(strQuery)

        Next
    End Sub
    Protected Sub ToggleRowSelection(ByVal sender As Object, ByVal e As EventArgs)

        TryCast(TryCast(sender, CheckBox).NamingContainer, GridItem).Selected = TryCast(sender, CheckBox).Checked
        Dim checkHeader As Boolean = True
        For Each dataItem As GridDataItem In rad_grid_invite_auctions.MasterTableView.Items
            If Not TryCast(dataItem.FindControl("CheckBox1"), CheckBox).Checked Then
                checkHeader = False
                Exit For
            End If
        Next
        Dim headerItem As GridHeaderItem = TryCast(rad_grid_invite_auctions.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)
        TryCast(headerItem.FindControl("headerChkbox"), CheckBox).Checked = checkHeader
    End Sub
    Protected Sub ToggleSelectedState(ByVal sender As Object, ByVal e As EventArgs)
        Dim headerCheckBox As CheckBox = TryCast(sender, CheckBox)
        For Each dataItem As GridDataItem In rad_grid_invite_auctions.MasterTableView.Items
            TryCast(dataItem.FindControl("CheckBox1"), CheckBox).Checked = headerCheckBox.Checked
            dataItem.Selected = headerCheckBox.Checked
        Next
    End Sub
    Private Sub SelectInvitedAuctions()
        Dim strQuery As String = "select ISNULL(auction_id,0) as auction_id from tbl_auction_invitation_filter_values where buyer_id=" & Request.QueryString.Get("i")
        Dim dt As DataTable = New DataTable()
        dt = SqlHelper.ExecuteDatatable(strQuery)

        If dt.Rows.Count > 0 Then

            For Each dataItem As GridDataItem In rad_grid_invite_auctions.MasterTableView.Items

                Dim CheckBox1 As CheckBox = TryCast(dataItem.FindControl("CheckBox1"), CheckBox)
                Dim auction_id As Integer = 0
                auction_id = dataItem.GetDataKeyValue("auction_id")
                For i As Integer = 0 To dt.Rows.Count - 1
                    If dt.Rows(i)("auction_id") = auction_id Then
                        dataItem.Selected = True
                        Exit For
                    End If
                Next
            Next
        End If

    End Sub

#End Region

End Class
