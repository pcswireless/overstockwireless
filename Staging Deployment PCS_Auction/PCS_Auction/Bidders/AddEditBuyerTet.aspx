﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="AddEditBuyerTet.aspx.vb" Inherits="Bidders_AddEditBuyerTet" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
        <script type="text/javascript">
            //On insert and update buttons click temporarily disables ajax to perform upload actions
            function conditionalPostback(e, sender) {
                var theRegexp = new RegExp("\.UpdateButton$|\.PerformInsertButton$", "ig");
                if (sender.get_eventTarget().match(theRegexp)) {
                    var upload = $find(window['FileUpload1']);
                    //AJAX is disabled only if file is selected for upload
                    if (upload.getFileInputs()[0].value != "") {
                        sender.set_enableAjax(false);
                    }
                }
            }
            function RequestStart(e, sender) {
                var upload1 = $find(window['File_product_details']);
                if (upload1.getFileInputs()[0].value != "") {
                    sender.set_enableAjax(false);
                }
                var upload2 = $find(window['File_tax_details']);
                if (upload2.getFileInputs()[0].value != "") {
                    sender.set_enableAjax(false);
                }
                var upload3 = $find(window['File_attach_payment_terms']);
                if (upload3.getFileInputs()[0].value != "") {
                    sender.set_enableAjax(false);
                }
                var upload4 = $find(window['File_attach_shipping_terms']);
                if (upload4.getFileInputs()[0].value != "") {
                    sender.set_enableAjax(false);
                }
                var upload5 = $find(window['File_attach_other_terms_and_conditions']);
                if (upload5.getFileInputs()[0].value != "") {
                    sender.set_enableAjax(false);
                }
            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Simple">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxPanel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" style="position:fixed;left:420px;top:180px;z-index:9999" runat="server" BackgroundPosition="Center">
      <img id="Image8" src="/images/img_loading.gif" />
  </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Bidders</div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="PageTab">
                    <span>
                        <%= IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), "New Bidder", "Edit Bidder")%></span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="PageTabContentEdit">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td class="tdTabItem">
                            <asp:Panel ID="Panel1_Header" runat="server" CssClass="collapsePanelHeader">
                                <div class="tabItemHeader" style="background: url(/Images/basic_info.gif) no-repeat;
                                    background-position: 10px 0px;">
                                    Basic Information
                                </div>
                                <asp:Image ID="Image1" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.jpg"
                                    CssClass="panelimage" />
                            </asp:Panel>
                            <ajax:CollapsiblePanelExtender ID="cpe1" BehaviorID="cpe1" runat="server" Enabled="True"
                                TargetControlID="Panel1_Content" CollapseControlID="Panel1_Header" ExpandControlID="Panel1_Header"
                                Collapsed="false" ImageControlID="Image1" CollapsedImage="/Images/down_Arrow.gif"
                                ExpandedImage="/Images/up_Arrow.gif">
                            </ajax:CollapsiblePanelExtender>
                            <asp:Panel ID="Panel1_Content" runat="server" CssClass="collapsePanel">
                                <telerik:RadAjaxPanel ID="pnlEditBuyer" runat="server" Width="100%">
                                    <div class="TabGrid">
                                        <div class="dv_md_heading">
                                            <div style="float: left;">
                                                &nbsp;Basic Information</div>
                                            <div style="float: right;">
                                                <img src="/Images/Q.mark.png" alt="help?" />&nbsp;
                                            </div>
                                        </div>
                                        <asp:Label ID="lblMessage" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                                        <table cellpadding="5" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td style="width: 15%;" class="caption">
                                                    Company Name
                                                </td>
                                                <td style="width: 35%;" class="details">
                                                    <asp:TextBox ID="txt_company" runat="server" CssClass="TextBox"></asp:TextBox>
                                                    <asp:Label ID="lbl_company" runat="server"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfv_company" runat="server" ControlToValidate="txt_company"
                                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="Name Required"
                                                        CssClass="error"></asp:RequiredFieldValidator>
                                                </td>
                                                <td style="width: 15%;" class="caption">
                                                    Contact Title
                                                </td>
                                                <td style="width: 35%;" class="details">
                                                    <asp:TextBox ID="txt_contact_title" runat="server" CssClass="TextBox"></asp:TextBox>
                                                    <asp:Label ID="lbl_contact_title" runat="server"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfv_contact_title" runat="server" ControlToValidate="txt_contact_title"
                                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="Title Required"
                                                        CssClass="error"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    First Name
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_first_name" runat="server" CssClass="TextBox"></asp:TextBox>
                                                    <asp:Label ID="lbl_first_name" runat="server"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfv_contact_name" runat="server" ControlToValidate="txt_first_name"
                                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="Name Required"
                                                        CssClass="error"></asp:RequiredFieldValidator>
                                                </td>
                                                <td class="caption">
                                                    Last Name
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_last_name" runat="server" CssClass="TextBox"></asp:TextBox>
                                                    <asp:Label ID="lbl_last_name" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Email
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_email" runat="server" CssClass="TextBox"></asp:TextBox>
                                                    <asp:Label ID="lbl_email" runat="server"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfv_email" runat="server" ControlToValidate="txt_email"
                                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="Email Required"
                                                        CssClass="error"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ValidationGroup="val_basic_info" ID="rev_email" runat="server"
                                                        ControlToValidate="txt_email" Display="Dynamic" ErrorMessage="Invalid Email"
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="error"></asp:RegularExpressionValidator>
                                                </td>
                                                <td class="caption">
                                                    <asp:Label ID="lbl_confirm_email_caption" runat="server" Text="Confirm Email"></asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_confirm_email" runat="server" CssClass="TextBox"></asp:TextBox>
                                                    <asp:Label ID="lbl_confirm_email" runat="server" Text="Confirm Email"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfv_confirm_email" runat="server" ControlToValidate="txt_confirm_email"
                                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="Email Required"
                                                        CssClass="error"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="compv_confirm_email" runat="server" ControlToValidate="txt_confirm_email"
                                                        ControlToCompare="txt_email" CssClass="error" Display="Dynamic" ValidationGroup="val_basic_info"
                                                        ErrorMessage="email mismatch"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Website
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_website" runat="server" CssClass="TextBox"></asp:TextBox>
                                                    <asp:Label ID="lbl_website" runat="server"></asp:Label>
                                                </td>
                                                <td class="caption">
                                                    Mobile
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_mobile" runat="server" CssClass="TextBox" MaxLength="15"></asp:TextBox>
                                                    <asp:Label ID="lbl_mobile" runat="server"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfv_mobile" runat="server" ControlToValidate="txt_mobile"
                                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="Mobile Required"
                                                        CssClass="error"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txt_mobile"
                                                        ValidationExpression="[0-9]{10,13}" EnableClientScript="true" ErrorMessage="Invalid"
                                                        runat="server" ValidationGroup="val_basic_info" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Phone
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_phone" runat="server" CssClass="TextBox" MaxLength="15"></asp:TextBox>
                                                    <asp:Label ID="lbl_phone" runat="server"></asp:Label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txt_phone"
                                                        ValidationExpression="[0-9]{10,13}" EnableClientScript="true" ErrorMessage="Invalid"
                                                        runat="server" ValidationGroup="val_basic_info" />
                                                </td>
                                                <td class="caption">
                                                    Fax
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_fax" runat="server" CssClass="TextBox" MaxLength="15"></asp:TextBox>
                                                    <asp:Label ID="lbl_fax" runat="server"></asp:Label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txt_fax"
                                                        ValidationExpression="[0-9]{10,13}" EnableClientScript="true" ErrorMessage="Invalid"
                                                        runat="server" ValidationGroup="val_basic_info" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Address1
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_address1" runat="server" CssClass="TextBox"></asp:TextBox>
                                                    <asp:Label ID="lbl_address1" runat="server"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfv_address1" runat="server" ControlToValidate="txt_address1"
                                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="Address Required"
                                                        CssClass="error"></asp:RequiredFieldValidator>
                                                </td>
                                                <td class="caption">
                                                    City
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_city" runat="server" CssClass="TextBox"></asp:TextBox>
                                                    <asp:Label ID="lbl_city" runat="server"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfv_city" runat="server" ControlToValidate="txt_city"
                                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="City Required"
                                                        CssClass="error"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Address2
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_address2" runat="server" CssClass="TextBox"></asp:TextBox>
                                                    <asp:Label ID="lbl_address2" runat="server"></asp:Label>
                                                </td>
                                                <td class="caption">
                                                    Country
                                                </td>
                                                <td class="details">
                                                    <asp:DropDownList ID="ddl_country" runat="server" DataTextField="name" DataValueField="country_id"
                                                        AutoPostBack="true" CausesValidation="false" Width="153">
                                                    </asp:DropDownList>
                                                    <asp:Label ID="lbl_country" runat="server"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfv_country" runat="server" ControlToValidate="ddl_country"
                                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="Country Required"
                                                        CssClass="error"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Zip
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_zip" runat="server" CssClass="TextBox" MaxLength="5"></asp:TextBox>
                                                    <asp:Label ID="lbl_zip" runat="server"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfv_zip" runat="server" ControlToValidate="txt_zip"
                                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="Zip Required"
                                                        CssClass="error"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txt_zip"
                                                        ValidationExpression="[0-9]{5}" EnableClientScript="true" ErrorMessage="Invalid"
                                                        runat="server" ValidationGroup="val_basic_info" />
                                                </td>
                                                <td class="caption">
                                                    State
                                                </td>
                                                <td class="details">
                                                    <asp:DropDownList ID="ddl_state" runat="server" DataTextField="name" DataValueField="state_id"
                                                        Width="153">
                                                    </asp:DropDownList>
                                                    <asp:Label ID="lbl_state" runat="server"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfv_state" runat="server" ControlToValidate="ddl_state"
                                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="State Required"
                                                        CssClass="error"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="dv_md_sub_heading">
                                                    Admin Details
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    User ID
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_user_id" runat="server" CssClass="TextBox"></asp:TextBox>
                                                    <asp:Label ID="lbl_user_id" runat="server"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfv_user_id" runat="server" ControlToValidate="txt_user_id"
                                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="User ID Required"
                                                        CssClass="error"></asp:RequiredFieldValidator>
                                                </td>
                                                <td class="caption">
                                                    Password
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_password" runat="server" CssClass="TextBox" TextMode="Password"></asp:TextBox>
                                                    <asp:Label ID="lbl_password" runat="server"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfv_password" runat="server" ControlToValidate="txt_password"
                                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="Password Required"
                                                        CssClass="error"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txt_password"
                                                        ValidationGroup="val_basic_info" ErrorMessage=" Password atleast 6 chars." ValidationExpression="^.{6,}$"
                                                        Display="Dynamic"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption" rowspan="2">
                                                    How did you find us?
                                                </td>
                                                <td rowspan="2" class="details">
                                                    <asp:CheckBoxList ID="chklst_how_find_us" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                                        Width="100%" CellPadding="0" CellSpacing="0" BorderWidth="0">
                                                    </asp:CheckBoxList>
                                                    <telerik:RadGrid ID="lst_how_find_us" GridLines="None" runat="server" AllowPaging="False"
                                                        AutoGenerateColumns="False" AllowMultiRowSelection="false" Skin="Simple" AllowFilteringByColumn="false"
                                                        ShowHeader="false" ShowFooter="false" Width="200">
                                                        <MasterTableView DataKeyNames="how_find_us_id" HorizontalAlign="NotSet" AutoGenerateColumns="False">
                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="how_find_us" HeaderText="how_find_us" UniqueName="how_find_us">
                                                                </telerik:GridBoundColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                    </telerik:RadGrid>
                                                </td>
                                                <td class="caption">
                                                    <asp:Label ID="lbl_retype_password_caption" runat="server" Text="Retype Password"></asp:Label>
                                                    &nbsp;&nbsp;&nbsp;
                                                </td>
                                                <td class="details">
                                                    <asp:TextBox ID="txt_retype_password" runat="server" CssClass="TextBox" TextMode="Password"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfv_retype_password" runat="server" ControlToValidate="txt_retype_password"
                                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="Retype Password Required"
                                                        CssClass="error"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="comp_retype_password" runat="server" ControlToValidate="txt_retype_password"
                                                        ControlToCompare="txt_password" CssClass="error" Display="Dynamic" ValidationGroup="val_basic_info"
                                                        ErrorMessage="password mismatch"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="caption">
                                                    Is Active?
                                                </td>
                                                <td class="details">
                                                    <asp:CheckBox ID="chk_is_active" runat="server" />
                                                    <asp:Label ID="lbl_is_active" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="dv_md_sub_heading">
                                                    What is your Business Type
                                                </td>
                                                <td colspan="2" class="dv_md_sub_heading">
                                                    What is your Industry Type
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="details">
                                                    <asp:CheckBoxList ID="chklst_business_types" runat="server" CellPadding="0" CellSpacing="0"
                                                        BorderWidth="0">
                                                    </asp:CheckBoxList>
                                                    <telerik:RadGrid ID="lst_business_types" GridLines="None" runat="server" AllowPaging="False"
                                                        AutoGenerateColumns="False" AllowMultiRowSelection="false" Skin="Simple" AllowFilteringByColumn="false"
                                                        ShowHeader="false" ShowFooter="false" Width="200">
                                                        <MasterTableView DataKeyNames="business_type_id" HorizontalAlign="NotSet" AutoGenerateColumns="False">
                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="business_type" HeaderText="business_type" UniqueName="business_type">
                                                                </telerik:GridBoundColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                    </telerik:RadGrid>
                                                </td>
                                                <td colspan="2" class="details">
                                                    <asp:CheckBoxList ID="chklst_industry_types" runat="server" CellPadding="0" CellSpacing="0"
                                                        BorderWidth="0">
                                                    </asp:CheckBoxList>
                                                    <telerik:RadGrid ID="lst_industry_types" GridLines="None" runat="server" AllowPaging="False"
                                                        AutoGenerateColumns="False" AllowMultiRowSelection="false" Skin="Simple" AllowFilteringByColumn="false"
                                                        ShowHeader="false" ShowFooter="false" Width="200">
                                                        <MasterTableView DataKeyNames="industry_type_id" HorizontalAlign="NotSet" AutoGenerateColumns="False">
                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="industry_type" HeaderText="industry_type" UniqueName="industry_type">
                                                                </telerik:GridBoundColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                    </telerik:RadGrid>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="dv_md_sub_heading">
                                                    Please select the companies you want to be linked
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="details">
                                                    <asp:CheckBoxList ID="chklst_companies_linked" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                                                        Width="50%" CellPadding="0" CellSpacing="0" BorderWidth="0">
                                                    </asp:CheckBoxList>
                                                    <telerik:RadGrid ID="lst_companies_linked" GridLines="None" runat="server" AllowPaging="False"
                                                        AutoGenerateColumns="False" AllowMultiRowSelection="false" Skin="Simple" AllowFilteringByColumn="false"
                                                        ShowHeader="false" ShowFooter="false" Width="200">
                                                        <MasterTableView DataKeyNames="seller_id" HorizontalAlign="NotSet" AutoGenerateColumns="False">
                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="company_name" HeaderText="company_name" UniqueName="company_name">
                                                                </telerik:GridBoundColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                    </telerik:RadGrid>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:CheckBox ID="chk_agree" runat="server" Text="I agree to terms and conditions" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="details">
                                                    <telerik:RadCaptcha ID="rad_captcha1" runat="server" Skin="black" EnableTheming="true"
                                                        ErrorMessage="Type the code from the image" CaptchaTextBoxLabel="" CaptchaLinkButtonText="Refresh"
                                                        IgnoreCase="true" EnableRefreshImage="true" EnableEmbeddedScripts="true" ValidationGroup="val_basic_info">
                                                    </telerik:RadCaptcha>
                                                </td>
                                                <td colspan="3">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" style="padding-bottom: 2px;">
                                                    <asp:ImageButton ID="imgbtn_basic_edit" ImageUrl="/Images/edit.gif" runat="server"
                                                        CausesValidation="false" />
                                                    <asp:ImageButton ID="imgbtn_basic_save" ValidationGroup="val_basic_info" runat="server"
                                                        AlternateText="Save" ImageUrl="/images/save.gif" />
                                                    <asp:ImageButton ID="imgbtn_basic_update" ValidationGroup="val_basic_info" runat="server"
                                                        AlternateText="Update" ImageUrl="/images/update.gif" />
                                                </td>
                                                <td align="right">
                                                    <asp:ImageButton ID="imgbtn_basic_cancel" CausesValidation="false" runat="server"
                                                        AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </telerik:RadAjaxPanel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdTabItem">
                            <asp:Panel ID="Panel2_Header" runat="server" CssClass="collapsePanelHeader">
                                <div class="tabItemHeader" style="background: url(/Images/current_stutas.gif) no-repeat;
                                    background-position: 10px 0px;">
                                    Official Information
                                </div>
                                <asp:Image ID="Image2" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.jpg"
                                    CssClass="panelimage" />
                            </asp:Panel>
                            <ajax:CollapsiblePanelExtender ID="cpe2" BehaviorID="cpe2" runat="server" Enabled="True"
                                ImageControlID="Image2" TargetControlID="Panel2_Content" CollapseControlID="Panel2_Header"
                                ExpandControlID="Panel2_Header" Collapsed="True" CollapsedImage="~/Images/down_Arrow.gif"
                                ExpandedImage="~/Images/up_Arrow.gif">
                            </ajax:CollapsiblePanelExtender>
                            <asp:Panel ID="Panel2_Content" runat="server" CssClass="collapsePanel">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                    CssClass="TabGrid">
                                    <div class="dv_md_heading">
                                        <div style="float: left;">
                                            &nbsp;Official Information</div>
                                        <div style="float: right;">
                                            <img src="/Images/Q.mark.png" alt="help?" />&nbsp;
                                        </div>
                                    </div>
                                    <table cellpadding="5" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td style="width: 15%;" class="caption">
                                                Max Amount of Bid ($) :
                                            </td>
                                            <td style="width: 35%;" class="details">
                                                <asp:TextBox ID="txt_max_amount_bid" runat="server" CssClass="TextBox" MaxLength="10"></asp:TextBox><asp:RegularExpressionValidator
                                                    ID="rev_max_amount_bid" runat="server" Display="Dynamic" ControlToValidate="txt_max_amount_bid"
                                                    ValidationGroup="val_official_info" CssClass="error" ErrorMessage=" Invalid amount"
                                                    ValidationExpression="^\d*\.?\d*$"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="req_max_amount_bid" runat="server" ControlToValidate="txt_max_amount_bid"
                                                    ValidationGroup="val_official_info" Display="Dynamic" ErrorMessage=" Max. Bidding Amount Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                                <asp:Label ID="lbl_max_amount_bid" runat="server"></asp:Label>
                                            </td>
                                            <td style="width: 15%;" class="caption">
                                                Approval Status
                                            </td>
                                            <td style="width: 35%;" class="details">
                                                <asp:Label ID="lbl_approval_status" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                # of Employees
                                            </td>
                                            <td class="details">
                                                <asp:Label ID="lbl_no_of_employees" runat="server"></asp:Label>
                                                <asp:TextBox ID="txt_no_of_employees" runat="server" CssClass="TextBox" MaxLength="6"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="req_no_of_employees" runat="server" ControlToValidate="txt_no_of_employees"
                                                    ValidationGroup="val_official_info" Display="Dynamic" ErrorMessage=" No. of Employees Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="rev_no_of_employees" runat="server" Display="Dynamic"
                                                    ControlToValidate="txt_no_of_employees" ValidationGroup="val_official_info" CssClass="error"
                                                    ErrorMessage="Invalid number" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td class="caption">
                                                Annual Turnover ($)
                                            </td>
                                            <td class="details">
                                                <asp:Label ID="lbl_annual_turnover" runat="server"></asp:Label>
                                                <asp:TextBox ID="txt_annual_turnover" runat="server" CssClass="TextBox" MaxLength="15"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="req_annual_turnover" runat="server" ControlToValidate="txt_annual_turnover"
                                                    ValidationGroup="val_official_info" Display="Dynamic" ErrorMessage="Turn Over Amount Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="rev_annual_turnover" runat="server" Display="Dynamic"
                                                    ControlToValidate="txt_annual_turnover" ValidationGroup="val_official_info" CssClass="error"
                                                    ErrorMessage="Invalid amount" ValidationExpression="^\d*\.?\d*$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="dv_md_sub_heading">
                                                Bank Information
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                Routing #
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_routing_no" runat="server" CssClass="TextBox" Width="200"></asp:TextBox>
                                                <asp:Label ID="lbl_routing_no" runat="server"></asp:Label>
                                                <asp:RequiredFieldValidator ID="req_routing_no" runat="server" ControlToValidate="txt_routing_no"
                                                    ValidationGroup="val_official_info" Display="Dynamic" ErrorMessage=" Routing Number Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                            </td>
                                            <td class="caption">
                                                Account #
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_account_no" runat="server" CssClass="TextBox" Width="200"></asp:TextBox>
                                                <asp:Label ID="lbl_account_no" runat="server"></asp:Label>
                                                <asp:RequiredFieldValidator ID="req_account_no" runat="server" ControlToValidate="txt_account_no"
                                                    ValidationGroup="val_official_info" Display="Dynamic" ErrorMessage=" Account # Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                Name of Bank
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_name_bank" runat="server" CssClass="TextBox" Width="200"></asp:TextBox>
                                                <asp:Label ID="lbl_name_bank" runat="server"></asp:Label>
                                                <asp:RequiredFieldValidator ID="req_name_bank" runat="server" ControlToValidate="txt_name_bank"
                                                    ValidationGroup="val_official_info" Display="Dynamic" ErrorMessage=" Bank Name Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                            </td>
                                            <td class="caption">
                                                Branch Name
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_branch_name" runat="server" CssClass="TextBox" Width="200"></asp:TextBox>
                                                <asp:Label ID="lbl_branch_name" runat="server"></asp:Label>
                                                <asp:RequiredFieldValidator ID="req_branch_name" runat="server" ControlToValidate="txt_branch_name"
                                                    ValidationGroup="val_official_info" Display="Dynamic" ErrorMessage=" Branch Name Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                Bank Contact Details
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_bank_contact_detail" runat="server" CssClass="TextBox" Width="200"></asp:TextBox>
                                                <asp:Label ID="lbl_bank_contact_detail" runat="server"></asp:Label>
                                            </td>
                                            <td class="caption">
                                                Bank Address
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_bank_address" runat="server" CssClass="TextBox" Width="200"></asp:TextBox>
                                                <asp:Label ID="lbl_bank_address" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                Tax ID/SSN #
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_tax_id_ssn_no" runat="server" CssClass="TextBox" Width="200"></asp:TextBox>
                                                <asp:Label ID="lbl_tax_id_ssn_no" runat="server"></asp:Label>
                                                <asp:RequiredFieldValidator ID="req_tax_id_ssn_no" runat="server" ControlToValidate="txt_tax_id_ssn_no"
                                                    ValidationGroup="val_official_info" Display="Dynamic" ErrorMessage="Tax ID/SSN # Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                            </td>
                                            <td class="caption">
                                            </td>
                                            <td class="details">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="imgbtn_official_edit" ImageUrl="/Images/edit.gif" runat="server"
                                                    CausesValidation="false" />
                                                <asp:ImageButton ID="imgbtn_official_update" ValidationGroup="val_official_info"
                                                    runat="server" AlternateText="Update" ImageUrl="/images/update.gif" />
                                            </td>
                                            <td colspan="2">
                                                <asp:Button ID="btn_approve" runat="server" Text="Approve" CssClass="Button_new" />
                                                <asp:Button ID="btn_reject" runat="server" Text="Reject" CssClass="Button_new" />
                                                <asp:Button ID="btn_on_hold" runat="server" Text="On Hold" CssClass="Button_new" />
                                                <asp:Button ID="btn_more_details" runat="server" Text="More Details" CssClass="Button_new" />
                                                <asp:Button ID="btn_panding" runat="server" Text="Pending" CssClass="Button_new" />
                                                <asp:Button ID="btn_activate_deactivate" runat="server" Text="Deactivate" CssClass="Button_new" />
                                            </td>
                                            <td align="right">
                                                <asp:ImageButton ID="imgbtn_official_cancel" CausesValidation="false" runat="server"
                                                    AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </telerik:RadAjaxPanel>
                                <telerik:RadAjaxPanel ID="RadAjaxPanel3" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                    ClientEvents-OnRequestStart="conditionalPostback" CssClass="TabGrid">
                                    <div class="dv_md_sub_heading">
                                        Attachment
                                    </div>
                                    <br />
                                    <telerik:RadGrid ID="RadGrid_Attachment" runat="server" Skin="Simple" GridLines="None"
                                        AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                        OnNeedDataSource="RadGrid_Attachment_NeedDataSource" OnDeleteCommand="RadGrid_Attachment_DeleteCommand"
                                        OnInsertCommand="RadGrid_Attachment_InsertCommand" OnUpdateCommand="RadGrid_Attachment_UpdateCommand"
                                        enableajax="true">
                                        <PagerStyle Mode="NumericPages"></PagerStyle>
                                        <MasterTableView Width="100%" CommandItemDisplay="TopAndBottom" DataKeyNames="buyer_attachment_id"
                                            HorizontalAlign="NotSet" AutoGenerateColumns="False" SkinID="Simple" EditMode="InPlace">
                                            <CommandItemStyle BackColor="#E1DDDD" />
                                                     <NoRecordsTemplate>
                                                    Attachments not available
                                                    </NoRecordsTemplate>
                                                    <CommandItemSettings AddNewRecordText="Add New Attachment" />
                                            <Columns>
                                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                                </telerik:GridEditCommandColumn>
                                                <telerik:GridBoundColumn DataField="buyer_attachment_id" HeaderText="attachment_id"
                                                    UniqueName="buyer_attachment_id" ReadOnly="True" Visible="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="title" HeaderText="Title" UniqueName="Title"
                                                    ColumnEditorID="GridTextBoxColumnEditor7">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="upload_date" HeaderText="Upload Date" UniqueName="upload_date"
                                                    ReadOnly="true">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="upload_by_user" HeaderText="Upload By" UniqueName="upload_by_user"
                                                    ReadOnly="true">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridTemplateColumn HeaderText="File Name" SortExpression="filename" UniqueName="filename"
                                                    EditFormColumnIndex="1">
                                                    <ItemTemplate>
                                                        <a href="/Upload/Bidders/official_attachments/<%= Request.QueryString("i") %>/<%#Eval("buyer_attachment_id") %>/<%#Eval("filename") %>"
                                                            target="_blank">
                                                            <%#Eval("filename") %></a>
                                                        <%--<asp:Label runat="server" ID="lblFile" Text='<%# Eval("filename") %>'></asp:Label>--%>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="TextBox" />
                                                    </EditItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"
                                                    ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete"
                                                    UniqueName="DeleteColumn">
                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                                </telerik:GridButtonColumn>
                                            </Columns>
                                            <EditFormSettings ColumnNumber="2" CaptionDataField="title" CaptionFormatString="Edit properties of Attachment {0}"
                                                InsertCaption="New Attachment">
                                                <FormTableItemStyle Wrap="False"></FormTableItemStyle>
                                                <FormCaptionStyle CssClass="EditFormHeader"></FormCaptionStyle>
                                                <FormMainTableStyle GridLines="None" CellSpacing="0" CellPadding="3" BackColor="White"
                                                    Width="100%" />
                                                <FormTableStyle CellSpacing="0" CellPadding="2" Height="110px" BackColor="White" />
                                                <FormTableAlternatingItemStyle Wrap="False"></FormTableAlternatingItemStyle>
                                                <EditColumn ButtonType="ImageButton" InsertText="Insert Order" UpdateText="Update record"
                                                    UniqueName="EditCommandColumn1" CancelText="Cancel edit">
                                                </EditColumn>
                                                <FormTableButtonRowStyle HorizontalAlign="Right" CssClass="EditFormButtonRow"></FormTableButtonRowStyle>
                                            </EditFormSettings>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditor7" runat="server" TextBoxStyle-Width="150px" />
                                </telerik:RadAjaxPanel>
                                <br />
                                <telerik:RadAjaxPanel ID="RadAjaxPanel5" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                    CssClass="TabGrid">
                                    <table cellpadding="5" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td class="dv_md_sub_heading" colspan="2">
                                                Comments
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="details">
                                                <asp:TextBox ID="txt_comment" runat="server" TextMode="MultiLine" Width="600" Height="50"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfv_comment" runat="server" ControlToValidate="txt_comment"
                                                    ValidationGroup="val_official_info_comment" Display="Dynamic" ErrorMessage=" Comment Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                            </td>
                                            <td valign="bottom" align="left">
                                                <asp:Button ID="btn_submit_comment" runat="server" Text="Submit" ValidationGroup="val_official_info_comment" CssClass="Button_new" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <telerik:RadGrid ID="rad_grid_comment" GridLines="None" runat="server" PageSize="10"
                                                    AllowPaging="True" AutoGenerateColumns="False" DataSourceID="sqlDataSource_comment"
                                                    AllowMultiRowSelection="false" Skin="Simple" AllowFilteringByColumn="True" AllowSorting="true">
                                                    <PagerStyle Mode="NumericPages" />
                                                    <MasterTableView DataKeyNames="buyer_comment_id" DataSourceID="sqlDataSource_comment"
                                                        HorizontalAlign="NotSet" AutoGenerateColumns="False" AllowFilteringByColumn="True">
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="submit_date" HeaderText="Date" SortExpression="submit_date"
                                                                UniqueName="submit_date" FilterControlWidth="50">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="comment" HeaderText="comment" SortExpression="comment"
                                                                UniqueName="comment" FilterControlWidth="50">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="submit_by_user" HeaderText="User" SortExpression="submit_by_user"
                                                                UniqueName="submit_by_user" FilterControlWidth="50">
                                                            </telerik:GridBoundColumn>
                                                        </Columns>
                                                        <NoRecordsTemplate>
                                                            No record found.
                                                        </NoRecordsTemplate>
                                                    </MasterTableView>
                                                </telerik:RadGrid>
                                                <asp:SqlDataSource ID="sqlDataSource_comment" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                                    ProviderName="System.Data.SqlClient" SelectCommand="select A.buyer_comment_id,ISNULL(A.comment,'') As comment,A.submit_date,(ISNULL(U.first_name,'') + ' ' + ISNULL(U.last_name,'')) AS submit_by_user from tbl_reg_buyer_comments A INNER JOIN tbl_sec_users U on A.submit_by_user_id=U.user_id where A.buyer_id=@buyer_id">
                                                    <SelectParameters>
                                                        <asp:Parameter Name="buyer_id" Type="Int32" DefaultValue="0" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                    </table>
                                </telerik:RadAjaxPanel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdTabItem">
                            <asp:Panel ID="Panel3_Header" runat="server" CssClass="collapsePanelHeader">
                                <div class="tabItemHeader" style="background: url(/Images/attachment.gif) no-repeat;
                                    background-position: 10px 0px;">
                                    Sub-Logins
                                </div>
                                <asp:Image ID="Image3" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.jpg"
                                    CssClass="panelimage" />
                            </asp:Panel>
                            <ajax:CollapsiblePanelExtender ID="cpe3" BehaviorID="cpe3" runat="server" Enabled="True"
                                ImageControlID="Image3" TargetControlID="Panel3_Content" CollapseControlID="Panel3_Header"
                                ExpandControlID="Panel3_Header" Collapsed="True" CollapsedImage="~/Images/down_Arrow.gif"
                                ExpandedImage="~/Images/up_Arrow.gif">
                            </ajax:CollapsiblePanelExtender>
                            <asp:Panel ID="Panel3_Content" runat="server" CssClass="collapsePanel">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel4" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                    CssClass="TabGrid">
                                    <div class="dv_md_heading">
                                        <div style="float: left;">
                                            &nbsp;Sub-Logins</div>
                                        <div style="float: right;">
                                            <img src="/Images/Q.mark.png" alt="help?" />&nbsp;
                                        </div>
                                    </div>
                                    <telerik:RadGrid ID="rad_grid_sublogins" runat="server" Skin="Simple" GridLines="None"
                                        AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                        OnNeedDataSource="rad_grid_sublogins_NeedDataSource" OnDeleteCommand="rad_grid_sublogins_DeleteCommand"
                                        OnInsertCommand="rad_grid_sublogins_InsertCommand" OnUpdateCommand="rad_grid_sublogins_UpdateCommand"
                                        enableajax="true">
                                        <PagerStyle Mode="NumericPages"></PagerStyle>
                                        <HeaderStyle BackColor="#BEBEBE" />
                                        <MasterTableView Width="100%" CommandItemDisplay="TopAndBottom" DataKeyNames="buyer_user_id"
                                            HorizontalAlign="NotSet" AutoGenerateColumns="False" SkinID="Simple" EditMode="InPlace"
                                            TableLayout="Fixed">
                                            <CommandItemStyle BackColor="#E1DDDD" />
                                                     <NoRecordsTemplate>
                                                    No sub login to display
                                                    </NoRecordsTemplate>
                                                    <CommandItemSettings AddNewRecordText="Add New Sub Login" />
                                            <Columns>
                                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                                </telerik:GridEditCommandColumn>
                                                <telerik:GridBoundColumn DataField="buyer_user_id" HeaderText="buyer_user_id" UniqueName="buyer_user_id"
                                                    ReadOnly="True" Visible="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="buyer_id" HeaderText="buyer_id" UniqueName="buyer_id"
                                                    ReadOnly="True" Visible="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="first_name" HeaderText="First Name" UniqueName="first_name"
                                                    ColumnEditorID="GridTextBoxColumnEditor1">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="last_name" HeaderText="Last Name" UniqueName="last_name"
                                                    ColumnEditorID="GridTextBoxColumnEditor1">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="title" HeaderText="Title" UniqueName="title"
                                                    ColumnEditorID="GridTextBoxColumnEditor1">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="username" HeaderText="Username" UniqueName="username"
                                                    ColumnEditorID="GridTextBoxColumnEditor1">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="password" HeaderText="Password" UniqueName="password"
                                                    ColumnEditorID="GridTextBoxColumnEditor1">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="bidding_limit" HeaderText="Bidding Limit" UniqueName="bidding_limit"
                                                    ColumnEditorID="GridTextBoxColumnEditor1">
                                                </telerik:GridBoundColumn>
                                                <%--  <telerik:GridTemplateColumn UniqueName="bidding_limit" SortExpression="bidding_limit" HeaderText="Bidding Limit">
                                    <ItemTemplate>
                                        <%# Eval("bidding_limit")%>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                            <asp:TextBox ID="txt_bidding_limit" runat="server" Text='<%# Eval("bidding_limit") %>' Width="70"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_bidding_limit" runat="server" ErrorMessage="*" ForeColor="red" ControlToValidate="txt_bidding_limit" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>--%>
                                                <telerik:GridTemplateColumn HeaderText="Is Active" SortExpression="is_active" UniqueName="is_active"
                                                    EditFormColumnIndex="1">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lbl_is_active" Text='<%# IIF(Eval("is_active"),"Yes","No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <%-- <asp:Label runat="server" ID="lbl_is_active1" Visible="false"></asp:Label>--%>
                                                        <asp:RadioButtonList ID="rdo_is_active" runat="server" RepeatColumns="2" SelectedValue='<%# IIF(checked(Eval("is_active")),"y","n") %>'
                                                            RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="y">Yes</asp:ListItem>
                                                            <asp:ListItem Value="n">No</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </EditItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"
                                                    ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete"
                                                    UniqueName="DeleteColumn">
                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                                </telerik:GridButtonColumn>
                                            </Columns>
                                            <EditFormSettings ColumnNumber="2" CaptionDataField="title" CaptionFormatString="Edit properties of sub login {0}"
                                                InsertCaption="New sub login">
                                                <FormTableItemStyle Wrap="False"></FormTableItemStyle>
                                                <FormCaptionStyle CssClass="EditFormHeader"></FormCaptionStyle>
                                                <FormMainTableStyle GridLines="None" CellSpacing="0" CellPadding="3" BackColor="White"
                                                    Width="100%" />
                                                <FormTableStyle CellSpacing="0" CellPadding="2" Height="110px" BackColor="White" />
                                                <FormTableAlternatingItemStyle Wrap="False"></FormTableAlternatingItemStyle>
                                                <EditColumn ButtonType="ImageButton" InsertText="Insert Order" UpdateText="Update record"
                                                    UniqueName="EditCommandColumn1" CancelText="Cancel edit">
                                                </EditColumn>
                                                <FormTableButtonRowStyle HorizontalAlign="Right" CssClass="EditFormButtonRow"></FormTableButtonRowStyle>
                                            </EditFormSettings>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditor1" runat="server" TextBoxStyle-Width="100px" />
                                </telerik:RadAjaxPanel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdTabItem">
                            <asp:Panel ID="Panel4_Header" runat="server" CssClass="collapsePanelHeader">
                                <div class="tabItemHeader" style="background: url(/Images/invetions.gif) no-repeat;
                                    background-position: 10px 0px;">
                                    Invite For Auctions
                                </div>
                                <asp:Image ID="Image4" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.jpg"
                                    CssClass="panelimage" />
                            </asp:Panel>
                            <ajax:CollapsiblePanelExtender ID="cpe4" BehaviorID="cpe4" runat="server" Enabled="True"
                                ImageControlID="Image4" TargetControlID="Panel4_Content" CollapseControlID="Panel4_Header"
                                ExpandControlID="Panel4_Header" Collapsed="True" CollapsedImage="/Images/down_Arrow.gif"
                                ExpandedImage="/Images/up_Arrow.gif">
                            </ajax:CollapsiblePanelExtender>
                            <asp:Panel ID="Panel4_Content" runat="server" CssClass="collapsePanel">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                    CssClass="TabGrid">
                                    <div class="dv_md_heading">
                                        <div style="float: left;">
                                            &nbsp;Select Auctions for Invitation</div>
                                        <div style="float: right;">
                                            <img src="/Images/Q.mark.png" alt="help?" />&nbsp;
                                        </div>
                                    </div>
                                    <telerik:RadGrid ID="rad_grid_invite_auctions" runat="server" Skin="Simple" GridLines="None"
                                        AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                        OnNeedDataSource="rad_grid_invite_auctions_NeedDataSource" enableajax="true">
                                        <PagerStyle Mode="NumericPages"></PagerStyle>
                                        <MasterTableView Width="100%" DataKeyNames="auction_id" HorizontalAlign="NotSet"
                                            AutoGenerateColumns="False" SkinID="Simple">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="auction_id" HeaderText="auction_id" UniqueName="auction_id"
                                                    ReadOnly="True" Visible="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="code" HeaderText="code" UniqueName="code">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="title" HeaderText="Title" UniqueName="Title">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="stock_location" HeaderText="Stock Location" UniqueName="stock_location">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="start_date" HeaderText="Start Date" UniqueName="start_date">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="end_date" HeaderText="End Date" UniqueName="end_date">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="time_zone" HeaderText="Time Zone" UniqueName="time_zone">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="status" HeaderText="Status" UniqueName="status">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridTemplateColumn UniqueName="CheckBoxTemplateColumn">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="ToggleRowSelection"
                                                            AutoPostBack="True" Checked='<%# Convert.ToBoolean(Eval("is_invited")) %>' />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="headerChkbox" runat="server" OnCheckedChanged="ToggleSelectedState"
                                                            AutoPostBack="True" />&nbsp;Select All
                                                    </HeaderTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <div style="padding: 10px 0px 10px 0px; text-align: right;">
                                        <asp:Button ID="btn_invite_all" runat="server" Text="Invite Selected" CssClass="Button_new" />
                                    </div>
                                </telerik:RadAjaxPanel>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
