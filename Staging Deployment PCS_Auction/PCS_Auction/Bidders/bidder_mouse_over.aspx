﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="bidder_mouse_over.aspx.vb" Inherits="Bidders_bidder_mouse_over" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PCS Bidding</title>
       <link type="text/css" rel="Stylesheet" href="/Style/master_menu.css" />
     <script type="text/javascript" src="/pcs_js/jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //-------------------------------------------------------
        /*shows the loading div every time we have an Ajax call*/
        $("#loading").bind("ajaxSend", function () {
            $(this).show();
        }).bind("ajaxComplete", function () {
            $(this).hide();
        });
        //-------------------------------------------------------
    })
    function loadImage() {
        //change the uri inside the load() brackets to load a page relative to your domain
        $("#loadedContent").load('/tags/index/50');
    }
</script>
<style type="text/css">
/*AJAX LOADER
-------------------*/
#loading{
    position: fixed;
    top: 10px;
    left: 65px; /*set it to "right: 0;" to have the bar displaying on the top right corner*/
    z-index: 5000;
    
    font-size: 120%;
    color: white;
   
    border:none;
}


</style>
 
</head>
<body class="details" style="margin:10px 6px 10px;padding:0;border:none;" onload="loadImage();">
<div id="Div1" style="display: none;"><img src="/images/img_loading.gif" /></div>
    <form id="form2" runat="server">
    <div id="loadedContent" style="border:none;">
    <div style="overflow: hidden;width:250px;font-size: 11px;color: #105386;">


                            <asp:Label runat="server" ID="lbl_bidder"></asp:Label>
                                    

                </div>                                </div>

    </form>
</body>
</html>
