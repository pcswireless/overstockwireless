﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BidderAuctions.ascx.vb" Inherits="Bidders_UserControls_BidderAuctions" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
    <script type="text/javascript">
        function Cancel_Ajax(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                 args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                args.set_enableAjax(false);
            }
            else {
                args.set_enableAjax(true);
            }
        }

    </script>
</telerik:RadScriptBlock>
<%--<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Vista">
    <ClientEvents OnRequestStart="Cancel_Ajax" />
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGrid_Auction_List">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid_Auction_List" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid_Auction_List" />
            </UpdatedControls>
        </telerik:AjaxSetting>


    </AjaxSettings>
</telerik:RadAjaxManager>--%>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" />
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed; left: 420px; top: 180px; z-index: 9999"
    runat="server" BackgroundPosition="Center">
    <img id="Image8" src="/images/img_loading.gif" />
</telerik:RadAjaxLoadingPanel>
<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <div class="dv_md_sub_heading">
                <strong><asp:label id="lblHeading" runat="server"></asp:label></strong>
            </div>
          
        </td>
    </tr>


    <tr>
        <td style="padding-left:16px;">
            <%--<asp:TextBox ID="txt_test" runat="server"></asp:TextBox>--%>
            <telerik:RadGrid runat="server" ID="RadGrid_Auction_List" AllowFilteringByColumn="True"
                AllowSorting="True" PageSize="10" ShowFooter="False" ShowGroupPanel="False" AllowPaging="True"
                AutoGenerateColumns="false" Skin="Vista" EnableLinqExpressions="false" Width="98%">
                <PagerStyle Mode="NextPrevAndNumeric" />
                <MasterTableView DataKeyNames="auction_id" AutoGenerateColumns="false"
                    ItemStyle-Height="40" AlternatingItemStyle-Height="40" CommandItemDisplay="None">
                    <CommandItemStyle BackColor="#d6d6d6" />
                    <CommandItemSettings ShowExportToWordButton="false" ShowExportToExcelButton="false" ShowExportToPdfButton="false"
                        ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                    <NoRecordsTemplate>
                        No Auction to display
                    </NoRecordsTemplate>
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="title" SortOrder="Ascending" />
                    </SortExpressions>
                    <Columns>
                        <telerik:GridBoundColumn DataField="code" HeaderText="Auction Code" SortExpression="code"
                            UniqueName="auction_code" AutoPostBackOnFilter="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn DataField="title" HeaderText="Title" SortExpression="title"
                            UniqueName="title" GroupByExpression="title [Title] group by title">
                            <ItemTemplate>
                                <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');" onclick="redirectIframe('/Backend_TopBar.aspx?t=<%# CommonCode.getAuctionTabPosition(Eval("auction_id")) %>&a=<%# Eval("auction_id") %>','/Backend_Leftbar.aspx?t=3&a=<%# Eval("auction_id") %>','/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %>')">
                                    <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("title") %>'></asp:Label></a>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn UniqueName="product_category" DataField="product_category"
                            SortExpression="product_category" HeaderText="Product Category" AllowFiltering="false" GroupByExpression="product_category [Product Category] group by product_category">
                            <FilterTemplate>
                                <telerik:RadComboBox ID="RadComboBoxCategory" DataSourceID="SqlDataSource2" DataTextField="product_category"
                                    DataValueField="product_category" Height="200px" AppendDataBoundItems="true"
                                    SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("product_category").CurrentFilterValue %>'
                                    runat="server" OnClientSelectedIndexChanged="CategoryIndexChanged">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="All" />
                                    </Items>
                                </telerik:RadComboBox>
                                <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                    <script type="text/javascript">
                                        function CategoryIndexChanged(sender, args) {
                                            var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                tableView.filter("product_category", args.get_item().get_value(), "EqualTo");

                                            }
                                    </script>
                                </telerik:RadScriptBlock>
                            </FilterTemplate>
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn DataField="start_date" SortExpression="start_date" UniqueName="start_date" AllowFiltering="false" HeaderText="Start Date" Groupable="false" GroupByExpression="start_date [Start Date] group by start_date" ItemStyle-Width="100">
                            <ItemTemplate>
                                <%# Convert.ToDateTime(Eval("start_date")).ToString("MM-dd-yyyy hh:mm tt")%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="display_end_time" SortExpression="display_end_time" UniqueName="display_end_time" AllowFiltering="false" HeaderText="End Date" Groupable="false" GroupByExpression="display_end_time [End Date] group by display_end_time" ItemStyle-Width="100">
                            <ItemTemplate>
                                <%# Convert.ToDateTime(Eval("display_end_time")).ToString("MM-dd-yyyy hh:mm tt")%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="status" HeaderText="Status" SortExpression="status" UniqueName="status" HeaderStyle-Width="120px"
                            DataType="System.String" ItemStyle-Width="120px" AllowFiltering="false">
                       </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn DataField="rank" UniqueName="rank" AllowFiltering="false" HeaderText="Rank" Groupable="false" ItemStyle-Width="100">
                            <ItemTemplate>
                                <% If AuctionMode = "IL" Then%>
                                    <%# IIf(Eval("rank")>0,Eval("rank"),IIf(Eval("auction_type_id")=3,"Not Participated","-"))%>
                                <% End If%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                         <telerik:GridTemplateColumn DataField="max_bid_amt" UniqueName="max_bid_amt" AllowFiltering="false" HeaderText="Highest Bid" Groupable="false" ItemStyle-Width="100">
                            <ItemTemplate>
                                <% If AuctionMode = "L" Then%>
                                  <%# CommonCode.GetFormatedMoney(Eval("bidder_max_bid_amt")) & "/ " & CommonCode.GetFormatedMoney(Eval("max_bid_amt"))%>
                                <% ElseIf AuctionMode = "W" Then%>
                                 <%# IIf(Eval("max_bid_amt") > 0, CommonCode.GetFormatedMoney(Eval("max_bid_amt")), "")%>
                                <% Else%>
                                -
                                <% End if %>

                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>

            </telerik:RadGrid>
            <asp:HiddenField ID="hid_user_id" runat="server" Value="0" />

            <asp:SqlDataSource ID="SqlDataSource2" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                ProviderName="System.Data.SqlClient" SelectCommand="select DISTINCT [name] as product_category,product_catetory_id from [tbl_master_product_categories]"
                runat="server"></asp:SqlDataSource>
        </td>
    </tr>
</table>
