﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="AddEditBuyer.aspx.vb" Inherits="Bidders_AddEditBuyer" MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/Bidders/UserControls/Bidder_Comments.ascx" TagName="AuctionComments"
    TagPrefix="uc3" %>
<%@ Register Src="~/Bidders/UserControls/BidderAuctions.ascx" TagName="AuctionInvited"
    TagPrefix="uc4" %>
<%@ Register Src="/Log/UserControls/SystemLogLink.ascx" TagName="SystemLogLink" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <script type="text/javascript">
        function open_pass_win(_path, _pwd) {
            w1 = window.open(_path + '?' + _pwd, '_paswd', 'top=' + ((screen.height - 350) / 2) + ', left=' + ((screen.width - 500) / 2) + ', height=350, width=500,scrollbars=yes,toolbars=no,resizable=1;');
            w1.focus();
            return false;

        }
        function conditionalPostback(e, sender) {
            sender.set_enableAjax(false);

        }
       
    </script>
    <telerik:RadAjaxManager ID="RadAjaxManager_Main" runat="server" SkinID="Vista">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_btnsend">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel5" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="imgbtn_basic_edit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid_Attachment" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="imgbtn_basic_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Rad_Ajax_panel_status" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel5" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxPanel5">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Rad_Ajax_panel_status" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="imgbtn_basic_save">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel5" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="imgbtn_basic_cancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="imgbtn_official_edit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel2" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel22" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="imgbtn_official_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel2" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel22" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Rad_Ajax_panel_status" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <%--<telerik:AjaxUpdatedControl ControlID="RadAjaxPanel2_attachment" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="imgbtn_official_cancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel2" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel22" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <%-- <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel2_attachment" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btn_approve">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel22" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btn_reject">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel22" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btn_on_hold">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel22" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btn_more_details">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel22" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btn_panding">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel22" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btn_activate_deactivate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel22" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Rad_Ajax_panel_status" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax1(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                     args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
                else {
                    args.set_enableAjax(true);
                }
            }

        </script>
    </telerik:RadScriptBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="top">
                <div style="float: left;">
                    <div class="pageheading">
                        <asp:Label ID="lblPageHeading" runat="server" Text="Bidder"></asp:Label>
                    </div>
                </div>
                <div style="float: right; margin: 10px; padding-right: 20px; width: 380px;">
                    <div style="float: left; width: 170px;">
                        <asp:Panel ID="pnl_bidder_status" runat="server">
                            <div style="float: left; padding-top: 6px;">
                                Status
                            </div>
                            <div style="float: left; padding-left: 10px; padding-top: 3px;">
                               <%-- <telerik:RadAjaxPanel ID="Rad_Ajax_panel_status" runat="server" EnableAJAX="false" LoadingPanelID="RadAjaxLoadingPanel1">--%>
                                    <asp:DropDownList ID="ddl_status" runat="server" CssClass="dropdown" AutoPostBack="true"
                                        DataTextField="status" DataValueField="status_id">
                                    </asp:DropDownList>
                               <%-- </telerik:RadAjaxPanel>--%>
                            </div>
                        </asp:Panel>
                    </div>
                    <div style="float: right; padding-right: 20px;">
                        <div style="float: left; padding-left: 20px; padding-top: 3px;">
                            <uc4:SystemLogLink ID="UC_System_log_link" runat="server" />
                        </div>
                        <div style="float: right; padding-left: 10px; padding-top: 3px;">
                            <a href="javascript:void(0);" onclick="javascript:open_help('1');">
                                <img src="/Images/help.gif" border="none" />
                            </a>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td align="left">
                <%--AutoPostBack="true" OnTabClick="RadTabStrip1_TabClick"--%>
                <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="" MultiPageID="RadMultiPage1"
                    SelectedIndex="0" Align="Left">
                    <Tabs>
                        <telerik:RadTab CssClass="auctiontabSelected">
                            <TabTemplate>
                                <span>
                                    <%= IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), "Add Bidder Details", "Edit Bidder")%></span>
                            </TabTemplate>
                        </telerik:RadTab>
                        <telerik:RadTab CssClass="auctiontab">
                            <TabTemplate>
                                <span>Invite For Auctions</span>
                            </TabTemplate>
                        </telerik:RadTab>
                        <telerik:RadTab CssClass="auctiontab">
                            <TabTemplate>
                                <span>Comments</span>
                            </TabTemplate>
                        </telerik:RadTab>
                        <telerik:RadTab CssClass="auctiontab">
                            <TabTemplate>
                                <span>Invited Auctions</span>
                            </TabTemplate>
                        </telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
            </td>
        </tr>
        <tr>
            <td class="PageTabContentEdit">
                <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" CssClass="multiPage">
                    <telerik:RadPageView ID="RadPageView1" runat="server">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td class="tdTabItem">
                                    <a name="1"></a>
                                    <asp:Panel ID="Panel1_Header1" runat="server" CssClass="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/basic_info.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            Basic Information
                                        </div>
                                        <asp:Image ID="pnl_img1" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.jpg"
                                            CssClass="panelimage" />
                                        
                                    </asp:Panel>
                                    <ajax:CollapsiblePanelExtender ID="cpe1" BehaviorID="cpe1" runat="server" Enabled="True"
                                        TargetControlID="Panel1_Content1" CollapseControlID="Panel1_Header1" ExpandControlID="Panel1_Header1"
                                        Collapsed="true" ImageControlID="pnl_img1" CollapsedImage="/Images/down_Arrow.gif"
                                        ExpandedImage="/Images/up_Arrow.gif">
                                    </ajax:CollapsiblePanelExtender>
                                    <ajax:CollapsiblePanelExtender ID="cpe11" BehaviorID="cpe11" runat="server" Enabled="True"
                                        TargetControlID="Panel1_Content1button" CollapseControlID="Panel1_Header1" ExpandControlID="Panel1_Header1"
                                        Collapsed="true" ImageControlID="pnl_img1" CollapsedImage="/Images/down_Arrow.gif"
                                        ExpandedImage="/Images/up_Arrow.gif">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="Panel1_Content1" runat="server" CssClass="collapsePanel">
                                        <div style="padding: 10px;">
                                            <%--<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                ClientEvents-OnRequestStart="conditionalPostback" CssClass="TabGrid">--%>
                                            <asp:TextBox ID="txt_test" runat="server" Visible="false"></asp:TextBox>
                                                <table cellpadding="0" cellspacing="2" border="0" width="838">
                                                    <tr>
                                                        <td colspan="4" style="padding-left:13px;">
                                                            <asp:Label ID="lblMessage" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                                                            <asp:HiddenField ID="hid_password" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" class="dv_md_sub_heading">
                                                            Company details
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 145px;" class="caption">
                                                            Company&nbsp;<span id="span_company_name" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td style="width: 270px;" class="details">
                                                            <asp:TextBox ID="txt_company" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_company" runat="server"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="rfv_company" runat="server" ControlToValidate="txt_company"
                                                                ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Company Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="caption" style="width: 145px;">
                                                            Company Website
                                                        </td>
                                                        <td class="details" style="width: 270px;">
                                                            <asp:TextBox ID="txt_website" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_website" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" class="dv_md_sub_heading">
                                                            Contact details
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            First Name&nbsp;<span id="span_first_name" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_first_name" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_first_name" runat="server"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="rfv_contact_name" runat="server" ControlToValidate="txt_first_name"
                                                                ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />First Name Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="caption">
                                                            Last Name
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_last_name" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_last_name" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            Title
                                                        </td>
                                                        <td class="details">
                                                            <asp:DropDownList ID="ddl_title" runat="server" DataTextField="name" DataValueField="name"
                                                                CssClass="DropDown" Width="204">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="lbl_title" runat="server"></asp:Label>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            Street&nbsp;<span id="span_address1" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_address1" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_address1" runat="server"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="rfv_address1" runat="server" ControlToValidate="txt_address1"
                                                                ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Street Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="caption">
                                                            Street Address 2
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_address2" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_address2" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            City&nbsp;<span id="span_city" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_city" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_city" runat="server"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="rfv_city" runat="server" ControlToValidate="txt_city"
                                                                ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />City Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="caption">
                                                            Country&nbsp;<span id="span_country" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:DropDownList ID="ddl_country" runat="server" DataTextField="name" DataValueField="country_id"
                                                                CssClass="DropDown" Width="204">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="lbl_country" runat="server"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="rfv_country" runat="server" ControlToValidate="ddl_country"
                                                                ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Country Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            State/Province&nbsp;<span id="span_state" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_other_state" runat="server" MaxLength="55" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_state" runat="server"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="rfv_txt_state" runat="server" ControlToValidate="txt_other_state"
                                                                ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />State Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="caption">
                                                            Postal Code&nbsp;<span id="span_zip" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_zip" runat="server" CssClass="inputtype" MaxLength="10"></asp:TextBox>
                                                            <asp:Label ID="lbl_zip" runat="server"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="rfv_zip" runat="server" ControlToValidate="txt_zip"
                                                                ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Postal Code Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txt_zip"
                                                                ValidationExpression="^\d+$" EnableClientScript="true" Display="Dynamic" ErrorMessage="<br />Invalid Postal Code"
                                                                runat="server" ValidationGroup="val_basic_info" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            Phone 1&nbsp;<span id="span_mobile" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:Label ID="lbl_mobile" runat="server"></asp:Label>
                                                            <asp:TextBox ID="txt_mobile" runat="server" CssClass="inputtype" MaxLength="15"></asp:TextBox>
                                                            <br />
                                                            <asp:RadioButton ID="rdo_ph_1_landline" runat="server" Text="Landline" GroupName="p1"
                                                                Checked="true" />
                                                            <asp:RadioButton ID="rdo_ph_1_mobile" runat="server" GroupName="p1" 
                                                                Text="Mobile" />
                                                            <asp:RequiredFieldValidator ID="rfv_mobile" runat="server" ControlToValidate="txt_mobile"
                                                                ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Phone 1 Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                                            <%--          <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txt_mobile"
                                                                ValidationExpression="[0-9]{10,13}" EnableClientScript="true" Display="Dynamic"
                                                                ErrorMessage="<br />Invalid Phone 1" runat="server" ValidationGroup="val_basic_info" />--%>
                                                        </td>
                                                        <td class="caption">
                                                            Phone Ext
                                                        </td>
                                                        <td class="details">
                                                            <asp:Label ID="lbl_phone_ext" runat="server"></asp:Label>
                                                            <asp:TextBox ID="txt_phone_ext" runat="server" CssClass="inputtype" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            Phone 2
                                                        </td>
                                                        <td class="details">
                                                            <asp:Label ID="lbl_phone" runat="server"></asp:Label>
                                                            <asp:TextBox ID="txt_phone" runat="server" CssClass="inputtype" MaxLength="15"></asp:TextBox>
                                                            <br />
                                                            <asp:RadioButton ID="rdo_ph_2_landline" runat="server" Text="Landline" GroupName="p2"
                                                                Checked="true" />
                                                            <asp:RadioButton ID="rdo_ph_2_mobile" runat="server" GroupName="p2" 
                                                                Text="Mobile" />
                                                            <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txt_phone"
                                                                ValidationExpression="[0-9]{10,13}" Display="Dynamic" EnableClientScript="true"
                                                                ErrorMessage="<br />Invalid Phone 2" runat="server" ValidationGroup="val_basic_info" />--%>
                                                        </td>
                                                        <td class="caption">
                                                            &nbsp;
                                                        </td>
                                                        <td class="details">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            Fax
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_fax" runat="server" CssClass="inputtype" MaxLength="15"></asp:TextBox>
                                                            <asp:Label ID="lbl_fax" runat="server"></asp:Label>
                                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txt_fax"
                                                                ValidationExpression="[0-9]{10,13}" Display="Dynamic" EnableClientScript="true"
                                                                ErrorMessage="<br />Invalid Fax" runat="server" ValidationGroup="val_basic_info" />--%>
                                                        </td>
                                                        <td class="caption">
                                                            Email&nbsp;<span id="span_email" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_email" runat="server" CssClass="inputtypePWD"></asp:TextBox>
                                                            <asp:Label ID="lbl_email" runat="server"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="rfv_email" runat="server" ControlToValidate="txt_email"
                                                                ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Email Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ValidationGroup="val_basic_info" ID="rev_email" runat="server"
                                                                ControlToValidate="txt_email" Display="Dynamic" ErrorMessage="<br />Invalid Email"
                                                                ValidationExpression="^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$"
                                                                CssClass="error"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" class="dv_md_sub_heading">
                                                            Account setup
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            User ID&nbsp;<span id="span_user_id" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_user_id" runat="server" CssClass="inputtypePWD"></asp:TextBox>
                                                            <asp:Label ID="lbl_user_id" runat="server"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="rfv_user_id" runat="server" ControlToValidate="txt_user_id"
                                                                ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />User ID Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="caption">
                                                            Password&nbsp;<span id="span_password" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:Literal ID="lit_change_password" runat="server"></asp:Literal>
                                                            <asp:TextBox ID="txt_password" runat="server" CssClass="inputtypePWD" TextMode="Password"></asp:TextBox>
                                                            <asp:Label ID="lbl_password" runat="server"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="rfv_password" runat="server" ControlToValidate="txt_password"
                                                                ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Password Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txt_password"
                                                                ValidationGroup="val_basic_info" ErrorMessage="<br />Alphanumeric of atleast 6 chars"
                                                                ValidationExpression="(?=.*\d)(?=.*[a-zA-Z]).{6,}" Display="Dynamic"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr id="trRetypePassword" runat="server">
                                                        <td class="details">
                                                            &nbsp;
                                                        </td>
                                                        <td class="details">
                                                            &nbsp;
                                                        </td>
                                                        <td class="caption">
                                                            <asp:Label ID="lbl_retype_password_caption" runat="server" Text="Retype Password"></asp:Label>&nbsp;<span
                                                                id="span_retype_passeord" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_retype_password" runat="server" CssClass="inputtypePWD" TextMode="Password"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfv_retype_password" runat="server" ControlToValidate="txt_retype_password"
                                                                ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Retype Password Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="comp_retype_password" runat="server" ControlToValidate="txt_retype_password"
                                                                ControlToCompare="txt_password" CssClass="error" Display="Dynamic" ValidationGroup="val_basic_info"
                                                                ErrorMessage="<br />Password Mismatch"></asp:CompareValidator>
                                                            <asp:Label ID="lbl_retype_password" runat="server" Text="******"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            Add comments here
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_comment" runat="server" CssClass="inputtype" TextMode="MultiLine"
                                                                Height="80px"></asp:TextBox>
                                                            <asp:Label ID="lbl_comment" runat="server"></asp:Label>
                                                        </td>
                                                        <td class="caption">
                                                            Status
                                                        </td>
                                                        <td class="details">
                                                        <asp:HiddenField ID="hid_status_click" runat="server" />
                                                            <asp:DropDownList ID="ddl_buyer_status" runat="server" DataTextField="status" DataValueField="status_id"
                                                                CssClass="DropDown" Width="204">
                                                            </asp:DropDownList>
                                                            <asp:HiddenField ID="hid_status_id" runat="server" Value="0" />
                                                            <asp:Label ID="lbl_status" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" class="dv_md_sub_heading">
                                                            Other details
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            Sales Representative&nbsp;<span id="spn_sales_rep" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:DropDownList ID="ddl_sale_rep" runat="server" DataTextField="name" DataValueField="user_id"
                                                                CssClass="DropDown" Width="204">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="lbl_sale_rep" runat="server"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" InitialValue="0" runat="server"
                                                                ControlToValidate="ddl_sale_rep" ValidationGroup="val_basic_info" Display="Dynamic"
                                                                ErrorMessage="<br />Sales Representative Required" CssClass="error"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="caption">
                                                            Linked Companies
                                                        </td>
                                                        <td class="details">
                                                            <asp:CheckBoxList ID="chklst_companies_linked" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                                                Width="100%" CellPadding="0" CellSpacing="0" BorderWidth="0">
                                                            </asp:CheckBoxList>
                                                            <asp:Literal ID="ltrl_linked_companies" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            Business Type
                                                        </td>
                                                        <td class="details">
                                                            <asp:CheckBoxList ID="chklst_business_types" runat="server" CellPadding="0" CellSpacing="0"
                                                                BorderWidth="0" Width="100%">
                                                            </asp:CheckBoxList>
                                                            <asp:Literal ID="ltrl_business_types" runat="server"></asp:Literal>
                                                        </td>
                                                        <td class="caption">
                                                            Industry Type
                                                        </td>
                                                        <td class="details">
                                                            <asp:CheckBoxList ID="chklst_industry_types" runat="server" CellPadding="0" CellSpacing="0"
                                                                BorderWidth="0" Width="100%">
                                                            </asp:CheckBoxList>
                                                            <asp:Literal ID="ltrl_industry_type" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            Buckets Assign
                                                        </td>
                                                        <td class="details">
                                                            <asp:CheckBoxList ID="chklst_buckets" runat="server" CellPadding="0" CellSpacing="0"
                                                                BorderWidth="0" Width="100%">
                                                            </asp:CheckBoxList>
                                                            <asp:Literal ID="ltrl_buckets" runat="server"></asp:Literal>
                                                        </td>
                                                        <td class="caption">
                                                            How did you find us?
                                                        </td>
                                                        <td class="details">
                                                            <asp:DropDownList ID="ddl_how_find_us" runat="server" CssClass="DropDown" Width="204">
                                                            </asp:DropDownList>
                                                            <asp:Literal ID="ltrl_how_find_us" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr id="tr2" runat="server">
                                                        <td class="caption">
                                                            PCS&nbsp;Account&nbsp;#
                                                        </td>
                                                        <td class="details">
                                                            <asp:Label ID="lbl_microtelecom_no" runat="server"></asp:Label>
                                                            <asp:TextBox ID="txt_microtelecom_no" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td class="caption">
                                                            Resale Certificate&nbsp;<span id="span_resale" runat="server" class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:FileUpload ID="fileupload1" runat="server" />
                                                            <asp:LinkButton ID="lnk_file1" runat="server" Text=""></asp:LinkButton>
                                                            <asp:LinkButton ID="lnk_remove" runat="server" ForeColor="Red" OnClientClick="return confirm('Are you sure to delete?');" Text="<img src='/images/false.gif' alt='remove'/>"  OnClick="lnk_remove_click"></asp:LinkButton>
                                                            <asp:Label ID="lbl_file" runat="server"></asp:Label>
                                                           <%-- <asp:CustomValidator runat="server" ID="cust_check_resale" EnableClientScript="true" ValidationGroup="val_basic_info" ErrorMessage="<br />Resale Certificate Required."
                                                                OnServerValidate="check_resale_validate"></asp:CustomValidator>--%>
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_unsubscribe_invite" runat="server" visible="false">
                                                        <td class="caption">
                                                            Unsubscribe for Auction Invitation
                                                        </td>
                                                        <td class="details">
                                                            <asp:CheckBox ID="chk_invite_auction" runat="server" />
                                                            <asp:Label ID="lbl_invite_auction_unsubscribe" runat="server"></asp:Label>
                                                            <asp:HiddenField ID="hid_unsubscribe_invite" runat="server" Value="0" />
                                                        </td>
                                                        <td class="caption">
                                                        </td>
                                                        <td class="details">
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr id="tr_tc_caption" runat ="server">
                                                        <td class="dv_md_sub_heading" colspan="4">
                                                            Terms And Conditions Acceptance Details
                                                        </td>
                                                    </tr>

                                                    <tr ID="tr_tc" runat="server">
                                                        <td class="caption">
                                                            Terms &amp; Condition Acceptance Date and Time
                                                        </td>
                                                        <td class="details">
                                                            <asp:Label ID="lbl_tc_date" runat="server"></asp:Label>
                                                        </td>
                                                        <td class="caption">
                                                            Terms &amp; Condition Acceptance IP Address
                                                        </td>
                                                        <td class="details">
                                                            <asp:Label ID="lbl_tc_ip" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                  

                                                </table>
                                            <%--</telerik:RadAjaxPanel>--%>
                                        </div>
                                    </asp:Panel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                    <asp:Panel ID="Panel1_Content1button" runat="server" CssClass="collapsePanel">
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel11" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                            ClientEvents-OnRequestStart="conditionalPostback">
                                            <div class="addButton">
                                                <asp:ImageButton ID="imgbtn_basic_edit" ImageUrl="/Images/edit.gif" runat="server"
                                                    CausesValidation="false" />
                                                <asp:ImageButton ID="imgbtn_basic_save" ValidationGroup="val_basic_info" runat="server"
                                                    AlternateText="Save" ImageUrl="/images/save.gif" />
                                                <asp:ImageButton ID="imgbtn_basic_update" ValidationGroup="val_basic_info" runat="server"
                                                    AlternateText="Update" ImageUrl="/images/update.gif" />
                                            </div>
                                            <div class="cancelButton" id="div_basic_cancel" runat="server">
                                                <asp:ImageButton ID="imgbtn_basic_cancel" CausesValidation="false" runat="server"
                                                    AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                            </div>
                                            <div style="padding: 10px 0px 5px 15px;">
                                                <asp:ImageButton ID="imgbtn_basic_delete" CausesValidation="false" runat="server"
                                                    AlternateText="Cancel" ImageUrl="/images/delete.png" />
                                            </div>
                                        </telerik:RadAjaxPanel>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trOfficialInfo" runat="server">
                                <td class="tdTabItem">
                                    <a name="offinfo"></a>
                                    <asp:Panel ID="Panel1_Header2" runat="server" CssClass="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/official_info_icon.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            Official Information
                                        </div>
                                        <asp:Image ID="pnl_img2" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                            CssClass="panelimage" />
                                    </asp:Panel>
                                    <ajax:CollapsiblePanelExtender ID="cpe2" BehaviorID="cpe2" runat="server" Enabled="True"
                                        TargetControlID="Panel1_Content2" CollapseControlID="Panel1_Header2" ExpandControlID="Panel1_Header2"
                                        Collapsed="true" ImageControlID="pnl_img2" CollapsedImage="/Images/down_Arrow.gif"
                                        ExpandedImage="/Images/up_Arrow.gif">
                                    </ajax:CollapsiblePanelExtender>
                                    <ajax:CollapsiblePanelExtender ID="cpe21" BehaviorID="cpe21" runat="server" Enabled="True"
                                        TargetControlID="Panel2_Content1button" CollapseControlID="Panel1_Header2" ExpandControlID="Panel1_Header2"
                                        Collapsed="true" ImageControlID="pnl_img2" CollapsedImage="/Images/down_Arrow.gif"
                                        ExpandedImage="/Images/up_Arrow.gif">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="Panel1_Content2" runat="server" CssClass="collapsePanel">
                                        <div style="padding: 10px;">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                CssClass="TabGrid">
                                                <table cellpadding="0" cellspacing="2" border="0" width="838">
                                                    <tr>
                                                        <td style="width: 145px;" class="caption">
                                                            Max Amount of Bid
                                                        </td>
                                                        <td style="width: 270px;" class="details">
                                                            <%--<asp:TextBox ID="txt_max_amount_bid" runat="server" CssClass="inputtype" MaxLength="10"></asp:TextBox>--%>
                                                            <asp:HiddenField ID="hid_buyer_max_bid_amount" runat="server" Value="0" />
                                                            <telerik:RadNumericTextBox ID="txt_max_amount_bid" runat="server" LabelCssClass="inputtype"
                                                                MaxLength="10" Type="Currency" EnableTheming="true" ValidationGroup="val_official_info"
                                                                Width="200">
                                                            </telerik:RadNumericTextBox>
                                                            <asp:RegularExpressionValidator ID="rev_max_amount_bid" runat="server" Display="Dynamic"
                                                                ControlToValidate="txt_max_amount_bid" ValidationGroup="val_official_info" CssClass="error"
                                                                ErrorMessage="<br />Invalid amount" ValidationExpression="^\d*\.?\d*$"></asp:RegularExpressionValidator>
                                                            <asp:Label ID="lbl_max_amount_bid" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="width: 145px;" class="caption">
                                                            Approval Status
                                                        </td>
                                                        <td style="width: 270px;" class="details">
                                                            <asp:Label ID="lbl_approval_status" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            # of Employees
                                                        </td>
                                                        <td class="details">
                                                            <asp:Label ID="lbl_no_of_employees" runat="server"></asp:Label>
                                                            <asp:DropDownList ID="dd_no_of_employees" runat="server" CssClass="DropDown" Width="204">
                                                                <asp:ListItem Selected="True" Text="--Select--" Value=""></asp:ListItem>
                                                                <asp:ListItem Text="less than 50" Value="less than 50"></asp:ListItem>
                                                                <asp:ListItem Text="50 to 100" Value="50 to 100"></asp:ListItem>
                                                                <asp:ListItem Text="100 and above" Value="100 and above"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="caption">
                                                            Annual Turnover
                                                        </td>
                                                        <td class="details">
                                                            <asp:Label ID="lbl_annual_turnover" runat="server"></asp:Label>
                                                            <asp:DropDownList ID="dd_annual_turnover" runat="server" CssClass="DropDown" Width="204">
                                                                <asp:ListItem Selected="True" Text="--Select--" Value=""></asp:ListItem>
                                                                <asp:ListItem Text="less than 10K" Value="less than 10K"></asp:ListItem>
                                                                <asp:ListItem Text="10K to 50K" Value="10K to 50K"></asp:ListItem>
                                                                <asp:ListItem Text="50K and above" Value="50K and above"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" class="dv_md_sub_heading">
                                                            Bank Information
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            Routing #
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_routing_no" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_routing_no" runat="server"></asp:Label>
                                                        </td>
                                                        <td class="caption">
                                                            Account #
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_account_no" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_account_no" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            Name of Bank
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_name_bank" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_name_bank" runat="server"></asp:Label>
                                                        </td>
                                                        <td class="caption">
                                                            Branch Name
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_branch_name" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_branch_name" runat="server"></asp:Label>
                                                            <%-- <asp:RequiredFieldValidator ID="req_branch_name" runat="server" ControlToValidate="txt_branch_name"
                                                                ValidationGroup="val_official_info" Display="Dynamic" ErrorMessage="<br />Branch Name Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            Bank Contact Details
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_bank_contact_detail" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_bank_contact_detail" runat="server"></asp:Label>
                                                        </td>
                                                        <td class="caption">
                                                            Bank Address
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_bank_address" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_bank_address" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">
                                                            Bank Phone
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_bank_phone" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_bank_phone" runat="server"></asp:Label>
                                                            <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ControlToValidate="txt_bank_phone"
                                                                ValidationExpression="[0-9]{10,13}" EnableClientScript="true" Display="Dynamic"
                                                                ErrorMessage="<br />Invalid" runat="server" ValidationGroup="val_official_info" />--%>
                                                        </td>
                                                        <td class="caption">
                                                            Tax ID/SSN #
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_tax_id_ssn_no" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            <asp:Label ID="lbl_tax_id_ssn_no" runat="server"></asp:Label>
                                                            <%--<asp:RequiredFieldValidator ID="req_tax_id_ssn_no" runat="server" ControlToValidate="txt_tax_id_ssn_no"
                                                                ValidationGroup="val_official_info" Display="Dynamic" ErrorMessage="<br />Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </telerik:RadAjaxPanel>
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel2_attachment" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                CssClass="TabGrid" ClientEvents-OnRequestStart="conditionalPostback">
                                                <telerik:RadProgressManager ID="RadProgressManager1" runat="server" Skin="Vista" />
                                                <telerik:RadProgressArea ID="RadProgressArea1" runat="server" Skin="Vista" />
                                                <div class="dv_md_sub_heading">
                                                    Attachments
                                                </div>
                                                <br />
                                                <telerik:RadGrid ID="RadGrid_Attachment" runat="server" Skin="Vista" AllowPaging="True"
                                                    AllowSorting="True" AutoGenerateColumns="False" Width="100%" OnNeedDataSource="RadGrid_Attachment_NeedDataSource"
                                                    OnDeleteCommand="RadGrid_Attachment_DeleteCommand" OnInsertCommand="RadGrid_Attachment_InsertCommand"
                                                    OnUpdateCommand="RadGrid_Attachment_UpdateCommand" AllowFilteringByColumn="True">
                                                    <PagerStyle Mode="NextPrevAndNumeric" />
                                                    <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="buyer_attachment_id"
                                                        HorizontalAlign="NotSet" AutoGenerateColumns="False" EditMode="EditForms">
                                                        <CommandItemStyle BackColor="#e1dddd" />
                                                        <NoRecordsTemplate>
                                                            Attachments not available</NoRecordsTemplate>
                                                        <SortExpressions>
                                                            <telerik:GridSortExpression FieldName="title" SortOrder="Ascending" />
                                                        </SortExpressions>
                                                        <CommandItemSettings AddNewRecordText="Add New Attachment" ShowRefreshButton="false" />
                                                        <Columns>
                                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                EditImageUrl="/Images/edit_grid.gif">
                                                                <ItemStyle HorizontalAlign="center" CssClass="MyImageButton" Width="30" />
                                                            </telerik:GridEditCommandColumn>
                                                            <telerik:GridBoundColumn DataField="buyer_attachment_id" HeaderText="attachment_id"
                                                                UniqueName="buyer_attachment_id" ReadOnly="True" Visible="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="title" HeaderText="Title" UniqueName="Title"
                                                                HeaderStyle-Width="180" ItemStyle-Width="150" FilterControlWidth="150">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridTemplateColumn HeaderText="File Name" SortExpression="filename" UniqueName="filename"
                                                                EditFormColumnIndex="1" ItemStyle-Width="150" GroupByExpression="filename [File Name] Group By filename">
                                                                <ItemTemplate>
                                                                    <a href="/Upload/Bidders/official_attachments/<%= Request.QueryString("i") %>/<%#Eval("buyer_attachment_id") %>/<%#Eval("filename") %>"
                                                                        target="_blank">
                                                                        <%#Eval("filename") %></a>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"
                                                                ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                                                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                                            </telerik:GridButtonColumn>
                                                        </Columns>
                                                        <EditFormSettings EditFormType="Template" EditColumn-ItemStyle-Width="100%" EditColumn-HeaderStyle-HorizontalAlign="Left"
                                                            EditColumn-ItemStyle-HorizontalAlign="Left">
                                                            <FormTemplate>
                                                                <table cellpadding="0" cellspacing="2" border="0" style="padding-top: 10px; text-align: left;"
                                                                    width="100%">
                                                                    <tr>
                                                                        <td style="font-weight: bold;" colspan="4">
                                                                            Attachment Detail
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="caption" style="width: 110px;">
                                                                            Title
                                                                        </td>
                                                                        <td class="details" style="width: 270px;">
                                                                            <asp:TextBox ID="txt_attach_title" runat="server" Text='<%# Bind("title") %>' CssClass="inputtype"
                                                                                ValidationGroup="val_attach"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfv_attach_title" runat="server" ErrorMessage="<br>Title Required"
                                                                                CssClass="error" ControlToValidate="txt_attach_title" Display="Dynamic" ValidationGroup="val_attach"></asp:RequiredFieldValidator>
                                                                        </td>
                                                                        <td style="width: 145px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td style="width: 270px;">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="caption" style="width: 110px;">
                                                                            Select File
                                                                        </td>
                                                                        <td class="details" style="width: 270px;">
                                                                            <asp:FileUpload ID="fileupload_attach" runat="server" CssClass="TextBox" />
                                                                            <asp:RequiredFieldValidator ID="rfv_attach_file" runat="server" ErrorMessage="<br>File Required"
                                                                                CssClass="error" ControlToValidate="fileupload_attach" Display="Dynamic" ValidationGroup="val_attach"></asp:RequiredFieldValidator>
                                                                        </td>
                                                                        <td style="width: 145px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td style="width: 270px;">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4" align="right">
                                                                            <asp:ImageButton ID="img_btn_save_attach" runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif", "/images/update.gif") %>'
                                                                                CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "PerformInsert", "Update")%>'
                                                                                ValidationGroup="val_attach" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <asp:ImageButton ID="img_btn_cancel" runat="server" ImageUrl="/images/cancel.gif"
                                                                                CausesValidation="false" CommandName="cancel" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </FormTemplate>
                                                        </EditFormSettings>
                                                    </MasterTableView>
                                                    <ClientSettings>
                                                        <Selecting AllowRowSelect="True"></Selecting>
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </telerik:RadAjaxPanel>
                                        </div>
                                    </asp:Panel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                                    <asp:Panel ID="Panel2_Content1button" runat="server" CssClass="collapsePanel">
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel22" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                                            <div class="addButton">
                                                <asp:ImageButton ID="imgbtn_official_edit" ImageUrl="/Images/edit.gif" runat="server"
                                                    CausesValidation="false" />
                                                <asp:ImageButton ID="imgbtn_official_update" ValidationGroup="val_official_info"
                                                    runat="server" AlternateText="Update" ImageUrl="/images/update.gif" />
                                            </div>
                                            <div class="cancelButton" id="div_official_cancel" runat="server">
                                                <asp:ImageButton ID="imgbtn_official_cancel" CausesValidation="false" runat="server"
                                                    AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                            </div>
                                        </telerik:RadAjaxPanel>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trSubLogin" runat="server">
                                <td class="tdTabItem">
                                    <a name="4"></a>
                                    <asp:Panel ID="Panel1_Header3" runat="server" CssClass="pnlTabItemHeader">
                                        <div class="tabItemHeader" style="background: url(/Images/sub_logins_icon.gif) no-repeat;
                                            background-position: 10px 0px;">
                                            Sub-Logins
                                        </div>
                                        <asp:Image ID="pnl_img3" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                            CssClass="panelimage" />
                                    </asp:Panel>
                                    <ajax:CollapsiblePanelExtender ID="cpe3" BehaviorID="cpe3" runat="server" Enabled="True"
                                        TargetControlID="Panel1_Content3" CollapseControlID="Panel1_Header3" ExpandControlID="Panel1_Header3"
                                        Collapsed="true" ImageControlID="pnl_img3" CollapsedImage="/Images/down_Arrow.gif"
                                        ExpandedImage="/Images/up_Arrow.gif">
                                    </ajax:CollapsiblePanelExtender>
                                    <asp:Panel ID="Panel1_Content3" runat="server" CssClass="collapsePanel">
                                        <div style="padding: 10px;">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel3" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                CssClass="TabGrid">
                                                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadGrid ID="rad_grid_sublogins" runat="server" Skin="Vista" GridLines="None"
                                                                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                                                OnNeedDataSource="rad_grid_sublogins_NeedDataSource" OnDeleteCommand="rad_grid_sublogins_DeleteCommand"
                                                                OnInsertCommand="rad_grid_sublogins_InsertCommand" OnUpdateCommand="rad_grid_sublogins_UpdateCommand"
                                                                enableajax="true" ClientSettings-Selecting-AllowRowSelect="false" PageSize="10"
                                                                ShowGroupPanel="true">
                                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="buyer_user_id"
                                                                    ItemStyle-Height="40" AlternatingItemStyle-Height="40" HorizontalAlign="NotSet"
                                                                    AutoGenerateColumns="False" EditMode="EditForms">
                                                                    <CommandItemStyle BackColor="#E1DDDD" />
                                                                    <NoRecordsTemplate>
                                                                        Sub login not available</NoRecordsTemplate>
                                                                    <SortExpressions>
                                                                        <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                                                                    </SortExpressions>
                                                                    <CommandItemSettings AddNewRecordText="Add New Sub-Login" />
                                                                    <Columns>
                                                                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                            EditImageUrl="/Images/edit_grid.gif" HeaderStyle-Width="90">
                                                                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="90" />
                                                                        </telerik:GridEditCommandColumn>
                                                                        <telerik:GridBoundColumn DataField="buyer_user_id" HeaderText="buyer_user_id" UniqueName="buyer_user_id"
                                                                            ReadOnly="True" Visible="false">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="buyer_id" HeaderText="buyer_id" UniqueName="buyer_id"
                                                                            ReadOnly="True" Visible="false">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Name" SortExpression="name" UniqueName="first_name"
                                                                            EditFormColumnIndex="1" HeaderStyle-Width="180" ItemStyle-Width="180" GroupByExpression="first_name [Name] Group By first_name">
                                                                            <ItemTemplate>
                                                                                <%# Eval("first_name") & " " & Eval("last_name")%>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Title" AllowFiltering="true" UniqueName="title"
                                                                            HeaderStyle-Width="100" ItemStyle-Width="90" GroupByExpression="title [Title] Group By title">
                                                                            <ItemTemplate>
                                                                                <%# Eval("title")%>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="User Name" AllowFiltering="true" UniqueName="username"
                                                                            HeaderStyle-Width="100" ItemStyle-Width="90" GroupByExpression="username [User Name] Group By username">
                                                                            <ItemTemplate>
                                                                                <%# Eval("username")%>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Password" AllowFiltering="false" UniqueName="password"
                                                                            EditFormColumnIndex="1" HeaderStyle-Width="90" ItemStyle-Width="80" Groupable="false">
                                                                            <ItemTemplate>
                                                                                ******
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Email" DataField="email" AllowFiltering="true"
                                                                            UniqueName="email" HeaderStyle-Width="100" ItemStyle-Width="90" GroupByExpression="email [Email] Group By email">
                                                                            <ItemTemplate>
                                                                                <a href='mailto:<%# Eval("email")%>'>
                                                                                    <%# Eval("email")%></a>
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate>
                                                                            </EditItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn UniqueName="bidding_limit" SortExpression="bidding_limit"
                                                                            HeaderText="Bidding Limit" HeaderStyle-Width="90" ItemStyle-Width="90" ItemStyle-HorizontalAlign="Right"
                                                                            GroupByExpression="bidding_limit [Bidding Limit] Group By bidding_limit">
                                                                            <ItemTemplate>
                                                                                <%# String.Format("{0:C2}", Eval("bidding_limit"))%>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Is Active" UniqueName="is_active" EditFormColumnIndex="1"
                                                                            HeaderStyle-Width="70" ItemStyle-Width="70" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Center" GroupByExpression="is_active [Is Active] Group By is_active">
                                                                            <ItemTemplate>
                                                                                <img src='<%#IIf(Eval("is_active"),"/Images/true.gif","/Images/false.gif") %>' style="border: none;"
                                                                                    alt="" />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"
                                                                            ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                                                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                                                        </telerik:GridButtonColumn>
                                                                    </Columns>
                                                                    <EditFormSettings InsertCaption="Insert New Sub-Login" EditFormType="Template">
                                                                        <FormTemplate>
                                                                            <table id="tbl_grd_item_edit" cellpadding="0" cellspacing="2" border="0" width="800px">
                                                                                <tr>
                                                                                    <td style="font-weight: bold; padding-top: 10px;" colspan="4">
                                                                                        Sub-Login Details
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="caption" style="width: 135px;">
                                                                                        First Name&nbsp;<span class="req_star">*</span>
                                                                                    </td>
                                                                                    <td style="width: 260px;" class="details">
                                                                                        <asp:TextBox ID="txt_sub_first_name" runat="server" Text='<%# Bind("first_name")%>'
                                                                                            CssClass="inputtype"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfv_first_name" runat="server" ErrorMessage="*" ForeColor="red"
                                                                                            ControlToValidate="txt_sub_first_name" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="caption" style="width: 135px;">
                                                                                        Last Name
                                                                                    </td>
                                                                                    <td style="width: 260px;" class="details">
                                                                                        <asp:TextBox ID="txt_sub_last_name" runat="server" Text='<%# Bind("last_name")%>'
                                                                                            CssClass="inputtype"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="caption">
                                                                                        Title
                                                                                    </td>
                                                                                    <td class="details">
                                                                                        <asp:DropDownList ID="dd_sub_title" DataSourceID="sub_login_title" DataTextField="name"
                                                                                            DataValueField="title_id" runat="server" SelectedValue='<%# Bind("title")%>'
                                                                                            CssClass="DropDown" Width="204">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                    <td class="caption">
                                                                                        Email&nbsp;<span class="req_star">*</span>
                                                                                    </td>
                                                                                    <td class="details">
                                                                                        <asp:TextBox ID="txt_sub_email" runat="server" Text='<%# Bind("email") %>' CssClass="inputtype"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfv_email" runat="server" ErrorMessage="*" ForeColor="red"
                                                                                            ControlToValidate="txt_sub_email" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ValidationGroup="val1_sub" ID="rev_email" runat="server"
                                                                                            ControlToValidate="txt_sub_email" Display="Dynamic" ErrorMessage="*" ValidationExpression="^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$"
                                                                                            CssClass="error"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="caption">
                                                                                        User Name&nbsp;<span class="req_star">*</span>
                                                                                    </td>
                                                                                    <td class="details">
                                                                                        <asp:TextBox ID="txt_sub_username" Enabled='<%#IIf(Eval("username").ToString<>"",false,true) %>'
                                                                                            runat="server" Text='<%# Bind("username") %>' CssClass="inputtypePWD" MaxLength="50"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfv_username" runat="server" ErrorMessage="*" ForeColor="red"
                                                                                            ControlToValidate="txt_sub_username" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="caption" style="width: 145px;">
                                                                                        Password&nbsp;<span class="req_star">*</span>
                                                                                    </td>
                                                                                    <td class="details">
                                                                                        <asp:TextBox ID="txt_sub_password" runat="server" TextMode="Password" Text='<%# Security.EncryptionDecryption.DecryptValue(IIF(Eval("password")IS DBNULL.Value,"",Eval("password"))) %>'
                                                                                            CssClass="inputtypePWD"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfv_password" runat="server" ErrorMessage="*" ForeColor="red"
                                                                                            ControlToValidate="txt_sub_password" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="rev_password" runat="server" Display="Dynamic"
                                                                                            ControlToValidate="txt_sub_password" ValidationGroup="val1_sub" CssClass="error"
                                                                                            ErrorMessage="<br>Alphanumeric of atleast 6 chars." ValidationExpression="(?=.*\d)(?=.*[a-zA-Z]).{6,}"></asp:RegularExpressionValidator>
                                                                                        <asp:HiddenField ID="hid_sub_password" runat="server" Value='<%# Security.EncryptionDecryption.DecryptValue(IIF(Eval("password") IS DBNULL.Value,"",Eval("password"))) %>' />
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td class="caption">
                                                                                        Terms & Condition Accept Date/Time
                                                                                    </td>
                                                                                    <td class="details">
                                                                                        <asp:Label ID="lbl_tc_subaccept_date" runat ="server" Text= '<%# Bind("tc_accepted_date") %>'></asp:Label>                                                                                       
                                                                                        
                                                                                    </td>
                                                                                    <td class="caption">
                                                                                        Terms & Condition Acceptance IP
                                                                                    </td>
                                                                                    <td class="details">
                                                                                   <asp:Label ID="lbl_tc_sublogin_ip" runat ="server"  Text= '<%# Bind("tc_accept_ip") %>'></asp:Label>
                                                                                        
                                                                                    </td>
                                                                                </tr>



                                                                                <tr>
                                                                                    <td class="caption">
                                                                                        Bidding Limit&nbsp;<span class="req_star">*</span>
                                                                                    </td>
                                                                                    <td class="details">
                                                                                        <asp:TextBox ID="txt_sub_bidding_limit" runat="server" MaxLength="8" Text='<%# Bind("bidding_limit") %>'
                                                                                            Type="Currency" CssClass="inputtype">
                                                                                        </asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfv_bidding_limit" runat="server" ErrorMessage="*"
                                                                                            ForeColor="red" ControlToValidate="txt_sub_bidding_limit" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="rev_bidding_limit" runat="server" Display="Dynamic"
                                                                                            ControlToValidate="txt_sub_bidding_limit" ValidationGroup="val1_sub" CssClass="error"
                                                                                            ErrorMessage="<br />Invalid" ValidationExpression="^\d*\.?\d*$"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                    <td class="caption">
                                                                                        Is Active
                                                                                    </td>
                                                                                    <td class="details">
                                                                                        <asp:CheckBox ID="chk_sub_is_active" runat="server" Checked='<%# IIF(checked(Eval("is_active")),True,False) %>' />
                                                                                        <asp:HiddenField ID="hid_old_active_status" runat="server" Value='<%# IIF(checked(Eval("is_active")),1,0) %>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" align="right">
                                                                                        <asp:Literal ID="lbl_grid_error" runat="server"></asp:Literal>
                                                                                    </td>
                                                                                    <td align="right" colspan="2">
                                                                                        <asp:ImageButton ID="but_grd_submit" CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem),"PerformInsert","Update") %>'
                                                                                            ValidationGroup="val1_sub" runat="server" AlternateText='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "Save" , "Update") %>'
                                                                                            ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif" , "/images/update.gif") %>' />
                                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="but_grd_cancel" CausesValidation="false" CommandName="Cancel"
                                                                                            runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </FormTemplate>
                                                                    </EditFormSettings>
                                                                </MasterTableView>
                                                                <ClientSettings AllowDragToGroup="true">
                                                                    <Selecting AllowRowSelect="True"></Selecting>
                                                                </ClientSettings>
                                                                <GroupingSettings ShowUnGroupButton="true" />
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </telerik:RadAjaxPanel>
                                        </div>
                                    </asp:Panel>
                                </td>
                                <td class="fixedcolumn" valign="top">
                                    <asp:SqlDataSource ID="sub_login_title" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                        runat="server" ProviderName="System.Data.SqlClient" SelectCommand="select '--Select--' as name,'' as title_id union all select isnull(name,'') as name,isnull(name,'') as title_id from tbl_master_titles order by name">
                                    </asp:SqlDataSource>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    
                        <telerik:RadPageView ID="RadPageView2" runat="server" CssClass="pageViewEducation">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr id="trInvite" runat="server">
                                    <td class="tdTabItem">
                                        <a name="5"></a>
                                        <asp:Panel ID="Panel1_Header4" runat="server" CssClass="pnlTabItemHeader">
                                            <div class="tabItemHeader" style="background: url(/Images/invite_auction_icon.gif) no-repeat;
                                                background-position: 10px 0px;">
                                                Invite For Auctions
                                            </div>
                                            <asp:Image ID="pnl_img4" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                                CssClass="panelimage" />
                                        </asp:Panel>
                                        <ajax:CollapsiblePanelExtender ID="cpe4" BehaviorID="cpe4" runat="server" Enabled="True"
                                            TargetControlID="Panel1_Content4" CollapseControlID="Panel1_Header4" ExpandControlID="Panel1_Header4"
                                            Collapsed="false" ImageControlID="pnl_img4" CollapsedImage="/Images/down_Arrow.gif"
                                            ExpandedImage="/Images/up_Arrow.gif">
                                        </ajax:CollapsiblePanelExtender>
                                        <asp:Panel ID="Panel1_Content4" runat="server" CssClass="collapsePanel">
                                            <div style="padding: 10px;">
                                                <telerik:RadAjaxPanel ID="RadAjaxPanel8" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                    CssClass="TabGrid">
                                                    <table cellpadding="0" cellspacing="2" border="0" width="100%">
                                                        <tr>
                                                            <td class="dv_md_sub_heading">
                                                                Select Auctions for Invitation
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding-top: 10px;">
                                                                <%--<div class="sub_details" style="margin-top: 10px;">--%>
                                                                <telerik:RadGrid ID="rad_grid_invite_auctions" runat="server" Skin="Vista" GridLines="None"
                                                                    AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                                                    GroupingEnabled="false" OnNeedDataSource="rad_grid_invite_auctions_NeedDataSource"
                                                                    enableajax="true">
                                                                    <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                    <ClientSettings>
                                                                        <Selecting AllowRowSelect="True"></Selecting>
                                                                    </ClientSettings>
                                                                    <MasterTableView DataKeyNames="auction_id" AutoGenerateColumns="False" ItemStyle-Height="40"
                                                                        AlternatingItemStyle-Height="40">
                                                                        <NoRecordsTemplate>
                                                                            Invite Auctions not available</NoRecordsTemplate>
                                                                        <SortExpressions>
                                                                            <telerik:GridSortExpression FieldName="code" SortOrder="Ascending" />
                                                                        </SortExpressions>
                                                                        <Columns>
                                                                            <telerik:GridBoundColumn DataField="auction_id" HeaderText="auction_id" UniqueName="auction_id"
                                                                                ReadOnly="True" Visible="false">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn DataField="code" HeaderText="Code" UniqueName="code" HeaderStyle-Width="100"
                                                                                ItemStyle-Width="80">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridTemplateColumn DataField="title" HeaderText="Title" SortExpression="title"
                                                                                UniqueName="title" FilterControlWidth="300" GroupByExpression="title [Title] group by title"
                                                                                HeaderStyle-Width="200" ItemStyle-Width="180">
                                                                                <ItemTemplate>
                                                                                    <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');"
                                                                                        onclick="redirectIframe('/Backend_TopBar.aspx?t=<%# CommonCode.getAuctionTabPosition(Eval("auction_id")) %>&a=<%# Eval("auction_id") %>','/Backend_Leftbar.aspx?t=3&a=<%# Eval("auction_id") %>','/Auctions/AddEditAuction.aspx?i=<%# Eval("auction_id") %>')">
                                                                                        <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("title") %>'></asp:Label></a>
                                                                                </ItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                            <%--<telerik:GridBoundColumn DataField="start_date" HeaderText="Start Date" UniqueName="start_date"
                                                                            HeaderStyle-Width="130" ItemStyle-Width="110">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="end_date" HeaderText="End Date" UniqueName="end_date"
                                                                            HeaderStyle-Width="130" ItemStyle-Width="110">
                                                                        </telerik:GridBoundColumn>--%>
                                                                            <telerik:GridBoundColumn DataField="status" HeaderText="Status" UniqueName="status"
                                                                                HeaderStyle-Width="130" ItemStyle-Width="110">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridTemplateColumn UniqueName="CheckBoxTemplateColumn" HeaderStyle-Width="80"
                                                                                ItemStyle-Width="60">
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="ToggleRowSelection"
                                                                                        AutoPostBack="True" />
                                                                                </ItemTemplate>
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="headerChkbox" runat="server" OnCheckedChanged="ToggleSelectedState"
                                                                                        AutoPostBack="True" />&nbsp;Select All
                                                                                </HeaderTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                        </Columns>
                                                                    </MasterTableView>
                                                                </telerik:RadGrid>
                                                                <%--</div>--%>
                                                                <div style="padding: 10px 0px 10px 0px; text-align: right;">
                                                                    <asp:ImageButton ID="btn_invite_all" runat="server" ImageUrl="/Images/invite_selected.gif" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <%--<div style="float: right;">
                                                <img src="/Images/Q.mark.png" alt="help?" />&nbsp;
                                            </div>--%>
                                                </telerik:RadAjaxPanel>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                    <td class="fixedcolumn" valign="top">
                                        <%-- <div class="cancelButton">
                                        <a href="javascript:void(0);" onclick="javascript:open_help('5');">
                                            <img src="/Images/help-button.gif" border="none" />
                                        </a>
                                    </div>--%>
                                    </td>
                                </tr>
                            </table>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView3" runat="server" CssClass="pageViewEducation">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr id="tr1" runat="server">
                                    <td class="tdTabItem">
                                        <a name="6"></a>
                                        <asp:Panel ID="Panel1_Header9" runat="server" CssClass="pnlTabItemHeader">
                                            <div class="tabItemHeader" style="background: url(/Images/comments_icon.gif) no-repeat;
                                                background-position: 10px 0px;">
                                                Comments
                                            </div>
                                            <asp:Image ID="pnl_img9" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                                CssClass="panelimage" />
                                        </asp:Panel>
                                        <asp:Panel ID="Panel1_Content9" runat="server" CssClass="collapsePanel">
                                            <div style="padding: 10px;">
                                                <telerik:RadAjaxPanel ID="RadAjaxPanel4" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                    CssClass="TabGrid">
                                                    <uc3:AuctionComments ID="UC_auction_comments" runat="server" />
                                                </telerik:RadAjaxPanel>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView4" runat="server" CssClass="pageViewEducation">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr id="trInvited" runat="server">
                                    <td class="tdTabItem">
                                        <a name="7"></a>
                                        <asp:Panel ID="Panel1_Header10" runat="server" CssClass="pnlTabItemHeader">
                                            <div class="tabItemHeader" style="background: url(/Images/comments_icon.gif) no-repeat;
                                                background-position: 10px 0px;">
                                                Auctions Invited
                                            </div>
                                            <asp:Image ID="pnl_img10" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                                CssClass="panelimage" />
                                        </asp:Panel>
                                        <asp:Panel ID="Panel1_Content10" runat="server" CssClass="collapsePanel">
                                            <div style="padding: 10px;">
                                                <telerik:RadAjaxPanel ID="RadAjaxPanel6" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                                    CssClass="TabGrid">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <uc4:AuctionInvited ID="UC_AuctionInvited" runat="server" AuctionMode="IU" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <uc4:AuctionInvited ID="AuctionInvited1" runat="server" AuctionMode="IL" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <uc4:AuctionInvited ID="UC_AuctionWon" runat="server" AuctionMode="W" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <uc4:AuctionInvited ID="UC_AuctionLost" runat="server" AuctionMode="L" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </telerik:RadAjaxPanel>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </telerik:RadPageView>
                    
                </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
    <telerik:RadAjaxPanel ID="RadAjaxPanel5" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
        ClientEvents-OnRequestStart="conditionalPostback">
        <asp:Button ID="btn_hideModel" runat="server" Style="display: none;" />
        <ajax:ModalPopupExtender ID="modalPopUpExtender1" runat="server" TargetControlID="btn_hideModel"
            PopupControlID="pnlPopUpLogin" BackgroundCssClass="modalBackground" CancelControlID="imgClostModal">
        </ajax:ModalPopupExtender>
        <asp:Panel runat="Server" ID="pnlPopUpLogin" Height="120px" Width="400px" BackColor="#ffffff"
            CssClass="confirm-dialog" Style="display: none; border: solid 1px #000000;">
            <div class="modalHeading" Style="display: none;">
                Please Fill the Following
                <asp:ImageButton runat="server" ID="imgClostModal" CssClass="close_modal" 
                    ImageUrl="/Images/close_modal.gif" />
            </div>
            <div class="modalBody1">
                <table cellpadding="3" cellspacing="3" border="0">
                    <tr style="display: none;">
                        <td align="right">
                            Max Amount of Bid :
                        </td>
                        <td align="left">
                            <%-- <asp:TextBox ID="txt_max_amt" runat="server" MaxLength="10" Text="0"></asp:TextBox>--%>
                            <%--<telerik:RadNumericTextBox ID="txt_max_amt" runat="server" Type="Currency" Width="150"
                                CssClass="TextBox" Value="0">
                            </telerik:RadNumericTextBox>--%>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td align="right">
                            Sale Representative :
                        </td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddl_sales" CssClass="TextBox" runat="server" DataTextField="name"
                                Width="155" DataValueField="user_id">
                            </asp:DropDownList>
                            <asp:Label ID="lbl_sales" runat="server" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td align="right" class="modalCaption" style="padding-top: 20px;">
                            File Upload
                        </td>
                        <td align="left" colspan="3">
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td align="right">
                            File Upload Title1 :
                        </td>
                        <td align="left">
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("title") %>' CssClass="TextBox"></asp:TextBox>
                        </td>
                        <td align="right">
                            Select File1 :
                        </td>
                        <td align="left">
                            <asp:FileUpload ID="topfileupload_attach" runat="server" CssClass="TextBox" Style="width: 250px" />
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td align="right">
                            File Upload Title2 :
                        </td>
                        <td align="left">
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("title") %>' CssClass="TextBox"></asp:TextBox>
                        </td>
                        <td align="right">
                            Select File2 :
                        </td>
                        <td align="left">
                            <asp:FileUpload ID="topfileupload_attach1" runat="server" CssClass="TextBox" Style="width: 250px" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                        </td>
                        <td colspan="3"><br />
                            <asp:CheckBox ID="chk_term" EnableViewState="true" ValidationGroup="ak" Text="Are you sure you have uploaded all the required documents?"
                                runat="server"></asp:CheckBox>
                            <%--<asp:CustomValidator runat="server" ID="CheckBoxRequired" EnableClientScript="true" ValidationGroup="ak" ErrorMessage="You must select this box to proceed."
                                 OnServerValidate="CheckBoxRequired_ServerValidate"
                                ClientValidationFunction="CheckBoxRequired_ClientValidate"></asp:CustomValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="4">
                            <asp:Label ID="lbl_file_upload_status" display="dynamic" runat="server" EnableViewState="true"></asp:Label>
                            <asp:Label ID="lbl_file_upload_status1" EnableViewState="true" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                        </td>
                        <td align="left" colspan="3">
                            <asp:Label ID="lbl_error" ForeColor="Red" runat="server" EnableViewState="false" Visible="false" Text="Invalid Amount"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                        </td>
                        <td align="left" colspan="3">
                            <asp:ImageButton runat="server" AlternateText="Login" ValidationGroup="ak" ID="img_btnsend"
                                ImageUrl="/Images/update.gif" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:ImageButton ID="img_btnCancel" CausesValidation="false" runat="server" AlternateText="Cancel"
                                ImageUrl="/Images/close.gif" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </telerik:RadAjaxPanel>
</asp:Content>
