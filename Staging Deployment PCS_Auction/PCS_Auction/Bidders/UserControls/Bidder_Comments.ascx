﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Bidder_Comments.ascx.vb" Inherits="Bidders_UserControls_Bidder_Comments" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
    Skin="Simple" />
<asp:HiddenField ID="hid_buyer_id" runat="server" Value="0" />
<asp:HiddenField ID="hid_user_id" runat="server" Value="0" />
<telerik:RadGrid ID="RadGrid_Comments" GridLines="None" runat="server" AllowAutomaticDeletes="True"
    AllowAutomaticInserts="True" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True"
    AutoGenerateColumns="False" DataSourceID="SqlDataSource1" AllowMultiRowSelection="false"
    AllowMultiRowEdit="false" Skin="Vista" AllowSorting="true" ShowGroupPanel="false">
    <PagerStyle Mode="NextPrevAndNumeric" />
    <HeaderStyle BackColor="#BEBEBE" />
    <ItemStyle Font-Size="11px" />
    <AlternatingItemStyle BackColor="#d6d6d6"  />
    <MasterTableView Width="100%" DataKeyNames="buyer_comment_id" DataSourceID="SqlDataSource1"
        HorizontalAlign="NotSet" AutoGenerateColumns="False" SkinID="Simple" CommandItemDisplay="Top" EditMode="EditForms">
        <CommandItemStyle BackColor="#E1DDDD" />
        <NoRecordsTemplate>
            No comment is available
        </NoRecordsTemplate>
        <CommandItemSettings AddNewRecordText="Add New Comment" />
        <Columns>
            <telerik:GridTemplateColumn HeaderText="Posted By" SortExpression="submit_by_user"
                UniqueName="submit_by_user" DataField="submit_by_user" GroupByExpression="submit_by_user [Posted By] Group By submit_by_user" ItemStyle-Width="200px">
                <ItemTemplate>
                    <div style="font-weight:bold;"> <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/users/user_mouse_over.aspx?i=<%# Eval("submit_by_user_id") %>','270','white','true');">
                                                            <%# Eval("submit_by_user")%></a></div>
                    <div style="color:Gray;"><%# Eval("submit_date")%></div>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn DataField="comment" HeaderText="Comment" SortExpression="comment"
                UniqueName="comment" >
            </telerik:GridBoundColumn>
          
        </Columns>
        <EditFormSettings ColumnNumber="2" EditFormType="Template" EditColumn-ItemStyle-Width="100%">
           
            <FormTemplate>
                <table cellpadding="0" cellspacing="2" border="0" width="838">
                    <tr>
                        <td style="font-weight: bold; padding-top: 10px;" colspan="2">
                            Add New Comment
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">
                            Comment<span id="span_auction_title" runat="server" class="req_star"> *</span>
                        </td>
                        <td class="details">
                            <asp:TextBox ID="txt_comment" runat="server" Text='<%#Bind("comment") %>' Width="400"
                                Height="100" TextMode="MultiLine" />
                            <asp:RequiredFieldValidator ID="rfv_comment" runat="server" Font-Size="10px" ControlToValidate="txt_comment"
                                Display="Static" ErrorMessage=" Comment Required" ValidationGroup="_comment"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <asp:ImageButton ID="img_btn_save_comment" runat="server" ImageUrl="/images/save.gif"
                                ValidationGroup="_comment" CommandName="PerformInsert" />
                            <asp:ImageButton ID="img_btn_cancel" runat="server" ImageUrl="/images/cancel.gif"
                                CausesValidation="false" CommandName="cancel" />
                           &nbsp;
                        </td>
                    </tr>
                </table>
            </FormTemplate>
        </EditFormSettings>
    </MasterTableView>
    <ClientSettings AllowDragToGroup="false">
        <Selecting AllowRowSelect="True"></Selecting>
    </ClientSettings>
    <GroupingSettings ShowUnGroupButton="false" />
</telerik:RadGrid>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
    ProviderName="System.Data.SqlClient" SelectCommand="select A.buyer_comment_id,A.buyer_id,ISNULL(A.comment,'') As comment,A.submit_date,ISNULL(A.submit_by_user_id,0) as submit_by_user_id,ISNULL(ISNULL(U.first_name,'') + ' ' + ISNULL(U.last_name,''),'System') AS submit_by_user 
from tbl_reg_buyer_comments A LEFT JOIN tbl_sec_users U ON A.submit_by_user_id=U.user_id where A.buyer_id=@buyer_id Order by submit_date DESC" InsertCommand="insert INTO tbl_reg_buyer_comments(buyer_id,comment,submit_date,submit_by_user_id) Values (@buyer_id,replace(replace(@comment,CHAR(13)+CHAR(10),'<br/>'),CHAR(10),'<br/>'),GETDATE(),@user_id)">
 <SelectParameters>
        <asp:ControlParameter Name="buyer_id" Type="Int32" ControlID="hid_buyer_id" PropertyName="Value" />
 </SelectParameters>
 <InsertParameters>
    <asp:ControlParameter Name="buyer_id" Type="Int32" ControlID="hid_buyer_id" PropertyName="Value" />
    <asp:Parameter Name="comment" Type="String" />
    <asp:ControlParameter Name="user_id" Type="Int32" ControlID="hid_user_id" PropertyName="Value" />
 </InsertParameters>

</asp:SqlDataSource>
