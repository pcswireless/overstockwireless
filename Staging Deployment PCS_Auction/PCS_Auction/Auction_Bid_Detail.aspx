﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="Auction_Bid_Detail.aspx.vb"
    Inherits="Auction_Bid_Detail" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Auctions</title>
    <link type="text/css" rel="Stylesheet" href="/Style/AuctionBody.css" />
    <script type="text/javascript" src="/pcs_js/thumbnailviewer2.js" defer="defer"></script>
    <script type="text/javascript">
        function Focus(objname, waterMarkText) {
            obj = document.getElementById(objname);
            if (obj.value == waterMarkText) {
                obj.value = "";
                obj.className = "WaterMarkText1";
                if (obj.value == "Response" || obj.value == "" || obj.value == null) {
                    obj.style.color = "black";
                }
            }
        }
        function Blur(objname, waterMarkText) {

            obj = document.getElementById(objname);
            if (obj.value == "") {
                obj.value = waterMarkText;
                //                if (objname != "txtPwd") {
                //                    obj.className = "WaterMarkedTextBox";
                //                }
                //                else {
                obj.className = "WaterMarkedTextBox1";
                //                }
            }
            else {
                obj.className = "WaterMarkText1";
            }

            if (obj.value == "Response" || obj.value == "" || obj.value == null) {
                obj.style.color = "gray";
            }
        }
    </script>
    <script type="text/javascript" src="/Style/countdownpro_expanded.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hid_flang" runat="server" Value="0" />
    <telerik:RadScriptManager ID="ScriptManager" runat="server" />
    <div style="width: 1080px; float: left; padding-left: 10px; margin-top: 10px; padding-right: 10px;
        border: solid 0px red;">
        <%--Top section starts here--%>
        <div class="block_with_border">
            <%--Title block starts here-- ---%>
            <div style="overflow: hidden; border: solid 0px red;">
                <div style="float: left;">
                    <div>
                        <asp:Label ID="lbl_title" runat="server" CssClass="auctionTitle"></asp:Label><br />
                        <asp:Label ID="lbl_sub_title" runat="server" CssClass="auctionSubTitle"></asp:Label>
                        <asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
                        <asp:HiddenField ID="hid_language" runat="server" Value="0" />
                        <asp:HiddenField ID="hid_auction_type_id" runat="server" Value="0" />
                        <asp:HiddenField ID="hid_auction_status" runat="server" Value="C" />
                        <asp:HiddenField ID="hid_buyer_id" runat="server" Value="0" />
                        <asp:HiddenField ID="hid_buyer_user_id" runat="server" Value="0" />
                        <asp:HiddenField ID="hid_rank1_bid_id" runat="server" Value="0" />
                        <asp:HiddenField ID="hid_rank1_bid_amount" runat="server" Value="0" />
                    </div>
                    <div style="padding-top: 6px;" class="AucCode">
                        <asp:Literal ID="lit_code" runat="server"  meta:resourcekey="lit_code"></asp:Literal> :
                        <asp:Label ID="lbl_auction_code" runat="server" Text="A001"></asp:Label>
                    </div>
                </div>
                <div style="float: right;">
                    <span>
                        <asp:Label ID="lbl_auction_status_start" runat="server"></asp:Label></span><br />
                    <span style="color: #2B83CA">
                        <asp:Label ID="lbl_auction_status_end" runat="server"></asp:Label>
                    </span>
                </div>
            </div>
            <%--Title block ends here-- ---%>
            <%---bottom block starts here----%>
            <div style="margin-top: 15px; padding-top: 5px; overflow: hidden;">
                <%-- Image block starts here ---%>
                <div style="float: left; width: 200px;">
                    <div style="padding-top: 30px; vertical-align: middle; width: 166px; height: 164px;
                        border: solid 1px #F3F3F3; text-align: center;">
                        <asp:Literal ID="LTRL_Main_Image" runat="server"></asp:Literal>
                    </div>
                    <div style="width: 166px; text-align: center; padding-top: 8px; border: solid 0px red;">
                        <asp:Literal ID="LTRL_Thumb_Images" runat="server"></asp:Literal>
                    </div>
                </div>
                <%-- Image block ends here ---%>
                <%-- pricing block starts here ---%>
                <div style="float: left; width: 460px; text-align: center;">
                    <div style="text-align: left;">
                        <asp:Panel ID="pnl_price" runat="server">
                            <iframe id="iframe_price" src='' runat="server" scrolling="no" frameborder="0" height="120px"
                                width="460px"></iframe>
                        </asp:Panel>
                        <asp:Literal ID="ltrl_quote_message" runat="server"></asp:Literal>
                    </div>
                    <div style="text-align: left; text-align: justify;">
                        <b><asp:Literal ID="lit_Description" runat="server" meta:resourcekey="lit_description"></asp:Literal> : </b>
                        <asp:Literal ID="lbl_short_desc" runat="server"></asp:Literal>
                    </div>
                    <div style="padding-top: 20px;">
                        <div style="width: 350px; color: #EE0004; font-size: 24px; background-color: #F3F8FC;
                            text-align: center; padding: 10px;">
                            <center>
                                <iframe id="iframe1" src='' scrolling="no" frameborder="0" width="205px" height="23px"
                                    runat="server"></iframe>
                            </center>
                        </div>
                    </div>
                </div>
                <%-- pricing block ends here  ---%>
                <%-- Private block starts here  ---%>
                <div style="float: right; border: solid 0px red; width: 380px;">
                    <asp:HiddenField ID="hid_auto_acceptance_price_private" runat="server" Value="0" />
                    <asp:HiddenField ID="hid_auto_acceptance_price_partial" runat="server" Value="0" />
                    <asp:HiddenField ID="hid_auto_acceptance_quantity" runat="server" Value="0" />
                    <asp:HiddenField runat="server" ID="hid_bynow_amount" Value="0" />
                    <asp:HiddenField runat="server" ID="hid_show_price" Value="0" />
                    <asp:HiddenField runat="server" ID="hid_tradition_amount" Value="0" />
                    <asp:HiddenField runat="server" ID="hid_bidlimit_amount" Value="0" />
                    <asp:HiddenField ID="hid_is_prebid_offer" runat="server" Value="0" />
                    <asp:TextBox ID="txt_test" runat="server" Visible="false"></asp:TextBox>
                    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" MultiPageID="RadMultiPage1"
                        SelectedIndex="0" CssClass="tabStrip" Visible="false">
                        <Tabs>
                            <telerik:RadTab meta:resourcekey="lit_bid_now" runat="server" PageViewID="RadPageView1">
                            </telerik:RadTab>
                            <telerik:RadTab meta:resourcekey="lit_private_offer" runat="server" PageViewID="RadPageView2">
                            </telerik:RadTab>
                            <telerik:RadTab meta:resourcekey="lit_partial_offer" runat="server" PageViewID="RadPageView3">
                            </telerik:RadTab>
                            <telerik:RadTab Text="Buy Now" runat="server" PageViewID="RadPageView4">
                            </telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip>
                    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" RenderSelectedPageOnly="false">
                        <telerik:RadPageView ID="RadPageView1" runat="server">
                            <asp:Panel ID="pnl_buy_now_direct_bid" runat="server" Visible="false">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td valign="top" style="text-align: center; border: solid 1px #D4D4D4">
                                            <center>
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                   <%-- <tr>
                                                        <td style="font-weight: bold; font-size: 12px; padding-top: 15px;" align="center">
                                                            <asp:Literal ID="lit_buy_now_price" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>--%>
                                                    <tr>
                                                        <td align="center" style="padding-top: 15px; padding-bottom: 10px;">
                                                            <asp:ImageButton ID="btn_buy_now_bid" runat="server" AlternateText="Buy Now" ImageUrl="/Images/fend/4.png" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-bottom: 10px;" align="center">
                                                            <asp:Label ID="lbl_msg_buy_now_bid" runat="server" EnableViewState="false" Text="Your bid has been submitted."
                                                                Visible="false" ForeColor="Red"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </center>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnl_trad_proxy_direct_bid" runat="server" Visible="false">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td valign="top" style="text-align: center; border: solid 1px #D4D4D4;">
                                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                <tr>
                                                    <td style="font-weight: bold; font-size: 12px; padding-top: 15px;" align="center">
                                                        <asp:Label ID="lbl_trad_proxy_max_bid_amt_caption" runat="server" Font-Bold="true">Max Bid Amount</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="color: #ACACAC; font-weight: bold; padding-top:10px;" align="center">
                                                        <asp:Label ID="lbl_min_amount_to_bid" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-top: 6px;" align="center">
                                                        <asp:TextBox ID="txt_trad_proxy_max_bid_amount" runat="server" Width="100"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfv_bid_amt" runat="server" ValidationGroup="trad_proxy"
                                                            ControlToValidate="txt_trad_proxy_max_bid_amount" ErrorMessage="*" ForeColor="Red"
                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txt_trad_proxy_max_bid_amount"
                                                            ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="true" ErrorMessage="Invalid"
                                                            runat="server" ValidationGroup="trad_proxy" Display="Dynamic" />
                                                        <asp:CompareValidator ID="comvRG" runat="server" Display="Dynamic" ControlToValidate="txt_trad_proxy_max_bid_amount"
                                                            ErrorMessage="<br>Invalid" Type="Double" ValidationGroup="trad_proxy" Operator="GreaterThanEqual"></asp:CompareValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-bottom: 10px; padding-top: 6px;" align="center">
                                                        <asp:ImageButton ID="btn_trad_proxy_bid" runat="server" ImageUrl="/Images/fend/5.png"
                                                            AlternateText="Bid Now" ValidationGroup="trad_proxy" />
                                                        <br />
                                                        <asp:Label ID="lbl_msg_trad_proxy_bid" runat="server" ForeColor="Red" EnableViewState="false"
                                                            Text="Your bid has been submitted" Visible="false"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnl_request_for_quote" runat="server" Visible="false">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td valign="top" style="text-align: center; border: solid 1px #D4D4D4">
                                            <center>
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td align="center" style="font-weight: bold; font-size: 12px; padding-top: 10px;">
                                                            <asp:Literal ID="lit_quotation" runat="server" meta:resourcekey="lit_quotation"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding-top: 10px;">
                                                            <asp:TextBox ID="txt_quote" runat="server" TextMode="MultiLine" Width="200" Height="50"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_quote"
                                                                ErrorMessage="*" ForeColor="Red" ValidationGroup="quote" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding-top: 10px; font-weight: bold; font-size: 12px;">
                                                            <asp:Literal ID="lit_attach" runat="server" meta:resourcekey="lit_attach"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding-top: 6px;">
                                                            <asp:FileUpload ID="fl_quote_file" runat="server" size="17" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding-top: 10px; padding-bottom: 10px;">
                                                            <asp:ImageButton ID="btn_request_quote" runat="server" AlternateText="Send Offer"
                                                                ImageUrl="/Images/fend/3.png" ValidationGroup="quote" />
                                                            <br />
                                                            <asp:Label ID="lbl_msg_quotation" runat="server" ForeColor="Red" EnableViewState="false"
                                                                Text="Your offer has been sent." Visible="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </center>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView2" runat="server">
                            <asp:Panel ID="pnl_buy_now_private_bid" runat="server" Visible="false">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td valign="top" style="text-align: center; border: solid 1px #D4D4D4">
                                            <center>
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td style="font-weight: bold; font-size: 12px; padding-top: 15px;" align="center">
                                                            <asp:Literal ID="lit_price" runat="server" meta:resourcekey="lit_price"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding-top: 10px;">
                                                            <asp:TextBox ID="txt_private_buy_now_auto_accept_price" runat="server" Width="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_private_buy_now_auto_accept_price"
                                                                ErrorMessage="Required" ForeColor="Red" ValidationGroup="buy_now_private_offer"
                                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" ControlToValidate="txt_private_buy_now_auto_accept_price"
                                                                ValidationExpression="[-+]?[0-9]*\.?[0-9]*" ErrorMessage="<br />Invalid" runat="server"
                                                                ValidationGroup="buy_now_private_offer" Display="Dynamic" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-bottom: 10px; padding-top: 10px;" align="center">
                                                            <asp:ImageButton ID="btn_buy_now_private_offer" runat="server" AlternateText="Send Private Offer"
                                                                ValidationGroup="buy_now_private_offer" ImageUrl="/Images/fend/2.png" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-bottom: 10px;" align="center">
                                                            <asp:Label ID="lbl_msg_buy_now_private" runat="server" EnableViewState="false" ForeColor="Red"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </center>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnl_trad_proxy_private_bid" runat="server" Visible="false">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td valign="top" style="text-align: center; border: solid 1px #D4D4D4">
                                            <center>
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td style="font-weight: bold; font-size: 12px; padding-top: 15px;" align="center">
                                                            <asp:Literal ID="lit_price1" runat="server" meta:resourcekey="lit_price"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding-top: 10px;">
                                                            <asp:TextBox ID="txt_trad_proxy_private_auto_accept_price" runat="server" Width="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_trad_proxy_private_auto_accept_price"
                                                                ErrorMessage="*" ForeColor="Red" ValidationGroup="trad_proxy_private_offer" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txt_trad_proxy_private_auto_accept_price"
                                                                ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="true" Display="Dynamic"
                                                                ErrorMessage="<br />Invalid" runat="server" ValidationGroup="trad_proxy_private_offer" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-bottom: 10px; padding-top: 6px;" align="center">
                                                            <asp:ImageButton ID="btn_trad_proxy_private_offer" runat="server" AlternateText="Send Private Offer"
                                                                ValidationGroup="trad_proxy_private_offer" ImageUrl="/Images/fend/2.png" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-bottom: 10px;" align="center">
                                                            <asp:Label ID="lbl_msg_trad_proxy_private" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </center>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="radPageView3" runat="server">
                            <asp:Panel ID="pnl_buy_now_partial_bid" runat="server" Visible="false">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td valign="top" style="text-align: center; border: solid 1px #D4D4D4">
                                            <center>
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td style="font-weight: bold; font-size: 12px; padding-top: 15px;" align="center">
                                                            <asp:Literal ID="lit_price2" runat="server" meta:resourcekey="lit_price"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding-top: 10px;">
                                                            <asp:TextBox ID="txt_partial_buy_now_auto_accept_price" runat="server" Width="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txt_partial_buy_now_auto_accept_price"
                                                                ErrorMessage="*" ForeColor="Red" ValidationGroup="buy_now_partial_offer" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ControlToValidate="txt_partial_buy_now_auto_accept_price"
                                                                ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="true" Display="Dynamic"
                                                                ErrorMessage="<br />Invalid" runat="server" ValidationGroup="buy_now_partial_offer" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold; font-size: 12px; padding-top: 15px;" align="center">
                                                            <asp:Literal ID="lit_quantity" runat="server" meta:resourcekey="lit_quantity"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-bottom: 10px;" align="center">
                                                            <asp:TextBox ID="txt_partial_buy_now_auto_accept_qty" runat="server" Width="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txt_partial_buy_now_auto_accept_qty"
                                                                ErrorMessage="*" ForeColor="Red" ValidationGroup="buy_now_partial_offer" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ControlToValidate="txt_partial_buy_now_auto_accept_qty"
                                                                ValidationExpression="[0-9]*" EnableClientScript="true" ErrorMessage="Invalid"
                                                                Display="Dynamic" runat="server" ValidationGroup="buy_now_partial_offer" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-bottom: 10px; padding-top: 6px;" align="center">
                                                            <asp:ImageButton ID="btn_buy_now_partial_offer" runat="server" AlternateText="Send Partial Offer"
                                                                ValidationGroup="buy_now_partial_offer" ImageUrl="/Images/fend/1.png" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-bottom: 10px;" align="center">
                                                            <asp:Label ID="lbl_msg_buy_now_partial_offer" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </center>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnl_trad_proxy_partial_bid" runat="server" Visible="false">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td valign="top" style="text-align: center; border: solid 1px #D4D4D4">
                                            <center>
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td style="font-weight: bold; font-size: 12px; padding-top: 15px;" align="center">
                                                            <asp:Literal ID="lit_price3" runat="server" meta:resourcekey="lit_price"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding-top: 10px;" >
                                                            <asp:TextBox ID="txt_trad_proxy_partial_auto_accept_price" runat="server" Width="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txt_trad_proxy_partial_auto_accept_price"
                                                                ErrorMessage="*" ForeColor="Red" ValidationGroup="trad_proxy_partial_offer" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txt_trad_proxy_partial_auto_accept_price"
                                                                ValidationExpression="[-+]?[0-9]*\.?[0-9]*" EnableClientScript="true" ErrorMessage="<br />Invalid"
                                                                runat="server" ValidationGroup="trad_proxy_partial_offer" Display="Dynamic" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold; font-size: 12px; padding-top: 6px;" align="center">
                                                            <asp:Literal ID="lit_quantity1" runat="server" meta:resourcekey="lit_quantity"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 10px;" align="center">
                                                            <asp:TextBox ID="txt_trad_proxy_partial_auto_accept_qty" runat="server" Width="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txt_trad_proxy_partial_auto_accept_qty"
                                                                ErrorMessage="*" ForeColor="Red" ValidationGroup="trad_proxy_partial_offer" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ControlToValidate="txt_trad_proxy_partial_auto_accept_qty"
                                                                ValidationExpression="[0-9]*" EnableClientScript="true" ErrorMessage="<br>Invalid"
                                                                runat="server" ValidationGroup="trad_proxy_partial_offer" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-bottom: 10px; padding-top: 10px;" align="center">
                                                            <asp:ImageButton ID="btn_trad_proxy_partial" runat="server" AlternateText="Send Partial Offer"
                                                                ValidationGroup="trad_proxy_partial_offer" ImageUrl="/Images/fend/1.png" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-bottom: 10px;" align="center">
                                                            <asp:Label ID="lbl_msg_trad_proxy_partial" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </center>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="radPageView4" runat="server">
                            <asp:Panel ID="pnl_trad_proxy_buy_now_bid" runat="server" Visible="false">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td valign="top" style="text-align: center; border: solid 1px #D4D4D4">
                                            <center>
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td style="font-weight: bold; font-size: 12px; padding-top: 15px;" align="center">
                                                            <asp:Literal ID="lit_trad_proxy_buy_now_price" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 20px; padding-bottom: 10px;" align="center">
                                                            <asp:ImageButton ID="btn_trad_proxy_buy_now" runat="server" AlternateText="Buy Now"
                                                                ImageUrl="/Images/fend/4.png" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-bottom: 10px;" align="center">
                                                            <asp:Label ID="lbl_msg_trad_proxy_buy_now" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </center>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
                </div>
                <%-- Private block ends here  ---%>
            </div>
        </div>
        <%--Top section ends here--%>
        <%-- User personal actions displays here ---%>
        <div>
            <asp:Panel ID="pnl_buyer_personal" runat="server">
            </asp:Panel>
        </div>
        <%-- User personal actions ends here ---%>
        <br />
        <div class="detailsSubHeading" style="font-size: 13px;">
            <asp:Literal ID="lit_item_desc" runat="server" meta:resourcekey="lit_item_desc"></asp:Literal>
        </div>
        <div style="padding-top: 6px; overflow: hidden;">
            <asp:Repeater ID="RadGrid_items" runat="server" DataSourceID="sqlDataSource1">
                <ItemTemplate>
                    <div style="clear: both;">
                        <div style="padding: 10px 0px 5px 0px;">
                            <b>
                                <%# Eval("manufacturer") & " " & Eval("name")%>
                            </b>&nbsp;<span class="auctionPartNo">
                                <asp:Literal ID="lit_part" runat="server" meta:resourcekey="lit_part"></asp:Literal> <%# Eval("part_no")%></span>
                        </div>
                        <div class="divItemDesc">
                            <table width="100%">
                                <tr>
                                    <td style="padding-right: 15px;">
                                        <b><asp:Literal ID="lit_description1" runat="server" meta:resourcekey="lit_description"></asp:Literal></b>
                                        <div class="detailsDesc">
                                            <%# Eval("description")%>
                                        </div>
                                    </td>
                                    <td style="width: 350px; padding-top: 5px;" valign="top">
                                        <div class="itmC1">
                                        </div>
                                        <div class="itmC2">
                                            <div style="height: 17px;">
                                                <b><asp:Literal ID="lit_stock" runat="server" meta:resourcekey="lit_stock"></asp:Literal> : </b>
                                                <%# Eval("stock_condition")%>
                                            </div>
                                            <div style="height: 17px;">
                                                <b><asp:Literal ID="lit_location" runat="server" meta:resourcekey="lit_location"></asp:Literal> : </b>
                                                <%# Eval("location")%>
                                            </div>
                                            <div style="height: 17px;">
                                                <b><asp:Literal ID="lit_packaging" runat="server" meta:resourcekey="lit_packaging"></asp:Literal> : </b>
                                                <%# Eval("packaging")%>
                                            </div>
                                            <div style="height: 17px;">
                                                <b><asp:Literal ID="lit_quantity2" runat="server" meta:resourcekey="lit_quantity2"></asp:Literal> : </b>
                                                <%# Eval("quantity")%>
                                            </div>
                                        </div>
                                        <div class="itmC3">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <asp:SqlDataSource ID="sqlDataSource1" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                ProviderName="System.Data.SqlClient" runat="server" SelectCommand="select product_item_id,isnull(M.description,'--') as manufacturer,isnull(case @lang when 'es-ES' then part_no_s when 'fr-FR' then part_no_f when 'zh-CN' then part_no_c else part_no end,'--') as part_no,isnull(case @lang when 'es-ES' then description_s when 'fr-FR' then description_f when 'zh-CN' then description_c else I.description end,'--') as description,isnull(case @lang when 'es-ES' then I.name_s when 'fr-FR' then I.name_f when 'zh-CN' then I.name_c else I.name end,'--') as name,isnull(L.name,'--') as location,isnull(quantity,0) as quantity,isnull(C.name,'--') as stock_condition,isnull(P.name,'--') as packaging from tbl_auction_product_items I left join tbl_master_manufacturers M on I.manufacturer_id=M.manufacturer_id left join tbl_master_stock_locations L on I.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions C on I.stock_condition_id=C.stock_condition_id left join tbl_master_packaging P on I.packaging_id=P.packaging_id where auction_id=@auction_id">
                <SelectParameters>
                    <asp:ControlParameter ControlID="hid_auction_id" Type="Int32" PropertyName="Value"
                        Name="auction_id" />
                    <asp:ControlParameter ControlID="hid_flang" Type="String" PropertyName="Value" Name="lang" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
        <div class="detailsSectionSep">
        </div>
        <div class="detailsSubHeading">
            <asp:Literal ID="lit_product_desc" runat="server" meta:resourcekey="lit_product_desc"></asp:Literal>
        </div>
        <div class="detailsDesc">
            <asp:Literal ID="ltrl_description" runat="server"></asp:Literal>
        </div>
        <div class="detailsSectionSep">
        </div>
        <div class="detailsSubHeading">
            <asp:Literal ID="lit_payment_term" runat="server" meta:resourcekey="lit_payment_term"></asp:Literal>
        </div>
        <asp:Panel ID="pnl_terms" runat="server" CssClass="pnlGray" ScrollBars="Vertical">
            <div class="detailsDesc">
                <asp:Literal ID="lbl_payment_term" runat="server"></asp:Literal>
            </div>
        </asp:Panel>
        <div class="view_more">
            <asp:Label ID="lbl_payment_term_attachment" runat="server"></asp:Label>
        </div>
        <div class="detailsSectionSep">
        </div>
        <div class="detailsSubHeading">
            <asp:Literal ID="lit_shipping_term" runat="server" meta:resourcekey="lit_shipping_term"></asp:Literal>
        </div>
        <asp:Panel ID="Panel2" runat="server" CssClass="pnlGray" ScrollBars="Vertical">
            <div class="detailsDesc">
                <asp:Literal ID="lbl_shipping_term" runat="server"></asp:Literal>
            </div>
        </asp:Panel>
        <div class="view_more">
            <asp:Label ID="lbl_shipping_term_attachment" runat="server"></asp:Label>
        </div>
        <div class="detailsSectionSep">
        </div>
        <div class="detailsSubHeading">
            <asp:Literal ID="lit_other_term" runat="server" meta:resourcekey="lit_other_term"></asp:Literal>
        </div>
        <asp:Panel ID="Panel3" runat="server" CssClass="pnlGray" ScrollBars="Vertical">
            <div class="detailsDesc">
                <asp:Literal ID="lbl_other_terms" runat="server"></asp:Literal>
            </div>
        </asp:Panel>
        <div class="view_more">
            <asp:Label ID="lbl_other_term_attachment" runat="server"></asp:Label>
        </div>
        <div class="termLink">
            <asp:Literal ID="lbl_product_attachment" runat="server"></asp:Literal>
            <asp:Literal ID="lbl_tax_details" runat="server"></asp:Literal>
        </div>
        <div class="detailsSectionSep">
        </div>
        <%--- Product details ends here -----%>
        <%--- After launch starts here -----%>
        <div style="width: 100%; padding-top: 10px; overflow:hidden;">
            <asp:Panel ID="pnl_after_launch_message" runat="server" Visible="false" Width="100%">
                <div class="detailsSubHeading">
                    <asp:Literal ID="lit_after_launch" runat="server" meta:resourcekey="lit_after_launch"></asp:Literal>
                </div>
                <div class="divItemDesc">
                    <asp:Literal ID="lit_after_launch_message" runat="server"></asp:Literal>
                </div>
            </asp:Panel>
        </div>
        <%--- After launch ends here -----%>
        <%--- bid history starts here -----%>
        <div style="width: 100%; padding-top: 20px;">
            <asp:Panel ID="pnl_history" runat="server" Visible="false" Width="100%">
                <table cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #FE9901;
                    width: 330px;">
                    <tr class="trHeader">
                        <td style="padding: 5px 0px 5px 10px; color: White; font-weight: bold;">
                            <asp:Literal ID="lit_bid_history" runat="server" meta:resourcekey="lit_bid_history"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px 0px 5px 1px; height: 300px;" valign="top">
                            <asp:Panel ID="Panel1" runat="server" Height="300" ScrollBars="Vertical">
                                <asp:Repeater ID="rep_bid_history" runat="server" DataSourceID="BidHistory_SqlDataSource">
                                    <ItemTemplate>
                                        <div style="clear: both;">
                                            <div style="float: left; width: 180px; padding: 5px 0px 5px 10px;">
                                                <b>
                                                    <%# Eval("bidder")%>
                                                </b>
                                                <br />
                                                <span class="auctionSubTitle">
                                                    <%# Eval("email")%></span>
                                            </div>
                                            <div style="float: left; width: 100px; padding: 5px 0px 5px 10px; color: #006FD5;">
                                                <b>
                                                    <%# "$" & FormatNumber(Eval("bid_amount"), 2)%></b>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <div style="background-color: #F5F5F5; overflow: hidden; clear: both;">
                                            <div style="float: left; width: 180px; padding: 5px 0px 5px 10px;">
                                                <b>
                                                    <%# Eval("bidder")%>
                                                </b>
                                                <br />
                                                <span class="auctionSubTitle">
                                                    <%# Eval("email")%></span>
                                            </div>
                                            <div style="float: left; width: 100px; padding: 5px 0px 5px 10px; color: #006FD5;">
                                                <b>
                                                    <%# "$" & FormatNumber(Eval("bid_amount"), 2)%></b>
                                            </div>
                                        </div>
                                    </AlternatingItemTemplate>
                                </asp:Repeater>
                                <asp:SqlDataSource ID="BidHistory_SqlDataSource" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT A.bid_amount, A.bid_type, (A1.first_name + ' ' +A1.last_name) as bidder,convert(varchar,bid_date, 120) as bid_date,A1.email FROM tbl_auction_bids A WITH (NOLOCK) inner join tbl_reg_buyer_users A1 on A.buyer_user_id=A1.buyer_user_id where A.auction_id=@auction_id order by A.bid_id desc"
                                    runat="server">
                                    <SelectParameters>
                                        <asp:QueryStringParameter Name="auction_id" QueryStringField="i" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <%--- bid history ends here -----%>
        <%--- bid comments starts here -----%>
        <br clear="all" />
        <asp:Panel ID="pnl_after_launch" runat="server" Visible="false">
            <div style="padding-top: 15px;">
                <div class="block_with_border">
                    <div class="detailsSubHeading">
                       <asp:Literal ID="lit_ask_question" runat="server" meta:resourcekey="lit_ask_question"></asp:Literal> 
                    </div>
                    <div class="grBackDescription" style="padding-left: 2px; margin-top: 10px; margin-bottom: 10px;
                        overflow: hidden;">
                        <div style="overflow: hidden; padding: 10px; float: left;">
                            <asp:TextBox runat="server" ID="txt_thread_title" Height="64" Width="868" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfv_create_thread" runat="server" ErrorMessage="*"
                                ForeColor="red" Display="Static" ControlToValidate="txt_thread_title" ValidationGroup="create_thread"></asp:RequiredFieldValidator>
                        </div>
                        <div style="float: left; padding-left: 10px;margin-top: 42px;">
                            <asp:ImageButton ID="img_btn_create_thread" runat="server" AlternateText="Create" ImageUrl="/Images/fend/send.png"
                                ValidationGroup="create_thread" />
                        </div>
                    </div>
                    <div class="detailsSubHeading">
                        <asp:Literal ID="lit_your_query" runat="server" meta:resourcekey="lit_your_query"></asp:Literal>
                    </div>
                    <asp:Repeater ID="rep_after_queries" runat="server">
                                                                <ItemTemplate>
                                                                    <div class="qryTitle">
                                                                        <%# Container.ItemIndex + 1 & ". " & Eval("title")%>
                                                                    </div>
                                                                    <div class="qryAlt" style="padding-left: 16px;">
                                                                     <%--Eval("company_name") & ", " & Eval("state") & --%>   <%# "on " & Eval("submit_date")%>
                                                                    </div>
                                                                    <div style="padding: 20px;">
                                                                        <asp:Repeater ID="rep_inner_after_queries" runat="server">
                                                                            <ItemTemplate>
                                                                                <div class="qryDesc">
                                                                                   <img src='<%# IIF(Eval("image_path")<>"","/upload/users/" & Eval("user_id") & "/" & Eval("image_path"),"/images/buyer_icon.gif") %>'
                                                            alt="" style="float:left;padding:2px;height:23px;width:22px;" /> <%#Eval("message") %>
                                                                                </div>
                                                                                <div class="qryAlt">
                                                                                    <%# IIf(Eval("buyer_title") = "", Eval("title"), Eval("buyer_title"))%> <%# IIf(Eval("first_name") = "", Eval("buyer_first_name") & " " & Eval("buyer_last_name"), Eval("first_name") & " " & Eval("last_name"))%>,
                                                                                    <%# Eval("submit_date")%>
                                                                                </div>
                                                                                <br />
                                                                                <br />
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                        
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                </div>
            </div>
        </asp:Panel>
        <%--- bid comments ends here -----%>
    </div>
    <asp:Button runat="server" ID="btnModalPopUp" Style="display: none" />
    <ajax:ModalPopupExtender ID="modalPopUpExtender1" runat="server" TargetControlID="btnModalPopUp"
        PopupControlID="pnlPopUp" BackgroundCssClass="modalBackground" CancelControlID="img_btnCancel">
    </ajax:ModalPopupExtender>
    <asp:Panel runat="Server" ID="pnlPopUp" Height="200px" Width="350px" BackColor="#ffffff"
        CssClass="confirm-dialog" Style="display: none; border: solid 1px #FF9900;">
        <asp:Label runat="server" Visible="false" ID="lbl_command"></asp:Label>
        <div>
            <asp:Panel runat="server" ID="pnl_login_content" Visible="true" DefaultButton="img_btnsend">
                <center>
                    <div style="background-color: #FF9900; height: 30px;">
                        <div style="font-size: 14px; font-weight: bold; padding-top: 6px;">
                            <asp:Literal ID="lit_login" runat="server" meta:resourcekey="lit_login"></asp:Literal></div>
                    </div>
                    <div style="margin: 10px; overflow: hidden;">
                        <div style="float: left; font-weight: bold; width: 120px; padding-top: 3px;">
                            <asp:Literal ID="lit_user_name" runat="server" meta:resourcekey="lit_user_name"></asp:Literal>:</div>
                        <div style="float: left;">
                            <asp:TextBox runat="server" ID="txt_login_name" Width="154" CssClass="TextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFUserName" runat="server" ControlToValidate="txt_login_name"
                                ErrorMessage="*" Display="Dynamic" ValidationGroup="login">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="margin: 10px; overflow: hidden;">
                        <div style="float: left; font-weight: bold; width: 120px; padding-top: 3px;">
                            <asp:Literal ID="lit_password" runat="server" meta:resourcekey="lit_password"></asp:Literal>:</div>
                        <div style="float: left;">
                            <asp:TextBox runat="server" ID="txt_login_pwd" Width="154" CssClass="TextBox" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFPassword" runat="server" ControlToValidate="txt_login_pwd"
                                ErrorMessage="*" Display="Dynamic" ValidationGroup="login">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="margin: 10px; overflow: hidden;">
                        <asp:Label ID="lbl_error" runat="server" Text="Invalid User Name/Password" Visible="false"
                            EnableViewState="false" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="margin: 10px; overflow: hidden; text-align: center; padding-left: 120px;">
                        <div style="float: left;">
                            <asp:ImageButton runat="server" AlternateText="Send" ID="img_btnsend" ImageUrl="/Images/login.png"
                                ValidationGroup="login" /></div>
                        <div style="float: left; padding-left: 2px;">
                            <asp:ImageButton ID="img_btnCancel" runat="server" AlternateText="Cancel" ImageUrl="/Images/cancel.png" /></div>
                    </div>
                </center>
            </asp:Panel>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
