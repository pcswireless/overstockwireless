﻿
Partial Class AuctionConfirmation_Old
    Inherits System.Web.UI.Page

    Public buy_id As Int32 = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            lit_date.Text = DateTime.Now.ToString("MM/dd/yyyy")
            bindAuction()
            bindToAddress()
            bindItems()
            select_shipping_option()
        End If
    End Sub

    Private Sub bindAuction()
        Dim qry As String = "SELECT A.auction_id, " & _
                "ISNULL(A.code, '') AS code," & _                "ISNULL(A.title, '') AS title," & _                "ISNULL(B.name,'') as stock_condition," & _                "ISNULL(C.name,'') as package_condition" & _                " FROM tbl_auctions A WITH (NOLOCK) LEFT JOIN tbl_master_stock_conditions B ON A.stock_condition_id=B.stock_condition_id LEFT JOIN tbl_master_packaging C ON A.packaging_id=C.packaging_id WHERE A.auction_id = " & Request.QueryString.Get("i")

        Dim dt As DataTable = New DataTable()
        dt = SqlHelper.ExecuteDatatable(qry)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lit_auc_code.Text = .Item("code")
                lit_auc_name.Text = .Item("title")
                lit_condition.Text = .Item("stock_condition")
                lit_auc_packaging.Text = .Item("package_condition")
            End With
        End If
    End Sub

    Private Sub bindToAddress()

        Dim sqlStr As String = "SELECT "
        sqlStr = sqlStr & "ISNULL(A.company_name, '') AS company_name,"        sqlStr = sqlStr & "ISNULL(A.contact_title, '') AS contact_title,"        sqlStr = sqlStr & "ISNULL(A.contact_first_name, '') AS contact_first_name,"        sqlStr = sqlStr & "ISNULL(A.contact_last_name, '') AS contact_last_name,"        sqlStr = sqlStr & "ISNULL(A.email, '') AS email,"        sqlStr = sqlStr & "ISNULL(A.website, '') AS website,"        sqlStr = sqlStr & "ISNULL(A.phone, '') AS phone,"        sqlStr = sqlStr & "ISNULL(A.mobile, '') AS mobile,"        sqlStr = sqlStr & "ISNULL(A.fax, '') AS fax,"        sqlStr = sqlStr & "ISNULL(A.address1, '') AS address1,"        sqlStr = sqlStr & "ISNULL(A.address2, '') AS address2,"        sqlStr = sqlStr & "ISNULL(A.city, '') AS city,"        sqlStr = sqlStr & "ISNULL(A.state_id, 0) AS state_id,"        sqlStr = sqlStr & "ISNULL(A.zip, '') AS zip,"        sqlStr = sqlStr & "ISNULL(A.country_id, 0) AS country_id,"        sqlStr = sqlStr & "ISNULL(A.state_text, '') AS state_text,"        sqlStr = sqlStr & "rtrim(ISNULL(U.first_name,'') + ' ' + ISNULL(U.last_name,'')) as sales_rep"        sqlStr = sqlStr & " FROM"
        sqlStr = sqlStr & " tbl_reg_buyers A WITH (NOLOCK) left join tbl_reg_sale_rep_buyer_mapping M on A.buyer_id=M.buyer_id inner join tbl_sec_users U on M.user_id=U.user_id"        sqlStr = sqlStr & " WHERE"        sqlStr = sqlStr & " A.buyer_id =" & CommonCode.Fetch_Cookie_Shared("buyer_id")

        Dim dtTable As DataTable = SqlHelper.ExecuteDatatable(sqlStr)
        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)
                sqlStr = .Item("contact_first_name") & IIf(.Item("contact_last_name").ToString() <> "", " " & .Item("contact_last_name"), "")
                sqlStr = sqlStr & "<br />" & .Item("company_name")
                sqlStr = sqlStr & "<br />" & .Item("address1") & " " & .Item("address2")
                sqlStr = sqlStr & "<br />" & .Item("city") & ", " & .Item("state_text") & " " & .Item("zip")
                If .Item("phone") <> "" Then sqlStr = sqlStr & "<br />Phone: " & .Item("phone")
                If .Item("fax") <> "" Then sqlStr = sqlStr & " Fax: " & .Item("fax")
                lit_bill_to.Text = sqlStr
                lit_sales_rep.Text = .Item("sales_rep")
            End With
        End If

    End Sub

    Private Sub bindItems()
        Dim strQry As String = "SELECT A.product_item_id,"
        strQry = strQry & "ISNULL(A.part_no, '') AS part_no,"        strQry = strQry & "ISNULL(A.packaging_id, 0) AS packaging_id,"        strQry = strQry & "ISNULL(A.name, '') AS name,"        strQry = strQry & "ISNULL(A.quantity, 0) * " & Request.QueryString.Get("q") & " AS quantity,"        strQry = strQry & "ISNULL(A.upc, '') AS upc,"        strQry = strQry & "ISNULL(A.sku, '') AS sku,"        strQry = strQry & "ISNULL(A.estimated_msrp, 0) AS estimated_msrp,"        strQry = strQry & "ISNULL(A.total_msrp, 0) * " & Request.QueryString.Get("q") & " AS total_msrp"
        strQry = strQry & " FROM "
        strQry = strQry & " tbl_auction_product_items A"
        strQry = strQry & " WHERE A.auction_id=" & Request.QueryString.Get("i")

        rpt_device.DataSource = SqlHelper.ExecuteDatatable(strQry)
        rpt_device.DataBind()


    End Sub

    Public Sub select_shipping_option()

        Dim shipping_preference As String = ""
        Dim last_shipping_option As String = ""
        Dim Str As String
        Dim dt As New DataTable
        Str = "select ISNULL(shipping_type,'') AS shipping_type,ISNULL(shipping_account_number,'') AS shipping_account_number,ISNULL(shipping_preference,'') AS shipping_preference,ISNULL(last_shipping_option,'') AS last_shipping_option from tbl_reg_buyers WITH (NOLOCK) where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id")
        dt = SqlHelper.ExecuteDatatable(Str)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                txt_shipping_type.Text = .Item("shipping_type")
                txt_shipping_account_number.Text = .Item("shipping_account_number")
                shipping_preference = .Item("shipping_preference")
                last_shipping_option = .Item("last_shipping_option")
            End With
        End If
        dt.Dispose()

        Str = "select A.use_pcs_shipping, A.use_your_shipping, isnull(S.name,'') as name, isnull(S.city,'') as city, isnull(S.state,'') as state from tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations S on S.stock_location_id=A.stock_location_id where A.auction_id=" & Request.QueryString.Get("i")
        dt = SqlHelper.ExecuteDatatable(Str)
        Dim stock_location = IIf(dt.Rows(0)("name") = "", "", dt.Rows(0)("name")) & IIf(dt.Rows(0)("city") = "", "", "</br>" & dt.Rows(0)("city")) & IIf(dt.Rows(0)("state") = "", "", ", " & dt.Rows(0)("state"))
        If dt.Rows.Count > 0 Then
            If dt.Rows(0)("use_pcs_shipping") = True And dt.Rows(0)("use_your_shipping") = True Then

                getFedExRate()
                rdo_option.Visible = True
                If rdo_option.Items.FindByValue(last_shipping_option) IsNot Nothing Then
                    rdo_option.ClearSelection()
                    rdo_option.Items.FindByValue(last_shipping_option).Selected = True
                Else
                    rdo_option.Items.FindByValue("2").Selected = True
                End If

                If rdo_option.SelectedValue = 1 Then
                    tbl_user_own.Visible = True
                    tbl_user_ours.Visible = False

                Else
                    tbl_user_own.Visible = False
                    tbl_user_ours.Visible = True
                    If rdo_fedex.Items.FindByValue(shipping_preference) IsNot Nothing Then
                        rdo_fedex.ClearSelection()
                        rdo_fedex.Items.FindByValue(shipping_preference).Selected = True
                    End If

                End If

            Else
                If dt.Rows(0)("use_your_shipping") = True Then
                    'use your only
                    rdo_option.Items.FindByValue("1").Selected = True
                    rdo_option.Visible = False
                    tbl_user_ours.Visible = False
                    tbl_user_own.Visible = True

                Else
                    'use PCS only
                    getFedExRate()
                    rdo_option.Items.FindByValue("2").Selected = True
                    If rdo_fedex.Items.FindByValue(shipping_preference) IsNot Nothing Then
                        rdo_fedex.ClearSelection()
                        rdo_fedex.Items.FindByValue(shipping_preference).Selected = True
                    End If
                    rdo_option.Visible = False
                    tbl_user_ours.Visible = True
                    tbl_user_own.Visible = False

                End If
            End If
        End If

        If stock_location <> "" Then
            table_stock.Visible = True
            lbl_stock_location.Text = stock_location
        Else
            table_stock.Visible = False
        End If

    End Sub
    Private Sub getFedExRate()
        Try
            Dim dt As New DataTable()

            If Request.QueryString.Get("q") <> "1" Then


                Dim obj As New FedAllRateServices
                Dim objreq As New FedexRateServices.RateRequest
                Dim objrep As New FedexRateServices.RateReply
                Dim Origin_StreetLines, Origin_City, Origin_StateOrProvinceCode, Origin_PostalCode, Origin_CountryCode, Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, Destination_PostalCode, Destination_CountryCode, PackageLineItems_SequenceNumber, PackageLineItems_GroupPackageCount, PackageLineItems_weight_value, PackageLineItems_Dimensions_length, PackageLineItems_Dimensions_width, PackageLineItems_Dimensions_height As String

                Origin_StreetLines = SqlHelper.of_FetchKey("fedex_source_street")
                Origin_City = SqlHelper.of_FetchKey("fedex_source_city")
                Origin_StateOrProvinceCode = SqlHelper.of_FetchKey("fedex_source_state")
                Origin_PostalCode = SqlHelper.of_FetchKey("fedex_source_zip")
                Origin_CountryCode = SqlHelper.of_FetchKey("fedex_source_country_code")

                dt = SqlHelper.ExecuteDatatable("SELECT ISNULL(A.address1, '') + ' ' + ISNULL(A.address2, '')  AS address, ISNULL(A.city, '') AS city,ISNULL(A.state_text, 0) AS state_text,ISNULL(A.zip, '') AS zip,ISNULL(C.CODE, 0) AS country_code FROM tbl_reg_buyers A WITH (NOLOCK) INNER JOIN tbl_master_countries C ON A.country_id=C.country_id WHERE A.buyer_id = " & CommonCode.Fetch_Cookie_Shared("buyer_id"))
                If dt.Rows.Count > 0 Then

                    With dt.Rows(0)
                        Destination_StreetLines = .Item("address").ToString().Trim()
                        Destination_City = .Item("city")
                        Destination_StateOrProvinceCode = .Item("state_text")
                        Destination_PostalCode = .Item("zip")
                        Destination_CountryCode = .Item("country_code")
                    End With

                Else

                    Destination_StreetLines = ""
                    Destination_City = ""
                    Destination_StateOrProvinceCode = ""
                    Destination_PostalCode = ""
                    Destination_CountryCode = ""

                End If
                dt.Dispose()

                PackageLineItems_SequenceNumber = "1"
                PackageLineItems_GroupPackageCount = "1"
                dt = SqlHelper.ExecuteDatatable("SELECT ISNULL(A.weight, 0) AS weight,ISNULL(A.ship_lenght, 0) AS ship_lenght,ISNULL(A.ship_width, 0) AS ship_width,ISNULL(A.ship_height, 0) AS ship_height FROM tbl_auctions A WITH (NOLOCK) WHERE A.auction_id =" & Request.QueryString.Get("i"))
                If dt.Rows.Count > 0 Then
                    With dt.Rows(0)
                        PackageLineItems_weight_value = .Item("weight") * Request.QueryString.Get("q")
                    End With
                Else
                    PackageLineItems_weight_value = ""
                End If
                dt.Dispose()

                dt = obj.GetFedexShippingOptions(Origin_StreetLines, Origin_City, Origin_StateOrProvinceCode, Origin_PostalCode, Origin_CountryCode, Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, Destination_PostalCode, Destination_CountryCode, PackageLineItems_SequenceNumber, PackageLineItems_GroupPackageCount, PackageLineItems_weight_value)

            Else
                dt = SqlHelper.ExecuteDatatable("SELECT ISNULL(A.shipping_amount, 0) AS Rate,ISNULL(A.shipping_options, '') AS ServiceType FROM tbl_auction_fedex_rates A WITH (NOLOCK) WHERE A.auction_id=" & Request.QueryString.Get("i") & " and A.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id"))
            End If
            If dt.Rows.Count > 0 Then
                Dim dv As DataView
                dv = dt.DefaultView
                dv.Sort = "Rate asc"
                rdo_fedex.Items.Clear()
                For i As Integer = 0 To dv.Count - 1
                    rdo_fedex.Items.Insert(i, New ListItem(dv.Item(i)("ServiceType").ToString().Replace("_", " ") & " <b>" & FormatCurrency(dv.Item(i)("Rate"), 2) & "</b>", i))
                Next
            End If
        Catch ex As Exception

        End Try


    End Sub
    Protected Sub rdo_option_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdo_option.SelectedIndexChanged
        If rdo_option.SelectedItem.Value = "1" Then
            tbl_user_own.Visible = True
            tbl_user_ours.Visible = False
        Else
            tbl_user_ours.Visible = True
            tbl_user_own.Visible = False
        End If
    End Sub

End Class
