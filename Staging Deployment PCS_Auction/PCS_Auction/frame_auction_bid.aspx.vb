﻿
Partial Class frame_auction_bid
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            hid_auction_id.Value = Request.QueryString.Get("a")
            Dim str As String = validOrder(True)
            If IsNumeric(Request.QueryString.Get("o")) AndAlso (str = "" And Request.QueryString.Get("o") <> 0) Then
                'if offer clicked
                Dim dtBid As New DataTable
                dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(B.bid_amount, 0) AS bid_amount,ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value from tbl_auction_bids B WITH (NOLOCK) inner join tbl_auctions A WITH (NOLOCK) on A.auction_id=B.auction_id  where A.auction_id=" & hid_auction_id.Value & " order by bid_amount desc")
                If dtBid.Rows.Count > 0 Then
                    With dtBid.Rows(0)
                        If Not (.Item("bid_amount") < .Item("thresh_hold_value")) Then
                            str = "Sorry, This offer is currently not available. Please bid to win this product."
                        End If
                    End With
                End If
            End If
            If str <> "" Then
                ImageButtonCancelMsg.Attributes.Add("onclick", "return closeNow('0', '')")
                ConfirmOrder(str)
            Else
                If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                    hid_buyer_user_id.Value = CommonCode.Fetch_Cookie_Shared("user_id")
                    hid_buyer_id.Value = CommonCode.Fetch_Cookie_Shared("buyer_id")
                End If
                Dim title As String = SqlHelper.ExecuteScalar("select title from tbl_auctions WITH (NOLOCK) where auction_id=" & hid_auction_id.Value)
                SetBidNowConfirm(title)
                ImageButtonCancelQuote.Attributes.Add("onclick", "return closeNow('0', '')")
                ImageButtonCancelOffer.Attributes.Add("onclick", "return closeNow('0', '')")
                If Request.QueryString.Get("o") = "0" Then
                    ImageButtonCancelMsg.Attributes.Add("onclick", "return closeNow('" & Request.QueryString.Get("t") & "', '" & Request.QueryString.Get("r") & "')")
                Else
                    ImageButtonCancelMsg.Attributes.Add("onclick", "return closeNow('0', '')")
                End If
            End If
        End If
    End Sub
    Private Sub SetBidNowConfirm(ByVal title As String)
        'This function is call when user logged in and ready to bid
        ' Response.Write("This function is call when user logged in and ready to bid")
        pnl_offer.Visible = False
        pnl_confirm.Visible = False
        pnl_send_quote.Visible = False
        tr_price.Visible = False
        tr_qty.Visible = False
        tr_message.Visible = False
        pnl_shipping.Visible = False

        ImageButtonOffer.ImageUrl = "/Images/fend/4.png"

        Select Case Request.QueryString.Get("o")
            Case 1 'Buy Now Offer
                ViewState("option") = 1
                pnl_offer.Visible = True
                tr_message.Visible = True
                hid_buy_now_price.Value = Request.QueryString.Get("p")
                lbl_modal_catg.Text = "Buy Now : "
                lbl_modal_caption.Text = title

                lit_offer_text.Text = "I want to buy this lot for $" & FormatNumber(Request.QueryString.Get("p"), 2)
            Case 2 'private offer
                ViewState("option") = 2
                pnl_offer.Visible = True
                tr_price.Visible = True
                lit_qty.Visible = True
                lbl_modal_catg.Text = "Buy Now : "
                lbl_modal_caption.Text = title
            Case 3 'Partial Offer
                ViewState("option") = 3
                pnl_offer.Visible = True
                tr_price.Visible = True
                tr_qty.Visible = True
                lit_qty.Visible = False
                lbl_modal_catg.Text = "Buy Now : "
                lbl_modal_caption.Text = title
            Case 0 'Buy Now/Trad/Proxy/Quote
                ViewState("option") = 0
                Select Case Request.QueryString.Get("t")
                    Case 1
                        pnl_offer.Visible = True
                        tr_message.Visible = True
                        lbl_modal_catg.Text = "Buy Now : "
                        lbl_modal_caption.Text = title
                        lit_offer_text.Text = "I want to buy this lot for $" & FormatNumber(Request.QueryString.Get("p"), 2)
                    Case 2
                        pnl_shipping.Visible = True
                        pnl_offer.Visible = True
                        tr_message.Visible = True
                        ImageButtonOffer.ImageUrl = "/Images/fend/5.png"
                        lbl_modal_catg.Text = "Bid Now : "
                        lbl_modal_caption.Text = title
                        lit_offer_text.Text = "I want to bid this lot for $" & FormatNumber(Request.QueryString.Get("p"), 2)
                    Case 3
                        pnl_shipping.Visible = True
                        pnl_offer.Visible = True
                        tr_message.Visible = True
                        ImageButtonOffer.ImageUrl = "/Images/fend/5.png"
                        lbl_modal_catg.Text = "Bid Now : "
                        lbl_modal_caption.Text = title
                        lit_offer_text.Text = "I want to bid this lot for $" & FormatNumber(Request.QueryString.Get("p"), 2)
                    Case 4
                        pnl_send_quote.Visible = True
                        lbl_modal_catg.Text = "Offer : "
                        lbl_modal_caption.Text = title

                End Select

        End Select
        If pnl_shipping.Visible Then
            select_shipping_option()
            If Request.QueryString.Get("o") = 0 And (Request.QueryString.Get("t") = 2 Or Request.QueryString.Get("t") = 3) Then
                BidNow(hid_buyer_user_id.Value, hid_buyer_id.Value)
            End If
        End If



    End Sub
    Private Sub ConfirmOrder(ByVal message As String)

        chk_accept_term.Checked = False
        pnl_offer.Visible = False
        pnl_send_quote.Visible = False
        pnl_confirm.Visible = True
        lbl_popmsg_msg.Text = message

    End Sub
    Public Sub select_shipping_option()

        Dim shipping_preference As String = ""
        Dim last_shipping_option As String = ""
        Dim Str As String
        Dim dt As New DataTable
        Str = "select ISNULL(shipping_type,'') AS shipping_type,ISNULL(shipping_account_number,'') AS shipping_account_number,ISNULL(shipping_preference,'') AS shipping_preference,ISNULL(last_shipping_option,'') AS last_shipping_option from tbl_reg_buyers WITH (NOLOCK) where buyer_id=" & hid_buyer_id.Value
        dt = SqlHelper.ExecuteDatatable(Str)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                txt_shipping_type.Text = .Item("shipping_type")
                txt_shipping_account_number.Text = .Item("shipping_account_number")
                shipping_preference = .Item("shipping_preference")
                last_shipping_option = .Item("last_shipping_option")
            End With
        End If
        dt.Dispose()

        Str = "select A.use_pcs_shipping, A.use_your_shipping, isnull(S.name,'') as name, isnull(S.city,'') as city, isnull(S.state,'') as state from tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations S on S.stock_location_id=A.stock_location_id where A.auction_id=" & hid_auction_id.Value
        dt = SqlHelper.ExecuteDatatable(Str)
        Dim stock_location As String = ""
        If dt.Rows.Count > 0 Then
            stock_location = IIf(dt.Rows(0)("name") = "", "", dt.Rows(0)("name")) & IIf(dt.Rows(0)("city") = "", "", "</br>" & dt.Rows(0)("city")) & IIf(dt.Rows(0)("state") = "", "", ", " & dt.Rows(0)("state"))
            If dt.Rows(0)("use_pcs_shipping") = True And dt.Rows(0)("use_your_shipping") = True Then

                getFedExRate()
                rdo_option.Visible = True

                rdo_option.ClearSelection()
                rdo_option.Items.FindByValue(Request.QueryString.Get("sOption")).Selected = True
                'If rdo_option.Items.FindByValue(last_shipping_option) IsNot Nothing Then
                '    rdo_option.ClearSelection()
                '    rdo_option.Items.FindByValue(last_shipping_option).Selected = True
                'Else
                '    rdo_option.Items.FindByValue("2").Selected = True
                'End If

                If rdo_option.SelectedValue = 1 Then
                    tbl_user_own.Visible = True
                    tbl_user_ours.Visible = False
                    If txt_shipping_type.Text.Trim() <> "" Then
                        chk_accept_term.Checked = True
                    End If
                Else
                    tbl_user_own.Visible = False
                    tbl_user_ours.Visible = True
                    If rdo_fedex.Items.FindByValue(shipping_preference) IsNot Nothing Then
                        rdo_fedex.ClearSelection()
                        rdo_fedex.Items.FindByValue(shipping_preference).Selected = True
                    End If
                    If rdo_fedex.SelectedIndex >= 0 Then
                        chk_accept_term.Checked = True
                    End If
                End If

            Else
                If dt.Rows(0)("use_your_shipping") = True Then
                    'use your only
                    rdo_option.Items.FindByValue("1").Selected = True
                    rdo_option.Visible = False
                    tbl_user_ours.Visible = False
                    tbl_user_own.Visible = True
                    If txt_shipping_type.Text.Trim() <> "" Then
                        chk_accept_term.Checked = True
                    End If
                Else
                    'use PCS only
                    getFedExRate()
                    rdo_option.Items.FindByValue("2").Selected = True
                    If rdo_fedex.Items.FindByValue(shipping_preference) IsNot Nothing Then
                        rdo_fedex.ClearSelection()
                        rdo_fedex.Items.FindByValue(shipping_preference).Selected = True
                    End If
                    rdo_option.Visible = False
                    tbl_user_ours.Visible = True
                    tbl_user_own.Visible = False
                    If rdo_fedex.SelectedIndex >= 0 Then
                        chk_accept_term.Checked = True
                    End If
                End If
            End If
        End If

        If stock_location <> "" Then
            table_stock.Visible = True
            lbl_stock_location.Text = stock_location
        Else
            table_stock.Visible = False
        End If

    End Sub
    Private Sub getFedExRate()
        Try
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("SELECT A.fedex_rate_id,ISNULL(A.shipping_amount, 0) AS Rate,ISNULL(A.shipping_options, '') AS ServiceType FROM tbl_auction_fedex_rates A WITH (NOLOCK) WHERE A.auction_id=" & hid_auction_id.Value & " and A.buyer_id=" & hid_buyer_id.Value & " ORDER BY A.shipping_amount")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView
                dv = dt.DefaultView
                rdo_fedex.Items.Clear()
                Dim litm As ListItem

                For i As Integer = 0 To dv.Count - 1
                    litm = New ListItem
                    litm.Text = dv.Item(i)("ServiceType").ToString().Replace("_", " ") & " <b>" & FormatCurrency(dv.Item(i)("Rate"), 2) & "</b>"
                    litm.Value = dv.Item(i)("fedex_rate_id")
                    If dv.Item(i)("Rate") = Request.QueryString.Get("sValue") Then
                        litm.Selected = True
                    End If
                    rdo_fedex.Items.Insert(i, litm)
                Next
            End If
        Catch ex As Exception

        End Try


    End Sub
    Protected Sub rdo_option_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdo_option.SelectedIndexChanged
        If rdo_option.SelectedItem.Value = "1" Then
            tbl_user_own.Visible = True
            tbl_user_ours.Visible = False
        Else
            tbl_user_ours.Visible = True
            tbl_user_own.Visible = False
        End If
    End Sub
    Protected Sub ImageButtonOffer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonOffer.Click
        If Not chk_accept_term.Checked Then
            lbl_terms_error.Visible = True
        Else
            BidNow(hid_buyer_user_id.Value, hid_buyer_id.Value)
            If pnl_shipping.Visible Then
                Dim shipping_type As String = ""
                Dim shipping_account_number As String = ""
                Dim shipping_preference As String = ""
                Dim last_shipping_option As String = rdo_option.SelectedValue

                Dim str As String
                If rdo_option.SelectedValue = 1 Then
                    shipping_type = txt_shipping_type.Text.Trim()
                    shipping_account_number = txt_shipping_account_number.Text.Trim()
                ElseIf rdo_option.SelectedValue = 2 Then
                    shipping_preference = rdo_fedex.SelectedValue
                End If

                str = "update tbl_reg_buyers set shipping_type='" & shipping_type & "', shipping_account_number='" & shipping_account_number & "', shipping_preference='" & shipping_preference & "', last_shipping_option='" & last_shipping_option & "' where buyer_id=" & hid_buyer_id.Value
                SqlHelper.ExecuteNonQuery(str)
            End If


        End If

    End Sub

#Region "Funtions"

    Private Sub BidNow(ByVal user_id As Integer, ByVal buyer_id As Integer)
        Dim validate As String = validOrder(False)
        If validate = "" Then
            Dim str As String = ""
            Dim obj As New Bid()
            Dim objEmail As New Email()
            Dim strShipping As String = ""
            Dim strShippingAmount As Double = 0
            If pnl_shipping.Visible Then
                If rdo_option.SelectedValue = 1 Then
                    strShipping = "My shipping carrier is: " & txt_shipping_type.Text.Trim() & " And My Account # is: " & txt_shipping_account_number.Text.Trim()
                Else
                    Dim dtTable As DataTable = SqlHelper.ExecuteDatatable("select ISNULL(A.shipping_amount, 0) AS shipping_amount,ISNULL(A.shipping_options, '') AS shipping_options from tbl_auction_fedex_rates A WITH (NOLOCK) where  A.fedex_rate_id=" & rdo_fedex.SelectedValue)
                    If dtTable.Rows.Count > 0 Then
                        strShipping = dtTable.Rows(0).Item("shipping_options")
                        strShippingAmount = dtTable.Rows(0).Item("shipping_amount")
                    End If

                    strShipping = "My shipping option: " & rdo_fedex.SelectedItem.Text
                End If
            End If

            Select Case Request.QueryString.Get("o")

                Case 0 'main button click
                    Select Case Request.QueryString.Get("t")
                        Case 1
                            Response.Write("<script>window.opener.redirectConfirm(1," & Request.QueryString.Get("p") & "," & hid_auction_id.Value & ",'buy now');window.opener.closeAuctionWindow();</script>")
                            'Response.Redirect("/AuctionConfirmation.aspx?t=b&a=" & hid_auction_id.Value & "&q=1&p=" & Request.QueryString.Get("p") & "")
                            'obj.save_buy_now_bid(hid_auction_id.Value, hid_buy_now_price.Value, 0, "buy now", user_id, buyer_id, strShipping)
                            ' objEmail.Send_Auction_Order_Mail(hid_auction_id.Value, Request.QueryString.Get("o"), user_id)
                            str = "We will review your request to Buy it Now and get back to you within 2 business days.<br><br>Thank you for participating!"
                        Case 2
                            'If rdo_option.SelectedValue = 1 Then
                            '    strShipping = "My shipping carrier is: " & txt_shipping_type.Text.Trim() & " And My Account # is: " & txt_shipping_account_number.Text.Trim()
                            'Else
                            '    strShipping = "My shipping option: " & rdo_fedex.SelectedItem.Text
                            'End If

                            obj.insert_tbl_auction_bids(hid_auction_id.Value, user_id, hid_bid_now_price.Value, strShipping, strShippingAmount)
                            Response.Write("<script>window.opener.refresh_history();</script>")
                            Dim rak As Integer = SqlHelper.ExecuteScalar("select dbo.buyer_bid_rank(" & hid_auction_id.Value & "," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
                            If rak = 1 Then
                                str = "Congratulations! You are now the highest bidder."
                            Else
                                'Dim dtHBid As New DataTable()
                                'dtHBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids where auction_id=" & hid_auction_id.Value & " order by bid_amount desc")
                                'Dim c_bid_amount As Double = 0
                                'If dtHBid.Rows.Count > 0 Then
                                '    c_bid_amount = dtHBid.Rows(0).Item("bid_amount")
                                'End If
                                'dtHBid.Dispose()
                                'str = "You have been outbid. The current high bid is " & FormatCurrency(c_bid_amount, 2) & ". Please submit a new bid."
                                str = "You have been outbid."
                            End If

                        Case 3
                            'If rdo_option.SelectedValue = 1 Then
                            '    strShipping = "My shipping carrier is: " & txt_shipping_type.Text.Trim() & " And My Account # is: " & txt_shipping_account_number.Text.Trim()
                            'Else
                            '    strShipping = "My shipping option: " & rdo_fedex.SelectedItem.Text
                            'End If
                            obj.insert_tbl_auction_bids(hid_auction_id.Value, user_id, Request.QueryString.Get("p"), strShipping, strShippingAmount)
                            Response.Write("<script>window.opener.refresh_history();</script>")
                            Dim rak As Integer = SqlHelper.ExecuteScalar("select dbo.buyer_bid_rank(" & hid_auction_id.Value & "," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
                            If rak = 1 Then
                                str = "Congratulations! You are now the highest bidder."
                            Else
                                'Dim dtHBid As New DataTable()
                                'dtHBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(max_bid_amount, 0) AS max_bid_amount,ISNULL(bid_amount, 0) AS bid_amount from tbl_auction_bids where auction_id=" & hid_auction_id.Value & " order by bid_amount desc")
                                'Dim c_bid_amount As Double = 0
                                'If dtHBid.Rows.Count > 0 Then
                                '    c_bid_amount = dtHBid.Rows(0).Item("bid_amount")
                                'End If
                                'dtHBid.Dispose()
                                'str = "You have been outbid. The current high bid is " & FormatCurrency(c_bid_amount, 2) & ". Please submit a new bid."
                                str = "You have been outbid."
                            End If
                        Case Else
                    End Select
                Case 1
                    Response.Write("<script>window.opener.redirectConfirm(1," & Request.QueryString.Get("p") & "," & hid_auction_id.Value & ",'buy now');window.opener.closeAuctionWindow();</script>")
                    'Response.Redirect("/AuctionConfirmation.aspx?t=b&a=" & hid_auction_id.Value & "&q=1&p=" & Request.QueryString.Get("p") & "")
                    'obj.save_buy_now_bid(hid_auction_id.Value, hid_buy_now_price.Value, 0, "buy now", user_id, buyer_id, strShipping)
                    ' objEmail.Send_Auction_Order_Mail(hid_auction_id.Value, Request.QueryString.Get("o"), user_id)
                    str = "Thank you for submitting your order."
                Case 2
                    Response.Write("<script>window.opener.redirectConfirm(1," & txt_amount.Text.Trim & "," & hid_auction_id.Value & ",'private');window.opener.closeAuctionWindow();</script>")
                    'Response.Redirect("/AuctionConfirmation.aspx?t=b&a=" & hid_auction_id.Value & "&q=1&p=" & txt_amount.Text.Trim & "")
                    'obj.save_buy_now_bid(hid_auction_id.Value, txt_amount.Text, 0, "private", user_id, buyer_id, strShipping)
                    ' objEmail.Send_Auction_Order_Mail(hid_auction_id.Value, Request.QueryString.Get("o"), user_id)
                    str = "Thank you for submitting private order."
                Case 3
                    Response.Write("<script>window.opener.redirectConfirm(" & txt_qty.Text.Trim & "," & txt_amount.Text.Trim & "," & hid_auction_id.Value & ",'partial');window.opener.closeAuctionWindow();</script>")
                    'Response.Redirect("/AuctionConfirmation.aspx?t=b&a=" & hid_auction_id.Value & "&q=" & txt_qty.Text.Trim & "&m=" & txt_amount.Text.Trim & "")
                    'obj.save_buy_now_bid(hid_auction_id.Value, txt_amount.Text, txt_qty.Text, "partial", user_id, buyer_id, strShipping)
                    ' objEmail.Send_Auction_Order_Mail(hid_auction_id.Value, Request.QueryString.Get("o"), user_id)
                    str = "Thank you for submitting partial order."
                Case Else

            End Select
            objEmail = Nothing
            obj = Nothing


            ConfirmOrder(str)
        Else
            ConfirmOrder(validate)
        End If

    End Sub
    Private Function validOrder(ByVal bid_over_check_only As Boolean) As String
        Dim flg As String = ""

        Dim auc_stat As Integer = SqlHelper.ExecuteScalar("select dbo.get_auction_status(" & hid_auction_id.Value & ")")
        Select Case auc_stat
            Case 2
                flg = "Sorry, but we’ve sold out of this product. Please check back regularly for the opportunity to participate in future auctions."
            Case 3
                flg = "Sorry, but we’ve sold out of this product. Please check back regularly for the opportunity to participate in future auctions."
        End Select

        If flg = "" And Request.QueryString.Get("t") = 3 And Request.QueryString.Get("o") = 0 Then

            Dim dttt As DataTable = SqlHelper.ExecuteDatatable("select bid_id from tbl_auction_bids where auction_id=" & hid_auction_id.Value & " and max_bid_amount='" & Request.QueryString.Get("p") & "'")
            If dttt.Rows.Count > 0 Then
                flg = "Sorry, another bidder already set this amount, try with another amount."
            Else

                Dim dtt1 As DataTable = SqlHelper.ExecuteDatatable("select top 1 A.bid_amount,B.increament_amount from tbl_auction_bids A WITH (NOLOCK) inner join tbl_auctions B WITH (NOLOCK) on A.auction_id=B.auction_id where A.auction_id=" & hid_auction_id.Value & " order by A.bid_amount desc")
                If dtt1.Rows.Count > 0 Then
                    If Request.QueryString.Get("p") < dtt1.Rows(0).Item("bid_amount") + dtt1.Rows(0).Item("increament_amount") Then
                        flg = "Sorry, Amount not Accepted, you have to bid " & FormatCurrency(dtt1.Rows(0).Item("bid_amount") + dtt1.Rows(0).Item("increament_amount"), 2) & " or more."
                    End If

                End If
                dtt1.Dispose()

            End If
            dttt.Dispose()
        End If


        If flg = "" And bid_over_check_only = False Then

            Dim bid_limit As Double = SqlHelper.ExecuteScalar("select case when exists(select buyer_user_id from tbl_reg_buyer_users where buyer_user_id=" & hid_buyer_user_id.Value & " ) then (select isnull(bidding_limit,0) from tbl_reg_buyer_users where buyer_user_id=" & hid_buyer_user_id.Value & ") else 0 end")
            If bid_limit = 0 Then bid_limit = Double.MaxValue
            If Request.QueryString.Get("o") = 0 Then 'if main button clicked

                Select Case Request.QueryString.Get("t")
                    Case 1
                        If bid_limit < hid_buy_now_price.Value Then
                            flg = "Sorry, Bid amount exceeds your bidding limit."
                        End If
                    Case 2
                        If bid_limit < hid_bid_now_price.Value Then
                            flg = "Sorry, Bid amount exceeds your bidding limit."
                        End If
                    Case 3
                        If IsNumeric(Request.QueryString.Get("p")) Then
                            If CDbl(Request.QueryString.Get("p")) < CDbl(hid_bid_now_price.Value) Then
                                flg = "Sorry, Invalid Amount."
                            Else
                                If bid_limit < Request.QueryString.Get("p") Then
                                    flg = "Sorry, Bid amount exceeds your bidding limit."
                                End If

                            End If
                        Else
                            flg = "Sorry, Bid amount is invalid."
                        End If

                End Select
            Else
                'if offer clicked
                Dim dtBid As New DataTable
                dtBid = SqlHelper.ExecuteDatatable("select top 1 ISNULL(B.bid_amount, 0) AS bid_amount,ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value from tbl_auction_bids B WITH (NOLOCK) inner join tbl_auctions A WITH (NOLOCK) on A.auction_id=B.auction_id  where A.auction_id=" & hid_auction_id.Value & " order by bid_amount desc")
                If dtBid.Rows.Count > 0 Then
                    With dtBid.Rows(0)
                        If Not (.Item("bid_amount") < .Item("thresh_hold_value")) Then
                            flg = "Sorry, This offer is currently not available. Please bid to win this product."
                        End If
                    End With
                End If
                If flg = "" Then
                    Select Case Request.QueryString.Get("o")
                        Case 1
                            If bid_limit < hid_buy_now_price.Value Then
                                flg = "Sorry, Bid amount exceeds your bidding limit."
                            End If
                        Case 2
                            If IsNumeric(txt_amount.Text) Then
                                If bid_limit < txt_amount.Text Then
                                    flg = "Sorry, Bid amount exceeds your bidding limit."
                                End If
                            Else
                                flg = "Sorry, Bid amount is invalid."
                            End If

                        Case 3
                            If IsNumeric(txt_amount.Text) Then
                                If bid_limit < txt_amount.Text Then
                                    flg = "Sorry, Bid amount exceeds your bidding limit."
                                End If
                            Else
                                flg = "Sorry, Bid amount is invalid."
                            End If

                            If IsNumeric(txt_qty.Text.Trim) Then
                                If txt_qty.Text.Trim = 0 Then
                                    flg = "Sorry, quantity is invalid."
                                End If
                            Else
                                flg = "Sorry, quantity is invalid."
                            End If

                        Case Else

                    End Select
                End If

            End If
        End If


        Return flg
    End Function

#End Region

    Protected Sub ImageButton_quote_send_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_quote_send.Click
        request_quote()
        Dim objEmail As New Email()
        objEmail.Send_Auction_Order_Mail(Request.QueryString.Get("a"), ViewState("option"), CommonCode.Fetch_Cookie_Shared("user_id"))
        objEmail = Nothing

        Dim str As String = ""
        str = "Thank you for submitting your Offer."

        ConfirmOrder(str)

    End Sub

    Private Sub request_quote()
        'Try
        Dim details As String = txt_quote_desc.Text.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")
        Dim Quotation_id As Integer = 0

        Dim filename As String = ""
        If fle_quote_upload.HasFile Then
            filename = fle_quote_upload.FileName
            filename = CommonCode.Fetch_Cookie_Shared("user_id") & "_" & filename
        End If
        Dim obj As New Bid()
        Quotation_id = obj.send_quotation(Request.QueryString.Get("a"), details.Trim(), CommonCode.Fetch_Cookie_Shared("user_id"), CommonCode.Fetch_Cookie_Shared("buyer_id"))

        If fle_quote_upload.HasFile Then
            If Quotation_id > 0 Then
                obj.upload_quotation_file(Request.QueryString.Get("a"), Quotation_id, fle_quote_upload, CommonCode.Fetch_Cookie_Shared("user_id"))
            End If
        Else
        End If
        obj = Nothing
        ' Catch ex As Exception

        ' End Try

    End Sub
End Class
