﻿Imports Telerik.Web.UI

Partial Class MessageBoard_InvitedBidderList
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("i")) Then
                If Not String.IsNullOrEmpty(Request.QueryString("mode")) Then
                    If Request.QueryString("mode") = "s" Then
                        ' bind_message_invited_sellers()
                        pnl_invited_sellers.Visible = True
                        pnl_invited_bidders.Visible = False
                        lbl_heading.Text = "Invited Users"
                    Else
                        'bind_auction_invited_bidders()
                        pnl_invited_sellers.Visible = False
                        pnl_invited_bidders.Visible = True
                        lbl_heading.Text = "Invited Bidders"
                    End If
                Else
                    ' bind_auction_invited_bidders()
                    pnl_invited_sellers.Visible = False
                    pnl_invited_bidders.Visible = True
                    lbl_heading.Text = "Invited Bidders"
                End If
            End If
        End If
    End Sub
    Private Sub bind_auction_invited_bidders()
        rad_grid_invited_bidders.DataSource = CommonCode.GetMessageInvitedBidders(Request.QueryString.Get("i"))
    End Sub
    Private Sub bind_message_invited_sellers()
        rad_grid_sellers.DataSource = CommonCode.GetMessageInvitedSellers(Request.QueryString.Get("i"))
    End Sub

    Protected Sub rad_grid_invited_bidders_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles rad_grid_invited_bidders.NeedDataSource

        bind_auction_invited_bidders()
    End Sub

    Protected Sub rad_grid_sellers_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles rad_grid_sellers.NeedDataSource
        bind_message_invited_sellers()
    End Sub

    Protected Sub rad_grid_invited_bidders_PreRender(sender As Object, e As System.EventArgs) Handles rad_grid_invited_bidders.PreRender

        Dim menu As GridFilterMenu = rad_grid_invited_bidders.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub

    Protected Sub rad_grid_sellers_PreRender(sender As Object, e As System.EventArgs) Handles rad_grid_sellers.PreRender
        Dim menu As GridFilterMenu = rad_grid_sellers.FilterMenu

        Dim i As Integer = 0
        While i < menu.Items.Count

            If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
                i = i + 1
            Else
                menu.Items.RemoveAt(i)
            End If
        End While
    End Sub
End Class
