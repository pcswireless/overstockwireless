﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MessageInvitationSellers.ascx.vb"
    Inherits="MessageBoard_UserControls_MessageInvitationSellers" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
    Skin="Simple" />
<telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
    CssClass="TabGrid">
   
    <table cellpadding="0" cellspacing="0" border="0" width="850">
        <tr>
            <td style="width: 400px;" class="invSubHeading">
                Available Options
            </td>
            <td style="width: 50px;">
                &nbsp;
            </td>
            <td style="width: 400px;" class="invSubHeading">
                Selected Options<span class="invLink"><asp:Literal ID="lit_no_of_bidder" runat="server"></asp:Literal></span>
            </td>
        </tr>
        <tr>
            <td class="greenBackSmall">
                <div style="float: left; padding-top: 3px;">
                    <asp:RadioButton ID="RDO_filter_type1" Checked="true" AutoPostBack="true" runat="server"
                        Text="Titles" TextAlign="Right" GroupName="grpType" />
                </div>
                <div style="float: left; padding-left: 20px; padding-top: 3px;">
                    <asp:RadioButton ID="RDO_filter_type2" AutoPostBack="true" runat="server" Text="Users"
                        TextAlign="Right" GroupName="grpType" />
                </div>
            </td>
            <td valign="top">
                &nbsp;
            </td>
            <td class="greenBackSmall">
                Populate Invitation Criteria from :
                <asp:DropDownList ID="ddl_invitation" runat="server" DataValueField="invitation_id"
                    DataTextField="name" AutoPostBack="true">
                </asp:DropDownList>
                <asp:HiddenField ID="HID_invitation_id" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px; border-left: 1px solid #E4E4E5; border-right: 1px solid #E4E4E5;"
                valign="top">
                <table width="100%" cellpadding="5" cellspacing="0" border="0">
                    <tr>
                        <td valign="top">
                            <asp:Panel ID="pnl_type1" runat="server" Height="450" ScrollBars="Vertical">
                                <telerik:RadGrid ID="RadGrid_SelectedTitleList_left" GridLines="None" runat="server"
                                    AllowPaging="False" AutoGenerateColumns="False" AllowMultiRowSelection="false"
                                    Skin="Simple" AllowFilteringByColumn="false" ShowHeader="false" ShowFooter="false"
                                    DataSourceID="SqlDataSource5" Width="400">
                                    <MasterTableView DataKeyNames="title_id" HorizontalAlign="NotSet" AutoGenerateColumns="False">
                                        <NoRecordsTemplate>
                                            Available Title has been selected
                                        </NoRecordsTemplate>
                                        <Columns>
                                            <telerik:GridTemplateColumn>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chk" runat="server" Text='<%#Eval("name") %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT [title_id], [name] FROM [tbl_master_titles] where title_id not in (select title_id from tbl_message_invitation_filter_values where message_board_id=@message_board_id) order by name">
                                    <SelectParameters>
                                        <asp:QueryStringParameter Name="message_board_id" QueryStringField="i" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:Panel>
                            <asp:Panel ID="pnl_type2" runat="server" Height="450" ScrollBars="Vertical" Visible="false">
                                <telerik:RadGrid ID="RadGrid_UserList" GridLines="None" runat="server" AllowPaging="False"
                                    AutoGenerateColumns="False" DataSourceID="SqlDataSource6" AllowMultiRowSelection="false"
                                    Skin="Vista" AllowFilteringByColumn="True" Width="400" AllowSorting="true">
                                    <MasterTableView DataKeyNames="user_id" DataSourceID="SqlDataSource6" HorizontalAlign="NotSet"
                                        AutoGenerateColumns="False" AllowFilteringByColumn="True">
                                        <HeaderStyle BackColor="#BEBEBE" />
                                        <NoRecordsTemplate>
                                            Available User has been selected
                                        </NoRecordsTemplate>
                                        <SortExpressions>
                                            <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                                        </SortExpressions>
                                        <Columns>
                                            <telerik:GridTemplateColumn HeaderText="Name" SortExpression="name" ShowSortIcon="true"
                                                UniqueName="name" DataField="name" FilterControlWidth="60">
                                                <ItemTemplate>
                                                    <a href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/users/user_mouse_over.aspx?i=<%# Eval("user_id") %>','270','white','true');"
                                                        onclick="return open_win_pop('/Users/Userdetail.aspx','<%# Eval("user_id") %>','_assign_u');">
                                                        <%# Eval("name")%></a>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Email Address" UniqueName="email" DataField="email"
                                                FilterControlWidth="90" SortExpression="email">
                                                <ItemTemplate>
                                                    <a href="mailto:<%# Eval("email")%>">
                                                        <%# Eval("email")%></a>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Select" Groupable="false" AllowFiltering="false">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chk_select" runat="server" />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                    ProviderName="System.Data.SqlClient" SelectCommand="select user_id,first_name+' '+first_name as name,email from tbl_sec_users  where is_active=1 and user_id not in (select seller_id from tbl_message_invitation_filter_values where message_board_id=@message_board_id) order by name">
                                    <SelectParameters>
                                        <asp:QueryStringParameter Name="message_board_id" QueryStringField="i" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="middle" align="center" style="padding: 5px;">
                <asp:ImageButton ID="btn_add_filter" runat="server" AlternateText="Save" ImageUrl="/images/add_filter.gif" />
            </td>
            <td style="padding: 5px; border-left: 1px solid #E4E4E5; border-right: 1px solid #E4E4E5;"
                valign="top">
                <asp:Panel ID="pnl_invitaion_data" runat="server">
                    <div id="div_opt_1" runat="server" visible='False'>
                        <div class="greenBackSmall">
                            <div style="padding-top: 8px;">
                                Titles</div>
                        </div>
                        <div class="greenBackSmall_down">
                            <asp:HiddenField ID="invitation_filter_id1" runat="server" Value="0" />
                            <telerik:RadGrid ID="RadGrid_SelectedTitleList" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                                AllowPaging="False" AutoGenerateColumns="False" AllowMultiRowSelection="false"
                                DataSourceID="SqlDataSource1" Skin="Simple" AllowFilteringByColumn="false" ShowHeader="false"
                                ShowFooter="false">
                                <MasterTableView DataKeyNames="invitation_filter_value_id" HorizontalAlign="NotSet"
                                    AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                                    <NoRecordsTemplate>
                                        <div style="height: 19px; padding-top: 3px; padding-left: 5px;">
                                            Titles not selected</div>
                                    </NoRecordsTemplate>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="name" HeaderText="name" UniqueName="name">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridButtonColumn ConfirmText="Delete this Titles?" ConfirmDialogType="RadWindow"
                                            ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                        </telerik:GridButtonColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                ProviderName="System.Data.SqlClient" SelectCommand="select A.invitation_filter_value_id,B.name from tbl_message_invitation_filter_values A join tbl_master_titles B on A.title_id=B.title_id where A.invitation_filter_id= @invitation_filter_id"
                                DeleteCommand="DELETE FROM [tbl_message_invitation_filter_values] WHERE [invitation_filter_value_id] = @invitation_filter_value_id">
                                <SelectParameters>
                                    <asp:ControlParameter Name="invitation_filter_id" Type="Int32" ControlID="invitation_filter_id1"
                                        PropertyName="Value" />
                                </SelectParameters>
                                <DeleteParameters>
                                    <asp:Parameter Name="invitation_filter_value_id" Type="Int32" />
                                </DeleteParameters>
                            </asp:SqlDataSource>
                        </div>
                    </div>
                    <div id="div_opt_2" runat="server" visible='False' style="padding-top: 10px;">
                        <div class="greenBackSmall">
                            <div style="padding-top: 8px;">
                                Users</div>
                        </div>
                        <div class="greenBackSmall_down">
                            <asp:HiddenField ID="invitation_filter_id2" runat="server" Value="0" />
                            <telerik:RadGrid ID="RadGrid_SelectedUserlist" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                                AllowPaging="False" AutoGenerateColumns="False" AllowMultiRowSelection="false"
                                DataSourceID="SqlDataSource2" Skin="Simple" AllowFilteringByColumn="false" ShowHeader="false"
                                ShowFooter="false">
                                <MasterTableView DataKeyNames="invitation_filter_value_id" HorizontalAlign="NotSet"
                                    AutoGenerateColumns="False" DataSourceID="SqlDataSource2">
                                    <NoRecordsTemplate>
                                        <div style="height: 19px; padding-top: 3px; padding-left: 5px;">
                                            User not selected</div>
                                    </NoRecordsTemplate>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="name" HeaderText="Name" SortExpression="name"
                                            UniqueName="name">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Email Address" UniqueName="email" SortExpression="email"
                                            DataField="email">
                                            <ItemTemplate>
                                                <a href="mailto:<%# Eval("email")%>">
                                                    <%# Eval("email")%></a>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridButtonColumn ConfirmText="Delete this user?" ConfirmDialogType="RadWindow"
                                            ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                        </telerik:GridButtonColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                ProviderName="System.Data.SqlClient" SelectCommand="select A.invitation_filter_value_id,B.first_name+' '+B.first_name as name,B.email from tbl_message_invitation_filter_values A join tbl_sec_users B on A.seller_id=B.user_id where A.invitation_filter_id= @invitation_filter_id"
                                DeleteCommand="DELETE FROM [tbl_message_invitation_filter_values] WHERE [invitation_filter_value_id] = @invitation_filter_value_id">
                                <SelectParameters>
                                    <asp:ControlParameter Name="invitation_filter_id" Type="Int32" ControlID="invitation_filter_id2"
                                        PropertyName="Value" />
                                </SelectParameters>
                                <DeleteParameters>
                                    <asp:Parameter Name="invitation_filter_value_id" Type="Int32" />
                                </DeleteParameters>
                            </asp:SqlDataSource>
                        </div>
                    </div>
                    <div style="background-color: White; margin-top: 10px;" id="div_item_not" runat="server">
                        <table cellpadding="5">
                            <tr>
                                <td>
                                    No Item Selected.
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px; border-left: 1px solid #E4E4E5; border-right: 1px solid #E4E4E5;
                border-bottom: 1px solid #E4E4E5;" valign="top">
            </td>
            <td>
                &nbsp;
            </td>
            <td style="padding: 5px; border-left: 1px solid #E4E4E5; border-right: 1px solid #E4E4E5;
                border-bottom: 1px solid #E4E4E5; background-color: #F6F6F6;" valign="top">
                <div style="margin-top: 10px;" id="div_save" runat="server">
                    <table cellpadding="5">
                        <tr>
                            <td>
                                Save this criteria as
                            </td>
                            <td>
                                <asp:HiddenField ID="HID_invitation_name" runat="server" Value="" />
                                <asp:TextBox ID="txt_invitation_name" runat="server" CssClass="TextBox" Width="100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfv_invitation_name" runat="server" ControlToValidate="txt_invitation_name"
                                    ValidationGroup="_invitation" Display="Dynamic" ErrorMessage="*" CssClass="error"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:ImageButton ID="btn_save_filter" ValidationGroup="_invitation" runat="server"
                                    AlternateText="Save" ImageUrl="/images/save.gif" />
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>
