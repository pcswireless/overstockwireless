﻿
Partial Class Auction_Listing
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not Page.IsPostBack Then
                showMessage()
                showtabname()
                Dim pnl As New Panel
                If IsNumeric(Request.QueryString.Get("c")) Then
                    auc_catg.SelectedIndex = Request.QueryString.Get("c")
                End If
            End If
            If Request.QueryString.Get("t") = 1 Then
                lit_tab.Text = "Live Auctions"
            ElseIf Request.QueryString.Get("t") = 2 Then
                lit_tab.Text = "Auction History"
            ElseIf Request.QueryString.Get("t") = 3 Then
                lit_tab.Text = "My Live Auctions"
            ElseIf Request.QueryString.Get("t") = 4 Then
                lit_tab.Text = "UpComing Auctions"
            Else
                lit_tab.Text = "Auctions"
            End If

            FillSideBar()
            pnl_noItem.Visible = False
            bindGrid()
        End If
    End Sub
    Private Sub showMessage()
        If CommonCode.Fetch_Cookie_Shared("buyer_id") <> "" AndAlso CommonCode.Fetch_Cookie_Shared("buyer_id") <> "0" Then
            rep_message.DataSource = SqlHelper.ExecuteDatatable("select [message_board_id],[title],[description] from [tbl_message_boards]  M  WITH (NOLOCK) " & _
                    "where dbo.message_board_status(ISNULL(display_from,'1/1/1900'),ISNULL(display_to,'1/1/1900'))='Running' and is_active=1 " & _
                    "and " & CommonCode.Fetch_Cookie_Shared("buyer_id") & " in (select buyer_id from fn_get_message_invited_buyers((select top 1 invitation_id from tbl_message_invitations  WITH (NOLOCK) where message_board_id=M.message_board_id)))")
            rep_message.DataBind()

            If rep_message.Items.Count > 0 Then
                div_topmsg.Visible = True
                rep_message.Visible = True
            Else
                div_topmsg.Visible = False
                rep_message.Visible = False
            End If

        Else
            div_topmsg.Visible = False
            rep_message.Visible = False
        End If


    End Sub
    Private Sub showtabname()
        If IsNumeric(Request.QueryString.Get("t")) AndAlso Request.QueryString.Get("t") = 1 Then
            tab_sel.Text = "Live Auctions"
        ElseIf IsNumeric(Request.QueryString.Get("t")) AndAlso Request.QueryString.Get("t") = 2 Then
            tab_sel.Text = "Auction History"
        ElseIf IsNumeric(Request.QueryString.Get("t")) AndAlso Request.QueryString.Get("t") = 3 Then
            tab_sel.Text = "My Live Auctions"
            'ElseIf IsNumeric(Request.QueryString.Get("t")) AndAlso Request.QueryString.Get("t") = 4 Then
            '    tab_sel.Text = "UpComing Auctions"
        Else
            tab_sel.Text = "Auctions"
        End If

    End Sub
    Private Sub bindGrid()

        Dim obj As New Auction

        Dim dv As DataView = obj.fetch_auction_Listing_new(IIf(String.IsNullOrEmpty(Request.QueryString.Get("t")), 1, Request.QueryString.Get("t")), IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), 0, Request.QueryString.Get("c")), IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), 0, Request.QueryString.Get("p")), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("user_id")), CommonCode.Fetch_Cookie_Shared("user_id"), 0), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("buyer_id")), CommonCode.Fetch_Cookie_Shared("buyer_id"), 0), SqlHelper.of_FetchKey("frontend_login_needed"), IIf(String.IsNullOrEmpty(Request.QueryString.Get("a")), 0, Request.QueryString.Get("a"))).Tables(0).DefaultView

        If dv.Count > 0 Then
            iframe_auctions.Attributes.Add("src", "/frame_auction_calculate.aspx?t=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("t")), 1, Request.QueryString.Get("t")) & "&c=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), 0, Request.QueryString.Get("c")) & "&p" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), 0, Request.QueryString.Get("p")))
            rpt_auctions.DataSource = dv
            rpt_auctions.DataBind()
            pnl_noItem.Visible = False
            pn_auction_content.Visible = True

        Else

            pn_auction_content.Visible = False
            pnl_noItem.Visible = True

            Select Case Request.QueryString.Get("t")
                Case 1
                    lit_no_item.Text = "Live Auctions currently not available"
                Case 2
                    lit_no_item.Text = "Auction History currently not available"
                Case 3
                    lit_no_item.Text = "My Live Auctions currently not available"
                Case 4
                    lit_no_item.Text = "UpComing auctions currently not available"
                Case 5
                    lit_no_item.Text = "Hidden auctions currently not available"
                Case 6
                    lit_no_item.Text = "Bidding auctions currently not available"
                Case Else
                    lit_no_item.Text = "Auction Not available in this section"
            End Select


        End If

        dv.Dispose()
        obj = Nothing

    End Sub
    Private Sub setAuction(ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs, ByVal auction_id As Integer)

        Dim str As String = "SELECT A.auction_id,"
        str = str & "ISNULL(A.code, '') AS code,"
        str = str & "ISNULL(A.title, '') AS title,"
        str = str & "ISNULL(A.sub_title, '') AS sub_title,"
        str = str & "ISNULL(A.product_catetory_id, 0) AS product_catetory_id,"
        str = str & "ISNULL(A.stock_condition_id, 0) AS stock_condition_id,"
        str = str & "ISNULL(A.auction_category_id, 0) AS auction_category_id,"
        str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"
        str = str & "ISNULL(A.is_private_offer, 0) AS is_private_offer,"
        str = str & "ISNULL(A.is_partial_offer, 0) AS is_partial_offer,"
        str = str & "ISNULL(A.auto_acceptance_price_private, 0) AS auto_acceptance_price_private,"
        str = str & "ISNULL(A.auto_acceptance_price_partial, 0) AS auto_acceptance_price_partial,"
        str = str & "ISNULL(A.auto_acceptance_quantity, 0) AS auto_acceptance_quantity,"
        str = str & "ISNULL(A.no_of_clicks, 0) AS no_of_clicks,"
        str = str & "ISNULL(A.start_price, 0) AS start_price,"
        str = str & "ISNULL(A.reserve_price, 0) AS reserve_price,"
        str = str & "ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value,"

        str = str & "ISNULL(A.is_show_reserve_price, 0) AS is_show_reserve_price,"
        str = str & "ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing,"
        str = str & "ISNULL(A.is_show_no_of_bids, 0) AS is_show_no_of_bids,"
        str = str & "A.use_pcs_shipping, A.use_your_shipping,"
        str = str & "ISNULL(A.increament_amount, 0) AS increament_amount,"
        str = str & "ISNULL(A.buy_now_price, 0) AS buy_now_price,"
        str = str & "ISNULL(A.is_buy_it_now, 0) AS is_buy_it_now,"
        str = str & "ISNULL(A.request_for_quote_message, '') AS request_for_quote_message,"
        str = str & "ISNULL(A.show_price, 0) AS show_price,"
        str = str & "ISNULL(A.short_description, '') AS short_description,"
        str = str & "ISNULL(A.total_qty, 0) AS total_qty,"
        str = str & "ISNULL(A.qty_per_bidder, 0) AS qty_per_bidder,"
        str = str & "dbo.get_auction_status(A.auction_id) as auction_status,"
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            str = str & "case when exists(select favourite_id from tbl_auction_favourites where auction_id=A.auction_id and buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ") then 1 else 0 end as in_fav,"
        Else
            str = str & "0 as in_fav,"
        End If
        str = str & "ISNULL(IMG.stock_image_id, 0) AS stock_image_id,ISNULL(IMG.filename, '') AS filename,"
        str = str & "isnull((Stuff((Select ' , ' + V.bucket_value From tbl_master_bucket_values V inner join tbl_auction_invitation_filter_values F on V.bucket_value_id=F.bucket_value_id Where  F.auction_id =A.auction_id  FOR XML PATH('')),1,2,'')),'') as bucket_value_for_filter"
        str = str & " FROM "
        str = str & "tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations L WITH (NOLOCK) on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B WITH (NOLOCK) on A.stock_condition_id=B.stock_condition_id "
        str = str & " left join tbl_auction_stock_images IMG WITH (NOLOCK) on A.auction_id=IMG.auction_id and IMG.stock_image_id=(SELECT top 1 stock_image_id from tbl_auction_stock_images WITH (NOLOCK) where auction_id=a.auction_id order by position) "
        str = str & " WHERE "
        str = str & "A.auction_id =" & auction_id
        'Response.Write(str)
        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDatatable(str)

        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)
                Dim title_url As String = .Item("title").ToString.Replace(" ", "-").Replace("/", "").Replace("'", "").Replace("&", "").Replace("#", "")
                CType(e.Item.FindControl("ltr_title"), Literal).Text = "<a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "' style='text-decoration: none;color:#323232;font-size:" & IIf(.Item("title").ToString.Length < 70, "19px", IIf(.Item("title").ToString.Length < 150, "18px", IIf(.Item("title").ToString.Length < 200, "16px", "14px"))) & ";'>" & .Item("title").ToString & "</a><br/><span style='font-family:dinbold; font-size: 14px;color:#686868; font-weight: normal;'>" & .Item("sub_title").ToString & "</span>"
                'If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                '    CType(e.Item.FindControl("ltr_title"), Literal).Text = "<a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "' style='text-decoration: none;color:#323232;font-size:" & IIf(.Item("title").ToString.Length < 70, "22px", IIf(.Item("title").ToString.Length < 150, "18px", IIf(.Item("title").ToString.Length < 200, "16px", "14px"))) & ";'>" & .Item("title").ToString & "</a>"
                'Else
                '    CType(e.Item.FindControl("ltr_title"), Literal).Text = "<a href='/login.html' style='text-decoration: none;color:#323232;font-size:" & IIf(.Item("title").ToString.Length < 70, "22px", IIf(.Item("title").ToString.Length < 150, "18px", IIf(.Item("title").ToString.Length < 200, "16px", "14px"))) & ";'>" & .Item("title").ToString & "</a>"
                'End If

                If .Item("filename").ToString <> "" Then
                    CType(e.Item.FindControl("img_auction_image"), Image).ImageUrl = "/upload/auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString.Replace(".", "_230.")
                    str = "SELECT A.auction_id,convert(varchar,A.auction_id)+'/'+convert(varchar,A.stock_image_id)+'/'+A.filename as path, b.title "
                    str = str & " from tbl_auction_stock_images A  WITH (NOLOCK) inner join tbl_auctions b  WITH (NOLOCK) on a.auction_id=b.auction_id where A.auction_id=" & auction_id
                    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).HRef = "/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t")
                    'If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                    '    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).HRef = "/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t")
                    'Else
                    '    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).HRef = "/login.html"
                    'End If


                    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).Title = .Item("title")
                Else
                    CType(e.Item.FindControl("img_auction_image"), Image).ImageUrl = "/images/imagenotavailable.gif"
                    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).HRef = "/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t")
                    'If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                    '    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).HRef = "/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t")
                    'Else
                    '    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).HRef = "/login.html"
                    'End If
                    'CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).HRef = "/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t")
                    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).Title = .Item("title")
                End If



                If .Item("auction_status") = 2 Then 'upcoming
                    CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>MORE INFO</a></div>"
                    'If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                    '    CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>MORE INFO</a></div>"
                    'Else
                    '    CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/login.html'>MORE INFO</a></div>"
                    'End If

                    CType(e.Item.FindControl("ltr_auction_status"), Literal).Text = "Starts in"
                ElseIf .Item("auction_status") = 3 Then 'over
                    CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>MORE INFO</a></div>"
                    'If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                    '    CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>MORE INFO</a></div>"
                    'Else
                    '    CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/login.html'>MORE INFO</a></div>"
                    'End If

                    CType(e.Item.FindControl("ltr_auction_status"), Literal).Text = "&nbsp;"

                Else 'running

                    If .Item("auction_type_id") = 1 Then
                        CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnblu'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>BUY IT NOW</a></div>"
                        'If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                        '    CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnblu'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>BUY IT NOW</a></div>"
                        'Else
                        '    CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnblu'><a href='/Login.html'>BUY IT NOW</a></div>"
                        'End If

                    ElseIf .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then
                        CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>BID NOW</a></div>"
                        'If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                        '    CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>BID NOW</a></div>"
                        'Else
                        '    CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/Login.html'>BID NOW</a></div>"
                        'End If

                    Else
                        CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>OFFER</a></div>"
                        'If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
                        '    CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>OFFER</a></div>"
                        'Else
                        '    CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/Login.html'>OFFER</a></div>"
                        'End If
                    End If

                    CType(e.Item.FindControl("ltr_auction_status"), Literal).Text = "Ends in"

                End If

            End With

        Else
            e.Item.Visible = False
        End If
        dtTable = Nothing
    End Sub
    Protected Sub rpt_auctions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_auctions.ItemDataBound
        setAuction(e, e.Item.DataItem("auction_id"))
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            If e.Item.DataItem("auction_type_id") <> 4 And e.Item.DataItem("start_price") > e.Item.DataItem("bidder_max_amount") And e.Item.DataItem("bidder_max_amount") <> 0 Then
                CType(e.Item.FindControl("lit_increase_bid_limit"), Literal).Text = "<div class='bidding_limit'><a href='javascript:void(0);' onclick='return open_increase_limit();'><img src='/Images/bidding_limit.png'></a></div>"
            Else
                CType(e.Item.FindControl("lit_increase_bid_limit"), Literal).Text = ""
            End If
        Else
            CType(e.Item.FindControl("lit_increase_bid_limit"), Literal).Text = ""
        End If

    End Sub

    Protected Sub FillSideBar()
        Dim sQry As String = ""
        sQry = "SELECT bucket_filter_id, caption FROM tbl_bucket_filter with (nolock) WHERE is_active = 1 and bucket_filter_id in (select bucket_filter_id from tbl_bucket_filter_value with (nolock) where is_active = 1)"
        Dim dtBucket As New DataTable()
        Dim dtBucketValue As New DataTable()
        dtBucket = SqlHelper.ExecuteDatatable(sQry)
        ltrSideBar.Text = "<table border='0' cellspacing='0' cellpadding='0'>"
        If dtBucket.Rows.Count > 0 Then
            For iCtr = 0 To dtBucket.Rows.Count - 1
                If hid_group_ids.Value = "" Then
                    hid_group_ids.Value = dtBucket.Rows(iCtr).Item("bucket_filter_id")
                Else
                    hid_group_ids.Value = hid_group_ids.Value & "," & dtBucket.Rows(iCtr).Item("bucket_filter_id")
                End If

                ltrSideBar.Text &= "<tr><td id='" & dtBucket.Rows(iCtr).Item("bucket_filter_id") & "' style='text-align:left;padding-bottom:15px;'><span class='sidebarheading'>" & dtBucket.Rows(iCtr).Item("caption") & "</span>"
                sQry = "SELECT caption FROM tbl_bucket_filter_value with (nolock) WHERE bucket_filter_id = " & dtBucket.Rows(iCtr).Item("bucket_filter_id") & " AND is_active = 1"
                dtBucketValue = SqlHelper.ExecuteDatatable(sQry)
                If dtBucketValue.Rows.Count > 0 Then
                    ltrSideBar.Text &= "<table border='0' cellspacing='30' cellpadding='30'>"
                    For ictr2 = 0 To dtBucketValue.Rows.Count - 1
                        ltrSideBar.Text &= "<tr><td style='padding-bottom:3px; padding-top:3px;'><input type='checkbox' name='BucketValue' value='" & dtBucket.Rows(iCtr).Item("bucket_filter_id") & ";" & dtBucketValue.Rows(ictr2).Item("caption") & "' onclick='getValueUsingClass();' class='chk'></td><td style='padding-bottom:3px; padding-top:3px;'>" & dtBucketValue.Rows(ictr2).Item("caption") & "</td></tr>"
                    Next
                    ltrSideBar.Text &= "</table>"
                End If

                ltrSideBar.Text &= "</td></tr>"
            Next
        End If

        ltrSideBar.Text &= "</table>"



    End Sub

End Class
