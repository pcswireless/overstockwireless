﻿Imports Telerik.Web.UI

Partial Class Auctions
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            If CommonCode.Fetch_Cookie_Shared("is_backend") = "1" Then
                Response.Write("<script language='javascript'>top.location.href = '/Backend_Home.aspx?t=1';</script>")
            End If
        End If

        If Not Page.IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString.Get("view")) Then
                If Request.QueryString.Get("view") = "list" Then
                    bindGrid(False)
                    lnk_view.Text = "<a href='/Auctions.aspx?t=" & Request.QueryString.Get("t") & IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), "", "&c=" & Request.QueryString.Get("c")) & IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), "", "&p=" & Request.QueryString.Get("p")) & "&view=grid'>Switch to Grid View</a>"
                Else
                    lnk_view.Text = "<a href='/Auctions.aspx?t=" & Request.QueryString.Get("t") & IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), "", "&c=" & Request.QueryString.Get("c")) & IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), "", "&p=" & Request.QueryString.Get("p")) & "&view=list'>Switch to List View</a>"
                    bindGrid(True)
                End If
            Else
                lnk_view.Text = "<a href='/Auctions.aspx?t=" & Request.QueryString.Get("t") & IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), "", "&c=" & Request.QueryString.Get("c")) & IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), "", "&p=" & Request.QueryString.Get("p")) & "&view=grid'>Switch to Grid View</a>"
                bindGrid(False)
            End If
            lnk_view1.Text = lnk_view.Text
            showMessage()
        End If

    End Sub
    Private Sub showMessage()
        rep_message.DataSource = SqlHelper.ExecuteDataTable("select [message_board_id],[title],[description] from [tbl_message_boards]  M " & _
        "where dbo.message_board_status(ISNULL(display_from,'1/1/1900'),ISNULL(display_to,'1/1/1900'))='Running' and is_active=1 " & _
        "and " & CommonCode.Fetch_Cookie_Shared("buyer_id") & " in (select buyer_id from fn_get_message_invited_buyers((select top 1 invitation_id from tbl_message_invitations where message_board_id=M.message_board_id)))")
        rep_message.DataBind()

        If rep_message.Items.Count > 0 Then
            div_topmsg.Visible = True
            rep_message.Visible = True
        Else
            div_topmsg.Visible = False
            rep_message.Visible = False
        End If

    End Sub
    Private Sub bindGrid(ByVal is_grid_view As Boolean)
        Dim obj As New Auction
        
        Dim dv As DataView = obj.fetch_auction_Listing(Request.QueryString.Get("t"), IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), 0, Request.QueryString.Get("c")), IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), 0, Request.QueryString.Get("p")), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("user_id")), CommonCode.Fetch_Cookie_Shared("user_id"), 0), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("buyer_id")), CommonCode.Fetch_Cookie_Shared("buyer_id"), 0), SqlHelper.of_FetchKey("frontend_login_needed")).Tables(0).DefaultView
        If dv.Count > 0 Then

            If is_grid_view Then

                rpt_auctions_gridview.DataSource = dv
                rpt_auctions_gridview.DataBind()
                rpt_auctions_gridview.Visible = True
                rpt_auctions.Visible = False

            Else

                rpt_auctions.DataSource = dv
                rpt_auctions.DataBind()
                rpt_auctions.Visible = True
                rpt_auctions_gridview.Visible = False

            End If

            pnl_noItem.Visible = False
            lnk_view.Visible = True
            lnk_view1.Visible = True

        Else

            rpt_auctions_gridview.Visible = False
            rpt_auctions.Visible = False
            pnl_noItem.Visible = True
            lnk_view.Visible = False
            lnk_view1.Visible = False

        End If

        dv.Dispose()
        obj = Nothing
    End Sub

    

End Class
