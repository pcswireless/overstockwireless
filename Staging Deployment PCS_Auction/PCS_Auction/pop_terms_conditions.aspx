﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="pop_terms_conditions.aspx.vb" Inherits="pop_terms_conditions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UserControls/terms.ascx" TagName="Terms" TagPrefix="UC3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Terms and Conditions</title>
      <link href="/Style/stylesheet.css" rel="stylesheet" type="text/css" /> 
      <script type="text/javascript">

          function PrintThisPage() {
              //alert(document.getElementById("termsframe"));
              // alert(window.frames["termsframe"]);
              window.frames["termsframe"].focus();
              window.frames["termsframe"].print();
              return false;
          } 

    </script>
    <script type="text/javascript">

        /***********************************************
        * Different CSS depending on OS (mac/pc)- © Dynamic Drive (www.dynamicdrive.com)
        * This notice must stay intact for use
        * Visit http://www.dynamicdrive.com/ for full source code
        ***********************************************/
        ///////No need to edit beyond here////////////

        var mactest = navigator.userAgent.indexOf("Mac") != -1


        if (mactest) {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet_mac.css"/>');
        }
        else {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet.css"/>');
        }
  </script>
</head>
<body>
<div class="products2">
        <div class="printbx_home" style="visibility: visible; width:60px; margin-top:20px;">
            <ul>
                   <li class="printbx_12"><a href="javascript:window.print();" >Print</a></li>
              
                <%--<li>
                            <asp:Literal ID="lit_pdf" runat="server" /></li>--%>
            </ul>
        </div>
    </div>
    <form id="form1" runat="server">

     <h1 class="pgtitle" style="margin-top: 10px; padding-left :10px;">
            Terms and Conditions</h1>
          
     <div class="detail">
          <UC3:Terms runat="server" ID="UC_Terms"></UC3:Terms>
           <div>
                        <div id='dv_askpop_ask' style="margin-top:0px; padding-left:10px;">
                            <input type="button" name="" id="ImageButtonCancelMsg" onclick="javascript:window.close();"
                                value="Close" />
                        </div>
                    </div>
                    </div>
    </form>
    <script type ="text/javascript" >
        window.focus();
    </script>
</body>
</html>
