﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="email_master_list.aspx.vb" Inherits="Master_email_master_list" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Skin="Simple" />
    <input type="hidden" id="hid_manufacturer_combo_id" name="hid_manufacturer_combo_id"
        value="0" />
    <asp:TextBox ID="txt_test" runat="server" Visible="false"></asp:TextBox>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" alt="" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Email Template
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxPanel ID="pnl1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                    <telerik:RadGrid ID="RadGrid_Email" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                        AllowAutomaticInserts="True" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True"
                        AllowSorting="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" AllowFilteringByColumn="true"
                        AllowMultiRowSelection="false" AllowMultiRowEdit="false" Skin="Vista" ShowGroupPanel="False" EnableLinqExpressions="false">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <HeaderStyle BackColor="#BEBEBE" />
                        <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="master_email_id"
                            DataSourceID="SqlDataSource1" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                            SkinID="Vista" ItemStyle-Height="40"  AlternatingItemStyle-Height="40">
                            <CommandItemStyle BackColor="#E1DDDD" />
                            <NoRecordsTemplate>
                                Email not available
                            </NoRecordsTemplate>
                            <CommandItemSettings ShowAddNewRecordButton="false" />
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                            </SortExpressions>
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="Name" SortExpression="name" UniqueName="name"
                                    DataField="name" EditFormHeaderTextFormat="Title Name" AllowFiltering="true" FilterControlWidth="250">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("name")%>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Email Subject" SortExpression="email_subject" FilterControlWidth="200"
                                    UniqueName="email_subject" DataField="email_subject" AllowFiltering="true">
                                    <ItemTemplate>
                                        <%# Eval("email_subject")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                 <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false"
                                    ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <a href="javascript:void(0);" onclick="redirectIframe('','','/master/email_master_detail.aspx?i=<%# Eval("master_email_id")%>')">
                                            <img src="/images/edit.png" style="border: none;" alt="Edit" /></a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                    </telerik:RadGrid>
                </telerik:RadAjaxPanel>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="select t.[master_email_id],t.[email_name] as name,t.[email_subject] from [tbl_master_Emails] t where t.is_active=1">
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
