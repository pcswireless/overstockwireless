﻿Imports Telerik.Web.UI
Partial Class Master_email_template_master
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        RadGrid_Titles.Attributes.Add("style", "word-break:break-all;word-wrap:break-word")
    End Sub
    'Sub Checkname(ByVal source As Object, ByVal e As ServerValidateEventArgs)
    '    Dim strQuery As String = ""
    '    Dim ds As New DataSet
    '    Dim txt_name As New TextBox
    '    Dim but_grd_submit As New ImageButton
    '    Dim hd_id As New HiddenField
    '    hd_id = CType(((source).NamingContainer).FindControl("hd_id"), HiddenField)
    '    but_grd_submit = CType(((source).NamingContainer).FindControl("but_grd_submit"), ImageButton)
    '    If but_grd_submit.AlternateText = "Update" Then
    '        txt_name = CType(((source).NamingContainer).FindControl("txt_grd_name"), TextBox)
    '        strQuery = "select email_template_id from tbl_master_email_template where email_template_name='" & txt_name.Text.Trim() & "'"
    '        ds = SqlHelper.ExecuteDataset(strQuery)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            If ds.Tables(0).Rows(0)(0) = hd_id.Value Then
    '                e.IsValid = True
    '            Else
    '                e.IsValid = False
    '            End If
    '        Else
    '            e.IsValid = True
    '        End If
    '    Else
    '        txt_name = CType(((source).NamingContainer).FindControl("txt_grd_name"), TextBox)
    '        strQuery = "select email_template_name from tbl_master_email_template where email_template_name='" & txt_name.Text.Trim() & "'"
    '        ds = SqlHelper.ExecuteDataset(strQuery)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            e.IsValid = False
    '        Else
    '            e.IsValid = True
    '        End If

    '    End If
    'End Sub

    Protected Sub RadGrid_Titles_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrid_Titles.ItemCommand

        If (e.CommandName = Telerik.Web.UI.RadGrid.InitInsertCommandName) Then

            'cancel the default operation
            e.Canceled = True
            'Prepare an IDictionary with the predefined values
            Dim newValues As System.Collections.Specialized.ListDictionary = New System.Collections.Specialized.ListDictionary()
            'set default checked state for checkbox inside the EditItemTemplate
            newValues("is_active") = False
            'newValues("Is_Disabled") = False
            'Insert the item and rebind
            e.Item.OwnerTableView.InsertItem(newValues)
        End If

    End Sub

    
End Class
