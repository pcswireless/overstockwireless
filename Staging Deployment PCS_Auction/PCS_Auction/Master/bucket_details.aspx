﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="bucket_details.aspx.vb" Inherits="Master_bucket_details" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/Log/UserControls/SystemLogLink.ascx" TagName="SystemLogLink"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <asp:HiddenField ID="hd_bucket_id" runat="server" Value="0" />
    <telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
        <script type="text/javascript">
            function Cancel_Ajax(sender, args) {
                // alert(args.get_eventTarget().toString);
                args.set_enableAjax(false);

            }

            function ButtonPostback(sender, args) {
                // alert(args.get_eventTarget().toString);
                //args.set_enableAjax(false)
                //args.set_enableAjax(false);
                if (args.get_eventTarget() == "<%= but_basic_cancel.UniqueID %>") {
                    // args.set_enableAjax(false);

                }
                if (args.get_eventTarget() == "<%= but_basic_save.UniqueID %>") {
                    //  alert(args.get_eventTarget().toString);
                    args.set_enableAjax(false);
                    //                    alert(args.get_eventTarget().toString);
                }
                if (args.get_eventTarget() == "<%= but_basic_edit.UniqueID %>") {
                    // args.set_enableAjax(false);

                }
                if (args.get_eventTarget() == "<%= but_basic_update.UniqueID %>") {
                    //  alert(args.get_eventTarget().toString);
                    args.set_enableAjax(false);
                    //                    alert(args.get_eventTarget().toString);
                }
            }




            function popup_PostBackFunction() {
                //alert(document.getElementById('<%=hid_popup_status.ClientID %>').value);
                document.getElementById('<%=but_bind_user_bidder.ClientID %>').disabled = false;
                document.getElementById('<%=but_bind_user_bidder.ClientID %>').click();

                //__doPostBack('<%=but_bind_user_bidder.ClientID %>', ''); document.getElementById("b1").disabled=false;
            }

            function Opendetail(page_name) {

                var width = "720";
                var height = "600";

                var left = (screen.width - width) / 2;
                var top = (screen.height - height) / 2;

                window.open(page_name, '_open_email_preview', 'left=' + left + ',top=' + top + ',width=' + width + ',height=' + height + ',status=1,scrollbars=1')
                window.focus();

            }


            function open_buyer_assign(sid) {
                window.open('../users/salerep_bidders.aspx?t=b&i=' + sid, '_bucketbidder', 'top=' + ((screen.height - 480) / 2) + ', left=' + ((screen.width - 840) / 2) + ', height=620, width=820,scrollbars=yes,toolbars=no,resizable=0;');
                window.focus();
                return false;

            }

            function open_buyer_assign(BucketId, BucketItemId) {
              window.open('../users/salerep_bidders.aspx?t=bi&i=' + BucketId +'&ii='+ BucketItemId, '_bucketbidder', 'top=' + ((screen.height - 480) / 2) + ', left=' + ((screen.width - 840) / 2) + ', height=620, width=820,scrollbars=yes,toolbars=no,resizable=0;');
              window.focus();
              return false;

            }


        </script>
    </telerik:RadScriptBlock>
    <asp:HiddenField runat="server" ID="hid_popup_status" />
    <asp:Button ID="but_bind_user_bidder" runat="server" BorderStyle="None" BackColor="Transparent"
        ForeColor="Transparent" Width="0" Height="0" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="but_basic_edit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic_2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_basic_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic_2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_basic_cancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic_2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="but_basic_save">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel_Basic_2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
           
           
            <telerik:AjaxSetting AjaxControlID="RadGrid_Bidder">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid_Bidder" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div style="float: left;">
                    <div class="pageheading">
                        <asp:Literal ID="page_heading" runat="server" Text="New Bucket"></asp:Literal></div>
                </div>
                <div style="float: right; padding-right: 20px; width: 180px;">
                    <div style="float: left;">
                        <uc4:SystemLogLink ID="UC_System_log_link" runat="server" />
                    </div>
                    <div style="float: left; padding-left: 10px;">
                        <a href="javascript:void(0);" onclick="javascript:open_help('3');">
                            <img src="/Images/help.gif" border="none" alt="" />
                        </a>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="PageTab">
                    <span>
                        <%= IIf(String.IsNullOrEmpty(Request.QueryString.Get("i")), "Add Bucket Details", "Edit Bucket")%></span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="PageTabContentEdit">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td class="tdTabItem">
                            <asp:Panel ID="Panel1_Header" runat="server" CssClass="pnlTabItemHeader">
                                <div class="tabItemHeader" style="background: url(/Images/basic_info.gif) no-repeat;
                                    background-position: 10px 0px;">
                                    Bucket Information
                                </div>
                                <asp:Image ID="Image1" runat="server" ImageAlign="left" ImageUrl="/Images/up_Arrow.gif"
                                    CssClass="panelimage" />
                            </asp:Panel>
                            <ajax:CollapsiblePanelExtender ID="cpe1" BehaviorID="cpe1" runat="server" Enabled="True"
                                TargetControlID="Panel1_Content" CollapseControlID="Panel1_Header" ExpandControlID="Panel1_Header"
                                Collapsed="false" ImageControlID="Image1" CollapsedImage="/Images/down_Arrow.gif"
                                ExpandedImage="/Images/up_Arrow.gif">
                            </ajax:CollapsiblePanelExtender>
                            <%--  <ajax:CollapsiblePanelExtender ID="cpe11" BehaviorID="cpe1" runat="server" Enabled="True"
                                TargetControlID="Panel1_Content1button" CollapseControlID="Panel1_Header" ExpandControlID="Panel1_Header"
                                Collapsed="false" ImageControlID="Image1" CollapsedImage="/Images/down_Arrow.gif"
                                CollapsedSize="0" ExpandedImage="/Images/up_Arrow.gif">
                            </ajax:CollapsiblePanelExtender>--%>
                            <asp:Panel ID="Panel1_Content" runat="server" CssClass="collapsePanel">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel_Basic" runat="server" CssClass="TabGrid" LoadingPanelID="RadAjaxLoadingPanel1">
                                    <div style="padding-left: 5px;">
                                        <asp:Label ID="lbl_error" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                                        <asp:HiddenField ID="hid_seller_active_modify" runat="server" Value="1" />
                                    </div>
                                    <table cellpadding="0" cellspacing="2" border="0" width="838px">
                                        <tr>
                                            <td class="caption" style="width: 145px;">
                                                Bucket Name&nbsp;<span id="span_basic_bucket_name" runat="server" class="req_star">*</span>
                                            </td>
                                            <td style="width: 500px;" class="details">
                                                <asp:TextBox ID="txt_basic_bucket_name" Width="150" MaxLength="100" CssClass="inputtype"
                                                    runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Required_txt_basic_bucket_name" runat="server" Font-Size="10px"
                                                    ControlToValidate="txt_basic_bucket_name" ValidationGroup="_basic" Display="Dynamic"
                                                    ErrorMessage="<br>Bucket Name Required"></asp:RequiredFieldValidator>
                                                <asp:Label ID="lbl_basic_bucket_name" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                Bucket Description&nbsp;<span id="span_basic_bucket_1" runat="server" class="req_star">*</span>
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_basic_bucket_1" TextMode="MultiLine" Height="150px" Width="550px"
                                                    MaxLength="500" CssClass="inputtype" runat="server"></asp:TextBox>
                                                <asp:Label ID="lbl_basic_bucket_1" runat="server" />
                                                <asp:RequiredFieldValidator ID="rfv_address1" runat="server" ControlToValidate="txt_basic_bucket_1"
                                                    ValidationGroup="_basic" Display="Dynamic" ErrorMessage="<br />Bucket Description Required"
                                                    CssClass="error"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regular_exp_description" runat="server" ControlToValidate="txt_basic_bucket_1"
                                                    ValidationExpression="^[\s\S]{0,500}$" ValidationGroup="_basic" ErrorMessage="<br>Maximum 500 characters are allowed">
                                                </asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                S.No&nbsp;<span id="span1" runat="server" class="req_star">*</span>
                                            </td>
                                            <td class="details">
                                                <asp:TextBox ID="txt_bckt_sno" Width="50" CssClass="inputtype" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfv_bckt_sno" runat="server" Font-Size="10px" ControlToValidate="txt_bckt_sno"
                                                    ValidationGroup="_basic" Display="Dynamic" ErrorMessage="<br>Bucket S.No Required"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="rev_bckt_sno" runat="server" Font-Size="10px"
                                                    ControlToValidate="txt_bckt_sno" ErrorMessage="<br>Please Enter Only Numbers"
                                                    Display="Dynamic" ValidationExpression="^\d+$" ValidationGroup="_basic"></asp:RegularExpressionValidator>
                                                <asp:Label ID="lbl_bckt_sno" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                Is Active
                                            </td>
                                            <td class="details">
                                                <asp:CheckBox ID="chk_basic_active" runat="server" />
                                                <asp:Label ID="lbl_basic_active" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption">
                                                Show in registration
                                            </td>
                                            <td class="details">
                                                <asp:CheckBox ID="chkShowInRegistration" runat="server" />
                                                <asp:Label ID="lblShowInRegistration" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </telerik:RadAjaxPanel>
                            </asp:Panel>
                        </td>
                        <td class="fixedcolumn" valign="top">
                            <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                            <asp:Panel ID="Panel1_Content1button" runat="server" CssClass="collapsePanel">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel_Basic_2" runat="server" ClientEvents-OnRequestStart="ButtonPostback"
                                    ClientEvents-OnResponseEnd="Cancel_Ajax">
                                    <asp:Panel ID="Panel1_Content1button_per" runat="server">
                                        <div class="addButton">
                                            <asp:ImageButton ID="but_basic_save" ValidationGroup="_basic" runat="server" AlternateText="Save"
                                                ImageUrl="/images/save.gif" />
                                           <%-- <asp:ImageButton ID="but_basic_update" ValidationGroup="_basic" runat="server" AlternateText="Update"
                                                ImageUrl="/images/update.gif" OnClientClick="return chk_aprv();" />--%>
                                                 <asp:ImageButton ID="but_basic_update" ValidationGroup="_basic" runat="server" AlternateText="Update"
                                                ImageUrl="/images/update.gif" />

                                            <asp:ImageButton ID="but_basic_edit" runat="server" AlternateText="Edit" ImageUrl="/images/edit.gif" />
                                            <%--<br />
                                            <br />
                                            <asp:ImageButton ID="but_preview" runat="server" AlternateText="Preview" ImageUrl="/images/preview.png" />
                                            --%>
                                        </div>
                                        <div class="cancelButton" id="div_basic_cancel" runat="server">
                                            <asp:ImageButton ID="but_basic_cancel" CausesValidation="false" runat="server" AlternateText="Cancel"
                                                ImageUrl="/images/cancel.gif" /></div>
                                    </asp:Panel>
                                </telerik:RadAjaxPanel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <%--Collapse Panel Bottom Starts--%>
                    <asp:Panel ID="pnl_edit_mode2" runat="server">
                        <tr id="trBucketItems" runat="server">
                            <td class="tdTabItem">
                                <asp:Panel ID="Panel2_Header" runat="server" Visible="false" CssClass="pnlTabItemHeader">
                                    <div class="tabItemHeader" style="background: url(/Images/bidders_icon.gif) no-repeat;
                                        background-position: 10px 0px;">
                                        Bucket Items
                                    </div>
                                    <asp:Image ID="Image2" runat="server" ImageAlign="left" ImageUrl="/Images/down_Arrow.gif"
                                        CssClass="panelimage" />
                                </asp:Panel>
                                <ajax:CollapsiblePanelExtender ID="cpe2" BehaviorID="cpe2" runat="server" Enabled="True"
                                    ImageControlID="Image2" TargetControlID="Panel2_Content" CollapseControlID="Panel2_Header"
                                    ExpandControlID="Panel2_Header" Collapsed="True" CollapsedImage="~/Images/down_Arrow.gif"
                                    ExpandedImage="~/Images/up_Arrow.gif">
                                </ajax:CollapsiblePanelExtender>
                                <asp:Panel ID="Panel2_Content" runat="server" CssClass="collapsePanel">
                                    <div class="TabGrid">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="font-size: 11px; font-weight: bold; padding-left: 15px;" colspan="2">
                                                    &nbsp;Items
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="sub_details">
                                                  <telerik:RadAjaxPanel ID="pnl1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                                                      <telerik:RadGrid ID="RadGrid_Bucket_Items"  EnableLinqExpressions ="false" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                                                        AllowAutomaticInserts="True" AllowFilteringByColumn ="true"  PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True"
                                                        AllowSorting="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                                                        AllowMultiRowSelection="false" AllowMultiRowEdit="false" Skin="Vista" ShowGroupPanel="False">
                                                          <PagerStyle Mode="NextPrevAndNumeric" />
                                                          <HeaderStyle BackColor="#BEBEBE" />
                                                          <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="bucket_value_id" DataSourceID="SqlDataSource1"
                                                              HorizontalAlign="NotSet" AutoGenerateColumns="False" SkinID="Vista" ItemStyle-Height="40"
                                                              EditMode="EditForms" AlternatingItemStyle-Height="40">
                                                              <CommandItemStyle BackColor="#E1DDDD" />
                                                              <NoRecordsTemplate>
                                                                  Bucket items not available
                                                              </NoRecordsTemplate>
                                                              <SortExpressions>
                                                                  <telerik:GridSortExpression FieldName="bucket_value" SortOrder="Ascending" />
                                                              </SortExpressions>
                                                              <CommandItemSettings AddNewRecordText="Add New Bucket Item" />
                                                              <Columns>
                                                                  <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                      EditImageUrl="/Images/edit_grid.gif">
                                                                      <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                                                  </telerik:GridEditCommandColumn>
                                                                  <telerik:GridTemplateColumn HeaderText="Bucket Value Id" SortExpression="bucket_value_id" UniqueName="bucket_value_id"
                                                                      DataField="bucket_value_id" EditFormHeaderTextFormat="Bucket Value Id">
                                                                      <ItemTemplate>
                                                                          <%# Eval("bucket_value_id")%>
                                                                      </ItemTemplate>
                                                                  </telerik:GridTemplateColumn>

                                                                  <telerik:GridTemplateColumn HeaderText="Item Name" SortExpression="bucket_value" UniqueName="bucket_value"
                                                                      DataField="bucket_value" EditFormHeaderTextFormat="Item Name">
                                                                      <ItemTemplate>
                                                                          <asp:Label ID="lbl_BucketValue" runat="server" Text='<%# Eval("bucket_value")%>' ></asp:Label>
                                                                      </ItemTemplate>
                                                                  </telerik:GridTemplateColumn>
    
                                                                  <telerik:GridTemplateColumn UniqueName="is_active" DataType="System.Boolean" DataField="is_active"
                                                                      SortExpression="is_active" HeaderText="Is Active">
                                                                      <FilterTemplate>
                                                                          <telerik:RadComboBox ID="RadComboBo_Chk_is_active" runat="server" OnClientSelectedIndexChanged="ActiveIndexChanged"
                                                                              Width="120px" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("is_active").CurrentFilterValue %>'>
                                                                              <Items>
                                                                                  <telerik:RadComboBoxItem Text="All" Value="" />
                                                                                  <telerik:RadComboBoxItem Text="Active" Value="True" />
                                                                                  <telerik:RadComboBoxItem Text="In-Active" Value="False" />
                                                                              </Items>
                                                                          </telerik:RadComboBox>
                                                                          <telerik:RadScriptBlock ID="RadScriptBlock_is_active" runat="server">
                                                                              <script type="text/javascript">
                                                                                function ActiveIndexChanged(sender, args) {
                                                                                  var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                                                    if (args.get_item().get_value() == "") {
                                                                                      tableView.filter("is_active", args.get_item().get_value(), "NoFilter");
                                                                                    }
                                                                                    else {
                                                                                      tableView.filter("is_active", args.get_item().get_value(), "EqualTo");
                                                                                    }
                                                                                  }
                                                                              </script>
                                                                          </telerik:RadScriptBlock>
                                                                      </FilterTemplate>
                                                                      <ItemTemplate>
                                                                          <img src='<%#IIf(Eval("is_active"),"/Images/true.gif","/Images/false.gif") %>' style="border: none;"
                                                                          alt="" />
                                                                      </ItemTemplate>
                                                                  </telerik:GridTemplateColumn>

                                                                  <telerik:GridTemplateColumn HeaderText="Assign Bidder" EditFormHeaderTextFormat="Assign Bidder" AllowFiltering="false" HeaderStyle-Width="130px" Visible="false">
                                                                      <ItemTemplate >
                                                                          <a href="javascript:open_buyer_assign('<%# Eval("bucket_id")%>', '<%# Eval("bucket_value_id")%>')">
                                                                          Assign Bidder</a>
                                                                          
                                                                      </ItemTemplate>
                                                                  </telerik:GridTemplateColumn>


                                                                  <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"   HeaderText="Delete" 
                                                                      ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                                                      CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                                      <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="50" />
                                                                      <HeaderStyle  HorizontalAlign="Center"  Width="50" />
                                                                  </telerik:GridButtonColumn>
                                                              </Columns>

                                                              
                                                              <EditFormSettings InsertCaption="New Items" EditFormType="Template">
                                                                  <FormTemplate>
                                                                      <table id="tbl_grd_item_edit" cellpadding="0" cellspacing="2" border="0" width="800px">
                                                                          <tr>
                                                                              <td style="font-weight: bold; padding-top: 10px;" colspan="6">
                                                                                  Item
                                                                              </td>
                                                                          </tr>
                                                                          <tr>
                                                                              <td class="caption" style="width: 135px;">
                                                                                  Item Name&nbsp;<span class="req_star">*</span>
                                                                              </td>
                                                                              <td style="width: 200px;" class="details">
                                                                                  <asp:TextBox ID="txt_grd_name" runat="server" Text='<%#Bind("bucket_value")%>' CssClass="inputtype" />
                                                                                  <asp:RequiredFieldValidator ID="Req_txt_grd_name" runat="server" Font-Size="10px"
                                                                                      ValidationGroup="grd_items" ControlToValidate="txt_grd_name" Display="Dynamic"
                                                                                      ErrorMessage="<br> Item Name Required"></asp:RequiredFieldValidator>
                                                                               </td>
                                                                              <td class="caption" style="width: 80px;">
                                                                                  Is Active
                                                                              </td>
                                                                              <td style="width: 20px;" class="details">
                                                                                  <asp:CheckBox ID="chk_basic_active" runat="server" Checked='<%#Bind("is_active") %>'/>
                                                                                   
                                                                               </td>
                                                                              <td align="right" colspan="2">
                                                                                  <asp:ImageButton ID="but_grd_submit" CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem),"PerformInsert","Update") %>'
                                                                                      ValidationGroup="grd_items" OnClientClick="return ValidationGroupEnable('grd_items', true);"
                                                                                      AlternateText='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "Save" , "Update") %>'
                                                                                      runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif" , "/images/update.gif") %>' />
                                                                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                  <asp:ImageButton ID="but_grd_cancel" CausesValidation="false" CommandName="Cancel"
                                                                                      runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                                                              </td>
                                                                          </tr>
                                                                      </table>
                                                                  </FormTemplate>
                                                              </EditFormSettings>
                                                          </MasterTableView>
                                                          <ClientSettings>
                                                              <Selecting AllowRowSelect="True"></Selecting>
                                                          </ClientSettings>
                                                      </telerik:RadGrid>
                                                  </telerik:RadAjaxPanel>
                                                  <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                                      ProviderName="System.Data.SqlClient" SelectCommand="SELECT bucket_value_id, bucket_id, bucket_value, ISNULL(is_active, 0) as is_active FROM tbl_master_bucket_values WHERE bucket_id = @bucket_id"
                                                      DeleteCommand="DELETE FROM tbl_master_bucket_values WHERE bucket_value_id = @bucket_value_id"
                                                      InsertCommand="INSERT INTO tbl_master_bucket_values(bucket_id, bucket_value, is_active) VALUES (@bucket_id, @bucket_value, @is_active)" 
                                                      UpdateCommand="UPDATE tbl_master_bucket_values SET bucket_value= @bucket_value, is_active = @is_active WHERE bucket_value_id= @bucket_value_id">
                                                      <SelectParameters>
                                                          <asp:ControlParameter Name="bucket_id" Type="Int32" ControlID="hd_bucket_id" PropertyName="Value" />
                                                      </SelectParameters>
                                                      <DeleteParameters>
                                                          <asp:Parameter Name="bucket_value_id" Type="Int32" />
                                                      </DeleteParameters>
                                                      <UpdateParameters>
                                                          <asp:Parameter Name="bucket_value_id" Type="Int32" />
                                                          <asp:Parameter Name="bucket_value" Type="String" />
                                                          <asp:Parameter Name="is_active" Type="Boolean"   />
                                                      </UpdateParameters>
                                                      <InsertParameters>
                                                          <asp:ControlParameter Name="bucket_id" Type="Int32" ControlID="hd_bucket_id" PropertyName="Value" />
                                                          <asp:Parameter Name="is_active" Type="Boolean"  />
                                                          <asp:Parameter Name="bucket_value" Type="String" />
                                                      </InsertParameters>
                                                  </asp:SqlDataSource>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </td>
                            <td class="fixedcolumn" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                    </asp:Panel>


                </table>
            </td>
        </tr>
    </table>
</asp:Content>
