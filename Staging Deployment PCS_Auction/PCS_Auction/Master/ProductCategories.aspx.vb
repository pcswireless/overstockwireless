﻿Imports Telerik.Web.UI
Partial Class Master_ProductCategories
    Inherits System.Web.UI.Page
    Sub Checkname(ByVal source As Object, ByVal e As ServerValidateEventArgs)
        Dim strQuery As String = ""
        Dim ds As New DataSet
        Dim txt_name As New TextBox
        Dim but_grd_submit As New ImageButton
        Dim hd_id As New HiddenField
        hd_id = CType(((source).NamingContainer).FindControl("hd_id"), HiddenField)
        but_grd_submit = CType(((source).NamingContainer).FindControl("but_grd_submit"), ImageButton)
        If but_grd_submit.AlternateText = "Update" Then
            txt_name = CType(((source).NamingContainer).FindControl("txt_grd_name"), TextBox)
            strQuery = "select product_catetory_id from tbl_master_product_categories where name='" & txt_name.Text.Trim() & "'"
            ds = SqlHelper.ExecuteDataset(strQuery)
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)(0) = hd_id.Value Then
                    e.IsValid = True
                Else
                    e.IsValid = False
                End If
            Else
                e.IsValid = True
            End If
        Else
            txt_name = CType(((source).NamingContainer).FindControl("txt_grd_name"), TextBox)
            strQuery = "select name from tbl_master_product_categories where name='" & txt_name.Text.Trim() & "'"
            ds = SqlHelper.ExecuteDataset(strQuery)
            If ds.Tables(0).Rows.Count > 0 Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If

        End If

    End Sub

    Protected Sub RadGrid_product_category_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid_product_category.ItemDataBound
        If e.Item.ItemType = GridItemType.AlternatingItem Or e.Item.ItemType = GridItemType.Item Then
            Dim hd_count As New HiddenField
            hd_count = CType(e.Item.Cells(1).FindControl("hd_count"), HiddenField)
            If hd_count.Value > 0 Then
                Dim cell As TableCell = e.Item.Cells(6)
                Dim ctl As Control = cell.Controls(0)
                If TypeOf ctl Is ImageButton Then
                    TryCast(ctl, ImageButton).Attributes.Add("onclick", "alert('Name : " & CType(e.Item.Cells(3).FindControl("lbl_name"), Label).Text & " can`t be delete, because its used in another Place.');return false;")
                End If
            End If
        End If
    End Sub
End Class
