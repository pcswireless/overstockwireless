﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="email_master_detail.aspx.vb" Inherits="Master_email_master_detail" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <asp:HiddenField ID="hid_master_email_id" runat="server" Value="0" />
    <script type="text/javascript">

        function open_pop_win(_path, _id, _winname) {
            w1 = window.open(_path + '?i=' + _id, _winname, 'top=' + ((screen.height - 510) / 2) + ', left=' + ((screen.width - 800) / 2) + ', height=540, width=800,scrollbars=yes,toolbars=no,resizable=1;');
            w1.focus();
            return false;

        }                                  

    </script>
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            //On insert and update buttons click temporarily disables ajax to perform upload actions
            function Cancel_Ajax(sender, args) {
                args.set_enableAjax(false);

            }
            
           function Opendetail() {

                     
                        if (document.getElementById('<%=txt_email_subject.ClientID%>'))
                        
                            {
                                 w1= window.open('/master/email_preview.aspx' + '?i=' + <%=Request("i") %> + '&h=' + document.getElementById('<%=hid_head.ClientID %>').value  + '&f=' + document.getElementById('<%=hid_foot.ClientID %>').value , '_email_masterpre', 'top=' + ((screen.height - 510) / 2) + ', left=' + ((screen.width - 800) / 2) + ', height=540, width=800,scrollbars=yes,toolbars=no,resizable=1;');
                                 w1.focus();
                                return false;
                        }
                        
                        else {
                        w2= window.open('/master/email_preview.aspx' + '?i=' + <%=Request("i") %> + '&t=e', '_email_masterpre', 'top=' + ((screen.height - 510) / 2) + ', left=' + ((screen.width - 800) / 2) + ', height=540, width=800,scrollbars=yes,toolbars=no,resizable=1;');
                          w2.focus();
                        return false;
                         }
                        
                    }
          
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" SkinID="Simple">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_edit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>


        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_save">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_update">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="img_button_cancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel11" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Skin="Simple" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div style="float: left;">
                    <div class="pageheading">
                        <asp:Literal ID="page_heading" runat="server" Text="Edit Email"></asp:Literal></div>
                </div>
                <div style="float: right; margin: 10px; padding-right: 20px;">
                    <a href="javascript:void(0);" onclick="javascript:open_help('4');">
                        <img src="/Images/help-button.gif" border="none" />
                    </a>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="PageTab">
                    <span>Edit Email Details</span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="PageTabContentEdit">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td class="tdTabItem">
                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                CssClass="TabGrid">
                                <asp:HiddenField ID="hid_head" runat="server" Value="0" />
                                <asp:HiddenField ID="hid_foot" runat="server" Value="0" />
                                <div style="padding-left: 5px; padding-bottom: 5px;">
                                    <asp:Label ID="lbl_msg" runat="server" Text="" ForeColor="Red" EnableViewState="false"></asp:Label>
                                </div>
                                <table cellpadding="0" cellspacing="2" border="0" width="838">
                                    <tr>
                                        <asp:Panel ID="Panel1_Header" runat="server" CssClass="pnlTabItemHeader">
                                            <div class="tabItemHeader" style="background: url(/Images/basic_info.gif) no-repeat;
                                                background-position: 10px 0px;">
                                                Basic Information
                                            </div>
                                            <asp:Image ID="Image1" runat="server" ImageAlign="left" ImageUrl="/Images/up_Arrow.gif"
                                                CssClass="panelimage" />
                                            <div class="tabItemHeader" style="/*background: url(/Images/basic_info.gif) no-repeat;
                                                background-position: 10px 0px; */ padding-left: 650px;">
                                                <div runat="server" onmouseout="hide_tip_new();" style="cursor: hand;" id="DIV_IMG_Default_Logo3_Img">
                                                    <img src="/Images/helpicon.png" alt="" /></div>
                                            </div>
                                        </asp:Panel>
                                    </tr>
                                    <tr>
                                        <td class="caption">
                                            Email Caption&nbsp;<%--<span id="span1" runat="server" class="req_star">*</span>--%></td>
                                        <td class="details">
                                            <asp:TextBox ID="txt_email" ValidationGroup="a" Width="500" runat="server" CssClass="inputtype"
                                                MaxLength="500"></asp:TextBox>
                                            <asp:RequiredFieldValidator ValidationGroup="a" ID="RequiredFieldValidator1" runat="server"
                                                ControlToValidate="txt_email" Display="Dynamic" ErrorMessage="<br>Email Required"></asp:RequiredFieldValidator>
                                            <asp:Label ID="lbl_email" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="caption">
                                            Email Code&nbsp;<%--<span id="span6" runat="server" class="req_star">*</span>--%></td>
                                        <td class="details">
                                            <asp:Label ID="lbl_email_code" runat="server" Text=""></asp:Label>                                                                                      
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="width: 145px;" class="caption">
                                            Header&nbsp;<span id="span4" runat="server" class="req_star">*</span>
                                        </td>
                                        <td class="details">
                                            <asp:RadioButtonList ID="rdolist_header" AutoPostBack="false" RepeatColumns="3" RepeatDirection="Horizontal"
                                                runat="server">
                                            </asp:RadioButtonList>
                                            <asp:Label ID="lbl_header" runat="server" Text=""></asp:Label>
                                         <%--   <asp:Label ID="lbl_prvw_header" runat="server" Text=""></asp:Label>--%>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 145px;" class="caption">
                                            Email Subject&nbsp;<span id="span2" runat="server" class="req_star">*</span>
                                        </td>
                                        <td class="details">
                                            <asp:TextBox ID="txt_email_subject" ValidationGroup="a" Width="500" runat="server"
                                                CssClass="inputtype" MaxLength="200"></asp:TextBox> 
                                            <asp:RequiredFieldValidator ID="rfv_company" runat="server" ControlToValidate="txt_email_subject"
                                                                ValidationGroup="a" Display="Dynamic" ErrorMessage="<br />Email Subject Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                            <asp:Label ID="lbl_email_subject" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="caption">
                                            Email Html&nbsp;<span id="span3" runat="server" class="req_star">*</span>
                                        </td>
                                        <td class="details" colspan="1">
                                            <asp:TextBox ID="txt_email_html" TextMode="MultiLine" Width="500" Height="100" ValidationGroup="a"
                                                runat="server" CssClass="inputtype"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_email_html"
                                                                ValidationGroup="a" Display="Dynamic" ErrorMessage="<br />Email HTML Required"
                                                                CssClass="error"></asp:RequiredFieldValidator>
                                            <asp:Label ID="lbl_email_html" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="caption">
                                            Footer&nbsp;<span id="span5" runat="server" class="req_star">*</span>
                                        </td>
                                        <td class="details">
                                            <asp:RadioButtonList ID="rdolist_footer" AutoPostBack="false" RepeatColumns="3" RepeatDirection="Horizontal"
                                                runat="server">
                                            </asp:RadioButtonList>
                                            <asp:Label ID="lbl_footer" runat="server"></asp:Label>
                                          <%--  <asp:Label ID="lbl_prvw_footer" runat="server"></asp:Label>--%>
                                           </td>
                                    </tr>
                                </table>
                            </telerik:RadAjaxPanel>
                        </td>
                        <td class="fixedcolumn" valign="top">
                            <img src="/Images/spacer1.gif" width="120" height="1" alt="" />
                            <asp:Panel ID="Panel1_Content1button" runat="server" CssClass="collapsePanel">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel11" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                                    <asp:Panel ID="Panel1_Content1button_per" runat="server">
                                        <div class="addButton">
                                            <asp:ImageButton ID="img_button_edit" ImageUrl="~/Images/edit.gif" runat="server" />
                                            <asp:ImageButton ID="img_button_save" ValidationGroup="a" ImageUrl="~/Images/save.gif"
                                                runat="server" />
                                            <asp:ImageButton ID="img_button_update" ValidationGroup="a" ImageUrl="~/Images/update.gif"
                                                runat="server" /><br /><br />
                                               <a id="but_preview" href="javascript:void(0);" style="text-decoration: none"  runat ="server" onclick="return Opendetail();">
                                                <img src="/images/preview.png" alt="Preview" /></a><br />

                                        </div>
                                        <div class="cancelButton" id="div_basic_cancel" runat="server">
                                           <%-- <br />
                                            <a id="but_preview" href="javascript:void(0);" style="text-decoration: none"  runat ="server" onclick="return Opendetail();">
                                                <img src="/images/preview.png" alt="Preview" /></a><br />
                                            <br />--%>                                                                                        
                                            <asp:ImageButton ID="img_button_cancel" ImageUrl="~/Images/cancel.gif" runat="server" />
                                        </div>
                                    </asp:Panel>
                                </telerik:RadAjaxPanel>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
     
    </script>
</asp:Content>
