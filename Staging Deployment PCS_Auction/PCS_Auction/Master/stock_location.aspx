﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false" CodeFile="stock_location.aspx.vb" Inherits="Master_stock_location" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Skin="Simple" />
    <input type="hidden" id="hid_stockloc_id" name="hid_stockloc_id"
        value="0" />
    <asp:TextBox ID="txt_test" runat="server" Visible="false"></asp:TextBox>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" alt="" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Stock Location Listing</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxPanel ID="pnl1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                    <telerik:RadGrid ID="RadGrid_Titles" EnableLinqExpressions ="false" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                        AllowAutomaticInserts="True" AllowFilteringByColumn ="true"  PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True"
                        AllowSorting="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                        AllowMultiRowSelection="false" AllowMultiRowEdit="false" Skin="Vista" ShowGroupPanel="False">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <HeaderStyle BackColor="#BEBEBE" />
                        <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="stock_location_id"  AllowFilteringByColumn ="true" DataSourceID="SqlDataSource1"
                            HorizontalAlign="NotSet" AutoGenerateColumns="False" SkinID="Vista" ItemStyle-Height="40" EditMode="EditForms"
                            AlternatingItemStyle-Height="40">
                            <CommandItemStyle BackColor="#E1DDDD" />
                            <NoRecordsTemplate>
                                Stock loaction not available
                            </NoRecordsTemplate>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                            </SortExpressions>
                            <CommandItemSettings AddNewRecordText="Add New Stock Loaction" />
                        
                            <Columns>
                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                    EditImageUrl="/Images/edit_grid.gif">
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                </telerik:GridEditCommandColumn>
                                <telerik:GridTemplateColumn HeaderText="Stock Loaction Id" SortExpression="stock_location_id" UniqueName="stock_location_id"
                                    DataField="stock_location_id" EditFormHeaderTextFormat="Stock Loaction Id">
                                    <ItemTemplate>
                                        <%# Eval("stock_location_id")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn HeaderText="Name" SortExpression="name" UniqueName="name"
                                    DataField="name" EditFormHeaderTextFormat="name">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("name")%>' ></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            
                                <telerik:GridTemplateColumn HeaderText="City" SortExpression="city" UniqueName="city"
                                    DataField="city" EditFormHeaderTextFormat="City">
                                    <ItemTemplate>
                                        <%# Eval("city")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                 <telerik:GridTemplateColumn HeaderText="State" SortExpression="state" UniqueName="state"
                                    DataField="state" EditFormHeaderTextFormat="State">
                                    <ItemTemplate>
                                        <%# Eval("state")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <%--<telerik:GridTemplateColumn HeaderText="Is Active" SortExpression="is_active" UniqueName="is_active"
                                    DataField="is_active" EditFormHeaderTextFormat="Is Active">
                                    <ItemTemplate>
                                        <%# Eval("is_active")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>--%>

                                <telerik:GridTemplateColumn UniqueName="is_active" DataType="System.Boolean" DataField="is_active"
                                    SortExpression="is_active" HeaderText="Is Active">
                                    <FilterTemplate>
                                        <telerik:RadComboBox ID="RadComboBo_Chk_is_active" runat="server" OnClientSelectedIndexChanged="ActiveIndexChanged"
                                            Width="120px" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("is_active").CurrentFilterValue %>'>
                                            <Items>
                                                <telerik:RadComboBoxItem Text="All" Value="" />
                                                <telerik:RadComboBoxItem Text="Active" Value="True" />
                                                <telerik:RadComboBoxItem Text="In-Active" Value="False" />
                                            </Items>
                                        </telerik:RadComboBox>
                                        <telerik:RadScriptBlock ID="RadScriptBlock_is_active" runat="server">
                                            <script type="text/javascript">
                                                function ActiveIndexChanged(sender, args) {
                                                    var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                    if (args.get_item().get_value() == "") {
                                                        tableView.filter("is_active", args.get_item().get_value(), "NoFilter");
                                                    }
                                                    else {
                                                        tableView.filter("is_active", args.get_item().get_value(), "EqualTo");
                                                    }
                                                }
                                            </script>
                                        </telerik:RadScriptBlock>
                                    </FilterTemplate>
                                    <ItemTemplate>
                                        <img src='<%#IIf(Eval("is_active"),"/Images/true.gif","/Images/false.gif") %>' style="border: none;"
                                        alt="" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>




                                <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"
                                    ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                    CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="50" />
                                </telerik:GridButtonColumn>
                            </Columns>

                            <EditFormSettings InsertCaption="New Items" EditFormType="Template">
                                <FormTemplate>
                                    <table id="tbl_grd_item_edit" cellpadding="0" cellspacing="2" border="0" width="800px">
                                        <tr>
                                            <td style="font-weight: bold; padding-top: 10px;" colspan="4">
                                                Stock Location Details
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 135px;">
                                                Name&nbsp;<span class="req_star">*</span>
                                            </td>
                                            <td style="width: 260px;" class="details">
                                                <asp:TextBox ID="txt_grd_name" TabIndex="1" runat="server" Text='<%#Bind("name") %>' CssClass="inputtype" />
                                                <asp:RequiredFieldValidator ID="Req_txt_grd_name" runat="server" Font-Size="10px"
                                                    ValidationGroup="grd_items" ControlToValidate="txt_grd_name" Display="Dynamic"
                                                    ErrorMessage="<br>Name Required"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="CustomValidator1" ControlToValidate="txt_grd_name" Font-Size="10px" ForeColor="red" OnServerValidate="Checkname"
                                                    ErrorMessage="</br>Name Already Exists" runat="server" Display="Dynamic" ValidationGroup="grd_items"/>
                                                    <asp:HiddenField ID="hd_id" runat="server" Value='<%#Bind("stock_location_id") %>' />
                                            </td>
                                            <td align="right" colspan="2" rowspan="3">
                                                <asp:ImageButton ID="but_grd_submit" TabIndex="4" CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem),"PerformInsert","Update") %>'
                                                    ValidationGroup="grd_items" OnClientClick="return ValidationGroupEnable('grd_items', true);"
                                                    AlternateText='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "Save" , "Update") %>'
                                                    runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif" , "/images/update.gif") %>' />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:ImageButton ID="but_grd_cancel" TabIndex="5" CausesValidation="false" CommandName="Cancel"
                                                    runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                            </td>
                                        </tr>
                                        <tr>
                                        <td class="caption" style="width: 135px;">
                                                City&nbsp;<span class="req_star">*</span>
                                            </td>
                                            <td style="width: 260px;" class="details">
                                                <asp:TextBox ID="txt_description" runat="server" TabIndex="2" Text='<%#Bind("city") %>' CssClass="inputtype" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Font-Size="10px"
                                                    ValidationGroup="grd_items" ControlToValidate="txt_description" Display="Dynamic"
                                                    ErrorMessage="<br>City Required"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td class="caption" style="width: 135px;">
                                                State &nbsp;<span class="req_star">*</span>
                                            </td>

                                               <td style="width: 260px;" class="details">
                                                <asp:TextBox ID="txt_state" runat="server" TabIndex="2" Text='<%#Bind("state") %>' CssClass="inputtype" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Font-Size="10px"
                                                    ValidationGroup="grd_items" ControlToValidate="txt_state" Display="Dynamic"
                                                    ErrorMessage="<br>State Required"></asp:RequiredFieldValidator>
                                            </td>
                                          
                                        </tr>

                                        <tr>
                                        <td class="caption" style="width: 135px;">
                                                Is Active &nbsp;<span class="req_star">*</span>
                                            </td>
                                            <td style="width: 260px;" class="details">
                                                
                                            <asp:Label ID="lbl_basic_active" runat="server" />

                                               <asp:CheckBox ID="chk_basic_active" runat="server" Checked='<%#Bind("is_active") %>'  CssClass="inputtype" /> 
                                               

                                            </td>                                    
                                        </tr> 
                                       
                                        
                                    </table>
                                </FormTemplate>
                            </EditFormSettings>

                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                    </telerik:RadGrid>
                </telerik:RadAjaxPanel>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT ISNULL(A.stock_location_id, '') AS stock_location_id, ISNULL(A.name, '') AS name, ISNULL(A.city, '') AS city, ISNULL(A.state, '') AS state, ISNULL(A.is_active, 0) AS is_active FROM tbl_master_stock_locations A order by A.stock_location_id "
                    DeleteCommand="DELETE FROM [tbl_master_stock_locations] WHERE [stock_location_id] = @stock_location_id"
                    InsertCommand="INSERT INTO tbl_master_stock_locations(name,city,state,is_active) 
    VALUES (@name,@city,@state,@is_active)" UpdateCommand="UPDATE tbl_master_stock_locations SET 
       name= @name,city=@city,state=@state,is_active=@is_active WHERE stock_location_id= @stock_location_id">
                    <DeleteParameters>
                        <asp:Parameter Name="stock_location_id" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="stock_location_id" Type="Int32" />
                        <asp:Parameter Name="name" Type="String" />
                        <asp:Parameter Name="city" Type="String" />
                        <asp:Parameter Name="state" Type="String" />
                        <asp:Parameter Name="is_active" Type="Boolean"  />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="name" Type="String" />
                         <asp:Parameter Name="city" Type="String" />
                        <asp:Parameter Name="state" Type="String" />
                        <asp:Parameter Name="is_active" Type="Boolean"  />
                    </InsertParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
