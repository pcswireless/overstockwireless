﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="Iprestriction.aspx.vb" Inherits="Master_Iprestriction" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Skin="Simple" />
    <input type="hidden" id="hid_manufacturer_combo_id" name="hid_manufacturer_combo_id"
        value="0" />
    <asp:TextBox ID="txt_test" runat="server" Visible="false"></asp:TextBox>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" alt="" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Allowed IP Addresses</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxPanel ID="pnl1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                    <telerik:RadGrid ID="RadGrid_IP" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                        AllowAutomaticInserts="True" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True"
                        AllowSorting="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                        AllowMultiRowSelection="false" AllowMultiRowEdit="false" Skin="Vista" ShowGroupPanel="False">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <HeaderStyle BackColor="#BEBEBE" />
                        <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="ip_id" DataSourceID="SqlDataSource1"
                            HorizontalAlign="NotSet" AutoGenerateColumns="False" SkinID="Vista" ItemStyle-Height="40"
                            EditMode="EditForms" AlternatingItemStyle-Height="40">
                            <CommandItemStyle BackColor="#E1DDDD" />
                            <NoRecordsTemplate>
                                IP Address not available
                            </NoRecordsTemplate>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="ip_address" SortOrder="Ascending" />
                            </SortExpressions>
                            <CommandItemSettings AddNewRecordText="Add New IP Address" />
                            <Columns>
                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                    EditImageUrl="/Images/edit_grid.gif">
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                </telerik:GridEditCommandColumn>
                                <telerik:GridTemplateColumn HeaderText="IP Address" SortExpression="ip_address" UniqueName="ip_address"
                                    DataField="ip_address" EditFormHeaderTextFormat="IP Address">
                                    <ItemTemplate>
                                        <%# Eval("ip_address")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"
                                    ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                    CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="50" />
                                </telerik:GridButtonColumn>
                            </Columns>
                            <EditFormSettings InsertCaption="New IP Address" EditFormType="Template">
                                <FormTemplate>
                                    <table id="tbl_grd_item_edit" cellpadding="0" cellspacing="2" border="0" width="800px">
                                        <tr>
                                            <td style="font-weight: bold; padding-top: 10px;" colspan="4">
                                                IP Address Details
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 135px;">
                                                IP Address&nbsp;<span class="req_star">*</span>
                                            </td>
                                            <td style="width: 260px;" class="details">
                                                <asp:TextBox ID="txt_grd_name" runat="server" Text='<%#Bind("ip_address") %>' CssClass="inputtype" />
                                                <asp:RequiredFieldValidator ID="Req_txt_grd_name" runat="server" Font-Size="10px"
                                                    ValidationGroup="grd_items" ControlToValidate="txt_grd_name" Display="Dynamic"
                                                    ErrorMessage="<br>IP Address Required"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="vldRejex" runat="server" ControlToValidate="txt_grd_name"
                                                    ValidationGroup="grd_items" ErrorMessage="Invalid IP Address" Display="Dynamic"
                                                    ValidationExpression="^(([01]?\d\d?|2[0-4]\d|25[0-5])\.){3}([01]?\d\d?|25[0-5]|2[0-4]\d)$">
                                                </asp:RegularExpressionValidator>
                                                <asp:CustomValidator ID="CustomValidator1" ControlToValidate="txt_grd_name" Font-Size="10px"
                                                    ForeColor="red" OnServerValidate="Checkname" ErrorMessage="</br>IP Already Exists"
                                                    runat="server" Display="Dynamic" ValidationGroup="grd_items" />
                                                <asp:HiddenField ID="ip_address" runat="server" Value='<%#Bind("ip_id") %>' />
                                            </td>
                                            <td align="right" colspan="2">
                                                <asp:ImageButton ID="but_grd_submit" CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem),"PerformInsert","Update") %>'
                                                    ValidationGroup="grd_items" OnClientClick="return ValidationGroupEnable('grd_items', true);"
                                                    AlternateText='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "Save" , "Update") %>'
                                                    runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif" , "/images/update.gif") %>' />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:ImageButton ID="but_grd_cancel" CausesValidation="false" CommandName="Cancel"
                                                    runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </FormTemplate>
                            </EditFormSettings>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                    </telerik:RadGrid>
                </telerik:RadAjaxPanel>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="select ip_id,ip_address from [tbl_ip_addresses]"
                    DeleteCommand="DELETE FROM [tbl_ip_addresses] WHERE [ip_id] = @ip_id" InsertCommand="INSERT INTO tbl_ip_addresses(ip_address) VALUES (@ip_address)"
                    UpdateCommand="UPDATE tbl_ip_addresses SET ip_address= @ip_address WHERE ip_id= @ip_id">
                    <DeleteParameters>
                        <asp:Parameter Name="ip_id" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="ip_id" Type="Int32" />
                        <asp:Parameter Name="ip_address" Type="String" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="ip_address" Type="String" />
                    </InsertParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
