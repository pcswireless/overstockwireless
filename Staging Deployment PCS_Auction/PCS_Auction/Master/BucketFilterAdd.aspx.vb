﻿Imports System.IO
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Partial Class Master_BucketFilterAdd
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Request.QueryString.Get("e") = "1" Then
                lbl_error.Text = "New Bucket created successfully"
            End If
            If IsNumeric(Request.QueryString.Get("i")) Then
                hd_bucket_filter_id.Value = Request.QueryString.Get("i")
                BasicTabEdit(True)
                Panel2_Header.Visible = True
            Else

                hd_bucket_filter_id.Value = 0
                BasicTabEdit(False)
            End If
        End If

    End Sub

  
    Private Sub BasicTabEdit(ByVal readMode As Boolean)

        If hd_bucket_filter_id.Value > 0 Then
            fillBasicTab(hd_bucket_filter_id.Value)
        Else
            EmptyBasicData()

        End If
        visibleBasicData(readMode)
    End Sub

    Private Sub fillBasicTab(ByVal bucket_filter_id As Integer)
        Dim qry As String = "SELECT A.bucket_filter_id as bucket_filter_id,ISNULL(A.caption, '') AS caption, ISNULL(A.is_active, 0) AS is_active FROM tbl_bucket_filter A where A.bucket_filter_id=" & bucket_filter_id & ""
        Dim dt As DataTable = New DataTable()
        dt = SqlHelper.ExecuteDatatable(qry)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                page_heading.Text = "Edit Left bar Bucket: " & CommonCode.decodeSingleQuote(.Item("caption"))
                
                txt_basic_bucket_name.Text = CommonCode.decodeSingleQuote(.Item("caption"))
               
                lbl_basic_bucket_name.Text = CommonCode.decodeSingleQuote(.Item("caption"))
               
                chk_basic_active.Checked = .Item("is_active")

               
                If .Item("is_active") Then
                    lbl_basic_active.Text = "<img src='/Images/true.gif' alt=''>"
                Else
                    lbl_basic_active.Text = "<img src='/Images/false.gif' alt=''>"
                End If

               

            End With

        Else
            EmptyBasicData()

        End If
        dt = Nothing

    End Sub

    Private Sub EmptyBasicData()
        txt_basic_bucket_name.Text = ""
        lbl_error.Text = ""
        lbl_basic_bucket_name.Text = ""
        chk_basic_active.Checked = True
        lbl_basic_active.Text = ""

    End Sub

    Private Sub visibleBasicData(ByVal readMode As Boolean)

        If readMode Then
            but_basic_edit.Visible = True
            'but_preview.Visible = True
            but_basic_save.Visible = False
            but_basic_update.Visible = False
            div_basic_cancel.Visible = False
        Else
            but_basic_edit.Visible = False
            'but_preview.Visible = False
            If hd_bucket_filter_id.Value = 0 Then
                but_basic_save.Visible = True
                but_basic_update.Visible = False
                div_basic_cancel.Visible = False
            Else
                but_basic_save.Visible = False
                but_basic_update.Visible = True
                ' but_basic_cancel.Visible = True
                div_basic_cancel.Visible = True
            End If
        End If


        'txt_basic_confirm_email.Visible = Not readMode

        txt_basic_bucket_name.Visible = Not readMode
        span_basic_bucket_name.Visible = Not readMode
        chk_basic_active.Visible = Not readMode
        lbl_basic_bucket_name.Visible = readMode
        lbl_basic_active.Visible = readMode
       
    End Sub

    Protected Sub btn_basic_edit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_edit.Click
        visibleBasicData(False)
    End Sub

    

    Protected Sub btn_basic_cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_cancel.Click
        visibleBasicData(True)
    End Sub

    Public Function check_bucket() As Boolean
        Dim str As String = ""
        Dim is_exists As Boolean = False
        Dim i As Integer = 0
        If hd_bucket_filter_id.Value > 0 Then
            str = "if exists(select bucket_filter_id from tbl_bucket_filter where caption='" & CommonCode.encodeSingleQuote(txt_basic_bucket_name.Text.Trim()) & "' and bucket_filter_id<>" & hd_bucket_filter_id.Value & ") select 1 else select 0"
        Else
            str = "if exists(select bucket_filter_id from tbl_bucket_filter where caption='" & CommonCode.encodeSingleQuote(txt_basic_bucket_name.Text.Trim()) & "')select 1 else select 0 "
        End If
        i = SqlHelper.ExecuteScalar(str)
        If i = 1 Then
            is_exists = True
        Else
            is_exists = False
        End If
        Return is_exists
    End Function

    Protected Sub btn_basic_save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_save.Click
        If Page.IsValid Then

            If check_bucket() Then
                lbl_error.Text = "Caption already exists."
                Exit Sub
            End If

            Dim obj As New CommonCode

            Dim insParameter As String = "caption"
            Dim insValue As String = "'" & CommonCode.encodeSingleQuote(txt_basic_bucket_name.Text.Trim()) & "'"

            
            insParameter = insParameter & ",is_active"
            insValue = insValue & ",'" & IIf(chk_basic_active.Checked, 1, 0) & "'"

            
            Dim qry As String = "INSERT INTO tbl_bucket_filter (" & insParameter & ") values(" & insValue & ")  select scope_identity()"
            '  Response.Write(SqlHelper.ExecuteScalar(qry))

            Dim comp_id As Int32 = SqlHelper.ExecuteScalar(qry)

            If comp_id > 0 Then
                Response.Redirect("/master/BucketFilterAdd.aspx?e=1&i=" & comp_id)
            End If
        End If


    End Sub

    Protected Sub btn_basic_update_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles but_basic_update.Click

        ' Dim radalertscript As String = "<script language='javascript'>function f(){callConfirm(); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>"
        ' Page.ClientScript.RegisterStartupScript(Me.[GetType](), "radalert", radalertscript)


        lbl_error.Text = ""
        If hd_bucket_filter_id.Value > 0 And Page.IsValid Then
            If check_bucket() Then
                lbl_error.Text = "Bucket already exists."
                Exit Sub
            End If
            Dim is_update_required As Boolean = False
            Dim updpara As String = ""


            If lbl_basic_bucket_name.Text.Trim() <> txt_basic_bucket_name.Text Then
                If is_update_required Then
                    updpara = updpara & ",caption='" & CommonCode.encodeSingleQuote(txt_basic_bucket_name.Text.Trim()) & "'"
                Else
                    updpara = updpara & "caption='" & CommonCode.encodeSingleQuote(txt_basic_bucket_name.Text.Trim()) & "'"
                End If

                is_update_required = True
            End If



            If is_update_required Then
                updpara = updpara & ",is_active='" & IIf(chk_basic_active.Checked, 1, 0) & "'"
            Else
                updpara = updpara & "is_active='" & IIf(chk_basic_active.Checked, 1, 0) & "'"
                is_update_required = True
            End If




            If is_update_required Then
                SqlHelper.ExecuteNonQuery("update tbl_bucket_filter set " & updpara & " where bucket_filter_id=" & hd_bucket_filter_id.Value)
                
                lbl_error.Text = "Bucket Updated Successfully."

            End If

            BasicTabEdit(True)

        End If

    End Sub

    Protected Sub RadGrid_Bucket_Items_ItemCommand(ByVal sender As Object, ByVal e As GridCommandEventArgs) Handles RadGrid_Bucket_Items.ItemCommand
        If (e.CommandName = Telerik.Web.UI.RadGrid.InitInsertCommandName) Then
            'cancel the default operation
            e.Canceled = True
            'Prepare an IDictionary with the predefined values
            Dim newValues As System.Collections.Specialized.ListDictionary = New System.Collections.Specialized.ListDictionary()
            'set default checked state for checkbox inside the EditItemTemplate
            newValues("is_active") = False
            e.Item.OwnerTableView.InsertItem(newValues)
        End If
    End Sub

    Protected Sub RadGrid_Bucket_Items_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles RadGrid_Bucket_Items.PreRender
        If (Not Page.IsPostBack) Then
            RadGrid_Bucket_Items.MasterTableView.FilterExpression = "([is_active] = True) "
            Dim column As GridColumn = RadGrid_Bucket_Items.MasterTableView.GetColumnSafe("is_active")
            column.CurrentFilterFunction = GridKnownFunction.EqualTo
            column.CurrentFilterValue = "True"
            RadGrid_Bucket_Items.MasterTableView.Rebind()
        End If
    End Sub
End Class
