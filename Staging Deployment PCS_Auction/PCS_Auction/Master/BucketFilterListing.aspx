﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false" CodeFile="BucketFilterListing.aspx.vb" Inherits="Master_BucketFilterListing" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" Runat="Server">
<table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Left bar Bucket</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; width: 60%; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    Please select the Bucket name to edit from below
                </div>
                <div style="float: right; width: 40%; text-align: right; margin-bottom: 3px;" id="div_new_auction"
                    runat="server">
                    <a style="text-decoration: none" href="/Master/BucketFilterAdd.aspx">
                        <img src="/images/addnewbucket.png" style="border: none" alt="Add New Bucket" /></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                    <ClientEvents OnRequestStart="Cancel_Ajax" />
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="RadGrid1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
                    left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
                    <img id="Image8" src="/images/img_loading.gif" />
                </telerik:RadAjaxLoadingPanel>
                    <telerik:RadGrid ID="RadGrid1" AllowFilteringByColumn="False" AutoGenerateColumns="false"
                    AllowSorting="True" ShowFooter="False" ShowGroupPanel="false" AllowPaging="False"
                    runat="server" Skin="Vista" EnableLinqExpressions="false">
                    <MasterTableView DataKeyNames="bucket_filter_id" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                        AllowFilteringByColumn="False" ItemStyle-Height="40" AlternatingItemStyle-Height="40">
                            <NoRecordsTemplate>
                                No bucket to display
                            </NoRecordsTemplate>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="caption" SortOrder="Ascending" />
                            </SortExpressions>
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="Caption" SortExpression="caption" UniqueName="caption" DataField="caption" FilterControlWidth="150" >
                                    <ItemTemplate>
                                               <%# Eval("Caption")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>  
                                <telerik:GridTemplateColumn UniqueName="is_active" DataType="System.Boolean" DataField="is_active"
                                    SortExpression="is_active" HeaderText="Is Active">
                                    <FilterTemplate>
                                        <telerik:RadComboBox ID="RadComboBo_Chk_is_active" runat="server" OnClientSelectedIndexChanged="ActiveIndexChanged"
                                            Width="120px" SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("is_active").CurrentFilterValue %>'>
                                            <Items>
                                                <telerik:RadComboBoxItem Text="All" Value="" />
                                                <telerik:RadComboBoxItem Text="Active" Value="True" />
                                                <telerik:RadComboBoxItem Text="In-Active" Value="False" />
                                            </Items>
                                        </telerik:RadComboBox>
                                        <telerik:RadScriptBlock ID="RadScriptBlock_is_active" runat="server">
                                            <script type="text/javascript">
                                                function ActiveIndexChanged(sender, args) {
                                                    var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                    if (args.get_item().get_value() == "") {
                                                        tableView.filter("is_active", args.get_item().get_value(), "NoFilter");
                                                    }
                                                    else {
                                                        tableView.filter("is_active", args.get_item().get_value(), "EqualTo");
                                                    }
                                                }
                                            </script>
                                        </telerik:RadScriptBlock>
                                    </FilterTemplate>
                                    <ItemTemplate>
                                        <img src='<%#IIf(Eval("is_active"),"/Images/true.gif","/Images/false.gif") %>' style="border: none;"
                                        alt="" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Select" Groupable="false"
                                    ItemStyle-Width="50">
                                    <ItemTemplate>
                                        <a href="javascript:void(0);" onclick="redirectIframe('','','/master/BucketFilterAdd.aspx?i=<%# Eval("bucket_filter_id")%>')">
                                                  <img src="/images/edit.png" style="border: none;" alt="Edit" /></a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                       
                    </telerik:RadGrid>              
            </td>
        </tr>
    </table>
</asp:Content>

