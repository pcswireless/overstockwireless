﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="email_template_master.aspx.vb" Inherits="Master_email_template_master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Skin="Simple" />
    <%--<input type="hidden" id="hid_stockloc_id" name="hid_stockloc_id" value="0" />--%>
    <asp:TextBox ID="txt_test" runat="server" Visible="false"></asp:TextBox>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" IsSticky="true" Style="position: fixed;
        left: 420px; top: 180px; z-index: 9999" runat="server" BackgroundPosition="Center">
        <img id="Image8" src="/images/img_loading.gif" alt="" />
    </telerik:RadAjaxLoadingPanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="pageheading">
                    Email Profiles</div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left; text-align: left; margin-bottom: 3px; padding-top: 10px;
                    font-weight: bold;">
                    &nbsp;
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxPanel ID="pnl1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                    <telerik:RadGrid ID="RadGrid_Titles" GridLines="None" runat="server" AllowAutomaticDeletes="True"
                        AllowAutomaticInserts="True" PageSize="10" AllowAutomaticUpdates="True" AllowPaging="True"
                        AllowSorting="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                        AllowMultiRowSelection="false" AllowMultiRowEdit="false" Skin="Vista" ShowGroupPanel="False">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <HeaderStyle BackColor="#BEBEBE" />
                        <MasterTableView Width="100%" CommandItemDisplay ="Top"   DataKeyNames="email_template_id"
                            DataSourceID="SqlDataSource1" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                            SkinID="Vista" ItemStyle-Height="40" EditMode="EditForms" AlternatingItemStyle-Height="40">
                            <CommandItemStyle BackColor="#E1DDDD" />
                            <NoRecordsTemplate>
                                No email template available
                            </NoRecordsTemplate>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="email_template_name" SortOrder="Ascending" />
                            </SortExpressions>
                            <CommandItemSettings ShowAddNewRecordButton ="false" />
                            <Columns>
                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                    EditImageUrl="/Images/edit_grid.gif">
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                </telerik:GridEditCommandColumn>
                                <telerik:GridTemplateColumn HeaderText="Email Template Id" Visible ="false" SortExpression="email_template_id"
                                    UniqueName="email_template_id" DataField="email_template_id" EditFormHeaderTextFormat="Email Template Id">
                                    <ItemTemplate>
                                        <%# Eval("email_template_id")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Email Profile Name" SortExpression="email_template_name"
                                    UniqueName="email_template_name" DataField="email_template_name" EditFormHeaderTextFormat="Email Template Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("email_template_name")%>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Email Records" ItemStyle-Width="450" SortExpression="email_records"
                                    UniqueName="email_records" DataField="email_records" EditFormHeaderTextFormat="Email Records">
                                    <ItemTemplate>
                                        <%--  <%# Eval("email_records").ToString().Replace(";",";<br />")%>--%>
                                        <%# Eval("email_records")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Is Active" Visible ="false" SortExpression="is_active" UniqueName="is_active"
                                    DataField="is_active" EditFormHeaderTextFormat="Is Active">
                                    <ItemTemplate>
                                        <%# Eval("is_active")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"
                                    ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                    CommandName="Delete" Text="Delete" Visible ="false" UniqueName="DeleteColumn">
                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="50" />
                                </telerik:GridButtonColumn>
                            </Columns>
                            <EditFormSettings InsertCaption="New Items" EditFormType="Template">
                                <FormTemplate>
                                    <table id="tbl_grd_item_edit" cellpadding="0" cellspacing="2" border="0" width="800px">
                                        <tr>
                                            <td style="font-weight: bold; padding-top: 10px;" colspan="4">
                                                Email Profile Details
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 150;">
                                                Email Profile Name&nbsp;<%--<span class="req_star">*</span>--%>
                                            </td>
                                            <td style="width: 260px;" class="details">
                                              <%--  <asp:TextBox ID="txt_grd_name" TabIndex="1" runat="server" Text='<%#Bind("email_template_name") %>'
                                                    CssClass="inputtype" />--%>

                                                     <asp:Label ID="lbl_name1" runat="server" Text='<%# Eval("email_template_name")%>'></asp:Label>
                                               <%-- <asp:RequiredFieldValidator ID="Req_txt_grd_name" runat="server" Font-Size="10px"
                                                    ValidationGroup="grd_items" ControlToValidate="txt_grd_name" Display="Dynamic"
                                                    ErrorMessage="<br>Name Required"></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="CustomValidator1" ControlToValidate="txt_grd_name" Font-Size="10px"
                                                    ForeColor="red" OnServerValidate="Checkname" ErrorMessage="</br>Email Name Already Exists"
                                                    runat="server" Display="Dynamic" ValidationGroup="grd_items" />--%>
                                                <asp:HiddenField ID="hd_id" runat="server" Value='<%#Bind("email_template_id") %>' />
                                            </td>
                                            <%--<td class="caption" style="width: 150; visibility:hidden ;">
                                                Is Active
                                            </td>
                                            <td style="width: 260px;" class="details">
                                                <asp:Label ID="lbl_basic_active" Visible ="false" runat="server" />
                                                <asp:CheckBox ID="chk_basic_active"  Visible ="false" runat="server" TabIndex="2" Checked='<%#Bind("is_active") %>'
                                                    CssClass="inputtype" />
                                            </td>--%>
                                        </tr>
                                        <tr>
                                            <td class="caption" style="width: 150px;">
                                                Email Records&nbsp;<span class="req_star">*</span><br />
                                                <span style="color: Gray; font-size: 10px;">Please enter multiple emails<br />
                                                    seperated by comma.</span>
                                            </td>
                                            <td colspan="3" style="width: 260px;" class="details">
                                                <asp:TextBox ID="txt_description" TextMode="MultiLine" Height="150px" Width="550px"
                                                    runat="server" TabIndex="3" Text='<%#Bind("email_records")%>' CssClass="inputtype" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Font-Size="10px"
                                                    ValidationGroup="grd_items" ControlToValidate="txt_description" Display="Dynamic"
                                                    ErrorMessage="<br>Email Required"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ValidationGroup="grd_items" ID="rev_email" runat="server"
                                                    ControlToValidate="txt_description" Display="Dynamic" ErrorMessage="<br>Invalid Email"
                                                    ValidationExpression="^(\s*,?\s*[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,}))+\s*$"
                                                    ForeColor="Red"></asp:RegularExpressionValidator>
                                            </td><%--^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$--%>
                                        </tr>
                                        <tr>
                                            <td align="right" colspan="2" rowspan="3" >
                                                <asp:ImageButton ID="but_grd_submit" TabIndex="4" CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem),"PerformInsert","Update") %>'
                                                    ValidationGroup="grd_items" OnClientClick="return ValidationGroupEnable('grd_items', true);"
                                                    AlternateText='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "Save" , "Update") %>'
                                                    runat="server" ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif" , "/images/update.gif") %>' />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:ImageButton ID="but_grd_cancel" TabIndex="5" CausesValidation="false" CommandName="Cancel"
                                                    runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </FormTemplate>
                            </EditFormSettings>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>
                    </telerik:RadGrid>
                </telerik:RadAjaxPanel>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT	(A.email_template_id) AS email_template_id, ISNULL(A.email_template_name, '') AS email_template_name, ISNULL(A.email_records, '') AS email_records, ISNULL(A.is_active, 0) AS is_active FROM tbl_master_email_template A where isnull(A.is_active,0)=1 ORDER BY email_template_name"
                    DeleteCommand="DELETE FROM [tbl_master_email_template] WHERE [email_template_id] = @email_template_id"
                    InsertCommand="INSERT INTO tbl_master_email_template(email_template_name,email_records,is_active) 
    VALUES (@email_template_name,@email_records,@is_active)" UpdateCommand="UPDATE tbl_master_email_template SET 
       email_records=@email_records WHERE email_template_id= @email_template_id">
                    <DeleteParameters>
                        <asp:Parameter Name="email_template_id" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="email_template_id" Type="Int32" />
                        <asp:Parameter Name="email_records" Type="String" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="email_template_name" Type="String" />
                        <asp:Parameter Name="email_records" Type="String" />
                        <asp:Parameter Name="is_active" Type="Boolean" />
                    </InsertParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
