﻿Imports Telerik.Web.UI
Partial Class Master_stock_location
    Inherits System.Web.UI.Page
    Sub Checkname(ByVal source As Object, ByVal e As ServerValidateEventArgs)
        Dim strQuery As String = ""
        Dim ds As New DataSet
        Dim txt_name As New TextBox
        Dim but_grd_submit As New ImageButton
        Dim hd_id As New HiddenField
        hd_id = CType(((source).NamingContainer).FindControl("hd_id"), HiddenField)
        but_grd_submit = CType(((source).NamingContainer).FindControl("but_grd_submit"), ImageButton)
        If but_grd_submit.AlternateText = "Update" Then
            txt_name = CType(((source).NamingContainer).FindControl("txt_grd_name"), TextBox)
            strQuery = "select stock_location_id from tbl_master_stock_locations where name='" & txt_name.Text.Trim() & "'"
            ds = SqlHelper.ExecuteDataset(strQuery)
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)(0) = hd_id.Value Then
                    e.IsValid = True
                Else
                    e.IsValid = False
                End If
            Else
                e.IsValid = True
            End If
        Else
            txt_name = CType(((source).NamingContainer).FindControl("txt_grd_name"), TextBox)
            strQuery = "select name from tbl_master_stock_locations where name='" & txt_name.Text.Trim() & "'"
            ds = SqlHelper.ExecuteDataset(strQuery)
            If ds.Tables(0).Rows.Count > 0 Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If

        End If
    End Sub

    Protected Sub RadGrid_Titles_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrid_Titles.ItemCommand
        If (e.CommandName = Telerik.Web.UI.RadGrid.InitInsertCommandName) Then
            'cancel the default operation
            e.Canceled = True
            'Prepare an IDictionary with the predefined values
            Dim newValues As System.Collections.Specialized.ListDictionary = New System.Collections.Specialized.ListDictionary()
            'set default checked state for checkbox inside the EditItemTemplate
            newValues("is_active") = False
            'newValues("Is_Disabled") = False
            'Insert the item and rebind
            e.Item.OwnerTableView.InsertItem(newValues)
        End If
    End Sub


    Protected Sub RadGrid_Titles_PreRender(sender As Object, e As System.EventArgs) Handles RadGrid_Titles.PreRender
        'Dim menu As GridFilterMenu = RadGrid_Titles.FilterMenu

        'Dim i As Integer = 0
        'While i < menu.Items.Count

        '    If menu.Items(i).Text.ToLower = "nofilter" Or menu.Items(i).Text.ToLower = "contains" Or menu.Items(i).Text.ToLower = "doesnotcontain" Or menu.Items(i).Text.ToLower = "startswith" Or menu.Items(i).Text.ToLower = "endswith" Then
        '        i = i + 1
        '    Else
        '        menu.Items.RemoveAt(i)
        '    End If
        'End While

        If (Not Page.IsPostBack) Then
            RadGrid_Titles.MasterTableView.FilterExpression = "([is_active] = True) "
            Dim column As GridColumn = RadGrid_Titles.MasterTableView.GetColumnSafe("is_active")
            column.CurrentFilterFunction = GridKnownFunction.EqualTo
            column.CurrentFilterValue = "True"
            RadGrid_Titles.MasterTableView.Rebind()
        End If

  

    End Sub
End Class
