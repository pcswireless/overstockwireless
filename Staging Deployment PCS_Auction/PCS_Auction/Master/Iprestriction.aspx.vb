﻿Imports Telerik.Web.UI
Partial Class Master_Iprestriction
    Inherits System.Web.UI.Page
    Protected Sub RadGrid_IP_ItemUpdated(ByVal source As Object, ByVal e As Telerik.Web.UI.GridUpdatedEventArgs) Handles RadGrid_IP.ItemUpdated
        Dim item As GridEditableItem = DirectCast(e.Item, GridEditableItem)
        Dim id As String = item.GetDataKeyValue("ip_id").ToString()
        Dim Str As String
        If Not e.Exception Is Nothing Then
            e.KeepInEditMode = True
            e.ExceptionHandled = True
            Str = "Item cannot be updated. Reason: " + e.Exception.Message
        Else
            Str = "Item with ID " + id + " is updated!"
        End If
        RadGrid_IP.Controls.Add(New LiteralControl(String.Format("<span style='color:red'>" & Str & "</span>")))
    End Sub

    Protected Sub RadGrid_IP_ItemInserted(ByVal source As Object, ByVal e As Telerik.Web.UI.GridInsertedEventArgs) Handles RadGrid_IP.ItemInserted
        Dim Str As String
        If Not e.Exception Is Nothing Then
            e.ExceptionHandled = True
            e.KeepInInsertMode = True
            Str = "Item cannot be inserted. Reason: " + e.Exception.Message
        Else
            Str = "New item inserted"
        End If
        RadGrid_IP.Controls.Add(New LiteralControl(String.Format("<span style='color:red'>" & Str & "</span>")))
    End Sub

    Sub Checkname(ByVal source As Object, ByVal e As ServerValidateEventArgs)
        Dim strQuery As String = ""
        Dim ds As New DataSet
        Dim txt_name As New TextBox
        Dim but_grd_submit As New ImageButton
        Dim ip_address As New HiddenField
        ip_address = CType(((source).NamingContainer).FindControl("ip_address"), HiddenField)
        but_grd_submit = CType(((source).NamingContainer).FindControl("but_grd_submit"), ImageButton)
        If but_grd_submit.AlternateText = "Update" Then
            txt_name = CType(((source).NamingContainer).FindControl("txt_grd_name"), TextBox)
            strQuery = "select ip_id from tbl_ip_addresses where ip_address='" & txt_name.Text.Trim() & "'"
            ds = SqlHelper.ExecuteDataset(strQuery)
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)(0) = ip_address.Value Then
                    e.IsValid = True
                Else
                    e.IsValid = False
                End If
            Else
                e.IsValid = True
            End If
        Else
            txt_name = CType(((source).NamingContainer).FindControl("txt_grd_name"), TextBox)
            strQuery = "select ip_address from tbl_ip_addresses where ip_address='" & txt_name.Text.Trim() & "'"
            ds = SqlHelper.ExecuteDataset(strQuery)
            If ds.Tables(0).Rows.Count > 0 Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If

        End If

    End Sub
End Class
