﻿
Partial Class EmailContent
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim objEmail As New Email
            Dim buyer_id As Integer = SqlHelper.ExecuteScalar("select top 1 buyer_id from tbl_reg_buyers WITH (NOLOCK)")
            Dim buyer_user_id_activate As Integer = SqlHelper.ExecuteScalar("select top 1 buyer_user_id from tbl_reg_buyer_users where is_active=1")
            Dim buyer_user_id_inactivate As Integer = SqlHelper.ExecuteScalar("select top 1 buyer_user_id from tbl_reg_buyer_users where is_active=0")

            Dim active_user_id As Integer = SqlHelper.ExecuteScalar("select top 1 user_id from tbl_sec_users where is_active=1 order by user_id desc")
            Dim inactive_user_id As Integer = SqlHelper.ExecuteScalar("select top 1 user_id from tbl_sec_users where is_active=0 order by user_id desc")

            lit_Registration_Mail.Text = objEmail.Send_Registration_Mail("test@test.com", "[First Name]", True)
            lit_Bidderd_Admin_Active.Text = objEmail.Bidderd_Admin_Active(buyer_id, True)
            lit_Registration_Approve_Mail.Text = objEmail.Send_Registration_Approve_Mail(buyer_user_id_activate, "test@test.com", "[First Name]", True)
            lit_Send_ChangePassword_Mail.Text = objEmail.Send_ChangePassword_Mail("test@test.com", "[First Name]", True)
            lit_Send_ChangePassword_Mail.Text = objEmail.Send_ChangePassword_Mail("test@test.com", "[First Name]", True)
            lit_Send_Forgot_Pwd.Text = objEmail.Send_Forgot_Pwd("XXXXXX", "test@test.com", "XXXXXX", "[First Name]", True)
            lit_send_buyer_approval_email.Text = objEmail.send_buyer_approval_email(buyer_id, True)
            lit_send_buyer_status_changed_email_Pending.Text = objEmail.send_buyer_status_changed_email(buyer_id, "Pending", True)
            lit_send_buyer_status_changed_email_details.Text = objEmail.send_buyer_status_changed_email(buyer_id, "More Details", True)
            lit_send_buyer_status_changed_email_hold.Text = objEmail.send_buyer_status_changed_email(buyer_id, "On Hold", True)
            lit_send_buyer_status_changed_email_Reject.Text = objEmail.send_buyer_status_changed_email(buyer_id, "Reject", True)
            lit_send_buyer_status_changed_email_Inactive.Text = objEmail.send_buyer_status_changed_email(buyer_id, "Inactive", True)
            If buyer_user_id_activate > 0 Then
                lit_send_bidder_sub_login_creation_email_active.Text = objEmail.send_bidder_sub_login_creation_email(buyer_user_id_activate, True)
                lit_send_bidder_sub_login_active_status_changed_email_active.Text = objEmail.send_bidder_sub_login_active_status_changed_email(buyer_user_id_activate, True)
            End If
            If buyer_user_id_inactivate > 0 Then
                lit_send_bidder_sub_login_creation_email_inactive.Text = objEmail.send_bidder_sub_login_creation_email(buyer_user_id_inactivate, True)
                lit_send_bidder_sub_login_active_status_changed_email_inactive.Text = objEmail.send_bidder_sub_login_active_status_changed_email(buyer_user_id_inactivate, True)
            End If

            If active_user_id > 0 Then
                lit_send_employee_creation_email_active.Text = objEmail.send_employee_creation_email(active_user_id, True)
                lit_send_employee_active_status_changed_email_active.Text = objEmail.send_employee_active_status_changed_email(active_user_id, True)
            End If
            If inactive_user_id > 0 Then
                lit_send_employee_creation_email_inactive.Text = objEmail.send_employee_creation_email(inactive_user_id, True)
                lit_send_employee_active_status_changed_email_inactive.Text = objEmail.send_employee_active_status_changed_email(inactive_user_id, True)
            End If

            lit_set_rep_contact_request_to_salesrep.Text = objEmail.set_rep_contact_request_to_salesrep(SqlHelper.of_FetchKey("default_sales_rep_id"), "[Test subject]", "[Test Question]", True)
            lit_set_rep_contact_request_to_user.Text = objEmail.set_rep_contact_request_to_user(buyer_user_id_activate, "[Test subject]", "[Test Question]", True)

            Dim query_id As Integer = SqlHelper.ExecuteScalar("select top 1 query_id from tbl_auction_queries where sales_rep_id=39 and (datalength(isnull(title,''))<>0 or datalength(isnull(message,''))<>0) order by query_id desc")
            If query_id > 0 Then
                lit_Send_Bidder_Reply.Text = objEmail.Send_Bidder_Reply(39, query_id, "[Test Contact]", True)
            End If

            query_id = SqlHelper.ExecuteScalar("select top 1 query_id from tbl_auction_queries where sales_rep_id=39 and (datalength(isnull(title,''))=0 or datalength(isnull(message,''))=0) order by query_id desc")
            If query_id > 0 Then
                lit_salerep_reply_question.Text = objEmail.Send_Bidder_Reply(39, query_id, "[Test Answer]", True)
            End If

            lit_sendOutOfBidMail.Text = objEmail.sendOutOfBidMail("test@test.com", "[First Name]", "[Auction Name]", "[Auction Code]", "$XXX.XX", 0, 5, True, True)


            Dim auction_id As Integer = SqlHelper.ExecuteScalar("select  top 1 auction_id from tbl_auctions WITH (NOLOCK) where auction_type_id=4")
            lit_Send_Auction_Order_Mail_order_4.Text = objEmail.Send_Auction_Order_Mail(auction_id, 0, buyer_user_id_activate, True)

            auction_id = SqlHelper.ExecuteScalar("select  top 1 auction_id from tbl_auctions WITH (NOLOCK) where auction_type_id=3")
            lit_Send_Auction_Order_Mail_order_23.Text = objEmail.Send_Auction_Order_Mail(auction_id, 0, buyer_user_id_activate, True)

            Dim buy_id As Integer = SqlHelper.ExecuteScalar("select top 1 buy_id from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now'")
            lit_Send_Auction_Order_Mail_order_5.Text = objEmail.Send_Mail_Confirm_Receipt(buy_id, "b", "[Body Content]", True)
            buy_id = SqlHelper.ExecuteScalar("select top 1 buy_id from tbl_auction_buy WITH (NOLOCK) where buy_type='private'")
            lit_Send_Auction_Order_Mail_order_6.Text = objEmail.Send_Mail_Confirm_Receipt(buy_id, "b", "[Body Content]", True)
            buy_id = SqlHelper.ExecuteScalar("select top 1 buy_id from tbl_auction_buy WITH (NOLOCK) where buy_type='partial'")
            lit_Send_Auction_Order_Mail_order_7.Text = objEmail.Send_Mail_Confirm_Receipt(buy_id, "b", "[Body Content]", True)

            Dim bid_id As Integer = SqlHelper.ExecuteScalar("select top 1 bid_id from tbl_auction_bids WITH (NOLOCK) where isnull(action,'')='Awarded'")
            If bid_id > 0 Then
                lit_SendWinningBidEmail_Awarded.Text = objEmail.SendWinningBidEmail(bid_id, True)
            End If
            bid_id = SqlHelper.ExecuteScalar("select top 1 bid_id from tbl_auction_bids WITH (NOLOCK) where isnull(action,'')='Denied'")
            If bid_id > 0 Then
                lit_SendWinningBidEmail_Denied.Text = objEmail.SendWinningBidEmail(bid_id, True)
            End If
            bid_id = SqlHelper.ExecuteScalar("select top 1 bid_id from tbl_auction_bids WITH (NOLOCK) where isnull(action,'')='Denied'")
            If bid_id > 0 Then
                lit_SendWinningBidEmail_Denied.Text = objEmail.SendWinningBidEmail(bid_id, True)
            End If

            buy_id = SqlHelper.ExecuteScalar("select top 1 buy_id from tbl_auction_buy WITH (NOLOCK) where isnull(action,'')='Awarded' and buy_type='buy now'")
            lit_SendBuyNowAcceptEmail_now.Text = objEmail.SendBuyNowAcceptEmail(buy_id, True)
            buy_id = SqlHelper.ExecuteScalar("select top 1 buy_id from tbl_auction_buy WITH (NOLOCK) where isnull(action,'')='Awarded' and buy_type='private'")
            lit_SendBuyNowAcceptEmail_private.Text = objEmail.SendBuyNowAcceptEmail(buy_id, True)
            buy_id = SqlHelper.ExecuteScalar("select top 1 buy_id from tbl_auction_buy WITH (NOLOCK) where isnull(action,'')='Awarded' and buy_type='partial'")
            lit_SendBuyNowAcceptEmail_partial.Text = objEmail.SendBuyNowAcceptEmail(buy_id, True)

            Dim quotation_id As Integer = SqlHelper.ExecuteScalar("select top 1 quotation_id from tbl_auction_quotations where isnull(action,'')='Awarded'")
            If quotation_id > 0 Then
                lit_SendQuotationAcceptEmail.Text = objEmail.SendQuotationAcceptEmail(quotation_id, True)
            End If

            lit_sendInvitationBeforeMail.Text = objEmail.sendInvitationBeforeMail_static(buyer_id)
            lit_sendInvitationBeforeEndMail.Text = objEmail.sendInvitationBeforeEndMail_static(buyer_id)
            lit_sendInvitationAfterMail.Text = objEmail.sendInvitationAfterMail_static(buyer_id)

            objEmail = Nothing
        End If
    End Sub
End Class
