﻿
Partial Class SiteLeftBar
    Inherits BasePage
    Public mnu_fav As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then

            If String.IsNullOrEmpty(Request.QueryString.Get("f")) Then
                If CommonCode.Fetch_Cookie_Shared("mnu_fav") <> "" Then
                    mnu_fav = CommonCode.Fetch_Cookie_Shared("mnu_fav")
                End If
            Else
                mnu_fav = Request.QueryString.Get("f")
                CommonCode.Create_Cookie_shared("mnu_fav", mnu_fav)
            End If

            Select Case Request.QueryString.Get("t")
                Case 0
                    If CommonCode.Fetch_Cookie_Shared("user_id") = "" Then
                        tab0.Visible = False
                    Else
                        tab0.Visible = True
                        If CommonCode.Fetch_Cookie_Shared("is_buyer") = "1" Then
                            div_personal.Visible = False
                        End If
                    End If
                Case Else
                    tabsetting()
            End Select
        End If
    End Sub

    Private Sub tabsetting()
        tab1.Visible = True

        Dim obj As New Auction
        Dim dt As New DataSet()
        dt = obj.fetch_auction_Listing(Request.QueryString.Get("t"), 0, 0, IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("user_id")), CommonCode.Fetch_Cookie_Shared("user_id"), 0), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("buyer_id")), CommonCode.Fetch_Cookie_Shared("buyer_id"), 0), SqlHelper.of_FetchKey("frontend_login_needed"))

        Dim dv As DataView

        dv = dt.Tables(1).DefaultView
        dv.RowFilter = "no_of_auction > 0"
        If dv.Count > 0 Then
            pnl_auctions.Visible = True
            rpt_auctions.DataSource = dv
            rpt_auctions.DataBind()
        Else
            pnl_auctions.Visible = False
        End If
        dt.Dispose()

        dv = dt.Tables(2).DefaultView
        dv.RowFilter = "no_of_auction > 0"
        If dv.Count > 0 Then
            pnl_buy_now.Visible = True
            rpt_buy_now.DataSource = dv
            rpt_buy_now.DataBind()
        Else
            pnl_buy_now.Visible = False
        End If
        dt.Dispose()

        dv = dt.Tables(3).DefaultView
        dv.RowFilter = "no_of_auction > 0"
        If dv.Count > 0 Then
            pnl_quote.Visible = True
            rpt_quote.DataSource = dv
            rpt_quote.DataBind()
        Else
            pnl_quote.Visible = False
        End If
        dt.Dispose()
        dt = Nothing

    End Sub

End Class
