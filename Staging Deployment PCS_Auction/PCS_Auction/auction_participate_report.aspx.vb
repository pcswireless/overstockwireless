﻿Imports Telerik.Web.UI
Imports Telerik.Charting

Partial Class auction_participate_report
    Inherits System.Web.UI.Page

    Protected Sub RadGrid_Auction_List_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid_Auction_List.ItemDataBound
        If e.Item.ItemType = GridItemType.AlternatingItem Or e.Item.ItemType = GridItemType.Item Then
            Dim RadChart1 As RadChart = DirectCast(e.Item.FindControl("Chart_Bidder"), RadChart)
            Dim dt As New DataTable()
            dt = SqlHelper.ExecuteDatatable("select top 3 A.bid_id,A.buyer_id,B.company_name,A.max_bid_amount,A.bid_amount from dbo.fn_get_current_status_chart_data(" & CType(e.Item.DataItem, DataRowView).Item("auction_id") & ") A inner join tbl_reg_buyers B WITH (NOLOCK) on A.buyer_id=B.buyer_id")
            'Response.Write("select A.bid_id,A.buyer_id,B.company_name,A.max_bid_amount,A.bid_amount from dbo.fn_get_current_status_chart_data(" & CType(e.Item.DataItem, DataRowView).Item("auction_id") & ") A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id")
            Try
                If dt.Rows.Count > 0 And CType(e.Item.DataItem, DataRowView).Item("auction_type_id") <> 1 Then

                    RadChart1.Visible = True
                

            Dim chart_max As Integer = 0
            Dim chart_step As Integer = 0

            Dim max_bid_amount As Double = 0

            max_bid_amount = dt.Rows(0).IsNull("bid_amount")



            If max_bid_amount > 1000 Then
                chart_max = (Convert.ToInt32(max_bid_amount / 1000) + 1) * 1000
                chart_step = 1000
            ElseIf max_bid_amount > 100 Then
                chart_max = (Convert.ToInt32(max_bid_amount / 100) + 1) * 100
                chart_step = 100
            ElseIf max_bid_amount > 10 Then
                chart_max = (Convert.ToInt32(max_bid_amount / 10) + 1) * 10
                If max_bid_amount > 50 Then
                    chart_step = 10
                Else
                    chart_step = 1
                End If
            Else
                chart_max = 10
                chart_step = 1
            End If

            RadChart1.PlotArea.YAxis.MaxValue = chart_max
            'RadChart1.PlotArea.YAxis.Step = chart_step
            'RadChart1.PlotArea.XAxis.Step = chart_step
            RadChart1.PlotArea.XAxis.AxisLabel.TextBlock.Text = "Bid -->"
            RadChart1.PlotArea.XAxis.AxisLabel.Visible = True
            RadChart1.PlotArea.YAxis.AxisLabel.TextBlock.Text = "Amount in $"
            RadChart1.PlotArea.YAxis.AxisLabel.Visible = True

            Dim ChrtSeries As New ChartSeries
            ChrtSeries.Name = "Bid"
            ChrtSeries.Appearance.FillStyle.MainColor = System.Drawing.Color.FromArgb(192, 80, 78)

            Dim ChrtSeries1 As New ChartSeries
            ChrtSeries1.Name = "Won"
            ChrtSeries1.Appearance.FillStyle.MainColor = System.Drawing.Color.FromArgb(79, 129, 190)

            ' Dim ChrtSeriesItem As ChartSeriesItem
            Dim ChrtSeriesItem1 As ChartSeriesItem
            Dim i As Integer
            For i = 0 To dt.Rows.Count - 1
                'ChrtSeriesItem = New ChartSeriesItem
                'ChrtSeriesItem.ActiveRegion.Tooltip = dt.Rows(i).Item("company_name")
                'ChrtSeriesItem.YValue = dt.Rows(i).Item("max_bid_amount")
                'ChrtSeriesItem.Label.Visible = False
                'ChrtSeries.Items.Add(ChrtSeriesItem)

                ChrtSeriesItem1 = New ChartSeriesItem
                ChrtSeriesItem1.ActiveRegion.Tooltip = dt.Rows(i).Item("company_name")
                ChrtSeriesItem1.YValue = dt.Rows(i).Item("bid_amount")
                ChrtSeriesItem1.Label.Visible = False
                ChrtSeries1.Items.Add(ChrtSeriesItem1)

            Next
            dt = Nothing
            'If auc_type = 3 Then RadChart1.Series.Add(ChrtSeries)
            RadChart1.Series.Add(ChrtSeries1)

        End If
            Catch ex As Exception

            End Try

        End If
    End Sub

    Protected Sub RadGrid_Auction_List_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid_Auction_List.NeedDataSource
        Dim sel As String = ""
        sel = "select distinct A.auction_id,isnull(code,'') as code,dbo.fn_get_auction_image(A.auction_id) As image_path,A.auction_type_id,A.title,isnull(A.sub_title,'') as sub_title,REPLACE(CONVERT(VARCHAR(9), A.start_date, 6), ' ', '-') AS start_date,REPLACE(CONVERT(VARCHAR(9), A.end_date, 6), ' ', '-') AS end_date, dbo.auction_status(A.auction_id) as status,(SELECT Count(buyer_id)FROM   tbl_auction_invited_bidders where auction_id=A.auction_id) AS invitated,case when A.auction_type_id=1 then (select count(distinct buyer_id) from tbl_auction_buy WITH (NOLOCK) where auction_id=A.auction_id) else (select count(distinct buyer_id) from tbl_auction_bids WITH (NOLOCK) where auction_id=A.auction_id) end as participated" & _
", case when A.auction_type_id=1 then '' else dbo.auction_participate_winner(A.auction_id) end as winner from [tbl_auctions] A WITH (NOLOCK) where A.end_date<getdate() and A.auction_type_id in (1,2,3)"

        RadGrid_Auction_List.DataSource = SqlHelper.ExecuteDatatable(sel)
        

    End Sub

End Class
