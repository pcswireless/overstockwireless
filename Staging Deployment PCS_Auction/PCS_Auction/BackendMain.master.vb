﻿Imports System
Imports Telerik.Web.UI
Partial Class BackendMain
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim is_user_login As Boolean = False
        Dim user_id As String = CommonCode.Fetch_Cookie_Shared("user_id")
        If Not String.IsNullOrEmpty(user_id) Then
            If user_id <> "" Then
                If IsNumeric(user_id) Then
                    If user_id > 0 Then
                        is_user_login = True
                    End If
                End If
            End If
        End If

        If is_user_login = False Then
            Response.Write("<script language='javascript'>top.location = '/login.html';</script>")

        End If
        If CommonCode.Fetch_Cookie_Shared("is_backend") = "0" Then
            Response.Write("<script language='javascript'>top.location = '/';</script>")

        End If
        

    End Sub

End Class

