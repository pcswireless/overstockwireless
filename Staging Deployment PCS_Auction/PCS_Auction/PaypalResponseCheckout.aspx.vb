
Partial Class ShoppingCart_ResponseCheckoutPP
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim mc_gross As String = Request("mc_gross")
            Dim invoice As String = Request("invoice")
            Dim auth_exp As String = Request("auth_exp")
            Dim protection_eligibility As String = Request("protection_eligibility")
            Dim payer_id As String = Request("payer_id")
            Dim payment_status As String = Request("payment_status")
            Dim txn_id As String = Request("txn_id")
            Dim payment_date As String = Request("payment_date")
            Dim tax As String = Request("mc_tax1")
            Dim shipping As String = Request("mc_shipping")
            Dim payer_status As String = Request("payer_status")
            Dim auth_amount As String = Request("auth_amount")
            Dim payment_gross As String = Request("payment_gross")
            Dim first_name As String = Request("first_name")
            Dim last_name As String = Request("last_name")
            Dim mc_currency As String = Request("mc_currency")

            Dim address_street As String = Request("address_street")
            Dim address_country_code As String = Request("address_country_code")
            Dim address_city As String = Request("address_city")
            Dim address_country As String = Request("address_country")
            Dim address_zip As String = Request("address_zip")
            Dim quantity As String = Request("quantity")
            Dim address_state As String = Request("address_state")
           
            Dim payer_email As String = Request("payer_email")
            Dim business_email As String = Request("business")

            If payment_status = "" And txn_id = "" Then
                payment_status = Request("st")
                txn_id = Request("tx")
            End If

            SqlHelper.ExecuteNonQuery("UPDATE tbl_paypal_transaction SET payment_status = '" & payment_status & "',txn_id = '" & txn_id & "',payment_date ='" & payment_date & "',mc_gross = '" & mc_gross & "',auth_exp = '" & auth_exp & "',protection_eligibility = '" & protection_eligibility & "',payer_id ='" & payer_id & "',quantity = '" & quantity & "',tax ='" & tax & "',shipping = '" & shipping & "',payer_status = '" & payer_status & "',auth_amount = '" & auth_amount & "',payment_gross = '" & payment_gross & "',	first_name = '" & first_name & "',last_name = '" & last_name & "',mc_currency = '" & mc_currency & "',address_street = '" & address_street & "',address_country_code = '" & address_country_code & "',address_city = '" & address_city & "',address_country = '" & address_country & "',address_zip = '" & address_zip & "',address_state = '" & address_state & "',payer_email ='" & payer_email & "',business_email ='" & business_email & "' WHERE invoice_no='" & invoice & "'")
            'Response.Write("payment_status = '" & payment_status & "',txn_id = '" & txn_id & "',payment_date ='" & payment_date & "',mc_gross = '" & mc_gross & "',auth_exp = '" & auth_exp & "',protection_eligibility = '" & protection_eligibility & "',payer_id ='" & payer_id & "',quantity = '" & quantity & "',tax ='" & tax & "',shipping = '" & shipping & "',payer_status = '" & payer_status & "',auth_amount = '" & auth_amount & "',payment_gross = '" & payment_gross & "',	first_name = '" & first_name & "',last_name = '" & last_name & "',mc_currency = '" & mc_currency & "',address_street = '" & address_street & "',address_country_code = '" & address_country_code & "',address_city = '" & address_city & "',address_country = '" & address_country & "',address_zip = '" & address_zip & "',address_state = '" & address_state & "',payer_email ='" & payer_email & "',business_email ='" & business_email & "',invoice_no='" & invoice & "'")
            If payment_status.ToUpper = "Completed".ToUpper Then
                lit_result.Text = "Your Payment received successfully.<br>PCS Receipt : " & invoice & "<br>Paypal txn id : " & txn_id
            Else
                lit_result.Text = "Payment not complete"
            End If

        End If
    End Sub
End Class
