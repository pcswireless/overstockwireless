﻿<%@ Page Language="VB" AutoEventWireup="false" EnableViewState="false" CodeFile="frame_price_summary_test.aspx.vb"
    Inherits="frame_price_summary_test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript">

        /***********************************************
        * Different CSS depending on OS (mac/pc)- © Dynamic Drive (www.dynamicdrive.com)
        * This notice must stay intact for use
        * Visit http://www.dynamicdrive.com/ for full source code
        ***********************************************/
        ///////No need to edit beyond here////////////

        var mactest = navigator.userAgent.indexOf("Mac") != -1


        if (mactest) {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet_mac.css"/>');
        }
        else {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet.css"/>');
        }
  </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="pnl_session">
        <center>
            <br />
            <span style="font-family: 'dinregular',arial; font-size: 20px; color: #EC6811;">Session Expired! Please Re-Login.</span></center>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnl_price">
        <asp:Literal ID="litRefresh" runat="server">
    <script type="text/javascript">        E_refresh = window.setTimeout(function () { window.location.href = window.location.href }, 5000);</script>
        </asp:Literal>
        <div style="width: 100%; overflow: hidden;">
            <asp:Literal ID="lit_price_setting" runat="server" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
