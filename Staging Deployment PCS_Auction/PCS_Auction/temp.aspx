﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AuctionMain.master" AutoEventWireup="false" CodeFile="temp.aspx.vb" Inherits="temp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" Runat="Server">
    <link href="/Style/temp/mb-comingsoon-iceberg.css" rel="stylesheet" />

<center>
  <div id="homeboxes" style="text-align:left">
        <div id="homeboxcontainer">
            <div id="homeboxcontent" style="width:900px !important; height:400px;">
                  <h1>Bidding starts July 30th. Private auctions for pre-approved resellers.</h1>
                  <div class="row centered text-center" id="myCounter"></div>
              </div>
            <div class="clear">
            </div>
         </div>
  </div>
</center>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>
      if (typeof jQuery == 'undefined') {
        document.write(unescape("%3Cscript src='/js/temp/Lib/jquery-1.11.0.min.js' type='text/javascript'%3E%3C/script%3E"));
      }
    </script>
    <script src="/js/temp/jquery.mb-comingsoon.min.js"></script>
    <script type="text/javascript">
      $(function () {
        var $section = $('section');
        $(window).on("resize", function () {
          var dif = Math.max($(window).height() - $section.height(), 0);
          var padding = Math.floor(dif / 2) + 'px';
          $section.css({ 'padding-top': padding, 'padding-bottom': padding });
        }).trigger("resize");
        $('#myCounter').mbComingsoon({ expiryDate: new Date(2014, 7, 0, -12, 1), speed: 150 });
        setTimeout(function () {
          $(window).resize();
        }, 200);
      });

    </script>
</asp:Content>

