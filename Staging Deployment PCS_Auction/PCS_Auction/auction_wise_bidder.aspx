﻿<%@ Page Title="" Language="VB" MasterPageFile="~/BackendMain.master" AutoEventWireup="false"
    CodeFile="auction_wise_bidder.aspx.vb" Inherits="Users_auction_wise_bidder" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <link rel="stylesheet" type="text/css" href="/Style/menu.css" />
    <div class="pageheading">
        Dashboard</div>
    <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
        EnableAJAX="false">
        <table cellspacing="0" cellpadding="0" border="0" width="988">
            <tr>
                <td>
                    <div>
                        <div style="width: 75px; float: left;">
                            <asp:Image runat="server" Height="72" Width="60" ID="img_sale_rep" />
                        </div>
                        <div style="width: 550px; float: left;">
                            <asp:Label runat="server" ID="lbl_sale_rep_name"></asp:Label><br />
                            <asp:Label runat="server" ID="lbl_sale_rep_email"></asp:Label><br />
                            <asp:Label runat="server" Font-Bold="true" ID="lbl_sale_rep_phone"></asp:Label><br />
                            <a style="color: #243E5A; text-decoration: none;" href="javscript:void(0);" onclick="return open_pop_query('','');">
                                <asp:Label runat="server" ForeColor="#363A83" Font-Bold="true" ID="lbl_sale_rep_query"></asp:Label></a>
                        </div>
                        <div style="width: 200px; float: left; text-align: left; padding-top: 0;" class="topR1">
                            <div class="l2" style="">
                                <asp:TextBox runat="server" Text="Search" ID="txt_search" onfocus="return clearBox();"
                                    AutoPostBack="true" ValidationGroup="sale_search" onkeydown="return on_key_down(event)"  onblur="return lostFocus()"
                                    CssClass="searchtextbox" />
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hid_user_id" runat="server" Value="0" />
                    <asp:Repeater runat="server" ID="rpt_parent_auction">
                        <ItemTemplate>
                            <table cellpadding="0" width="100%" cellspacing="0" style="padding-top: 40px; text-align: left;">
                                <tr>
                                    <td colspan="2">
                                        <div style="width: 350px; padding-top: 0px; color: #243E5A; font-weight: bold; font-size: 13px;
                                            float: left;">
                                            <a style="color: #363A83; text-decoration: none;" href='/Auction_Pdf.aspx?i=<%# Eval("auction_id") %>'
                                                onmouseout="hide_tip_new();" onmouseover="tip_new('/Auctions/auction_mouse_over.aspx?i=<%# Eval("auction_id") %>','250','white','true');">
                                                <%#Eval("title")%></a>
                                        </div>
                                        <div style="width: 150px; padding-left: 6px; color: #243E5A; font-weight: bold; font-size: 13px;
                                            float: left;">
                                            <a style="color: #243E5A; text-decoration: none;" href="javscript:void(0);" onclick="return open_pop_query('<%# Eval("auction_id") %>','');">
                                                <asp:Literal ID="lit_query_pending" Text='<%# Eval("query_count") & " Pending Query" %>'
                                                    runat="server"></asp:Literal></a>
                                        </div>
                                        <div style="width: 200px; padding-left: 6px; float: right;">
                                            <iframe id="iframe_timer" style="font-size: 14px;" src='/timerframe.aspx?i=<%# Eval("auction_id") %>'
                                                scrolling="no" frameborder="0" width="200px" height="25px"></iframe>
                                        </div>
                                        <div style="clear: both; overflow: hidden;">
                                            <hr />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style="padding: 0;">
                                        <asp:HiddenField ID="hid_auction_id" runat="server" Value='<%# Eval("auction_id") %>' />
                                        <asp:DataList ID="rpt_bidders" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                                            CellSpacing="0" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <div style='width: 325px; padding: 4px; text-align: left;'>
                                                    <div style='width: 312px; padding: 7px; text-align: left; border: 1px solid #E7E2DF'>
                                                        <div style="overflow: hidden; background-color: #F8F6F7;">
                                                            <div style="line-height: 17px; font-size: 13px; padding-left: 8px; padding-top: 3px;
                                                                border: 0px solid Red;">
                                                                <div style="color: #363A83; font-weight: bold;">
                                                                    <asp:Literal ID="ltrl_bidder_company" Text='<%# Eval("company_name") %>' runat="server"></asp:Literal>
                                                                </div>
                                                                <div>
                                                                <a style="text-decoration:none;color: #363A83;" href="javascript:void(0);" onmouseout="hide_tip_new();" onmouseover="tip_new('/Bidders/bidder_mouse_over.aspx?i=<%# Eval("buyer_id") %>','220','white','true');")"><%# Eval("name")%></a>
                                                                </div>
                                                                <div>
                                                                    <a style="text-decoration: none; color: #363A83;" href="mailto:<%# Eval("email")%>">
                                                                        <%# Eval("email")%></a>
                                                                </div>
                                                                <div style="color: #303030;">
                                                                    <asp:Literal ID="ltrl_bidder_phone" Text='<%# "Ph:" & Eval("phone1") %>' runat="server"></asp:Literal>
                                                                </div>
                                                            </div>
                                                            <div style="padding: 10px; text-align: center;">
                                                                <iframe id="iframe_rank" src='/buyer_rank_query.aspx?a=<%# Eval("auction_id") %>&b=<%# Eval("buyer_id") %>'
                                                                    scrolling="no" frameborder="0" width="170px" height="50px"></iframe>
                                                            </div>
                                                         </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                    <td>
                                        <div style="color: #105386;">
                                            <div>
                                                <asp:Label ID="ltrl_current_bid_status" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:SqlDataSource ID="SqlDataSource_Auctions" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                        ProviderName="System.Data.SqlClient" SelectCommand="" runat="server">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnl_noItem" runat="server">
                        <div style="border: 1px solid #10AAEA; background: url('/Images/fend/grdnt_template.jpg') repeat;
                            height: 800px; padding-top: 25px; text-align: center;">
                            <b>Auction Not available in this section</b>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <telerik:RadScriptBlock ID="RadScriptBlock_Main" runat="server">
        <script type="text/javascript">
            function lostFocus() {
                if (document.getElementById("<%=txt_search.ClientID %>").value == '') {
                    document.getElementById("<%=txt_search.ClientID %>").value = 'Search';
                    return false;
                }
                else
                    return true;
            }


            function clearBox() {
                if (document.getElementById("<%=txt_search.ClientID %>").value == 'Search')
                    document.getElementById("<%=txt_search.ClientID %>").value = '';
                return false;
            }

            function open_pop_query(aid, bid) {
                if (aid != '' & bid != '') {
                    w1 = window.open('/Backend_Dashbord_qry.aspx' + '?a=' + aid + '&b=' + bid, '_assign18', 'top=' + ((screen.height - 450) / 2) + ', left=' + ((screen.width - 550) / 2) + ', height=400, width=550,scrollbars=yes,toolbars=no,resizable=1;');
                    w1.focus();
                    return false;
                }
                else if (aid != '') {
                    w1 = window.open('/Backend_Dashbord_qry.aspx' + '?a=' + aid, '_assign18', 'top=' + ((screen.height - 450) / 2) + ', left=' + ((screen.width - 550) / 2) + ', height=400, width=550,scrollbars=yes,toolbars=no,resizable=1;');
                    w1.focus();
                    return false;
                }
                else {
                    w1 = window.open('/Backend_Dashbord_qry.aspx', '_assign18', 'top=' + ((screen.height - 450) / 2) + ', left=' + ((screen.width - 550) / 2) + ', height=400, width=550,scrollbars=yes,toolbars=no,resizable=1;');
                    w1.focus();
                    return false;
                }

            }
        </script>
    </telerik:RadScriptBlock>

      
</asp:Content>
