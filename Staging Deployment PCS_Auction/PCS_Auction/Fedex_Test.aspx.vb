﻿
Imports FedexRateServices
Partial Class Fedex_Test
    Inherits System.Web.UI.Page
    Private Sub fetchShippingRate(ByVal Destination_StreetLines As String, ByVal Destination_City As String, ByVal Destination_StateOrProvinceCode As String, ByVal destination_zip As String, ByVal destination_country_code As String, ByVal seq_no As Int32, ByVal package_count As Int32, ByVal auction_weight As Double)
        'Try
        ' Dim obj As New FedAllRateServices
        Dim objreq As New FedexRateServices.RateRequest
        Dim objrep As New FedexRateServices.RateReply
        Dim dt As New DataTable()
        Dim i As Integer
        Dim Origin_StreetLines, Origin_City, Origin_StateOrProvinceCode, Origin_PostalCode, Origin_CountryCode As String
        Origin_StreetLines = "3940 30th street" ' SqlHelper.of_FetchKey("fedex_source_street")
        Origin_City = "Long island city" 'SqlHelper.of_FetchKey("fedex_source_city")
        Origin_StateOrProvinceCode = "NY" 'SqlHelper.of_FetchKey("fedex_source_state")
        Origin_PostalCode = 11101 'SqlHelper.of_FetchKey("fedex_source_zip")
        Origin_CountryCode = "US" 'SqlHelper.of_FetchKey("fedex_source_country_code")


        dt = GetFedexShippingOptions(Origin_StreetLines, Origin_City, Origin_StateOrProvinceCode, Origin_PostalCode, Origin_CountryCode, Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, destination_zip, destination_country_code, seq_no, package_count, auction_weight).DefaultView.ToTable(True, "ServiceType", "Rate", "Error")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                With dt.Rows(i)
                    Response.Write((i + 1).ToString & ") Rate: " & .Item("Rate") & ", Service: " & .Item("ServiceType") & ", Error: " & .Item("Error") & "<br>")
                End With
            Next
        Else
            'SqlHelper.ExecuteNonQuery("if not exists(select fedex_rate_id from tbl_auction_fedex_rates where auction_id=" & auction_id & " and buyer_id =" & buyer_id & " and shipping_options='') begin INSERT INTO tbl_auction_fedex_rates(auction_id, buyer_id, shipping_amount,shipping_options,error_log,is_error) VALUES (" & auction_id & ", " & buyer_id & ",0,'',1) end")
        End If
        dt.Dispose()

        'Catch ex As Exception
        'Response.Write(ex.Message & "<br>")
        'End Try
    End Sub
    Public Function GetFedexShippingOptions(ByRef Origin_StreetLines As String, ByRef Origin_City As String, ByRef Origin_StateOrProvinceCode As String, ByRef Origin_PostalCode As String, ByRef Origin_CountryCode As String, ByRef Destination_StreetLines As String, ByRef Destination_City As String, ByRef Destination_StateOrProvinceCode As String, ByRef Destination_PostalCode As String, ByRef Destination_CountryCode As String, ByRef PackageLineItems_SequenceNumber As Int32, ByRef PackageLineItems_GroupPackageCount As Int32, ByRef PackageLineItems_weight_value As String) As DataTable

        Dim request As RateRequest = CreateRateRequest(Origin_StreetLines, Origin_City, Origin_StateOrProvinceCode, Origin_PostalCode, Origin_CountryCode, Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, Destination_PostalCode, Destination_CountryCode, PackageLineItems_SequenceNumber, PackageLineItems_GroupPackageCount, PackageLineItems_weight_value)
        '
        Dim service As RateService = New RateService() ' Initialize the service
        '
        ' Log the xml request
        '  Uncomment this piece of code to log the web service request. The request will be logged in 'access.log' file under bin folder
        'Dim tm As System.DateTime
        'Dim requestSerializer As New Serialization.XmlSerializer(GetType(RateRequest))
        'Dim file1 As FileInfo = New FileInfo(HttpContext.Current.Server.MapPath("/Upload/access.log"))
        'Dim sWriter As StreamWriter = file1.AppendText()
        'tm = Now
        'sWriter.WriteLine("{0} - Request:", tm)
        'requestSerializer.Serialize(sWriter, request)
        'sWriter.WriteLine()
        'sWriter.Close()
        '
        Try
            ' Call the web service passing in a RateRequest and returning a RateReply
            Dim reply As RateReply = service.getRates(request)
            '
            ' Log the xml reply
            ' Uncomment this piece of code to log the web service reply. The reply will be logged in 'access.log' file under bin folder
            'Dim replySerializer As New Serialization.XmlSerializer(GetType(RateReply))
            'Dim rWriter As StreamWriter = file1.AppendText()
            'tm = Now
            'rWriter.WriteLine()
            'rWriter.WriteLine("{0} Reply:", tm)
            'replySerializer.Serialize(rWriter, reply)
            'rWriter.WriteLine()
            'rWriter.Close()
            '

            If ((reply.HighestSeverity = NotificationSeverityType.SUCCESS) Or (reply.HighestSeverity = NotificationSeverityType.NOTE)) Then
                Return CType(ShowRateReply(reply, Destination_CountryCode), DataTable)
            Else
                Return CType(ShowErrorReply(reply), DataTable)
            End If
            '
            'Catch e As SoapException

        Catch e As Exception
            Dim dt As New DataTable
            dt.Columns.Add("ServiceType")
            dt.Columns.Add("Rate")
            dt.Columns.Add("Error")
            Return dt
        End Try

    End Function
    Function CreateRateRequest(ByRef Origin_StreetLines As String, ByRef Origin_City As String, ByRef Origin_StateOrProvinceCode As String, ByRef Origin_PostalCode As String, ByRef Origin_CountryCode As String, ByRef Destination_StreetLines As String, ByRef Destination_City As String, ByRef Destination_StateOrProvinceCode As String, ByRef Destination_PostalCode As String, ByRef Destination_CountryCode As String, ByRef PackageLineItems_SequenceNumber As Int32, ByRef PackageLineItems_GroupPackageCount As Int32, ByRef PackageLineItems_weight_value As String) As RateRequest
        ' Build a RateRequest
        Dim request As RateRequest = New RateRequest()
        '
        request.WebAuthenticationDetail = New WebAuthenticationDetail()
        request.WebAuthenticationDetail.UserCredential = New WebAuthenticationCredential()
        request.WebAuthenticationDetail.UserCredential.Key = SqlHelper.of_FetchKey("fedex_key") ' Replace "XXX" with the Key
        request.WebAuthenticationDetail.UserCredential.Password = SqlHelper.of_FetchKey("fedex_pwd") ' Replace "XXX" with the Password
        '
        request.ClientDetail = New ClientDetail()
        request.ClientDetail.AccountNumber = SqlHelper.of_FetchKey("fedex_acc") ' Replace "XXX" with client's account number
        request.ClientDetail.MeterNumber = SqlHelper.of_FetchKey("fedex_meter") ' Replace "XXX" with client's meter number
        '
        request.TransactionDetail = New TransactionDetail()
        request.TransactionDetail.CustomerTransactionId = "Test Back Field" ' This is a reference field for the customer.  Any value can be used and will be provided in the response.
        '
        request.Version = New VersionId() ' WSDL version information, value is automatically set from wsdl
        '
        request.ReturnTransitAndCommit = True
        request.ReturnTransitAndCommitSpecified = True
        '
        SetShipmentDetails(request, Origin_StreetLines, Origin_City, Origin_StateOrProvinceCode, Origin_PostalCode, Origin_CountryCode, Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, Destination_PostalCode, Destination_CountryCode, PackageLineItems_SequenceNumber, PackageLineItems_GroupPackageCount, PackageLineItems_weight_value)
        '
        Return request
    End Function
    Sub SetShipmentDetails(ByRef request As RateRequest, ByRef Origin_StreetLines As String, ByRef Origin_City As String, ByRef Origin_StateOrProvinceCode As String, ByRef Origin_PostalCode As String, ByRef Origin_CountryCode As String, ByRef Destination_StreetLines As String, ByRef Destination_City As String, ByRef Destination_StateOrProvinceCode As String, ByRef Destination_PostalCode As String, ByRef Destination_CountryCode As String, ByRef PackageLineItems_SequenceNumber As Int32, ByRef PackageLineItems_GroupPackageCount As Int32, ByRef PackageLineItems_weight_value As String)
        'RateAvailableServicesRequest(request1 = New RateAvailableServicesRequest())
        request.RequestedShipment = New RequestedShipment()
        request.RequestedShipment.ShipTimestamp = DateTime.Now ' Ship date and time
        request.RequestedShipment.ShipTimestampSpecified = True
        request.RequestedShipment.DropoffType = DropoffType.REGULAR_PICKUP 'Drop off types are BUSINESS_SERVICE_CENTER, DROP_BOX, REGULAR_PICKUP, REQUEST_COURIER, STATION
        request.RequestedShipment.DropoffTypeSpecified = True
        request.RequestedShipment.PackagingType = PackagingType.YOUR_PACKAGING
        request.RequestedShipment.PackagingTypeSpecified = True

        'request.RequestedShipment.FreightShipmentDetail.
        'request.RequestedShipment.p
        '
        '---add by ajay above

        SetOrigin(request, Origin_StreetLines, Origin_City, Origin_StateOrProvinceCode, Origin_PostalCode, Origin_CountryCode)
        '
        SetDestination(request, Destination_StreetLines, Destination_City, Destination_StateOrProvinceCode, Destination_PostalCode, Destination_CountryCode)
        '
        SetPackageLineItems(request, PackageLineItems_SequenceNumber, PackageLineItems_GroupPackageCount, PackageLineItems_weight_value)
        '''request.RequestedShipment.RateRequestTypes = New RateRequestType(1) {} ' Rate types requested LIST, MULTIWEIGHT, ...
        '''request.RequestedShipment.RateRequestTypes(0) = RateRequestType.ACCOUNT
        '''request.RequestedShipment.RateRequestTypes(1) = RateRequestType.LIST
        '''request.RequestedShipment.PackageCount = txt_package.Text
        ''''request.RequestedShipment.RateRequestTypes.

        ' set to true to request COD shipment
        ''Dim isCodShipment As Boolean = False
        ''If (isCodShipment) Then
        ''    SetCOD(request)
        ''End If

    End Sub
    Sub SetOrigin(ByRef request As RateRequest, ByRef Origin_StreetLines As String, ByRef Origin_City As String, ByRef Origin_StateOrProvinceCode As String, ByRef Origin_PostalCode As String, ByRef Origin_CountryCode As String)
        request.RequestedShipment.Shipper = New Party()
        request.RequestedShipment.Shipper.Address = New Address()

        If Origin_CountryCode.ToUpper() <> "US" Then
            request.RequestedShipment.Shipper.Address.StreetLines = New String(0) {Origin_StreetLines}
            request.RequestedShipment.Shipper.Address.City = Origin_City
            request.RequestedShipment.Shipper.Address.StateOrProvinceCode = Origin_StateOrProvinceCode
        End If

        request.RequestedShipment.Shipper.Address.PostalCode = Origin_PostalCode
        request.RequestedShipment.Shipper.Address.CountryCode = Origin_CountryCode

    End Sub
    Sub SetDestination(ByRef request As RateRequest, ByRef Destination_StreetLines As String, ByRef Destination_City As String, ByRef Destination_StateOrProvinceCode As String, ByRef Destination_PostalCode As String, ByRef Destination_CountryCode As String)
        request.RequestedShipment.Recipient = New Party()
        request.RequestedShipment.Recipient.Address = New Address()

        If Destination_CountryCode.ToUpper() <> "US" Then

            'request.RequestedShipment.ServiceType = ServiceType.INTERNATIONAL_PRIORITY
            request.RequestedShipment.Recipient.Address.StreetLines = New String(0) {Destination_StreetLines}
            request.RequestedShipment.Recipient.Address.City = Destination_City
            'request.RequestedShipment.Recipient.Address.StateOrProvinceCode = Destination_StateOrProvinceCode

        End If

        request.RequestedShipment.Recipient.Address.PostalCode = Destination_PostalCode
        request.RequestedShipment.Recipient.Address.CountryCode = Destination_CountryCode

    End Sub
    Sub SetPackageLineItems(ByRef request As RateRequest, ByRef PackageLineItems_SequenceNumber As Int32, ByRef PackageLineItems_GroupPackageCount As Int32, ByRef PackageLineItems_weight_value As String)

        '---add by ajay below        
        request.RequestedShipment.RateRequestTypes = New RateRequestType(1) {} ' Rate types requested LIST, MULTIWEIGHT, ...
        request.RequestedShipment.RateRequestTypes(0) = RateRequestType.ACCOUNT
        request.RequestedShipment.RateRequestTypes(1) = RateRequestType.LIST
        'request.RequestedShipment.RateRequestTypes(2) = RateRequestType.MULTIWEIGHT
        'request.RequestedShipment.Reque
        'request.RequestedShipment.PackageCount = txt_package.Text
        request.RequestedShipment.PackageCount = PackageLineItems_GroupPackageCount
        ''''comment by ajay
        'request.RequestedShipment.RequestedPackageLineItems = New RequestedPackageLineItem(2) {New RequestedPackageLineItem(), New RequestedPackageLineItem(), New RequestedPackageLineItem()}
        request.RequestedShipment.RequestedPackageLineItems = New RequestedPackageLineItem(0) {}
        request.RequestedShipment.RequestedPackageLineItems(0) = New RequestedPackageLineItem()
        'request.RequestedShipment.RequestedPackageLineItems = New RequestedPackageLineItem(2)
        request.RequestedShipment.RequestedPackageLineItems(0).SequenceNumber = 1 'PackageLineItems_SequenceNumber ' package sequence number
        request.RequestedShipment.RequestedPackageLineItems(0).GroupPackageCount = 1 ' PackageLineItems_GroupPackageCount
        ' Package weight
        'request.RequestedShipment.RequestedPackageLineItems(0).Dimensions = New Dimensions()
        request.RequestedShipment.RequestedPackageLineItems(0).Weight = New Weight()
        request.RequestedShipment.RequestedPackageLineItems(0).Weight.Units = WeightUnits.LB
        request.RequestedShipment.RequestedPackageLineItems(0).Weight.Value = CType(PackageLineItems_weight_value, Decimal)

        'request.RequestedShipment.RequestedPackageLineItems(1) = New RequestedPackageLineItem()
        'request.RequestedShipment.RequestedPackageLineItems(1).SequenceNumber = 2 'PackageLineItems_SequenceNumber ' package sequence number
        'request.RequestedShipment.RequestedPackageLineItems(1).GroupPackageCount = 1 ' PackageLineItems_GroupPackageCount
        '' Package weight
        'request.RequestedShipment.RequestedPackageLineItems(1).Weight = New Weight()
        'request.RequestedShipment.RequestedPackageLineItems(1).Weight.Units = WeightUnits.LB
        'request.RequestedShipment.RequestedPackageLineItems(1).Weight.Value = CType(PackageLineItems_weight_value / 3, Decimal)

        'request.RequestedShipment.RequestedPackageLineItems(2) = New RequestedPackageLineItem()
        'request.RequestedShipment.RequestedPackageLineItems(2).SequenceNumber = 3 ' PackageLineItems_SequenceNumber ' package sequence number
        'request.RequestedShipment.RequestedPackageLineItems(2).GroupPackageCount = 1 ' PackageLineItems_GroupPackageCount
        '' Package weight
        'request.RequestedShipment.RequestedPackageLineItems(2).Weight = New Weight()
        'request.RequestedShipment.RequestedPackageLineItems(2).Weight.Units = WeightUnits.LB
        'request.RequestedShipment.RequestedPackageLineItems(2).Weight.Value = CType(PackageLineItems_weight_value / 3, Decimal)
        '''request.RequestedShipment.RequestedPackageLineItems = New RequestedPackageLineItem(0) {New RequestedPackageLineItem()}
        '''request.RequestedShipment.RequestedPackageLineItems(0).SequenceNumber = PackageLineItems_SequenceNumber ' package sequence number
        '''request.RequestedShipment.RequestedPackageLineItems(0).GroupPackageCount = PackageLineItems_GroupPackageCount
        ' ''' Package weight
        '''request.RequestedShipment.RequestedPackageLineItems(0).Weight = New Weight()
        '''request.RequestedShipment.RequestedPackageLineItems(0).Weight.Units = WeightUnits.LB
        '''request.RequestedShipment.RequestedPackageLineItems(0).Weight.Value = CType(PackageLineItems_weight_value, Decimal)

    End Sub
    Sub SetCOD(ByRef request As RateRequest)
        ' To get all COD rates, set both COD details at both package and shipment level
        ' Set COD at Package level for Ground Services
        request.RequestedShipment.RequestedPackageLineItems(0).SpecialServicesRequested = New PackageSpecialServicesRequested()
        request.RequestedShipment.RequestedPackageLineItems(0).SpecialServicesRequested.SpecialServiceTypes = New PackageSpecialServiceType(0) {PackageSpecialServiceType.COD}
        '
        request.RequestedShipment.RequestedPackageLineItems(0).SpecialServicesRequested.CodDetail = New CodDetail()
        request.RequestedShipment.RequestedPackageLineItems(0).SpecialServicesRequested.CodDetail.CollectionType = CodCollectionType.GUARANTEED_FUNDS
        request.RequestedShipment.RequestedPackageLineItems(0).SpecialServicesRequested.CodDetail.CodCollectionAmount = New Money()
        request.RequestedShipment.RequestedPackageLineItems(0).SpecialServicesRequested.CodDetail.CodCollectionAmount.Amount = 250
        request.RequestedShipment.RequestedPackageLineItems(0).SpecialServicesRequested.CodDetail.CodCollectionAmount.AmountSpecified = True
        request.RequestedShipment.RequestedPackageLineItems(0).SpecialServicesRequested.CodDetail.CodCollectionAmount.Currency = "USD"


        '--add by ajay below
        '
        ''request.RequestedShipment.RequestedPackageLineItems(1).SpecialServicesRequested.CodDetail = New CodDetail()
        ''request.RequestedShipment.RequestedPackageLineItems(1).SpecialServicesRequested.CodDetail.CollectionType = CodCollectionType.GUARANTEED_FUNDS
        ''request.RequestedShipment.RequestedPackageLineItems(1).SpecialServicesRequested.CodDetail.CodCollectionAmount = New Money()
        ''request.RequestedShipment.RequestedPackageLineItems(1).SpecialServicesRequested.CodDetail.CodCollectionAmount.Amount = 250
        ''request.RequestedShipment.RequestedPackageLineItems(1).SpecialServicesRequested.CodDetail.CodCollectionAmount.AmountSpecified = True
        ''request.RequestedShipment.RequestedPackageLineItems(1).SpecialServicesRequested.CodDetail.CodCollectionAmount.Currency = "USD"
        ' ''
        ''request.RequestedShipment.RequestedPackageLineItems(2).SpecialServicesRequested.CodDetail = New CodDetail()
        ''request.RequestedShipment.RequestedPackageLineItems(2).SpecialServicesRequested.CodDetail.CollectionType = CodCollectionType.GUARANTEED_FUNDS
        ''request.RequestedShipment.RequestedPackageLineItems(2).SpecialServicesRequested.CodDetail.CodCollectionAmount = New Money()
        ''request.RequestedShipment.RequestedPackageLineItems(2).SpecialServicesRequested.CodDetail.CodCollectionAmount.Amount = 250
        ''request.RequestedShipment.RequestedPackageLineItems(2).SpecialServicesRequested.CodDetail.CodCollectionAmount.AmountSpecified = True
        ''request.RequestedShipment.RequestedPackageLineItems(2).SpecialServicesRequested.CodDetail.CodCollectionAmount.Currency = "USD"
        'add by ajay above
        ' Set COD at Shipment level for Express Services
        request.RequestedShipment.SpecialServicesRequested = New ShipmentSpecialServicesRequested() ' Special service requested
        request.RequestedShipment.SpecialServicesRequested.SpecialServiceTypes = New ShipmentSpecialServiceType(0) {ShipmentSpecialServiceType.COD}
        '
        request.RequestedShipment.SpecialServicesRequested.CodDetail = New CodDetail()
        request.RequestedShipment.SpecialServicesRequested.CodDetail.CodCollectionAmount = New Money()
        request.RequestedShipment.SpecialServicesRequested.CodDetail.CodCollectionAmount.Amount = 150
        request.RequestedShipment.SpecialServicesRequested.CodDetail.CodCollectionAmount.AmountSpecified = True
        request.RequestedShipment.SpecialServicesRequested.CodDetail.CodCollectionAmount.Currency = "USD"
        request.RequestedShipment.SpecialServicesRequested.CodDetail.CollectionType = CodCollectionType.GUARANTEED_FUNDS ' ANY, CASH, GUARANTEED_FUNDS
    End Sub
    Function ShowErrorReply(ByRef reply As RateReply) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("ServiceType")
        dt.Columns.Add("Rate")
        dt.Columns.Add("Error")

        If reply.Notifications Is Nothing Then Return dt

        Dim dr As DataRow = dt.NewRow()
        dr(0) = ""
        dr(1) = ""
        dr(2) = reply.Notifications(0).Message
        dt.Rows.Add(dr)

        Return dt
    End Function

    Private Function ShowRateReply(reply As RateReply) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("ServiceType")
        dt.Columns.Add("PackagingType")
        dt.Columns.Add("Rate")
        dt.Columns.Add("Error")
        For Each rateReplyDetail As RateReplyDetail In reply.RateReplyDetails
            Dim dr As DataRow = dt.NewRow()
            If rateReplyDetail.ServiceTypeSpecified Then
                dr(0) = rateReplyDetail.ServiceType.ToString()
            Else
                dr(0) = ""
            End If
            If rateReplyDetail.PackagingTypeSpecified Then
                dr(1) = rateReplyDetail.PackagingType.ToString()
            Else
                dr(1) = ""
            End If


            For Each shipmentDetail As RatedShipmentDetail In rateReplyDetail.RatedShipmentDetails
                ShowShipmentRateDetails(shipmentDetail)
            Next
            ShowDeliveryDetails(rateReplyDetail)
        Next
        If dt.Rows.Count = 0 Then
            Dim dr As DataRow = dt.NewRow()
            dr(0) = ""
            dr(1) = ""
            dr(2) = ""
            dr(3) = "SERVICE not available"
            dt.Rows.Add(dr)
        End If

        Return dt
    End Function

    ' below function comment by ajay

    Function ShowRateReply(ByRef reply As RateReply, ByVal Destination_CountryCode As String) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("ServiceType")
        dt.Columns.Add("Rate")
        dt.Columns.Add("Error")
        If (reply.RateReplyDetails Is Nothing) Then
            If reply.Notifications Is Nothing Then Return dt
            Dim dr As DataRow = dt.NewRow()
            dr(0) = ""
            dr(1) = ""
            dr(2) = reply.Notifications(0).Message
            dt.Rows.Add(dr)
            Return dt
        End If

        'Console.WriteLine("RateReply details:")
        'System.Web.HttpContext.Current.Response.Write("RateReply details:<br/>")

        For i As Integer = 0 To reply.RateReplyDetails.Length - 1

            Dim rateReplyDetail As RateReplyDetail = reply.RateReplyDetails(i)
            'rateReplyDetail.


            'Console.WriteLine("Rate Reply Detail for Service {0} ", i + 1)
            'System.Web.HttpContext.Current.Response.Write(String.Format("Rate Reply Detail for Service {0} ", i + 1) & ":<br />")

            If (rateReplyDetail.ServiceTypeSpecified) Then
                'System.Web.HttpContext.Current.Response.Write(String.Format("Service Type: {0}", rateReplyDetail.ServiceType) & "<br />")
            End If
            If (rateReplyDetail.PackagingTypeSpecified) Then
                'System.Web.HttpContext.Current.Response.Write(String.Format("Packaging Type: {0}", rateReplyDetail.PackagingType) & "<br />")
            End If

            If (rateReplyDetail.RatedShipmentDetails IsNot Nothing) Then
                For j As Integer = 0 To rateReplyDetail.RatedShipmentDetails.Length - 1

                    Dim shipmentDetail As RatedShipmentDetail = rateReplyDetail.RatedShipmentDetails(j)
                    'System.Web.HttpContext.Current.Response.Write(String.Format("---Rated Shipment Detail for Rate Type {0}---", j + 1) & "<br />")

                    'Response.Write(shipmentDetail.ShipmentRateDetail.RateType & "<br>")
                    'Response.Write("$" & ShowShipmentRateDetails(shipmentDetail) & " - " & rateReplyDetail.ServiceType.ToString() & "<br>")

                    If Destination_CountryCode <> "US" Then
                        If shipmentDetail.ShipmentRateDetail.RateType = ReturnedRateType.PAYOR_ACCOUNT_SHIPMENT Then
                            Dim dr As DataRow = dt.NewRow()
                            dr(0) = rateReplyDetail.ServiceType.ToString()
                            dr(1) = "$" & ShowShipmentRateDetails(shipmentDetail)
                            dr(2) = ""
                            dt.Rows.Add(dr)
                        End If

                    Else
                        Dim dr As DataRow = dt.NewRow()
                        If rateReplyDetail.ServiceType = ServiceType.FEDEX_2_DAY Or rateReplyDetail.ServiceType = ServiceType.FEDEX_EXPRESS_SAVER Or rateReplyDetail.ServiceType = ServiceType.PRIORITY_OVERNIGHT Then
                            If shipmentDetail.ShipmentRateDetail.RateType = ReturnedRateType.PAYOR_ACCOUNT_PACKAGE Then
                                If rateReplyDetail.ServiceType = ServiceType.FEDEX_2_DAY Then
                                    dr(0) = "FEDEX_2_DAY"
                                    dr(1) = "$" & ShowShipmentRateDetails(shipmentDetail)
                                    dr(2) = ""
                                    dt.Rows.Add(dr)
                                ElseIf rateReplyDetail.ServiceType = ServiceType.FEDEX_EXPRESS_SAVER Then
                                    dr(0) = "FEDEX_EXPRESS_SAVER"
                                    dr(1) = "$" & ShowShipmentRateDetails(shipmentDetail)
                                    dr(2) = ""
                                    dt.Rows.Add(dr)
                                ElseIf rateReplyDetail.ServiceType = ServiceType.PRIORITY_OVERNIGHT Then
                                    dr(0) = "PRIORITY_OVERNIGHT"
                                    dr(1) = "$" & ShowShipmentRateDetails(shipmentDetail)
                                    dr(2) = ""
                                    dt.Rows.Add(dr)
                                Else
                                    'Dim dr As DataRow = dt.NewRow()
                                    dr(0) = rateReplyDetail.ServiceType.ToString()
                                    dr(1) = "$" & ShowShipmentRateDetails(shipmentDetail)
                                    dr(2) = ""
                                    dt.Rows.Add(dr)
                                End If
                            Else
                                'Dim dr As DataRow = dt.NewRow()
                                dr(0) = rateReplyDetail.ServiceType.ToString()
                                dr(1) = "$" & ShowShipmentRateDetails(shipmentDetail)
                                dr(2) = ""
                                dt.Rows.Add(dr)
                            End If
                        Else
                            'Dim dr As DataRow = dt.NewRow()
                            dr(0) = rateReplyDetail.ServiceType.ToString()
                            dr(1) = "$" & ShowShipmentRateDetails(shipmentDetail)
                            dr(2) = ""
                            dt.Rows.Add(dr)
                            'Response.Write("$" & ShowShipmentRateDetails(shipmentDetail) & " - " & rateReplyDetail.ServiceType.ToString() & "<br>")
                        End If

                    End If

                    'ShowPackageRateDetails(shipmentDetail.RatedPackages)

                Next j
            End If
            'ShowDeliveryDetails(rateReplyDetail)
            'System.Web.HttpContext.Current.Response.Write("**********************************************************<br><br>")
            'End If
        Next i
        If dt.Rows.Count = 0 Then
            Dim dr As DataRow = dt.NewRow()
            dr(0) = ""
            dr(1) = ""
            dr(2) = "SERVICE not available"
            dt.Rows.Add(dr)
        End If

        Return dt
    End Function
    Function ShowShipmentRateDetails(ByRef shipmentDetail As RatedShipmentDetail) As String
        If (shipmentDetail Is Nothing) Then Return ""
        If (shipmentDetail.ShipmentRateDetail Is Nothing) Then Return ""
        Dim rateDetail As ShipmentRateDetail = shipmentDetail.ShipmentRateDetail
        'System.Web.HttpContext.Current.Response.Write("--- Shipment Rate Detail ---")
        '
        'System.Web.HttpContext.Current.Response.Write(String.Format("RateType: {0}", rateDetail.RateType) & "<br />")
        If (rateDetail.TotalBillingWeight IsNot Nothing) Then
            'System.Web.HttpContext.Current.Response.Write(String.Format("Total Billing Weight: {0} {1}", rateDetail.TotalBillingWeight.Value, rateDetail.TotalBillingWeight.Units) & "<br />")
        End If
        If (rateDetail.TotalBaseCharge IsNot Nothing) Then
            'System.Web.HttpContext.Current.Response.Write(String.Format("Total Base Charge: {0} {1}", rateDetail.TotalBaseCharge.Amount, rateDetail.TotalBaseCharge.Currency) & "<br />")
        End If
        If (rateDetail.TotalFreightDiscounts IsNot Nothing) Then
            ' System.Web.HttpContext.Current.Response.Write(String.Format("Total Freight Discounts: {0} {1}", rateDetail.TotalFreightDiscounts.Amount, rateDetail.TotalFreightDiscounts.Currency) & "<br />")
        End If
        If (rateDetail.TotalSurcharges IsNot Nothing) Then
            ' System.Web.HttpContext.Current.Response.Write(String.Format("Total Surcharges: {0} {1}", rateDetail.TotalSurcharges.Amount, rateDetail.TotalSurcharges.Currency) & "<br />")
        End If

        If (rateDetail.Surcharges IsNot Nothing) Then
            For Each surcharge As Surcharge In rateDetail.Surcharges
                '     System.Web.HttpContext.Current.Response.Write(String.Format(" {0} surcharge {1} {2}", surcharge.SurchargeType, surcharge.Amount.Amount, surcharge.Amount.Currency) & "<br />")
            Next surcharge
        End If
        If (rateDetail.TotalNetCharge IsNot Nothing) Then
            'System.Web.HttpContext.Current.Response.Write(String.Format("Total Net Charge: {0} {1}", rateDetail.TotalNetCharge.Amount, rateDetail.TotalNetCharge.Currency) & "<br />")
            Return CType(rateDetail.TotalNetCharge.Amount, String)
        End If
        Return ""
    End Function
    Sub ShowPackageRateDetails(ByRef ratedPackages As RatedPackageDetail())
        If (ratedPackages Is Nothing) Then Return
        'System.Web.HttpContext.Current.Response.Write("--- Rated Package Detail ---")

        For i As Integer = 0 To ratedPackages.Length - 1
            Dim ratedPackage As RatedPackageDetail = ratedPackages(i)
            'System.Web.HttpContext.Current.Response.Write(String.Format("Package {0}", i + 1) & "<br />")
            If (ratedPackage.PackageRateDetail IsNot Nothing) Then
                'System.Web.HttpContext.Current.Response.Write(String.Format("Billing weight {0} {1}", ratedPackage.PackageRateDetail.BillingWeight.Value, ratedPackage.PackageRateDetail.BillingWeight.Units) & "<br />")
                'System.Web.HttpContext.Current.Response.Write(String.Format("Base charge {0} {1}", ratedPackage.PackageRateDetail.BaseCharge.Amount, ratedPackage.PackageRateDetail.BaseCharge.Currency) & "<br />")
                If (ratedPackage.PackageRateDetail.TotalSurcharges IsNot Nothing) Then
                    'System.Web.HttpContext.Current.Response.Write(String.Format("Total Surcharges: {0} {1}", ratedPackage.PackageRateDetail.TotalSurcharges.Amount, ratedPackage.PackageRateDetail.TotalSurcharges.Currency) & "<br />")
                End If
                If (ratedPackage.PackageRateDetail.Surcharges IsNot Nothing) Then
                    For Each surcharge As Surcharge In ratedPackage.PackageRateDetail.Surcharges
                        'System.Web.HttpContext.Current.Response.Write(String.Format(" {0} surcharge {1} {2}", surcharge.SurchargeType, surcharge.Amount.Amount, surcharge.Amount.Currency) & "<br />")
                    Next
                End If
                'System.Web.HttpContext.Current.Response.Write(String.Format("Net charge {0} {1}", ratedPackage.PackageRateDetail.NetCharge.Amount, ratedPackage.PackageRateDetail.NetCharge.Currency) & "<br />")
            End If
        Next i
    End Sub
    Sub ShowDeliveryDetails(ByRef rateReplyDetail As RateReplyDetail)
        If (rateReplyDetail.DeliveryTimestampSpecified) Then
            System.Web.HttpContext.Current.Response.Write("Delivery timestamp: " & rateReplyDetail.DeliveryTimestamp.ToString)
        End If
        If (rateReplyDetail.TransitTimeSpecified) Then
            System.Web.HttpContext.Current.Response.Write("Transit time: " & rateReplyDetail.TransitTime.ToString)
        End If
    End Sub
    Sub ShowNotifications(ByRef reply As RateReply)
        System.Web.HttpContext.Current.Response.Write("Notifications")
        For i As Integer = 0 To reply.Notifications.Length - 1
            Dim notification As Notification = reply.Notifications(i)
            System.Web.HttpContext.Current.Response.Write(String.Format("Notification no. {0}", i) & "<br />")
            System.Web.HttpContext.Current.Response.Write(String.Format(" Severity: {0}", notification.Severity) & "<br />")
            System.Web.HttpContext.Current.Response.Write(String.Format(" Code: {0}", notification.Code) & "<br />")
            System.Web.HttpContext.Current.Response.Write(String.Format(" Message: {0}", notification.Message) & "<br />")
            System.Web.HttpContext.Current.Response.Write(String.Format(" Source: {0}", notification.Source) & "<br />")
        Next
    End Sub
    Protected Sub but_submit_Click(sender As Object, e As System.EventArgs) Handles but_submit.Click
        fetchShippingRate("1000 Bishop St", "Honolulu", "HI", "96813-4202", "US", 1, 1, "50")
    End Sub
End Class
