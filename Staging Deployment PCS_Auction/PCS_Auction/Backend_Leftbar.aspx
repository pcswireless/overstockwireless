﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Backend_Leftbar.aspx.vb"
    Inherits="Backend_Leftbar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" rel="Stylesheet" href="/Style/backend_leftbar.css" />
    <script type="text/javascript" language="JavaScript">

        var hide_menu_cols;
        var show_menu_cols;

        function init() {
            // load menu vars
            var framset_menu = parent.window.document.getElementById('framset_menu');
            hide_menu_cols = framset_menu.getAttribute('hide_menu_width') + ',*';
            show_menu_cols = framset_menu.getAttribute('show_menu_width') + ',*';

            // check if menu is hidden
            var framset_menu = parent.window.document.getElementById('framset_menu');
            if (framset_menu.cols == hide_menu_cols) {
                framset_menu.cols = '242,*'
                framset_menu.setAttribute('show_menu_width', 222)
                document.getElementById('img_drag').src = "/Images/hide.png";
                //document.getElementById('div_leftbar_arrow').className = "arrow_expanded";  //("class", );
                document.getElementById('div_leftbar').style.display = "block";
            }

            //            // check if menu need to hidden
            hu = window.location.search.substring(1);
            //alert(hu);
            if (hu == 't=0') {
                framset_menu.cols = '30,*'
                framset_menu.setAttribute('show_menu_width', 30)
                document.getElementById('img_drag').src = "/Images/show.png";
                document.getElementById('div_leftbar_arrow').className = "arrow_collapsed";
                document.getElementById('div_leftbar').style.display = "none";
            }
            //            //            else {
            //            //                document.getElementById('div_leftbar_arrow').className = "arrow_expanded";

            //            //            }
        }

        function closeLeftBar() {

            // load menu vars
            var framset_menu = parent.window.document.getElementById('framset_menu');
            hide_menu_cols = framset_menu.getAttribute('hide_menu_width') + ',*';
            show_menu_cols = framset_menu.getAttribute('show_menu_width') + ',*';
            // check if menu is hidden
            var framset_menu = parent.window.document.getElementById('framset_menu');
            if (framset_menu.cols == hide_menu_cols) {
                framset_menu.cols = '242,*'
                framset_menu.setAttribute('show_menu_width', 222)
                document.getElementById('img_drag').src = "/Images/hide.png";
                //document.getElementById('div_leftbar_arrow').className = "arrow_expanded";
                document.getElementById('div_leftbar').style.display = "block";
            }
            else {
                framset_menu.cols = '30,*'
                framset_menu.setAttribute('show_menu_width', 30)
                document.getElementById('img_drag').src = "/Images/show.png";
                //document.getElementById('div_leftbar_arrow').className = "arrow_collapsed";
                document.getElementById('div_leftbar').style.display = "none";
            }

        }

        function redirectIframe(path1, path2, path3) {
            if (path1 != '')
                top.topFrame.location = path1;
            if (path2 != '')
                top.leftFrame.location = path2;
            if (path3 != '')
                top.mainFrame.location = path3;
        }
        function redirectPage(path) {
            if (path != '')
                top.location.href = path;
        }

        // (C) 2004 www.CodeLifter.com
        // Free for all users, but leave in this header
        function framePrint(whichFrame) {
            parent[whichFrame].focus();
            parent[whichFrame].print();
        }

        function click_master() {
            if (document.getElementById('divmaster').style.display == "none")
                document.getElementById('divmaster').style.display = "block";
            else
                document.getElementById('divmaster').style.display = "none";
            return false;
        }
        function click_report() {
            if (document.getElementById('divreport').style.display == "none")
                document.getElementById('divreport').style.display = "block";
            else
                document.getElementById('divreport').style.display = "none";
            return false;
        }
        function click_help() {
          if (document.getElementById('divhelp').style.display == "none")
            document.getElementById('divhelp').style.display = "block";
          else
            document.getElementById('divhelp').style.display = "none";
          return false;
        }
    </script>
</head>
<body onload="init();" style="height: 100%; padding-left: 2px;">
    <form id="form1" runat="server" enableviewstate="false">
    <div id="div_leftbar_arrow" class="arrow_expanded">
        <img src="/Images/hide.png" id="img_drag" width="15" height="92" onclick="closeLeftBar();"
            style="cursor: pointer;"></div>
    <div id="div_leftbar" style="float: left; padding-top: 20px; width: 200px; display: block;
        overflow-y: auto; overflow-x: hidden">
        <div id="tab0" runat="server" visible="false">
            <% If View_Master Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="return click_master();">Masters</a>
            </div>
            <div id="divmaster" style="display: none; padding-bottom: 2px;">
                <div class="leftSubmnu" style="margin-top: -2px;">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Users/TitleListing.aspx')">
                        Position Listing</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/Iprestriction.aspx')">
                        Allowed IP Addresses</a>
                </div>
                <%--<div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/industrytype.aspx')">
                        Industry Types</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/bussinesstype.aspx')">
                        Business Types</a>
                </div>--%>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/buckets_listing.aspx')">
                        Bucket Listing</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/BucketFilterListing.aspx')">
                        Left bar Bucket</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/packaging.aspx')">
                        Packaging</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/stock_location.aspx')">
                        Stock Location</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/stockconditions.aspx')">
                        Stock Conditions</a>
                </div>
                <div class="leftSubmnu" style="display: none;">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/flag_master.aspx')">
                        Flag Listing</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/email_template_master.aspx')">
                        Email Profiles</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/email_master_list.aspx')">
                        Email Template</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/header_master.aspx')">
                        Email Header Template</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/footer_master.aspx')">
                        Email Footer Template</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/manufacturers.aspx')">
                        Manufacturers</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Master/ProductCategories.aspx')">
                        Product Categories</a>
                </div>
            </div>
            <% End If%>
            <% If New_Company Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/companies/seller-details.aspx')">
                    New Company</a>
            </div>
            <% End If%>
            <% If View_Company Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/companies/seller-listing.aspx')">
                    Company Listing</a>
            </div>
            <% End If%>
            <% If New_User Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/Users/Userdetail.aspx')">
                    New User</a>
            </div>
            <% End If%>
            <% If View_User Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/Users/usermaster.aspx')">
                    User Listing</a>
            </div>
            <% End If%>
            <% If New_Profile Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/Users/profiledetail.aspx')">
                    New Profile</a>
            </div>
            <% End If%>
            <% If View_Profile Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/Users/profiles.aspx')">
                    Profile Listing</a>
            </div>
            <% End If%>
            <% If New_Message_Board Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/MessageBoard/NewMessage.aspx')">
                    New Message Board</a>
            </div>
            <% End If%>
            <% If View_Message_Board Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/MessageBoard/Message-main.aspx')">
                    Message Board Listing</a>
            </div>
            <% End If%>
            <% If View_report_button Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="return click_report();">Reports</a>
            </div>
            <div id="divreport" style="display: none; padding-bottom: 2px;">
                <div class="leftSubmnu" style="margin-top: -2px;">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/auction_sale_report.aspx')">
                        Auction Sale</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/auction_participate_report.aspx')">
                        Auction Participate</a>
                </div>
            </div>
            <% End If%>
            <% If View_Help_List Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="return click_help();">Manage Help</a>
            </div>
            <div id="divhelp" style="display: none; padding-bottom: 2px;">
                <div class="leftSubmnu" style="margin-top: -2px;">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Help/Help.aspx')">
                        Backend Help List</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Help/CaptionHelp.aspx')">
                        Auction Help</a>
                </div>
            </div>
            <% End If%>
        </div>
        <div id="tab1" runat="server" visible="false">
            <%-- <% If View_Offer Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/Backend_Dashbord.aspx?t=<%=Request.QueryString.Get("t") %>')">
                    View Offers (<asp:Literal runat="server" ID="lit_offers"></asp:Literal>) </a>
            </div>
            <% End If%>--%>
            <% If View_Bidder_Bulk_Listing Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/Bidders/BidderApproval.aspx')">
                    <asp:Literal runat="server" ID="lit_bidder_approval1"></asp:Literal>
                </a>
            </div>
            <% End If%>
            <% If Auction_Queries Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/Backend_Dashbord_qry.aspx?t=1&q=0')">
                    Auction Queries (<asp:Literal runat="server" ID="lit_no_of_auction_queries"></asp:Literal>)
                </a>
            </div>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/Backend_Dashbord_qry.aspx?t=1&q=1')">
                    Quick Contact Queries (<asp:Literal runat="server" ID="lit_no_of_bidder_queries"></asp:Literal>)
                </a>
            </div>
            <% End If%>
            <%-- <% If View_Auction_Wise_Bidder Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/SalesHome.aspx')">
                    Auctions
                </a>
            </div>
            <% End If%>--%>
            <% If View_Bidder_Wise_Auction Then%>
            <%--<div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','','/bidder_wise_auction.aspx')">
                    Bidders Wise Auctions
                 </a>
            </div>--%>
            <% End If%>
        </div>
        <div id="tab2" runat="server" visible="false">
            <% If New_Bidder Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('','/Backend_Leftbar.aspx?t=2','/Bidders/AddEditBuyer.aspx')">
                    New Bidder</a>
            </div>
            <% End If%>
            <asp:Panel ID="pnl_bidder_submenu" runat="server">
                <div class="leftSubmnu" style="margin-top: -2px;">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Bidders/AddEditBuyer.aspx?t=1&i=<%=Request.QueryString.Get("b") %>#1')">
                        Basic Information</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Bidders/AddEditBuyer.aspx?t=2&i=<%=Request.QueryString.Get("b") %>#offinfo')">
                        Financial Information</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Bidders/AddEditBuyer.aspx?t=4&i=<%=Request.QueryString.Get("b") %>#4')">
                        Sub-Logins</a>
                </div>
                <% If Not CommonCode.is_bidder_approval_agent() Then%>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Bidders/AddEditBuyer.aspx?t=5&i=<%=Request.QueryString.Get("b") %>#5')">
                        Invite For Auctions</a>
                </div>
                <div class="leftSubmnu" style="margin-bottom: 10px;">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Bidders/AddEditBuyer.aspx?t=6&i=<%=Request.QueryString.Get("b") %>#6')">
                        Bidder Comments</a>
                </div>
                <% End If%>
            </asp:Panel>
            <% If View_Bidder_Bulk_Listing Then%>
            <div class="leftmnu" id="div_bidder_listing" runat="server">
                <a href="javascript:void(0);" onclick="redirectIframe('/Backend_TopBar.aspx?t=2','/Backend_Leftbar.aspx?t=2','/Bidders/Buyers.aspx')">
                    Bidder Listing</a>
            </div>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('/Backend_TopBar.aspx?t=2','/Backend_Leftbar.aspx?t=2','/Bidders/BidderApproval.aspx')">
                    <asp:Literal runat="server" ID="lit_bidder_approval"></asp:Literal>
                </a>
            </div>
            <% End If%>
            <%--  <% If CommonCode.is_admin_user() Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('/Backend_TopBar.aspx?t=2','/Backend_Leftbar.aspx?t=2','/Bidders/BidderChecklist.aspx')">
                    <asp:Literal runat="server" ID="lit_bidder_checklist" Text="Bidder Checklist"></asp:Literal>
                </a>
            </div>
            <% End If%>--%>
        </div>
        <div id="tab3" runat="server" visible="false">
            <% If New_Auction Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('/Backend_TopBar.aspx?t=3','/Backend_Leftbar.aspx?t=3','/Auctions/AddEditAuction.aspx')">
                    New Auction</a>
            </div>
            <% End If%>
            <asp:Panel ID="pnl_auction_submenu" runat="server">
                <div class="leftSubmnu" style="margin-top: -2px;">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Auctions/AddEditAuction.aspx?t=1&i=<%=Request.QueryString.Get("a") %>#1')">
                        Basic Information</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Auctions/AddEditAuction.aspx?t=8&i=<%=Request.QueryString.Get("a") %>#8')">
                        Other Details</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Auctions/AddEditAuction.aspx?t=10&i=<%=Request.QueryString.Get("a") %>#10')">
                        Terms and Conditions</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Auctions/AddEditAuction.aspx?t=2&i=<%=Request.QueryString.Get("a") %>#2')">
                        Items</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Auctions/AddEditAuction.aspx?t=3&i=<%=Request.QueryString.Get("a") %>#3')">
                        Attachments</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Auctions/AddEditAuction.aspx?t=4&i=<%=Request.QueryString.Get("a") %>#4')">
                        Bidding Details</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Auctions/AddEditAuction.aspx?t=5&i=<%=Request.QueryString.Get("a") %>');">
                        Invitations</a>
                </div>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Auctions/AddEditAuction.aspx?t=6&i=<%=Request.QueryString.Get("a") %>')">
                        After Launch</a>
                </div>
                <%--<div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Auctions/AddEditAuction.aspx?t=6&i=<%=Request.QueryString.Get("a") %>')">
                        Query</a>
                </div>--%>
                <div class="leftSubmnu">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Auctions/AddEditAuction.aspx?t=7&i=<%=Request.QueryString.Get("a") %>')">
                        Current Status</a>
                </div>
                <div class="leftSubmnu" style="margin-bottom: 10px;">
                    <a href="javascript:void(0);" onclick="redirectIframe('','','/Auctions/AddEditAuction.aspx?t=9&i=<%=Request.QueryString.Get("a") %>')">
                        Comments</a>
                </div>
            </asp:Panel>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('/Backend_TopBar.aspx?t=3','/Backend_Leftbar.aspx?t=3','/Auctions/AuctionListing.aspx')">
                    Auction Listing</a>
            </div>
            <% If Auction_Email_Schedule Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('/Backend_TopBar.aspx?t=3','/Backend_Leftbar.aspx?t=3','/Auctions/AuctionEmailSchedule.aspx')">
                    Email Schedule</a>
            </div>
            <% End If%>
            <% If View_Auction_Bulk_Listing Then%>
            <div class="leftmnu">
                <a href="javascript:void(0);" onclick="redirectIframe('/Backend_TopBar.aspx?t=3','/Backend_Leftbar.aspx?t=3','/Auctions/AuctionBulkListing.aspx')">
                    Bulk Listing</a>
            </div>
            <% End If%>
        </div>
        <% If View_rss_button Then%>
        <div class="leftmnu">
            <a href="javascript:void(0);" onclick="redirectIframe('','','/Rss.aspx')">RSS Feed</a>
        </div>
        <% End If%>
        <% If View_print_button Then%>
        <div class="printFrame">
            <a href="javascript:framePrint('mainFrame');">Print the Main Page</a>
        </div>
        <% End If%>
    </div>
    </form>
</body>
</html>
