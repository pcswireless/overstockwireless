﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SalesRep.ascx.vb" Inherits="UserControls_SalesRep" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<link rel="stylesheet" type="text/css" href="/Style/dddropdownpanel.css" />
<script type="text/javascript" src="/pcs_js/dddropdownpanel.js">

    /***********************************************
    * DD Drop Down Panel- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
    * This notice MUST stay intact for legal use
    * Visit Dynamic Drive at http://www.dynamicdrive.com/ for this script and 100s more
    ***********************************************/

</script>
 <style type="text/css">  
        .modalBackground {  
            background-color:Gray;  
            filter:alpha(opacity=70);  
            opacity:0.7;  
        }  
        #popup   
        {  
            background-color:White;  
        }  
        .target_display
        {
            display:none;
        }
    </style>  

<center>
    <div id="mypanel" class="ddpanel">
        <table cellpadding="0" cellspacing="0" width="844" border="0">
            <tr>
                <td class="RPR1C1">
                </td>
                <td class="RPR1C2">
                </td>
                <td class="RPR1C3">
                </td>
                <td class="RPR1C4">
                </td>
            </tr>
            <tr>
                <td colspan="4" class="RPR2C">
                    <div id="mypanelcontent" class="ddpanelcontent"     >
                        <div style="float:left;">
                            <div class="RPR2C1">
                            </div>
                            <div class="RPR2C2">
                                <div class="RPImage">
                                    <asp:Image ID="img_user" Width="60" Height="72" ImageUrl="~/Images/buyer_icon.gif"
                                        runat="server"></asp:Image>
                                </div>
                                    <asp:UpdatePanel ID="upd_contact_rp" runat="server">
                                        <ContentTemplate>
                                <div class="RPContact">
                                    <div class="RPContactName">
                                        <asp:Literal ID="lit_name" runat="server" />
                                    </div>
                                    <div class="RPContactPhone">
                                        <asp:Literal ID="lit_contact" runat="server" />
                                                                            <div id='dv_qukcont_confirm' style="width:160px;font-size:0.8em;padding-top:50px;line-height:11px;clear:both;overflow:hidden;">
                                                                            <asp:Label ID="rp_message" runat="server" Text="Thanks for your question. Someone will get back to you within 24 hours." ForeColor="Red"></asp:Label>
                                                                             </div>
                                    </div>
                                </div>
                                <div class="RPContactForm">
                                            <div class="RPContactTitle">
                                                Quick Contact<asp:HiddenField ID="hid_sales_rep_id" runat="server" Value="0" />
                                            </div>
                                            <div style="color: #0B457B; font-size: 12px; font-weight: bold; padding-top: 5px;">
                                                <asp:Literal ID="lit_contact1" runat="server" />
                                            </div>
                                            <div style="padding-top: 5px; overflow:hidden;">
                                                <asp:TextBox ID="txt_subject" runat="server" CssClass="RPTextBox_S"  Width="175" Height="15px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_subject"
                                                    ValidationGroup="_sales_contact" Display="Static" ErrorMessage="*" CssClass="error"></asp:RequiredFieldValidator>
                                                <ajax:TextBoxWatermarkExtender ID="TBWE1" runat="server" TargetControlID="txt_subject"
                                                    WatermarkText="Subject" WatermarkCssClass="WaterMarkedTextBox"  />
                                            </div>
                                            <div style="padding-top: 5px;">
                                                <asp:TextBox ID="txt_question" runat="server" CssClass="RPTextBox" Width="175" TextMode="MultiLine" Height="47px"
                                                    Rows="3" Columns="20"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_question"
                                                    ValidationGroup="_sales_contact" Display="Static" ErrorMessage="*" CssClass="error"></asp:RequiredFieldValidator>
                                                <ajax:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txt_question"
                                                    WatermarkText="Your Question" WatermarkCssClass="WaterMarkedTextBox" />
                                            </div>
                                            <div style="padding-top: 5px; vertical-align: top;">
                                            <div style="display:block;" id='dv_qukcont_ask'>
                                                <asp:ImageButton ID="btn_contact" runat="server" OnClientClick="question_wait('_sales_contact','qukcont')" ValidationGroup="_sales_contact"
                                                    ImageUrl="/Images/submit_1.png" />
                                                    </div>
                                                                      <div style="display:none;font-size:11px;" id='dv_qukcont_msg'>
                                                                      <span style="color:Red">Please Wait...</span>
                                                                            </div>
                                            </div>

        <%--<asp:Panel ID="Panel_Msg" runat="server" Style="display: none;border:solid 1px Gray;width:250px;height:115px;color:Black;background-color:White;" CssClass="modalPopup">  
          <center><div style="padding:10px 5px;"> <asp:Label ID="rp_message" runat="server" ForeColor="Red"
           CssClass="RPMsg" Text="Thanks for your question. Someone will get back to you within 24 hours."></asp:Label></div></center>
           <center><div style="padding:10px 5px;"><asp:ImageButton runat="server" ID="img_lnk_close" ImageUrl="/Images/close.gif" AlternateText="Close" /></div></center>
        </asp:Panel>  
          <asp:LinkButton runat="server" ID="link_target" CssClass="target_display"  />  
    <ajax:ModalPopupExtender ID="ModalPopupExtender" runat="server"   
        TargetControlID="link_target"  
        PopupControlID="Panel_Msg"   
        BackgroundCssClass="modalBackground"  
        CancelControlID="img_lnk_close"   
        DropShadow="false"   />--%>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="RPContactSummary">
                                    <div class="RPContactTitle">
                                    "Please feel free to<br />contact me with your<br />questions and concerns."
                                     </div>
                                </div>
                            </div>
                            <div class="RPR2C3">
                            </div>
                        </div>
                        <div style="border: 0px solid green;">
                            <div class="RPR3C1">
                            </div>
                            <div class="RPR3C2">
                                <div style="width: 238px; float: right; border: 0px solid red; height: 25px; background-color: #ADDCF0;">
                                </div>
                                <div style="width: 12px; float: right; border: 0px solid red; height: 25px; background-image: url('/Images/corner_reverse_L_sales_rep.png');
                                    background-color: White;">
                                </div>
                            </div>
                            <div class="RPR3C3">
                            </div>
                        </div>
                    
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td colspan="2" align="left" class="RPR4C3">
                    <div id="mypaneltab" class="ddpaneltab">
                        <a href="#"><span></span></a>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</center>
