﻿
Partial Class UserControls_bidder_salesrep
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            rp_message.Visible = False
            'ModalPopupExtender.Hide()
            If CommonCode.Fetch_Cookie_Shared("buyer_id") <> "" And IsNumeric(CommonCode.Fetch_Cookie_Shared("buyer_id")) Then
                Dim dt As New DataTable
                dt = SqlHelper.ExecuteDatatable("SELECT A.user_id,(A.first_name+' '+A.last_name) as name,isnull(A.mobile,'') as mobile,ISNULL(A.phone,'') As phone,ISNULL(A.phone_ext,'') As phone_ext,isnull(image_path,'') as image_path, isnull(A.email,'') as email,isnull(is_phone1_mobile,0) as is_phone1_mobile FROM tbl_sec_users A where A.user_id in (select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
                'Response.Write("SELECT A.user_id,(A.first_name+' '+A.last_name) as name,isnull(A.mobile,'') as mobile,ISNULL(A.phone,'') As phone,ISNULL(A.phone_ext,'') As phone_ext,isnull(image_path,'') as image_path, isnull(A.email,'') as email,isnull(is_phone1_mobile,0) as is_phone1_mobile FROM tbl_sec_users A where A.user_id in (select user_id from tbl_reg_sale_rep_buyer_mapping where buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")")
                If dt.Rows.Count > 0 Then
                    hid_sales_rep_id.Value = dt.Rows(0)("user_id").ToString
                    img_user.ImageUrl = IIf(dt.Rows(0)("image_path").ToString = "", "/images/imagenotavailable.gif", "/Upload/Users/" & dt.Rows(0)("user_id") & "/" & dt.Rows(0)("image_path"))
                    ltr_name1.Text = dt.Rows(0)("name").ToString
                    ltr_name2.Text = dt.Rows(0)("name").ToString
                    ltr_phone1.Text = IIf(dt.Rows(0)("mobile").ToString <> "" And CInt(dt.Rows(0)("is_phone1_mobile")) = 0 And dt.Rows(0)("phone_ext") <> "", dt.Rows(0)("mobile").ToString & " Ext." & dt.Rows(0)("phone_ext").ToString, dt.Rows(0)("mobile").ToString)
                    ltr_phone2.Text = dt.Rows(0)("phone").ToString
                    ltr_email.Text = "<a href='mailto:" & dt.Rows(0)("email").ToString & "'>" & dt.Rows(0)("email").ToString & "</a>"
                    'Dim auc_total As Integer = SqlHelper.ExecuteScalar("select COUNT(distinct I.auction_id) from tbl_auction_bids I where I.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & "")
                    'Dim auc_bidding As Integer = SqlHelper.ExecuteScalar("select COUNT(distinct A.auction_id) from tbl_auctions A inner join tbl_auction_bids I on A.auction_id=I.auction_id where dbo.get_auction_status(A.auction_id) in (1,2) and I.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & "")
                    'Dim auc_winning As Integer = SqlHelper.ExecuteScalar("select COUNT(distinct A.auction_id) from tbl_auctions A inner join tbl_auction_bids I on A.auction_id=I.auction_id where dbo.get_auction_status(A.auction_id) = 3 and dbo.buyer_bid_rank(A.auction_id," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ")=1")
                    'Dim auc_loosing As Integer = auc_total - (auc_bidding + auc_winning)
                    'lit_auction_count.Text = "Total: " & IIf(auc_total > 1, auc_total & " Auctions", auc_total & " Auction") & "<br>Bidding on: " & IIf(auc_bidding > 1, auc_bidding & " Auctions", auc_bidding & " Auction") & "<br>Winning: " & IIf(auc_winning > 1, auc_winning & " Auctions", auc_winning & " Auction") & "<br>Loosing: " & IIf(auc_loosing > 1, auc_loosing & " Auctions", auc_loosing & " Auction")
                Else
                    Me.Visible = False
                End If
            Else
                Me.Visible = False
            End If

        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        rp_message.Visible = False
        If Page.IsValid Then
            Dim str As String = "INSERT INTO tbl_auction_queries(auction_id,title, message, sales_rep_id, buyer_id, buyer_user_id, user_id, parent_query_id, submit_date) " & _
                                "VALUES (0,'" & txt_subject.Text.Trim() & "','" & question.Text.Trim().Replace(Char.ConvertFromUtf32(10), "<br/>").Replace("'", "''") & "'," & hid_sales_rep_id.Value & "," & CommonCode.Fetch_Cookie_Shared("buyer_id") & "," & CommonCode.Fetch_Cookie_Shared("user_id") & ", 0, 0,getdate()); select SCOPE_IDENTITY();"
            Dim query_id As Integer = 0
            query_id = SqlHelper.ExecuteScalar(str)


            Dim mailid As String
            Dim Comm As New Email()
            mailid = Comm.set_rep_contact_request_to_salesrep(hid_sales_rep_id.Value, txt_subject.Text, question.Text, query_id:=query_id)
            mailid = Comm.set_rep_contact_request_to_user(CommonCode.Fetch_Cookie_Shared("user_id"), txt_subject.Text, question.Text)
            Comm = Nothing

            txt_subject.Text = ""
            question.Text = ""

            If query_id > 0 Then
                ' Response.Write(query_id)
                rp_message.Visible = True
                'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "qus_pop", "alert('" & query_id & "');", True)
                'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "qus_pop", "w1 =window.open('/question_confirm_pop.aspx', '_QuestionPop', 'left=' + ((screen.width - 300) / 2) + ',top=' + ((screen.height - 300) / 2) + ',width=270,height=150,scrollbars=no,toolbars=no,resizable=no');w1.focus();", True)

            End If
            ' ModalPopupExtender.Show()
        Else
            ' ModalPopupExtender.Hide()
        End If
    End Sub
End Class
