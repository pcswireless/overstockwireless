﻿
Partial Class UserControls_My_Auctions
    Inherits System.Web.UI.UserControl

    Public mode As String = ""
    Public type As String = ""

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ViewState("buyer_id") = 0
            ViewState("user_id") = 0
            Dim buyer_id As String = CommonCode.Fetch_Cookie_Shared("buyer_id")
            Dim user_id As String = CommonCode.Fetch_Cookie_Shared("user_id")
            If Not String.IsNullOrEmpty(buyer_id) Then
                If buyer_id <> "" Then
                    If IsNumeric(buyer_id) Then
                        If buyer_id > 0 Then

                            If Not String.IsNullOrEmpty(user_id) Then
                                If user_id <> "" Then
                                    If IsNumeric(user_id) Then
                                        If user_id > 0 Then
                                            ViewState("buyer_id") = buyer_id
                                            ViewState("user_id") = user_id
                                        End If
                                    End If
                                End If
                            End If

                        End If
                    End If
                End If
            End If
            bindAuction()
        End If
    End Sub
    Private Sub bindAuction()

        Dim str As String = ""
        'Dim strQuery As String = "select auction_id,dbo.get_auction_status(auction_id) as status_id,case dbo.get_auction_status(auction_id) when 1 then display_end_time else start_date end as timeleft from tbl_auctions where tbl_auctions.is_active=1 " & str
        Dim strQuery As String = ""
        If Not String.IsNullOrEmpty(mode) Then
            If mode = "q" Then
                strQuery = "select A.auction_id,A.title,dbo.get_auction_status(A.auction_id) as status_id,case dbo.get_auction_status(A.auction_id) when 1 then display_end_time else start_date end as timeleft,B.filename,B.action,B.quotation_id,ISNULL(details,'') AS details,0 as buy_id from tbl_auctions A WITH (NOLOCK) right join tbl_auction_quotations B WITH (NOLOCK) on A.auction_id=B.auction_id  where B.buyer_user_id=" & ViewState("user_id") & ""
            ElseIf mode = "pr" Then
                strQuery = "select A.auction_id,A.title,dbo.get_auction_status(A.auction_id) as status_id,case dbo.get_auction_status(A.auction_id) when 1 then display_end_time else start_date end as timeleft,B.price,B.status,ISNULL(B.buy_id,0) As buy_id,0 as quotation_id from tbl_auctions A WITH (NOLOCK) inner join  tbl_auction_buy B WITH (NOLOCK) on A.auction_id = B.auction_id where B.buyer_id=" & ViewState("buyer_id") & " and B.buy_type='private'"
            ElseIf mode = "pa" Then
                strQuery = "select A.auction_id,A.title,dbo.get_auction_status(A.auction_id) as status_id,case dbo.get_auction_status(A.auction_id) when 1 then display_end_time else start_date end as timeleft,B.price,B.quantity,B.status,ISNULL(B.buy_id,0) As buy_id,0 as quotation_id from tbl_auctions A WITH (NOLOCK) inner join  tbl_auction_buy B WITH (NOLOCK) on A.auction_id = B.auction_id where B.buyer_id=" & ViewState("buyer_id") & " and B.buy_type='partial'"
            ElseIf mode = "bu" Then
                strQuery = "select A.auction_id,A.title,dbo.get_auction_status(A.auction_id) as status_id,case dbo.get_auction_status(A.auction_id) when 1 then display_end_time else start_date end as timeleft,B.price,B.status,ISNULL(B.buy_id,0) As buy_id,0 as quotation_id from tbl_auctions A WITH (NOLOCK) inner join  tbl_auction_buy B WITH (NOLOCK) on A.auction_id = B.auction_id where B.buyer_id=" & ViewState("buyer_id") & " and B.buy_type='buy now'"
            Else
                strQuery = "select A.auction_id,A.title,A.display_end_time,dbo.get_auction_status(A.auction_id) as status_id,case dbo.get_auction_status(A.auction_id) when 1 then display_end_time else start_date end as timeleft,B.bid_amount,B.bid_type,B.bid_id as buy_id,0 as quotation_id from tbl_auctions A WITH (NOLOCK) inner join tbl_auction_bids B WITH (NOLOCK) on A.rank1_bid_id=B.bid_id where A.display_end_time <=getdate() and B.buyer_id=" & ViewState("buyer_id") & ""
            End If
        End If

        Select Case type
            Case "1"
                strQuery = strQuery & "and dbo.get_auction_status(A.auction_id)=1"
            Case "2"
                strQuery = strQuery & " and dbo.get_auction_status(A.auction_id)=1 "
            Case "3"
                strQuery = strQuery & " and  dbo.get_auction_status(A.auction_id)=2"
            Case "4"
                strQuery = strQuery & " and  dbo.get_auction_status(A.auction_id)=3 "
            Case "5"
                strQuery = strQuery & " and display_end_time >= getdate() "
                If ViewState("user_id") > 0 Then
                    strQuery = strQuery & " and A.auction_id in (select auction_id from tbl_auction_bids WITH (NOLOCK) where buyer_id=" & ViewState("user_id") & ") "
                End If
            Case Else


        End Select
        strQuery = strQuery & "  order by status_id,timeleft "
        'lbl_test.Text = strQuery
        rpt_auctions.DataSource = SqlHelper.ExecuteDatatable(strQuery)
        rpt_auctions.DataBind()

        If rpt_auctions.Items.Count > 0 Then
            rpt_auctions.Visible = True
            pnl_noItem.Visible = False
        Else
            rpt_auctions.Visible = False
            pnl_noItem.Visible = True
        End If

    End Sub
    Private Sub setAuction(ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs, ByVal auction_id As Integer, ByVal buy_id As Integer, ByVal quotation_id As Integer)

        Dim str As String = "SELECT A.auction_id,"
        str = str & "ISNULL(A.code, '') AS code,"
        str = str & "ISNULL(A.title, '') AS title,"
        str = str & "ISNULL(A.sub_title, '') AS sub_title,"
        str = str & "ISNULL(A.start_date, '1/1/1900') AS start_date,ISNULL(display_end_time,'1/1/1900') As display_end_time,"
        str = str & "ISNULL(A.product_catetory_id, 0) AS product_catetory_id,"
        str = str & "ISNULL(A.stock_condition_id, 0) AS stock_condition_id,"
        str = str & "ISNULL(A.auction_category_id, 0) AS auction_category_id,"
        str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"
        str = str & "ISNULL(A.is_private_offer, 0) AS is_private_offer,"
        str = str & "ISNULL(A.is_partial_offer, 0) AS is_partial_offer,"
        str = str & "ISNULL(A.auto_acceptance_price_private, 0) AS auto_acceptance_price_private,"
        str = str & "ISNULL(A.auto_acceptance_price_partial, 0) AS auto_acceptance_price_partial,"
        str = str & "ISNULL(A.auto_acceptance_quantity, 0) AS auto_acceptance_quantity,"
        str = str & "ISNULL(A.no_of_clicks, 0) AS no_of_clicks,"
        str = str & "ISNULL(A.start_price, 0) AS start_price,"
        str = str & "ISNULL(A.reserve_price, 0) AS reserve_price,"
        str = str & "ISNULL(A.increament_amount, 0) AS increament_amount,"
        str = str & "ISNULL(A.buy_now_price, 0) AS buy_now_price,"
        str = str & "ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing,"
        str = str & "ISNULL(A.is_buy_it_now, 0) AS is_buy_it_now,"
        str = str & "ISNULL(A.request_for_quote_message, '') AS request_for_quote_message,"
        str = str & "ISNULL(A.show_price, 0) AS show_price,"
        str = str & "ISNULL(A.code_f, '') AS code_f,"
        str = str & "ISNULL(A.code_s, '') AS code_s,"
        str = str & "ISNULL(A.code_c, '') AS code_c,"
        str = str & "ISNULL(A.title_f, '') AS title_f,"
        str = str & "ISNULL(A.title_s, '') AS title_s,"
        str = str & "ISNULL(A.title_c, '') AS title_c,"
        str = str & "ISNULL(A.sub_title_f, '') AS sub_title_f,"
        str = str & "ISNULL(A.sub_title_s, '') AS sub_title_s,"
        str = str & "ISNULL(A.sub_title_c, '') AS sub_title_c,"
        str = str & "ISNULL(A.short_description, '') AS short_description,"
        str = str & "ISNULL(A.short_description_f, '') AS short_description_f,"
        str = str & "ISNULL(A.short_description_s, '') AS short_description_s,"
        str = str & "ISNULL(A.short_description_c, '') AS short_description_c,"
        str = str & "ISNULL((select count(distinct buyer_id) from tbl_auction_bids WITH (NOLOCK) where auction_id=A.auction_id), 0) AS no_of_bid,"
        str = str & "ISNULL((select max(bid_amount) from tbl_auction_bids WITH (NOLOCK) where auction_id=A.auction_id), 0) AS actual_price,"
        str = str & "isnull((case when exists(select bid_id from tbl_auction_bids WITH (NOLOCK) where auction_id=A.auction_id) then ISNULL((select max(bid_amount) from tbl_auction_bids where auction_id=A.auction_id) + ISNULL(A.increament_amount, 0), 0) else A.start_price end),0) AS bidding_price,"
        str = str & "isnull((select count(*) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and status='Accept' and auction_id=A.auction_id),0) as buy_it_now_bid,"
        If ViewState("buyer_id") = 0 Then
            str = str & "0 AS current_rank,"
            str = str & "0 AS bidder_price,"
        Else
            str = str & "dbo.buyer_bid_rank(A.auction_id," & ViewState("buyer_id") & ") AS current_rank,"
            str = str & "isnull((select max(bid_amount) from tbl_auction_bids WITH (NOLOCK) where auction_id=A.auction_id and buyer_id=" & ViewState("buyer_id") & " ),0) AS bidder_price,"
            str = str & "isnull((select max(max_bid_amount) from tbl_auction_bids WITH (NOLOCK) where auction_id=A.auction_id and buyer_id=" & ViewState("buyer_id") & " ),0) AS bidder_max_price,"

        End If
        str = str & "dbo.get_auction_status(A.auction_id) as auction_status,"
        str = str & "ISNULL(IMG.stock_image_id, 0) AS stock_image_id,ISNULL(IMG.filename, '') AS filename,"

        If mode = "q" Then
            str = str & "(select ISNULL(action,'') As status from tbl_auction_quotations where quotation_id=" & quotation_id & ") as auc_sta"
        ElseIf mode = "pr" Or mode = "pa" Or mode = "bu" Then
            str = str & "(select case ISNULL(status,'') when 'Accept' then (case ISNULL(action,'') when 'Awarded' then 'Accepted' when 'Denied' then 'Rejected' else 'Pending Decision' end) when 'Pending' then 'Pending Decision' when 'Reject' then 'Rejected' end from tbl_auction_buy WITH (NOLOCK) where buy_id=" & buy_id & ") as auc_sta"
       Else
            str = str & "'' as auc_sta"
        End If
        str = str & " FROM "
        str = str & "tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations L WITH (NOLOCK) on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B WITH (NOLOCK) on A.stock_condition_id=B.stock_condition_id "
        str = str & " left join tbl_auction_stock_images IMG WITH (NOLOCK) on A.auction_id=IMG.auction_id and IMG.position=1 "
        str = str & " WHERE "
        str = str & "A.auction_id =" & auction_id

        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDatatable(str)

        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)
                'CType(e.Item.FindControl("ltr_title"), Literal).Text = "<a href='/Auction_Pdf.aspx?i=" & auction_id & "&t=" & type & "' style='text-decoration: none;color:#323232;font-size:" & IIf(.Item("title").ToString.Length < 70, "22px", IIf(.Item("title").ToString.Length < 150, "18px", IIf(.Item("title").ToString.Length < 200, "16px", "14px"))) & ";'>" & .Item("title").ToString & "</a>"
                'CType(e.Item.FindControl("ltr_title"), Literal).Text = "<a target='_blank' href='/Auction_Pdf.aspx?i=" & auction_id & "' style='text-decoration: none;color:#323232;font-size:" & IIf(.Item("title").ToString.Length < 70, "22px", IIf(.Item("title").ToString.Length < 150, "18px", IIf(.Item("title").ToString.Length < 200, "16px", "14px"))) & ";'>" & .Item("title").ToString & "</a>"
                'CType(e.Item.FindControl("ltr_title"), Literal).Text = "<a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "' style='text-decoration: none;color:#323232;font-size:" & IIf(.Item("title").ToString.Length < 70, "22px", IIf(.Item("title").ToString.Length < 150, "18px", IIf(.Item("title").ToString.Length < 200, "16px", "14px"))) & ";'>" & .Item("title").ToString & "</a>"
                CType(e.Item.FindControl("ltr_title"), Literal).Text = "<a href='/auction_details.aspx?t=4&" & IIf(mode.Trim = "", "", "m=" & mode & "&") & IIf(quotation_id > 0, "i=" & Security.EncryptionDecryption.EncryptValueFormatted(quotation_id) & "&", "") & IIf(buy_id > 0, "i=" & Security.EncryptionDecryption.EncryptValueFormatted(buy_id) & "&", "") & "a=" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_id")) & "' style='text-decoration: none;color:#323232;font-size:" & IIf(.Item("title").ToString.Length < 70, "22px", IIf(.Item("title").ToString.Length < 150, "18px", IIf(.Item("title").ToString.Length < 200, "16px", "14px"))) & ";'>" & .Item("title").ToString & "</a>"
                If .Item("filename").ToString <> "" Then
                    CType(e.Item.FindControl("img_auction_image"), Image).ImageUrl = "/upload/auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString.Replace(".", "_230.")
                    str = "SELECT A.auction_id,convert(varchar,A.auction_id)+'/'+convert(varchar,A.stock_image_id)+'/'+A.filename as path, b.title "
                    str = str & " from tbl_auction_stock_images A WITH (NOLOCK) inner join tbl_auctions b WITH (NOLOCK) on a.auction_id=b.auction_id where A.auction_id=" & auction_id
                    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).HRef = "/auction_details.aspx?t=4&" & IIf(mode.Trim = "", "", "m=" & mode & "&") & IIf(quotation_id > 0, "i=" & Security.EncryptionDecryption.EncryptValueFormatted(quotation_id) & "&", "") & IIf(buy_id > 0, "i=" & Security.EncryptionDecryption.EncryptValueFormatted(buy_id) & "&", "") & "a=" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_id"))
                    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).Title = .Item("title")
                Else
                    CType(e.Item.FindControl("img_auction_image"), Image).ImageUrl = "/images/imagenotavailable.gif"
                    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).HRef = "/auction_Details.aspx?t=4&" & "m=" & mode & "&" & IIf(quotation_id > 0, "i=" & Security.EncryptionDecryption.EncryptValueFormatted(quotation_id) & "&", "") & IIf(buy_id > 0, "i=" & Security.EncryptionDecryption.EncryptValueFormatted(buy_id) & "&", "") & "a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id)


                End If

                Dim start_date As DateTime = .Item("start_date")
                Dim cur_date As DateTime = DateTime.Now()
                Dim display_end_time As DateTime = .Item("display_end_time")

                Dim span As TimeSpan = display_end_time.Subtract(cur_date)
                Dim time_left As String = ""
                Select Case .Item("auction_status")
                    Case 1
                        CType(e.Item.FindControl("ltrl_status"), Literal).Text = "<span style='font-size:14px;color:#339933;'>Running</span><br /><span>Closing on : " & display_end_time.ToShortDateString() & "</span>"
                    Case 2
                        span = start_date.Subtract(cur_date)
                        time_left = span.Days & "D " & span.Hours & "H " & span.Minutes & "M" & " left to start"
                        CType(e.Item.FindControl("ltrl_status"), Literal).Text = "<span style='color:#FF7F00; font-size:14px;'>Upcoming</span><br /><span>" & time_left & "</span>"

                    Case 3
                        CType(e.Item.FindControl("ltrl_status"), Literal).Text = "<span style='color:#FF2A2A; font-size:14px;'>Closed</span><br /><span>Closed on : " & display_end_time.ToShortDateString() & "</span>"
                End Select


                'CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng' style='padding:6px 0 7px'><a href='/auction_details.aspx?t=4&" & IIf(mode.Trim = "", "", "m=" & mode & "&") & IIf(quotation_id > 0, "i=" & quotation_id & "&", "") & IIf(buy_id > 0, "i=" & buy_id & "&", "") & "a=" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_id")) & "'>MORE INFO</a></div>"
                CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng' style='padding:6px 0 7px'><a href='/auction_details.aspx?t=4&" & IIf(mode.Trim = "", "", "m=" & mode & "&") & IIf(quotation_id > 0, "i=" & Security.EncryptionDecryption.EncryptValueFormatted(quotation_id) & "&", "") & IIf(buy_id > 0, "i=" & Security.EncryptionDecryption.EncryptValueFormatted(buy_id) & "&", "") & "a=" & Security.EncryptionDecryption.EncryptValueFormatted(.Item("auction_id")) & "'>MORE INFO</a></div>"
                If mode = "q" Then
                    CType(e.Item.FindControl("pnl_iframe"), Panel).Visible = True
                    ' CType(e.Item.FindControl("lit_approval_status"), Literal).Text = IIf(.Item("auc_sta") = "" Or .Item("auc_sta").ToString().ToUpper() = "PENDING", "Pending Decision", .Item("auc_sta"))
                Else
                    CType(e.Item.FindControl("pnl_iframe"), Panel).Visible = False
                    CType(e.Item.FindControl("lit_approval_status"), Literal).Text = .Item("auc_sta")
                End If
                If CType(e.Item.FindControl("lit_approval_status"), Literal).Text.Trim = "" Then
                    CType(e.Item.FindControl("lit_auc_status"), Literal).Text = ""
                End If
            End With

        Else
            e.Item.Visible = False
        End If
        dtTable = Nothing
    End Sub
    Protected Sub rpt_auctions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_auctions.ItemDataBound
        setAuction(e, CType(e.Item.FindControl("hid_auction_id"), HiddenField).Value, CType(e.Item.FindControl("hid_buy_id"), HiddenField).Value, CType(e.Item.FindControl("hid_quotation_id"), HiddenField).Value)

    End Sub
End Class
