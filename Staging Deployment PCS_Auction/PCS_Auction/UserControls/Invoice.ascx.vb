﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class UserControls_Invoice
    Inherits System.Web.UI.UserControl
    Private _inv_id As Integer

    Public Property setInvoiceId As Integer
        Get
            Return _inv_id
        End Get
        Set(ByVal value As Integer)
            _inv_id = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            bindSummary()
            bindRepeter()
        End If
    End Sub

    Private Sub bindSummary()

        Dim sqlQry As String = "SELECT A.invoice_id,"
        sqlQry = sqlQry & "ISNULL(A.create_date, '1/1/1900') AS create_date,"        sqlQry = sqlQry & "ISNULL(A.dealer_id, 0) AS dealer_id,"        sqlQry = sqlQry & "ISNULL(A.payment_option_id, 0) AS payment_option_id,"        sqlQry = sqlQry & "ISNULL(A.total_amount, 0) AS total_amount,"        sqlQry = sqlQry & "ISNULL(A.estimated_shipping_amount, 0) AS estimated_shipping_amount,"        sqlQry = sqlQry & "ISNULL(A.net_amount, 0) AS net_amount,"        sqlQry = sqlQry & "ISNULL(A.company_name, '') AS company_name,"        sqlQry = sqlQry & "ISNULL(A.owner_name, '') AS owner_name,"        sqlQry = sqlQry & "ISNULL(A.address1, '') AS address1,"        sqlQry = sqlQry & "ISNULL(A.address2, '') AS address2,"        sqlQry = sqlQry & "ISNULL(A.city, '') AS city,"        sqlQry = sqlQry & "ISNULL(A.state, '') AS state,"        sqlQry = sqlQry & "ISNULL(A.country, '') AS country,"        sqlQry = sqlQry & "ISNULL(A.zip, '') AS zip,"        sqlQry = sqlQry & "ISNULL(A.phone1, '') AS phone1,"        sqlQry = sqlQry & "ISNULL(A.phone2, '') AS phone2,"        sqlQry = sqlQry & "ISNULL(A.fax, '') AS fax,"        sqlQry = sqlQry & "ISNULL(A.email, '') AS email,"        sqlQry = sqlQry & "ISNULL(A.comments, '') AS comments,"
        sqlQry = sqlQry & "ISNULL(B.title, '') AS payment_option"
        sqlQry = sqlQry & " FROM tbl_invoices A inner join tbl_payment_options B on A.payment_option_id=B.payment_option_id WHERE A.invoice_id = " & _inv_id


        Dim ds As New DataTable
        ds = SqlHelper.ExecuteDatatable(sqlQry)

        With ds.Rows(0)

            lit_order_no.Text = .Item("invoice_id")
            lit_date.Text = Convert.ToDateTime(.Item("create_date")).ToString("MM/dd/yyyy")
            lit_total.Text = FormatCurrency(.Item("net_amount"), 2)
            lit_shipping.Text = FormatCurrency(.Item("estimated_shipping_amount"), 2)
            lit_payble.Text = FormatCurrency(.Item("total_amount"), 2)
            lbl_payment_method.Text = .Item("payment_option")
            sqlQry = .Item("owner_name")
            sqlQry = sqlQry & "<br />" & .Item("company_name")
            sqlQry = sqlQry & "<br />" & .Item("address1") & " " & .Item("address2")
            sqlQry = sqlQry & "<br />" & .Item("city") & ", " & .Item("state") & " " & .Item("zip")
            If .Item("phone1") <> "" Then sqlQry = sqlQry & "<br />Phone: " & .Item("phone1")

            lit_bill_to.Text = sqlQry

        End With
        ds.Dispose()
    End Sub

    Private Sub bindRepeter()

        Dim sqlstr As String = "select A.invoice_item_id,P.product_id,isnull(P.name,'') as name,isnull(A.quantity,0) as quantity,ISNULL(A.unit_price, 0) AS unit_price,ISNULL(A.total_price, 0) AS total_price from tbl_products P inner join tbl_invoice_line_items A on P.product_id=A.product_id where A.invoice_id=" & _inv_id
        rpt_device.DataSource = SqlHelper.ExecuteDatatable(sqlstr)
        rpt_device.DataBind()

    End Sub

End Class
