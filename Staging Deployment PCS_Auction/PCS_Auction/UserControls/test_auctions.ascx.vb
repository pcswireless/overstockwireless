﻿
Partial Class UserControls_test_auctions
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            pnl_noItem.Visible = False
            bindGrid()
        End If
    End Sub
    Private Sub bindGrid()

        'Response.Write(IIf(String.IsNullOrEmpty(Request.QueryString.Get("t")), 1, Request.QueryString.Get("t")) & "," & IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), 0, Request.QueryString.Get("c")) & "," & IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), 0, Request.QueryString.Get("p")) & "," & IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("user_id")), CommonCode.Fetch_Cookie_Shared("user_id"), 0) & "," & IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("buyer_id")), CommonCode.Fetch_Cookie_Shared("buyer_id"), 0) & "," & SqlHelper.of_FetchKey("frontend_login_needed") & "," & IIf(String.IsNullOrEmpty(Request.QueryString.Get("a")), 0, Request.QueryString.Get("a")))
        Dim obj As New Auction

        Dim dv As DataView = obj.fetch_auction_Listing_new(IIf(String.IsNullOrEmpty(Request.QueryString.Get("t")), 1, Request.QueryString.Get("t")), IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), 0, Request.QueryString.Get("c")), IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), 0, Request.QueryString.Get("p")), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("user_id")), CommonCode.Fetch_Cookie_Shared("user_id"), 0), IIf(IsNumeric(CommonCode.Fetch_Cookie_Shared("buyer_id")), CommonCode.Fetch_Cookie_Shared("buyer_id"), 0), SqlHelper.of_FetchKey("frontend_login_needed"), IIf(String.IsNullOrEmpty(Request.QueryString.Get("a")), 0, Request.QueryString.Get("a"))).Tables(0).DefaultView
        If dv.Count > 0 Then
            iframe_auctions.Attributes.Add("src", "/frame_auction_calculate.aspx?t=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("t")), 1, Request.QueryString.Get("t")) & "&c=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("c")), 0, Request.QueryString.Get("c")) & "&p" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("p")), 0, Request.QueryString.Get("p")))

            rpt_auctions.DataSource = dv
            rpt_auctions.DataBind()
            pnl_noItem.Visible = False
            pn_auction_content.Visible = True

        Else

            pn_auction_content.Visible = False
            pnl_noItem.Visible = True

            Select Case Request.QueryString.Get("t")
                Case 1
                    lit_no_item.Text = "Live Auctions currently not available"
                Case 2
                    lit_no_item.Text = "Auction History currently not available"
                Case 3
                    lit_no_item.Text = "My Auctions currently not available"
                Case 4
                    lit_no_item.Text = "UpComing auctions currently not available"
                Case 5
                    lit_no_item.Text = "Hidden auctions currently not available"
                Case 6
                    lit_no_item.Text = "Bidding auctions currently not available"
                Case Else
                    lit_no_item.Text = "Auction Not available in this section"
            End Select


        End If

        dv.Dispose()
        obj = Nothing

    End Sub
    Private Sub setAuction(ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs, ByVal auction_id As Integer)

        Dim str As String = "SELECT A.auction_id,"
        str = str & "ISNULL(A.code, '') AS code,"
        str = str & "ISNULL(A.title, '') AS title,"
        str = str & "ISNULL(A.sub_title, '') AS sub_title,"
        str = str & "ISNULL(A.product_catetory_id, 0) AS product_catetory_id,"
        str = str & "ISNULL(A.stock_condition_id, 0) AS stock_condition_id,"
        str = str & "ISNULL(A.auction_category_id, 0) AS auction_category_id,"
        str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"
        str = str & "ISNULL(A.is_private_offer, 0) AS is_private_offer,"
        str = str & "ISNULL(A.is_partial_offer, 0) AS is_partial_offer,"
        str = str & "ISNULL(A.auto_acceptance_price_private, 0) AS auto_acceptance_price_private,"
        str = str & "ISNULL(A.auto_acceptance_price_partial, 0) AS auto_acceptance_price_partial,"
        str = str & "ISNULL(A.auto_acceptance_quantity, 0) AS auto_acceptance_quantity,"
        str = str & "ISNULL(A.no_of_clicks, 0) AS no_of_clicks,"
        str = str & "ISNULL(A.start_price, 0) AS start_price,"
        str = str & "ISNULL(A.reserve_price, 0) AS reserve_price,"
        str = str & "ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value,"

        str = str & "ISNULL(A.is_show_reserve_price, 0) AS is_show_reserve_price,"
        str = str & "ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing,"
        str = str & "ISNULL(A.is_show_no_of_bids, 0) AS is_show_no_of_bids,"
        str = str & "A.use_pcs_shipping, A.use_your_shipping,"
        str = str & "ISNULL(A.increament_amount, 0) AS increament_amount,"
        str = str & "ISNULL(A.buy_now_price, 0) AS buy_now_price,"
        str = str & "ISNULL(A.is_buy_it_now, 0) AS is_buy_it_now,"
        str = str & "ISNULL(A.request_for_quote_message, '') AS request_for_quote_message,"
        str = str & "ISNULL(A.show_price, 0) AS show_price,"
        str = str & "ISNULL(A.short_description, '') AS short_description,"
        str = str & "ISNULL(A.total_qty, 0) AS total_qty,"
        str = str & "ISNULL(A.qty_per_bidder, 0) AS qty_per_bidder,"
        str = str & "dbo.get_auction_status(A.auction_id) as auction_status,"
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            str = str & "case when exists(select favourite_id from tbl_auction_favourites where auction_id=A.auction_id and buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id") & ") then 1 else 0 end as in_fav,"
        Else
            str = str & "0 as in_fav,"
        End If
        str = str & "ISNULL(IMG.stock_image_id, 0) AS stock_image_id,ISNULL(IMG.filename, '') AS filename"
        str = str & " FROM "
        str = str & "tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations L WITH (NOLOCK) on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B WITH (NOLOCK) on A.stock_condition_id=B.stock_condition_id "
        str = str & " left join tbl_auction_stock_images IMG WITH (NOLOCK) on A.auction_id=IMG.auction_id and IMG.stock_image_id=(SELECT top 1 stock_image_id from tbl_auction_stock_images WITH (NOLOCK) where auction_id=a.auction_id order by position) "
        str = str & " WHERE "
        str = str & "A.auction_id =" & auction_id

        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDatatable(str)

        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)
                Dim title_url As String = .Item("title").ToString.Replace(" ", "-").Replace("/", "").Replace("'", "").Replace("&", "").Replace("#", "")

                CType(e.Item.FindControl("ltr_title"), Literal).Text = "<a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "' style='text-decoration: none;color:#323232;font-size:" & IIf(.Item("title").ToString.Length < 70, "22px", IIf(.Item("title").ToString.Length < 150, "18px", IIf(.Item("title").ToString.Length < 200, "16px", "14px"))) & ";'>" & .Item("title").ToString & "</a>"



                If .Item("filename").ToString <> "" Then
                    CType(e.Item.FindControl("img_auction_image"), Image).ImageUrl = "/upload/auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString.Replace(".", "_230.")
                    str = "SELECT A.auction_id,convert(varchar,A.auction_id)+'/'+convert(varchar,A.stock_image_id)+'/'+A.filename as path, b.title "
                    str = str & " from tbl_auction_stock_images A  WITH (NOLOCK) inner join tbl_auctions b  WITH (NOLOCK) on a.auction_id=b.auction_id where A.auction_id=" & auction_id
                    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).HRef = "/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t")
                    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).Title = .Item("title")
                Else
                    CType(e.Item.FindControl("img_auction_image"), Image).ImageUrl = "/images/imagenotavailable.gif"
                    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).HRef = "/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t")
                    CType(e.Item.FindControl("a_img_auction"), HtmlAnchor).Title = .Item("title")
                End If



                If .Item("auction_status") = 2 Then 'upcoming
                    CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>MORE INFO</a></div>"
                    CType(e.Item.FindControl("ltr_auction_status"), Literal).Text = "Starts in"
                ElseIf .Item("auction_status") = 3 Then 'over
                    CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>MORE INFO</a></div>"
                    CType(e.Item.FindControl("ltr_auction_status"), Literal).Text = "&nbsp;"
                Else 'running

                    If .Item("auction_type_id") = 1 Then
                        CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnblu'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>BUY NOW</a></div>"
                    ElseIf .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then
                        CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>BID NOW</a></div>"
                    Else
                        CType(e.Item.FindControl("ltr_button_auction"), Literal).Text = "<div class='btn btnorng'><a href='/Auction_Details.aspx?a=" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&t=" & Request.QueryString.Get("t") & "'>OFFER</a></div>"
                    End If

                    CType(e.Item.FindControl("ltr_auction_status"), Literal).Text = "Ends in"
                End If

            End With

        Else
            e.Item.Visible = False
        End If
        dtTable = Nothing
    End Sub
    Protected Sub rpt_auctions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_auctions.ItemDataBound
        setAuction(e, e.Item.DataItem("auction_id"))

    End Sub
End Class
