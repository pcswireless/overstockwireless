﻿
Partial Class UserControls_MyBids
    Inherits System.Web.UI.UserControl
    Public auction_id As Integer
    Public buy_id As Integer
    Public quotation_id As Integer
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ViewState("auction_id") = auction_id

            lit_sub_auction.Text = GetGlobalResourceObject("MyBids", "lit_sub_auction").ToString()
            lit_sub_bid_start.Text = GetGlobalResourceObject("MyBids", "lit_sub_bid_start").ToString()
            lit_sub_buy_now.Text = GetGlobalResourceObject("MyBids", "lit_sub_buy_now").ToString()
            lit_ask_question.Text = GetGlobalResourceObject("MyBids", "lit_ask_question").ToString()
            lit_your_query.Text = GetGlobalResourceObject("MyBids", "lit_your_query").ToString()

            ' img_bid_now.ValidationGroup = "vg_bid" & auction_id

            'RFUserName.ValidationGroup = "vg_login" & auction_id
            'RFPassword.ValidationGroup = "vg_login" & auction_id
            'img_btnsend.ValidationGroup = "vg_login" & auction_id

            'RFOffer1.ValidationGroup = "vg_offer" & auction_id
            'CValidator1.ValidationGroup = "vg_offer" & auction_id
            'RFOffer2.ValidationGroup = "vg_offer" & auction_id
            'CValidator2.ValidationGroup = "vg_offer" & auction_id
            'ImageButtonOffer.ValidationGroup = "vg_offer" & auction_id

            setAuction()
            setAuctiondetails()
        Else
            auction_id = ViewState("auction_id")
        End If
        ' lbl_error.Visible = False
    End Sub

    Private Sub setAuction()

        'If CommonCode.Fetch_Cookie_Shared("user_id") <> "" And (Request.QueryString.Get("t") = "1" Or Request.QueryString.Get("t") = "2") Then
        '    img_hide_now.Visible = True
        '    If Request.QueryString.Get("t") = "1" Then
        '        img_hide_now.ImageUrl = "/images/fend/9.png"
        '    Else
        '        img_hide_now.ImageUrl = "/images/fend/10.png"
        '    End If
        'Else
        '    img_hide_now.Visible = False
        'End If

        Dim str As String = "SELECT A.auction_id,"
        str = str & "ISNULL(A.code, '') AS code,"
        str = str & "ISNULL(A.title, '') AS title,"
        str = str & "ISNULL(A.sub_title, '') AS sub_title,"
        str = str & "ISNULL(A.start_date, '1/1/1900') AS start_date,ISNULL(display_end_time,'1/1/1900') As display_end_time,"
        str = str & "ISNULL(A.product_catetory_id, 0) AS product_catetory_id,"
        str = str & "ISNULL(A.stock_condition_id, 0) AS stock_condition_id,"
        str = str & "ISNULL(A.auction_category_id, 0) AS auction_category_id,"
        str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"
        str = str & "ISNULL(A.is_private_offer, 0) AS is_private_offer,"
        str = str & "ISNULL(A.is_partial_offer, 0) AS is_partial_offer,"
        str = str & "ISNULL(A.auto_acceptance_price_private, 0) AS auto_acceptance_price_private,"
        str = str & "ISNULL(A.auto_acceptance_price_partial, 0) AS auto_acceptance_price_partial,"
        str = str & "ISNULL(A.auto_acceptance_quantity, 0) AS auto_acceptance_quantity,"
        str = str & "ISNULL(A.no_of_clicks, 0) AS no_of_clicks,"
        str = str & "ISNULL(A.start_price, 0) AS start_price,"
        str = str & "ISNULL(A.reserve_price, 0) AS reserve_price,"
        str = str & "ISNULL(A.increament_amount, 0) AS increament_amount,"
        str = str & "ISNULL(A.buy_now_price, 0) AS buy_now_price,"
        str = str & "ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing,"
        str = str & "ISNULL(A.is_buy_it_now, 0) AS is_buy_it_now,"
        str = str & "ISNULL(A.is_accept_pre_bid, 0) AS is_accept_pre_bid,"
        str = str & "ISNULL(A.request_for_quote_message, '') AS request_for_quote_message,"
        str = str & "ISNULL(A.show_price, 0) AS show_price,"
        str = str & "ISNULL(A.code_f, '') AS code_f,"
        str = str & "ISNULL(A.code_s, '') AS code_s,"
        str = str & "ISNULL(A.code_c, '') AS code_c,"
        str = str & "ISNULL(A.title_f, '') AS title_f,"
        str = str & "ISNULL(A.title_s, '') AS title_s,"
        str = str & "ISNULL(A.title_c, '') AS title_c,"
        str = str & "ISNULL(A.sub_title_f, '') AS sub_title_f,"
        str = str & "ISNULL(A.sub_title_s, '') AS sub_title_s,"
        str = str & "ISNULL(A.sub_title_c, '') AS sub_title_c,"
        str = str & "ISNULL(A.short_description, '') AS short_description,"
        str = str & "ISNULL(A.short_description_f, '') AS short_description_f,"
        str = str & "ISNULL(A.short_description_s, '') AS short_description_s,"
        str = str & "ISNULL(A.short_description_c, '') AS short_description_c,"
        str = str & "ISNULL((select count(distinct buyer_id) from tbl_auction_bids WITH (NOLOCK) where auction_id=A.auction_id), 0) AS no_of_bid,"
        str = str & "ISNULL((select max(bid_amount) from tbl_auction_bids WITH (NOLOCK) where auction_id=A.auction_id), 0) AS actual_price,"
        str = str & "isnull((case when exists(select bid_id from tbl_auction_bids WITH (NOLOCK) where auction_id=A.auction_id) then ISNULL((select max(bid_amount) from tbl_auction_bids where auction_id=A.auction_id) + ISNULL(A.increament_amount, 0), 0) else A.start_price end),0) AS bidding_price,"
        str = str & "isnull((select count(*) from tbl_auction_buy WITH (NOLOCK) where buy_type='buy now' and status='Accept' and auction_id=A.auction_id),0) as buy_it_now_bid,"
        If CommonCode.Fetch_Cookie_Shared("buyer_id") = "" Then
            str = str & "0 AS current_rank,"
            str = str & "0 AS bidder_price,"
        Else
            str = str & "dbo.buyer_bid_rank(A.auction_id," & CommonCode.Fetch_Cookie_Shared("buyer_id") & ") AS current_rank,"
            str = str & "isnull((select max(bid_amount) from tbl_auction_bids WITH (NOLOCK) where auction_id=A.auction_id and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " ),0) AS bidder_price,"
            str = str & "isnull((select max(max_bid_amount) from tbl_auction_bids WITH (NOLOCK) where auction_id=A.auction_id and buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " ),0) AS bidder_max_price,"

        End If
        str = str & "dbo.get_auction_status(A.auction_id) as auction_status,"
        str = str & "ISNULL(IMG.stock_image_id, 0) AS stock_image_id,ISNULL(IMG.filename, '') AS filename"
        str = str & " FROM "
        str = str & "tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations L on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B on A.stock_condition_id=B.stock_condition_id "
        str = str & " left join tbl_auction_stock_images IMG on A.auction_id=IMG.auction_id and IMG.position=1 "
        str = str & " WHERE "
        str = str & "A.auction_id =" & auction_id



        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDataTable(str)
        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)
                Select Case CommonCode.Fetch_Cookie_Shared("_lang")
                    Case "es-ES"
                        lbl_auction_code.Text = .Item("code_s")
                        lbl_title.Text = .Item("title_s")
                        lbl_sub_title.Text = .Item("sub_title_s")
                        lbl_short_desc.Text = .Item("short_description_s")

                    Case "fr-FR"
                        lbl_auction_code.Text = .Item("code_f")
                        lbl_title.Text = .Item("title_f")
                        lbl_sub_title.Text = .Item("sub_title_f")
                        lbl_short_desc.Text = .Item("short_description_f")

                    Case "zh-CN"
                        lbl_auction_code.Text = .Item("code_c")
                        lbl_title.Text = .Item("title_c")
                        lbl_sub_title.Text = .Item("sub_title_c")
                        lbl_short_desc.Text = .Item("short_description_c")

                    Case Else
                        lbl_auction_code.Text = .Item("code")
                        lbl_title.Text = .Item("title")
                        lbl_sub_title.Text = .Item("sub_title")
                        lbl_short_desc.Text = .Item("short_description")

                End Select


                If .Item("filename").ToString <> "" Then
                    img_image.ImageUrl = "/upload/auctions/stock_images/" & auction_id & "/" & .Item("stock_image_id").ToString & "/" & .Item("filename").ToString.Replace(".", "_thumb1.")
                Else
                    img_image.ImageUrl = "/images/imagenotavailable.gif"
                End If


                'panel setting
                If .Item("auction_type_id") = 1 Then
                    pnl_proxy.Visible = False
                    pnl_quote.Visible = False
                    pnl_now.Visible = True
                    'lit_trad_proxy_buy_now_price.Text = "Buy it now for $" & FormatNumber(.Item("show_price"))
                    lit_buy_now_price.Text = "$" & FormatNumber(.Item("show_price"), 2)
                    ' lit_amount_caption.Text = lit_sub_buy_now.Text & " : $" & FormatNumber(.Item("show_price"), 2)
                    'lit_amount_caption.Visible = True
                    Dim print_order_confirmation As String = ""
                    print_order_confirmation = GetGlobalResourceObject("AuctionList", "lit_print_order_confirmation")
                    lbl_print_confirmation.Visible = True
                    lbl_print_confirmation.Text = "<a href=""javascript:void(0);"" onclick=""return open_pop_order_confirmation('/AuctionConfirmation.aspx','" & buy_id & "','b');"">" & print_order_confirmation & "</a>"
                ElseIf .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then

                    pnl_now.Visible = False
                    pnl_proxy.Visible = True
                    pnl_quote.Visible = False
                    lit_start_price.Text = "$" & FormatNumber(.Item("start_price"), 2)
                    lit_increment.Text = "+$" & FormatNumber(.Item("increament_amount"), 2)
                    If .Item("auction_type_id") = 3 Then
                        ltrl_max_bid_amount.Text = "$" & FormatNumber(.Item("bidder_max_price"), 2)
                    End If

                    'lit_rank.Text = GetGlobalResourceObject("AuctionList", "lit_rank").ToString & " : " & IIf(.Item("current_rank") = 0, "--", .Item("current_rank"))
                    'lit_trad_proxy_buy_now_price.Text = "Buy it now for $" & FormatNumber(.Item("buy_now_price"))
                    If .Item("bidder_price") <> 0 Then
                        lit_biddeer_price.Text = GetGlobalResourceObject("AuctionList", "lit_bidder_price").ToString & " $" & FormatNumber(.Item("bidder_price"), 2)
                    Else
                        lit_biddeer_price.Visible = False
                    End If


                    If Convert.ToBoolean(.Item("is_show_actual_pricing")) Then
                        lit_price.Text = GetGlobalResourceObject("AuctionList", "lit_price").ToString & " : $" & FormatNumber(.Item("actual_price"), 2)
                    Else
                        lit_price.Visible = False
                    End If

                    lbl_bidder_history.Visible = True
                    Dim view_history As String = GetGlobalResourceObject("AuctionList", "lit_sub_history").ToString
                    lbl_bidder_history.Text = "<a href=""javascript:void(0);"" onclick=""return open_pop_win('/Bid_History.aspx','" & Security.EncryptionDecryption.EncryptValueFormatted(auction_id) & "&pop=1');"">" & view_history & "</a>"

                Else
                    pnl_quote.Visible = True
                    lit_quote_msg.Text = .Item("request_for_quote_message")
                    pnl_now.Visible = False
                    pnl_proxy.Visible = False
                End If
                If Not String.IsNullOrEmpty(Request.QueryString("mode")) Then
                    If Request.QueryString("mode") = "pr" Or Request.QueryString("mode") = "pa" Or Request.QueryString("mode") = "bu" Then
                        pnl_trad_proxy.Visible = False
                        pnl_buyer_quote.Visible = False
                        Dim strBuy As String = "select bid_date,price,isnull(grand_total_amount,0) as grand_total_amount,quantity,case ISNULL(status,'') when 'Accept' then 'Accepted' when 'Pending' then 'Pending Decision' when 'Reject' then 'Rejected' end AS status from tbl_auction_buy WITH (NOLOCK) where buy_id=" & buy_id
                        Dim dtBuy As DataTable = New DataTable()
                        dtBuy = SqlHelper.ExecuteDatatable(strBuy)
                        If dtBuy.Rows.Count > 0 Then
                            lit_bid_date_buy.Text = "<span style='font-weight:bold;'>Bid Date : </span>" & Convert.ToDateTime(dtBuy.Rows(0)("bid_date")).ToShortDateString()
                            lit_buy_price.Text = "<span style='font-weight:bold'>Price : </span>" & "$" & FormatNumber(dtBuy.Rows(0)("grand_total_amount"), 2)
                            lit_approval_status.Text = "<span style='font-weight:bold;'>Status : </span>" & dtBuy.Rows(0)("status")
                            If Request.QueryString("mode") = "pa" Then
                                lit_buy_qty.Text = "<span style='font-weight:bold;'>Qty : </span>" & dtBuy.Rows(0)("quantity")
                            End If
                            Dim print_order_confirmation As String = ""
                            print_order_confirmation = GetGlobalResourceObject("AuctionList", "lit_print_order_confirmation")
                            lbl_print_confirmation.Visible = True
                            lbl_print_confirmation.Text = "<a href=""javascript:void(0);"" onclick=""return open_pop_order_confirmation('/AuctionConfirmation.aspx','" & buy_id & "','b');"">" & print_order_confirmation & "</a>"
                        End If
                        dtBuy = Nothing
                    ElseIf Request.QueryString("mode") = "q" Then
                        pnl_trad_proxy.Visible = False
                        pnl_private_partial.Visible = False

                        Dim strQuote As String = "select bid_date,details,ISNULL(filename,'') As filename, ISNULL(action,'') As status from tbl_auction_quotations where quotation_id=" & quotation_id
                        Dim dtQuote As DataTable = New DataTable()
                        dtQuote = SqlHelper.ExecuteDataTable(strQuote)
                        If dtQuote.Rows.Count > 0 Then
                            lit_bid_date_quote.Text = "<span style='font-weight:bold;'>Bid Date : </span>" & Convert.ToDateTime(dtQuote.Rows(0)("bid_date")).ToShortDateString()
                            lit_buyer_quote_msg.Text = dtQuote.Rows(0)("details")
                            If dtQuote.Rows(0)("status") <> "" Then
                                lit_quote_approval_status.Text = "<span style='font-weight:bold;'>Status : </span>" & IIf(dtQuote.Rows(0)("status") = "" Or dtQuote.Rows(0)("status").ToString().ToUpper() = "PENDING", "Pending Decision", dtQuote.Rows(0)("status"))
                            End If

                            If dtQuote.Rows(0)("filename") <> "" Then
                                lit_quote_attach_file.Text = "<a href='/Upload/Quotation/" & auction_id & "/" & dtQuote.Rows(0)("filename") & "' target='_blank'>" & dtQuote.Rows(0)("filename") & "</a>"
                            End If
                        End If
                        dtQuote = Nothing
                    ElseIf Request.QueryString("mode") = "w" Then
                        pnl_private_partial.Visible = False
                        pnl_buyer_quote.Visible = False
                        Dim print_order_receipt As String = ""
                        print_order_receipt = GetGlobalResourceObject("AuctionList", "lit_print_order_receipt")
                        lbl_print_confirmation.Visible = True
                        lbl_print_confirmation.Text = "<a href=""javascript:void(0);"" onclick=""return open_pop_order_confirmation('/AuctionConfirmation.aspx','" & buy_id & "','a');"">" & print_order_receipt & "</a>"
                    End If
                End If
                Dim start_date As DateTime = .Item("start_date")
                Dim cur_date As DateTime = DateTime.Now()
                Dim display_end_time As DateTime = .Item("display_end_time")

                Dim span As TimeSpan = display_end_time.Subtract(cur_date)
                Dim time_left As String = ""
                Select Case .Item("auction_status")
                    Case 1
                        ltrl_status.Text = "<span style='font-size:12px;color:#339933;'>Running</span><br /><span>Closing on : " & display_end_time.ToShortDateString() & "</span>"
                    Case 2
                        ltrl_status.Text = "<span style='color:#FF7F00;'>Upcoming</span>"
                        span = start_date.Subtract(cur_date)
                        time_left = span.Days & "D " & span.Hours & "H " & span.Minutes & "M" & " left to start"
                    Case 3
                        ltrl_status.Text = "<span style='color:#FF2A2A; font-size:12px;'>Closed</span><br /><span>Closed on : " & display_end_time.ToShortDateString() & "</span>"
                End Select

                'bid button setting
                'If .Item("auction_status") = 1 Or (.Item("auction_status") = 2 And (.Item("auction_type_id") = 3 Or .Item("auction_type_id") = 2) And Convert.ToBoolean(.Item("is_accept_pre_bid"))) Then

                '    'img_bid_now.Visible = True
                '    If .Item("auction_type_id") = 1 Then
                '        ' img_bid_now.AlternateText = "Buy Now"
                '        If .Item("buy_it_now_bid") <= .Item("no_of_clicks") Then

                '            If Convert.ToBoolean(.Item("is_private_offer")) Then
                '                btn_send_private_offer.Visible = True
                '            End If
                '            If Convert.ToBoolean(.Item("is_partial_offer")) Then
                '                btn_send_partial_offer.Visible = True
                '            End If
                '        End If

                '    ElseIf .Item("auction_type_id") = 2 Or .Item("auction_type_id") = 3 Then
                '        'img_bid_now.ImageUrl = "/images/fend/6.png"

                '        If .Item("auction_type_id") = 2 Then
                '            'img_bid_now.AlternateText = "Bid Now"
                '            'lit_amount_caption.Text = "Bid now for $" & FormatNumber(.Item("bidding_price"), 2)
                '            'lit_trad_proxy_buy_now_price.Text = "Bid it now for price $" & FormatNumber(.Item("bidding_price"))
                '        Else
                '            ' img_bid_now.AlternateText = "Bid For"
                '            'lit_amount_caption.Text = "Put maximum bid amount of $" & FormatNumber(.Item("bidding_price"), 2) & " or more"
                '        End If

                '        If .Item("auction_type_id") = 3 Then
                '            'txt_bid_now.Visible = True
                '            ' txt_bid_now.Text = FormatNumber(.Item("bidding_price"), 2)
                '            'validPrice.ValidationGroup = "vg_bid" & auction_id
                '            'comvRG.ValidationGroup = "vg_bid" & auction_id
                '            'comvRG.ValueToCompare = CDbl(.Item("bidding_price").ToString)
                '            'comvRG.ErrorMessage = "Amount should be $" & FormatNumber(.Item("bidding_price"), 2) & " or more"
                '        End If

                '        'If Convert.ToBoolean(.Item("is_private_offer")) Then
                '        '    btn_send_private_offer.Visible = True
                '        'End If
                '        'If Convert.ToBoolean(.Item("is_partial_offer")) Then
                '        '    btn_send_partial_offer.Visible = True
                '        'End If
                '        'If Convert.ToBoolean(.Item("is_buy_it_now")) And .Item("buy_it_now_bid") <= .Item("no_of_clicks") Then
                '        '    btn_buy_now.Visible = True
                '        'End If

                '    Else

                '        'dv_amount_cap.Visible = False
                '        'img_bid_now.ImageUrl = "/images/fend/8.png"
                '        'img_bid_now.AlternateText = "Quotation"

                '    End If
                'End If

                'iframe_price.Attributes.Add("src", "/timerframe.aspx?i=" & auction_id)

            End With
        Else
            Me.Visible = False
        End If
        dtTable = Nothing

    End Sub

    Private Sub setAuctiondetails()


        Grid_Items.DataSource = SqlHelper.ExecuteDataTable("select [product_item_id], isnull(m.[description],'') AS manufacturer, case '" & Application("_lang") & "' when 'fr-FR' then  [part_no_f] when 'es-ES' then  [part_no_s] when 'zh-CN' then  [part_no_c] else [part_no] End AS part_no, case '" & Application("_lang") & "' when 'fr-FR' then p.[description_f] when 'es-ES' then p.[description_s] when 'zh-CN' then p.[description_c] else p.[description] End AS description,isnull(quantity,0) as quantity  from [tbl_auction_product_items] p left join tbl_master_manufacturers m on p.manufacturer_id=m.manufacturer_id where auction_id= " & auction_id)
        Grid_Items.DataBind()


        Dim str As String = "SELECT A.auction_id,"
        str = str & "ISNULL(A.tax_details, '') AS tax_details,"
        str = str & "ISNULL(A.product_catetory_id, 0) AS product_catetory_id,"
        str = str & "ISNULL(A.stock_condition_id, 0) AS stock_condition_id,"
        str = str & "ISNULL(A.packaging_id, 0) AS packaging_id,"
        str = str & "ISNULL(L.name, '') AS stock_location,"
        str = str & "ISNULL(A.description, '') AS description,"
        str = str & "ISNULL(A.special_conditions, '') AS special_conditions,"
        str = str & "ISNULL(A.payment_terms, '') AS payment_terms,"
        str = str & "ISNULL(A.product_details, '') AS product_details,"
        str = str & "ISNULL(A.shipping_terms, '') AS shipping_terms,"
        str = str & "ISNULL(A.other_terms, '') AS other_terms,"
        str = str & "ISNULL(A.auction_category_id, 0) AS auction_category_id,"
        str = str & "ISNULL(A.auction_type_id, 0) AS auction_type_id,"
        str = str & "ISNULL(A.request_for_quote_message, '') AS request_for_quote_message,"
        str = str & "ISNULL(A.after_launch_message, '') AS after_launch_message,"
        str = str & "ISNULL(A.seller_id, 0) AS seller_id,"
        str = str & "ISNULL((select top 1 (convert(varchar(10),tax_attachment_id)+'#'+case '" & CommonCode.Fetch_Cookie_Shared("_lang") & "' when 'fr-FR' then filename_f when 'es-ES' then filename_s when 'zh-CN' then filename_c else filename end) from tbl_auction_tax_attachments where auction_id=" & auction_id & " order by tax_attachment_id desc), '') AS tax_details_attach,"
        str = str & "ISNULL(A.tax_details_f, '') AS tax_details_f,"
        str = str & "ISNULL(A.tax_details_s, '') AS tax_details_s,"
        str = str & "ISNULL(A.tax_details_c, '') AS tax_details_c,"
        str = str & "ISNULL(A.description_f, '') AS description_f,"
        str = str & "ISNULL(A.description_s, '') AS description_s,"
        str = str & "ISNULL(A.description_c, '') AS description_c,"
        str = str & "ISNULL((select top 1 (convert(varchar(10),special_condition_id)+'#'+case '" & CommonCode.Fetch_Cookie_Shared("_lang") & "' when 'fr-FR' then filename_f when 'es-ES' then filename_s when 'zh-CN' then filename_c else filename end) from tbl_auction_special_conditions where auction_id=" & auction_id & " order by special_condition_id desc), '') AS special_terms_attach,"
        str = str & "ISNULL(A.special_conditions_f, '') AS special_conditions_f,"
        str = str & "ISNULL(A.special_conditions_s, '') AS special_conditions_s,"
        str = str & "ISNULL(A.special_conditions_c, '') AS special_conditions_c,"
        str = str & "ISNULL((select top 1 (convert(varchar(10),payment_term_attachment_id)+'#'+case '" & CommonCode.Fetch_Cookie_Shared("_lang") & "' when 'fr-FR' then filename_f when 'es-ES' then filename_s when 'zh-CN' then filename_c else filename end) from tbl_auction_payment_term_attachments where auction_id=" & auction_id & " order by payment_term_attachment_id desc), '') AS payment_terms_attach,"
        str = str & "ISNULL(A.payment_terms_f, '') AS payment_terms_f,"
        str = str & "ISNULL(A.payment_terms_s, '') AS payment_terms_s,"
        str = str & "ISNULL(A.payment_terms_c, '') AS payment_terms_c,"
        str = str & "ISNULL((select top 1 (convert(varchar(10),product_attachment_id)+'#'+case '" & CommonCode.Fetch_Cookie_Shared("_lang") & "' when 'fr-FR' then filename_f when 'es-ES' then filename_s when 'zh-CN' then filename_c else filename end) from tbl_auction_product_attachments where auction_id=" & auction_id & " order by product_attachment_id desc), '') AS product_details_attach,"
        str = str & "ISNULL(A.product_details_f, '') AS product_details_f,"
        str = str & "ISNULL(A.product_details_s, '') AS product_details_s,"
        str = str & "ISNULL(A.product_details_c, '') AS product_details_c,"
        str = str & "ISNULL((select top 1 (convert(varchar(10),shipping_term_attachment_id)+'#'+case '" & CommonCode.Fetch_Cookie_Shared("_lang") & "' when 'fr-FR' then filename_f when 'es-ES' then filename_s when 'zh-CN' then filename_c else filename end) from tbl_auction_shipping_term_attachments where auction_id=" & auction_id & " order by shipping_term_attachment_id desc), '') AS shipping_terms_attach,"
        str = str & "ISNULL(A.shipping_terms_f, '') AS shipping_terms_f,"
        str = str & "ISNULL(A.shipping_terms_s, '') AS shipping_terms_s,"
        str = str & "ISNULL(A.shipping_terms_c, '') AS shipping_terms_c,"
        str = str & "ISNULL((select top 1 (convert(varchar(10),term_cond_attachment_id)+'#'+case '" & CommonCode.Fetch_Cookie_Shared("_lang") & "' when 'fr-FR' then filename_f when 'es-ES' then filename_s when 'zh-CN' then filename_c else filename end) from tbl_auction_terms_cond_attachments where auction_id=" & auction_id & " order by term_cond_attachment_id desc), '') AS other_terms_attach,"
        str = str & "ISNULL(A.other_terms_f, '') AS other_terms_f,"
        str = str & "ISNULL(A.other_terms_s, '') AS other_terms_s,"
        str = str & "ISNULL(A.other_terms_c, '') AS other_terms_c,"
        str = str & "ISNULL(B.name, '') AS stock_condition,"
        str = str & "ISNULL(L.name, '') AS stock_location,"
        str = str & "ISNULL(C.name, '') AS packaging,"
        str = str & "ISNULL(D.company_name, '') AS seller"
        str = str & " FROM "
        str = str & "tbl_auctions A WITH (NOLOCK) left join tbl_master_stock_locations L on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B on A.stock_condition_id=B.stock_condition_id "
        str = str & "Left Join tbl_master_packaging C on A.packaging_id=C.packaging_id left join tbl_reg_sellers D on A.seller_id=D.seller_id"
        str = str & " WHERE "
        str = str & "A.auction_id =" & auction_id


        img_btn_create_thread.CommandArgument = auction_id

        load_auction_bidder_queries()

        Dim dtTable As New DataTable()
        dtTable = SqlHelper.ExecuteDataTable(str)
        If dtTable.Rows.Count > 0 Then
            With dtTable.Rows(0)
                Select Case CommonCode.Fetch_Cookie_Shared("_lang")
                    Case "es-ES"
                        lbl_auction_description.Text = .Item("description_s")
                        lbl_auction_payment.Text = .Item("payment_terms_s")
                        lbl_auction_shipping.Text = .Item("shipping_terms_s")
                        lbl_auction_other.Text = .Item("other_terms_s")
                        lbl_auction_special.Text = .Item("special_conditions_s")
                        lbl_auction_tax.Text = .Item("tax_details_s")
                        lbl_auction_prodetail.Text = .Item("product_details_s")
                    Case "fr-FR"
                        lbl_auction_description.Text = .Item("description_f")
                        lbl_auction_payment.Text = .Item("payment_terms_f")
                        lbl_auction_shipping.Text = .Item("shipping_terms_f")
                        lbl_auction_other.Text = .Item("other_terms_f")
                        lbl_auction_special.Text = .Item("special_conditions_f")
                        lbl_auction_tax.Text = .Item("tax_details_f")
                        lbl_auction_prodetail.Text = .Item("product_details_f")
                    Case "zh-CN"
                        lbl_auction_description.Text = .Item("description_c")
                        lbl_auction_payment.Text = .Item("payment_terms_c")
                        lbl_auction_shipping.Text = .Item("shipping_terms_c")
                        lbl_auction_other.Text = .Item("other_terms_c")
                        lbl_auction_special.Text = .Item("special_conditions_c")
                        lbl_auction_tax.Text = .Item("tax_details_c")
                        lbl_auction_prodetail.Text = .Item("product_details_c")
                    Case Else
                        lbl_auction_description.Text = .Item("description")
                        lbl_auction_payment.Text = .Item("payment_terms")
                        lbl_auction_shipping.Text = .Item("shipping_terms")
                        lbl_auction_other.Text = .Item("other_terms")
                        lbl_auction_special.Text = .Item("special_conditions")
                        lbl_auction_tax.Text = .Item("tax_details")
                        lbl_auction_prodetail.Text = .Item("product_details")
                End Select

                lbl_auction_afterlaunch.Text = .Item("after_launch_message")

                If lbl_auction_payment.Text.Trim <> "" Then
                    lbl_auction_payment.Text = "<div style='padding-bottom:10px;'><b>Payment Terms</b></div>" & lbl_auction_payment.Text
                    If .Item("payment_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("payment_terms_attach").ToString.Trim.Remove(0, .Item("payment_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                        lbl_auction_payment.Text = lbl_auction_payment.Text & "<br><a target='_blank' href='/Upload/Auctions/payment_term_attachments/" & auction_id & "/" & .Item("payment_terms_attach").ToString.Remove(.Item("payment_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("payment_terms_attach").ToString.Remove(0, .Item("payment_terms_attach").ToString.LastIndexOf("#") + 1) & "'>View attached payment terms.</a>"
                    End If
                Else
                    If .Item("payment_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("payment_terms_attach").ToString.Trim.Remove(0, .Item("payment_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                        lbl_auction_payment.Text = "<div style='padding-bottom:10px;'><b>Payment Terms</b></div><a target='_blank' href='/Upload/Auctions/payment_term_attachments/" & auction_id & "/" & .Item("payment_terms_attach").ToString.Remove(.Item("payment_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("payment_terms_attach").ToString.Remove(0, .Item("payment_terms_attach").ToString.LastIndexOf("#") + 1) & "'>View attached payment terms.</a>"
                    End If
                End If
                If lbl_auction_shipping.Text.Trim <> "" Then
                    lbl_auction_shipping.Text = "<div style='padding-bottom:10px;'><b>Shipping Terms</b></div>" & lbl_auction_shipping.Text
                    If .Item("shipping_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("shipping_terms_attach").ToString.Trim.Remove(0, .Item("shipping_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                        lbl_auction_shipping.Text = lbl_auction_shipping.Text & "<br><a target='_blank' href='/Upload/Auctions/shipping_term_attachments/" & auction_id & "/" & .Item("shipping_terms_attach").ToString.Remove(.Item("shipping_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("shipping_terms_attach").ToString.Remove(0, .Item("shipping_terms_attach").ToString.LastIndexOf("#") + 1) & "'>View attached shipping terms.</a>"
                    End If
                Else
                    If .Item("shipping_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("shipping_terms_attach").ToString.Trim.Remove(0, .Item("shipping_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                        lbl_auction_shipping.Text = "<div style='padding-bottom:10px;'><b>Shipping Terms</b></div><a target='_blank' href='/Upload/Auctions/shipping_term_attachments/" & auction_id & "/" & .Item("shipping_terms_attach").ToString.Remove(.Item("shipping_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("shipping_terms_attach").ToString.Remove(0, .Item("shipping_terms_attach").ToString.LastIndexOf("#") + 1) & "'>View attached shipping terms.</a>"
                    End If
                End If
                If lbl_auction_other.Text.Trim <> "" Then
                    lbl_auction_other.Text = "<div style='padding-bottom:10px;'><b>Other Terms</b></div>" & lbl_auction_other.Text
                    If .Item("other_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("other_terms_attach").ToString.Trim.Remove(0, .Item("other_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                        lbl_auction_other.Text = lbl_auction_other.Text & "<br><a target='_blank' href='/Upload/Auctions/terms_cond_attachments/" & auction_id & "/" & .Item("other_terms_attach").ToString.Remove(.Item("other_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("other_terms_attach").ToString.Remove(0, .Item("other_terms_attach").ToString.LastIndexOf("#") + 1) & "'>View attached other terms.</a>"
                    End If
                Else
                    If .Item("other_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("other_terms_attach").ToString.Trim.Remove(0, .Item("other_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                        lbl_auction_other.Text = "<div style='padding-bottom:10px;'><b>Other Terms</b></div><a target='_blank' href='/Upload/Auctions/terms_cond_attachments/" & auction_id & "/" & .Item("other_terms_attach").ToString.Remove(.Item("other_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("other_terms_attach").ToString.Remove(0, .Item("other_terms_attach").ToString.LastIndexOf("#") + 1) & "'>View attached other terms.</a>"
                    End If
                End If
                If lbl_auction_special.Text.Trim <> "" Then
                    lbl_auction_special.Text = "<div style='padding-bottom:10px;'><b>Special Terms</b></div>" & lbl_auction_special.Text
                    If .Item("special_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("special_terms_attach").ToString.Trim.Remove(0, .Item("special_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                        lbl_auction_special.Text = lbl_auction_special.Text & "<br><a target='_blank' href='/Upload/Auctions/special_conditions/" & auction_id & "/" & .Item("special_terms_attach").ToString.Remove(.Item("special_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("special_terms_attach").ToString.Remove(0, .Item("special_terms_attach").ToString.LastIndexOf("#") + 1) & "'>View attached special terms.</a>"
                    End If
                Else
                    If .Item("special_terms_attach").ToString.Trim.Length > 1 AndAlso (.Item("special_terms_attach").ToString.Trim.Remove(0, .Item("special_terms_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                        lbl_auction_special.Text = "<div style='padding-bottom:10px;'><b>Special Terms</b></div><a target='_blank' href='/Upload/Auctions/special_conditions/" & auction_id & "/" & .Item("special_terms_attach").ToString.Remove(.Item("special_terms_attach").ToString.LastIndexOf("#")) & "/" & .Item("special_terms_attach").ToString.Remove(0, .Item("special_terms_attach").ToString.LastIndexOf("#") + 1) & "'>View attached special terms.</a>"
                    End If
                End If
                If lbl_auction_tax.Text.Trim <> "" Then
                    lbl_auction_tax.Text = "<div style='padding-bottom:10px;'><b>Tax Details</b></div>" & lbl_auction_tax.Text
                    If .Item("tax_details_attach").ToString.Trim.Length > 1 AndAlso (.Item("tax_details_attach").ToString.Trim.Remove(0, .Item("tax_details_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                        lbl_auction_tax.Text = lbl_auction_tax.Text & "<br><a target='_blank' href='/Upload/Auctions/tax_attachments/" & auction_id & "/" & .Item("tax_details_attach").ToString.Remove(.Item("tax_details_attach").ToString.LastIndexOf("#")) & "/" & .Item("tax_details_attach").ToString.Remove(0, .Item("tax_details_attach").ToString.LastIndexOf("#") + 1) & "'>View attached tax details.</a>"
                    End If
                Else
                    If .Item("tax_details_attach").ToString.Trim.Length > 1 AndAlso (.Item("tax_details_attach").ToString.Trim.Remove(0, .Item("tax_details_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                        lbl_auction_tax.Text = "<div style='padding-bottom:10px;'><b>Tax Details</b></div><a target='_blank' href='/Upload/Auctions/tax_attachments/" & auction_id & "/" & .Item("tax_details_attach").ToString.Remove(.Item("tax_details_attach").ToString.LastIndexOf("#")) & "/" & .Item("tax_details_attach").ToString.Remove(0, .Item("tax_details_attach").ToString.LastIndexOf("#") + 1) & "'>View attached tax details.</a>"
                    End If
                End If
                If lbl_auction_prodetail.Text.Trim <> "" Then
                    lbl_auction_prodetail.Text = "<div style='padding-bottom:10px;'><b>Product Details</b></div>" & lbl_auction_prodetail.Text
                    If .Item("product_details_attach").ToString.Trim.Length > 1 AndAlso (.Item("product_details_attach").ToString.Trim.Remove(0, .Item("product_details_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                        lbl_auction_prodetail.Text = lbl_auction_prodetail.Text & "<br><a target='_blank' href='/Upload/Auctions/product_attachments/" & auction_id & "/" & .Item("product_details_attach").ToString.Remove(.Item("product_details_attach").ToString.LastIndexOf("#")) & "/" & .Item("product_details_attach").ToString.Remove(0, .Item("product_details_attach").ToString.LastIndexOf("#") + 1) & "'>View attached product details.</a>"
                    End If
                Else
                    If .Item("product_details_attach").ToString.Trim.Length > 1 AndAlso (.Item("product_details_attach").ToString.Trim.Remove(0, .Item("product_details_attach").ToString.Trim.LastIndexOf("#") + 1)).Length > 1 Then
                        lbl_auction_prodetail.Text = "<div style='padding-bottom:10px;'><b>Product Details</b></div><a target='_blank' href='/Upload/Auctions/product_attachments/" & auction_id & "/" & .Item("product_details_attach").ToString.Remove(.Item("product_details_attach").ToString.LastIndexOf("#")) & "/" & .Item("product_details_attach").ToString.Remove(0, .Item("product_details_attach").ToString.LastIndexOf("#") + 1) & "'>View attached product details.</a>"
                    End If
                End If

                Dim tbl_build As New StringBuilder
                tbl_build.Append("<table><tr><td style='width: 100px;'>" & IIf(.Item("stock_condition").ToString.Trim <> "", "<b>Stock Condition</b></td><td width='200px'>" & .Item("stock_condition").ToString & "</td>", "</td>"))
                tbl_build.Append("<td style='width: 100px;'>" & IIf(.Item("stock_location").ToString.Trim <> "", "<b>Stock Location</b></td><td width='200px'>" & .Item("stock_location").ToString & "</td></tr>", "</td></tr>"))
                tbl_build.Append("<tr><td>" & IIf(.Item("packaging").ToString.Trim <> "", "<b>Packaging</b></td><td>" & .Item("packaging").ToString & "</td>", "</td>"))
                tbl_build.Append("<td>" & IIf(.Item("seller").ToString.Trim <> "", "<b>Company</b></td><td>" & .Item("seller").ToString & "</td></tr></table>", "</td></tr></table>"))
                lit_auction_summary.Text = tbl_build.ToString

                lit_auction_summary.Visible = IIf(.Item("stock_condition").ToString.Trim <> "" Or .Item("stock_location").ToString.Trim <> "" Or .Item("packaging").ToString.Trim <> "" Or .Item("seller").ToString.Trim <> "", True, False)

                TabStip1.Tabs(0).Visible = IIf(lbl_auction_afterlaunch.Text.Trim <> "" Or lit_auction_summary.Visible, True, False)
                TabStip1.Tabs(1).Visible = IIf(Grid_Items.Items.Count > 0, True, False)
                TabStip1.Tabs(2).Visible = IIf(lbl_auction_description.Text.Trim <> "" Or lbl_auction_prodetail.Text.Trim <> "", True, False)
                TabStip1.Tabs(3).Visible = IIf(lbl_auction_payment.Text.Trim <> "" Or lbl_auction_shipping.Text.Trim <> "" Or lbl_auction_other.Text.Trim <> "" Or lbl_auction_special.Text.Trim <> "", True, False)

                For i As Int16 = 0 To TabStip1.Tabs.Count - 1
                    If TabStip1.Tabs(i).Visible Then
                        Multipage1.SelectedIndex = i
                        Exit For
                    End If
                Next

            End With
        Else
            Me.Visible = False
        End If
        dtTable = Nothing

    End Sub

    'Protected Sub img_hide_now_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_hide_now.Click
    '    If Request.QueryString.Get("t") = "1" Then
    '        SqlHelper.ExecuteNonQuery("insert into tbl_auction_hidden(auction_id,buyer_user_id) values ('" & auction_id & "','" & CommonCode.Fetch_Cookie_Shared("user_id") & "')")
    '    Else
    '        SqlHelper.ExecuteNonQuery("delete from tbl_auction_hidden where auction_id=" & auction_id & " and buyer_user_id=" & CommonCode.Fetch_Cookie_Shared("user_id"))
    '        Page.ClientScript.RegisterStartupScript(Page.GetType(), "_call_set", "redirectPage('','/SiteTopBar.aspx?t=2','Auctions.aspx?t=2');", True)
    '    End If

    '    Me.Visible = False
    'End Sub

    'Protected Sub img_btnsend_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_btnsend.Click
    '    If txt_login_name.Text.Trim() <> "" And txt_login_pwd.Text.Trim() <> "" Then
    '        Dim str As String = "select * from vw_login_users where username='" & txt_login_name.Text.Trim() & "' and password='" & Security.EncryptionDecryption.EncryptValue(txt_login_pwd.Text.Trim()) & "'"
    '        Dim dt As New DataTable
    '        dt = SqlHelper.ExecuteDataTable(str)

    '        If dt.Rows.Count > 0 Then

    '            If dt.Rows(0)("is_backend").ToString = "1" Then
    '                lbl_error.Visible = True
    '                modalPopUpExtender1.Show()

    '            ElseIf dt.Rows(0)("is_backend").ToString = "0" Then
    '                CommonCode.Create_Cookie_shared("user_id", dt.Rows(0)("user_id").ToString)
    '                CommonCode.Create_Cookie_shared("username", dt.Rows(0)("username").ToString)
    '                CommonCode.Create_Cookie_shared("buyer_id", dt.Rows(0)("buyer_id").ToString)
    '                CommonCode.Create_Cookie_shared("is_backend", dt.Rows(0)("is_backend").ToString)
    '                CommonCode.Create_Cookie_shared("is_buyer", dt.Rows(0)("is_buyer").ToString)
    '                str = "insert into tbl_sec_login_log (user_id,buyer_id,buyer_user_id,login_date,ip_address,browser_info,log_type) values (" & dt.Rows(0)("user_id") & "," & dt.Rows(0)("buyer_id").ToString & "," & dt.Rows(0)("user_id").ToString & ",getdate(),'" & HttpContext.Current.Request.UserHostAddress & "','" & Request.Browser.Browser.ToString & " - " & Request.Browser.Version.ToString & " - " & Request.Browser.Platform.ToString & "','Login')"
    '                SqlHelper.ExecuteNonQuery(str)
    '                modalPopUpExtender1.Hide()
    '                bid_now()

    '            End If

    '        Else
    '            lbl_error.Visible = True
    '            modalPopUpExtender1.Show()
    '        End If
    '    End If
    'End Sub

    'Protected Sub img_bid_now_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles img_bid_now.Click
    '    If CommonCode.Fetch_Cookie_Shared("user_id") = "" Then

    '        modalPopUpExtender1.Show()
    '    Else
    '        bid_now()
    '    End If
    'End Sub

    'Protected Sub bid_now()
    '    ViewState("option") = 0
    '    lbl_login_auction.Text = lbl_title.Text
    '    lbl_quote_auction.Text = lbl_title.Text
    '    lbl_popmsg_auction.Text = lbl_title.Text

    '    If img_bid_now.AlternateText.ToLower.Contains("quotation") Then
    '        BidNow(CommonCode.Fetch_Cookie_Shared("user_id"), CommonCode.Fetch_Cookie_Shared("buyer_id"))
    '    Else
    '        offer_bid_buy_click(img_bid_now.AlternateText)
    '    End If



    'End Sub
    'Private Sub BidNow(ByVal user_id As Integer, ByVal buyer_id As Integer)

    '    Dim str As String = "SELECT A.auction_id,ISNULL(A.is_accept_pre_bid, 0) AS is_accept_pre_bid, dbo.get_auction_status(A.auction_id) as auction_status,isNULL(buy_now_price,0) AS buy_now_price,isnull(show_price,0) as show_price,isnull(case when rank1_bid_id >0 then (select bid_amount from tbl_auction_bids where bid_id=rank1_bid_id)+(increament_amount) else start_price end,0) as tradition_amount,isnull((select bidding_limit from tbl_reg_buyer_users where buyer_user_id='" & user_id & "'),0) AS bidding_limit"
    '    str = str & " FROM tbl_auctions A WHERE A.auction_id =" & auction_id

    '    Dim dtTable As New DataTable()
    '    dtTable = SqlHelper.ExecuteDataTable(str)
    '    If dtTable.Rows.Count > 0 Then
    '        With dtTable.Rows(0)

    '            If .Item("auction_status") = 1 Or (.Item("auction_status") = 2 And Convert.ToBoolean(.Item("is_accept_pre_bid"))) Then
    '                Dim obj As New Bid()
    '                Dim buy_type As String = "Manual"
    '                Dim price As Double = 0
    '                Dim typr As String = ""
    '                If ViewState("option") = 0 Then 'if main button clicked

    '                    Select Case ViewState("auction_type_id")
    '                        Case 1

    '                            If CommonCode.Fetch_Cookie_Shared("is_buyer") = "1" Then
    '                                obj.save_buy_now_bid(auction_id, 0, 0, "buy now", user_id, buyer_id)
    '                                lbl_popmsg_msg.Text = "Thanks for submitting your order. We will get back to you within 2 business day."

    '                            ElseIf CDbl(.Item("show_price")) <= CDbl(.Item("bidding_limit").ToString) Then
    '                                obj.save_buy_now_bid(auction_id, 0, 0, "buy now", user_id, buyer_id)
    '                                lbl_popmsg_msg.Text = "Thanks for submitting your order. We will get back to you within 2 business day."

    '                            Else
    '                                lbl_popmsg_msg.Text = "Sorry, Bid amount exceeds your bidding limit."

    '                            End If

    '                        Case 2

    '                            If CommonCode.Fetch_Cookie_Shared("is_buyer") = "1" Then
    '                                obj.insert_tbl_auction_bids(auction_id, user_id, 0)
    '                                lbl_popmsg_msg.Text = "Thanks for submitting your order. We will get back to you within 2 business day."

    '                            ElseIf CDbl(.Item("tradition_amount")) <= CDbl(.Item("bidding_limit").ToString) Then
    '                                obj.insert_tbl_auction_bids(auction_id, user_id, 0)
    '                                lbl_popmsg_msg.Text = "Thanks for submitting your order. We will get back to you within 2 business day."

    '                            Else
    '                                lbl_popmsg_msg.Text = "Sorry, Bid amount exceeds your bidding limit."

    '                            End If

    '                        Case 3
    '                            price = txt_bid_now.Value.ToString.Trim()
    '                            If CommonCode.Fetch_Cookie_Shared("is_buyer") = "1" Then
    '                                obj.insert_tbl_auction_bids(auction_id, user_id, price)
    '                                lbl_popmsg_msg.Text = "Thanks for submitting your order. We will get back to you within 2 business day."

    '                            ElseIf price <= CDbl(.Item("bidding_limit").ToString) Then
    '                                obj.insert_tbl_auction_bids(auction_id, user_id, price)
    '                                lbl_popmsg_msg.Text = "Thanks for submitting your order. We will get back to you within 2 business day."

    '                            Else
    '                                lbl_popmsg_msg.Text = "Sorry, Bid amount exceeds your bidding limit."

    '                            End If

    '                        Case 4
    '                            hid_buyer_id.Value = buyer_id
    '                            hid_auction_id.Value = auction_id
    '                            modalPopUpExtender3.Show()
    '                            Exit Sub

    '                        Case Else

    '                    End Select
    '                Else
    '                    'if offer clicked

    '                    Select Case ViewState("option")
    '                        Case 1
    '                            If CommonCode.Fetch_Cookie_Shared("is_buyer") = "1" Then
    '                                obj.save_buy_now_bid(auction_id, 0, 0, "buy now", user_id, buyer_id)
    '                                lbl_popmsg_msg.Text = "Thanks for submitting your order. We will get back to you within 2 business day."
    '                            ElseIf CDbl(.Item("buy_now_price").ToString) <= CDbl(.Item("bidding_limit").ToString) Then
    '                                obj.save_buy_now_bid(auction_id, 0, 0, "buy now", user_id, buyer_id)
    '                                lbl_popmsg_msg.Text = "Thanks for submitting your order. We will get back to you within 2 business day."
    '                            Else
    '                                lbl_popmsg_msg.Text = "Sorry, Bid amount exceeds your bidding limit."
    '                            End If

    '                        Case 2
    '                            price = txt_amount.Text.Trim
    '                            If CommonCode.Fetch_Cookie_Shared("is_buyer") = "1" Then
    '                                obj.save_buy_now_bid(auction_id, price, 0, "private", user_id, buyer_id)
    '                                lbl_popmsg_msg.Text = "Thanks for submitting your order. We will get back to you within 2 business day."

    '                            ElseIf price <= CDbl(.Item("bidding_limit").ToString) Then
    '                                obj.save_buy_now_bid(auction_id, price, 0, "private", user_id, buyer_id)
    '                                lbl_popmsg_msg.Text = "Thanks for submitting your order. We will get back to you within 2 business day."

    '                            Else
    '                                lbl_popmsg_msg.Text = "Sorry, Bid amount exceeds your bidding limit."

    '                            End If

    '                        Case 3
    '                            price = txt_amount.Text.Trim
    '                            If CommonCode.Fetch_Cookie_Shared("is_buyer") = "1" Then
    '                                obj.save_buy_now_bid(auction_id, price, txt_qty.Text, "partial", user_id, buyer_id)
    '                                lbl_popmsg_msg.Text = "Thanks for submitting your order. We will get back to you within 2 business day."

    '                            ElseIf price <= CDbl(.Item("bidding_limit").ToString) Then
    '                                obj.save_buy_now_bid(auction_id, price, txt_qty.Text, "partial", user_id, buyer_id)
    '                                lbl_popmsg_msg.Text = "Thanks for submitting your order. We will get back to you within 2 business day."

    '                            Else
    '                                lbl_popmsg_msg.Text = "Sorry, Bid amount exceeds your bidding limit."

    '                            End If

    '                        Case Else

    '                    End Select
    '                End If

    '                obj = Nothing


    '            Else
    '                'bid is over
    '                lbl_popmsg_msg.Text = "Sorry, Bid time is over."

    '            End If

    '        End With
    '        txt_amount.Text = ""
    '        'txt_bid_now.Text = ""
    '        txt_login_name.Text = ""
    '        txt_qty.Text = ""
    '        txt_login_pwd.Text = ""

    '        modalPopUp_msg.Show()
    '    End If
    '    dtTable = Nothing
    'End Sub

    Private Sub send_to_confirmation_page(ByVal bid_id As Integer, ByVal bid_type As String)
        Response.Redirect("/Bid_confirmation.aspx?bid_id=" & bid_id & "&bid_type=" & bid_type & "&t=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("t")), "0", Request.QueryString.Get("t")))
    End Sub

    'Protected Sub btn_buy_now_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_buy_now.Click
    '    ViewState("option") = 1
    '    offer_bid_buy_click("Buy Now")

    'End Sub

    'Protected Sub btn_send_private_offer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_send_private_offer.Click
    '    ViewState("option") = 2
    '    offer_bid_buy_click("Send Private Offer")

    'End Sub

    'Protected Sub btn_send_partial_offer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_send_partial_offer.Click
    '    ViewState("option") = 3
    '    offer_bid_buy_click("Send Partial Offer")

    'End Sub

    'Protected Sub offer_bid_buy_click(ByVal _opt As String)
    '    lbl_bid_auction.Text = lbl_title.Text
    '    lbl_popmsg_auction.Text = lbl_title.Text
    '    lit_offer_heading.Text = _opt & " : "
    '    ImageButtonOffer.ImageUrl = "/images/fend/4.png"
    '    ImageButtonOffer.CausesValidation = True
    '    If _opt.ToLower.Contains("partial") Then
    '        div_qty.Visible = True
    '        RFOffer2.Enabled = True
    '        CValidator2.Enabled = True

    '        div_amount.Visible = True
    '        div_buy_now_price_label.Visible = False
    '        pnlPopUpOffer.Height = 245
    '    ElseIf _opt.ToLower.Contains("buy") Or _opt.ToLower = "bid now" Then
    '        div_qty.Visible = False
    '        RFOffer2.Enabled = False
    '        CValidator2.Enabled = False
    '        div_amount.Visible = False

    '        div_buy_now_price_label.Visible = True
    '        pnlPopUpOffer.Height = 200
    '    Else
    '        div_qty.Visible = False
    '        RFOffer2.Enabled = False
    '        CValidator2.Enabled = False
    '        div_amount.Visible = True

    '        div_buy_now_price_label.Visible = False
    '        pnlPopUpOffer.Height = 200
    '    End If
    '    'CValidator_a.Enabled = IIf(Not _opt.ToLower = "bid for", False, True)
    '    'CValidator_a.ValueToCompare = IIf(Not _opt.ToLower = "bid for", Nothing, txt_bid_now.Value)
    '    ' txt_amount.Text = IIf(Not _opt.ToLower = "bid for", "", txt_bid_now.Text)
    '    modalPopUpExtender2.Show()
    'End Sub
    'Protected Sub ImageButtonOffer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonOffer.Click
    '    If CommonCode.Fetch_Cookie_Shared("user_id") = "" Then
    '        lbl_login_auction.Text = lbl_title.Text
    '        modalPopUpExtender1.Show()
    '    Else
    '        BidNow(CommonCode.Fetch_Cookie_Shared("user_id"), CommonCode.Fetch_Cookie_Shared("buyer_id"))

    '    End If
    'End Sub

    'Protected Sub ImageButton_quote_send_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_quote_send.Click
    '    request_quote()
    '    modalPopUpExtender3.Hide()
    '    lbl_popmsg_msg.Text = "Thanks for submitting your order. We will get back to you within 2 business day."
    '    modalPopUp_msg.Show()
    'End Sub

    'Private Sub request_quote()
    '    Dim auction_id As Integer = hid_auction_id.Value
    '    Dim buyer_user_id As Integer = CommonCode.Fetch_Cookie_Shared("user_id")
    '    Dim details As String = txt_quote_desc.Text.Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>")
    '    Dim str As String = ""
    '    Dim Quotation_id As Integer = 0

    '    Dim filename As String = ""
    '    If fle_quote_upload.HasFile Then
    '        filename = fle_quote_upload.FileName
    '        filename = buyer_user_id.ToString() & "_" & filename
    '    End If
    '    str = "Insert into tbl_auction_quotations(auction_id,buyer_user_id,bid_date,details,filename)Values(" & auction_id & "," & buyer_user_id & ",getdate(),'" & details & "','" & filename & "') select scope_identity()"
    '    Dim obj As New Bid()
    '    Quotation_id = obj.send_quotation(auction_id, details.Trim(), buyer_user_id, hid_buyer_id.Value)

    '    If fle_quote_upload.HasFile Then
    '        If Quotation_id > 0 Then
    '            obj.upload_quotation_file(auction_id, Quotation_id, fle_quote_upload, buyer_user_id)

    '        End If

    '    Else

    '    End If

    '    obj = Nothing
    'End Sub


    'Protected Sub ImageButton_msg_cancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_msg_cancel.Click
    '    Me.Page_Load(sender, e)
    'End Sub

    Protected Sub load_auction_bidder_queries()
        If CommonCode.Fetch_Cookie_Shared("user_id") <> "" Then
            TabStip1.Tabs(4).Visible = True
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDatatable("select A.query_id,A.title,A.submit_date,A.buyer_id,B.company_name,isnull(case when B.state_id<>0 then (select name from tbl_master_states where state_id=B.state_id) else B.state_text end,'') as state from tbl_auction_queries A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id inner join tbl_auctions A1 WITH (NOLOCK) on A.auction_id=A1.auction_id where A.parent_query_id=0 and A.auction_id=" & IIf(String.IsNullOrEmpty(ViewState("auction_id").ToString), 0, ViewState("auction_id").ToString) & " and A.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " order by submit_date desc")
            rfv_create_thread.ValidationGroup = "create_" & img_btn_create_thread.CommandArgument & "_thread"
            img_btn_create_thread.ValidationGroup = "create_" & img_btn_create_thread.CommandArgument & "_thread"
            rep_after_queries.DataSource = dt
            rep_after_queries.DataBind()
            If dt.Rows.Count = 0 Then
                lit_your_query.Visible = False
            End If
        Else
            TabStip1.Tabs(4).Visible = False
        End If
    End Sub

    Protected Sub img_btn_create_thread_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs)
        Dim str As String = "INSERT into tbl_auction_queries(auction_id,title,buyer_id,buyer_user_id,parent_query_id,submit_date) " & _
        " Values (" & img_btn_create_thread.CommandArgument & ",'" & txt_thread_title.Text.Trim().Replace(Char.ConvertFromUtf32(13), "<br/>").Replace(Char.ConvertFromUtf32(10), "<br/>") & "'," & CommonCode.Fetch_Cookie_Shared("buyer_id") & "," & CommonCode.Fetch_Cookie_Shared("user_id") & ",0,getdate())"
        SqlHelper.ExecuteNonQuery(str)
        txt_thread_title.Text = ""
        load_auction_bidder_queries()
    End Sub

    Protected Sub rep_after_queries_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rep_after_queries.ItemCommand
        If e.CommandName = "send_query" Then
            Dim Rsp As String = DirectCast(e.Item.FindControl("rep_txt_message"), TextBox).Text.Trim.Replace("'", "''")
            If Rsp <> "Response" Then
                SqlHelper.ExecuteNonQuery("insert into tbl_auction_queries(auction_id,message,buyer_id,buyer_user_id,parent_query_id,submit_date)values(" & ViewState("auction_id") & ",'" & Rsp & "'," & CommonCode.Fetch_Cookie_Shared("buyer_id") & "," & CommonCode.Fetch_Cookie_Shared("user_id") & "," & e.CommandArgument & ",getdate())")
                load_auction_bidder_queries()
            End If
        End If
    End Sub

    Protected Sub rep_after_queries_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rep_after_queries.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim rep_inner As Repeater = DirectCast(e.Item.FindControl("rep_inner_after_queries"), Repeater)
            rep_inner.DataSource = SqlHelper.ExecuteDataTable("select A.message, A.submit_date, ISNULL(C.first_name,'') AS first_name,ISNULL(C.last_name,'') AS last_name,ISNULL(D.first_name,'') AS buyer_first_name,ISNULL(D.last_name,'') AS buyer_last_name,isnull(C.title,'') as title,isnull(D.title,'') as buyer_title,case ISNULL(A.user_id,0) when 0 then cast(0 as Bit) else cast(1 as bit) end AS is_admin,isnull(C.image_path,'') as image_path,C.user_id from tbl_auction_queries A inner join tbl_reg_buyers B on A.buyer_id=B.buyer_id left join tbl_sec_users C on A.user_id=C.user_id LEFT JOIN tbl_reg_buyer_users D ON A.buyer_user_id=D.buyer_user_id where A.parent_query_id=" & DirectCast(e.Item.DataItem, DataRowView).Item(0) & " order by submit_date desc")
            rep_inner.DataBind()
        End If
    End Sub
End Class
