﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="order_confirm.ascx.vb"
    Inherits="UserControls_order_confirm" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<script type="text/javascript" language="javascript">
    function ShowWait() {
        document.getElementById('wait').style.display = 'block';
        document.getElementById('<%=img_but_confirm.ClientID%>').style.display = 'none';
       return true;

        }
</script>

      <div class="innerwraper" style=" background-color:#FFFFFF;">
<div runat="server" id="dv_cnfm_msg" style="color: Red; font-weight: bold; font-size: 16px;
    text-align: left; padding: 10px; padding-left: 20px;">
    Your request is not yet submitted. Please scroll down and click on "CONFIRM THIS
    ORDER" button.
</div>
<!--Add the middle section here-->
                          
<link href="/Style/AuctionBody.css" rel="stylesheet" type="text/css" id="LNK_CSS" runat="server" />
        
<asp:Panel runat="server" ID="pnl_ord_cnfm">
    <asp:hiddenField ID="hid_bid_id" runat="server" Value="0" />
    <div style="width: 800px; padding-left: 20px; padding-bottom: 20px;">
        <div style="padding-bottom: 15px; padding-top: 10px;">
            <div class="auctionTitle" style="width: 650px; float: left; color: #006FD5; font-family: pcchead,arial;
                font-size: 19px;">
                <asp:Label runat="server" ID="lbl_page_heading"></asp:Label>
            </div>
            <div style="width: 150px; float: right; text-align: right;">
                <asp:ImageButton runat="server" ID="img_but_print" Visible="false" OnClientClick="printPartOfPage('dv_print');"
                    ImageUrl="/images/print_icon.png" AlternateText="PRINT" />&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton runat="server" ID="img_but_email" Visible="false" AlternateText="EMAIL"
                    ImageUrl="/images/email_icon.png" />
            </div>
        </div>
        <div style="clear: both;" id="dv_print">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="color: #767676; text-align: left;">
                        <span style="color: #000000; font-size: 16px; font-weight: bold; text-align: left;">
                        PCS Wireless LLC</span><br />
                        11 Vreeland Road<br />
                        Florham Park NJ, 07932<br />
                        Phone: (973) 850-7400 Fax: (973) 301-0975<br />
                        www.overstockwireless.com<br />
                        <%= SqlHelper.of_FetchKey("site_email_to")  %>
                    </td>
                    <td align="right" valign="top">
                        <img src='<%= SqlHelper.of_FetchKey("ServerHttp")%>/Images/logo.png' alt="PCS Wireless Auctions"
                            border="0" />
                        <br />
                        <br />
                        <table>
                            <tr>
                                <td style="color: #3C3C3C; font-weight: bold; text-align: right;">
                                    Date:
                                </td>
                                <td style="color: #3C3C3C; font-weight: bold; text-align: left;">
                                    <asp:Literal ID="lit_date" runat="server" />
                                </td>
                            </tr>
                            <tr runat="server" id="tr_confirm_no">
                                <td style="color: #3C3C3C; font-weight: bold; text-align: right;">
                                    Confirmation No.:
                                </td>
                                <td style="color: #3C3C3C; font-weight: bold; text-align: left;">
                                    <asp:Literal ID="lit_confirm_no" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div style="border: 1px solid #E3E3E3; float: left; margin: 20px 0; padding: 20px;
                text-align: left;">
<strong>Ship To : </strong>
                            <br />
                            <asp:Literal ID="lit_bill_to" runat="server" />
               
            </div>
            <br />
            <table width="100%" cellspacing="0" border="0" style="border-spacing: 0;">
                <tr>
                    <td align="center" style="border: 1px solid  #E3E3E3; padding-left: 15px; width: 27%;
                        padding: 10px;">
                        <b>AUCTION TITLE</b>
                    </td>
                    <td align="center" style="border: 1px solid  #E3E3E3; width: 13%; padding: 10px;">
                        <b>AUCTION NO</b>
                    </td>
                    <td align="center" style="border: 1px solid  #E3E3E3; width: 13%; padding: 10px;">
                        <b>CONDITION</b>
                    </td>
                    <td style="border: 1px solid  #E3E3E3; width: 13%; padding: 10px;" align="center">
                        <b>PACKAGING</b>
                    </td>
                    <td align="center" style="border: 1px solid  #E3E3E3; width: 8%; padding: 10px;">
                        <b>QTY</b>
                    </td>
                    <td align="center" style="border: 1px solid  #E3E3E3; width: 13%; padding: 10px;">
                        <b>PRICE</b>
                    </td>
                    <td align="center" style="border: 1px solid  #E3E3E3; width: 13%; padding: 10px;">
                        <b>AMOUNT</b>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="border: 1px solid  #E3E3E3; padding: 10px; padding-left: 15px;">
                        <asp:Literal ID="lit_auc_name" runat="server" />
                    </td>
                    <td style="border: 1px solid  #E3E3E3; padding: 10px;" align="center">
                        <asp:Literal ID="lit_auc_code" runat="server" />
                    </td>
                    <td style="border: 1px solid  #E3E3E3; padding: 10px;" align="center">
                        <asp:Literal ID="lit_condition" runat="server" />
                    </td>
                    <td style="border: 1px solid  #E3E3E3; padding: 10px;" align="center">
                        <asp:Literal ID="lit_auc_packaging" runat="server" />
                    </td>
                    <td style="border: 1px solid  #E3E3E3; padding: 10px;" align="center">
                        <asp:Literal ID="lit_qty" runat="server" />
                    </td>
                    <td style="border: 1px solid  #E3E3E3; padding: 10px;" align="center">
                        <asp:Literal ID="lit_price" runat="server" />
                    </td>
                    <td style="border: 1px solid  #E3E3E3; padding: 10px;" align="right">
                        <asp:Literal ID="lit_amount" runat="server" />
                    </td>
                </tr>
                <asp:Panel runat="server" ID="pnl_upd">
                    <asp:UpdatePanel ID="upd_items" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnl_upd_ship">
                                <tr>
                                    <td align="left" style="border: 1px solid  #E3E3E3; padding: 10px; padding-left: 15px;
                                        background-color: #FFFFFF" colspan="5" rowspan="4">
                                        <table cellpadding="5" cellspacing="1" width="90%" border="0" style="background-color: #E3E3E3;
                                            padding-bottom: 5px;">
                                            <tr>
                                                <td style="text-align: left; font-size: 13px;">
                                                    <b>Shipping Options</b><br />
                                                    <asp:Label runat="server" ID="lbl_shipping_option" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr runat="server" id="tr_ship_option">
                                                <td style="padding: 0px 0px 0px 10px; color: black;" align="left">
                                                    <asp:RadioButton ID="rdo_my_ship" runat="server" Text="Use my own shipping carrier to ship this item to me"
                                                        AutoPostBack="true" GroupName="_ship_opt" />
                                                    <div id="tbl_user_own" runat="server" visible="false" style="margin-left: 10px; margin-top: 5px">
                                                        <table border="0" style="border: 1px solid grey; width: 260px;">
                                                            <tr>
                                                                <td style="text-align: left;">
                                                                    My shipping carrier is<br />
                                                                    <asp:TextBox ID="txt_shipping_type" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td rowspan="2">
                                                                    <table id="table_stock" runat="server" width="100%">
                                                                        <tr>
                                                                            <td valign="top" style="border-left: 1px solid gray;">
                                                                                <span style="color: #10C0F5;">Stock Location:</span><br />
                                                                                <asp:Label ID="lbl_stock_location" runat="server" Text=""></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: left" colspan="2">
                                                                    My account # is<br />
                                                                    <asp:TextBox ID="txt_shipping_account_number" runat="server"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <br />
                                                    <asp:RadioButton ID="rdo_fedex_ship" runat="server" Text="Estimated FedEx Shipping Rates"
                                                        AutoPostBack="true" GroupName="_ship_opt" />
                                                    <%-- <asp:RadioButtonList ID="rdo_option" runat="server" AutoPostBack="true">
                                                <asp:ListItem Text="Use my own shipping carrier to ship this item to me" 
                                                    Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Estimated FedEx Shipping Rates" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>

                                                    --%>
                                                    <div id="tbl_user_ours" runat="server" visible="false" style="border: 1px solid grey;
                                                        width: 280px; text-align: left; margin-left: 10px; margin-top: 5px;">
                                                        <asp:RadioButtonList ID="rdo_fedex" runat="server" AutoPostBack="true">
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <%--<asp:Button ID="btn_save" runat="server" Text="Save" />--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="border: 1px solid  #E3E3E3; padding: 10px;" align="left">
                                        <b>Sub Total</b>
                                    </td>
                                    <td style="border: 1px solid  #E3E3E3; padding: 10px;" align="right">
                                        <b>
                                            <asp:Literal ID="lit_sub_total" runat="server"></asp:Literal></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid  #E3E3E3; padding: 10px;" align="left">
                                        <b>Tax</b>
                                    </td>
                                    <td style="border: 1px solid  #E3E3E3; padding: 10px;" align="right">
                                        <b>
                                            <asp:Literal ID="lit_tax" runat="server"></asp:Literal></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid  #E3E3E3; padding: 10px;" align="left">
                                        <b>Freight</b>
                                    </td>
                                    <td style="border: 1px solid  #E3E3E3; padding: 10px;" align="right">
                                        <b>
                                            <asp:Literal ID="lit_ship_amount" runat="server"></asp:Literal></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid  #E3E3E3; padding: 10px;" align="left">
                                        <b>Total</b>
                                    </td>
                                    <td style="border: 1px solid  #E3E3E3; padding: 10px;" align="right">
                                        <b>
                                            <asp:Literal ID="lit_grand_total" runat="server"></asp:Literal></b>
                                    </td>
                                </tr>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <tr style="background-color: #FFFFFF;" runat="server" id="tr_confirm_but">
                    <td colspan="7" align="right" style="background-color: #FFFFFF; padding: 10px; border: 1px solid  #E3E3E3;">
                        <asp:ImageButton runat="server" ID="img_but_confirm" AlternateText="Confirm This Order"
                            ImageUrl="/images/confirmthisorder_2.png" OnClientClick="javascript:ShowWait();" CausesValidation="false" />
                        <img id="wait" src="/Images/waitimg.gif" border='none' alt="Wait" style="display: none;" />
                    </td>
                </tr>
                <tr runat="server" id="tr_payment" visible="false">
                    <td colspan="7" align="right"><br />
                        <asp:Literal id="lit_payment_link" runat="server"></asp:Literal>
                        
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
<!--middle section ends here-->
<center style="padding: 25px 15px; min-height:800px;">
   <asp:Label ID="lbl_popmsg_msg" Font-Size="Large" runat="server"
        ForeColor="#B18B4F" Font-Bold="true"></asp:Label>
</center>
</div>
<script type="text/javascript">
    function printPartOfPage(elementId) {
        var printContent = document.getElementById(elementId);
        var windowUrl = 'about:blank';
        var uniqueName = new Date();
        var windowName = 'Print' + uniqueName.getTime();
        var printWindow = window.open(windowUrl, windowName, 'left=500,top=400,width=500,height=500');
        //alert("<html><head><title>" + document.getElementById('<%=lbl_page_heading.ClientID %>').innerHTML + "</title></head><body>" + printContent.innerHTML + "</body>");
        //printWindow.document.title = "Confirm Order";
        printWindow.document.write("<html><head><title>" + document.getElementById('<%=lbl_page_heading.ClientID %>').innerHTML + "</title></head><body>" + printContent.innerHTML + "</body>");
        printWindow.document.close();
        printWindow.focus();
        printWindow.print();
        printWindow.close();
    }
</script>
<%--</ContentTemplate>
            </ajax:UpdatePanel>--%>