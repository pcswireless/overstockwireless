﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="My_Auctions.ascx.vb"
    Inherits="UserControls_My_Auctions" %>
<asp:Panel ID="pn_auction_content" runat="server">
    <asp:Repeater ID="rpt_auctions" runat="server">
        <ItemTemplate>
            <asp:HiddenField ID="hid_auction_type" runat="server" Value="0" />
            <asp:HiddenField ID="hid_buy_id" runat="server" Value='<%# Eval("buy_id")%>' />
            <asp:HiddenField ID="hid_quotation_id" runat="server" Value='<%# Eval("quotation_id")%>' />
            <asp:HiddenField ID="hid_auction_id" runat="server" Value='<%# Eval("auction_id")%>' />
            <div class='box <%# IIf(((Container.ItemIndex + 2) Mod 3) = 0, "box2", "box1")%>'>
                <div class="proimg">
                    <a id="a_img_auction" style="text-decoration: none; cursor: pointer" runat="server">
                        <asp:Image ID="img_auction_image" runat="server" CssClass="aucImage" />
                    </a>
                </div>
                <!--proimg -->
                <div class="txtbx">
                    <h1>
                        <asp:Literal runat="server" ID="ltr_title"></asp:Literal></h1>
                    <div class="clear">
                    </div>
                    <div class="MyBidstatus">
                        <asp:Literal ID="ltrl_status" runat="server" />
                    </div>
                    <div class="clear">
                    </div>
                    <asp:Literal ID="ltr_button_auction" runat="server" Text="More Info"></asp:Literal>
                    <!--btn -->
                    <div class="timetxt" style="padding-top:15px;">
                        <span class="txt1">
                            <asp:Literal ID="lit_auc_status" runat="server" Text="Status"></asp:Literal></span><div
                                class="clear">
                            </div>
                        <span class="txt2" style="font-size: 15px;white-space:nowrap;">
                            <asp:panel ID="pnl_iframe" runat="server" Visible="false" ViewStateMode="Disabled">
                            <iframe id="iframe_time" style="width: 100%; overflow: hidden;" height="50;" scrolling="no"
                                frameborder="0" src='/frame_auction_time.aspx?is_frontend=1&i=<%#Eval("auction_id")%>&refresh=0'>
                            </iframe>
                            </asp:panel>
                            <asp:Literal ID="lit_approval_status" runat="server" Text=""></asp:Literal></span>
                            <div class="clear">
                            </div>
                    </div>
                    <!-- status text -->
                    <div class="clear">
                    </div>
                </div>
                <!--txtbx -->
            </div>
            <%# IIf(((Container.ItemIndex + 1) Mod 3) = 0, "<div class='clr'></div>", "")%>
        </ItemTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Panel ID="pnl_noItem" runat="server">
<center><br /><span style="font-family:'dinregular',arial;font-size:20px;color:#EC6811;">Auction Not available in this section.</span></center>
    <div style="border: 0px solid #10AAEA; background: url('/Images/fend/grdnt_template.jpg') repeat-y 100%;
        height: 800px; padding-top: 25px; text-align: center;">
        <b>
            <asp:Literal ID="lit_no_item" runat="server"></asp:Literal>
        </b>
    </div>
</asp:Panel>
<div class="clear">
</div>
