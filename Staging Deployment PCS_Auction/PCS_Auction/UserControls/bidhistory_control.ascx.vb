﻿
Partial Class bidhistory_control
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
             If CommonCode.Fetch_Cookie_Shared("buyer_id") <> "" Then
                rep_bid_history.DataSource = SqlHelper.ExecuteDatatable("SELECT A.bid_amount, A.bid_type, (A1.first_name + ' ' +A1.last_name) as bidder,convert(varchar,bid_date, 120) as bid_date,A1.email FROM tbl_auction_bids A WITH (NOLOCK) inner join tbl_reg_buyer_users A1 on A.buyer_user_id=A1.buyer_user_id where A.buyer_id=" & CommonCode.Fetch_Cookie_Shared("buyer_id") & " and A.auction_id=" & Security.EncryptionDecryption.DecryptValueFormatted(Request.QueryString.Get("i")) & " order by A.bid_id desc")
                rep_bid_history.DataBind()
                If rep_bid_history.Items.Count > 0 Then
                    empty_data.Visible = False
                    rep_bid_history.Visible = True
                Else
                    rep_bid_history.Visible = False
                    empty_data.Visible = True
                End If
            End If
        End If
    End Sub
End Class
