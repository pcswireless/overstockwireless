﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AuctionMain.master" AutoEventWireup="false"
    CodeFile="Auction_Listing.aspx.vb" Inherits="Auction_Listing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <script type="text/javascript">

        items = 0;
        a = new Array(200);
        for (i = 0; i < 200; i++)
            a[i] = new Array(1);



        function getValueUsingClass() {
            /* declare an checkbox array */
            var chkArray = [];

            /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
            $(".chk:checked").each(function () {
                chkArray.push($(this).val());
            });

            /* we join the array separated by the comma */
            var selected;
            selected = chkArray.join(',');


            /* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
            if (selected.length > 1) {
                var iCheck = 0;
                var groupPass = 0;
                var allGroupPass = 0;
                var group_ids = document.getElementById('<%=me.hid_group_ids.clientid%>').value.split(',');
                var BucketItem = selected.split(',');
                //alert(group_ids.length);   
                for (i = 0; i < items; i++) {

                    if (a[i][1] != '') {

                        allGroupPass = 1;
                        for (var igr = 0; igr < group_ids.length; igr++) {
                            groupPass = 0;
                            if (selected.indexOf(group_ids[igr]) != -1) {
                                //if filter selected from this group
                                for (var iCtr = 0; iCtr < BucketItem.length; iCtr++) {
                                    var BucketValue = BucketItem[iCtr].split(';');
                                    if (group_ids[igr] == BucketValue[0]) {
                                        if (a[i][1].indexOf(BucketValue[1].toUpperCase()) != -1) {
                                            groupPass = 1;
                                        }
                                    }
                                } //for iCtr
                            }
                            else {
                                groupPass = 1;
                            }
                            if (groupPass == 0)
                                allGroupPass = 0;

                        } //for igr

                        if (allGroupPass == 1) {
                            document.getElementById("divItem" + a[i][0]).style.display = "block";
                            iCheck = iCheck + 1;
                        }
                        else {
                            document.getElementById("divItem" + a[i][0]).style.display = "none";
                        }

                    }
                    else {
                        document.getElementById("divItem" + a[i][0]).style.display = "none";
                    }
                }


                if (iCheck == 0)
                { document.getElementById("Msg").style.display = "block"; }
                else
                { document.getElementById("Msg").style.display = "none"; }

            }
            else {
                for (i = 0; i < items; i++) { document.getElementById("divItem" + a[i][0]).style.display = "block"; }
                document.getElementById("Msg").style.display = "none";
            }

        }
    </script>
    <br />
    <div class="innerwraper">
        <div class="paging">
            <ul>
                <li><a href="/">Home</a></li>
                <li>
                    <asp:Literal ID="lit_tab" runat="server" /></li>
                <li>
                    <asp:Literal ID="lit_type" runat="server" /></li>
            </ul>
        </div>
        <div class="printbx" style="visibility: hidden;">
            <ul>
                <li class="print"><a href="javascript:window.print();">Print</a></li>
                <%--       <li><a href='#' target="_blank" id="a_convert_to_pdf" runat="server">Convert To Pdf</a></li>--%>
                <li>
                    <asp:Literal ID="lit_pdf" runat="server" /></li>
            </ul>
        </div>
        <div id="div_topmsg" runat="server" style="background-color: #EDF9FF; padding: 5px;
            margin-top: 10px; padding-left: 40px; background-image: url('/images/bulb.gif');
            background-repeat: no-repeat; background-position: 10px 5px;">
            <span class="duplAuction">Message for you...</span>
            <asp:Repeater ID="rep_message" runat="server">
                <ItemTemplate>
                    <div style="border: 1px solid #FCAD35; padding: 5px; margin-top: 15px; text-align: justify;">
                        <b>
                            <%# Eval("title")%></b> -
                        <%# Eval("description")%>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="clear">
        </div>
        <div class="products">
            <div class="seprator">
            </div>
            <!--seprator -->
            <div class="slctmenu">
                <select id="auc_catg" onchange="page_redirect(this.value);" class="slc" runat="server">
                    <option value="select" selected="selected" class="sclt">--All--</option>
                    <option value="1">Auctions Only</option>
                    <option value="2">Buy Now Only</option>
                    <option value="3">Offers Only</option>
                </select>
            </div>
            <!--slctmenu -->
            <%--Live Auctions--%>
            <h1 class="title">
                <asp:Literal ID="tab_sel" runat="server" /></h1>
            <div class="probxwrap">
                <script type="text/javascript">
                    function calculate_rank(_ranks) {
                        //alert(_ranks);
                        var auction_ranks = new Array();
                        auction_ranks = _ranks.split("~");
                        if (auction_ranks.length > 0) {
                            //alert(auction_ranks.length);
                            var auction_ids = new Array();
                            //alert(auction_ranks.length);
                            //return;
                            for (var i = 0; i < auction_ranks.length - 1; i++) {
                                //alert(auction_ranks[i]);
                                //return;
                                if (auction_ranks[i]) {
                                    auction_ids = auction_ranks[i].split("#");
                                    if (auction_ids.length > 0) {
                                        if (document.getElementById('auc_' + auction_ids[0]))
                                            document.getElementById('auc_' + auction_ids[0]).innerHTML = auction_ids[1];
                                    }
                                }
                            }


                        }
                        //var auction_ranks = _ranks.split("~")
                        //alert(_ranks);
                    }
                </script>
                <asp:Panel ID="pn_auction_content" runat="server">
                    <iframe id="iframe_auctions" runat="server" style="width: 100%; display: none; overflow: hidden;"
                        height="50;" scrolling="no" frameborder="0"></iframe>
                    <div id="sidebar">
                        <asp:HiddenField ID="hid_group_ids" runat="server" Value="" />
                        <asp:Literal ID="ltrSideBar" runat="server"></asp:Literal>
                    </div>
                    <div id="divContent">
                        <div id="Msg" style="display: none; margin-left: 20px;">
                            <h1>
                                No Item available for this search criteria !</h1>
                        </div>
                        <asp:Repeater ID="rpt_auctions" runat="server">
                            <ItemTemplate>
                                <script language="javascript" type="text/javascript"> 
                                        a[<%# Container.ItemIndex%>][0] = '<%#eval("auction_id")%>';
                                        a[<%# Container.ItemIndex%>][1] = '<%#UCase(Eval("bucket_value_for_filter")).trim().ToString().Replace("&AMP;", "&")%>';
                                        items++;
                                </script>
                                <asp:HiddenField ID="hid_auction_id" runat="server" Value='<%# Eval("auction_id")%>' />
                                <div id="divItem<%#eval("auction_id")%>" class='ItemBox'>
                                    <asp:Literal ID="lit_increase_bid_limit" runat="server"></asp:Literal>
                                    <div class="proimg">
                                        <a id="a_img_auction" style="text-decoration: none; cursor: pointer" runat="server">
                                            <asp:Image ID="img_auction_image" runat="server" CssClass="aucImageListing" />
                                        </a>
                                    </div>
                                    <!--proimg -->
                                    <div class="txtbx">
                                        <h2>
                                            <asp:Literal runat="server" ID="ltr_title"></asp:Literal></h2>
                                        <div class="clear">
                                        </div>
                                        <div class="MyBidstatus">
                                            <span id='auc_<%# Eval("auction_id")%>'>&nbsp;</span>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <asp:Literal ID="ltr_button_auction" runat="server" Text="More Info"></asp:Literal><!--btn -->
                                        <div class="timetxt">
                                            <span class="txt1">
                                                <asp:Literal ID="ltr_auction_status" runat="server" Text="Starts in"></asp:Literal>
                                            </span>
                                            <div class="clear">
                                            </div>
                                            <span class="txt2">
                                                <iframe id="iframe_time" style="width: 100%; overflow: hidden;" height="50;" scrolling="no"
                                                    frameborder="0" src='/frame_auction_time.aspx?is_frontend=1&i=<%#Eval("auction_id")%>&refresh=0'>
                                                </iframe>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <!--timetext -->
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <!--txtbx -->
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnl_noItem" runat="server">
                    <div style="border: 1px solid #10AAEA; background: url('/Images/fend/grdnt_template.jpg') repeat-y 100%;
                        height: 800px; padding-top: 25px; text-align: center;">
                        <div style="font-family: dinbold; font-size: 18px;">
                            <asp:Literal ID="lit_no_item" runat="server"></asp:Literal></div>
                    </div>
                    <br />
                </asp:Panel>
                <div class="clear">
                </div>
            </div>
            <!--probxwrap -->
        </div>
        <!--products -->
    </div>
    <!--innerwraper -->
    <script type="text/javascript">
        function page_redirect(_catg) {
            var url = location.href;
            var str = url;
            if (url != "") {
                var n = url.indexOf("?c=");
                if (n > 0) {
                    str = url.slice(n, url.length);
                    url = url.replace(str, '');
                }

                if (!isNaN(_catg)) {

                    if (url.indexOf("?") > 0)
                        window.location.href = url + '&c=' + _catg;
                    else
                        window.location.href = url + '?c=' + _catg;

                }
                else
                    window.location.href = url;
            }
        }
    </script>
</asp:Content>
