﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AuctionMain.master" AutoEventWireup="false"
    CodeFile="Auction_Details.aspx.vb" Inherits="Auction_Details" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <link href="/Style/elastislide.css" rel="stylesheet" type="text/css" />
    <link href="/style/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/Style/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/source/jquery.fancybox.js?v=2.1.5"></script>   
    <link rel="stylesheet" type="text/css" href="/source/jquery.fancybox.css?v=2.1.5"
        media="screen" />
    <script type="text/javascript">

        $(document).ready(function () {
            /*
            *  Simple image gallery. Uses default settings
            */

            $('.fancybox').fancybox();

            /*
            *  Different effects
            */

            // Change title type, overlay closing speed
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    overlay: {
                        speedOut: 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                helpers: {
                    title: {
                        type: 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS: 'fancybox-custom',
                closeClick: true,

                openEffect: 'none',

                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay: {
                        css: {
                            'background': 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });

            // Remove padding, set opening and closing animations, close if clicked and disable overlay
            $(".fancybox-effects-d").fancybox({
                padding: 0,

                openEffect: 'elastic',
                openSpeed: 150,

                closeEffect: 'elastic',
                closeSpeed: 150,

                closeClick: true,

                helpers: {
                    overlay: null
                }
            });

            /*
            *  Button helper. Disable animations, hide close button, change title type and content
            */

            $('.fancybox-buttons').fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                prevEffect: 'none',
                nextEffect: 'none',

                closeBtn: false,

                helpers: {
                    title: {
                        type: 'inside'
                    },
                    buttons: {}
                },

                afterLoad: function () {
                    this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
                }
            });


            /*
            *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
            */

            $('.fancybox-thumbs').fancybox({
                prevEffect: 'none',
                nextEffect: 'none',

                closeBtn: false,
                arrows: false,
                nextClick: true,

                helpers: {
                    thumbs: {
                        width: 50,
                        height: 50
                    }
                }
            });

            /*
            *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
            */
            $('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
				    openEffect: 'none',
				    closeEffect: 'none',
				    prevEffect: 'none',
				    nextEffect: 'none',

				    arrows: false,
				    helpers: {
				        media: {},
				        buttons: {}
				    }
				});

            /*
            *  Open manually
            */

            $("#fancybox-manual-a").click(function () {
                $.fancybox.open('1_b.jpg');
            });

            $("#fancybox-manual-b").click(function () {
                $.fancybox.open({
                    href: 'iframe.html',
                    type: 'iframe',
                    padding: 5
                });
            });

            $("#fancybox-manual-c").click(function () {
                $.fancybox.open([
					{
					    href: '1_b.jpg',
					    title: 'My title'
					}, {
					    href: '2_b.jpg',
					    title: '2nd title'
					}, {
					    href: '3_b.jpg'
					}
                ], {
                    helpers: {
                        thumbs: {
                            width: 75,
                            height: 50
                        }
                    }
                });
            });


        });
	</script>
    <script type="text/javascript">
        $(function () {

            $(".typtxt").each(function () {
                $tb = $(this);
                if ($tb.val() != this.title) {

                }
            });

            $(".typtxt").focus(function () {
                $tb = $(this);
                if ($tb.val() == this.title) {
                    $tb.val("");

                }
            });

            $(".typtxt").blur(function () {
                $tb = $(this);
                if ($.trim($tb.val()) == "") {
                    $tb.val(this.title);
                    $tb.addClass("typtxt");
                }
            });
        });

      
       

    </script>
    <style type="text/css">
        .fancybox-custom .fancybox-skin
        {
            box-shadow: 0 0 50px #222;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function printDiv(divID) {
            //Get the HTML of div

            var divElements = document.getElementById(divID).innerHTML;

            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head><title></title><link href='/Style/stylesheet.css' rel='stylesheet' type='text/css' /></head><body> <div class='detail'><div  class='innerwraper wraper1' style='width:1000px;'>" +
              divElements + "</div></div></body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;


        }
        function open_print() {
            window.open('/print_detail.aspx?a=<%=Request("a")%>&t=<%=Request("t")%>&i=<%=Request("i")%>&m=<%=Request("m")%>', '_print', 'top=' + ((screen.height - 700) / 2) + ', left=' + ((screen.width - 1000) / 2) + ', height=700, width=1000,scrollbars=yes,toolbars=no,resizable=1;');
            window.focus();
            return false;
        }
        function open_print_pdf() {
            window.open('/pdf_detail.aspx?a=<%=Request("a")%>&t=<%=Request("t")%>&i=<%=Request("i")%>&m=<%=Request("m")%>', '_pdf', 'top=' + ((screen.height - 700) / 2) + ', left=' + ((screen.width - 1000) / 2) + ', height=700, width=1000,scrollbars=yes,toolbars=no,resizable=1;');
            window.focus();
            return false;
        }

       
    </script>
    <script type="text/javascript" src="/js/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/js/jquery.elastislide.js"></script>
    <script type="text/javascript" src="/js/gallery.js"></script>
    <script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">
        <div class="rg-image-wrapper">
            {{if itemsCount > 1}}
        <div class="rg-image-nav">
        </div>
            {{/if}}
        <div class="rg-image" style="padding-left:5px\9;padding-right:5px\9;">
        </div>
            <div class="rg-loading">
            </div>
            <div class="rg-caption-wrapper">
                <div class="rg-caption" style="display: none;">
                    <p>
                    </p>
                </div>
            </div>
        </div>
    </script>
    <asp:HiddenField ID="hid_quantity" runat="server" Value="0" />
    <asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
    <asp:HiddenField ID="hid_auction_type" runat="server" Value="0" />
    <asp:HiddenField ID="hid_bid_id" runat="server" Value="0" />
    <asp:Panel ID="pnlDetails" runat="server">
        <div class="detail">
            <div class="innerwraper wraper1">
                <div class="products1">
                    <div class="paging">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li>
                                <asp:Literal ID="ltr_bradcom" Text="<a href='/Auction_Listing.aspx?t=1'>Live Auctions</a>"
                                    runat="server"></asp:Literal></li>
                            <li>
                                <asp:Literal ID="lit_type" runat="server" /></li>
                        </ul>
                    </div>
                    <!--paging -->
                    <div class="printbx" style="display: none;">
                        <ul>
                            <li class="print"><a href='javascript:void(0)' onclick="javascript:open_print()">Print</a></li>
                            <li style="display: none;"><a href='javascript:void(0)' onclick="javascript:open_print_pdf()">
                                Convert To Pdf</a>
                                <asp:Literal ID="lit_pdf" runat="server" /></li>
                        </ul>
                    </div>
                    <!--print -->
                    <div class="clear">
                    </div>
                    <div class="seprator">
                    </div>
                    <!--seprator -->
                    <div class="clear">
                    </div>
                </div>
                <div id="div_print">
                    <div class="products1">
                        <div class="detailbxwrap">
                            <%--<asp:Panel ID="Panel1" runat="server" CssClass="noprint">--%>
                            <div class="lftside glry">
                                <asp:Literal ID="lit_increase_bid_limit1" runat="server"></asp:Literal>
                                <div id="rg-gallery" class="rg-gallery">
                                    <div class="rg-thumbs">
                                        <!-- Elastislide Carousel Thumbnail Viewer -->
                                        <asp:Panel ID="pnl" runat="server" CssClass="noprint">
                                            <div class="es-carousel-wrapper" id="div_thumb">
                                                <div class="es-nav">
                                                    <span class="es-nav-prev">Previous</span> <span class="es-nav-next">Next</span>
                                                </div>
                                                <div class="es-carousel" style="padding-left: 15px; margin-top: -10px;">
                                                    <ul>
                                                        <asp:Repeater ID="rpt_auction_stock_image" runat="server">
                                                            <ItemTemplate>
                                                                <li><a href="#">
                                                                    <img src='/upload/auctions/stock_images/<%#Eval("path").ToString.Replace(".", "_95.") %>'
                                                                        data-large='/upload/auctions/stock_images/<%#Eval("path").ToString.Replace(".", "_375.") %>'
                                                                        alt='image<%#IIf((Container.ItemIndex) <> -1,(Container.ItemIndex) +2,0) %>'
                                                                        data-description="" /></a></li>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </ul>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <!-- End Elastislide Carousel Thumbnail Viewer -->
                                    </div>
                                    <!-- rg-thumbs -->
                                </div>
                                <!-- rg-gallery -->
                                <asp:Literal runat="server" ID="Lit_prod_info"></asp:Literal>
                            </div>
                            <%--</asp:Panel>--%>
                            <!--lftside -->
                            <div class="rightside">
                                <h1>
                                    <asp:Literal ID="ltr_title" Text="title" runat="server" /></h1>
                                <h2>
                                    <asp:Literal ID="ltr_sub_title" Text="sub title" runat="server" /></h2>
                                <div class="sTitle">
                                    Auction # : <strong>
                                        <asp:Literal ID="ltr_auction_code" runat="server" /></strong></div>
                                <asp:Literal ID="lit_increase_bid_limit" runat="server" /><br />
                                <div class="prodetail rglry" style="overflow: visible;">
                                    <asp:Panel runat="server" ID="pnl_from_live">
                                        <iframe id="iframe_price_summary" width="100%" height="150px" scrolling="no" frameborder="0"
                                            runat="server"></iframe>
                                        <div class="clear">
                                        </div>
                                        <div style="float: left;">
                                            <iframe id="iframe_auction_rank" scrolling="no" frameborder="0" width="200" runat="server"
                                                height="55px"></iframe>
                                        </div>
                                        <div style="float: left; padding-top: 20px;">
                                            <asp:Literal runat="server" ID="lit_rank_help" Text=""></asp:Literal>
                                        </div>
                                        <asp:Literal runat="server" ID="ltr_quote_msg"></asp:Literal>
                                        <div class="clear">
                                        </div>
                                        <div style="padding-top: 15px; padding-left: 20px; font-family: 'dinbold'; font-size: 18px;
                                            color: #575757; font-weight: normal;">
                                            <div class="tmlft">
                                                <span style="padding-top: 0px;">
                                                    <asp:Literal runat="server" ID="ltr_timer_caption" Text="Time Left"></asp:Literal></span>
                                            </div>
                                            <!--tmlft -->
                                            <div class="hrtime">
                                                <iframe id="iframe_auction_time" scrolling="no" frameborder="0" width="100%" runat="server"
                                                    height="50px"></iframe>
                                            </div>
                                            <!--htime -->
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <!--timeklft -->
                                        <div class="shpmthd">
                                            <div style="float: left;">
                                                <asp:Panel runat="server" ID="dv_bidding" Visible="false" Style="padding-top: 15px;">
                                                    <asp:RadioButton ID="rb_total_bid_amt" Checked="True" Text="Total" GroupName="max_bid_amt"   runat="server" />
                                                     <asp:RadioButton ID="rb_partial_bid_amt" Checked="False" Text="per unit" GroupName="max_bid_amt" runat="server" />
                                                  <br />
                                                    <asp:TextBox runat="server" CssClass="typtxt" ID="txt_max_bid_amt" Text="Enter Your Bid Amount"
                                                        ToolTip="Enter Your Bid Amount" Width="200"></asp:TextBox>
                                                    <asp:HiddenField runat="server" ID="hid_accept_bid_amt" Value="0" />
                                                    <span class="timeleft_qty" style="float: left;">
                                                        <asp:Literal runat="server" ID="ltr_buy_qty_or_caption" Text=""></asp:Literal>
                                                    </span>
                                                    <asp:DropDownList CssClass="typtxt" ID="ddl_buy_now_qty" Style="width: 100px;" runat="server"
                                                        AutoPostBack="false" />
                                                    <div class="clear">
                                                    </div>
                                                    <span style="color: red; font-size: 14px; font-weight: bold;" id="lotMsg" runat="server"
                                                        visible="false">This bid amount is for entire lot
                                                        <br />
                                                        and not per single unit.<br />
                                                        <br />
                                                    </span>
                                                    <span id="lotPerUnitT" runat="server" >
                                                         <asp:Label id="LPerUnitTotal" runat="server" Text="" />      
                                                    </span>
                                                </asp:Panel>
                                                <iframe id="iframe_auction_amount" scrolling="no" frameborder="0" width="200px;"
                                                    runat="server" height="35px;"></iframe>
                                                <asp:Panel runat="server" ID="dv_bidding1" Visible="false">
                                                    <div class="clear">
                                                    </div>
                                                    <asp:Label ForeColor="Red" Font-Size="22" Text="Sold Out" Font-Names="'dinregular',arial"
                                                        Visible="false" runat="server" ID="ltr_buynow_close"></asp:Label>
                                                </asp:Panel>
                                            </div>
                                            <div style="float: left; overflow: hidden;" runat="server" id="dv_shipping" visible="false">
                                                <div class="btnship">
                                                    <asp:Label Visible="false" runat="server" ID="lit_shipping"></asp:Label>
                                                    <asp:DropDownList ID="ddl_shipping" runat="server" CssClass="typtxt" Style="margin-top: 15px;">
                                                    </asp:DropDownList>
                                                    <div style="clear: both; color: #6F6F6F; font-family: 'dinregular', arial; font-size: 13px;
                                                        line-height: 18px; padding-top: 3px; float: left;">                                                      
                                                    The shipping quote is for shipping inside the USA ONLY! For international shipping quotes, please contact us at 973-805-7400 ext 179  or at <a class="email" href="mailto:info@overstockwireless.com">info@overstockwireless.com</a>  </div>
                                                </div>
                                            </div>
                                        </div>
                                        <asp:HiddenField ID="hid_buy_now_confirm_amount" runat="server" Value="0" />
                                        <div class="clear">
                                        </div>
                                        <!--shpmthd -->
                                        <div class="bidnw">
                                            <div class="cs_Validator" id="div_Validator">
                                                <span id="spn_valid"></span><span id="spn_amt_hlp" style="display: none;">
                                                    <asp:Literal ID="ltr_amt_help" runat="server"></asp:Literal>
                                                </span>
                                            </div>
                                            <asp:Panel ID="pnlNoprint" runat="server" CssClass="noprint">
                                                <asp:Button runat="server" ID="but_main_auction" CssClass="bid" Text="Bid it Now" />
                                                <asp:Literal runat="server" ID="lit_main_auction_help" Text=""></asp:Literal>
                                                <asp:Button runat="server" ID="btn_livebid_bidnow" CssClass="proxyBid" Text="Bid Now"
                                                    Visible="false" />
                                                <asp:Literal runat="server" ID="lit_livebid_help" Text=""></asp:Literal>
                                                <asp:Literal runat="server" ID="ltr_offer_button"></asp:Literal>
                                            </asp:Panel>
                                            <!---------------------------------------------------------->
                                            <div id="buynow-popup" style="display: none;">
                                                <h2>
                                                    <asp:Literal ID="ltr_buy_now_pop_heading" runat="server" Text=""></asp:Literal>
                                                </h2>
                                                <div class="inputboxes" style="width: 550px;">
                                                    <div class="fleft qnty">
                                                        <span class="text">I Want to buy</span>
                                                        <asp:DropDownList ID="ddl_buy_pop_qty" CssClass="slc typtxt" runat="server" AutoPostBack="false" />
                                                        <span class="text">unit(s) for </span>
                                                        <asp:HiddenField ID="hid_buy_now_pop_amount" runat="server" Value="0" />
                                                    </div>
                                                    <asp:Label runat="server" ID="lbl_popmsg_msg" CssClass="orange"></asp:Label>
                                                    <div class="fleft">
                                                        <span class="text">$ </span><span class="text">
                                                            <asp:Literal ID="ltr_buy_pop_amt" runat="server" Text=""></asp:Literal>/unit</span>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <p class="orange">
                                                    Please note that PCSWireless reserve the to cancel any order.
                                                </p>
                                                <p class="orange">
                                                    Rates are subject to change. Additional shipping and handing fees may apply
                                                </p>
                                                <p class="checkbox">
                                                    <asp:CheckBox ID="chk_accept_term" runat="server" Text=" Accept our Terms & Conditions" />
                                                </p>
                                                <div class="clear">
                                                </div>
                                                <asp:Button runat="server" ID="btn_buy_now" CssClass="fleft" Text="Buy Now" />
                                                <input type="button" name="" id="popupClose" class="fleft" value="Cancel" />
                                            </div>
                                            <div id="bidnow-popup" style="display: none;">
                                                <iframe runat="server" id="iframe_auction_window" scrolling="no" frameborder="1">
                                                </iframe>
                                            </div>
                                            <!---------------------------------------------------------->
                                            <div class="clear">
                                            </div>
                                            <p>
                                                <asp:Literal ID="ltr_bid_note" runat="server"></asp:Literal>
                                            </p>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="pnl_from_account">
                                        <asp:Literal runat="server" ID="ltr_offer_attachement"></asp:Literal>
                                        <asp:Literal runat="server" ID="ltr_current_status"></asp:Literal>
                                    </asp:Panel>
                                    <div class="clear">
                                    </div>
                                    <!--bidnow -->
                                </div>
                                <!--prodetail -->
                            </div>
                            <!--rightside -->
                            <div class="clear">
                            </div>
                        </div>
                        <!--detailbxwrap -->
                        <div class="clear">
                        </div>
                        <div class="middlerow">
                            <div class="askbx">
                                <span class="noprint">
                                    <asp:LinkButton runat="server" ID="lnk_my_auction" CausesValidation="false" CssClass="actn"
                                        CommandName="ins" Text="Add To My Auctions"></asp:LinkButton>
                                    <asp:Literal ID="lit_ask_question" runat="server" /></span>
                                <div class="clear">
                                </div>
                            </div>
                            <!--ask -->
                            <asp:Literal runat="server" ID="ltr_bidder_history" Text=""></asp:Literal>
                            <div class="clear">
                            </div>
                        </div>
                        <!--middlerow -->
                        <div id="history-popup" style="display: none;">
                            <div class="inputboxes">
                                <asp:Repeater ID="rep_bid_history" runat="server">
                                    <ItemTemplate>
                                        <div style="clear: both;">
                                            <div style="float: left; width: 180px; padding: 5px 0px 5px 10px;">
                                                <b>
                                                    <%# Eval("bidder")%>
                                                </b>
                                                <br />
                                                <span class="auctionSubTitle">
                                                    <%# Eval("email")%></span>
                                            </div>
                                            <div style="float: left; width: 100px; padding: 5px 0px 5px 10px; color: #006FD5;">
                                                <b>
                                                    <%# "$" & FormatNumber(Eval("bid_amount"), 2)%></b>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <div style="background-color: #F5F5F5; overflow: hidden; clear: both;">
                                            <div style="float: left; width: 180px; padding: 5px 0px 5px 10px;">
                                                <b>
                                                    <%# Eval("bidder")%>
                                                </b>
                                                <br />
                                                <span class="auctionSubTitle">
                                                    <%# Eval("email")%></span>
                                            </div>
                                            <div style="float: left; width: 100px; padding: 5px 0px 5px 10px; color: #006FD5;">
                                                <b>
                                                    <%# "$" & FormatNumber(Eval("bid_amount"), 2)%></b>
                                            </div>
                                        </div>
                                    </AlternatingItemTemplate>
                                </asp:Repeater>
                                <span class="text">
                                    <asp:Literal ID="empty_data" runat="server" Text="No bid submitted for this auction."></asp:Literal></span>
                            </div>
                            <div class="clear">
                            </div>
                            <input type="button" name="" id="historyClose" class="fleft" value="Cancel" />
                        </div>
                    </div>
                    <!--products -->
                    <div class="clear">
                    </div>
                    <!--items -->
                    <div runat="server" id="dv_items_details" class="details dtltxtbx">
                        <br>
                        <h1 class="pgtitle">
                            Items</h1>
                        <br />
                        <table width="100%">
                            <tr>
                                <th style="background-color: White;">
                                    <span class='txtx'>Manufacturer</span>
                                </th>
                                <th style="background-color: White;">
                                    <span class='txtx'>Title</span>
                                </th>
                                <th style="background-color: White;">
                                    <span class='txtx'>Quantity</span>
                                </th>
                                <th style="background-color: White;">
                                    <span class='txtx'>#</span>
                                </th>
                                <th style="background-color: White;">
                                    <span class='txtx'>MSRP</span>
                                </th>
                            </tr>
                            <tr>
                                <asp:Repeater ID="rptItems" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td style="background-color: White;" valign="top">
                                                <p>
                                                    <%# Eval("manufacturer")%></p>
                                            </td>
                                            <td style="background-color: White;" valign="top">
                                                <p>
                                                    <%# Eval("title")%></p>
                                            </td>
                                            <td style="background-color: White;" valign="top">
                                                <p>
                                                    <%# Eval("quantity")%></p>
                                            </td>
                                            <td style="background-color: White;" valign="top">
                                                <p>
                                                    Part:
                                                    <%# Eval("part_no")%><br />
                                                    UPC:
                                                    <%# Eval("UPC")%><br />
                                                    SKU:
                                                    <%# Eval("SKU")%><br />
                                                </p>
                                            </td>
                                            <td style="background-color: White;" valign="top">
                                                <p>
                                                    Estimated:
                                                    <%# CommonCode.GetFormatedMoney(Eval("estimated_msrp"))%><br />
                                                    Total :
                                                    <%# CommonCode.GetFormatedMoney(Eval("total_msrp"))%></p>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                        </table>
                        <!--text6 -->
                    </div>
                    <asp:Literal runat="server" ID="ltr_auction_details"></asp:Literal>
                    <!--details -->
                    <asp:Literal runat="server" ID="ltr_summary_detail"></asp:Literal>
                    <!--summrybx -->
                </div>
                <!--print content end -->
                <asp:Literal runat="server" ID="ltr_term_condition"></asp:Literal>
                <!--terms -->
            </div>
            <a id="openDummy" href="#bidnow-popup" style="display: none;"></a><a id="closeDummy"
                href="javascript:void(0);" style="display: none;"></a>
            <asp:Button ID="btn_history" runat="server" Style="display: none;" CommandName="history" />
            <asp:Button ID="btn_query" runat="server" Style="display: none;" CommandName="query" />
        </div>
    </asp:Panel>
    <script type="text/javascript">
        function refresh_history() {
            document.getElementById('<%=btn_history.ClientID%>').click();
            return true;
        }
        function refresh_query() {
            document.getElementById('<%=btn_query.ClientID%>').click();
            return true;
        }
        $('.btnship').click(function () { $('.shiping-methods').toggle(); });
        $("a#buynow").fancybox({ 'hideOnContentClick': true });
        $("a#openDummy").fancybox({ 'hideOnContentClick': true });
        $('#popupClose').click(function () { $.fancybox.close(); });
        $("a#question").fancybox({ 'hideOnContentClick': true });
        $('#questionClose').click(function () { $.fancybox.close(); });
        $("a#bidhistory").fancybox({ 'hideOnContentClick': true });
        $('#historyClose').click(function () { $.fancybox.close(); });
        $('#closeDummy').click(function () { $.fancybox.close(); });
        function openAuctionWindow() {
            $("a#openDummy").trigger('click');
            return false;
        }
        function closeAuctionWindow() {
            $.fancybox.close();
            return false;
        }
        function redirectConfirm(_qid, _prc, _aid, _type) {
            var ddq = document.getElementById(_qid);
            var _qty = 0;
            if (ddq != null)
                _qty = ddq.options[ddq.selectedIndex].value;
            else
                _qty = _qid;

            window.location.href = '/Auctionconfirmation.aspx?t=b&a=' + _aid + '&q=' + _qty + '&p=' + _prc + '&y=' + _type;
            return false;
        }
        function redirectBuyNow(_qid, _prc, _aid, _type) {
            var ddq = document.getElementById(_qid);
            var _qty = 0;
            if (ddq != null)
                _qty = ddq.options[ddq.selectedIndex].value;
            else
                _qty = _qid;

            var totamt = _prc * _qty

            window.location.href = '/Auctionconfirmation.aspx?t=b&a=' + _aid + '&q=' + _qty + '&m=' + totamt + '&y=' + _type;
            return false;
        }
        function changeShipping(bidAmt, shipAmt, hidOCtl, hidFCtl, hidSCtl, totalCtl) {

            document.getElementById(hidOCtl).value = 2;
            document.getElementById(hidFCtl).value = shipAmt;
            document.getElementById(hidSCtl).value = shipAmt;
            //document.getElementById(shipCtl).innerHTML = '$' + parseFloat(shipAmt).toFixed(2);
            document.getElementById(totalCtl).value = parseFloat(bidAmt) + parseFloat(shipAmt);
            //document.getElementById(totalCtl).innerHTML = 'Total:&nbsp;&nbsp;&nbsp;&nbsp;$' + ttlAmt.toFixed(2);
            return true;
        }




        function openBid(aucid, bidamt, auc_type, refr_id, shipOption, shipValue) {
            window.open('/AuctionBidPop.aspx?a=' + aucid + '&o=ti0i8u6Oh_l2RKo9llO2UQ==&t=' + auc_type + '&p=' + bidamt + '&r=' + refr_id + '&sOption=' + shipOption + '&sValue=' + shipValue, "_AuctionBidPop", "left=" + ((screen.width - 600) / 2) + ",top=" + ((screen.height - 420) / 2) + ",width=600,height=420,scrollbars=yes,toolbars=no,resizable=yes");
            window.focus();
            return false;
        }

        function setValueComp(valToComp) {
            document.getElementById('<%=hid_accept_bid_amt.ClientID %>').value = valToComp;
        }
        function openAuctionBid(aucid, auc_type, txt_max_bid_amt, ddl_shipping, is_proxy_bid, is_fixed_shipping, shipping_amount) {
            var bidamt;
            var bidamtperunit;
            var strSel;
            var sValue;
            var valToComp;
            RecalculateTotal();
           
            bidamt = document.getElementById(txt_max_bid_amt).value;
            var perunit = document.getElementById('<%=hid_quantity.ClientID %>').value;

            if ((bidamt) && (parseInt(perunit) > 1)) {
                bidamtperunit =  parseInt(perunit);                
            }
            
            valToComp = document.getElementById('<%=hid_accept_bid_amt.ClientID %>').value;
            var sp_help = document.getElementById('spn_amt_hlp');
            var sp_valid = document.getElementById('spn_valid');


            sp_valid.innerHTML = '';
            if (sp_help.style.display != 'none') {
                sp_help.style.display = 'none';
                //alert(parseFloat(bidamt) < parseFloat(valToComp));
            }
            if (sp_valid.style.display != 'none') {
                sp_valid.style.display = 'none';
                //alert(parseFloat(bidamt) < parseFloat(valToComp));
            }

            if (bidamt == '' || bidamt == '0' || bidamt == 'Enter Your Bid Amount') {
                sp_valid.style.display = '';
                sp_valid.innerHTML = 'Amount Required';
            }

            else {

                if (isNaN(bidamt)) {
                    sp_valid.style.display = '';
                    sp_valid.innerHTML = 'Invalid Amount';
                }

                else {
                    if (parseFloat(bidamt) < parseFloat(valToComp)) {
                        sp_valid.style.display = '';
                        sp_valid.innerHTML = 'Amount not Accepted';
                        sp_help.style.display = '';
                    }
                    else {

                        if (is_fixed_shipping == '1') {
                            sValue = shipping_amount;
                            strSel = 'The shipping cost for this lot will be $' + shipping_amount;
                           
                            w = window.open('/AuctionBidPop.aspx?a=' + aucid + '&o=ti0i8u6Oh_l2RKo9llO2UQ==&t=' + auc_type + '&p=' + bidamt + '&q=' + bidamtperunit.toFixed(2) +  '&sOption=' + strSel + '&sValue=' + sValue + '&setProxy=' + is_proxy_bid, "_AuctionBidPop", "left=" + ((screen.width - 600) / 2) + ",top=" + ((screen.height - 600) / 2) + ",width=600,height=600,scrollbars=yes,toolbars=no,resizable=yes");
                            w.focus();
                        }
                        else {

                            var e = document.getElementById(ddl_shipping);
                            strSel = e.options[e.selectedIndex].value; //  + " and text is: " + e.options[e.selectedIndex].text;
                            strSel = strSel.replace(',', '');
                            if (strSel == '0') {
                                sp_valid.style.display = '';
                                sp_valid.innerHTML = 'Please Select Shipping';
                            }
                            else {
                                if (strSel == '1' || strSel == '2')
                                    sValue = 0;
                                else {
                                    if (!isNaN(strSel))
                                        sValue = strSel;
                                    else
                                        sValue = 0;
                                }

                                strSel = e.options[e.selectedIndex].text;
                                
                                w = window.open('/AuctionBidPop.aspx?a=' + aucid + '&o=ti0i8u6Oh_l2RKo9llO2UQ==&t=' + auc_type + '&p=' + bidamt +  '&q=' + bidamtperunit.toFixed(2) + '&sOption=' + strSel + '&sValue=' + sValue + '&setProxy=' + is_proxy_bid, "_AuctionBidPop", "left=" + ((screen.width - 600) / 2) + ",top=" + ((screen.height - 600) / 2) + ",width=600,height=600,scrollbars=yes,toolbars=no,resizable=yes");
                                w.focus();
                            }

                        }


                    }
                }
            }
            return false;
        }


        function askquestion_wait(_valdt_grp, _aid) {
            if (Page_ClientValidate(_valdt_grp)) {
                var dv_ask = document.getElementById('dv_' + _aid + '_ask');
                var dv_msg = document.getElementById('dv_' + _aid + '_msg');
                var dv_cnf = document.getElementById('dv_' + _aid + '_confirm');
                if (dv_ask.style.display == 'block') {
                    dv_ask.style.display = 'none';
                    dv_cnf.style.display = 'none';
                    dv_msg.style.display = 'block';
                }
                else {
                    dv_ask.style.display = 'block';
                    dv_cnf.style.display = 'block';
                    dv_msg.style.display = 'none';
                }
                return true;
            }
            else
                return false;
        }

        function open_pop_win(_path, _id) {
            w = window.open(_path + '?i=' + _id, '_history', 'top=' + ((screen.height - 400) / 2) + ', left=' + ((screen.width - 324) / 2) + ', height=400, width=324,scrollbars=yes,toolbars=no,resizable=1;');
            w.focus();
            return false;
        }
        function openOffer(aucid, buy_option, auc_type, bidamt) {
            w = window.open('/AuctionBidPop.aspx?a=' + aucid + '&o=' + buy_option + '&t=' + auc_type + '&p=' + bidamt, "_AuctionBidPop", "left=" + ((screen.width - 600) / 2) + ",top=" + ((screen.height - 420) / 2) + ",width=600,height=420,scrollbars=yes,toolbars=no,resizable=yes");
            w.focus();
            return false;
        }
        function openAskQuestion(aucid) {
            w = window.open('/AuctionQuestion.aspx?i=' + aucid, "_AuctionQuestion", "left=" + ((screen.width - 600) / 2) + ",top=" + ((screen.height - 480) / 2) + ",width=600,height=480,scrollbars=yes,toolbars=no,resizable=yes");
            w.focus();
            return false;
        }


        function CalculateTotal() {
            
                var txtAmount = $get("<%=txt_max_bid_amt.ClientID%>").value;
                var txtQuantity = $get("<%=hid_quantity.ClientID%>").value;
                var total

                if ((txtAmount) && (parseInt(txtQuantity) > 1)) {
                    total = parseFloat(txtAmount) * parseInt(txtQuantity);
                    if (!isNaN(total) ){
                        document.getElementById('<%=txt_max_bid_amt.ClientID%>').value = total.toFixed(2);
                        document.getElementById('<%=lotMsg.ClientID%>').style.display = 'block';                 
                        document.getElementById('<%=LPerUnitTotal.ClientID%>').innerHTML = 'Per Unit: ' + txtAmount;
                    }
                    else {
                        document.getElementById('<%=txt_max_bid_amt.ClientID%>').value = '';
                        document.getElementById('<%=LPerUnitTotal.ClientID%>').innerHTML = '';
                    }
                }
            }
        

        function CalculatePerUnit() {
           
                var txtAmount = $get("<%=txt_max_bid_amt.ClientID%>").value;
                var txtQuantity = $get("<%=hid_quantity.ClientID%>").value;
                var perunit

                if ((txtAmount) && (parseInt(txtQuantity) > 1)) {
                    perunit = parseFloat(txtAmount) / parseInt(txtQuantity);
                    if (!isNaN(perunit) ) {
                        document.getElementById('<%=txt_max_bid_amt.ClientID%>').value = perunit.toFixed(2);                       
                        document.getElementById('<%=lotMsg.ClientID%>').style.display = 'none';                    
                        document.getElementById('<%=LPerUnitTotal.ClientID%>').innerHTML = 'Total: ' + txtAmount;
                    }
                    else
                    {
                        document.getElementById('<%=txt_max_bid_amt.ClientID%>').value = '';
                        document.getElementById('<%=LPerUnitTotal.ClientID%>').innerHTML = ''
                    }
                }
        }

        function RecalculateTotal() {                             
            if (document.getElementById('<%=rb_partial_bid_amt.ClientID%>').checked) {                               
                document.getElementById('<%=rb_total_bid_amt.ClientID%>').checked = true;
                CalculateTotal();
            }        
            }        

        function recalculatePerUnitTotal() {            
            var txtAmount = $get("<%=txt_max_bid_amt.ClientID%>").value;
            var txtQuantity = $get("<%=hid_quantity.ClientID%>").value;
            var perunitTotal
            var txtdescription
            document.getElementById('<%=LPerUnitTotal.ClientID%>').innerHTML = '';

            if ((txtAmount) && (parseInt(txtQuantity) > 1)) {
                if (document.getElementById('<%=rb_partial_bid_amt.ClientID%>').checked) {
                    txtdescription = 'Total: ';
                    perunitTotal = parseFloat(txtAmount) * parseInt(txtQuantity);                   
                }
                else {
                    perunitTotal = parseFloat(txtAmount) / parseInt(txtQuantity);
                    txtdescription = 'Per Unit: ';
                }
                if (!isNaN(perunitTotal)) {
                    document.getElementById('<%=LPerUnitTotal.ClientID%>').innerHTML = txtdescription + perunitTotal.toFixed(2);
                }                
            }
        }
       
    </script>
</asp:Content>
