USE [PCS_Auction]
GO

/****** Object:  StoredProcedure [dbo].[get_auction_byid]    Script Date: 11/12/2014 11:04:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_auction_byid]
(
	-- Add the parameters for the stored procedure here
	@auction_id   int =  0, 
	@b_user_id  int= 0
	)
	AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT A.auction_id,
          ISNULL(A.code, '') AS code,
          ISNULL(A.title, '') AS title,
          ISNULL(A.sub_title, '') AS sub_title,
          ISNULL(A.product_catetory_id, 0) AS product_catetory_id,
          ISNULL(A.stock_condition_id, 0) AS stock_condition_id,
          ISNULL(A.auction_category_id, 0) AS auction_category_id,
          ISNULL(A.auction_type_id, 0) AS auction_type_id,
          ISNULL(A.is_private_offer, 0) AS is_private_offer,
          ISNULL(A.is_partial_offer, 0) AS is_partial_offer,
          ISNULL(A.auto_acceptance_price_private, 0) AS auto_acceptance_price_private,
          ISNULL(A.auto_acceptance_price_partial, 0) AS auto_acceptance_price_partial,
          ISNULL(A.auto_acceptance_quantity, 0) AS auto_acceptance_quantity,
          ISNULL(A.no_of_clicks, 0) AS no_of_clicks,
          ISNULL(A.start_price, 0) AS start_price,
          ISNULL(A.reserve_price, 0) AS reserve_price,
          ISNULL(A.thresh_hold_value, 0) AS thresh_hold_value,
          ISNULL(A.is_show_reserve_price, 0) AS is_show_reserve_price,
          ISNULL(A.is_show_actual_pricing, 0) AS is_show_actual_pricing,
          ISNULL(A.is_show_no_of_bids, 0) AS is_show_no_of_bids,
          A.use_pcs_shipping, A.use_your_shipping,
          ISNULL(A.increament_amount, 0) AS increament_amount,
          ISNULL(A.buy_now_price, 0) AS buy_now_price,
          ISNULL(A.is_buy_it_now, 0) AS is_buy_it_now,
          ISNULL(A.request_for_quote_message, '') AS request_for_quote_message,
          ISNULL(A.show_price, 0) AS show_price,
          ISNULL(A.short_description, '') AS short_description,
          ISNULL(A.total_qty, 0) AS total_qty,
          ISNULL(A.qty_per_bidder, 0) AS qty_per_bidder,
          dbo.get_auction_status(A.auction_id) as auction_status,    
          
              
              case when exists(select favourite_id from tbl_auction_favourites where auction_id=A.auction_id and buyer_user_id= @b_user_id) then 1 else 0 end as in_fav,
        
          ISNULL(IMG.stock_image_id, 0) AS stock_image_id,ISNULL(IMG.filename, '') AS filename,
          
          
          
          isnull(Stuff((Select ',' +  V.bucket_value From tbl_master_bucket_values V inner join tbl_auction_invitation_filter_values F on V.bucket_value_id=F.bucket_value_id Where  F.auction_id =A.auction_id  FOR XML PATH('')),1,2,''), '') as bucket_value_for_filter
          
          
           FROM 
          tbl_auctions A	 left join tbl_master_stock_locations L 
							 on A.stock_location_id=L.stock_location_id left join tbl_master_stock_conditions B 
							 on A.stock_condition_id=B.stock_condition_id left join tbl_auction_stock_images IMG 
							 on A.auction_id=IMG.auction_id and IMG.stock_image_id=(SELECT top 1 stock_image_id from tbl_auction_stock_images 
							 where auction_id=a.auction_id order by position) 
           WHERE A.auction_id = @auction_id
           
         
        
END

GO

