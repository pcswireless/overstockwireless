﻿<%@ Page Language="VB" AutoEventWireup="false" EnableViewState="false" CodeFile="frame_auction_time.aspx.vb" Inherits="frame_auction_time" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript">

        /***********************************************
        * Different CSS depending on OS (mac/pc)- © Dynamic Drive (www.dynamicdrive.com)
        * This notice must stay intact for use
        * Visit http://www.dynamicdrive.com/ for full source code
        ***********************************************/
        ///////No need to edit beyond here////////////

        var mactest = navigator.userAgent.indexOf("Mac") != -1


        if (mactest) {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet_mac.css"/>');
        }
        else {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet.css"/>');
        }
  </script>
    <style type="text/css">
        .lcdstyle1
        {
            color: green;
            font-family:dinregular;
            white-space:nowrap;
            text-align:center;
            font-size: 27px;
        }
        .lcdstyle2
        {
            color: orange;
            font-family:dinregular;
            white-space:nowrap;
            text-align:left;
        }
        .lcdstyle3
        {
            color: red;
            font-family:dinregular;
            white-space:nowrap;
            text-align:left;
            font-size: 27px;
        }
        .msg1
        {
            color: red;
            font-family:dinregular;
            white-space:nowrap;
            text-align:left;
            font-size: 12px;
        }
        
        .lcdstyle sup
        {
            /*Example CSS to create LCD countdown look
            font-size: 80%;*/
        }
        .currentRank
        {
            padding: 4px 0px 15px 0px;
            font-family:dinregular;
            color: #18472C;
            font-weight: bold;
            font-size: 12px;
            white-space:nowrap;
        }
.frame_bg
{
    /*background-color: #F4F3F0;*/
    font-size: 12px;
    color: Green;
    font-weight: bold;
    width: 200px;
    height: 40px;
    display: block;
    text-align: left;
    vertical-align: middle;
    font-family: Verdana;
}
.txt2 
{
    color: #24B4E8;
    font-family: 'dinregular';
    font-size: 27px;
    font-weight: normal;
    line-height: 24px;
}
    </style>
    <script type="text/javascript">

        /***********************************************
        * Universal Countdown script- © Dynamic Drive (http://www.dynamicdrive.com)
        * This notice MUST stay intact for legal use
        * Visit http://www.dynamicdrive.com/ for this script and 100s more.
        ***********************************************/

        function cdLocalTime(container, servermode, offsetMinutes, targetdate, debugmode) {
            if (!document.getElementById || !document.getElementById(container)) return
            this.container = document.getElementById(container)
            var servertimestring = (servermode == "server-php") ? '<? print date("F d, Y H:i:s", time())?>' : (servermode == "server-ssi") ? '<!--#config timefmt="%B %d, %Y %H:%M:%S"--><!--#echo var="DATE_LOCAL" -->' : '<%= Now() %>'
            this.localtime = this.serverdate = new Date(servertimestring)
            this.targetdate = new Date(targetdate)
            this.debugmode = (typeof debugmode != "undefined") ? 1 : 0
            this.timesup = false
            this.localtime.setTime(this.serverdate.getTime() + offsetMinutes * 60 * 1000) //add user offset to server time
            this.updateTime()
        }

        cdLocalTime.prototype.updateTime = function () {
            var thisobj = this
            this.localtime.setSeconds(this.localtime.getSeconds() + 1)
            setTimeout(function () { thisobj.updateTime() }, 1000) //update time every second
        }

        cdLocalTime.prototype.displaycountdown = function (baseunit, functionref) {
            this.baseunit = baseunit
            this.formatresults = functionref
            this.showresults()
        }

        cdLocalTime.prototype.showresults = function () {
            var thisobj = this
            var debugstring = (this.debugmode) ? "<p style=\"background-color: #FCD6D6; color: black; padding: 0px\"><big>Debug Mode on!</big><br /><b>Current Local time:</b> " + this.localtime.toLocaleString() + "<br />Verify this is the correct current local time, in other words, time zone of count down date.<br /><br /><b>Target Time:</b> " + this.targetdate.toLocaleString() + "<br />Verify this is the date/time you wish to count down to (should be a future date).</p>" : ""

            var timediff = (this.targetdate - this.localtime) / 1000 //difference btw target date and current date, in seconds
            if (timediff < 0) { //if time is up
                this.timesup = true
                this.container.innerHTML = debugstring + this.formatresults()
                return
            }
            var oneMinute = 60 //minute unit in seconds
            var oneHour = 60 * 60 //hour unit in seconds
            var oneDay = 60 * 60 * 24 //day unit in seconds
            var dayfield = Math.floor(timediff / oneDay)
            var hourfield = Math.floor((timediff - dayfield * oneDay) / oneHour)
            var minutefield = Math.floor((timediff - dayfield * oneDay - hourfield * oneHour) / oneMinute)
            var secondfield = Math.floor((timediff - dayfield * oneDay - hourfield * oneHour - minutefield * oneMinute))
            if (this.baseunit == "hours") { //if base unit is hours, set "hourfield" to be topmost level
                hourfield = dayfield * 24 + hourfield
                dayfield = "n/a"
            }
            else if (this.baseunit == "minutes") { //if base unit is minutes, set "minutefield" to be topmost level
                minutefield = dayfield * 24 * 60 + hourfield * 60 + minutefield
                dayfield = hourfield = "n/a"
            }
            else if (this.baseunit == "seconds") { //if base unit is seconds, set "secondfield" to be topmost level
                var secondfield = timediff
                dayfield = hourfield = minutefield = "n/a"
            }
            this.container.innerHTML = debugstring + this.formatresults(dayfield, hourfield, minutefield, secondfield)
            setTimeout(function () { thisobj.showresults() }, 1000) //update results every second
        }

        /////CUSTOM FORMAT OUTPUT FUNCTIONS BELOW//////////////////////////////

        //Create your own custom format function to pass into cdLocalTime.displaycountdown()
        //Use arguments[0] to access "Days" left
        //Use arguments[1] to access "Hours" left
        //Use arguments[2] to access "Minutes" left
        //Use arguments[3] to access "Seconds" left

        //The values of these arguments may change depending on the "baseunit" parameter of cdLocalTime.displaycountdown()
        //For example, if "baseunit" is set to "hours", arguments[0] becomes meaningless and contains "n/a"
        //For example, if "baseunit" is set to "minutes", arguments[0] and arguments[1] become meaningless etc

        //1) Display countdown using plain text
        function formatresults() {
            if (this.timesup == false) {//if target date/time not yet met
                var displaystring = "<span style='background-color: #CFEAFE'>" + arguments[1] + " hours " + arguments[2] + " minutes " + arguments[3] + " seconds</span> left until launch time"
            }
            else { //else if target date/time met
                var displaystring = "Launch time!"
            }
            return displaystring
        }

        //2) Display countdown with a stylish LCD look, and display an alert on target date/time
        function formatresults2() {
            if (this.timesup == false) { //if target date/time not yet met
                var displaystring;
                if (arguments[0] <= 0) {
                    if (arguments[1] <= 0) {
                        if (arguments[2] < 2)
                            displaystring = "<span style='color:red;' class='txt2'>"
                        else if (arguments[2] < 10)
                            displaystring = "<span style='color:orange;' class='txt2'>"
                        else {
                            //displaystring = "<span class='lcdstyle1'>"
                            displaystring = "<span class='txt2'>"
                        }
                    }
                    else {
                        //displaystring = "<span class='lcdstyle1'>"
                        displaystring = "<span class='txt2'>"
                    }
                }
                else
                {
                    //displaystring = "<span class='lcdstyle1'>"
                    displaystring = "<span class='txt2'>"
                }    
                    //for days format
            //displaystring = displaystring + arguments[0] + "D " + arguments[1] + "H " + arguments[2] + "M " + arguments[3] + "S</span>"
               // alert(arguments[1].toString.length);
            var str0 = (arguments[0]>=10) ? '' : '0';//'': ((arguments[0] > 9 && arguments[0] < 100) ? '0' : '00');
            var str1 = (arguments[1]>= 10) ? '' : '0';
            var str2= (arguments[2] >= 10) ? '' : '0';
            var str3 = (arguments[3] >= 10) ? '' : '0';
            var fl = ('<%=Request("p") %>' == 'd') ? '29px' : '20px';
            var fs = ('<%=Request("p") %>' == 'd') ? '17px' : '13px';
            var wl =  ('<%=Request("p") %>' == 'd') ? '19%' : '14%';
            var w =  ('<%=Request("p") %>' == 'd') ? '19%' : '14%';
            var ws =  ('<%=Request("p") %>' == 'd') ? '20%' : '15%';
            var wxs = ('<%=Request("p") %>' == 'd') ? '18%' : '14%';
                //for hours format

            displaystring = displaystring + "<div class='auc_timer' style='width:" + wl + ";height:100%;float:left;font-size:" + fl + ";'>" + str0 + arguments[0] + ":<br><span class='auc_timer' style='color:#686868;font-size:" + fs + "; '>day</span></div>" + "<div class='auc_timer' style='width:" + w + ";height:100%;float:left;font-size:" + fl + "; '>" + str1 + arguments[1] + ":<br><span class='auc_timer' style='color:#686868;font-size:" + fs + ";'>hrs</span></div>" + "<div class='auc_timer' style='width:" + ws + ";height:100%;float:left;font-size:" + fl + ";'>" + str2 + arguments[2] + ":<br><span class='auc_timer' style='color:#686868;font-size:" + fs + ";'>min</span></div>" + "<div class='auc_timer' style='width:" + wxs + ";height:100%;float:left;font-size:" + fl + ";'>" + str3 + arguments[3] + "<br><span class='auc_timer' style='color:#686868;font-size:" + fs + ";'>sec</span></div></span>";
                
            }
            else { //else if target date/time met
                var displaystring = "<span class='lcdstyle3'></span>"; //Don't display any text
                
                //alert("Launch time!") //Instead, perform a custom alert
            }
            return displaystring
        }

    </script>
</head>
<body style="margin: 0; padding: 0;background:white;font-family: dinregular; " class="frame_bg">
    <form id="form1" runat="server">
    <asp:Literal ID="litRefresh" runat="server">
    <script type="text/javascript">        E_refresh = window.setTimeout(function () { window.location.href = window.location.href }, 15000);</script>
    </asp:Literal>
    
    <div id="cdcontainer" style="margin:0;padding:0;text-align:left;">
    </div>
    <asp:Literal runat="server" ID="lit_span"></asp:Literal>
    
  
    </form>
</body>
</html>
