﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AuctionMain.master" AutoEventWireup="false"
    CodeFile="My_Account.aspx.vb" Inherits="My_Account" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="~/UserControls/My_Auctions.ascx" TagName="AuctionList" TagPrefix="UC1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
    <link href="style/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="/style/bootstrap.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="js/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="js/jquery.ui.tabs.js"></script>
    <script type="text/javascript">
        function open_pass_win(_path, _pwd) {
            w1 = window.open(_path + '?' + _pwd, '_paswd', 'top=' + ((screen.height - 400) / 2) + ', left=' + ((screen.width - 600) / 2) + ', height=400, width=600,scrollbars=yes,toolbars=no,resizable=1;');
            w1.focus();
            return false;

        }
        function open_bucket_assign(_buyer_encrypted) {
            w1 = window.open('/Bidder_Buckets.aspx?b=' + _buyer_encrypted + '&pop=1', '_Bidder_Buckets', 'top=' + ((screen.height - 600) / 2) + ', left=' + ((screen.width - 800) / 2) + ', height=600, width=800,scrollbars=yes,toolbars=no,resizable=1;');
            w1.focus();
            return false;

        }
        function refresh_history() {
            document.getElementById('<%=btn_history.ClientID%>').click();
            return true;
        }
        function refresh_buckets() {
            document.getElementById('<%=btn_refresh_buckets.ClientID%>').click();
            return true;
        }
    </script>
    <script type="text/javascript">

        function Opendetail(id, id1) {

            var width = "700";
            var height = "500";

            var left = (screen.width - width) / 2;
            var top = (screen.height - height) / 2;

            window.open('/AddEditSubLogin.aspx?u=' + id + "&s=" + id1, '_AddEditSubLogin', 'left=' + left + ',top=' + top + ',width=' + width + ',height=' + height + ',status=1,scrollbars=1')
            window.focus();
            return false;

        }
        function Opendetailnew(id) {

            var width = "700";
            var height = "500";

            var left = (screen.width - width) / 2;
            var top = (screen.height - height) / 2;

            window.open('/AddEditSubLogin.aspx?u=' + id, '_AddEditSubLogin1', 'left=' + left + ',top=' + top + ',width=' + width + ',height=' + height + ',status=1,scrollbars=1')
            window.focus();
            return false;

        }
        function check_comment(source, arguments) {
            if (arguments.Value.replace(/\n/g, "\n\r").length < 5000) {
                arguments.IsValid = true;
            }
            else {
                source.innerHTML = "<br />The length of text you entered is " + arguments.Value.replace(/\n/g, "\n\r").length + ". It must not be more than 5000 chars."
                arguments.IsValid = false;
            }
        }
           
    </script>
    <asp:HiddenField ID="hd_buyer_id" runat="server" Value="0" />
    <asp:HiddenField ID="hd_buyer_user_id" runat="server" Value="0" />
    <asp:HiddenField ID="hd_is_buyer" runat="server" Value="0" />
    <asp:HiddenField ID="hid_flang" runat="server" Value="0" />
    <asp:HiddenField ID="hid_buyer_max_bid_amount" runat="server" Value="0" />
    <asp:Button ID="btn_history" runat="server" Style="display: none;" CommandName="history" />
    <div class="tabs">
        <div class="innerwraper wraper1">
            <div class="products1">
                <div class="printbx" style="visibility: hidden;">
                    <ul>
                        <li class="print"><a href="#">Print</a></li>
                    </ul>
                </div>
                <!--print -->
                <div class="clear">
                </div>
                <div class="seprator">
                </div>
                <!--seprator -->
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1">My Information</a></li>
                        <li><a href="#tabs-2">Auction Won</a></li>
                        <li><a href="#tabs-3">My Offers</a></li>
                        <li><a href="#tabs-4">Private Offers</a></li>
                        <li><a href="#tabs-5">Partial Offers</a></li>
                        <li><a href="#tabs-6">Buy It Now</a></li>
                        <li runat="server" id="pnl_edit_mode"><a href="#tabs-7">Sub Logins</a></li>
                    </ul>
                    <div id="tabs-1">
                        <h1>
                            Account Details</h1>
                        <h2>
                            Basic Information</h2>
                        <asp:Panel Visible="true" runat="server" ID="pnl_buyer">
                            <ul class="formm1">
                                <li>
                                    <lable>Company&nbsp;<span id="span_company_name" runat="server" class="req_star">*</span></lable>
                                    <asp:TextBox ID="txt_company" runat="server" CssClass="typtxt"></asp:TextBox>
                                    <asp:Label ID="lbl_company" runat="server"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rfv_company" runat="server" ControlToValidate="txt_company"
                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Company Required"
                                        CssClass="error"></asp:RequiredFieldValidator>
                                </li>
                                <li>
                                    <lable>Company Website</lable>
                                    <asp:TextBox ID="txt_website" runat="server" CssClass="typtxt"></asp:TextBox>
                                    <asp:Label ID="lbl_website" runat="server"></asp:Label>
                                </li>
                            </ul>
                            <div class="clear">
                            </div>
                            <h2>
                                Contact Details</h2>
                            <ul class="formm1">
                                <li>
                                    <lable>First Name&nbsp;<span id="span_first_name" runat="server" class="req_star">*</span></lable>
                                    <asp:TextBox ID="txt_first_name" runat="server" CssClass="typtxt"></asp:TextBox>
                                    <asp:Label ID="lbl_first_name" runat="server"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rfv_contact_name" runat="server" ControlToValidate="txt_first_name"
                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />First Name Required"
                                        CssClass="error"></asp:RequiredFieldValidator>
                                </li>
                                <li>
                                    <lable>Last Name</lable>
                                    <asp:TextBox ID="txt_last_name" runat="server" CssClass="typtxt"></asp:TextBox>
                                    <asp:Label ID="lbl_last_name" runat="server"></asp:Label>
                                </li>
                                <li>
                                    <lable>Title</lable>
                                    <asp:DropDownList ID="ddl_title" runat="server" DataTextField="name" DataValueField="name"
                                        CssClass="slc typtxt">
                                    </asp:DropDownList>
                                    <asp:Label ID="lbl_title" runat="server"></asp:Label>
                                </li>
                                <li class="blank-li"></li>
                                <li>
                                    <lable>Street Address 1&nbsp;<span id="span_address1" runat="server" class="req_star">*</span></lable>
                                    <asp:TextBox ID="txt_address1" runat="server" CssClass="typtxt"></asp:TextBox>
                                    <asp:Label ID="lbl_address1" runat="server"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rfv_address1" runat="server" ControlToValidate="txt_address1"
                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Street Required"
                                        CssClass="error"></asp:RequiredFieldValidator>
                                </li>
                                <li>
                                    <lable>Street Address 2</lable>
                                    <asp:TextBox ID="txt_address2" runat="server" CssClass="typtxt"></asp:TextBox>
                                    <asp:Label ID="lbl_address2" runat="server"></asp:Label>
                                </li>
                                <li>
                                    <lable>City&nbsp;<span id="span_city" runat="server" class="req_star">*</span></lable>
                                    <asp:TextBox ID="txt_city" runat="server" CssClass="typtxt"></asp:TextBox>
                                    <asp:Label ID="lbl_city" runat="server"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rfv_city" runat="server" ControlToValidate="txt_city"
                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />City Required"
                                        CssClass="error"></asp:RequiredFieldValidator>
                                </li>
                                <li>
                                    <lable>Country&nbsp;<span id="span_country" runat="server" class="req_star">*</span></lable>
                                    <asp:DropDownList ID="ddl_country" runat="server" DataTextField="name" DataValueField="country_id"
                                        CssClass="slc typtxt">
                                    </asp:DropDownList>
                                    <asp:Label ID="lbl_country" runat="server"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rfv_country" runat="server" ControlToValidate="ddl_country"
                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Country Required"
                                        CssClass="error"></asp:RequiredFieldValidator>
                                </li>
                                <li>
                                    <lable>State/ Province&nbsp;<span id="span_state" runat="server" class="req_star">*</span></lable>
                                    <asp:TextBox ID="txt_other_state" runat="server" MaxLength="55" CssClass="typtxt"></asp:TextBox>
                                    <asp:Label ID="lbl_state" runat="server"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rfv_txt_state" runat="server" ControlToValidate="txt_other_state"
                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />State Required"
                                        CssClass="error"></asp:RequiredFieldValidator>
                                </li>
                                <li>
                                    <lable>Postal Code&nbsp;<span id="span_zip" runat="server" class="req_star">*</span></lable>
                                    <asp:TextBox ID="txt_zip" runat="server" CssClass="typtxt" MaxLength="15"></asp:TextBox>
                                    <asp:Label ID="lbl_zip" runat="server"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rfv_zip" runat="server" ControlToValidate="txt_zip"
                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Postal Code Required"
                                        CssClass="error"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txt_zip"
                                        ValidationExpression="^[a-zA-Z0-9 ]*$" EnableClientScript="true" Display="Dynamic"
                                        ErrorMessage="<br />Invalid Postal Code" runat="server" ValidationGroup="val_basic_info" />
                                </li>
                                <li>
                                    <lable>Phone 1&nbsp;<span id="span_mobile" runat="server" class="req_star">*</span></lable>
                                    <asp:Label ID="lbl_mobile" runat="server"></asp:Label>
                                    <asp:TextBox ID="txt_mobile" runat="server" CssClass="typtxt" MaxLength="15"></asp:TextBox>
                                    <p style="clear: left; line-height: normal;">
                                        <asp:RadioButton ID="rdo_ph_1_landline" runat="server" Text="Landline" GroupName="p1"
                                            Checked="true" Font-Bold="false" /><asp:RadioButton ID="rdo_ph_1_mobile" runat="server"
                                                Text="Mobile" GroupName="p1" Font-Bold="false" />
                                    </p>
                                    <asp:RequiredFieldValidator ID="rfv_mobile" runat="server" ControlToValidate="txt_mobile"
                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Phone 1 Required"
                                        CssClass="error"></asp:RequiredFieldValidator>
                                </li>
                                <li>
                                    <lable>Phone 2</lable>
                                    <asp:Label ID="lbl_phone" runat="server"></asp:Label>
                                    <asp:TextBox ID="txt_phone" runat="server" CssClass="typtxt" MaxLength="15"></asp:TextBox>
                                    <p style="clear: left; line-height: normal;">
                                        <asp:RadioButton ID="rdo_ph_2_landline" runat="server" Text="Landline" GroupName="p2"
                                            Checked="true" Font-Bold="false" /><asp:RadioButton ID="rdo_ph_2_mobile" runat="server"
                                                Text="Mobile" GroupName="p2" Font-Bold="false" />
                                    </p>
                                </li>
                                <li>
                                    <lable>Email&nbsp;<span id="span_email" runat="server" class="req_star">*</span></lable>
                                    <asp:TextBox ID="txt_email" runat="server" CssClass="typtxt"></asp:TextBox>
                                    <asp:Label ID="lbl_email" runat="server"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rfv_email" runat="server" ControlToValidate="txt_email"
                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />Email Required"
                                        CssClass="error"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ValidationGroup="val_basic_info" ID="rev_email" runat="server"
                                        ControlToValidate="txt_email" Display="Dynamic" ErrorMessage="<br />Invalid Email"
                                        ValidationExpression="^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$"
                                        CssClass="error"></asp:RegularExpressionValidator>
                                </li>
                                <li>
                                    <lable>Fax</lable>
                                    <asp:TextBox ID="txt_fax" runat="server" CssClass="typtxt" MaxLength="15"></asp:TextBox>
                                    <asp:Label ID="lbl_fax" runat="server"></asp:Label>
                                </li>
                            </ul>
                            <div class="clear">
                            </div>
                            <h2>
                                Account Setup</h2>
                            <ul class="formm1">
                                <li>
                                    <lable>User ID&nbsp;<span id="span_user_id" runat="server" class="req_star">*</span></lable>
                                    <asp:TextBox ID="txt_user_id" runat="server" CssClass="typtxt"></asp:TextBox>
                                    <asp:Label ID="lbl_user_id" runat="server"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rfv_user_id" runat="server" ControlToValidate="txt_user_id"
                                        ValidationGroup="val_basic_info" Display="Dynamic" ErrorMessage="<br />User ID Required"
                                        CssClass="error"></asp:RequiredFieldValidator>
                                </li>
                                <li>
                                    <lable>Password&nbsp;<span id="span_password" runat="server" class="req_star">*</span></lable>
                                    <asp:HiddenField ID="hid_password" runat="server" />
                                    <asp:Literal ID="lit_change_password" runat="server"></asp:Literal>
                                    <asp:Label ID="lbl_password" runat="server"></asp:Label>
                                </li>
                                <li>
                                    <%-- <lable style="visibility:hidden">Comments</lable>--%>
                                    <asp:Label ID="lbl_comment_caption" runat="server" Text="Comments" Visible="false"
                                        Style="color: #5C5C5C; display: block; font-family: 'dinregular',arial; font-size: 16px;
                                        font-weight: bold; padding: 0 0 6px;"></asp:Label>
                                    <asp:TextBox ID="txt_comment" runat="server" CssClass="typtxt" Rows="5" Columns="50"
                                        TextMode="MultiLine" MaxLength="5000"></asp:TextBox>
                                    <asp:Label ID="lbl_comment" runat="server"></asp:Label>
                                    <asp:CustomValidator ID="cust_comment" runat="server" ForeColor="Red" ControlToValidate="txt_comment"
                                        CssClass="error" Display="Dynamic" ValidationGroup="val_basic_info" ClientValidationFunction="check_comment"></asp:CustomValidator>
                                </li>
                            </ul>
                            <div class="clear">
                            </div>
                            <h2>
                                Other Details</h2>
                            <ul class="formm1">
                                <li>
                                    <lable>Buckets Assign</lable>
                                    <asp:Button ID="btn_refresh_buckets" runat="server" Style="display: none;" Text="Refresh" />
                                    <asp:Literal ID="ltrl_buckets" runat="server"></asp:Literal>
                                    <br />
                                    <asp:Literal ID="ltrl_bucket_link" runat="server"></asp:Literal>
                                </li>
                                <%--<li>
                                    <lable>Industry Type</lable>
                                    <asp:CheckBoxList ID="chklst_industry_types" Font-Bold="false" runat="server" CellPadding="0"
                                        CellSpacing="0" BorderWidth="0" Width="100%">
                                    </asp:CheckBoxList>
                                    <asp:Literal ID="ltrl_industry_type" runat="server"></asp:Literal>
                                </li>--%>
                                <li>
                                    <lable>Linked Companies</lable>
                                    <asp:CheckBoxList ID="chklst_companies_linked" Font-Bold="false" runat="server" RepeatColumns="2"
                                        RepeatDirection="Horizontal" Width="100%" CellPadding="0" CellSpacing="0" BorderWidth="0">
                                    </asp:CheckBoxList>
                                    <asp:Literal ID="ltrl_linked_companies" runat="server"></asp:Literal>
                                </li>
                                <li>
                                    <lable>How did you find us ?</lable>
                                    <asp:DropDownList ID="ddl_how_find_us" runat="server" CssClass="slc typtxt" Width="204">
                                    </asp:DropDownList>
                                    <asp:Literal ID="ltrl_how_find_us" runat="server"></asp:Literal>
                                </li>
                            </ul>
                            <div class="clear">
                            </div>
                            <ul class="unsbsbx">
                                <li>
                                    <asp:Label ID="lbl_invite_auction_unsubscribe" runat="server"></asp:Label><asp:CheckBox
                                        ID="chk_invite_auction" runat="server" />&nbsp;Unsubscribe for Auction Invitation
                                    &nbsp; &nbsp;<%-- <a href="#" class="unsbs">Unsubscribe</a>--%></li>
                                <li>
                                    <asp:HiddenField ID="hid_unsubscribe_invite" runat="server" Value="0" />
                                    <%--<input name="" type="button" value="Submit" class="sbmt_btn">--%>
                                    <asp:Label ID="lblMessage" runat="server" CssClass="lbl_mssg" EnableViewState="false"></asp:Label>
                                    <asp:Button ID="imgbtn_basic_edit" Text="Edit" runat="server" CssClass="sbmt_btn"
                                        Visible="false" />
                                    <asp:Button ID="imgbtn_basic_pwd" Text="Change Password" CssClass="chg_pwd" runat="server" />
                                    <asp:Button ID="imgbtn_basic_save" ValidationGroup="val_basic_info" runat="server"
                                        Text="Save" CssClass="sbmt_btn" />
                                    <asp:Button ID="imgbtn_basic_update" ValidationGroup="val_basic_info" runat="server"
                                        Text="Update" CssClass="sbmt_btn" />
                                    <span class="cancelButton" id="div_basic_cancel" runat="server">
                                        <asp:Button ID="imgbtn_basic_cancel" runat="server" Text="Cancel" CssClass="sbmt_btn" />
                                    </span></li>
                            </ul>

                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnl_buyer_user">
                            <h2 style="color: #26b3e9; font-size: 24px; font-weight: normal;">
                                Personal Information</h2>
                            <ul class="formm1">
                                <li>
                                    <lable>First Name&nbsp;<span id="span_sub_first" runat="server" class="req_star">*</span></lable>
                                    <asp:TextBox ID="txt_sub_first" runat="server" CssClass="typtxt"></asp:TextBox>
                                    <asp:Label ID="lbl_sub_first" runat="server"></asp:Label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_sub_first"
                                        ValidationGroup="val_sub_info" Display="Dynamic" ErrorMessage="<br />Name Required"
                                        CssClass="error"></asp:RequiredFieldValidator>
                                </li>
                                <li>
                                    <lable>Last Name</lable>
                                    <asp:TextBox ID="txt_sub_last" runat="server" CssClass="typtxt"></asp:TextBox>
                                    <asp:Label ID="lbl_sub_last" runat="server"></asp:Label>
                                </li>
                            </ul>
                            <div class="clear">
                            </div>
                            <ul class="formm1">
                                <li>
                                    <lable>Title</lable>
                                    <asp:DropDownList ID="dd_sub_title" runat="server" DataSourceID="sub_login_title"
                                        DataTextField="name" DataValueField="title_id" CssClass="slc typtxt" Width="204">
                                    </asp:DropDownList>
                                    <asp:Label ID="lbl_sub_title" runat="server"></asp:Label>
                                </li>
                                <li>
                                    <lable>Email&nbsp;<span id="span_sub_email" runat="server" class="req_star">*</span></lable>
                                    <asp:TextBox ID="txt_sub_email" runat="server" CssClass="typtxt"></asp:TextBox>
                                    <asp:Label ID="lbl_sub_email" runat="server"></asp:Label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_sub_email"
                                        ValidationGroup="val_sub_info" Display="Dynamic" ErrorMessage="<br />Email Required"
                                        CssClass="error"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ValidationGroup="val_sub_info" ID="RegularExpressionValidator5"
                                        runat="server" ControlToValidate="txt_sub_email" Display="Dynamic" ErrorMessage="<br>Invalid Email"
                                        ValidationExpression="^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$"
                                        CssClass="error"></asp:RegularExpressionValidator>
                                </li>
                            </ul>
                            <div class="clear">
                            </div>
                            <ul class="formm1">
                                <li>
                                    <lable>User Name</lable>
                                    <asp:Label ID="lbl_sub_username" runat="server"></asp:Label>
                                </li>
                                <li>
                                    <lable> Password&nbsp;<span id="span_sub_password" runat="server" class="req_star">*</span></lable>
                                    <asp:Literal ID="lit_sub_change_password" runat="server"></asp:Literal>
                                    <asp:Label ID="lbl_sub_password" Text="******" runat="server"></asp:Label>
                                </li>
                            </ul>
                            <div class="clear">
                            </div>
                            <ul class="unsbsbx">
                                <li>
                                    <asp:Label ID="Label1" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                                    <%--<input name="" type="button" value="Submit" class="sbmt_btn">--%>
                                    <asp:Button ID="imgbtn_sub_edit" Text="Edit" runat="server" CssClass="sbmt_btn" Visible="false" />
                                    <asp:Button ID="imgbtn_sub_change_password" Text="Change Password" CssClass="sbmt_btn"
                                        runat="server" />
                                    <asp:Button ID="imgbtn_sub_update" ValidationGroup="val_basic_info" runat="server"
                                        Text="Update" CssClass="sbmt_btn" />
                                    <span id="div_sub_info" runat="server">
                                        <asp:Button ID="imgbtn_sub_cancel" runat="server" Text="Cancel" CssClass="sbmt_btn" />
                                    </span></li>
                            </ul>
                        </asp:Panel>
                    </div>
                    <div id="tabs-2" style="padding: 1.2em 0">
                        <UC1:AuctionList runat="server" ID="Auction_Won" mode="w" type="4"></UC1:AuctionList>
                    </div>
                    <div id="tabs-3" style="padding: 1.2em 0">
                        <UC1:AuctionList runat="server" ID="Auction_Offer" mode="q" type=""></UC1:AuctionList>
                    </div>
                    <div id="tabs-4" style="padding: 1.2em 0">
                        <UC1:AuctionList runat="server" ID="Auction_Private" mode="pr" type=""></UC1:AuctionList>
                    </div>
                    <div id="tabs-5" style="padding: 1.2em 0">
                        <UC1:AuctionList runat="server" ID="Auction_Partial" mode="pa" type=""></UC1:AuctionList>
                    </div>
                    <div id="tabs-6" style="padding: 1.2em 0">
                        <UC1:AuctionList runat="server" ID="Auction_Buy" mode="bu" type=""></UC1:AuctionList>
                    </div>
                    <div id="tabs-7">
                        <asp:Panel runat="server" ID="pnl_edit_mode1">
                            <telerik:RadAjaxPanel ID="RadAjaxPanel_Sublogin" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
                                CssClass="TabGrid">
                                <asp:Literal ID="lit_refresh" runat="server"></asp:Literal>
                                <div class="txt6">
                                    <div class="item_size">
                                        <div class="table-responsive">
                                            <div style="float: right; margin-bottom: 10px;">
                                                <asp:Literal ID="lit_add" runat="server" Visible="true" Text='<%=CommonCode.Fetch_Cookie_Shared("user_id") %>'></asp:Literal>
                                            </div>
                                            <br clear="left" />
                                            <table class="table">
                                                <tr>
                                                    <th>
                                                        Name
                                                    </th>
                                                    <th>
                                                        Title
                                                    </th>
                                                    <th>
                                                        User Name
                                                    </th>
                                                    <th>
                                                        Password
                                                    </th>
                                                    <th>
                                                        Email
                                                    </th>
                                                    <th>
                                                        Bidding Limit
                                                    </th>
                                                    <th>
                                                        Is Active
                                                    </th>
                                                    <th>
                                                        Edit
                                                    </th>
                                                    <th>
                                                        Remove
                                                    </th>
                                                </tr>
                                                <asp:Repeater ID="rptItems" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="background-color: White;">
                                                                <%# Eval("first_name")%>&nbsp;<%# Eval("last_name")%>
                                                            </td>
                                                            <td style="background-color: White;">
                                                                <%# Eval("title")%>
                                                            </td>
                                                            <td style="background-color: White;">
                                                                <%# Eval("username")%>
                                                            </td>
                                                            <td style="background-color: White;">
                                                                ******
                                                            </td>
                                                            <td style="background-color: White;">
                                                                <a href='mailto:<%# Eval("email")%>'>
                                                                    <%# Eval("email")%></a>
                                                            </td>
                                                            <td style="background-color: White;">
                                                                <%# String.Format("{0:C2}", Eval("bidding_limit"))%>
                                                            </td>
                                                            <td style="background-color: White;">
                                                                <img src='<%#IIf(Eval("is_active"),"/Images/true.gif","/Images/false.gif") %>' style="border: none;"
                                                                    alt="" />
                                                            </td>
                                                            <td>
                                                                <a href="#" onclick="return Opendetail('<%#Eval("buyer_id") %>','<%#Eval("buyer_user_id") %>');">
                                                                    <img src="/images/edit_grid.gif" alt="edit" /></a>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="img_remove" runat="server" OnClientClick="return confirm('Are you sure to delete?');"
                                                                    ImageUrl="/images/delete_grid.gif" CommandName="delete" CommandArgument='<%#Eval("buyer_user_id") %>' />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <%--<telerik:RadGrid ID="rad_grid_sublogins" runat="server" Skin="Vista" GridLines="None" GroupingEnabled="false"
                                    AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                    OnNeedDataSource="rad_grid_sublogins_NeedDataSource" OnDeleteCommand="rad_grid_sublogins_DeleteCommand"
                                    OnInsertCommand="rad_grid_sublogins_InsertCommand" OnUpdateCommand="rad_grid_sublogins_UpdateCommand"
                                    enableajax="true" ClientSettings-Selecting-AllowRowSelect="false" PageSize="10"
                                    ShowGroupPanel="true">
                                    <PagerStyle Mode="NumericPages"></PagerStyle>
                                    <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="buyer_user_id"
                                        ItemStyle-Height="40" AlternatingItemStyle-Height="40" HorizontalAlign="NotSet"
                                        AutoGenerateColumns="False" EditMode="EditForms">
                                        <CommandItemStyle BackColor="#E1DDDD" />
                                        <NoRecordsTemplate>
                                            Sub login not available
                                        </NoRecordsTemplate>
                                        <SortExpressions>
                                            <telerik:GridSortExpression FieldName="name" SortOrder="Ascending" />
                                        </SortExpressions>
                                        <CommandItemSettings AddNewRecordText="Add New Sub-Login" />
                                        <Columns>
                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                EditImageUrl="/Images/edit_grid.gif" HeaderStyle-Width="90">
                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="90" />
                                            </telerik:GridEditCommandColumn>
                                            <telerik:GridBoundColumn DataField="buyer_user_id" HeaderText="buyer_user_id" UniqueName="buyer_user_id"
                                                ReadOnly="True" Visible="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="buyer_id" HeaderText="buyer_id" UniqueName="buyer_id"
                                                ReadOnly="True" Visible="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn HeaderText="Name" SortExpression="name" UniqueName="first_name"
                                                EditFormColumnIndex="1" HeaderStyle-Width="180" ItemStyle-Width="180" GroupByExpression="first_name [Name] Group By first_name">
                                                <ItemTemplate>
                                                    <%# Eval("first_name") & " " & Eval("last_name")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Title" AllowFiltering="true" UniqueName="title"
                                                HeaderStyle-Width="100" ItemStyle-Width="90" GroupByExpression="title [Title] Group By title">
                                                <ItemTemplate>
                                                    <%# Eval("title")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="User Name" AllowFiltering="true" UniqueName="username"
                                                HeaderStyle-Width="100" ItemStyle-Width="90" GroupByExpression="username [User Name] Group By username">
                                                <ItemTemplate>
                                                    <%# Eval("username")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Password" AllowFiltering="false" UniqueName="password"
                                                EditFormColumnIndex="1" HeaderStyle-Width="90" ItemStyle-Width="80" Groupable="false">
                                                <ItemTemplate>
                                                    ******
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Email" DataField="email" AllowFiltering="true"
                                                UniqueName="email" HeaderStyle-Width="100" ItemStyle-Width="90" GroupByExpression="email [Email] Group By email">
                                                <ItemTemplate>
                                                    <a href='mailto:<%# Eval("email")%>'>
                                                        <%# Eval("email")%></a>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                </EditItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn UniqueName="bidding_limit" SortExpression="bidding_limit"
                                                HeaderText="Bidding Limit" HeaderStyle-Width="90" ItemStyle-Width="90" ItemStyle-HorizontalAlign="Right"
                                                GroupByExpression="bidding_limit [Bidding Limit] Group By bidding_limit">
                                                <ItemTemplate>
                                                    <%# String.Format("{0:C2}", Eval("bidding_limit"))%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Is Active" UniqueName="is_active" EditFormColumnIndex="1"
                                                HeaderStyle-Width="70" ItemStyle-Width="70" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" GroupByExpression="is_active [Is Active] Group By is_active">
                                                <ItemTemplate>
                                                    <img src='<%#IIf(Eval("is_active"),"/Images/true.gif","/Images/false.gif") %>' style="border: none;"
                                                        alt="" />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridButtonColumn ConfirmText="Delete this Item?" ConfirmDialogType="RadWindow"
                                                ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="/Images/delete_grid.gif"
                                                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30" />
                                            </telerik:GridButtonColumn>
                                        </Columns>
                                        <EditFormSettings InsertCaption="Insert New Sub-Login" EditFormType="Template">
                                            <FormTemplate>
                                                <table id="tbl_grd_item_edit" cellpadding="0" cellspacing="2" border="0" width="800px">
                                                    <tr>
                                                        <td style="font-weight: bold; padding-top: 10px;" colspan="4">Sub-Login Details
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption" style="width: 135px;">First Name&nbsp;<span class="req_star">*</span>
                                                        </td>
                                                        <td style="width: 260px;" class="details">
                                                            <asp:TextBox ID="txt_sub_first_name" runat="server" Text='<%# Bind("first_name")%>'
                                                                CssClass="inputtype"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfv_first_name" runat="server" ErrorMessage="*" ForeColor="red"
                                                                ControlToValidate="txt_sub_first_name" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="caption" style="width: 135px;">Last Name
                                                        </td>
                                                        <td style="width: 260px;" class="details">
                                                            <asp:TextBox ID="txt_sub_last_name" runat="server" Text='<%# Bind("last_name")%>'
                                                                CssClass="inputtype"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">Title
                                                        </td>
                                                        <td class="details">
                                                            <asp:DropDownList ID="dd_sub_title" DataSourceID="sub_login_title" DataTextField="name"
                                                                DataValueField="title_id" runat="server" SelectedValue='<%# Bind("title")%>'
                                                                CssClass="DropDown" Width="204">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="caption">Email&nbsp;<span class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_sub_email" runat="server" Text='<%# Bind("email") %>' CssClass="inputtype"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfv_email" runat="server" ErrorMessage="*" ForeColor="red"
                                                                ControlToValidate="txt_sub_email" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ValidationGroup="val1_sub" ID="rev_email" runat="server"
                                                                ControlToValidate="txt_sub_email" Display="Dynamic" ErrorMessage="*" ValidationExpression="^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$"
                                                                CssClass="error"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">User Name&nbsp;<span class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_sub_username" Enabled='<%#IIf(Eval("username").ToString<>"",false,true) %>'
                                                                runat="server" Text='<%# Bind("username") %>' CssClass="inputtypePWD"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfv_username" runat="server" ErrorMessage="*" ForeColor="red"
                                                                ControlToValidate="txt_sub_username" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="caption" style="width: 145px;">Password&nbsp;<span class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_sub_password" runat="server" TextMode="Password" Text='<%# Security.EncryptionDecryption.DecryptValue(IIF(Eval("password")IS DBNULL.Value,"",Eval("password"))) %>'
                                                                CssClass="inputtypePWD"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfv_password" runat="server" ErrorMessage="*" ForeColor="red"
                                                                ControlToValidate="txt_sub_password" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="rev_password" runat="server" Display="Dynamic"
                                                                ControlToValidate="txt_sub_password" ValidationGroup="val1_sub" CssClass="error"
                                                                ErrorMessage="<br>Alphanumeric of atleast 6 chars" ValidationExpression="(?=.*\d)(?=.*[a-zA-Z]).{6,}"></asp:RegularExpressionValidator>
                                                            <asp:HiddenField ID="hid_sub_password" runat="server" Value='<%# Security.EncryptionDecryption.DecryptValue(IIF(Eval("password") IS DBNULL.Value,"",Eval("password"))) %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption">Bidding Limit&nbsp;<span class="req_star">*</span>
                                                        </td>
                                                        <td class="details">
                                                            <asp:TextBox ID="txt_sub_bidding_limit" runat="server" MaxLength="8" Text='<%# Bind("bidding_limit") %>'
                                                                CssClass="inputtype">
                                                            </asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfv_bidding_limit" runat="server" ErrorMessage="*"
                                                                ForeColor="red" ControlToValidate="txt_sub_bidding_limit" Display="Dynamic" ValidationGroup="val1_sub"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="rev_bidding_limit" runat="server" Display="Dynamic"
                                                                ControlToValidate="txt_sub_bidding_limit" ValidationGroup="val1_sub" CssClass="error"
                                                                ErrorMessage="<br />Invalid" ValidationExpression="^\d*\.?\d*$"></asp:RegularExpressionValidator>
                                                        </td>
                                                        <td class="caption">Is Active
                                                        </td>
                                                        <td class="details">
                                                            <asp:CheckBox ID="chk_sub_is_active" runat="server" Checked='<%# IIF(checked(Eval("is_active")),True,False) %>' />
                                                            <asp:HiddenField ID="hid_old_active_status" runat="server" Value='<%# IIF(checked(Eval("is_active")),1,0) %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" align="right">
                                                            <asp:Literal ID="lbl_grid_error" runat="server"></asp:Literal>
                                                        </td>
                                                        <td align="right" colspan="2">
                                                            <asp:ImageButton ID="but_grd_submit" CommandName='<%# IIf((TypeOf(Container) is GridEditFormInsertItem),"PerformInsert","Update") %>'
                                                                ValidationGroup="val1_sub" runat="server" AlternateText='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "Save" , "Update") %>'
                                                                ImageUrl='<%# IIf((TypeOf(Container) is GridEditFormInsertItem), "/images/save.gif" , "/images/update.gif") %>' />
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <asp:ImageButton ID="but_grd_cancel" CausesValidation="false" CommandName="Cancel"
                                                                runat="server" AlternateText="Cancel" ImageUrl="/images/cancel.gif" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </FormTemplate>
                                        </EditFormSettings>
                                    </MasterTableView>
                                    <ClientSettings AllowDragToGroup="true">
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <GroupingSettings ShowUnGroupButton="true" />
                                </telerik:RadGrid>--%>
                                <asp:SqlDataSource ID="sub_login_title" ConnectionString="<%$ ConnectionStrings:TConnectionString %>"
                                    runat="server" ProviderName="System.Data.SqlClient" SelectCommand="select '--Select--' as name,'' as title_id union all select isnull(name,'') as name,isnull(name,'') as title_id from tbl_master_titles order by name">
                                </asp:SqlDataSource>
                            </telerik:RadAjaxPanel>
                        </asp:Panel>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <!--innerwraper -->
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $("#tabs").tabs();
        });
    </script>
</asp:Content>
