﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AuctionMain.master" AutoEventWireup="false"
    CodeFile="register_confirm.aspx.vb" Inherits="register_confirm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Child_Content" runat="Server">
 <br />
    <div class="innerwraper">
        <div class="paging">
            <ul>
                <li><a href="/">Home</a></li>
                <li>Confirm</li>
            </ul>
        </div>
        <div class="clear">
        </div>
        <br />
        <div id="secondarycontent" style="background-color: White;">
            <br />
            <div id="leftnav">
                <h4>
                    <a href="/login.html">LOGIN</a>
                </h4>
                <div class="leftcalloutbox">
                    <div class="leftcalloutboxcontainer">
                        Helping you grow your business is our number-one priority.
                    </div>
                </div>
                <div id="leftcontentboxfooter">
                    <div id="leftcontentboxfootercontainer">
                        <div style="font-size: 13px; font-weight: bold;">
                            Why become a member?</div>
                        <div style="padding-top: 10px;">
                            <ul style="list-style: url(/images/arrow_home.gif)">
                                <li>No bidding fees.</li>
                                <li>Auctions without minimum bids.</li>
                                <li>Purchase outright with buy it now options.</li>
                                <li>Phones, tablets, laptops and accessories.</li>
                                <li>New and pre-owned products.</li>
                                <li>Hard to find models.</li>
                                <li>Choose from multiple technologies, models and brands.</li>
                                <li>Pre-qualified buyers.</li>
                                <li>Market driven pricing.</li>
                                <li>Products in stock now.</li>
                            </ul>
                        </div>
                        <div>
                            <a href="/sign-up.html" style="color: #10AAEA; font-weight: bold;">Join now</a>
                        </div>
                        <div style="padding-top: 16px;">
                            <div style="font-weight: bold;">
                                Questions?</div>
                            <div style="padding-top: 10px;">
                                Email: <a href="mailto:<%= SqlHelper.of_FetchKey("site_email_to")  %>"><%= SqlHelper.of_FetchKey("site_email_to")  %></a><br />
                                Phone: 973.805.7400 ext 179
                            </div>
                        </div>
                    </div>
                </div>
                <div class="leftcontentbox">
                </div>
            </div>
            <div id="maincontent">
                <div class="products">
                    <div class="seprator">
                    </div>
                    <div style="text-align:left;">
                      <h1>
                          <asp:Literal ID="lit_heading" runat="server" Text="THANKS"></asp:Literal></h1>
                      <table cellspacing="0" cellpadding="0" width="595px" border="0">
        
                          <tr>
                              <td style="padding-top: 15px;" align="left">
                                  <asp:Literal ID="lbl_message" runat="server"></asp:Literal>
                                  <asp:HiddenField ID="hid_reister_type" runat="server" />
                              </td>
                          </tr>
                      </table>
   
                    </div>
                </div>
                <!--products
    -->
            </div>
            <div class="clear">
            </div>
        </div>
    </div>

</asp:Content>
