﻿
Partial Class register_confirm
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ' Response.Write("<script language='javascript'>top.topFrame.location = '/SiteTopBar.aspx?t=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("t")), "0", Request.QueryString.Get("t")) & "';top.leftFrame.location= '/SiteLeftBar.aspx?t=" & IIf(String.IsNullOrEmpty(Request.QueryString.Get("t")), "0", Request.QueryString.Get("t")) & "';</script>")
           
            If Not Request.QueryString("rt") Is Nothing Then
                hid_reister_type.Value = Request.QueryString("rt")
            End If

            show_message()

        End If
    End Sub
    Private Sub show_message()
        Dim reister_type As String = ""
        reister_type = hid_reister_type.Value.ToUpper()
        If reister_type = "N" Then
            lbl_message.Text = "Thank you for registering as a bidder on <a href='http://www.overstockwireless.com/' target='_blank'>www.overstockwireless.com</a>. We will contact you shortly.<br>Your account will be activated once approved by moderator. You will receive confirmation email regarding activation of your account."
        ElseIf reister_type = "A" Then
            lbl_message.Text = "Bidder is successfully activated."
        ElseIf reister_type = "F" Then
            lbl_message.Text = "The password has been sent successfully to your registered e-mail address."
        ElseIf reister_type = "E" Then
            lbl_message.Text = "Bidder is already activated."
        ElseIf reister_type = "C" Then
            lbl_message.Text = "Thank you for registering as a bidder on <a href='http://www.overstockwireless.com/' target='_blank'>www.overstockwireless.com</a>.<br><a href='" & SqlHelper.of_FetchKey("ServerHttp") & "/login.html'>Click here to login</a>"
        Else
            lit_heading.Text = "SORRY"
            'lbl_thank_you.Visible = False
            lbl_message.Text = "Sorry, contact to administrator."
        End If
    End Sub
End Class
