﻿<%@ Page Title="PCS Auction" Language="VB" MasterPageFile="~/SitePopUp.master" AutoEventWireup="false"
    CodeFile="AuctionBidPop.aspx.vb" Inherits="AuctionBidPop" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="/Style/stylesheet.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript">

        /***********************************************
        * Different CSS depending on OS (mac/pc)- © Dynamic Drive (www.dynamicdrive.com)
        * This notice must stay intact for use
        * Visit http://www.dynamicdrive.com/ for full source code
        ***********************************************/
        ///////No need to edit beyond here////////////

        var mactest = navigator.userAgent.indexOf("Mac") != -1


        if (mactest) {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet_mac.css"/>');
        }
        else {
            document.write('<link rel="stylesheet" type="text/css" href="/style/stylesheet.css"/>');
        }
    </script>
    <script type="text/javascript">
        $(function () {

            $(".typtxt").each(function () {
                $tb = $(this);
                if ($tb.val() != this.title) {

                }
            });

            $(".typtxt").focus(function () {
                $tb = $(this);
                if ($tb.val() == this.title) {
                    $tb.val("");

                }
            });

            $(".typtxt").blur(function () {
                $tb = $(this);
                if ($.trim($tb.val()) == "") {
                    $tb.val(this.title);
                    $tb.addClass("typtxt");
                }
            });
        });       
 
    </script>
    <script language="javascript" type="text/javascript">
        function closeNow(typeId, ctlId) {

            if (parseInt(typeId) == 2 || parseInt(typeId) == 3)
            { window.opener.RefreshList(ctlId); }

            window.opener.location.reload();
            window.close();
            return false;
        }

        function ShowWait() {
            document.getElementById('wait').style.display = 'block';
            document.getElementById('<%=btn_proxy.ClientID%>').style.display = 'none';
            return true;

        }
    </script>
    <div id="bid-popup" class="fancybox-skin" style="width: auto; height: auto; display: block;">
        <div class="" style="padding: 15px 0;">

            <asp:HiddenField ID="hid_auction_id" runat="server" Value="0" />
            <asp:HiddenField ID="hid_buyer_id" runat="server" Value="0" />
            <asp:HiddenField ID="hid_buyer_user_id" runat="server" Value="0" />
            <asp:HiddenField ID="hid_buy_now_price" runat="server" Value="0" />
            <asp:HiddenField ID="hid_buy_avl_qty" runat="server" Value="0" />
            <asp:HiddenField ID="hid_bid_now_price" runat="server" Value="0" />
            <asp:HiddenField ID="hid_quantity" runat="server" Value="0" />
            <asp:HiddenField ID="hid_accepted_bid" runat="server" Value="0" />
            <h2 style="word-break: break-word!important;">
                <span class="blue">
                    <asp:Literal ID="lbl_modal_catg" runat="server"></asp:Literal></span>
                <asp:Literal ID="lbl_modal_caption" runat="server"></asp:Literal><br />
            </h2>
            <asp:Literal ID="ltrl_requset_msg" runat="server"></asp:Literal>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:Button ID="btn_proxy" OnClientClick="javascript:ShowWait();" runat="server"
                            CssClass="bidbtn" />
                        <img id="wait" src="/Images/waitimg.gif" border='none' alt="Wait" style="display: none;" />
                    </td>
                    <td valign="top" style="padding-left: 10px;" id="heading" runat="server">
                        <span style="color: red; font-size: 14px; font-weight: bold;">This bid amount is for
                            entire lot
                            <br />
                            and not per single unit.</span>
                    </td>
                </tr>
            </table>
            <asp:Literal ID="ltrl_shipping_msg" runat="server"></asp:Literal>
            <div id="pnl_offer" runat="server" visible="true" style="width: auto; height: auto;"
                defaultbutton="ImageButtonOffer">
                <%-- <center>--%>
                <div>
                    <div id="tr_price" runat="server">
                        <div>
                            <div class="inputboxes" style="margin: 20px 0px!important;">
                                <div class="fleft qnty" style="word-wrap: break-word;">
                                    <span class="text">I want to buy</span><span class="" id="tr_qty" runat="server">
                                        <asp:TextBox ID="txt_qty" runat="server" CssClass="typtxt" MaxLength="10" Width="100"></asp:TextBox>
                                        <asp:CompareValidator ID="CValidator2" runat="server" Display="Dynamic" ControlToValidate="txt_qty"
                                            ErrorMessage=" * " Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="RFOffer2" runat="server" ControlToValidate="txt_qty"
                                            ErrorMessage=" * " Display="Dynamic"> </asp:RequiredFieldValidator>
                                        <ajax:TextBoxWatermarkExtender ID="TBWE1" runat="server" TargetControlID="txt_qty"
                                            WatermarkText="Quantity" WatermarkCssClass="typtxt" />
                                    </span><span id="ddl_qty" runat="server" visible="false">
                                        <asp:DropDownList CssClass="typtxt" ID="ddl_buy_now_qty" Style="width: 80px; margin-left: 5px;"
                                            runat="server" AutoPostBack="false" />
                                    </span><span class="text">&nbsp;unit(s) for</span><span class="text">
                                        <asp:Literal runat="server" ID="lit_qty" Text="&nbsp;this lot for"></asp:Literal></span><span
                                            class="text">&nbsp;$ </span><span id="spn_amt_txt" runat="server">
                                                <%--<telerik:RadNumericTextBox ID="txt_amount" runat="server"  MaxLength="10" Type="Number"
                                    EmptyMessage="Amount">
                                </telerik:RadNumericTextBox>--%>
                                                <asp:TextBox ID="txt_amount" runat="server" CssClass="typtxt" MaxLength="10" Width="100"></asp:TextBox>
                                                <%-- <asp:TextBox ID="txt_amount" runat="server" CssClass="typtxt" Text="Amount" ToolTip="Amount"> 
                                   </asp:TextBox>--%>
                                                <asp:RequiredFieldValidator ID="RFOffer1" runat="server" ErrorMessage=" * " ForeColor="red"
                                                    ControlToValidate="txt_amount" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="CValidator1" runat="server" Display="Dynamic"
                                                    ControlToValidate="txt_amount" ErrorMessage=" * " ValidationExpression="^\d*\.?\d*$"></asp:RegularExpressionValidator>
                                                <asp:CompareValidator ID="CValidator_a" runat="server" Display="Dynamic" ControlToValidate="txt_amount"
                                                    ErrorMessage=" * " Type="Double" Operator="GreaterThanEqual"></asp:CompareValidator>
                                                <ajax:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txt_amount"
                                                    WatermarkText="Amount" WatermarkCssClass="typtxt" />
                                            </span><span class="text">
                                                <asp:Literal ID="lit_buy_now_amount" runat="server"></asp:Literal>&nbsp;/unit.</span></div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </div>
                <div id="tr_message" runat="server">
                    <div style="padding: 5px 0px 5px 0px; text-align: center;">
                        <span class="text">
                            <asp:Literal ID="lit_offer_text" runat="server"></asp:Literal></span>
                    </div>
                </div>
                <div style="padding-top: 30px;">
                    <p class="orange">
                        Please note that PCSWireless reserve the rights to cancel any order.</p>
                    <p class="orange">
                        Rates are subject to change. Additional shipping and handling fees may apply.</p>
                    <p class="checkbox">
                        <asp:CheckBox ID="chk_accept_term" runat="server" Text="Accept our <a href='#' onClick=window.open('/pop_terms_conditions.aspx','mywindow','width=600,height=500,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=no')>Terms & Conditions</a>"
                            TextAlign="Right" /></p>
                </div>
                <asp:Button runat="server" ID="btn_buy_now" CssClass="fleft" Text="Buy Now" />
                <input type="button" name="" id="popupClose" onclick="javascript: window.opener.location.reload(); window.close();"
                    value="Cancel" />
                <div style="clear: both">
                </div>
                <span class="text">
                    <asp:Label ID="lbl_terms_error" runat="server" Text="Please check our terms & conditions."
                        ForeColor="Red" Visible="false" EnableViewState="false"></asp:Label></span>
            </div>
            <%-- </center>--%>
        </div>
        <div runat="server" id="pnl_confirm" visible="true">
            <center>
                <div style="height: 420px; margin-top: 20px;">
                    <div>
                        <div>
                            <asp:Label ID="lbl_popmsg_msg" ViewStateMode="Disabled" runat="server" Font-Names="Arial, verdana, serif"
                                Font-Size="14" ForeColor="#e9650e" Style="margin: 2px 0px;" Font-Bold="true"></asp:Label>
                        </div>
                    </div>
                    <asp:Panel runat="server" ID="dv_bidding" Visible="false" Style="padding: 20px;margin-top:25px; text-align: left;background-color:#FFFFFF;">
                        <div style="float: left;">
                            <asp:RadioButton ID="rb_total_bid_amt" Checked="True" Text="Total" GroupName="max_bid_amt" runat="server" style="margin-right:15px" />
                            <asp:RadioButton ID="rb_partial_bid_amt" Checked="False" Text="per unit" GroupName="max_bid_amt" runat="server"  style="margin-right:15px" />
                                             <br />
                            <asp:TextBox runat="server" CssClass="typtxt" ID="txt_max_bid_amt" Text=""></asp:TextBox>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" Display="Dynamic" ControlToValidate="txt_max_bid_amt"
                                ErrorMessage="* " Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                              <!--  <asp:CompareValidator ID="CompareValidator2" runat="server" Display="Dynamic" ControlToValidate="txt_max_bid_amt"
                                ErrorMessage="<br>Amount not Accepted" Type="Double" Operator="GreaterThanEqual"></asp:CompareValidator>-->
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_max_bid_amt"
                                ErrorMessage="* " Display="Dynamic"> </asp:RequiredFieldValidator>
                            <ajax:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txt_max_bid_amt"
                                WatermarkText="Enter New Bid Amount" WatermarkCssClass="typtxt" />                            
                        </div>
                        <div style="float: left;margin-left:25px;padding-top:5px;">
                            <asp:Button runat="server" ID="but_main_auction" CssClass="bid" Text="UPDATE BID" />
                        </div>
                        <div class="clear">
                        </div>
                        
                        <span id="lotPerUnitT" style="color: red;"  runat="server">
                            <asp:Label ID="LPerUnitTotal" runat="server" Text="" />
                        </span>
                        <p>
                            <span style="color: red; font-size: 14px; font-weight: bold;">This bid amount is for
                                entire lot and not per single unit. </span>
                            <br />
                            <br />
                            Your bid is a non-revocable offer to enter into a binding contract with PCS. If,
                            at the sole discretion of PCS, your bid is accepted and selected as the winning
                            bid, you have entered into a legally binding contract with PCS and are contractually
                            obligated to purchase the item(s).
                        </p><asp:Literal ID="lit_ship_note1" runat="server"></asp:Literal>

                    </asp:Panel>
                    <div>
                        <div id='dv_askpop_ask' style="margin-top: 20px;">
                            <input type="button" name="" id="ImageButtonCancelMsg" onclick="javascript: window.opener.location.reload(); window.close();"
                                value="Close" />
                        </div>
                    </div>
                </div>
            </center>
        </div>
        <div runat="server" id="pnl_send_quote" visible="false" defaultbutton="ImageButton_quote_send">
            <table cellpadding="5" cellspacing="0" border="0" style="height: 300px;">
                <tr>
                    <td align="left" colspan="2">
                        <div style="float: left;">
                            <asp:TextBox runat="server" ID="txt_quote_desc" TextMode="MultiLine" CssClass="qsttxt"
                                Height="100" Columns="50" Style="resize: vertical;"></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="Required_txt_quote_desc" ValidationGroup="VG1" runat="server"
                            Font-Size="10px" ControlToValidate="txt_quote_desc" ErrorMessage="*" Display="Dynamic"> </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="modalCaption" width="150" align="center" style="font-size: 15px;">
                        Quote File
                    </td>
                    <td align="left">
                        <asp:FileUpload ID="fle_quote_upload" Width="200" size="15" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button runat="server" ID="Button_quote_send" Text="Send Offer" ValidationGroup="VG1" />
                        <input type="button" name="" id="ImageButtonCancelQuote" onclick="javascript: window.opener.location.reload(); window.close();"
                            value="Cancel" />
                    </td>
                </tr>
                <tr>
                    <td class="modalCaption">
                    </td>
                    <td>
                        <asp:Label ID="lbl_error_quote" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script>
        function open_pop_win(_path, _id) {
            w = window.open('/terms_conditions.aspx', 'top=' + ((screen.height - 400) / 2) + ', left=' + ((screen.width - 324) / 2) + ', height=400, width=324,scrollbars=yes,toolbars=no,resizable=1;');
            w.focus();
            return false;
        }

    </script>
    <script type="text/javascript">
        window.focus();
        function CalculateTotal() {

            var txtAmount = $get("<%=txt_max_bid_amt.ClientID%>").value;
            var txtQuantity = $get("<%=hid_quantity.ClientID%>").value;
            var total

            if ((txtAmount) && (parseInt(txtQuantity) > 1)) {
                total = parseFloat(txtAmount) * parseInt(txtQuantity);
                if (!isNaN(total)) {
                    document.getElementById('<%=txt_max_bid_amt.ClientID%>').value = total.toFixed(2);
                       
                        document.getElementById('<%=LPerUnitTotal.ClientID%>').innerHTML = 'Per Unit: ' + txtAmount;
                    }
                    else {
                        document.getElementById('<%=txt_max_bid_amt.ClientID%>').value = '';
                        document.getElementById('<%=LPerUnitTotal.ClientID%>').innerHTML = '';
                    }
                }
            }


            function CalculatePerUnit() {

                var txtAmount = $get("<%=txt_max_bid_amt.ClientID%>").value;
            var txtQuantity = $get("<%=hid_quantity.ClientID%>").value;
            var perunit

            if ((txtAmount) && (parseInt(txtQuantity) > 1)) {
                perunit = parseFloat(txtAmount) / parseInt(txtQuantity);
                if (!isNaN(perunit)) {
                    document.getElementById('<%=txt_max_bid_amt.ClientID%>').value = perunit.toFixed(2);
                        
                        document.getElementById('<%=LPerUnitTotal.ClientID%>').innerHTML = 'Total: ' + txtAmount;
                    }
                    else {
                        document.getElementById('<%=txt_max_bid_amt.ClientID%>').value = '';
                        document.getElementById('<%=LPerUnitTotal.ClientID%>').innerHTML = ''
                    }
                }
            }

            function RecalculateTotal() {
                if (document.getElementById('<%=rb_partial_bid_amt.ClientID%>').checked) {
                document.getElementById('<%=rb_total_bid_amt.ClientID%>').checked = true;
                CalculateTotal();
            }
        }

        function recalculatePerUnitTotal() {
            var txtAmount = $get("<%=txt_max_bid_amt.ClientID%>").value;
            var txtQuantity = $get("<%=hid_quantity.ClientID%>").value;
            var perunitTotal
            var txtdescription
            document.getElementById('<%=LPerUnitTotal.ClientID%>').innerHTML = '';
          
            if ((txtAmount) && (parseInt(txtQuantity) > 1)) {
                if (document.getElementById('<%=rb_partial_bid_amt.ClientID%>').checked) {
                    txtdescription = 'Total: ';
                    perunitTotal = parseFloat(txtAmount) * parseInt(txtQuantity);
                }
                else {
                    perunitTotal = parseFloat(txtAmount) / parseInt(txtQuantity);
                    txtdescription = 'Per Unit: ';
                }
                if (!isNaN(perunitTotal)) {
                    document.getElementById('<%=LPerUnitTotal.ClientID%>').innerHTML = txtdescription + perunitTotal.toFixed(2);
                    comparebid();
                }
            }
        }

        function comparebid() {

            var txtAmount = parseFloat($get("<%=txt_max_bid_amt.ClientID%>").value);
            var txtAccepted =parseFloat( $get("<%= hid_accepted_bid.ClientID%>").value);
            var txtdecription = document.getElementById('<%=LPerUnitTotal.ClientID%>').innerHTML;

           

            if (txtAccepted) {
                if (txtAmount < txtAccepted) {
                   
                    document.getElementById('<%=LPerUnitTotal.ClientID%>').innerHTML = txtdecription + '  - Amount not Accepted'
                    return false;
                }
                else {
                    return true;

                }
            }
        }
       


    </script>
</asp:Content>
