﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AuctionMain.master" AutoEventWireup="false"
    MaintainScrollPositionOnPostback="true" CodeFile="signup.aspx.vb" Inherits="signup" %>

<%@ Register Assembly="App_Code" Namespace="RampValidators" TagPrefix="RampValidators" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Child_Content" runat="Server">
    <style type="text/css">
           @media only screen and (max-width : 740px)
{
    #maincontent {
        width:100%;
           border: 0 solid red;
    float: right;
    padding: 0 20px 0 0;
    
}
        .file_upload {
            width:100px;
        }
   .bktHeader
{
    background-color:#0190BC;
    color:White;
    font-weight:bold;
    font-size:12px;
    padding:5px;
    width:95px;
    }
        .bucket_width {
            width:100px;
        }
    }

    </style>
    <script type="text/javascript">
        function open_pop() {
            window.open('/pop_terms_conditions.aspx', 'mywindow', 'width=600,height=500,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=no');
            window.focus();
            return false;
        }
       
       
        function optoutmode(ctl) {
            
            var gridView = document.getElementById("tb_rpt_bucket");
            var checkBoxes = gridView.getElementsByTagName("input");            
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    checkBoxes[i].checked = false;   
                }
                if (ctl.checked) {
                    checkBoxes[i].disabled = true;   
                }
                else {
                    checkBoxes[i].disabled = false;
                }
            }  
            
            if (ctl.checked) {               
                document.getElementById("tb_rpt_bucket").style.color = "#ACAEB3";               
            }
            else {                
                document.getElementById("tb_rpt_bucket").style.color = "#6f6f6f";
            }
        }

      

       

    </script>
  
    <br />
    <div class="innerwraper">
        <div class="paging">
            <ul>
                <li><a href="/">Home</a></li>
                <li>Signup</li>
            </ul>
        </div>
        <div class="clear">
        </div>
        <br />
        <div id="secondarycontent" style="background-color: White;">
            <br />
            <div id="leftnav">
                <h4>
                    <a href="/sign-up.html">SIGNUP</a>
                </h4>
                <div class="leftcalloutbox">
                    <div class="leftcalloutboxcontainer">
                        Helping you grow your business is our number-one priority.
                    </div>
                </div>
                <div id="leftcontentboxfooter">
                    <div id="leftcontentboxfootercontainer">
                        <div style="font-size: 13px; font-weight: bold;">
                            Why become a member?</div>
                        <div style="padding-top: 10px;">
                            <ul style="list-style: url(/images/arrow_home.gif)">
                                <li>No bidding fees.</li>
                                <li>Auctions without minimum bids.</li>
                                <li>Purchase outright with buy it now options.</li>
                                <li>Phones, tablets, laptops and accessories.</li>
                                <li>New and pre-owned products.</li>
                                <li>Hard to find models.</li>
                                <li>Choose from multiple technologies, models and brands.</li>
                                <li>Pre-qualified buyers.</li>
                                <li>Market driven pricing.</li>
                                <li>Products in stock now.</li>
                            </ul>
                        </div>
                        <div>
                            <a href="/sign-up.html" style="color: #10AAEA; font-weight: bold;">Join now</a>
                        </div>
                        <div style="padding-top: 16px;">
                            <div style="font-weight: bold;">
                                Questions?</div>
                            <div style="padding-top: 10px;">
                                Email: <a href="mailto:<%= SqlHelper.of_FetchKey("site_email_to")  %>"><%= SqlHelper.of_FetchKey("site_email_to")  %></a><br />
                                Phone: 973.805.7400 ext 179
                            </div>
                        </div>
                    </div>
                </div>
                <div class="leftcontentbox">
                </div>
            </div>
            <div id="maincontent"  >
                <div class="products" style="width:auto; max-width:100%;">
                    <div class="seprator">
                    </div>
                    <asp:HiddenField ID="hid_comp_id" runat="server" Value="0" />
                    <h1>
                        Join the Online Auction: Become a Member</h1>
                    <p>
                        Only members of PCS Wireless Online Auction may buy products.
                    </p>
                    <p>
                        If you are already a member, please <a href="javascript:void(0);" style="color: #10AAEA;
                            font-weight: bold;" onclick='return Modal_Dialog();'>log in now.</a>
                    </p>
                    <p>
                        To become a member, and start bidding on products currently being auctioned on the
                        site, please complete the online application below and submit along with your resale certificate or company registration documents.
                    </p>
                    <p>
                        <b>Questions?</b> Contact us at <a href="mailto:<%= SqlHelper.of_FetchKey("site_email_to")  %>"><%= SqlHelper.of_FetchKey("site_email_to")  %></a>
                        or 973.805.7400 ext 179
                    </p>
                    <%--<p>
    PCS Wireless online auction portal is an auction site that allows buyers and sellers
    to come together & enjoy the online auction experience. We allow you the advantage
    of purchasing a product at best prices by the means of competitive bidding.</p>--%>
                    <p>
                    <table style="font-size: 13px; overflow-x:auto;"  border="0" cellpadding="2" cellspacing="2" >
                        <tbody>
                            <tr>
                                <td colspan="2" align="center" width="50">
                                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
                                    <asp:HiddenField ID="hid_password" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h4>
                                        STEP 1: Your company details</h4>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Company:</label><span style="color: Red; font-size: 10px;">*</span>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_company" runat="server" MaxLength="100" CssClass="TextBoxField_Capitalization"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="rfv_company" runat="server" ControlToValidate="txt_company"
                                            Font-Size="10px" ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="Company Required"
                                            CssClass="error"></asp:RequiredFieldValidator>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Company Website:</label>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_website" runat="server" MaxLength="100" CssClass="TextBoxField"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br />
                                    <h4>
                                        STEP 2: Contact details</h4>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        First name:</label><span style="color: Red; font-size: 10px;">*</span>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_first_name" runat="server" MaxLength="50" CssClass="TextBoxField_Capitalization"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="rfv_contact_name" runat="server" Font-Size="10px"
                                            ControlToValidate="txt_first_name" ValidationGroup="val_signup_info" Display="Dynamic"
                                            ErrorMessage="First Name Required" CssClass="error"></asp:RequiredFieldValidator>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Last name:</label>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_last_name" runat="server" MaxLength="50" CssClass="TextBoxField_Capitalization"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                          <!--  <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Position:</label>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:DropDownList ID="ddl_title" Font-Size="12px" Width="155px" runat="server" DataTextField="name"
                                            DataValueField="country_id">
                                        </asp:DropDownList>
                                    </div>
                                </td>
                            </tr>-->
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Street:</label><span style="color: Red; font-size: 10px;">*</span>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_address1" runat="server" MaxLength="100" CssClass="TextBoxField_Capitalization"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="rfv_address1" runat="server" Font-Size="10px" ControlToValidate="txt_address1"
                                            ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="Street Required"
                                            CssClass="error"></asp:RequiredFieldValidator>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Street Address 2:</label>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_address2" runat="server" MaxLength="100" CssClass="TextBoxField_Capitalization"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        City:</label><span style="color: Red; font-size: 10px;">*</span>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_city" runat="server" MaxLength="50" CssClass="TextBoxField_Capitalization"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="rfv_city" runat="server" Font-Size="10px" ControlToValidate="txt_city"
                                            ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="City Required"
                                            CssClass="error"></asp:RequiredFieldValidator>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Country:</label><span style="color: Red; font-size: 10px;">*</span>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:DropDownList ID="ddl_country" Font-Size="12px" Width="155px" runat="server"
                                            DataTextField="name" DataValueField="country_id">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:RequiredFieldValidator ID="rfv_country" runat="server" Font-Size="10px" ControlToValidate="ddl_country"
                                            ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="Country Required"
                                            CssClass="error"></asp:RequiredFieldValidator>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        State/Province:</label><span style="color: Red; font-size: 10px;">*</span>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_other_state" runat="server" MaxLength="50" CssClass="TextBoxField_Capitalization"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="rfv_txt_state" runat="server" Font-Size="10px" ControlToValidate="txt_other_state"
                                            ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="State Required"
                                            CssClass="error"></asp:RequiredFieldValidator>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Postal code:</label><span style="color: Red; font-size: 10px;">*</span>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_zip" runat="server" CssClass="TextBoxField" MaxLength="15"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="rfv_zip" runat="server" Font-Size="10px" ControlToValidate="txt_zip"
                                            ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="Postal Code Required"
                                            CssClass="error"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Font-Size="10px"
                                            ControlToValidate="txt_zip" ValidationExpression="^[a-zA-Z0-9 ]*$" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="Invalid Postal Code" runat="server" ValidationGroup="val_signup_info" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Phone 1:</label><span style="color: Red; font-size: 10px;">*</span>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_mobile" runat="server" CssClass="TextBoxField" MaxLength="15"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfv_mobile" Font-Size="10px" runat="server" ControlToValidate="txt_mobile"
                                            ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="Phone 1 Required"
                                            CssClass="error"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="rev_mobile" runat="server" ValidationGroup="val_signup_info"
                                            ErrorMessage="Phone is invalid" CssClass="error"
                                            ControlToValidate="txt_mobile" ValidationExpression="([0-9]\-?)*"></asp:RegularExpressionValidator><br />
                                    </div>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:RadioButton ID="rdo_ph_1_landline" runat="server" Text="Landline" GroupName="p1"
                                            Checked="true" /><asp:RadioButton ID="rdo_ph_1_mobile" runat="server" Text="Mobile"
                                                GroupName="p1" />                                       

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Phone 2:</label>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_phone" runat="server" CssClass="TextBoxField" MaxLength="15"></asp:TextBox>
                                         <asp:RegularExpressionValidator ID="rev_phone" runat="server" ValidationGroup="val_signup_info"
                                            ErrorMessage="Phone is invalid" CssClass="error"
                                            ControlToValidate="txt_phone" ValidationExpression="([0-9]\-?)*"></asp:RegularExpressionValidator><br />
                                    </div>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:RadioButton ID="rdo_ph_2_landline" runat="server" Text="Landline" GroupName="p2"
                                            Checked="true" /><asp:RadioButton ID="rdo_ph_2_mobile" runat="server" Text="Mobile"
                                                GroupName="p2" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Fax:</label>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_fax" runat="server" CssClass="TextBoxField" MaxLength="15"></asp:TextBox>
                                         <asp:RegularExpressionValidator ID="rev_fax" runat="server" ValidationGroup="val_signup_info"
                                            ErrorMessage="Fax number is invalid" CssClass="error"
                                            ControlToValidate="txt_fax" ValidationExpression="([0-9]\-?)*"></asp:RegularExpressionValidator><br />
                                        <br />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Your Email:</label><span style="color: Red; font-size: 10px;">*</span>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_email" runat="server" MaxLength="50" CssClass="TextBoxField"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="rfv_email" runat="server" Font-Size="10px" ControlToValidate="txt_email"
                                            ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="Email Required"
                                            CssClass="error"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ValidationGroup="val_signup_info" ID="rev_email"
                                            runat="server" ControlToValidate="txt_email" Display="Dynamic" ErrorMessage="Invalid Email"
                                            Font-Size="10px" ValidationExpression="^[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-zA-Z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$"
                                            CssClass="error"></asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="CustomValidator2" runat="server" Font-Size="10px" ControlToValidate="txt_email"
                                            ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="Email is already in our Record."
                                            OnServerValidate="CheckEmailID" CssClass="error" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Confirm Email:</label><span style="color: Red; font-size: 10px;">*</span>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_confirm_email" runat="server" MaxLength="50" CssClass="TextBoxField"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="rfv_confirm_email" runat="server" ControlToValidate="txt_confirm_email"
                                            ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="Confirm Email Required"
                                            Font-Size="10px" CssClass="error"></asp:RequiredFieldValidator>
                                        <RampValidators:StringCompareValidator ID="StringCompareValidatorReEmail" ControlToValidate="tbEmail2"
                                            ControlToCompare="tbEmail1" Text="Email addresses should match." Display="Dynamic"
                                            TrimText="false" IgnoreCase="true" Operator="Equal" runat="server" CssClass="required"></RampValidators:StringCompareValidator>
                                        <RampValidators:StringCompareValidator ID="compv_confirm_email" Font-Size="10px"
                                            runat="server" ControlToValidate="txt_confirm_email" IgnoreCase="true" ControlToCompare="txt_email"
                                            CssClass="error" Display="Dynamic" ValidationGroup="val_signup_info" TrimText="false"
                                            ErrorMessage="Email Mismatch"></RampValidators:StringCompareValidator>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br />
                                    <h4>
                                        STEP 3: Account setup</h4>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        User ID:</label><span style="color: Red; font-size: 10px;">*</span>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_user_id" runat="server" CssClass="TextBoxField" MaxLength="50"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="rfv_user_id" runat="server" Font-Size="10px" ControlToValidate="txt_user_id"
                                            ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="User ID Required"
                                            CssClass="error"></asp:RequiredFieldValidator>
                                          <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_user_id"
                                            Font-Size="10px" ValidationGroup="val_signup_info" ErrorMessage="Please use alphanumeric chars." CssClass="error" 
                                            ValidationExpression="^[a-zA-Z\d\s\.]+$" Display="Dynamic"></asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" Font-Size="10px" ControlToValidate="txt_user_id"
                                            ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="User Exists, Please Choose other User ID."
                                            OnServerValidate="CheckUserID" CssClass="error" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Password:</label><span style="color: Red; font-size: 10px;">*</span>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_password" runat="server" MaxLength="20" CssClass="TextBoxField"
                                            TextMode="Password"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator Font-Size="10px" ID="rfv_password" runat="server" ControlToValidate="txt_password"
                                            ValidationGroup="val_signup_info" Display="Dynamic" ErrorMessage="Password Required"
                                            CssClass="error"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="Regular_password" runat="server" ControlToValidate="txt_password"
                                            Font-Size="10px" ValidationGroup="val_signup_info" ErrorMessage="Alphanumeric atleast 6 chars."
                                            ValidationExpression="^(?=.{6,}$)(?=.*\d)(?=.*[a-zA-Z])(?!.*\s).*" Display="Dynamic"></asp:RegularExpressionValidator>
                                        <asp:CompareValidator ID="compval" Display="dynamic" ControlToValidate="txt_password"
                                            ControlToCompare="txt_user_id" Font-Size="10px" Type="String" Operator="NotEqual"
                                            Text="User ID and Password cannot be same" ValidationGroup="val_signup_info"
                                            runat="server" />
                                    </div>
                                    <%--(?=.*\d)(?=.*[a-zA-Z]).{6,}--%>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Retype Password:</label><span style="color: Red; font-size: 10px;">*</span>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_retype_password" runat="server" MaxLength="20" CssClass="TextBoxField"
                                            TextMode="Password"></asp:TextBox><br />
                                        <asp:CompareValidator ID="comp_retype_password" runat="server" Font-Size="10px" ControlToValidate="txt_retype_password"
                                            ControlToCompare="txt_password" CssClass="error" Display="Dynamic" ValidationGroup="val_signup_info"
                                            ErrorMessage="Password Mismatch"></asp:CompareValidator>
                                        <asp:RequiredFieldValidator Font-Size="10px" ID="RequiredFieldValidator1" runat="server"
                                            ControlToValidate="txt_retype_password" ValidationGroup="val_signup_info" Display="Dynamic"
                                            ErrorMessage="Retype Password Required" CssClass="error"></asp:RequiredFieldValidator>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Add comments here:</label>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:TextBox ID="txt_comment" runat="server" MaxLength="5000" CssClass="TextBoxField"
                                            Width="155px" Height="45px" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br />
                                    <h4>
                                        STEP 4: Other details</h4>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Buckets :</label>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <div class="cls_optout">
                                            <asp:CheckBox ID="chk_optout" onclick="javascript: optoutmode(this);" runat="server" Text="Do not send me any emails"    runat="server"/>
                                        </div>
                                        
                                    </div>
                                </td>
                            </tr>
                            <tr>
                            <td colspan="2" style="padding-bottom:20px;">
                            <table id="tb_rpt_bucket" cellpadding="0" cellspacing="1" style="border:0px solid red;  background-color: Gray;">
                                            <tr>
                                                <asp:Repeater ID="rpt_bucket" runat="server">
                                                    <ItemTemplate>
                                                        <td valign="top" style="background-color: White;">
                                                            <div class="bktHeader">
                                                                <%# Eval("bucket_name") %>
                                                            </div>
                                                            <asp:Literal ID="lit_bucket_id" runat="server" Text='<%# Eval("bucket_id") %>' Visible="false"></asp:Literal>
                                                            <asp:Repeater ID="rpt_bucket_value" runat="server">
                                                                <ItemTemplate>
                                                                    <div class="bucket_width">
                                                                    <asp:Literal ID="lit_bucket_id" runat="server" Text='<%# Eval("bucket_id") %>' Visible="false"></asp:Literal>
                                                                    <asp:Literal ID="lit_bucket_value_id" runat="server" Text='<%# Eval("bucket_value_id") %>'
                                                                        Visible="false"></asp:Literal>
                                                                    <div class='<%# iif(container.itemindex mod 2 =0,"bucketStyle","bucketStyleAlternate") %>'>
                                                                        <asp:CheckBox ID="chk_select" runat="server" Text='<%# Eval("bucket_value") %>' />
                                                                    </div>
                                                                        </div>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                  
                                            </tr>
                                        </table>
                            </td>
                            </tr>
                            <tr runat="server" id="tr_company">
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        Linked Companies:</label>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:CheckBoxList ID="chklst_companies_linked" Font-Size="12px" runat="server" RepeatColumns="2"
                                            RepeatDirection="Horizontal" Width="100%" CellPadding="0" CellSpacing="0" BorderWidth="0">
                                        </asp:CheckBoxList>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table border="0" cellspacing="0" cellpadding="0" class="EditingFormLabel">
                                      <tr><td>Resale Certificate <span style="color: Red; font-size: 10px;">*</span></td></tr>
                                      <tr><td style="font-size:14px;padding-top:5px; padding-bottom:5px;padding-left:45px;" >OR</td></tr>
                                      <tr><td>Company Registration Documents:</td></tr>
                                    </table>
                                    
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:FileUpload ID="file_upload1" runat="server" CssClass="file_upload" /> 
                                        <asp:RegularExpressionValidator ID="rev_file_upload" EnableClientScript="true" CssClass="error"  ValidationGroup="val_signup_info"
                                            runat="server" ErrorMessage="<br/>Only PDF and Image Files are allowed!" ValidationExpression="^.+\.((jpg)|(JPG)|(gif)|(GIF)|(jpeg)|(JPEG)|(png)|(PNG)|(bmp)|(BMP)|(pdf)|(PDF))$" ControlToValidate="file_upload1"> </asp:RegularExpressionValidator>

                                        <asp:CustomValidator runat="server" ID="cust_check_resale" EnableClientScript="true" CssClass="error"
                                            ValidationGroup="val_signup_info" ErrorMessage="<br />Resale Certificate or Company Registration documents Required. Size of the file should be less than 4MB"
                                            OnServerValidate="check_resale_validate"></asp:CustomValidator>
                                                                           </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label class="EditingFormLabel">
                                        How did you find us?:</label>
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl">
                                        <asp:DropDownList ID="ddl_how_find_us" Font-Size="12px" runat="server" Width="155px">
                                        </asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    &nbsp;
                                </td>
                                <td>
                                    <div class="EditingFormControlNestedControl" style="padding: 0px 0px 0px 0px; color: Blue;">
                                        <asp:CheckBox ID="chk_accept_term" runat="server" Text="<a href='#' onClick='return open_pop();'>Accept our Terms & Conditions</a>"
                                            TextAlign="Right" />
                                        <asp:CustomValidator ID="customvalidate" runat="server" ValidationGroup="val_signup_info" CssClass="error"
                                            OnServerValidate="check_term" ForeColor="Red" ErrorMessage="Please check our terms & conditions."></asp:CustomValidator>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                        </p>
                    <br />
                    <asp:Button ID="imgbtn_basic_save" runat="server" ValidationGroup="val_signup_info"
                        Text="Submit" />
                    <asp:ValidationSummary ID="summary" runat="server" CssClass="validation_summary_as_bulletlist"
                        EnableClientScript="true" DisplayMode="BulletList" HeaderText="An error occurred, Please scroll up for details"
                        ShowMessageBox="false" ShowSummary="true" ValidationGroup="val_signup_info" />
                </div>
                <!--products
    -->
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
</asp:Content>
