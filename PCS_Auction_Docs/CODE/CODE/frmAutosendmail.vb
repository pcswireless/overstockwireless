Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Mail
Public Class frmAutosendmail
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 5000
        '
        'frmAutosendmail
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(274, 168)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmAutosendmail"
        Me.Opacity = 0
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Auto Mail Sending Application"

    End Sub

#End Region

    'Dim dt As DataTable                                             'datatable Object
    Dim Cmd_R As SqlCommand                                         'SqlCommand Object for Remote Connection
    Dim Da_R As SqlDataAdapter                                      'SqlDataAdapter Object for Client Connection
    Dim SqlTrans_R As SqlTransaction

    Private Sub frmAutosendmail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            ReadTextFile()
            SendMail()
        Catch ex As Exception
            SendMailAllToSupport("support@propertymixer.com", "bimal.kumar@enidus.com", "Error Occured In Auto Mail Sending Application", "Hello Dear, auto mail sending application is not working properly. Please review the caution. " & ex.Message, "minal.arora@propertymixer.com")
        End Try
    End Sub

    Private Sub SendMailAllToSupport(ByVal MsgFrom As String, ByVal MsgTo As String, ByVal MsgSubject As String, ByVal MsgBody As String, ByVal MsgCc As String)
     Try
            Dim smtpClient As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient()

            Dim Message As New System.Net.Mail.MailMessage

            Message.From = New System.Net.Mail.MailAddress(MsgFrom, "Support Team")
            Message.To.Add(New System.Net.Mail.MailAddress(MsgTo))
            Message.CC.Add(New System.Net.Mail.MailAddress(MsgCc))
            Message.Bcc.Add(New System.Net.Mail.MailAddress("support@propertymixer.com"))
            Message.Subject = MsgSubject
            Message.IsBodyHtml = True
            Message.Body = MsgBody
            Message.Priority = Net.Mail.MailPriority.High

            Dim SMTPUserInfo As System.Net.NetworkCredential = New System.Net.NetworkCredential("support@propertymixer.com", "enidus")
            smtpClient.UseDefaultCredentials = False
            smtpClient.Credentials = SMTPUserInfo
            smtpClient.Send(Message)
        Catch ex As Exception
           ' MsgBox(ex.Message)
        End Try

    End Sub

    Public Sub SendMail()
        Timer1.Enabled = False
        Try
            Dim maildt As New DataTable
            maildt = FetchFromDatabase("select top 10 * from iip_EmailSchedule where Status=0 order by Priority")
            Dim i As Integer = 0
            If Not maildt Is Nothing Then
                If (maildt.Rows.Count > 0) Then
                    For i = 0 To maildt.Rows.Count - 1
                        SendMailAll(maildt.Rows(i)(1), maildt.Rows(i)(2), maildt.Rows(i)(3), maildt.Rows(i)(4), maildt.Rows(i)(0), maildt.Rows(i)(11), maildt.Rows(i)(12))
                    Next
                End If
            End If
            Timer1.Enabled = True
        Catch ex As Exception
            SendMailAllToSupport("support@propertymixer.com", "bimal.kumar@enidus.com", "Error Occured In Auto Mail Sending Application", "Hello Dear, auto mail sending application is not working properly. Please review the caution. " & ex.Message, "minal.arora@propertymixer.com")
        End Try

    End Sub

    Private Sub SendMailAll(ByVal MsgFrom As String, ByVal MsgTo As String, ByVal MsgSubject As String, ByVal MsgBody As String, ByVal id As Integer, ByVal MsgCC As String, ByVal MsgBCC As String)
        Try
            Dim smtpClient As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient()
           
            Dim Message As New System.Net.Mail.MailMessage
            Dim i As Integer
            Dim arr1() As String
            Dim str1 As String = ""
            Dim SMTP As String = "", UID As String = "", PWD As String = ""
            GetSMTPDetail(SMTP, UID, PWD)

            Message.From = New System.Net.Mail.MailAddress(MsgFrom, "Support Team")  'MsgFrom
            Message.To.Add(New System.Net.Mail.MailAddress(MsgTo))

            arr1 = MsgCC.Split(",")

            For i = 0 To arr1.Length - 1
                str1 = Trim(arr1(i))
                If str1 <> "" Then
                    Message.CC.Add(New System.Net.Mail.MailAddress(str1))
                End If
            Next

            arr1 = MsgBCC.Split(",")

            For i = 0 To arr1.Length - 1
                str1 = Trim(arr1(i))
                If str1 <> "" Then
                    Message.Bcc.Add(New System.Net.Mail.MailAddress(str1))
                End If
            Next

            Message.Subject = MsgSubject
            Message.IsBodyHtml = True
            Message.Body = MsgBody
            Message.Priority = Net.Mail.MailPriority.Normal

            smtpClient.Host = "74.55.65.98"
            'Dim SMTPUserInfo As System.Net.NetworkCredential = New System.Net.NetworkCredential(UID, PWD)
            'smtpClient.UseDefaultCredentials = False
            'smtpClient.Credentials = SMTPUserInfo
            smtpClient.Send(Message)
            System.Threading.Thread.Sleep(4000)
            UpdateTable(id)
        Catch ex As Exception
            ' MsgBox(ex.Message)
        End Try

    End Sub

    Public Sub UpdateTable(ByVal str As Integer)
        Try
            Dim con As New SqlConnection(strConnection)
            If con.State = ConnectionState.Closed Then con.Open()
            SqlTrans_R = Nothing
            SqlTrans_R = con.BeginTransaction
            Cmd_R = New SqlCommand("update iip_EmailSchedule set Status=1 where ScheduleID=" & str & " ", con, SqlTrans_R)
            Cmd_R.ExecuteNonQuery()
            SqlTrans_R.Commit()
            If con.State = ConnectionState.Open Then con.Close()
        Catch ex As Exception
            SendMailAllToSupport("support@propertymixer.com", "bimal.kumar@enidus.com", "Error Occured In Auto Mail Sending Application", "Hello Dear, auto mail sending application is not working properly. Please review the caution. " & ex.Message, "minal.arora@propertymixer.com")
        End Try
    End Sub

    Public Function FetchFromDatabase(ByVal SqlStr As String) As DataTable
        Dim dt As New DataTable
        Try
            Dim con As New SqlConnection(strConnection)
            If con.State = ConnectionState.Closed Then con.Open()
            Da_R = New SqlDataAdapter(SqlStr, con)
            Da_R.Fill(dt)
            If con.State = ConnectionState.Open Then con.Close()
        Catch ex As Exception
            'MsgBox(ex.Message)
            dt = Nothing
            FetchFromDatabase = dt
            Exit Function
        End Try
        FetchFromDatabase = dt

    End Function

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try
            ReadTextFile()
            SendMail()
        Catch ex As Exception
            SendMailAllToSupport("support@propertymixer.com", "bimal.kumar@enidus.com", "Error Occured In Auto Mail Sending Application", "Hello Dear, auto mail sending application is not working properly. Please review the caution. " & ex.Message, "minal.arora@propertymixer.com")
        End Try

    End Sub

    Public Sub ReadTextFile()
        Try
            Dim oRead As System.IO.StreamReader
            Dim LineIn As String = ""
            oRead = File.OpenText(Application.StartupPath & "\PMConnection.ini")
            ' While oRead.Peek - 1
            LineIn &= oRead.ReadLine()
            'End While
            strConnection = LineIn
            oRead.Close()
        Catch ex As Exception
            SendMailAllToSupport("support@propertymixer.com", "bimal.kumar@enidus.com", "Error Occured In Auto Mail Sending Application", "Hello Dear, auto mail sending application is not working properly. Please review the caution. " & ex.Message, "minal.arora@propertymixer.com")
        End Try

    End Sub

    Public Sub GetSMTPDetail(ByRef SMTP As String, ByRef UID As String, ByRef PWD As String)
        Try
            Dim smtpRead As System.IO.StreamReader
            Dim LineDetail(2) As String
            Dim CheckStr As String
            Dim i As Integer = 0
            smtpRead = File.OpenText(Application.StartupPath & "\PMsmtp.ini")
            While smtpRead.Peek <> -1
                CheckStr = Trim(smtpRead.ReadLine)
                If CheckStr Is Nothing Then
                Else
                    LineDetail(i) = Trim(CheckStr.Trim)
                    i = i + 1
                End If
            End While
            SMTP = LineDetail(0)
            PWD = LineDetail(1)
            UID = LineDetail(2)
            smtpRead.Close()
        Catch ex As Exception
            SendMailAllToSupport("support@propertymixer.com", "bimal.kumar@enidus.com", "Error Occured In Auto Mail Sending Application", "Hello Dear, auto mail sending application is not working properly. Please review the caution. " & ex.Message, "minal.arora@propertymixer.com")
        End Try

    End Sub
End Class
