﻿Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data
Public Class SqlHelper
    Public Shared Sub ExecuteNonQuery(ByVal strConnection As String, ByVal strQuery As String)
        Using connection As New SqlConnection(strConnection)

            Dim command As New SqlCommand(strQuery, connection)
            Try
                command.Connection.Open()
                command.ExecuteNonQuery()
                command.Connection.Close()
            Catch ex As Exception
            Finally
                If command.Connection.State = ConnectionState.Open Then command.Connection.Close()
            End Try
        End Using

    End Sub
    Public Shared Function ExecuteDataset(ByVal strConnection As String, ByVal strQuery As String) As DataSet
        Dim dataSet As DataSet = New DataSet()
        Using connection As SqlConnection = New SqlConnection(strConnection)

            Dim sqlAdapter As SqlDataAdapter = New SqlDataAdapter()
            Try
                connection.Open()

                Dim sqlCommand As SqlCommand = New SqlCommand(strQuery, connection)
                sqlCommand.CommandType = CommandType.Text
                sqlAdapter.SelectCommand = sqlCommand
                sqlAdapter.Fill(dataSet)
            Finally
                If connection.State = ConnectionState.Open Then connection.Close()
            End Try


        End Using
        Return dataSet
    End Function

    Public Shared Function ExecuteScalar(ByVal strConnection As String, ByVal strQuery As String) As Object
        Dim obj As Object
        Using connection As New SqlConnection(strConnection)
            Dim command As New SqlCommand(strQuery, connection)
            Try
                command.Connection.Open()
                obj = command.ExecuteScalar()
                command.Connection.Close()
            Finally
                If command.Connection.State = ConnectionState.Open Then command.Connection.Close()
            End Try
        End Using
        Return obj
    End Function
    Public Shared Function ExecuteDatatable(ByVal strConnection As String, ByVal strQuery As String) As DataTable
        Dim Datatable As DataTable = New DataTable()
        Using connection As SqlConnection = New SqlConnection(strConnection)

            Dim sqlAdapter As SqlDataAdapter = New SqlDataAdapter()
            Try
                connection.Open()

                Dim sqlCommand As SqlCommand = New SqlCommand(strQuery, connection)
                sqlCommand.CommandType = CommandType.Text
                sqlAdapter.SelectCommand = sqlCommand
                sqlAdapter.Fill(Datatable)
            Finally
                If connection.State = ConnectionState.Open Then connection.Close()
            End Try


        End Using
        Return Datatable
    End Function
End Class
