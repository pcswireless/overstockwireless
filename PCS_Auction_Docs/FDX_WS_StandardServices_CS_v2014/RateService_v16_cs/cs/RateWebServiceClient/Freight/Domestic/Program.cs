// This code was built using Visual Studio 2005
using System;
using System.Web.Services.Protocols;
using RateWebServiceClient.RateServiceWebReference;

// Sample code to call the FedEx Rate Web Service for Freight
// Tested with Microsoft Visual Studio 2005 Professional Edition

namespace RateWebServiceClient
{
    class Program
    {
        static void Main(string[] args)
        {
            RateRequest request = CreateRateRequest();
            //
            RateService service = new RateService();
			if (usePropertyFile())
            {
                service.Url = getProperty("endpoint");
            }
            try
            {
                // Call the web service passing in a RateRequest and returning a RateReply
                RateReply reply = service.getRates(request);

                if (reply.HighestSeverity == NotificationSeverityType.SUCCESS || reply.HighestSeverity == NotificationSeverityType.NOTE || reply.HighestSeverity == NotificationSeverityType.WARNING)
                {
                    ShowRateReply(reply);
                }
                else
                {
                    ShowNotifications(reply);
                }
            }
            catch (SoapException e)
            {
                Console.WriteLine(e.Detail.InnerText);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Press any key to quit!");
            Console.ReadKey();
        }

        private static RateRequest CreateRateRequest()
        {
            // Build a RateRequest
            RateRequest request = new RateRequest();
            //
            request.WebAuthenticationDetail = new WebAuthenticationDetail();
            request.WebAuthenticationDetail.UserCredential = new WebAuthenticationCredential();
            request.WebAuthenticationDetail.UserCredential.Key = "XXX"; // Replace "XXX" with the Key
            request.WebAuthenticationDetail.UserCredential.Password = "XXX"; // Replace "XXX" with the Password
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.WebAuthenticationDetail.UserCredential.Key = getProperty("key");
                request.WebAuthenticationDetail.UserCredential.Password = getProperty("password");
            }
            //
            request.ClientDetail = new ClientDetail();
            request.ClientDetail.AccountNumber = "XXX"; // Replace "XXX" with the client's account number
            request.ClientDetail.MeterNumber = "XXX"; // Replace "XXX" with the client's meter number
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.ClientDetail.AccountNumber = getProperty("accountnumber");
                request.ClientDetail.MeterNumber = getProperty("meternumber");
            }
            //
            request.TransactionDetail = new TransactionDetail();
            request.TransactionDetail.CustomerTransactionId = "***Freight Rate Request using VC#***"; // This is a reference field for the customer.  Any value can be used and will be provided in the response.
            //
            request.Version = new VersionId();
            //
            request.ReturnTransitAndCommit = true;
            request.ReturnTransitAndCommitSpecified = true;
            request.CarrierCodes = new CarrierCodeType[1];
            request.CarrierCodes[0] = CarrierCodeType.FXFR;
            //
            SetShipmentDetails(request);
            //
            return request;
        }

        private static void SetShipmentDetails(RateRequest request)
        {
            request.RequestedShipment = new RequestedShipment();
            request.RequestedShipment.ShipTimestamp = DateTime.Now; // Shipping date and time
            request.RequestedShipment.ShipTimestampSpecified = true;
            request.RequestedShipment.DropoffType = DropoffType.REGULAR_PICKUP; //Drop off types are BUSINESS_SERVICE_CENTER, DROP_BOX, REGULAR_PICKUP, REQUEST_COURIER, STATION
            // If ServiceType is omitted, all applicable ServiceTypes are returned.
            //request.RequestedShipment.ServiceType = ServiceType.FEDEX_FREIGHT_PRIORITY; // Service types are STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND ...
            //request.RequestedShipment.ServiceTypeSpecified = true;
            request.RequestedShipment.PackagingType = PackagingType.YOUR_PACKAGING; // Packaging type FEDEX_BOK, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
            request.RequestedShipment.PackagingTypeSpecified = true;
            //
            SetSender(request);
            //
            SetRecipient(request);
            //
            SetPayment(request);
            //
            SetFreightShipmentDetail(request);
            //
            request.RequestedShipment.PackageCount = "1";
        }

        private static void SetSender(RateRequest request)
        {
            request.RequestedShipment.Shipper = new Party();
            request.RequestedShipment.Shipper.Address = new Address();
            request.RequestedShipment.Shipper.Address.StreetLines = new string[1] { "SHIPPER ADDRESS LINE 1" };
            // Replace "XXX" with sender address
            request.RequestedShipment.Shipper.Address.City = "Harrison";
            request.RequestedShipment.Shipper.Address.StateOrProvinceCode = "AR";
            request.RequestedShipment.Shipper.Address.PostalCode = "72601";
            request.RequestedShipment.Shipper.Address.CountryCode = "US";
        }

        private static void SetRecipient(RateRequest request)
        {
            request.RequestedShipment.Recipient = new Party();
            request.RequestedShipment.Recipient.Address = new Address();
            request.RequestedShipment.Recipient.Address.StreetLines = new string[1] { "RECIPIENT ADDRESS LINE 1" };
            request.RequestedShipment.Recipient.Address.City = "COLORADO SPRINGS";
            request.RequestedShipment.Recipient.Address.StateOrProvinceCode = "CO";
            request.RequestedShipment.Recipient.Address.PostalCode = "80915";
            request.RequestedShipment.Recipient.Address.CountryCode = "US";
        }

        private static void SetPayment(RateRequest request)
        {
            request.RequestedShipment.ShippingChargesPayment = new Payment();
            request.RequestedShipment.ShippingChargesPayment.PaymentType = PaymentType.SENDER;
            request.RequestedShipment.ShippingChargesPayment.PaymentTypeSpecified = true;
            request.RequestedShipment.ShippingChargesPayment.Payor = new Payor();
            request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty = new Party();
            request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.AccountNumber = "XXX"; // Replace "XXX" with client's account number
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.AccountNumber = getProperty("payoraccount");
            }
            request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Contact = new Contact();
            request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Address = new Address();
            request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Address.CountryCode = "US";
        }

        private static void SetFreightShipmentDetail(RateRequest request)
        {
            request.RequestedShipment.FreightShipmentDetail = new FreightShipmentDetail();
            request.RequestedShipment.FreightShipmentDetail.FedExFreightAccountNumber = "XXX"; // Replace "XXX" with the client's account number
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.RequestedShipment.FreightShipmentDetail.FedExFreightAccountNumber = getProperty("freightaccount");
            }
            SetFreightBillingContactAddress(request);
            request.RequestedShipment.FreightShipmentDetail.Role = FreightShipmentRoleType.SHIPPER;
            request.RequestedShipment.FreightShipmentDetail.RoleSpecified = true;
            SetFreightDeclaredValue(request);
            SetFreightLiabilityCoverageDetail(request);
            request.RequestedShipment.FreightShipmentDetail.TotalHandlingUnits = "15";
            SetFreightShipmentDimensions(request);
            SetFreightShipmentLineItems(request);
        }

        private static void SetFreightBillingContactAddress(RateRequest request)
        {
            request.RequestedShipment.FreightShipmentDetail.FedExFreightBillingContactAndAddress = new ContactAndAddress();
            request.RequestedShipment.FreightShipmentDetail.FedExFreightBillingContactAndAddress.Contact = new Contact();
            request.RequestedShipment.FreightShipmentDetail.FedExFreightBillingContactAndAddress.Contact.PersonName = "Freight Billing Contact";
            request.RequestedShipment.FreightShipmentDetail.FedExFreightBillingContactAndAddress.Contact.CompanyName = "Freight Billing Company";
            request.RequestedShipment.FreightShipmentDetail.FedExFreightBillingContactAndAddress.Contact.PhoneNumber = "1234567890";
            //
            request.RequestedShipment.FreightShipmentDetail.FedExFreightBillingContactAndAddress.Address = new Address();
            request.RequestedShipment.FreightShipmentDetail.FedExFreightBillingContactAndAddress.Address.StreetLines = new string[1] { "FREIGHT BILLING ADDRESS LINE 1" };
            // Replace "XXX" with Freight billing address
            request.RequestedShipment.FreightShipmentDetail.FedExFreightBillingContactAndAddress.Address.City = "Harrison";
            request.RequestedShipment.FreightShipmentDetail.FedExFreightBillingContactAndAddress.Address.StateOrProvinceCode = "AR";
            request.RequestedShipment.FreightShipmentDetail.FedExFreightBillingContactAndAddress.Address.PostalCode = "72601";
            request.RequestedShipment.FreightShipmentDetail.FedExFreightBillingContactAndAddress.Address.CountryCode = "US";
        }

        private static void SetFreightDeclaredValue(RateRequest request)
        {
            request.RequestedShipment.FreightShipmentDetail.DeclaredValuePerUnit = new Money();
            request.RequestedShipment.FreightShipmentDetail.DeclaredValuePerUnit.Currency = "USD";
            request.RequestedShipment.FreightShipmentDetail.DeclaredValuePerUnit.Amount = 50.0M;
            request.RequestedShipment.FreightShipmentDetail.DeclaredValuePerUnit.AmountSpecified = true;
        }

        private static void SetFreightLiabilityCoverageDetail(RateRequest request)
        {
            request.RequestedShipment.FreightShipmentDetail.LiabilityCoverageDetail = new LiabilityCoverageDetail();
            request.RequestedShipment.FreightShipmentDetail.LiabilityCoverageDetail.CoverageType = LiabilityCoverageType.NEW;
            request.RequestedShipment.FreightShipmentDetail.LiabilityCoverageDetail.CoverageTypeSpecified = true;
            request.RequestedShipment.FreightShipmentDetail.LiabilityCoverageDetail.CoverageAmount = new Money();
            request.RequestedShipment.FreightShipmentDetail.LiabilityCoverageDetail.CoverageAmount.Currency = "USD";
            request.RequestedShipment.FreightShipmentDetail.LiabilityCoverageDetail.CoverageAmount.Amount = 50.0M;
            request.RequestedShipment.FreightShipmentDetail.LiabilityCoverageDetail.CoverageAmount.AmountSpecified = true;
        }

        private static void SetFreightShipmentDimensions(RateRequest request)
        {
            request.RequestedShipment.FreightShipmentDetail.ShipmentDimensions = new Dimensions();
            request.RequestedShipment.FreightShipmentDetail.ShipmentDimensions.Length = "90";
            request.RequestedShipment.FreightShipmentDetail.ShipmentDimensions.Width = "60";
            request.RequestedShipment.FreightShipmentDetail.ShipmentDimensions.Height = "50";
            request.RequestedShipment.FreightShipmentDetail.ShipmentDimensions.Units = LinearUnits.IN;
            request.RequestedShipment.FreightShipmentDetail.ShipmentDimensions.UnitsSpecified = true;
        }

        private static void SetFreightShipmentLineItems(RateRequest request)
        {
            request.RequestedShipment.FreightShipmentDetail.LineItems = new FreightShipmentLineItem[1];
            request.RequestedShipment.FreightShipmentDetail.LineItems[0] = new FreightShipmentLineItem();
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].FreightClass = FreightClassType.CLASS_050;
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].FreightClassSpecified = true;
            //
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Packaging = PhysicalPackagingType.BOX;
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].PackagingSpecified = true;
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Description = "Freight line item description";
            //
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Weight = new Weight();
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Weight.Units = WeightUnits.LB;
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Weight.UnitsSpecified = true;
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Weight.Value = 1000.0M;
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Weight.ValueSpecified = true;
            //
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Dimensions = new Dimensions();
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Dimensions.Length = "90";
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Dimensions.Width = "60";
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Dimensions.Height = "50";
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Dimensions.Units = LinearUnits.IN;
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Dimensions.UnitsSpecified = true;
            //
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Volume = new Volume();
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Volume.Units = VolumeUnits.CUBIC_FT;
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Volume.UnitsSpecified = true;
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Volume.Value = 30M;
            request.RequestedShipment.FreightShipmentDetail.LineItems[0].Volume.ValueSpecified = true;
        }

        private static void ShowRateReply(RateReply reply)
        {
            Console.WriteLine("RateReply details:");
            foreach (RateReplyDetail rateDetail in reply.RateReplyDetails)
            {
                Console.WriteLine("ServiceType: {0}", rateDetail.ServiceType);
                Console.WriteLine();
                foreach (RatedShipmentDetail shipmentDetail in rateDetail.RatedShipmentDetails)
                {
                    ShowShipmentRateDetails(shipmentDetail);
                }
                ShowDeliveryDetails(rateDetail);
                Console.WriteLine("**********************************************************");
            }
            ShowNotifications(reply);
        }

        private static void ShowShipmentRateDetails(RatedShipmentDetail shipmentDetail)
        {
            if (shipmentDetail == null) return;
            if (shipmentDetail.ShipmentRateDetail == null) return;
            ShipmentRateDetail rateDetail = shipmentDetail.ShipmentRateDetail;
            //
            Console.WriteLine("RateType: {0}", rateDetail.RateType);
            if (rateDetail.TotalBillingWeight != null) Console.WriteLine("Total Billing Weight: {0} {1}", rateDetail.TotalBillingWeight.Value, shipmentDetail.ShipmentRateDetail.TotalBillingWeight.Units);
            if (rateDetail.TotalBaseCharge != null) Console.WriteLine("Total Base Charge: {0} {1}", rateDetail.TotalBaseCharge.Amount, rateDetail.TotalBaseCharge.Currency);
            if (rateDetail.TotalFreightDiscounts != null) Console.WriteLine("Total Freight Discounts: {0} {1}", rateDetail.TotalFreightDiscounts.Amount, rateDetail.TotalFreightDiscounts.Currency);
            if (rateDetail.TotalSurcharges != null) Console.WriteLine("Total Surcharges: {0} {1}", rateDetail.TotalSurcharges.Amount, rateDetail.TotalSurcharges.Currency);
            if (rateDetail.Surcharges != null)
            {
                // Individual surcharge for each package
                foreach (Surcharge surcharge in rateDetail.Surcharges)
                    Console.WriteLine(" {0} surcharge {1} {2}", surcharge.SurchargeType, surcharge.Amount.Amount, surcharge.Amount.Currency);
            }
            if (rateDetail.TotalNetCharge != null) Console.WriteLine("Total Net Charge: {0} {1}", rateDetail.TotalNetCharge.Amount, rateDetail.TotalNetCharge.Currency);
            ShowFreightRateDetail(rateDetail.FreightRateDetail);
        }

        private static void ShowFreightRateDetail(FreightRateDetail freightRateDetail)
        {
            if (freightRateDetail == null) return;
            Console.WriteLine();
            Console.WriteLine("Freight Rate details");
            if (freightRateDetail.QuoteNumber != null)
                Console.WriteLine("Quote number {0} ", freightRateDetail.QuoteNumber);
            // Individual FreightBaseCharge for each shipment
            foreach (FreightBaseCharge freightBaseCharge in freightRateDetail.BaseCharges)
            {
                if (freightBaseCharge.Description != null)
                    Console.WriteLine("Description " + freightBaseCharge.Description);
                if (freightBaseCharge.Weight != null)
                    Console.WriteLine("Weight {0} {1} ", freightBaseCharge.Weight.Value, freightBaseCharge.Weight.Units);
                if (freightBaseCharge.ChargeRate != null)
                    Console.WriteLine("Charge rate {0} {1} ", freightBaseCharge.ChargeRate.Amount, freightBaseCharge.ChargeRate.Currency);
                if (freightBaseCharge.ExtendedAmount != null)
                    Console.WriteLine("Extended amount {0} {1} ", freightBaseCharge.ExtendedAmount.Amount, freightBaseCharge.ExtendedAmount.Currency);
                Console.WriteLine();
            }
        }

        private static void ShowDeliveryDetails(RateReplyDetail rateDetail)
        {
            if (rateDetail.DeliveryTimestampSpecified)
                Console.WriteLine("Delivery timestamp: " + rateDetail.DeliveryTimestamp.ToString());
            if (rateDetail.TransitTimeSpecified)
                Console.WriteLine("Transit Time: " + rateDetail.TransitTime);
        }

        private static void ShowNotifications(RateReply reply)
        {
            Console.WriteLine("Notifications");
            for (int i = 0; i < reply.Notifications.Length; i++)
            {
                Notification notification = reply.Notifications[i];
                Console.WriteLine("Notification no. {0}", i);
                if (notification.SeveritySpecified)
                    Console.WriteLine(" Severity: {0}", notification.Severity);
                Console.WriteLine(" Code: {0}", notification.Code);
                Console.WriteLine(" Message: {0}", notification.Message);
                Console.WriteLine(" Source: {0}", notification.Source);
            }
        }
        private static bool usePropertyFile() //Set to true for common properties to be set with getProperty function.
        {
            return getProperty("usefile").Equals("True");
        }
        private static String getProperty(String propertyname) //Sets common properties for testing purposes.
        {
            try
            {
                String filename = "C:\\filepath\\filename.txt";
                if (System.IO.File.Exists(filename))
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(filename);
                    do
                    {
                        String[] parts = sr.ReadLine().Split(',');
                        if (parts[0].Equals(propertyname) && parts.Length == 2)
                        {
                            return parts[1];
                        }
                    }
                    while (!sr.EndOfStream);
                }
                Console.WriteLine("Property {0} set to default 'XXX'", propertyname);
                return "XXX";
            }
            catch (Exception e)
            {
                Console.WriteLine("Property {0} set to default 'XXX'", propertyname);
                return "XXX";
            }
        }
    }
}