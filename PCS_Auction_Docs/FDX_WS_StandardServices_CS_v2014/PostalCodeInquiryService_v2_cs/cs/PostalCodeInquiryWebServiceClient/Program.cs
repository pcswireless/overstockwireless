//This code was built using Visual Studio 2005
using System;
using System.Web.Services.Protocols;
using PostalCodeInquiryWebServiceClient.PostalCodeInquiryServiceWebReference;

namespace PostalCodeInquiryWebServiceClient
{
    class Program
    {
        static void Main(string[] args)
        {
            PostalCodeInquiryRequest request = CreatePostalCodeInquiryRequest();
            //
            CountryService service = new CountryService();
			if (usePropertyFile())
            {
                service.Url = getProperty("endpoint");
            }
            //
            try
            {
                PostalCodeInquiryReply reply = service.postalCodeInquiry(request);
                if(reply.HighestSeverity == NotificationSeverityType.SUCCESS || reply.HighestSeverity == NotificationSeverityType.NOTE || reply.HighestSeverity == NotificationSeverityType.WARNING)
                {
                    ShowPostalCodeInquiryReply(reply);
                }
                else
                {
                    Console.WriteLine("Postal Code Inquiry failed : {0}", reply.Notifications[0].Message);
                }
            }
            catch (SoapException e)
            {
                Console.WriteLine(e.Detail.InnerText);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Press any key to quit!");
            Console.ReadKey();
        }

        private static PostalCodeInquiryRequest CreatePostalCodeInquiryRequest()
        {
            // Build the PostalCodeInquiryRequest
            PostalCodeInquiryRequest request = new PostalCodeInquiryRequest();
            //
            request.WebAuthenticationDetail = new WebAuthenticationDetail();
            request.WebAuthenticationDetail.UserCredential = new WebAuthenticationCredential();
            request.WebAuthenticationDetail.UserCredential.Key = "DayBelTIzrdgj97H"; // Replace "XXX" with the Key
            request.WebAuthenticationDetail.UserCredential.Password = "icnetbGDIhl9RII6YPFHhv9XP"; // Replace "XXX" with the Password
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.WebAuthenticationDetail.UserCredential.Key = getProperty("key");
                request.WebAuthenticationDetail.UserCredential.Password = getProperty("password");
            }
            //
            request.ClientDetail = new ClientDetail();
            request.ClientDetail.AccountNumber = "326787612"; // Replace "XXX" with the client's account number
            request.ClientDetail.MeterNumber = "104522487"; // Replace "XXX" with the client's meter number
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.ClientDetail.AccountNumber = getProperty("accountnumber");
                request.ClientDetail.MeterNumber = getProperty("meternumber");
            }
            //
            request.TransactionDetail = new TransactionDetail();
            request.TransactionDetail.CustomerTransactionId = "***Postal Code Inquiry Service Request using VC#***"; // The client will get the same value back in the response
            //
            request.Version = new VersionId(); // Creates the Version element with all child elements populated from the wsdl
            //
            request.PostalCode = "11204";
            request.CountryCode = "US";
            return request;
        }

        private static void ShowPostalCodeInquiryReply(PostalCodeInquiryReply reply)
        {
            Console.WriteLine("PostalCodeInquiryReply details:");
            if (reply.ExpressDescription != null)
            {
                Console.WriteLine("--- Express ---");
                Console.WriteLine("Location Id - " + reply.ExpressDescription.LocationId);
                Console.WriteLine("Postal Code - " + reply.ExpressDescription.PostalCode);
                Console.WriteLine("Service Area - " + reply.ExpressDescription.ServiceArea);
                Console.WriteLine("State/Province Code - " + reply.ExpressDescription.StateOrProvinceCode);
            }
            if (reply.ExpressFreightDescription != null)
            {
                Console.WriteLine("--- Express Freight ---");
                Console.WriteLine("Location Id - " + reply.ExpressFreightDescription.LocationId);
                Console.WriteLine("Postal Code - " + reply.ExpressFreightDescription.PostalCode);
                Console.WriteLine("Service Area - " + reply.ExpressFreightDescription.ServiceArea);
                Console.WriteLine("State/Province Code - " + reply.ExpressFreightDescription.StateOrProvinceCode);
            }
        }
        private static bool usePropertyFile() //Set to true for common properties to be set with getProperty function.
        {
            return getProperty("usefile").Equals("True");
        }
        private static String getProperty(String propertyname) //Sets common properties for testing purposes.
        {
            try
            {
                String filename = "C:\\filepath\\filename.txt";
                if (System.IO.File.Exists(filename))
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(filename);
                    do
                    {
                        String[] parts = sr.ReadLine().Split(',');
                        if (parts[0].Equals(propertyname) && parts.Length == 2)
                        {
                            return parts[1];
                        }
                    }
                    while (!sr.EndOfStream);
                }
                Console.WriteLine("Property {0} set to default 'XXX'", propertyname);
                return "XXX";
            }
            catch (Exception e)
            {
                Console.WriteLine("Property {0} set to default 'XXX'", propertyname);
                return "XXX";
            }
        }
    }
}