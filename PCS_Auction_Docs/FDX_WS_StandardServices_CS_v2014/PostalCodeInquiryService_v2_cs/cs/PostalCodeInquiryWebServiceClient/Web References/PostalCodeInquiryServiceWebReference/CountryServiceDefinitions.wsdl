<?xml version="1.0" encoding="utf-8"?>
<definitions xmlns:s1="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:ns="http://fedex.com/ws/cnty/v2" name="CountryServiceDefinitions" targetNamespace="http://fedex.com/ws/cnty/v2" xmlns="http://schemas.xmlsoap.org/wsdl/">
  <types>
    <xs:schema attributeFormDefault="qualified" elementFormDefault="qualified" targetNamespace="http://fedex.com/ws/cnty/v2" xmlns:xs="http://www.w3.org/2001/XMLSchema">
      <xs:element name="PostalCodeInquiryReply" type="ns:PostalCodeInquiryReply" />
      <xs:element name="PostalCodeInquiryRequest" type="ns:PostalCodeInquiryRequest" />
      <xs:complexType name="ClientDetail">
        <xs:annotation>
          <xs:documentation>Descriptive data for the client submitting a transaction.</xs:documentation>
        </xs:annotation>
        <xs:sequence>
          <xs:element minOccurs="1" name="AccountNumber" type="xs:string">
            <xs:annotation>
              <xs:documentation>The FedEx account number associated with this transaction.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="1" name="MeterNumber" type="xs:string">
            <xs:annotation>
              <xs:documentation>This number is assigned by FedEx and identifies the unique device from which the request is originating</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="0" name="MeterInstance" type="xs:string" />
          <xs:element minOccurs="0" name="IntegratorId" type="xs:string">
            <xs:annotation>
              <xs:documentation>Only used in transactions which require identification of the FedEx Office integrator.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="0" name="Region" type="ns:ExpressRegionCode">
            <xs:annotation>
              <xs:documentation>Indicates the region from which the transaction is submitted.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="0" name="Localization" type="ns:Localization">
            <xs:annotation>
              <xs:documentation>The language to be used for human-readable Notification.localizedMessages in responses to the request containing this ClientDetail object. Different requests from the same client may contain different Localization data. (Contrast with TransactionDetail.localization, which governs data payload language/translation.)</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:complexType>
      <xs:simpleType name="ExpressRegionCode">
        <xs:annotation>
          <xs:documentation>Indicates a FedEx Express operating region.</xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
          <xs:enumeration value="APAC" />
          <xs:enumeration value="CA" />
          <xs:enumeration value="EMEA" />
          <xs:enumeration value="LAC" />
          <xs:enumeration value="US" />
        </xs:restriction>
      </xs:simpleType>
      <xs:complexType name="Localization">
        <xs:annotation>
          <xs:documentation>Identifies the representation of human-readable text.</xs:documentation>
        </xs:annotation>
        <xs:sequence>
          <xs:element minOccurs="1" name="LanguageCode" type="xs:string">
            <xs:annotation>
              <xs:documentation>Two-letter code for language (e.g. EN, FR, etc.)</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="0" name="LocaleCode" type="xs:string">
            <xs:annotation>
              <xs:documentation>Two-letter code for the region (e.g. us, ca, etc..).</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:complexType>
      <xs:complexType name="Notification">
        <xs:annotation>
          <xs:documentation>The descriptive data regarding the result of the submitted transaction.</xs:documentation>
        </xs:annotation>
        <xs:sequence>
          <xs:element minOccurs="0" name="Severity" type="ns:NotificationSeverityType">
            <xs:annotation>
              <xs:documentation>The severity of this notification. This can indicate success or failure or some other information about the request. The values that can be returned are SUCCESS - Your transaction succeeded with no other applicable information. NOTE - Additional information that may be of interest to you about your transaction. WARNING - Additional information that you need to know about your transaction that you may need to take action on. ERROR - Information about an error that occurred while processing your transaction. FAILURE - FedEx was unable to process your transaction at this time due to a system failure. Please try again later</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="0" name="Source" type="xs:string">
            <xs:annotation>
              <xs:documentation>Indicates the source of this notification. Combined with the Code it uniquely identifies this notification</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="0" name="Code" type="xs:string">
            <xs:annotation>
              <xs:documentation>A code that represents this notification. Combined with the Source it uniquely identifies this notification.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="0" name="Message" type="xs:string">
            <xs:annotation>
              <xs:documentation>Human-readable text that explains this notification.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="0" name="LocalizedMessage" type="xs:string">
            <xs:annotation>
              <xs:documentation>The translated message. The language and locale specified in the ClientDetail. Localization are used to determine the representation. Currently only supported in a TrackReply.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="0" maxOccurs="unbounded" name="MessageParameters" type="ns:NotificationParameter">
            <xs:annotation>
              <xs:documentation>A collection of name/value pairs that provide specific data to help the client determine the nature of an error (or warning, etc.) witout having to parse the message string.</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:complexType>
      <xs:complexType name="NotificationParameter">
        <xs:sequence>
          <xs:element minOccurs="0" name="Id" type="xs:string">
            <xs:annotation>
              <xs:documentation>Identifies the type of data contained in Value (e.g. SERVICE_TYPE, PACKAGE_SEQUENCE, etc..).</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="0" name="Value" type="xs:string">
            <xs:annotation>
              <xs:documentation>The value of the parameter (e.g. PRIORITY_OVERNIGHT, 2, etc..).</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:complexType>
      <xs:simpleType name="NotificationSeverityType">
        <xs:restriction base="xs:string">
          <xs:enumeration value="ERROR" />
          <xs:enumeration value="FAILURE" />
          <xs:enumeration value="NOTE" />
          <xs:enumeration value="SUCCESS" />
          <xs:enumeration value="WARNING" />
        </xs:restriction>
      </xs:simpleType>
      <xs:complexType name="PostalCodeInquiryReply">
        <xs:sequence>
          <xs:element minOccurs="1" name="HighestSeverity" type="ns:NotificationSeverityType" />
          <xs:element minOccurs="1" maxOccurs="unbounded" name="Notifications" type="ns:Notification" />
          <xs:element minOccurs="0" name="TransactionDetail" type="ns:TransactionDetail" />
          <xs:element minOccurs="1" name="Version" type="ns:VersionId" />
          <xs:element minOccurs="0" name="ExpressFreightContractorDeliveryArea" type="xs:boolean" />
          <xs:element minOccurs="0" name="ExpressDescription" type="ns:PostalCodeServiceAreaDescription" />
          <xs:element minOccurs="0" name="ExpressFreightDescription" type="ns:PostalCodeServiceAreaDescription">
            <xs:annotation>
              <xs:documentation>Only service area field is currently provided for Express Freight.</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:complexType>
      <xs:complexType name="PostalCodeInquiryRequest">
        <xs:sequence>
          <xs:element minOccurs="1" name="WebAuthenticationDetail" type="ns:WebAuthenticationDetail">
            <xs:annotation>
              <xs:documentation>Descriptive data to be used in authentication of the sender's identity (and right to use FedEx web services).</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="1" name="ClientDetail" type="ns:ClientDetail" />
          <xs:element minOccurs="0" name="TransactionDetail" type="ns:TransactionDetail" />
          <xs:element minOccurs="1" name="Version" type="ns:VersionId" />
          <xs:element minOccurs="0" name="ShipDate" type="xs:date" />
          <xs:element minOccurs="0" name="PostalCode" type="xs:string">
            <xs:annotation>
              <xs:documentation>Only used with postal-aware countries.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="0" name="CountryCode" type="xs:string" />
        </xs:sequence>
      </xs:complexType>
      <xs:complexType name="PostalCodeServiceAreaDescription">
        <xs:sequence>
          <xs:element minOccurs="0" name="LocationId" type="xs:string" />
          <xs:element minOccurs="0" name="StateOrProvinceCode" type="xs:string" />
          <xs:element minOccurs="0" name="PostalCode" type="xs:string" />
          <xs:element minOccurs="0" name="ServiceArea" type="xs:string" />
        </xs:sequence>
      </xs:complexType>
      <xs:complexType name="TransactionDetail">
        <xs:sequence>
          <xs:element minOccurs="0" name="CustomerTransactionId" type="xs:string">
            <xs:annotation>
              <xs:documentation>Free form text to be echoed back in the reply. Used to match requests and replies.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="0" name="Localization" type="ns:Localization">
            <xs:annotation>
              <xs:documentation>Governs data payload language/translations (contrasted with ClientDetail.localization, which governs Notification.localizedMessage language selection).</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:complexType>
      <xs:complexType name="WebAuthenticationDetail">
        <xs:annotation>
          <xs:documentation>Used in authentication of the sender's identity.</xs:documentation>
        </xs:annotation>
        <xs:sequence>
          <xs:element minOccurs="1" name="UserCredential" type="ns:WebAuthenticationCredential">
            <xs:annotation>
              <xs:documentation>Credential used to authenticate a specific software application. This value is provided by FedEx after registration.</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:complexType>
      <xs:complexType name="WebAuthenticationCredential">
        <xs:annotation>
          <xs:documentation>Two part authentication string used for the sender's identity</xs:documentation>
        </xs:annotation>
        <xs:sequence>
          <xs:element minOccurs="1" name="Key" type="xs:string">
            <xs:annotation>
              <xs:documentation>Identifying part of authentication credential. This value is provided by FedEx after registration</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="1" name="Password" type="xs:string">
            <xs:annotation>
              <xs:documentation>Secret part of authentication key. This value is provided by FedEx after registration.</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:complexType>
      <xs:complexType name="VersionId">
        <xs:annotation>
          <xs:documentation>Identifies the version/level of a service operation expected by a caller (in each request) and performed by the callee (in each reply).</xs:documentation>
        </xs:annotation>
        <xs:sequence>
          <xs:element minOccurs="1" fixed="cnty" name="ServiceId" type="xs:string">
            <xs:annotation>
              <xs:documentation>Identifies a system or sub-system which performs an operation.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="1" fixed="2" name="Major" type="xs:int">
            <xs:annotation>
              <xs:documentation>Identifies the service business level.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="1" fixed="0" name="Intermediate" type="xs:int">
            <xs:annotation>
              <xs:documentation>Identifies the service interface level.</xs:documentation>
            </xs:annotation>
          </xs:element>
          <xs:element minOccurs="1" fixed="0" name="Minor" type="xs:int">
            <xs:annotation>
              <xs:documentation>Identifies the service code level.</xs:documentation>
            </xs:annotation>
          </xs:element>
        </xs:sequence>
      </xs:complexType>
    </xs:schema>
  </types>
  <message name="PostalCodeInquiryRequest">
    <part name="PostalCodeInquiryRequest" element="ns:PostalCodeInquiryRequest" />
  </message>
  <message name="PostalCodeInquiryReply">
    <part name="PostalCodeInquiryReply" element="ns:PostalCodeInquiryReply" />
  </message>
  <portType name="CountryPortType">
    <operation name="postalCodeInquiry" parameterOrder="PostalCodeInquiryRequest">
      <input message="ns:PostalCodeInquiryRequest" />
      <output message="ns:PostalCodeInquiryReply" />
    </operation>
  </portType>
  <binding name="CountryServiceSoapBinding" type="ns:CountryPortType">
    <s1:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <operation name="postalCodeInquiry">
      <s1:operation soapAction="http://fedex.com/ws/cnty/v2/postalCodeInquiry" style="document" />
      <input>
        <s1:body use="literal" />
      </input>
      <output>
        <s1:body use="literal" />
      </output>
    </operation>
  </binding>
  <service name="CountryService">
    <port name="CountryServicePort" binding="ns:CountryServiceSoapBinding">
      <s1:address location="https://wsbeta.fedex.com:443/web-services/cnty" />
    </port>
  </service>
</definitions>