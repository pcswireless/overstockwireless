// This code was built using Visual Studio 2005
using System;
using System.IO;
using System.Web.Services.Protocols;
using SPODWebServiceClient.SPODWebReference;

// Sample code to call the SignatureProofOfDeliveryLetterRequest Web Service
// Tested with Microsoft Visual Studio 2005 Professional Edition
namespace SPODWebServiceClient
{
    class Program
    {
        static void Main(string[] args)
        {
            SignatureProofOfDeliveryLetterRequest request = CreateSPODLetterRequest();
            //
            TrackService service = new TrackService();
			if (usePropertyFile())
            {
                service.Url = getProperty("endpoint");
            }
            //
            try
            {
                // Call the Track web service passing in a SignatureProofOfDeliveryLetterRequest and returning a SignatureProofOfDeliveryLetterReply
                SignatureProofOfDeliveryLetterReply reply = service.retrieveSignatureProofOfDeliveryLetter(request);
                if (reply.HighestSeverity == NotificationSeverityType.SUCCESS || reply.HighestSeverity == NotificationSeverityType.NOTE || reply.HighestSeverity == NotificationSeverityType.WARNING)
                {
                    if (null != reply.Letter)
                    {
                        SaveSPODLetter(reply.Letter);
                    }
                }
                ShowNotifications(reply);
            }
            catch (SoapException e)
            {
                Console.WriteLine(e.Detail.InnerText);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Press any key to quit!");
            Console.ReadKey();
        }

        private static SignatureProofOfDeliveryLetterRequest CreateSPODLetterRequest()
        {
            // Build the SignatureProofOfDeliveryLetterRequest
            SignatureProofOfDeliveryLetterRequest request = new SignatureProofOfDeliveryLetterRequest();
            //
            request.WebAuthenticationDetail = new WebAuthenticationDetail();
            request.WebAuthenticationDetail.UserCredential = new WebAuthenticationCredential();
            request.WebAuthenticationDetail.UserCredential.Key = "XXX"; // Replace "XXX" with the Key
            request.WebAuthenticationDetail.UserCredential.Password = "XXX"; // Replace "XXX" with the Password
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.WebAuthenticationDetail.UserCredential.Key = getProperty("key");
                request.WebAuthenticationDetail.UserCredential.Password = getProperty("password");
            }
            //
            request.ClientDetail = new ClientDetail();
            request.ClientDetail.AccountNumber = "XXX"; // Replace "XXX" with the client's account number
            request.ClientDetail.MeterNumber = "XXX"; // Replace "XXX" with the client's meter number
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.ClientDetail.AccountNumber = getProperty("accountnumber");
                request.ClientDetail.MeterNumber = getProperty("meternumber");
            }
            //
            request.TransactionDetail = new TransactionDetail();
            request.TransactionDetail.CustomerTransactionId = "***SPOD Request using VC#***"; //This is a reference field for the customer.  Any value can be used and will be provided in the response.
            request.TransactionDetail.Localization = new Localization();
            request.TransactionDetail.Localization.LanguageCode = "EN";
            //
            request.Version = new VersionId();
            //
            SetQualifiedTrackingNumber(request);
            //
            request.AdditionalComments = "XXX"; // Replace "XXX" with your comments, if any
            //
            request.LetterFormat = SignatureProofOfDeliveryImageType.PDF;
            request.LetterFormatSpecified = true;
            //
            SetConsignee(request);
            //
            return request;
        }

        private static void SetQualifiedTrackingNumber(SignatureProofOfDeliveryLetterRequest request)
        {
            request.QualifiedTrackingNumber = new QualifiedTrackingNumber();
            //request.QualifiedTrackingNumber.AccountNumber = "XXX"; // Replace "XXX" with client's account number
            request.QualifiedTrackingNumber.TrackingNumber = "XXX"; // Replace "XXX" with the tracking number
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.QualifiedTrackingNumber.TrackingNumber = getProperty("trackingnumber");
            }
            // Date range is optional.
            // If omitted, set to false
            request.QualifiedTrackingNumber.ShipDate = DateTime.Parse("04/17/2014"); // MM/DD/YYYY
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.QualifiedTrackingNumber.ShipDate = DateTime.Parse(getProperty("date"));
            }
            request.QualifiedTrackingNumber.ShipDateSpecified = true;
        }

        private static void SetConsignee(SignatureProofOfDeliveryLetterRequest request)
        {
            request.Consignee = new ContactAndAddress();
            request.Consignee.Contact = new Contact();
            request.Consignee.Contact.PersonName = "Ralph Johnson";
            request.Consignee.Contact.Title = "Mr";
            request.Consignee.Contact.CompanyName = "Farmer's Supply";
            request.Consignee.Contact.PhoneNumber = "9015550001";
        }

        private static void SaveSPODLetter(byte[] Letter)
        {
            // Save spod letter image to file
            String LabelPath = "c:\\";
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                LabelPath = getProperty("labelpath");
            }
            String LabelFileName = LabelPath + "spodletter" + ".pdf";
            FileStream LabelFile = new FileStream(LabelFileName, FileMode.Create);
            LabelFile.Write(Letter, 0, Letter.Length);
            LabelFile.Close();
            DisplaySPODReplyInAcrobat(LabelFileName);
        }

        private static void DisplaySPODReplyInAcrobat(string LabelFileName)
        {
            Console.WriteLine("SPOD Reply will display in Adobe Acrobat.");
            System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo(LabelFileName.ToString());
            info.UseShellExecute = true;
            info.Verb = "open";
            System.Diagnostics.Process.Start(info);
        }

        private static void ShowNotifications(SignatureProofOfDeliveryLetterReply reply)
        {
            Console.WriteLine("Notifications");
            for (int i = 0; i < reply.Notifications.Length; i++)
            {
                Notification notification = reply.Notifications[i];
                Console.WriteLine("Notification no. {0}", i);
                Console.WriteLine(" Severity: {0}", notification.Severity);
                Console.WriteLine(" Code: {0}", notification.Code);
                Console.WriteLine(" Message: {0}", notification.Message);
                Console.WriteLine(" Source: {0}", notification.Source);
            }
        }
        private static bool usePropertyFile() //Set to true for common properties to be set with getProperty function.
        {
            return getProperty("usefile").Equals("True");
        }
        private static String getProperty(String propertyname) //Sets common properties for testing purposes.
        {
            try
            {
                String filename = "C:\\filepath\\filename.txt";
                if (System.IO.File.Exists(filename))
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(filename);
                    do
                    {
                        String[] parts = sr.ReadLine().Split(',');
                        if (parts[0].Equals(propertyname) && parts.Length == 2)
                        {
                            return parts[1];
                        }
                    }
                    while (!sr.EndOfStream);
                }
                Console.WriteLine("Property {0} set to default 'XXX'", propertyname);
                return "XXX";
            }
            catch (Exception e)
            {
                Console.WriteLine("Property {0} set to default 'XXX'", propertyname);
                return "XXX";
            }
        }
    }
}