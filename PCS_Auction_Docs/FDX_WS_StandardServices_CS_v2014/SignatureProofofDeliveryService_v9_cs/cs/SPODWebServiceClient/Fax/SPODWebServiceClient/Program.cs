// This code was built using Visual Studio 2005
using System;
using System.Web.Services.Protocols;
using SPODWebServiceClient.SPODWebReference;

// Sample code to call the FedEx SignatureProofOfDeliveryFaxRequest Web Service
// Tested with Microsoft Visual Studio 2005 Professional Edition
namespace SPODWebServiceClient
{
    class Program
    {
        static void Main(string[] args)
        {
            SignatureProofOfDeliveryFaxRequest request = CreateSPODFaxRequest();
            //
            TrackService service = new TrackService();
			if (usePropertyFile())
            {
                service.Url = getProperty("endpoint");
            }
            //
            try
            {
                // Call the Track web service passing in a SignatureProofOfDeliveryFaxRequest and returning a SignatureProofOfDeliveryFaxReply
                SignatureProofOfDeliveryFaxReply reply = service.sendSignatureProofOfDeliveryFax(request);

                if (reply.HighestSeverity == NotificationSeverityType.SUCCESS || reply.HighestSeverity == NotificationSeverityType.NOTE || reply.HighestSeverity == NotificationSeverityType.WARNING)
                {
                    Console.WriteLine("SPOD Fax Reply Confirmation Number: {0}", reply.FaxConfirmationNumber);
                }
                ShowNotifications(reply);
            }
            catch (SoapException e)
            {
                Console.WriteLine(e.Detail.InnerText);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Press any key to quit!");
            Console.ReadKey();
        }

        private static SignatureProofOfDeliveryFaxRequest CreateSPODFaxRequest()
        {
            // Build the SignatureProofOfDeliveryFaxRequest
            SignatureProofOfDeliveryFaxRequest request = new SignatureProofOfDeliveryFaxRequest();
            //
            request.WebAuthenticationDetail = new WebAuthenticationDetail();
            request.WebAuthenticationDetail.UserCredential = new WebAuthenticationCredential();
            request.WebAuthenticationDetail.UserCredential.Key = "XXX"; // Replace "XXX" with the Key
            request.WebAuthenticationDetail.UserCredential.Password = "XXX"; // Replace "XXX" with the Password
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.WebAuthenticationDetail.UserCredential.Key = getProperty("key");
                request.WebAuthenticationDetail.UserCredential.Password = getProperty("password");
            }
            //
            request.ClientDetail = new ClientDetail();
            request.ClientDetail.AccountNumber = "XXX"; // Replace "XXX" with the client's account number
            request.ClientDetail.MeterNumber = "XXX"; // Replace "XXX" with the client's meter number
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.ClientDetail.AccountNumber = getProperty("accountnumber");
                request.ClientDetail.MeterNumber = getProperty("meternumber");
            }
            //
            request.TransactionDetail = new TransactionDetail();
            request.TransactionDetail.CustomerTransactionId = "***SPOD Request using VC#***"; //This is a reference field for the customer.  Any value can be used and will be provided in the response.
            request.TransactionDetail.Localization = new Localization();
            request.TransactionDetail.Localization.LanguageCode = "EN";
            //
            request.Version = new VersionId();
            //
            SetQualifiedTrackingNumber(request);
            //
            request.AdditionalComments = "NONE";
            //
            SetFaxSender(request);
            //
            SetFaxRecipient(request);
            //
            return request;
        }

        private static void SetQualifiedTrackingNumber(SignatureProofOfDeliveryFaxRequest request)
        {
            request.QualifiedTrackingNumber = new QualifiedTrackingNumber();
            //request.QualifiedTrackingNumber.AccountNumber = "XXX"; // Replace "XXX" with client's account number
            request.QualifiedTrackingNumber.TrackingNumber = "XXX"; // Replace "XXX" with the tracking number
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.QualifiedTrackingNumber.TrackingNumber = getProperty("trackingnumber");
            }
            request.QualifiedTrackingNumber.ShipDate = DateTime.Parse("04/17/2014"); // MM/DD/YYYY
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.QualifiedTrackingNumber.ShipDate = DateTime.Parse(getProperty("date"));
            }
            request.QualifiedTrackingNumber.ShipDateSpecified = true;
        }

        private static void SetFaxSender(SignatureProofOfDeliveryFaxRequest request)
        {
            request.FaxSender = new ContactAndAddress();
            request.FaxSender.Contact = new Contact();
            request.FaxSender.Contact.PersonName = "Ralph Johnson";
            request.FaxSender.Contact.CompanyName = "Farmer's Supply";
            request.FaxSender.Contact.PhoneNumber = "9015550001";
            request.FaxSender.Contact.FaxNumber = "9015550009";
        }

        private static void SetFaxRecipient(SignatureProofOfDeliveryFaxRequest request)
        {
            request.FaxRecipient = new ContactAndAddress();
            request.FaxRecipient.Contact = new Contact();
            request.FaxRecipient.Contact.PersonName = "Tester";
            request.FaxRecipient.Contact.Title = "Tester";
            request.FaxRecipient.Contact.CompanyName = "Acme Supply";
            request.FaxRecipient.Contact.PhoneNumber = "9015551234";
            request.FaxRecipient.Contact.FaxNumber = "9015551234";
            request.FaxRecipient.Address = new Address();
            request.FaxRecipient.Address.StreetLines = new String[1] { "1 Recipient ST" };
            request.FaxRecipient.Address.City = "Colliervile";
            request.FaxRecipient.Address.StateOrProvinceCode = "TN";
            request.FaxRecipient.Address.PostalCode = "38017";
            request.FaxRecipient.Address.CountryCode = "US";
        }

        private static void ShowNotifications(SignatureProofOfDeliveryFaxReply reply)
        {
            Console.WriteLine("Notifications");
            for (int i = 0; i < reply.Notifications.Length; i++)
            {
                Notification notification = reply.Notifications[i];
                Console.WriteLine("Notification no. {0}", i);
                Console.WriteLine(" Severity: {0}", notification.Severity);
                Console.WriteLine(" Code: {0}", notification.Code);
                Console.WriteLine(" Message: {0}", notification.Message);
                Console.WriteLine(" Source: {0}", notification.Source);
            }
        }
        private static bool usePropertyFile() //Set to true for common properties to be set with getProperty function.
        {
            return getProperty("usefile").Equals("True");
        }
        private static String getProperty(String propertyname) //Sets common properties for testing purposes.
        {
            try
            {
                String filename = "C:\\filepath\\filename.txt";
                if (System.IO.File.Exists(filename))
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(filename);
                    do
                    {
                        String[] parts = sr.ReadLine().Split(',');
                        if (parts[0].Equals(propertyname) && parts.Length == 2)
                        {
                            return parts[1];
                        }
                    }
                    while (!sr.EndOfStream);
                }
                Console.WriteLine("Property {0} set to default 'XXX'", propertyname);
                return "XXX";
            }
            catch (Exception e)
            {
                Console.WriteLine("Property {0} set to default 'XXX'", propertyname);
                return "XXX";
            }
        }
    }
}