// This code was built using Visual Studio 2005
using System;
using System.Web.Services.Protocols;
using ServiceAvailabilityWebServiceClient.ServiceAvailabilityWebReference;

//
// Sample code to call the FedEx ServiceAvailability Web Service
//
namespace ServiceAvailabilityWebServiceClient
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceAvailabilityRequest request = CreateServiceAvailabilityRequest();
            //
            ValidationAvailabilityAndCommitmentService service = new ValidationAvailabilityAndCommitmentService(); // Initialize the service
			if (usePropertyFile())
            {
                service.Url = getProperty("endpoint");
            }
            //
            try
            {
                //This is the call to the web service passing in a ServiceAvailabilityRequest and returning a ServiceAvailabilityReply
                ServiceAvailabilityReply reply = service.serviceAvailability(request);
                if (reply.HighestSeverity == NotificationSeverityType.SUCCESS || reply.HighestSeverity == NotificationSeverityType.NOTE || reply.HighestSeverity == NotificationSeverityType.WARNING) // check if the call was successful
                {
                    ShowServiceAvailabilityReply(reply);
                }
                else
                {
                    Console.WriteLine("Service availability failed : {0}", reply.Notifications[0].Message);
                }
            }
            catch (SoapException e)
            {
                Console.WriteLine(e.Detail.InnerText);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Press any key to quit!");
            Console.ReadKey();
        }

        private static ServiceAvailabilityRequest CreateServiceAvailabilityRequest()
        {
            // Build the ServiceAvailabilityRequest
            ServiceAvailabilityRequest request = new ServiceAvailabilityRequest();
            //
            request.WebAuthenticationDetail = new WebAuthenticationDetail();
            request.WebAuthenticationDetail.UserCredential = new WebAuthenticationCredential();
            request.WebAuthenticationDetail.UserCredential.Key = "XXX"; // Replace "XXX" with the Key
            request.WebAuthenticationDetail.UserCredential.Password = "XXX"; // Replace "XXX" with the Password
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.WebAuthenticationDetail.UserCredential.Key = getProperty("key");
                request.WebAuthenticationDetail.UserCredential.Password = getProperty("password");
            }
            //
            request.ClientDetail = new ClientDetail();
            request.ClientDetail.AccountNumber = "XXX"; // Replace "XXX" with the client's account number
            request.ClientDetail.MeterNumber = "XXX"; // Replace "XXX" with the client's meter number
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.ClientDetail.AccountNumber = getProperty("accountnumber");
                request.ClientDetail.MeterNumber = getProperty("meternumber");
            }
            //
            request.TransactionDetail = new TransactionDetail();
            request.TransactionDetail.CustomerTransactionId = "***Service Availability Request using VC#***"; // This is a reference field for the customer.  Any value can be used and will be provided in the response.
            //
            request.Version = new VersionId(); // Creates the Version element with all child elements populated from the wsdl
            //
            request.Origin = new Address(); // Origin information
            request.Origin.PostalCode = "77510";
            request.Origin.CountryCode = "US";
            //
            request.Destination = new Address(); // Destination information
            request.Destination.PostalCode = "38017";
            request.Destination.CountryCode = "US";
            //
            request.ShipDate = DateTime.Now; // Shipping date and time
            request.CarrierCode = CarrierCodeType.FDXE; // Carrier code types are FDXC(Cargo), FDXE(Express), FDXG(Ground), FXCC(Custom Critical), FXFX(Freight)
            //If a service is specified it will be checked, if no service is specified all available services will be returned
            request.Service = ServiceType.PRIORITY_OVERNIGHT; // Service types are STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND ...
            request.ServiceSpecified = true;
            request.Packaging = PackagingType.YOUR_PACKAGING; // Packaging type FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
            request.PackagingSpecified = true;
            return request;
        }

        private static void ShowServiceAvailabilityReply(ServiceAvailabilityReply reply)
        {
            Console.WriteLine("ServiceAvailabilityReply details:");
            Console.WriteLine("*********");
            foreach (ServiceAvailabilityOption option in reply.Options)
            {
                Console.WriteLine("Service Type : " + option.Service.ToString());
                Console.WriteLine("Delivery Date : " + option.DeliveryDate.ToShortDateString());
                Console.WriteLine("Delivery Day : " + option.DeliveryDay.ToString());
                Console.WriteLine("Destination StationId ID : " + option.DestinationStationId);
            }
            Console.WriteLine("*********");
        }
        private static bool usePropertyFile() //Set to true for common properties to be set with getProperty function.
        {
            return getProperty("usefile").Equals("True");
        }
        private static String getProperty(String propertyname) //Sets common properties for testing purposes.
        {
            try
            {
                String filename = "C:\\filepath\\filename.txt";
                if (System.IO.File.Exists(filename))
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(filename);
                    do
                    {
                        String[] parts = sr.ReadLine().Split(',');
                        if (parts[0].Equals(propertyname) && parts.Length == 2)
                        {
                            return parts[1];
                        }
                    }
                    while (!sr.EndOfStream);
                }
                Console.WriteLine("Property {0} set to default 'XXX'", propertyname);
                return "XXX";
            }
            catch (Exception e)
            {
                Console.WriteLine("Property {0} set to default 'XXX'", propertyname);
                return "XXX";
            }
        }
    }
}