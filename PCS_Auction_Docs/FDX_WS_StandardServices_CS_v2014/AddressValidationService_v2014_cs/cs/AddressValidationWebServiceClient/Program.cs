// This code was built using Visual Studio 2005
using System;
using System.Web.Services.Protocols;
using AddressValidationWebServiceClient.AddressValidationServiceWebReference;

namespace AddressValidationWebServiceClient
{
    class Program
    {
        static void Main(string[] args)
        {
            AddressValidationRequest request = CreateAddressValidationRequest();
            //
            AddressValidationService service = new AddressValidationService();
			if (usePropertyFile())
            {
                service.Url = getProperty("endpoint");
            }
            //
            try
            {
                // Call the AddressValidationService passing in an AddressValidationRequest and returning an AddressValidationReply
                AddressValidationReply reply = service.addressValidation(request);
                //
                if (reply.HighestSeverity == NotificationSeverityType.SUCCESS || reply.HighestSeverity == NotificationSeverityType.NOTE || reply.HighestSeverity == NotificationSeverityType.WARNING)
                {
                    ShowAddressValidationReply(reply);
                }
                else
                {
                    foreach (Notification notification in reply.Notifications)
                        Console.WriteLine(notification.Message);
                }
            }
            catch (SoapException e)
            {
                Console.WriteLine(e.Detail.InnerText);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Press any key to quit!");
            Console.ReadKey();
        }

        private static AddressValidationRequest CreateAddressValidationRequest()
        {
            // Build the AddressValidationRequest
            AddressValidationRequest request = new AddressValidationRequest();
            //
            request.WebAuthenticationDetail = new WebAuthenticationDetail();
            request.WebAuthenticationDetail.UserCredential = new WebAuthenticationCredential();
            request.WebAuthenticationDetail.UserCredential.Key = "DayBelTIzrdgj97H"; // Replace "XXX" with the Key
            request.WebAuthenticationDetail.UserCredential.Password = "icnetbGDIhl9RII6YPFHhv9XP"; // Replace "XXX" with the Password
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.WebAuthenticationDetail.UserCredential.Key = getProperty("key");
                request.WebAuthenticationDetail.UserCredential.Password = getProperty("password");
            }
            //
            request.ClientDetail = new ClientDetail();
            request.ClientDetail.AccountNumber = "326787612"; // Replace "XXX" with the client's account number
            request.ClientDetail.MeterNumber = "104522487"; // Replace "XXX" with the client's meter number
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.ClientDetail.AccountNumber = getProperty("accountnumber");
                request.ClientDetail.MeterNumber = getProperty("meternumber");
            }
            //
            request.TransactionDetail = new TransactionDetail();
            request.TransactionDetail.CustomerTransactionId = "***Address Validation Request using VC#***"; // The client will get the same value back in the reply
            //
            request.Version = new VersionId(); // Creates the Version element with all child elements populated
            //
            request.InEffectAsOfTimestamp = DateTime.Now;
            request.InEffectAsOfTimestampSpecified = true;
            //
            SetAddress(request);
            //
            return request;
        }

        private static void SetAddress(AddressValidationRequest request)
        {
            request.AddressesToValidate = new AddressToValidate[2];
            request.AddressesToValidate[0] = new AddressToValidate();
            request.AddressesToValidate[0].ClientReferenceId = "ClientReferenceId1";
            request.AddressesToValidate[0].Address = new Address();
            request.AddressesToValidate[0].Address.StreetLines = new String[1] { "11 Vreeland Rd" };
            request.AddressesToValidate[0].Address.PostalCode = "07932";
            //
            request.AddressesToValidate[1] = new AddressToValidate();
            request.AddressesToValidate[1].ClientReferenceId = "ClientReferenceId2";
            request.AddressesToValidate[1].Address = new Address();
            request.AddressesToValidate[1].Address.StreetLines = new String[1] { "50 N Front St" };
            request.AddressesToValidate[1].Address.PostalCode = "38103";
        }

        private static void ShowAddressValidationReply(AddressValidationReply reply)
        {
            Console.WriteLine("AddressValidationReply details:");
            Console.WriteLine("*****************************************************");
            foreach (AddressValidationResult result in reply.AddressResults)
            {
                Console.WriteLine("Address Id : " + result.ClientReferenceId);
                if(result.ClassificationSpecified) { Console.WriteLine("Classification: " + result.Classification);}
                if (result.StateSpecified) { Console.WriteLine("State: " + result.State); }
                Console.WriteLine("Proposed Address--");
                Address address = result.EffectiveAddress;
                foreach (String street in address.StreetLines)
                {
                    Console.WriteLine("  " + street);
                }
                Console.WriteLine("  " + address.City);
                Console.WriteLine("  " + address.StateOrProvinceCode);
                Console.WriteLine("  " + address.PostalCode);
                Console.WriteLine("  " + address.CountryCode);
                Console.WriteLine();
                Console.WriteLine("Address Attributes:");
                foreach (AddressAttribute attribute in result.Attributes)
                {
                    Console.WriteLine("  " + attribute.Name + ": " + attribute.Value);
                }
            }
        }
        private static bool usePropertyFile() //Set to true for common properties to be set with getProperty function.
        {
            return getProperty("usefile").Equals("True");
        }
        private static String getProperty(String propertyname) //Sets common properties for testing purposes.
        {
            try
            {
                String filename = "C:\\filepath\\filename.txt";
                if (System.IO.File.Exists(filename))
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(filename);
                    do
                    {
                        String[] parts = sr.ReadLine().Split(',');
                        if (parts[0].Equals(propertyname) && parts.Length == 2)
                        {
                            return parts[1];
                        }
                    }
                    while (!sr.EndOfStream);
                }
                Console.WriteLine("Property {0} set to default 'XXX'", propertyname);
                return "XXX";
            }
            catch (Exception e)
            {
                Console.WriteLine("Property {0} set to default 'XXX'", propertyname);
                return "XXX";
            }
        }
    }
}